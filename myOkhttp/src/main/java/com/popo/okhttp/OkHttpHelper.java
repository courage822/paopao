package com.popo.okhttp;

import com.popo.okhttp.builder.GetBuilder;
import com.popo.okhttp.builder.HeadBuilder;
import com.popo.okhttp.builder.OtherRequestBuilder;
import com.popo.okhttp.builder.PostFileBuilder;
import com.popo.okhttp.builder.PostFormBuilder;
import com.popo.okhttp.builder.PostStringBuilder;
import com.popo.okhttp.callback.OkhttpCallback;
import com.popo.okhttp.request.CommonRequest;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * OkHttp帮助类，封装get、post、head等常用的http请求。功能：
 * <li>1、设置公用param和header</li>
 * <li>2、普通的GET、POST请求</li>
 * <li>3、上传JSON等String文件</li>
 * <li>4、上传File文件</li>
 * <li>5、下载文件，提供下载进度</li>
 * <li>6、取消http请求</li>
 * Created by zhangdroid on 2017/5/20.
 */
public class OkHttpHelper {
    private volatile static OkHttpHelper sInstance;
    private OkHttpClient mOkHttpClient;
    private Platform mPlatform;
    // 公用参数
    private static Map<String, String> sCommonParams;
    // 公用参数
    private static Map<String, List<String>> sCommonListParams;
    // 公用header
    private static Map<String, String> sCommonHeaders;

    public OkHttpHelper(OkHttpClient okHttpClient) {
        if (okHttpClient == null) {
            mOkHttpClient = new OkHttpClient();
        } else {
            mOkHttpClient = okHttpClient;
        }
        mPlatform = Platform.get();
        sCommonParams = new LinkedHashMap<>();
        sCommonHeaders = new LinkedHashMap<>();
        sCommonListParams = new LinkedHashMap<>();
    }

    /**
     * @return 获得默认配置
     */
    public static OkHttpHelper getInstance() {
        return init(null);
    }

    /**
     * @param okHttpClient {@link OkHttpClient}
     * @return
     */
    public static OkHttpHelper init(OkHttpClient okHttpClient) {
        if (sInstance == null) {
            synchronized (OkHttpHelper.class) {
                if (sInstance == null) {
                    sInstance = new OkHttpHelper(okHttpClient);
                }
            }
        }
        return sInstance;
    }

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    public Executor getDelivery() {
        return mPlatform.defaultCallbackExecutor();
    }

    public Map<String, String> getCommonParams() {
        return sCommonParams;
    }

    public OkHttpHelper addCommonParams(Map<String, String> params) {
        sCommonParams.putAll(params);
        return this;
    }

    public OkHttpHelper addCommonParam(String key, String val) {
        sCommonParams.put(key, val);
        return this;
    }

    public OkHttpHelper addCommonHeaders(Map<String, String> params) {
        sCommonHeaders.putAll(params);
        return this;
    }

    public OkHttpHelper addCommonHeader(String key, String val) {
        sCommonHeaders.put(key, val);
        return this;
    }

    public static GetBuilder get() {
        GetBuilder getBuilder = new GetBuilder();
        if (null != sCommonParams && !sCommonParams.isEmpty() && sCommonParams.size() > 0) {
            getBuilder.params(sCommonParams);
        }
        if (null != sCommonHeaders && !sCommonHeaders.isEmpty() && sCommonHeaders.size() > 0) {
            getBuilder.headers(sCommonHeaders);
        }
        return getBuilder;
    }

    public static PostFormBuilder post() {
        PostFormBuilder postFormBuilder = new PostFormBuilder();
        if (null != sCommonParams && !sCommonParams.isEmpty() && sCommonParams.size() > 0) {
            postFormBuilder.params(sCommonParams);

        }
        if (null != sCommonHeaders && !sCommonHeaders.isEmpty() && sCommonHeaders.size() > 0) {
            postFormBuilder.headers(sCommonHeaders);
        }
        return postFormBuilder;
    }
    public static PostFormBuilder post2() {
        PostFormBuilder postFormBuilder = new PostFormBuilder();
        if (null != sCommonListParams && !sCommonListParams.isEmpty() && sCommonListParams.size() > 0) {
            postFormBuilder.params2(sCommonListParams);

        }
        if (null != sCommonHeaders && !sCommonHeaders.isEmpty() && sCommonHeaders.size() > 0) {
            postFormBuilder.headers(sCommonHeaders);
        }
        return postFormBuilder;
    }

    public static GetBuilder download() {
        GetBuilder getBuilder = new GetBuilder();
        if (null != sCommonParams && !sCommonParams.isEmpty() && sCommonParams.size() > 0) {
            getBuilder.params(sCommonParams);
        }
        if (null != sCommonHeaders && !sCommonHeaders.isEmpty() && sCommonHeaders.size() > 0) {
            getBuilder.headers(sCommonHeaders);
        }
        return getBuilder;
    }

    public static PostStringBuilder uploadString() {
        PostStringBuilder postStringBuilder = new PostStringBuilder();
        if (null != sCommonParams && !sCommonParams.isEmpty() && sCommonParams.size() > 0) {
            postStringBuilder.params(sCommonParams);
        }
        if (null != sCommonHeaders && !sCommonHeaders.isEmpty() && sCommonHeaders.size() > 0) {
            postStringBuilder.headers(sCommonHeaders);
        }
        return postStringBuilder;
    }

    public static PostFileBuilder uploadFile() {
        PostFileBuilder postFileBuilder = new PostFileBuilder();
        if (null != sCommonParams && !sCommonParams.isEmpty() && sCommonParams.size() > 0) {
            postFileBuilder.params(sCommonParams);
        }
        if (null != sCommonHeaders && !sCommonHeaders.isEmpty() && sCommonHeaders.size() > 0) {
            postFileBuilder.headers(sCommonHeaders);
        }
        return postFileBuilder;
    }

    public static OtherRequestBuilder put() {
        return new OtherRequestBuilder(METHOD.PUT);
    }

    public static HeadBuilder head() {
        return new HeadBuilder();
    }

    public static OtherRequestBuilder delete() {
        return new OtherRequestBuilder(METHOD.DELETE);
    }

    public static OtherRequestBuilder patch() {
        return new OtherRequestBuilder(METHOD.PATCH);
    }

    public void cancelTag(Object tag) {
        for (Call call : mOkHttpClient.dispatcher().queuedCalls()) {
            if (tag.equals(call.request().tag())) {
                call.cancel();
            }
        }
        for (Call call : mOkHttpClient.dispatcher().runningCalls()) {
            if (tag.equals(call.request().tag())) {
                call.cancel();
            }
        }
    }

    public void execute(final CommonRequest requestCall, OkhttpCallback callback) {
        if (callback == null)
            callback = OkhttpCallback.CALLBACK_DEFAULT;
        final OkhttpCallback finalCallback = callback;
        final int id = requestCall.getOkHttpRequest().getId();

        requestCall.getCall().enqueue(new okhttp3.Callback() {

            @Override
            public void onFailure(Call call, final IOException e) {
                sendFailResultCallback(call, e, finalCallback, id);
            }

            @Override
            public void onResponse(final Call call, final Response response) {
                try {
                    if (call.isCanceled()) {
                        sendFailResultCallback(call, new IOException("Canceled!"), finalCallback, id);
                        return;
                    }

                    if (!finalCallback.validateReponse(response, id)) {
                        sendFailResultCallback(call, new IOException("request failed , reponse's code is : " + response.code()), finalCallback, id);
                        return;
                    }

                    Object o = finalCallback.parseNetworkResponse(response, id);
                    sendSuccessResultCallback(o, finalCallback, id);
                } catch (Exception e) {
                    sendFailResultCallback(call, e, finalCallback, id);
                } finally {
                    if (response.body() != null)
                        response.body().close();
                }
            }
        });
    }

    public void sendFailResultCallback(final Call call, final Exception e, final OkhttpCallback callback, final int id) {
        if (callback == null) return;
        mPlatform.execute(new Runnable() {
            @Override
            public void run() {
                callback.onError(call, e, id);
                callback.onAfter(id);
            }
        });
    }

    public void sendSuccessResultCallback(final Object object, final OkhttpCallback callback, final int id) {
        if (callback == null) return;
        mPlatform.execute(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(object, id);
                callback.onAfter(id);
            }
        });
    }

    public static final class METHOD {
        public static final String HEAD = "HEAD";
        public static final String DELETE = "DELETE";
        public static final String PUT = "PUT";
        public static final String PATCH = "PATCH";
    }

}

