package com.popo.okhttp.callback;

import java.io.IOException;

import okhttp3.Response;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public abstract class StringCallback extends OkhttpCallback<String> {

    @Override
    public String parseNetworkResponse(Response response, int id) throws IOException {
        return response.body().string();
    }

}
