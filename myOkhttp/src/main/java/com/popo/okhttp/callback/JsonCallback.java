package com.popo.okhttp.callback;

import android.text.TextUtils;

import com.google.gson.Gson;

import java.lang.reflect.ParameterizedType;

import okhttp3.Response;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public abstract class JsonCallback<T> extends OkhttpCallback<T> {

    @Override
    public T parseNetworkResponse(Response response, int id) throws Exception {
        String json = response.body().string();
        if (!TextUtils.isEmpty(json))
            return new Gson().fromJson(json, (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
        return null;
    }

}
