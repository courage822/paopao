package com.popo.okhttp.builder;

import com.popo.okhttp.request.CommonRequest;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public abstract class OkHttpRequestBuilder<T extends OkHttpRequestBuilder>  {

    protected String url;
    protected Object tag;
    protected Map<String, String> headers;
    protected Map<String, String> params;
    protected Map<String,List<String>> listParams;
    protected int id;

    public OkHttpRequestBuilder() {
        headers = new LinkedHashMap<>();
        params = new LinkedHashMap<>();
        listParams = new LinkedHashMap<>();
    }

    public T id(int id) {
        this.id = id;
        return (T) this;
    }

    public T url(String url) {
        this.url = url;
        return (T) this;
    }

    public T tag(Object tag) {
        this.tag = tag;
        return (T) this;
    }

    public T headers(Map<String, String> headers) {
        this.headers.putAll(headers);
        return (T) this;
    }

    public T addHeader(String key, String val) {
        headers.put(key, val);
        return (T) this;
    }

    public T params(Map<String, String> headers) {
        this.params.putAll(headers);
        return (T) this;
    }
    public T params2(Map<String, List<String>> headers) {
        this.listParams.putAll(headers);
        return (T) this;
    }

    public T addParam(String key, String val) {
        params.put(key, val);
        return (T) this;
    }
    public T addListParams(String key, List<String> values) {//这是为了群打招呼做的参数处理
        if (key != null && values != null && !values.isEmpty()) {
            listParams.put(key, values);
        }

        return (T) this;
    }

    public abstract CommonRequest build();

}
