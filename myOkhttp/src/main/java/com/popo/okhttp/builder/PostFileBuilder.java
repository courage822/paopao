package com.popo.okhttp.builder;

import com.popo.okhttp.request.PostFileRequest;
import com.popo.okhttp.request.CommonRequest;

import java.io.File;

import okhttp3.MediaType;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public class PostFileBuilder extends OkHttpRequestBuilder<PostFileBuilder> {
    private File file;
    private MediaType mediaType;

    public OkHttpRequestBuilder file(File file) {
        this.file = file;
        return this;
    }

    public OkHttpRequestBuilder mediaType(MediaType mediaType) {
        this.mediaType = mediaType;
        return this;
    }

    @Override
    public CommonRequest build() {
        return new PostFileRequest(url, tag, params, headers, file, mediaType, id).build();
    }

}
