package com.popo.okhttp.builder;


import com.popo.okhttp.OkHttpHelper;
import com.popo.okhttp.request.CommonRequest;
import com.popo.okhttp.request.OtherRequest;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public class HeadBuilder extends GetBuilder {

    @Override
    public CommonRequest build() {
        return new OtherRequest(null, null, OkHttpHelper.METHOD.HEAD, url, tag, params, headers, id).build();
    }

}
