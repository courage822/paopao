package com.popo.okhttp.log;

import android.util.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/8/4.
 */

public class SignInterceptor implements Interceptor {
    public SignInterceptor() {}
    public static Map<String, String> parseParams(Request request) {
        //GET POST DELETE PUT PATCH
        String method = request.method();
        Map<String, String> params = null;
        if ("GET".equals(method)) {
            params = doGet(request);
        } else if ("POST".equals(method) || "PUT".equals(method) || "DELETE".equals(method) || "PATCH".equals(method)) {
            RequestBody body = request.body();
            if (body != null && body instanceof FormBody) {
                params = doForm(request);
            }
        }
        return params;
    }
    /**
     * 获取get方式的请求参数
     * @param request
     * @return
     */
    private static Map<String, String> doGet(Request request) {
        Map<String, String> params = null;
        HttpUrl url = request.url();
        Set<String> strings = url.queryParameterNames();
        if (strings != null) {
            Iterator<String> iterator = strings.iterator();
            params = new HashMap<>();
            int i = 0;
            while (iterator.hasNext()) {
                String name = iterator.next();
                String value = url.queryParameterValue(i);
                params.put(name, value);
                i++;
            }
        }
        return params;
    }

    /**
     * 获取表单的请求参数
     * @param request
     * @return
     */
    private static Map<String, String> doForm(Request request) {
        Map<String, String> params = null;
        FormBody body = null;
        try {
            body = (FormBody) request.body();
        } catch (ClassCastException c) {
        }
        if (body != null) {
            int size = body.size();
            if (size > 0) {
                params = new HashMap<>();
                for (int i = 0; i < size; i++) {
                    params.put(body.name(i), body.value(i));
                }
            }
        }
        return params;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();
        //请求体定制：统一添加token参数
        Log.e("AAAAAAA", "intercept: 外部的------");
        if(request.body() instanceof FormBody){
            Log.e("AAAAAAA", "intercept: 内部的6666666666666666666666");
            FormBody oidFormBody = (FormBody) request.body();
            if(oidFormBody!=null&&oidFormBody.size()>0){
                for (int i = 0;i<oidFormBody.size();i++){
                    Log.e("AAAAAAA", "intercept: ------name="+oidFormBody.encodedName(i)+"-------+=value="+oidFormBody.encodedValue(i));
                }
            }
        }
        //Base64加密
      //  String sign_s = android.util.Base64.encodeToString(sign.toString().getBytes(), android.util.Base64.NO_WRAP);
        //Md5加密
//        sign_s = MD5.GetMD5Code(sign_s);
        //重新拼接url
        HttpUrl.Builder httpUrlBuilder = request.url().newBuilder();
        //添加参数
       // httpUrlBuilder.addQueryParameter("signTime", sign_s);
        //httpUrlBuilder.addQueryParameter("sign", timestamp);
        Request.Builder requestBuilder = request.newBuilder();
        requestBuilder.url(httpUrlBuilder.build());
        request = requestBuilder.build();

        return chain.proceed(request);
    }
}
