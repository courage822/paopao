package com.popo.library.image;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.popo.library.R;

/**
 * Image loader config, used build pattern
 * Created by zhangdroid on 2017/5/12.
 */
public class ImageLoader {

    /**
     * 需要加载的网络图片url
     */
    private String url;
    /**
     * 加载中显示的图片
     */
    private int placeHolder;
    /**
     * 加载失败时显示的图片
     */
    private int error;
    /**
     * 对图片的转换（圆形/圆角等）
     */
    private Transformation transformation;
    /**
     * 图片Config
     */
    private Bitmap.Config config;
    private ImageView imageView;

    private ImageLoader(Builder builder) {
        this.url = builder.url;
        this.placeHolder = builder.placeHolder;
        this.error = builder.error;
        this.transformation = builder.transformation;
        this.config = builder.config;
        this.imageView = builder.imageView;
    }

    public String getUrl() {
        return url;
    }

    public int getPlaceHolder() {
        return placeHolder;
    }

    public int getError() {
        return error;
    }

    public Transformation getTransformation() {
        return transformation;
    }

    public Bitmap.Config getConfig() {
        return config;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public static class Builder {
        private String url;
        private int placeHolder;
        private int error;
        private Transformation transformation;
        private Bitmap.Config config;
        private ImageView imageView;

        public Builder() {
            this.url = null;
            this.placeHolder = R.drawable.full_transform;
            this.error = R.drawable.full_transform;
            this.transformation = new Transformation() {
                @Override
                public Resource transform(Resource resource, int outWidth, int outHeight) {
                    return resource;
                }

                @Override
                public String getId() {
                    return "default";
                }
            };
            this.config = Bitmap.Config.ARGB_4444;
            this.imageView = null;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder placeHolder(int placeHolder) {
            this.placeHolder = placeHolder;
            return this;
        }

        public Builder error(int error) {
            this.error = error;
            return this;
        }

        public Builder transform(Transformation transformation) {
            this.transformation = transformation;
            return this;
        }

        public Builder config(Bitmap.Config config) {
            this.config = config;
            return this;
        }

        public Builder imageView(ImageView imageView) {
            this.imageView = imageView;
            return this;
        }

        public ImageLoader build() {
            return new ImageLoader(this);
        }

    }

}
