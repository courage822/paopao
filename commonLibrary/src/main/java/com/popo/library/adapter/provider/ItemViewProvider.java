package com.popo.library.adapter.provider;

import com.popo.library.adapter.RecyclerViewHolder;

/**
 * RecyclerView multi item type provider, every item type is a ItemViewProvider.
 * Created by zhangdroid on 2017/5/18.
 */
public interface ItemViewProvider<T> {

    /**
     * @return The layout resource id of this item view
     */
    int getItemViewLayoutResId();

    /**
     * Check if this item is the right view type.
     *
     * @param item     the item bean
     * @param position the item position in the adapter
     * @return
     */
    boolean isViewType(T item, int position);

    /**
     * Implement this method and use the view holder to adapt the view of the given item bean.
     *
     * @param t        the item bean to display
     * @param position the item position in the adapter
     * @param holder   the fully helper {@link RecyclerViewHolder} for RecyclerView
     */
    void convert(T t, int position, RecyclerViewHolder holder);

}
