package com.popo.library.adapter;

import android.content.Context;

import com.popo.library.adapter.provider.ItemViewProvider;

import java.util.List;

/**
 * <p>The common base adapter for RecyclerView. This adapter is only support single item type.
 * For multi item type, use the {@link MultiTypeRecyclerViewAdapter} instead.<p/>
 * Created by zhangdroid on 2017/5/18.
 */
public abstract class  CommonRecyclerViewAdapter<T> extends MultiTypeRecyclerViewAdapter<T> {

    public CommonRecyclerViewAdapter(Context context, int layoutResId) {
        this(context, layoutResId, null);
    }

    public CommonRecyclerViewAdapter(Context context, final int layoutResId, List<T> dataList) {
        super(context, dataList);
        addItemViewProvider(new ItemViewProvider<T>() {
            @Override
            public int getItemViewLayoutResId() {
                return layoutResId;
            }

            @Override
            public boolean isViewType(T item, int position) {
                // 只有一种布局
                return true;
            }

            @Override
            public void convert(T t, int position, RecyclerViewHolder holder) {
                CommonRecyclerViewAdapter.this.convert(t, position, holder);
            }
        });
    }

    /**
     * Implement this method and use the view holder to adapt the view of the given item bean.
     *
     * @param t        the item bean to display
     * @param position the item position in the adapter
     * @param holder   the fully helper {@link RecyclerViewHolder} for RecyclerView
     */
    public abstract void convert(T t, int position, RecyclerViewHolder holder);

}
