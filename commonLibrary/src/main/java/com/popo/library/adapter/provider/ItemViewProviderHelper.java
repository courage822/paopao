package com.popo.library.adapter.provider;

import android.support.v4.util.SparseArrayCompat;

import com.popo.library.adapter.RecyclerViewHolder;

/**
 * Multi type item view helper for RecyclerView.
 * Created by zhangdroid on 2017/5/18.
 */
public class ItemViewProviderHelper<T> {
    // 保存所有添加的item
    private SparseArrayCompat<ItemViewProvider<T>> mProviders = new SparseArrayCompat();

    public ItemViewProviderHelper<T> addProvider(ItemViewProvider<T> provider) {
        if (null != provider) {
            mProviders.put(mProviders.size(), provider);
        }
        return this;
    }

    public ItemViewProviderHelper<T> addProvider(int viewType, ItemViewProvider<T> provider) {
        if (null != mProviders.get(viewType)) {
            throw new IllegalArgumentException(
                    "An ItemViewProvider is already registered for the viewType = " + viewType
                            + ". Already registered ItemViewProvider is " + mProviders.get(viewType));
        }
        mProviders.put(viewType, provider);
        return this;
    }

    /**
     * Get item view type with specific item bean and position
     */
    public int getItemViewType(T item, int position) {
        int size = mProviders.size();
        for (int i = size - 1; i >= 0; i--) {
            ItemViewProvider<T> provider = mProviders.valueAt(i);
            if (provider.isViewType(item, position)) {
                return mProviders.keyAt(i);
            }
        }
        throw new IllegalArgumentException("No ItemViewProvider added that matches position=" + position + " in data source");
    }

    /**
     * Get item view with specific view type
     */
    public int getItemViewLayoutResId(int viewType) {
        return getItemViewProvider(viewType).getItemViewLayoutResId();
    }

    /**
     * Set the item data with the item bean and RecyclerViewHolder
     */
    public void convert(T item, int position, RecyclerViewHolder holder) {
        int size = mProviders.size();
        for (int i = 0; i < size; i++) {
            ItemViewProvider<T> provider = mProviders.valueAt(i);
            if (provider.isViewType(item, position)) {
                provider.convert(item, position, holder);
                return;
            }
        }
        throw new IllegalArgumentException("No ItemViewProviderHelper added that matches position=" + position + " in data source");
    }

    /**
     * @return 返回当前设置的item类型数目
     */
    public int getItemViewProviderCount() {
        return mProviders.size();
    }

    /**
     * 根据设置的viewType返回对应的ItemViewProvider
     */
    public ItemViewProvider getItemViewProvider(int viewType) {
        return mProviders.get(viewType);
    }

}
