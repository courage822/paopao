package com.popo.library.adapter.wrapper;

/**
 * Created by zhangdroid on 2017/6/7.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
