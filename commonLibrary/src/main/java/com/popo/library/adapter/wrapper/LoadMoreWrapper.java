package com.popo.library.adapter.wrapper;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.popo.library.adapter.RecyclerViewHolder;

/**
 * A wrapper for recyclerview adapter to show load more.
 * Created by zhangdroid on 2017/5/18.
 */
public class LoadMoreWrapper extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    // 加载更多类型常量
    private static final int ITEM_TYPE_LOAD_MORE = Integer.MAX_VALUE - 2;
    // 需要显示加载更多的adapter
    private RecyclerView.Adapter mInnerAdapter;
    // 需要显示加载更多的Item view
    private View mLoadMoreView;
    private int mLoadMoreLayoutId;
    // 加载更多RecyclerViewHolder
    private RecyclerViewHolder mRecyclerViewHolder;

    public LoadMoreWrapper(RecyclerView.Adapter adapter) {
        if (null == adapter) {
            throw new IllegalArgumentException("The parameter adapter cannot be null.");
        } else {
            this.mInnerAdapter = adapter;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isShowLoadMore(position)) {
            return ITEM_TYPE_LOAD_MORE;
        }
        return mInnerAdapter.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE_LOAD_MORE) {
            if (null != mLoadMoreView) {
                mRecyclerViewHolder = RecyclerViewHolder.createViewHolder(parent.getContext(), mLoadMoreView);
            } else if (mLoadMoreLayoutId != 0) {
                mRecyclerViewHolder = RecyclerViewHolder.createViewHolder(parent.getContext(), parent, mLoadMoreLayoutId);
            }
            if (null != mRecyclerViewHolder) {
                return mRecyclerViewHolder;
            }
        }
        return mInnerAdapter.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (isShowLoadMore(position)) {
            return;
        }
        mInnerAdapter.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return mInnerAdapter.getItemCount() + (hasLoadMore() ? 1 : 0);
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        // 处理StaggeredGridLayoutManager，设置充满整行
        ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();
        if (null != layoutParams && layoutParams instanceof StaggeredGridLayoutManager.LayoutParams) {
            int position = holder.getLayoutPosition();
            // 设置HeaderView充满整行
            StaggeredGridLayoutManager.LayoutParams lp = (StaggeredGridLayoutManager.LayoutParams) layoutParams;
            lp.setFullSpan(isShowLoadMore(position));
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        // 处理GridLayoutManager，设置充满整行
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager) {
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
            // 设置某个item所占据的列数或行数
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {

                @Override
                public int getSpanSize(int position) {
                    if (isShowLoadMore(position)) {
                        // 设置充满整行
                        return gridLayoutManager.getSpanCount();
                    }
                    return 1;
                }
            });
        }
    }

    private boolean hasLoadMore() {
        return mLoadMoreView != null || mLoadMoreLayoutId != 0;
    }

    private boolean isShowLoadMore(int position) {
        return hasLoadMore() && (position >= mInnerAdapter.getItemCount());
    }

    // **************************************** 设置加载更多布局 ****************************************

    public void setLoadMoreView(int layoutId) {
        this.mLoadMoreLayoutId = layoutId;
    }

    public void setLoadMoreView(View loadMoreView) {
        this.mLoadMoreView = loadMoreView;
    }

    public void hide() {
        if (hasLoadMore() && null != mRecyclerViewHolder) {
            mRecyclerViewHolder.getConvertView().setVisibility(View.GONE);
        }
    }

    public void show() {
        if (hasLoadMore() && null != mRecyclerViewHolder) {
            mRecyclerViewHolder.getConvertView().setVisibility(View.VISIBLE);
        }
    }

    public void setText(int viewId, String msg) {
        if (hasLoadMore() && null != mRecyclerViewHolder) {
            mRecyclerViewHolder.setText(viewId, msg);
        }
    }

    public void setVisibility(int viewId, boolean isVisible) {
        if (hasLoadMore() && null != mRecyclerViewHolder) {
            mRecyclerViewHolder.setVisibility(viewId, isVisible);
        }
    }

}
