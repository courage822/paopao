package com.popo.library.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * <p>The common ViewHolder for RecyclerView. This class help {@link MultiTypeRecyclerViewAdapter}/{@link CommonRecyclerViewAdapter} obtain the ViewHolder and the associated item data,
 * and provide some support methods to help to set the view properties.</p>
 * Created by zhangdroid on 2017/5/18.
 */
public class RecyclerViewHolder extends RecyclerView.ViewHolder {
    // 获得Context对象，方便在ViewHolder中使用
    protected Context mContext;
    private View mConvertView;
    /**
     * 保存ViewHolder中的每个item对应的View，key为View的ID
     */
    private SparseArray<View> mItemViews;

    public RecyclerViewHolder(Context context, View itemView) {
        super(itemView);
        this.mContext = context;
        this.mConvertView = itemView;
        if (null == mItemViews) {
            mItemViews = new SparseArray<View>();
        }
        // set RecyclerViewHolder as tag
        mConvertView.setTag(this);
    }

    public static RecyclerViewHolder getInstance(Context context, View convertView, ViewGroup parent, int layoutResId) {
        if (convertView == null) {
            View itemView = LayoutInflater.from(context).inflate(layoutResId, parent, false);
            return new RecyclerViewHolder(context, itemView);
        }
        // get tag RecyclerViewHolder
        RecyclerViewHolder tagBaseAdapterHelper = (RecyclerViewHolder) convertView.getTag();
        return tagBaseAdapterHelper;
    }

    public static RecyclerViewHolder createViewHolder(Context context, View itemView) {
        return new RecyclerViewHolder(context, itemView);
    }

    public static RecyclerViewHolder createViewHolder(Context context, ViewGroup parent, int itemViewLayoutRes) {
        return new RecyclerViewHolder(context, LayoutInflater.from(context).inflate(itemViewLayoutRes, parent, false));
    }

    //************************************** 以下是View相关常用辅助方法 **************************************

    public Context getContext() {
        return mContext;
    }

    public View getConvertView() {
        return mConvertView;
    }

    /**
     * 通过ID获得View
     *
     * @param viewId The id of view that you want to get
     */
    public View getView(int viewId) {
        View view = mItemViews.get(viewId);
        if (view == null) {
            view = mConvertView.findViewById(viewId);
            mItemViews.put(viewId, view);
        }
        return view;
    }

    public RecyclerViewHolder setVisibility(int viewId, boolean isVisible) {
        getView(viewId).setVisibility(isVisible ? View.VISIBLE : View.GONE);
        return this;
    }

    public RecyclerViewHolder setText(int viewId, String text) {
        TextView textView = (TextView) getView(viewId);
        textView.setText(text);
        return this;
    }

    public RecyclerViewHolder setTextColorResource(int viewId, int textColorRes) {
        TextView textView = (TextView) getView(viewId);
        textView.setTextColor(mContext.getResources().getColor(textColorRes));
        return this;
    }

    public RecyclerViewHolder setTextColor(int viewId, int color) {
        TextView textView = (TextView) getView(viewId);
        textView.setTextColor(color);
        return this;
    }

    public RecyclerViewHolder setTextSize(int viewId, float size) {
        TextView textView = (TextView) getView(viewId);
        textView.setTextSize(size);
        return this;
    }

    public RecyclerViewHolder setImageResource(int viewId, int resId) {
        ImageView imageView = (ImageView) getView(viewId);
        imageView.setImageResource(resId);
        return this;
    }

    public RecyclerViewHolder setImageBitmap(int viewId, Bitmap bitmap) {
        ImageView imageView = (ImageView) getView(viewId);
        imageView.setImageBitmap(bitmap);
        return this;
    }

    public RecyclerViewHolder setImageDrawable(int viewId, Drawable drawable) {
        ImageView imageView = (ImageView) getView(viewId);
        imageView.setImageDrawable(drawable);
        return this;
    }

    public RecyclerViewHolder setBackgroudColor(int viewId, int color) {
        getView(viewId).setBackgroundColor(color);
        return this;
    }

    public RecyclerViewHolder setBackgroudResource(int viewId, int backgroudRes) {
        getView(viewId).setBackgroundResource(backgroudRes);
        return this;
    }

    //************************************** 以下是View事件相关辅助方法 **************************************

    public RecyclerViewHolder setOnClickListener(int viewId, View.OnClickListener listener) {
        if (null != listener) {
            getView(viewId).setOnClickListener(listener);
        }
        return this;
    }

    public RecyclerViewHolder setOnLongClickListener(int viewId, View.OnLongClickListener listener) {
        if (null != listener) {
            getView(viewId).setOnLongClickListener(listener);
        }
        return this;
    }

    public RecyclerViewHolder setOnTouchClickListener(int viewId, View.OnTouchListener listener) {
        if (null != listener) {
            getView(viewId).setOnTouchListener(listener);
        }
        return this;
    }

}
