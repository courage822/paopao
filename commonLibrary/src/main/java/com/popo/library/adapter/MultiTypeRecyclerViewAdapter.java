package com.popo.library.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.popo.library.adapter.provider.ItemViewProvider;
import com.popo.library.adapter.provider.ItemViewProviderHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>Multi item type for RecyclerView.</p>
 * Created by zhangdroid on 2017/5/18.
 */
public abstract class MultiTypeRecyclerViewAdapter<T> extends RecyclerView.Adapter<RecyclerViewHolder> {
    protected Context mContext;
    // 数据源
    private List<T> mDataList;
    // 多种类型的item帮助类
    private ItemViewProviderHelper<T> mItemViewProviderHelper;
    // 单击及长按事件监听器
    private OnItemClickListener mOnItemClickListener;
    private OnItemLongClickListener mOnItemLongClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        if (onItemClickListener != null) {
            mOnItemClickListener = onItemClickListener;
        }
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        if (onItemLongClickListener != null) {
            mOnItemLongClickListener = onItemLongClickListener;
        }
    }

    public interface OnItemClickListener {
        /**
         * Called when a item view of RecyclerView has been clicked
         *
         * @param view       The view that was clicked
         * @param position   Adapter position of the item that was clicked
         * @param viewHolder {@link RecyclerViewHolder} for RecyclerView
         */
        void onItemClick(View view, int position, RecyclerViewHolder viewHolder);
    }

    public interface OnItemLongClickListener {
        /**
         * Called when a item view of RecyclerView has been long clicked
         *
         * @param view       The view that was long clicked
         * @param position   Adapter position of the item that was long clicked
         * @param viewHolder {@link RecyclerViewHolder} for RecyclerView
         */
        void onItemLongClick(View view, int position, RecyclerViewHolder viewHolder);
    }

    public MultiTypeRecyclerViewAdapter(Context context) {
        this(context, null);
    }

    public MultiTypeRecyclerViewAdapter(Context context, List<T> dataList) {
        this.mContext = context;
        if (!isListEmpty(dataList)) {
            this.mDataList = dataList;
        } else {
            mDataList = new ArrayList<>();
        }
        this.mItemViewProviderHelper = new ItemViewProviderHelper<>();
    }

    @Override
    public int getItemViewType(int position) {
        if (mItemViewProviderHelper.getItemViewProviderCount() > 0) {
            return mItemViewProviderHelper.getItemViewType(getItemByPosition(position), position);
        }
        return super.getItemViewType(position);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return RecyclerViewHolder.getInstance(mContext, null, parent, mItemViewProviderHelper.getItemViewLayoutResId(viewType));
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        mItemViewProviderHelper.convert(getItemByPosition(position), position, holder);
        // 设置点击和长按事件监听
        addListeners(holder, position);
    }

    @Override
    public int getItemCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    /**
     * Add item onItemClick and onItemLongClick listener
     *
     * @param holder   {@link RecyclerViewHolder} for RecyclerView
     * @param position Adapter position of the item
     */
    private void addListeners(final RecyclerViewHolder holder, final int position) {
        holder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(v, position, holder);
                }
            }
        });
        holder.getConvertView().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mOnItemLongClickListener != null) {
                    mOnItemLongClickListener.onItemLongClick(v, position, holder);
                    return true;
                }
                return false;
            }
        });
    }

    //**************************************  多类型布局 **************************************

    public MultiTypeRecyclerViewAdapter<T> addItemViewProvider(ItemViewProvider<T> itemViewProvider) {
        mItemViewProviderHelper.addProvider(itemViewProvider);
        return this;
    }

    public MultiTypeRecyclerViewAdapter<T> addItemViewProvider(int viewType, ItemViewProvider<T> itemViewProvider) {
        mItemViewProviderHelper.addProvider(viewType, itemViewProvider);
        return this;
    }

    //************************************** 数据处理公用方法 **************************************

    public boolean isListEmpty(List<T> list) {
        return list == null || list.size() == 0;
    }

    /**
     * @return 返回Adater数据源集合
     */
    public List<T> getAdapterDataList() {
        return mDataList;
    }

    /**
     * @return 返回Adater数据源集合大小
     */
    public int getSize() {
        return isListEmpty(mDataList) ? 0 : mDataList.size();
    }

    /**
     * 判断当前索引是否有效
     */
    private boolean isValidPosition(int position) {
        return (position >= 0 && position <= getItemCount());
    }

    /**
     * 根据指定位置获得对应的数据对象
     *
     * @param position The position of which you want to get
     * @return T
     */
    public T getItemByPosition(int position) {
        if (!isListEmpty(mDataList) && isValidPosition(position)) {
            return mDataList.get(position);
        }
        return null;
    }

    /**
     * 绑定数据源
     */
    public void bind(List<T> list) {
        if (isListEmpty(list)) {
            return;
        }
        if (!isListEmpty(mDataList)) {
            mDataList.clear();
        }
        mDataList.addAll(list);
        notifyDataSetChanged();
    }

    /**
     * 分页时使用
     */
    public void appendToList(List<T> list) {
        if (isListEmpty(list)) {
            return;
        }
        mDataList.addAll(list);
        notifyDataSetChanged();
    }

    /**
     * 清空数据
     */
    public void clear() {
        if (!isListEmpty(mDataList)) {
            mDataList.clear();
            notifyDataSetChanged();
        }
    }

    /**
     * 往指定位置插入item
     *
     * @param position
     * @param t
     */
    public void insertItem(int position, T t) {
        if (!isValidPosition(position)) {
            return;
        }
        mDataList.add(position, t);
        notifyItemInserted(position);
        // 由于复用机制，插入Item后需要刷新position之后所有item，防止Position错乱
        notifyItemRangeChanged(position, getItemCount() - 1 - position);
    }

    /**
     * 局部刷新
     */
    public void updateItem(int position, T t) {
        if (!isValidPosition(position)) {
            return;
        }
        mDataList.set(position, t);
        notifyItemChanged(position, t);
    }

    /**
     * 删除指定位置的item
     *
     * @param position
     */
    public void removeItem(int position) {
        if (!isValidPosition(position)) {
            return;
        }
        mDataList.remove(position);
        notifyItemRemoved(position);
        // 由于复用机制，删除Item后需要刷新position之后所有item，防止Position错乱
        notifyItemRangeChanged(position, getItemCount() - 1 - position);
    }

    /**
     * 删除指定位置的item
     *
     * @param t
     */
    public void removeItem(T t) {
        if (t == null) {
            return;
        }
        removeItem(mDataList.indexOf(t));
    }

    /**
     * 将一个item从fromPosition移动toPosition
     *
     * @param fromPosition 开始索引
     * @param toPosition   目标索引
     */
    public void moveItem(int fromPosition, int toPosition) {
        if (!isValidPosition(fromPosition) || !isValidPosition(toPosition)) {
            return;
        }
        Collections.swap(mDataList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }

}
