package com.popo.library.adapter.wrapper;

import android.support.v4.util.SparseArrayCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.popo.library.adapter.RecyclerViewHolder;

/**
 * A wrapper for recyclerview adapter to add header and footer.
 * Created by zhangdroid on 2017/5/18.
 */
public class HeaderAndFooterWrapper extends RecyclerView.Adapter {
    // header类型常量起始值，多个header时加1
    private static final int HEADER_TYPE_BASE = 1000000;
    // footer类型常量起始值，多个footer时加1
    private static final int FOOTER_TYPE_BASE = 9000000;
    // 保存所有添加的header的集合，key为int类型，方便区分多header的类型
    private SparseArrayCompat<View> mHeaderViews = new SparseArrayCompat<>();
    // 保存所有添加的footer的集合，key为int类型，方便区分多footer的类型
    private SparseArrayCompat<View> mFooterViews = new SparseArrayCompat<>();

    // 需要添加header或footer的Adapter
    private RecyclerView.Adapter mInnerAdapter;
    // 设置Header和Footer是否占满正行，默认占满
    private boolean mIsFullSpan = true;

    public HeaderAndFooterWrapper(RecyclerView.Adapter adapter) {
        this.mInnerAdapter = adapter;
    }

    @Override
    public int getItemViewType(int position) {
        if (isHeaderView(position)) {
            return mHeaderViews.keyAt(position);
        } else if (isFooterView(position)) {
            return mFooterViews.keyAt(position - getInnerAdapterCount() - getHeaderViewCount());
        }
        return mInnerAdapter.getItemViewType(position - getHeaderViewCount());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View headerView = mHeaderViews.get(viewType);
        if (null != headerView && viewType >= HEADER_TYPE_BASE) {
            return RecyclerViewHolder.createViewHolder(parent.getContext(), headerView);
        }
        View footerView = mFooterViews.get(viewType);
        if (null != footerView && viewType >= FOOTER_TYPE_BASE) {
            return RecyclerViewHolder.createViewHolder(parent.getContext(), footerView);
        }
        return mInnerAdapter.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (isHeaderView(position) || isFooterView(position)) {
            return;
        }
        mInnerAdapter.onBindViewHolder(holder, position - getHeaderViewCount());
    }

    @Override
    public int getItemCount() {
        // header数+ footer数 + 原本的adapter个数
        return getHeaderViewCount() + getFooterViewCount() + getInnerAdapterCount();
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if (mIsFullSpan) {
            // 处理StaggeredGridLayoutManager，设置充满整行
            ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();
            if (null != layoutParams && layoutParams instanceof StaggeredGridLayoutManager.LayoutParams) {
                int position = holder.getLayoutPosition();
                // 设置HeaderView充满整行
                StaggeredGridLayoutManager.LayoutParams lp = (StaggeredGridLayoutManager.LayoutParams) layoutParams;
                lp.setFullSpan(isHeaderView(position) || isFooterView(position));
            }
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (mIsFullSpan) {
            // 处理GridLayoutManager，设置充满整行
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof GridLayoutManager) {
                final GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
                // 设置某个item所占据的列数或行数
                gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {

                    @Override
                    public int getSpanSize(int position) {
                        if (isHeaderView(position) || isFooterView(position)) {
                            // 设置充满整行
                            return gridLayoutManager.getSpanCount();
                        }
                        return 1;
                    }
                });
            }
        }
    }

    /**
     * @return 原本adapter中item的个数
     */
    private int getInnerAdapterCount() {
        return mInnerAdapter.getItemCount();
    }

    /**
     * 判断当前索引下的item是否为header view
     *
     * @param position
     * @return
     */
    private boolean isHeaderView(int position) {
        return (getHeaderViewCount() > 0) && (position < getHeaderViewCount());
    }

    /**
     * 判断当前索引下的item是否为footer view
     *
     * @param position
     * @return
     */
    private boolean isFooterView(int position) {
        return (getFooterViewCount() > 0) && (position >= getHeaderViewCount() + getInnerAdapterCount());
    }

    // ******************************** 公用方法 ********************************

    /**
     * 设置HeaderView和FooterView是否充满
     *
     * @param isFullSpan true表示充满，false表示只占据一个item的空间
     */
    public void setFullSpan(boolean isFullSpan) {
        this.mIsFullSpan = isFullSpan;
    }

    /**
     * 添加HeaderView
     *
     * @param view 待添加的HeaderView
     */
    public void addHeaderView(View view) {
        mHeaderViews.put(HEADER_TYPE_BASE + getHeaderViewCount(), view);
    }

    /**
     * 添加HeaderView并设置点击事件监听
     *
     * @param view            待添加的HeaderView
     * @param onClickListener 点击事件监听器
     */
    public void addHeaderView(View view, View.OnClickListener onClickListener) {
        mHeaderViews.put(HEADER_TYPE_BASE + getHeaderViewCount(), view);
        if (null != onClickListener) {
            view.setOnClickListener(onClickListener);
        }
    }

    /**
     * 添加FooterView
     *
     * @param view 待添加的FooterView
     */
    public void addFooterView(View view) {
        mFooterViews.put(FOOTER_TYPE_BASE + getFooterViewCount(), view);
    }

    /**
     * 添加FooterView并设置点击事件监听
     *
     * @param view            待添加的FooterView
     * @param onClickListener 点击事件监听器
     */
    public void addFooterView(View view, View.OnClickListener onClickListener) {
        mFooterViews.put(FOOTER_TYPE_BASE + getFooterViewCount(), view);
        if (null != onClickListener) {
            view.setOnClickListener(onClickListener);
        }
    }

    /**
     * @return The count of header views
     */
    public int getHeaderViewCount() {
        return mHeaderViews.size();
    }

    /**
     * @return The count of footer views
     */
    public int getFooterViewCount() {
        return mFooterViews.size();
    }

}
