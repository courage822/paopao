package com.popo.library.widget.tab;

/**
 * Child tab view for{@link TabLayout}
 * Created by zhangdroid on 2017/5/23.
 */
public class TabView {
    public String mTabLabel;
    public int mDefaultImgRes;
    public int mSelectedImgRes;
    public ItemBadge mItemBadge;

    public TabView(String mTabLabel, int mDefaultImgRes, int mSelectedImgRes) {
        this.mTabLabel = mTabLabel;
        this.mDefaultImgRes = mDefaultImgRes;
        this.mSelectedImgRes = mSelectedImgRes;
    }

    public TabView setBadge(ItemBadge itemBadge) {
        this.mItemBadge = itemBadge;
        return this;
    }

}