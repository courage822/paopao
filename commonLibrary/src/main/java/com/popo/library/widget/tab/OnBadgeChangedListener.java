package com.popo.library.widget.tab;

/**
 * Created by zhangdroid on 2017/5/23.
 */
public interface OnBadgeChangedListener {
    /**
     * Called when the item badge is show.
     */
    void onBadgeShown(ItemBadge itemBadge);

    /**
     * Called when the item badge is hide.
     */
    void onBadgeHidden(ItemBadge itemBadge);
}
