package com.popo.library.widget.tab;

/**
 * Created by zhangdroid on 2017/5/23.
 */
public interface OnTabSelectedListener {
    /**
     * Called when tab is selected.
     *
     * @param position the position of selected tab
     */
    void onTabSelected(int position);
}
