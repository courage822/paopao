package com.popo.library.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.popo.library.adapter.wrapper.HeaderAndFooterWrapper;
import com.popo.library.adapter.wrapper.LoadMoreWrapper;
import com.popo.library.adapter.wrapper.OnLoadMoreListener;

import java.util.Arrays;

/**
 * A RecyclerView can load more and add header and footer easy.
 * Created by zhangdroid on 2017/6/8.
 */
public class XRecyclerView extends RecyclerView {
    private HeaderAndFooterWrapper mHeaderAndFooterWrapper;
    private LayoutInflater mLayoutInflater;
    private LoadMoreWrapper mLoadMoreWrapper;
    private OnLoadMoreListener mOnLoadingMoreListener;
    // 标记是否正在加载更多
    private boolean mIsLoadingMore;

    public XRecyclerView(Context context) {
        this(context, null);
    }

    public XRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public XRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mLayoutInflater = LayoutInflater.from(context);
        addOnScrollListener(new OnScrollListener() {
            // RecyclerView最后可见项的索引
            int lastVisibleItemPosition = 0;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                // 停止滚动并且已经滚动到底部
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (!mIsLoadingMore && newState == RecyclerView.SCROLL_STATE_IDLE && !recyclerView.canScrollVertically(1) && layoutManager.getChildCount() > 0
                        && lastVisibleItemPosition >= layoutManager.getItemCount() - 1) {
                    // 加载更多
                    mIsLoadingMore = true;
                    if (null != mOnLoadingMoreListener) {
                        mOnLoadingMoreListener.onLoadMore();
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // 计算滚动后位置坐标
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager instanceof GridLayoutManager) {
                    GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
                    lastVisibleItemPosition = gridLayoutManager.findLastVisibleItemPosition();
                } else if (layoutManager instanceof LinearLayoutManager) {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
                    lastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();
                } else if (layoutManager instanceof StaggeredGridLayoutManager) {
                    StaggeredGridLayoutManager staggeredGridLayoutManager = (StaggeredGridLayoutManager) layoutManager;
                    int[] lastInto = staggeredGridLayoutManager.findLastVisibleItemPositions(new int[staggeredGridLayoutManager.getSpanCount()]);
                    // 排序并取最大值
                    Arrays.sort(lastInto);
                    lastVisibleItemPosition = lastInto[lastInto.length - 1];
                }
            }
        });
    }

    private void initAdapter() {
        if (null == mHeaderAndFooterWrapper) {
            mHeaderAndFooterWrapper = new HeaderAndFooterWrapper(getAdapter());
        }
    }

    // ************************************** 公用方法（添加header和footer时必须先调用setAdapter()） **************************************

    /**
     * 设置适配器和加载更多布局
     *
     * @param adapter  inner adapter
     * @param layoutId layour of load more
     */
    public void setAdapter(Adapter adapter, int layoutId) {
        if (null != adapter) {
            mLoadMoreWrapper = new LoadMoreWrapper(adapter);
            mLoadMoreWrapper.setLoadMoreView(layoutId);
            mLoadMoreWrapper.hide();// 初始化时不显示
            super.setAdapter(mLoadMoreWrapper);
        } else {
            throw new IllegalArgumentException("You must set a not null adapter.");
        }
    }

    public void setAdapter(Adapter adapter, View view) {
        if (null != adapter) {
            mLoadMoreWrapper = new LoadMoreWrapper(adapter);
            mLoadMoreWrapper.setLoadMoreView(view);
            mLoadMoreWrapper.hide();// 初始化时不显示
            super.setAdapter(mLoadMoreWrapper);
        } else {
            throw new IllegalArgumentException("You must set a not null adapter.");
        }
    }

    public void setOnLoadingMoreListener(OnLoadMoreListener onLoadingMoreListener) {
        if (null != onLoadingMoreListener) {
            this.mOnLoadingMoreListener = onLoadingMoreListener;
        }
    }

    /**
     * 开始加载更多
     */
    public void startLoadMore() {
        mIsLoadingMore = false;
        if (null != mLoadMoreWrapper) {
            mLoadMoreWrapper.show();
        }
    }

    /**
     * 加载更多完成
     */
    public void finishLoadMore() {
        if (null != mLoadMoreWrapper) {
            mLoadMoreWrapper.hide();
        }
        mIsLoadingMore = false;
    }

    /**
     * 设置提示信息
     *
     * @param viewId 显示提示的View
     * @param msg    提示信息
     */
    public void setText(int viewId, String msg) {
        if (null != mLoadMoreWrapper) {
            mLoadMoreWrapper.setText(viewId, msg);
        }
    }

    /**
     * 加载更多状态改变时调用（显示/隐藏指定的View）
     *
     * @param viewId
     * @param isVisible
     */
    public void setVisibility(int viewId, boolean isVisible) {
        if (null != mLoadMoreWrapper) {
            mLoadMoreWrapper.setVisibility(viewId, isVisible);
        }
    }

    public void addHeaderView(int viewId) {
        if (viewId != 0) {
            initAdapter();
            mHeaderAndFooterWrapper.addHeaderView(mLayoutInflater.inflate(viewId, null));
            super.setAdapter(mHeaderAndFooterWrapper);
        }
    }

    public void addHeaderView(View view) {
        if (null != view) {
            initAdapter();
            mHeaderAndFooterWrapper.addHeaderView(view);
            super.setAdapter(mHeaderAndFooterWrapper);
        }
    }

    public void addFooterView(int viewId) {
        if (viewId != 0) {
            initAdapter();
            mHeaderAndFooterWrapper.addHeaderView(mLayoutInflater.inflate(viewId, null));
            super.setAdapter(mHeaderAndFooterWrapper);
        }
    }

    public void addFooterView(View view) {
        if (null != view) {
            initAdapter();
            mHeaderAndFooterWrapper.addHeaderView(view);
            super.setAdapter(mHeaderAndFooterWrapper);
        }
    }

}
