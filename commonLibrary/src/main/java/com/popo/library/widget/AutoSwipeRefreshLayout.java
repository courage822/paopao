package com.popo.library.widget;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 该控件初次进入时会自动显示SwipeRefreshLayout的刷新动画
 * Created by zhangdroid on 2016/2/17.
 */
public class AutoSwipeRefreshLayout extends SwipeRefreshLayout {
    private boolean mMeasure = false;
    // 向下滑动的距离，超过后则认为是下拉
    private int mTouchSlop;
    // 是否正在横向滑动
    private boolean mIsHorizontalScroll;
    // 起始坐标
    private float startX = 0;
    private float startY = 0;

    public AutoSwipeRefreshLayout(Context context) {
        this(context, null);
    }

    public AutoSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (!mMeasure) {
            mMeasure = true;
            // only run once time
            autoRefresh();
        }
    }

    /**
     * 利用反射调用SwipeRefreshLayout的私有方法
     * <p>{@link SwipeRefreshLayout#setRefreshing(boolean refreshing, boolean notify)}</p>
     * 实现初次进入自动显示刷新动画的效果
     * <p>该方法必须在onMeasure()方法执行后调用才生效</p>
     */
    public void autoRefresh() {
        try {
            // 获取动画CircleView对象并显示
            Field mCircleView = SwipeRefreshLayout.class.getDeclaredField("mCircleView");
            mCircleView.setAccessible(true);
            View progress = (View) mCircleView.get(this);
            progress.setVisibility(VISIBLE);
            // 调用SwipeRefreshLayout#setRefreshing(boolean refreshing, boolean notify)显示加载动画
            Method setRefreshing = SwipeRefreshLayout.class.getDeclaredMethod("setRefreshing", boolean.class, boolean.class);
            setRefreshing.setAccessible(true);
            setRefreshing.invoke(this, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 拦截滑动事件，处理左右滑动冲突(嵌套可滑动控件时)
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = ev.getX();
                startY = ev.getY();
                mIsHorizontalScroll = false;
                break;

            case MotionEvent.ACTION_MOVE:
                // 如果正在横向滑动，则不拦截
                if (mIsHorizontalScroll) {
                    return false;
                }
                float distanceX = Math.abs(ev.getX() - startX);
                float distanceY = Math.abs(ev.getY() - startY);
                if (distanceX > mTouchSlop && distanceX > distanceY) {// X轴位移大于Y轴并且距离超过mTouchSlop
                    mIsHorizontalScroll = true;
                    return false;
                }
                startX = ev.getX();
                startY = ev.getY();
                break;

            case MotionEvent.ACTION_UP:
                mIsHorizontalScroll = false;
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

}
