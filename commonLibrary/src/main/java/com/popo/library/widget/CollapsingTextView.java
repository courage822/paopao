package com.popo.library.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.popo.library.R;
import com.popo.library.util.DeviceUtil;

/**
 * 可以展开/收起的TextView
 * Created by zhangdroid on 2016/7/7.
 */
public class CollapsingTextView extends LinearLayout {
    // 显示内容的TextView
    private TextView mTvContent;
    // 展开/收起的TextView
    private TextView mTvOperate;
    // 最多显示的行数，超过后就可以展开
    private int mMaxLines;
    // 内容文字颜色
    private int mTextColor;
    // 内容文字字号
    private float mTextSize;
    // 按钮文字颜色
    private int mBtnColor;
    // 按钮文字字号
    private float mBtnSize;
    // 折叠按钮文字
    private String mCollapsingText;
    // 展开按钮文字
    private String mExpandableText;
    // 默认显示的最大行数
    private static final int DEFAULT_MAX_LINES = 5;
    private STATE mCurrentState = STATE.IDLE;

    /**
     * 状态常量
     */
    private enum STATE {
        /**
         * 默认状态
         */
        IDLE,
        /**
         * 展开状态
         */
        UNFOLDED,
        /**
         * 收起状态
         */
        FOLDED
    }

    public CollapsingTextView(Context context) {
        this(context, null);
    }

    public CollapsingTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CollapsingTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CollapsingTextView, defStyleAttr, 0);
        mMaxLines = typedArray.getInteger(R.styleable.CollapsingTextView_maxLines, DEFAULT_MAX_LINES);
        mTextColor = typedArray.getColor(R.styleable.CollapsingTextView_contentTextColor, Color.BLACK);
        mTextSize = typedArray.getDimension(R.styleable.CollapsingTextView_contentTextSize, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                16, getResources().getDisplayMetrics()));
        mBtnColor = typedArray.getColor(R.styleable.CollapsingTextView_btnTextColor, Color.BLACK);
        mBtnSize = typedArray.getDimension(R.styleable.CollapsingTextView_btnTextSize, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                18, getResources().getDisplayMetrics()));
        mCollapsingText = typedArray.getString(R.styleable.CollapsingTextView_collapsingText);
        mExpandableText = typedArray.getString(R.styleable.CollapsingTextView_expandableText);
        typedArray.recycle();

        if (TextUtils.isEmpty(mCollapsingText)) {
            mCollapsingText = getResources().getString(R.string.folded);
        }
        if (TextUtils.isEmpty(mExpandableText)) {
            mExpandableText = getResources().getString(R.string.unfolded);
        }

        setOrientation(LinearLayout.VERTICAL);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        attachChildViews(context);
    }

    private void attachChildViews(Context context) {
        // 设置显示内容的TextView
        mTvContent = new TextView(context);
        mTvContent.setTextColor(mTextColor);
        mTvContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
        mTvContent.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        mTvContent.setEllipsize(TextUtils.TruncateAt.END);
        addView(mTvContent, 0, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        // 设置展开/收起的TextView
        mTvOperate = new TextView(context);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.RIGHT;
        layoutParams.setMargins(0, DeviceUtil.dip2px(context, 8), 0, 0);
        mTvOperate.setTextSize(TypedValue.COMPLEX_UNIT_PX, mBtnSize);
        mTvOperate.setTextColor(mBtnColor);
        mTvOperate.setGravity(Gravity.CENTER);
        mTvOperate.setOnClickListener(mOnClickListener);
        addView(mTvOperate, 1, layoutParams);
    }

    private OnClickListener mOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mCurrentState == STATE.FOLDED) {
                mCurrentState = STATE.UNFOLDED;
            } else if (mCurrentState == STATE.UNFOLDED) {
                mCurrentState = STATE.FOLDED;
            }
            changeState();
        }
    };

    private void changeState() {
        switch (mCurrentState) {
            case UNFOLDED:// 展开
                mTvOperate.setText(mCollapsingText);
                mTvContent.setHeight(mTvContent.getLineHeight() * mTvContent.getLineCount());
                break;

            case FOLDED: // 收起
                mTvOperate.setText(mExpandableText);
                mTvContent.setHeight(mTvContent.getLineHeight() * mMaxLines);
                break;
        }
    }

    public void setText(String text) {
        mTvContent.setText(text);
        mTvContent.post(new Runnable() {
            @Override
            public void run() {
                if (mTvContent.getLineCount() <= mMaxLines) {// 不超过最大行数，不显示收起/展开按钮
                    mTvOperate.setVisibility(GONE);
                    mCurrentState = STATE.IDLE;
                } else {
                    mTvOperate.setVisibility(VISIBLE);
                    // 默认为收起状态
                    mCurrentState = STATE.FOLDED;
                    changeState();
                }
            }
        });
    }

    public void setTextColor(int color) {
        mTvContent.setTextColor(color);
    }

    public void setTextSize(int size) {
        mTvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
    }

    public void setMaxLines(int lines) {
        this.mMaxLines = lines;
    }

    public TextView getContentTextView() {
        return mTvContent;
    }

}
