package com.popo.library.widget.tab;

/**
 * Badge decoration for {@link TabView}
 * Created by zhangdroid on 2017/5/23.
 */
public class ItemBadge {
    public String mBadgeText;
    public int mBadgeColor;
    public int mBadgeSize;
    public int mBadgeGravity;
    private OnBadgeChangedListener mOnBadgeChangedListener;

    public ItemBadge(int badgeColor, int badgeSize, int badgeGravity) {
        this.mBadgeColor = badgeColor;
        this.mBadgeSize = badgeSize;
        this.mBadgeGravity = badgeGravity;
    }

    public ItemBadge setText(String text) {
        this.mBadgeText = text;
        return this;
    }

    public ItemBadge setTextColor(int color) {
        this.mBadgeColor = color;
        return this;
    }

    public ItemBadge setTextSize(int size) {
        this.mBadgeSize = size;
        return this;
    }

    public ItemBadge setGravity(int gravity) {
        this.mBadgeGravity = gravity;
        return this;
    }

    public ItemBadge show() {
        if (null != mOnBadgeChangedListener) {
            mOnBadgeChangedListener.onBadgeShown(this);
        }
        return this;
    }

    public ItemBadge hide() {
        if (null != mOnBadgeChangedListener) {
            mOnBadgeChangedListener.onBadgeHidden(this);
        }
        return this;
    }

    public void setOnBadgeChangedListener(OnBadgeChangedListener onBadgeChangedListener) {
        if (null != onBadgeChangedListener) {
            this.mOnBadgeChangedListener = onBadgeChangedListener;
        }
    }

}
