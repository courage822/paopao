package com.popo.library.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.popo.library.R;

/**
 * 聊天图片气泡
 * Created by zhangdroid on 2017/6/28.
 */
public class ChatImageView extends AppCompatImageView {
    private static final int ARROW_ORIENTATION_LEFT = 0;
    private static final int ARROW_ORIENTATION_RIGHT = 1;
    private static final int DEFAULT_CORNER_RADUIS = 30;// 默认圆角半径
    private static final int DEFAULT_ARROW_WIDTH = 40;// 箭头的默认宽度
    private static final int DEFAULT_ARROW_HEIGHT = 50;// 箭头的默认高度
    private static final int DEFAULT_ARROW_Y_OFFSET = 120;// 默认箭头在y方向上的位移

    /**
     * 图片圆角半径
     */
    private int mCorderRaduis;
    /**
     * 箭头方向
     */
    private int mArrowOrientation;
    /**
     * 箭头宽度
     */
    private int mArrowWidth;
    /**
     * 箭头高度
     */
    private int mArrowHeight;
    /**
     * 箭头在y方向上的位移，相对于图片的顶部
     */
    private int mArrowYOffset;
    /**
     * 要显示的图片Bitmap
     */
    private Bitmap mBitmap;
    private Paint mPaint;

    public ChatImageView(Context context) {
        this(context, null);
    }

    public ChatImageView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChatImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedValue = context.obtainStyledAttributes(attrs, R.styleable.ChatImageView, defStyleAttr, 0);
        mCorderRaduis = typedValue.getDimensionPixelSize(R.styleable.ChatImageView_corner_raduis, DEFAULT_CORNER_RADUIS);
        mArrowOrientation = typedValue.getInteger(R.styleable.ChatImageView_arrow_orientation, ARROW_ORIENTATION_LEFT);
        mArrowWidth = typedValue.getDimensionPixelSize(R.styleable.ChatImageView_arrow_width, DEFAULT_ARROW_WIDTH);
        mArrowHeight = typedValue.getDimensionPixelSize(R.styleable.ChatImageView_arrow_height, DEFAULT_ARROW_HEIGHT);
        mArrowYOffset = typedValue.getDimensionPixelSize(R.styleable.ChatImageView_arrow_yOffset, DEFAULT_ARROW_Y_OFFSET);
        typedValue.recycle();

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(Color.GRAY);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
//        super.setImageBitmap(bm);
        setup(bm);
    }

    @Override
    public void setImageResource(@DrawableRes int resId) {
//        super.setImageResource(resId);
        setup(getBitmapFromDrawable(getResources().getDrawable(resId)));
    }

    @Override
    public void setImageDrawable(@Nullable Drawable drawable) {
//        super.setImageDrawable(drawable);
        setup(getBitmapFromDrawable(drawable));
    }

    @Override
    public void setImageURI(@Nullable Uri uri) {
//        super.setImageURI(uri);
        setup(getBitmapFromDrawable(getDrawable()));
    }

    private void setup(Bitmap bitmap) {
        mBitmap = bitmap;
        invalidate();
    }

    /**
     * Drawable转Bitmap
     */
    private Bitmap getBitmapFromDrawable(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        try {
            Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mBitmap == null)
            return;

        Path path = new Path();
        RectF rect = new RectF();
        switch (mArrowOrientation) {
            case ARROW_ORIENTATION_LEFT:// 箭头在左边
                // 圆角矩形位置
                rect.left = mArrowWidth;
                rect.top = 0;
                rect.right = getWidth();
                rect.bottom = getHeight();
                path.addRoundRect(rect, mCorderRaduis, mCorderRaduis, Path.Direction.CCW);
                // 箭头位置
                path.moveTo(mArrowWidth, mArrowYOffset);
                path.lineTo(0, mArrowYOffset + (mArrowHeight / 2));
                path.lineTo(mArrowWidth, mArrowYOffset + mArrowHeight);
                break;

            case ARROW_ORIENTATION_RIGHT:// 箭头在右边
                // 圆角矩形位置
                rect.left = 0;
                rect.top = 0;
                rect.right = getWidth() - mArrowWidth;
                rect.bottom = getHeight();
                path.addRoundRect(rect, mCorderRaduis, mCorderRaduis, Path.Direction.CCW);
                // 箭头位置
                path.moveTo(getWidth() - mArrowWidth, mArrowYOffset);
                path.lineTo(getWidth(), mArrowYOffset + (mArrowHeight / 2));
                path.lineTo(getWidth() - mArrowWidth, mArrowYOffset + mArrowHeight);
                break;
        }
        // 保存canvas
        canvas.save();
        // 裁剪canvas成制定的path
        canvas.clipPath(path);
        // 将bitmap绘制到裁剪后的canvas上
        canvas.drawBitmap(mBitmap, null, new RectF(0, 0, getWidth(), getHeight()), mPaint);
        // 恢复canvas
        canvas.restore();
    }

}
