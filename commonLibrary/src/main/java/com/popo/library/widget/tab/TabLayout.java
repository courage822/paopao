package com.popo.library.widget.tab;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.util.SparseArrayCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.popo.library.R;
import com.popo.library.util.DeviceUtil;

/**
 * A common tab layout in horizontal orientation.
 * Created by zhangdroid on 2017/5/23.
 */
public class TabLayout extends LinearLayout {
    /**
     * 整个tab的背景色
     */
    private int mBgColor;
    /**
     * tab默认文字颜色
     */
    private int mTabTextColor;
    /**
     * tab选中时文字颜色
     */
    private int mTabSelectedTextColor;
    /**
     * tab视频秀选中时文字颜色
     */
    private int mTabShowSelectedTextColor;
    /**
     * tab文字大小
     */
    private float mTabTextSize;
    /**
     * tab文字和图标间距
     */
    private int mTabPadding;
    /**
     * 当前选中的tab position
     */
    private int mTabSelectedPos;

    // 保存所有的TabView
    private SparseArrayCompat<TabView> mTabViewArray;
    private OnTabSelectedListener mOnTabSelectedListener;

    public TabLayout(Context context) {
        this(context, null);
    }

    public TabLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TabLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TabLayout, defStyleAttr, 0);
        mBgColor = typedArray.getColor(R.styleable.TabLayout_backgroundColor, Color.WHITE);
        mTabTextColor = typedArray.getColor(R.styleable.TabLayout_tabTextColor, Color.LTGRAY);
        mTabSelectedTextColor = typedArray.getColor(R.styleable.TabLayout_tabSelectTextColor, Color.BLACK);
        mTabShowSelectedTextColor = typedArray.getColor(R.styleable.TabLayout_tabShowSelectColor, Color.parseColor("#fd698b"));
        mTabTextSize = typedArray.getDimension(R.styleable.TabLayout_tabTextSize, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                12, getResources().getDisplayMetrics()));
        mTabPadding = (int) typedArray.getDimension(R.styleable.TabLayout_tabPadding, 0);
        mTabSelectedPos = typedArray.getInteger(R.styleable.TabLayout_tabDefaultPos, 0);
        typedArray.recycle();

        // 设置tab排列方向为水平
        setOrientation(HORIZONTAL);
        // 设置背景颜色
        if (mBgColor != 0) {
            setBackgroundColor(mBgColor);
        }
    }

    public TabLayout addTabView(TabView tabView) {
        if (null == mTabViewArray) {
            mTabViewArray = new SparseArrayCompat<>();
        }
        if (null != tabView) {
            mTabViewArray.put(mTabViewArray.size(), tabView);
        }
        return this;
    }

    public TabLayout setFirstSelectedPosition(int position) {
        this.mTabSelectedPos = position;
        return this;
    }

    public void initialize() {
        if (getChildCount() > 0) {
            // 清空之前所有的view
            removeAllViews();
        }
        int size = mTabViewArray.size();
        for (int i = 0; i < size; i++) {
            TabView tabView = mTabViewArray.get(i);
            if (null != tabView) {
                final FrameLayout frameLayout = new FrameLayout(getContext());
                // 设置tab文字和图标
                TextView tvTab = new TextView(getContext());
                if (i == 2) {
                    tvTab.setTextColor(i == mTabSelectedPos ? mTabShowSelectedTextColor : mTabTextColor);
                } else {
                    tvTab.setTextColor(i == mTabSelectedPos ? mTabSelectedTextColor : mTabTextColor);
                }
                tvTab.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTabTextSize);
                tvTab.setGravity(Gravity.CENTER);
                String label = tabView.mTabLabel;
                tvTab.setText(TextUtils.isEmpty(label) ? "" : label);
                // 默认图标放在文字上方
                Drawable topDrawable = getResources().getDrawable(i == mTabSelectedPos ? tabView.mSelectedImgRes : tabView.mDefaultImgRes);
                topDrawable.setBounds(0, 0, topDrawable.getMinimumWidth(), topDrawable.getMinimumHeight());
                tvTab.setCompoundDrawables(null, topDrawable, null, null);
                tvTab.setCompoundDrawablePadding(mTabPadding);
                frameLayout.addView(tvTab);
                // 设置tab角标
                ItemBadge itemBadge = tabView.mItemBadge;
                if (null != itemBadge) {
                    final TextView tvBadge = new TextView(getContext());
                    tvBadge.setText(itemBadge.mBadgeText);
                    tvBadge.setTextColor(itemBadge.mBadgeColor);
                    tvBadge.setTextSize(itemBadge.mBadgeSize);
                    tvBadge.setGravity(Gravity.CENTER);
                    tvBadge.setBackgroundResource(R.drawable.shape_circle_red);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.gravity = itemBadge.mBadgeGravity;
                    layoutParams.setMargins(0, 0, DeviceUtil.dip2px(getContext(), 20), 0);
                    frameLayout.addView(tvBadge, layoutParams);
                    itemBadge.setOnBadgeChangedListener(new OnBadgeChangedListener() {
                        @Override
                        public void onBadgeShown(ItemBadge itemBadge) {
                            tvBadge.setVisibility(View.GONE);
                            tvBadge.setText(itemBadge.mBadgeText);
                            tvBadge.setTextColor(itemBadge.mBadgeColor);
                            tvBadge.setTextSize(itemBadge.mBadgeSize);
                             tvBadge.setVisibility(VISIBLE);
                            requestLayout();
                        }

                        @Override
                        public void onBadgeHidden(ItemBadge itemBadge) {
                            tvBadge.setVisibility(GONE);
                            requestLayout();
                        }
                    });
                }
                // 设置tag
                frameLayout.setTag(i);
                // 设置tab点击事件
                frameLayout.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int currPosition = (int) frameLayout.getTag();
                        if (currPosition != mTabSelectedPos) {
                            mTabSelectedPos = currPosition;
                            initialize();
                            if (null != mOnTabSelectedListener) {
                                mOnTabSelectedListener.onTabSelected(mTabSelectedPos);
                            }
                        }
                    }
                });
                // attch this tab view to tablayout
                addView(frameLayout, new LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f));
            }
        }
    }

    public void setOnTabSelectedListener(OnTabSelectedListener listener) {
        if (null != listener) {
            this.mOnTabSelectedListener = listener;
        }
    }

}
