package com.popo.library.widget;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.popo.library.util.DeviceUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;


/**
 * 可以下拉刷新和加载更多的RecyclerView
 * Created by zhangdroid on 2016/8/5.
 */
public class RefreshRecyclerView extends SwipeRefreshLayout {
    private static final int ITEM_TYPE_FOOTER = 80000;
    private boolean mMeasure = false;
    // 向下滑动的距离阙值，超过后则认为是下拉
    private int mTouchSlop;
    // 是否正在横向滑动
    private boolean mIsHorizontalScroll;
    // 起始坐标
    private float startX = 0;
    private float startY = 0;
    private RecyclerView mRecyclerView;
    /**
     * 处理RecycleView设置的Adapter逻辑
     */
    private AdapterWrapper mAdapterWrapper;
    /**
     * 加载更多footer view
     */
    private View mFooterView;
    /**
     * 标记是否正在加载数据
     */
    private boolean mIsLoadingMore = false;
    /**
     * 加载更多监听
     */
    private OnLoadingMoreListener mOnLoadingMoreListener;
    private StopRefishListener listener;
    private int maxHeight;
    private int originH;
    private Boolean getIsIntercept=true;

    public void setOnLoadingMoreListener(OnLoadingMoreListener listener) {
        if (listener != null) {
            mOnLoadingMoreListener = listener;
        }
    }

    public void setOnLoadingMoreListener(OnLoadingMoreListener listener, Context context, Boolean isIntercept) {
        if (listener!= null) {
            if (isIntercept) {
                init(context);
            }

            mOnLoadingMoreListener = listener;
        }
    }

    public interface OnLoadingMoreListener {
        /**
         * 加载更多监听
         */
        void onLoadMore();
    }

    public RefreshRecyclerView(Context context) {
        this(context, null);
    }

    public RefreshRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // 初始化
        init(context);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (!mMeasure) {
            mMeasure = true;
            // only run once time
            autoRefresh();
        }
    }

    /**
     * 利用反射调用SwipeRefreshLayout的私有方法
     * <p>{@link SwipeRefreshLayout#setRefreshing(boolean refreshing, boolean notify)}</p>
     * 实现初次进入自动显示刷新动画的效果
     * <p>该方法必须在onMeasure()方法执行后调用才生效</p>
     */
    public void autoRefresh() {
        try {
            // 获取动画CircleView对象并显示
            Field mCircleView = SwipeRefreshLayout.class.getDeclaredField("mCircleView");
            mCircleView.setAccessible(true);
            View progress = (View) mCircleView.get(this);
            progress.setVisibility(VISIBLE);
            // 调用SwipeRefreshLayout#setRefreshing(boolean refreshing, boolean notify)显示加载动画
            Method setRefreshing = SwipeRefreshLayout.class.getDeclaredMethod("setRefreshing", boolean.class, boolean.class);
            setRefreshing.setAccessible(true);
            setRefreshing.invoke(this, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 拦截滑动事件，处理左右滑动冲突(嵌套可以水平方向滑动控件时)
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = ev.getX();
                startY = ev.getY();
                mIsHorizontalScroll = false;
                break;

            case MotionEvent.ACTION_MOVE:
                // 如果正在横向滑动，则不拦截
                if (mIsHorizontalScroll) {
                    return false;
                }
                float distanceX = Math.abs(ev.getX() - startX);
                float distanceY = Math.abs(ev.getY() - startY);
                if (distanceX > mTouchSlop && distanceX > distanceY) {// X轴位移大于Y轴并且距离超过mTouchSlop
                    mIsHorizontalScroll = true;
                    return false;
                }
                startX = ev.getX();
                startY = ev.getY();
                break;

            case MotionEvent.ACTION_UP:
                mIsHorizontalScroll = false;
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }


    public void init(Context context) {
        // 将RecyclerView添加到SwipeRefreshLayout
        mRecyclerView = new RecyclerView(context);
        addView(mRecyclerView);
        // 创建footer view
        createFooterView(context);
        // 设置滚动监听
        setScrollListener();
    }

    private void createFooterView(final Context context) {
        // 设置加载中图片
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT));
        linearLayout.setGravity(Gravity.CENTER);
        int padding = DeviceUtil.dip2px(context, 15);
        linearLayout.setPadding(0, padding, 0, padding);
//
        ImageView ivHert = new ImageView(context);
//        ivHert.setImageResource(R.drawable.heart);
//        ivHert.setLayoutParams(new LayoutParams(100,
//                100));
        linearLayout.addView(ivHert);
//
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.1f, 1.5f, 0.1f, 1.5f);
        scaleAnimation.setDuration(500);
        scaleAnimation.setRepeatCount(-1);
        scaleAnimation.setRepeatMode(Animation.REVERSE);
//
////        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(context, R.anim.anim_show_loading);
//        ivHert.clearAnimation();
        ivHert.startAnimation(scaleAnimation);
        mFooterView = linearLayout;

//        TextView tvLoading = new TextView(context);
//        tvLoading.setPadding(padding, 0, 0, 0);
//        tvLoading.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
//        tvLoading.setTextColor(Color.GRAY);
//        tvLoading.setText("Loading...");
//         View contentView = LayoutInflater.from(context).inflate(R.layout.footer_refresh_reccler, null);
//        ImageView ivNeck = (ImageView) contentView.findViewById(R.id.iv_heart);
//        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(context, R.anim.anim_show_loading);
//        ivNeck.clearAnimation();
//        ivNeck.startAnimation(hyperspaceJumpAnimation);
//        LinearLayout llRember = (LinearLayout) contentView.findViewById(R.id.ll_rember);
//        LinearLayout llLoading = (LinearLayout) contentView.findViewById(R.id.ll_loading);
//        llRember.setVisibility(GONE);
//        linearLayout.addView(contentView);

    }

    /**
     * 设置滚动监听，判断RecyclerView是否滚动到底部的逻辑
     */
    private void setScrollListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            // RecyclerView第一个完全可见项的索引
            int firstCompletelyVisibleItemPosition = 0;
            // RecyclerView最后可见项的索引
            int lastVisibleItemPosition = 0;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                // 停止滚动并且已经滚动到底部
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (newState == RecyclerView.SCROLL_STATE_IDLE && layoutManager.getChildCount() > 0 && lastVisibleItemPosition >= layoutManager.getItemCount() - 1) {
                    if (firstCompletelyVisibleItemPosition == 0) {
                        // 滚到到最顶部时可以下拉刷新
                        RefreshRecyclerView.this.setRefreshing(true);
                    } else {
                        RefreshRecyclerView.this.setRefreshing(false);
                        // 加载更多
                        if (!isRefreshing() && !mIsLoadingMore && mOnLoadingMoreListener != null) {
                            mIsLoadingMore = true;
                             showFooterView();
                            mOnLoadingMoreListener.onLoadMore();
                        }
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // 计算滚动后位置坐标
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager instanceof GridLayoutManager) {
                    GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
                    firstCompletelyVisibleItemPosition = gridLayoutManager.findFirstCompletelyVisibleItemPosition();
                    lastVisibleItemPosition = gridLayoutManager.findLastVisibleItemPosition();
                } else if (layoutManager instanceof LinearLayoutManager) {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
                    firstCompletelyVisibleItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                    lastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();
                } else if (layoutManager instanceof StaggeredGridLayoutManager) {
                    StaggeredGridLayoutManager staggeredGridLayoutManager = (StaggeredGridLayoutManager) layoutManager;
                    int[] firstInto = staggeredGridLayoutManager.findFirstCompletelyVisibleItemPositions(new int[staggeredGridLayoutManager.getSpanCount()]);
                    int[] lastInto = staggeredGridLayoutManager.findLastVisibleItemPositions(new int[staggeredGridLayoutManager.getSpanCount()]);
                    // 排序并取最大值
                    Arrays.sort(firstInto);
                    Arrays.sort(lastInto);
                    firstCompletelyVisibleItemPosition = firstInto[0];
                    lastVisibleItemPosition = lastInto[lastInto.length - 1];
                }
            }
        });
    }

    /**
     * 下拉刷新完成，取消动画
     */
    public void refreshCompleted() {
        if (isRefreshing()) {
            setRefreshing(false);
        }
    }

    /**
     * 下拉刷新完成，延时取消动画
     *
     * @param delaySecs 延时时间（秒）
     */
    public void refreshCompleted(int delaySecs) {
        postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshCompleted();
            }
        }, delaySecs * 1000);
    }

    /**
     * 加载更多完成，取消动画并隐藏FooterView
     */
    public void loadMoreCompleted() {
        mIsLoadingMore = false;
        hideFooterView();
    }

    /**
     * 加载更多完成，延时取消动画并隐藏FooterView
     *
     * @param delaySecs 延时时间（秒）
     */
    public void loadMoreCompleted(int delaySecs) {
        postDelayed(new Runnable() {
            @Override
            public void run() {
                loadMoreCompleted();
            }
        }, delaySecs * 1000);
    }

    public void showFooterView() {
        if (mFooterView != null && !mFooterView.isShown()) {
            mFooterView.setVisibility(View.VISIBLE);
        }
    }

    public void hideFooterView() {
        if (mFooterView != null && mFooterView.isShown()) {
            mFooterView.setVisibility(View.GONE);
        }
    }

    //******************************************* 设置RecyclerView******************************************//

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return mRecyclerView.getLayoutManager();
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        if (mRecyclerView != null) {
            mRecyclerView.setLayoutManager(layoutManager);
        }
    }

    public void addItemDecoration(RecyclerView.ItemDecoration itemDecoration) {
        if (itemDecoration == null) {
            return;
        }
        if (mRecyclerView != null) {
            mRecyclerView.addItemDecoration(itemDecoration);
        }
    }

    public void setItemAnimator(RecyclerView.ItemAnimator itemAnimator) {
        if (itemAnimator == null) {
            return;
        }
        if (mRecyclerView != null) {
            mRecyclerView.setItemAnimator(itemAnimator);
        }
    }

    /**
     * 设置拖拽和测滑监听
     *
     * @param listener {@link ItemTouchHelperCallback.ItemTouchHelperListener}
     */
    public void setItemTouchHelperListener(ItemTouchHelperCallback.ItemTouchHelperListener listener) {
        if (listener != null && mRecyclerView != null) {
            ItemTouchHelperCallback itemTouchHelperCallback = new ItemTouchHelperCallback(true, listener);
            itemTouchHelperCallback.setIsLongPressDragEnabled(false);
            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView);
        }
    }

    /**
     * 设置加载更多view，需要在{@link RefreshRecyclerView#setAdapter(RecyclerView.Adapter)}之前调用
     *
     * @param loadingView 显示加载动画的view
     */
    public void setLoadingView(View loadingView) {
        if (loadingView != null) {
            mFooterView = loadingView;
            mFooterView.setVisibility(GONE);
        }
    }

    /**
     * 设置RecyclerView的Adapter
     *
     * @param adapter the adapter of RecyclerView
     */
    public void setAdapter(RecyclerView.Adapter adapter) {

        mAdapterWrapper = new AdapterWrapper(adapter);
        mRecyclerView.setAdapter(mAdapterWrapper);
        adapter.registerAdapterDataObserver(mDataObserver);
        mDataObserver.onChanged();
    }

    private RecyclerView.AdapterDataObserver mDataObserver = new RecyclerView.AdapterDataObserver() {

        @Override
        public void onChanged() {
            if (mAdapterWrapper != null) {
                mAdapterWrapper.notifyDataSetChanged();
            }
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            if (mAdapterWrapper != null) {
                mAdapterWrapper.notifyItemRangeChanged(positionStart, itemCount);
            } else {
                super.onItemRangeChanged(positionStart, itemCount);
            }
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
            if (mAdapterWrapper != null) {
                mAdapterWrapper.notifyItemRangeChanged(positionStart, itemCount, payload);
            } else {
                super.onItemRangeChanged(positionStart, itemCount, payload);
            }
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            if (mAdapterWrapper != null) {
                mAdapterWrapper.notifyItemRangeInserted(positionStart, itemCount);
            } else {
                super.onItemRangeInserted(positionStart, itemCount);
            }
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            if (mAdapterWrapper != null) {
                mAdapterWrapper.notifyItemRangeRemoved(positionStart, itemCount);
            } else {
                super.onItemRangeRemoved(positionStart, itemCount);
            }
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            if (mAdapterWrapper != null) {
                mAdapterWrapper.notifyItemMoved(fromPosition, toPosition);
            } else {
                super.onItemRangeMoved(fromPosition, toPosition, itemCount);
            }
        }
    };

    public class AdapterWrapper extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        /**
         * RecyclerView真正的Adapter
         */
        private RecyclerView.Adapter mAdapter;

        public AdapterWrapper(RecyclerView.Adapter adapter) {
            this.mAdapter = adapter;
        }

        /**
         * Whether this item is the footer view or not
         *
         * @param position current item position
         * @return
         */
        private boolean isFooterView(int position) {
            return mAdapter != null && (position == getItemCount() - 1);
        }

        /**
         * 计算真实Adapter中的索引
         *
         * @param position current item position
         * @return
         */
        private int getRealPosition(int position) {
            return position;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == ITEM_TYPE_FOOTER) {// footer view
                return new FooterViewHolder(mFooterView);
            }
            return mAdapter.onCreateViewHolder(parent, viewType);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (isFooterView(position)) {
                return;
            }
            if (mAdapter != null) {
                if (mAdapter.getItemCount() == 0) {
                    return;
                }
                mAdapter.onBindViewHolder(holder, getRealPosition(position));
            }
        }

        @Override
        public int getItemCount() {
            return mAdapter == null ? 1 : mAdapter.getItemCount() + 1;
        }

        @Override
        public int getItemViewType(int position) {
            if (mAdapter != null) {
                // is footer
                if (isFooterView(position)) {
                    return ITEM_TYPE_FOOTER;
                } else {
                    return mAdapter.getItemViewType(getRealPosition(position));
                }
            }
            return super.getItemViewType(position);
        }

        @Override
        public long getItemId(int position) {
            return mAdapter == null ? super.getItemId(position) : mAdapter.getItemId(position);
        }

        @Override
        public void onViewRecycled(RecyclerView.ViewHolder holder) {
            if (mAdapter == null) {
                super.onViewRecycled(holder);
            } else {
                mAdapter.onViewRecycled(holder);
            }
        }

        @Override
        public boolean onFailedToRecycleView(RecyclerView.ViewHolder holder) {
            return mAdapter == null ? super.onFailedToRecycleView(holder) : mAdapter.onFailedToRecycleView(holder);
        }

        @Override
        public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
            if (mAdapter == null) {
                super.onViewAttachedToWindow(holder);
            } else {
                // 处理StaggeredGridLayoutManager
                LayoutParams layoutParams = holder.itemView.getLayoutParams();
                if (layoutParams != null && layoutParams instanceof StaggeredGridLayoutManager.LayoutParams) {
                    if (isFooterView(holder.getLayoutPosition())) {
                        // 设置footer view充满整行
                        StaggeredGridLayoutManager.LayoutParams lp = (StaggeredGridLayoutManager.LayoutParams) layoutParams;
                        lp.setFullSpan(true);
                    }
                }
                mAdapter.onViewAttachedToWindow(holder);
            }
        }

        @Override
        public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
            if (mAdapter == null) {
                super.onViewDetachedFromWindow(holder);
            } else {
                mAdapter.onViewDetachedFromWindow(holder);
            }
        }

        @Override
        public void registerAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
            if (mAdapter == null) {
                super.registerAdapterDataObserver(observer);
            } else {
                mAdapter.registerAdapterDataObserver(observer);
            }
        }

        @Override
        public void unregisterAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
            if (mAdapter == null) {
                super.unregisterAdapterDataObserver(observer);
            } else {
                mAdapter.unregisterAdapterDataObserver(observer);
            }
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            if (mAdapter == null) {
                super.onAttachedToRecyclerView(recyclerView);
            } else {
                // 处理StaggeredGridLayoutManager
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager instanceof GridLayoutManager) {
                    final GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
                    // 设置某个item所占据的列数或行数
                    gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {

                        @Override
                        public int getSpanSize(int position) {
                            // 设置footer view充满整行
                            return isFooterView(position) ? gridLayoutManager.getSpanCount() : 1;
                        }
                    });
                }
                mAdapter.onAttachedToRecyclerView(recyclerView);
            }
        }

        @Override
        public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
            if (mAdapter == null) {
                super.onDetachedFromRecyclerView(recyclerView);
            } else {
                mAdapter.onDetachedFromRecyclerView(recyclerView);
            }
        }
    }

    /**
     * footer view holder
     */
    public class FooterViewHolder extends RecyclerView.ViewHolder {

        public FooterViewHolder(View itemView) {
            super(itemView);
            LayoutParams layoutParams = itemView.getLayoutParams();
            if (layoutParams != null && layoutParams instanceof StaggeredGridLayoutManager.LayoutParams) {
                StaggeredGridLayoutManager.LayoutParams lp = (StaggeredGridLayoutManager.LayoutParams) layoutParams;
                lp.setFullSpan(true);
            }
        }
    }

    /**
     * 自定义一个停止或者开始刷新界面的接口，根据手指的触摸状态判断是否自动刷新
     **/
    public interface StopRefishListener {
        void isTouch(boolean istouch);
    }

    public void setIsTouchListener(StopRefishListener listener) {
        this.listener = listener;
    }

    private boolean isTouch;
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                isTouch = true;
                if (listener != null) {
                    listener.isTouch(isTouch);
                }
                break;
            case MotionEvent.ACTION_MOVE:
                isTouch = true;
                if (listener != null) {
                    listener.isTouch(isTouch);
                }
                break;
            case MotionEvent.ACTION_UP:
                isTouch = false;
                if (listener != null) {
                    listener.isTouch(isTouch);
                }
        }
        return super.onInterceptTouchEvent(ev);
    }

}
