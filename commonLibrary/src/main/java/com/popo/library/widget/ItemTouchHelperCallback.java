package com.popo.library.widget;

import android.graphics.Canvas;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

/**
 * 处理RecyclerView测滑和拖拽事件
 * Created by zhangdroid on 2017/4/27.
 */
public class ItemTouchHelperCallback extends ItemTouchHelper.Callback {
    // 是否支持长按拖拽，默认为true
    private boolean mIsSwipeEnabled = true;
    // 是否支持侧滑，默认为true
    private boolean mIsLongPressDragEnabled = true;
    // 是否支持向左滑动，默认为true，false表示向右滑动
    private boolean mIsLeftSwipeEnabled = true;
    // 拖拽和侧滑监听
    private ItemTouchHelperListener mItemTouchHelperListener;

    public ItemTouchHelperCallback(boolean isLeftSwipeEnable, ItemTouchHelperListener itemTouchHelperListener) {
        this.mIsLeftSwipeEnabled = isLeftSwipeEnable;
        this.mItemTouchHelperListener = itemTouchHelperListener;
    }

    public void setIsLongPressDragEnabled(boolean isLongPressDragEnabled) {
        this.mIsLongPressDragEnabled = isLongPressDragEnabled;
    }

    public void setIsSwipeEnabled(boolean isSwipeEnabled) {
        this.mIsSwipeEnabled = isSwipeEnabled;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return mIsLongPressDragEnabled;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return mIsSwipeEnabled;
    }

    /**
     * 设置拖拽和滑动标志
     *
     * @param recyclerView
     * @param viewHolder
     * @return 被允许拖拽/滑动的方向
     */
    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags;
        int swipeFlags;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
            swipeFlags = mIsLeftSwipeEnabled ? ItemTouchHelper.START : ItemTouchHelper.END;
        } else {
            dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
            swipeFlags = 0;
        }
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    /**
     * 拖拽
     *
     * @param recyclerView
     * @param viewHolder
     * @param target
     * @return
     */
    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        if (mItemTouchHelperListener != null) {
            mItemTouchHelperListener.onItemDrag(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        }
        return true;
    }

    /**
     * 滑动
     *
     * @param viewHolder
     * @param direction
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        if (mItemTouchHelperListener != null) {
            mItemTouchHelperListener.onItemSwipe(viewHolder.getAdapterPosition());
        }
    }

    /**
     * 实现拖拽和滑动时自定义动画效果
     *
     * @param c
     * @param recyclerView
     * @param viewHolder
     * @param dX
     * @param dY
     * @param actionState
     * @param isCurrentlyActive
     */
    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if (mItemTouchHelperListener == null || !mItemTouchHelperListener.onItemAnim(c, recyclerView, viewHolder, dX, dY, actionState)) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    }

    /**
     * 拖拽和滑动结束时恢复之前设置的动画效果
     *
     * @param recyclerView
     * @param viewHolder
     */
    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);
        if (mItemTouchHelperListener != null) {
            mItemTouchHelperListener.onItemClear(recyclerView, viewHolder);
        }
    }

    /**
     * Listener for RecyclerView item drag & drop and swipe to dismiss.
     */
    public interface ItemTouchHelperListener {

        /**
         * 拖拽
         *
         * @param fromPosition
         * @param toPosition
         */
        void onItemDrag(int fromPosition, int toPosition);

        /**
         * 侧滑
         *
         * @param position
         */
        void onItemSwipe(int position);

        /**
         * 实现Item自定义动画
         *
         * @param c
         * @param recyclerView
         * @param viewHolder
         * @param dX
         * @param dY
         * @param actionState
         * @return 是否消费
         */
        boolean onItemAnim(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState);

        /**
         * 去掉Item自定义动画
         *
         * @param recyclerView
         * @param viewHolder
         * @return 是否消费
         */
        void onItemClear(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder);
    }

}
