package com.popo.library.permission;

/**
 * 请求权限结果回调
 * Created by zhangdroid on 2017/5/19.
 */
public interface PermissionCallback {
    /**
     * 权限请求被允许
     *
     * @param permisson
     * @param position
     */
    void onGuaranteed(String permisson, int position);

    /**
     * 权限请求被拒绝
     *
     * @param permisson
     * @param position
     */
    void onDenied(String permisson, int position);

    /**
     * 权限请求结束
     */
    void onFinished();

    /**
     * 权限多次请求被关闭
     */
    void onClosed();

}
