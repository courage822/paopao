package com.popo.library.permission;

import android.content.Context;

import com.popo.library.R;
import com.popo.library.adapter.CommonRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;

import java.util.List;

/**
 * 权限列表适配器
 * Created by zhangdroid on 2017/5/19.
 */
public class PermissionAdapter extends CommonRecyclerViewAdapter<PermissonItem> {

    public PermissionAdapter(Context context, int layoutResId, List<PermissonItem> dataList) {
        super(context, layoutResId, dataList);
    }

    @Override
    public void convert(PermissonItem permissonItem, int position, RecyclerViewHolder holder) {
        if (null != permissonItem) {
             holder.setText(R.id.permission_name, permissonItem.permissionName);
           // holder.setImageResource(R.id.permission_icon, permissonItem.permissionIconRes);
        }
    }

}
