package com.popo.library.permission;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import com.popo.library.util.LaunchHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 6.0及以上系统权限管理类
 * Created by zhangdroid on 2017/5/19.
 */
public class PermissionManager {
    private static volatile PermissionManager sPermissionManager;
    private Context mContext;
    private String mTitle;
    private String mDescription;
    private List<PermissonItem> mPermissionList;

    private PermissionManager(Context context) {
        this.mContext = context;
    }

    public static PermissionManager getInstance(Context context) {
        if (null == sPermissionManager) {
            synchronized (PermissionManager.class) {
                if (null == sPermissionManager) {
                    sPermissionManager = new PermissionManager(context.getApplicationContext());
                }
            }
        }
        return sPermissionManager;
    }

    /**
     * 设置请求权限标题
     *
     * @param title
     * @return
     */
    public PermissionManager title(String title) {
        this.mTitle = title;
        return this;
    }

    /**
     * 设置请求权限说明文字
     *
     * @param desc
     * @return
     */
    public PermissionManager description(String desc) {
        this.mDescription = desc;
        return this;
    }

    /**
     * 添加一组权限
     *
     * @param permissonItems
     * @return
     */
    public PermissionManager permissions(List<PermissonItem> permissonItems) {
        this.mPermissionList = (null == permissonItems) ? new ArrayList<PermissonItem>() : permissonItems;
        return this;
    }

    /**
     * 添加单个权限
     *
     * @param permissonItem
     * @return
     */
    public PermissionManager addPermission(PermissonItem permissonItem) {
        if (null == mPermissionList) {
            mPermissionList = new ArrayList<>();
        }
        if (null != permissonItem) {
            if (mPermissionList.size() > 0) {
                // 过滤重复申请的权限
                boolean isRepeat = false;
                for (PermissonItem item : mPermissionList) {
                    if (item.permission.equals(permissonItem.permission)) {
                        isRepeat = true;
                        break;
                    }
                }
                if (!isRepeat) {
                    mPermissionList.add(permissonItem);
                }
            } else {
                mPermissionList.add(permissonItem);
            }
        } else {
            throw new IllegalArgumentException("You cannot set a null PermissonItem to check.");
        }
        return this;
    }

    /**
     * 检查指定权限是否开启
     *
     * @param context
     * @param permission
     * @return
     */
    public static boolean checkPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * 检查一组权限
     *
     * @param callback 权限请求结果回调
     */
    public void checkMutiPermission(PermissionCallback callback) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {// 6.0以下系统不请求权限
            callback.onFinished();
            return;
        }

        // 检查权限，过滤已允许的权限
        Iterator<PermissonItem> iterator = mPermissionList.listIterator();
        while (iterator.hasNext()) {
            if (checkPermission(mContext, iterator.next().permission))
                iterator.remove();
        }
        if (mPermissionList.size() > 0) {
            showRequestDialog(callback, RequestPermissionActivity.PERMISSION_TYPE_MUTI);
        } else {
            callback.onFinished();
        }
    }

    /**
     * 检查单个权限
     *
     * @param permission
     * @param callback
     */
    public void checkSinglePermission(String permission, PermissionCallback callback) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkPermission(mContext, permission)) {
            callback.onGuaranteed(permission, 0);
            return;
        }
        mPermissionList = null;
        showRequestDialog(callback, RequestPermissionActivity.PERMISSION_TYPE_SINGLE);
    }

    /**
     * 显示请求权限对话框
     */
    private void showRequestDialog(PermissionCallback callback, int type) {
        RequestPermissionActivity.setCallBack(callback);
        PermissionParcelable permissionParcelable = new PermissionParcelable();
        permissionParcelable.type = type;
        permissionParcelable.title = mTitle;
        permissionParcelable.description = mDescription;
        permissionParcelable.permissonList = mPermissionList;
        LaunchHelper.getInstance().launch(mContext, RequestPermissionActivity.class, permissionParcelable);
    }

}
