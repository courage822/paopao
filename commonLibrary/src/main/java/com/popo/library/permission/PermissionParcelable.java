package com.popo.library.permission;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * 显示权限申请对话框时需要传递的参数
 * Created by zhangdroid on 2017/5/19.
 */
public class PermissionParcelable implements Parcelable {
    /** 请求权限类型（单个权限/一组权限）*/
    public int type;
    /** 请求权限时标题 */
    public String title;
    /** 请求权限时说明文字 */
    public String description;
    /** 请求权限列表 */
    public List<PermissonItem> permissonList;

    public PermissionParcelable(int type, String title, String description, List<PermissonItem> permissonList) {
        this.type = type;
        this.title = title;
        this.description = description;
        this.permissonList = permissonList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeTypedList(this.permissonList);
    }

    public PermissionParcelable() {
    }

    protected PermissionParcelable(Parcel in) {
        this.type = in.readInt();
        this.title = in.readString();
        this.description = in.readString();
        this.permissonList = in.createTypedArrayList(PermissonItem.CREATOR);
    }

    public static final Parcelable.Creator<PermissionParcelable> CREATOR = new Parcelable.Creator<PermissionParcelable>() {
        @Override
        public PermissionParcelable createFromParcel(Parcel source) {
            return new PermissionParcelable(source);
        }

        @Override
        public PermissionParcelable[] newArray(int size) {
            return new PermissionParcelable[size];
        }
    };

}
