package com.popo.library.permission;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 需要展示的权限item
 * Created by zhangdroid on 2017/5/19.
 */
public class PermissonItem implements Parcelable {
    // 被请求的系统权限
    public String permission;
    // 该权限的展示名称
    public String permissionName;
    // 该权限的展示图标
    public int permissionIconRes;

    public PermissonItem(String permission, String permissionName, int permissionIconRes) {
        this.permission = permission;
        this.permissionName = permissionName;
        this.permissionIconRes = permissionIconRes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.permission);
        dest.writeString(this.permissionName);
        dest.writeInt(this.permissionIconRes);
    }

    public PermissonItem() {
    }

    protected PermissonItem(Parcel in) {
        this.permission = in.readString();
        this.permissionName = in.readString();
        this.permissionIconRes = in.readInt();
    }

    public static final Parcelable.Creator<PermissonItem> CREATOR = new Parcelable.Creator<PermissonItem>() {
        @Override
        public PermissonItem createFromParcel(Parcel source) {
            return new PermissonItem(source);
        }

        @Override
        public PermissonItem[] newArray(int size) {
            return new PermissonItem[size];
        }
    };

}
