package com.popo.library.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * <p>对话框公用基类</p>
 * <p>注意：子类实现时推荐使用newInstance方式并传入{@link BaseDialogFragment#getDialogBundle(String, String, String, String, boolean)}作为arguments</p>
 * Created by zhangdroid on 2017/5/23.
 */
public abstract class BaseDialogFragment extends DialogFragment {
    // 标题
    private static final String ARGUMENT_DIALOG_TITLE = "dialog_title";
    // 提示信息
    private static final String ARGUMENT_DIALOG_MESSAGE = "dialog_message";
    // 确认按钮文字
    private static final String ARGUMENT_DIALOG_POSITIVE = "dialog_positive";
    // 取消按钮文字
    private static final String ARGUMENT_DIALOG_NEGATIVE = "dialog_negative";
    // 是否可以取消
    private static final String ARGUMENT_DIALOG_CANCELABLE = "dialog_cancelable";

    private Bundle mBundle;

    public Bundle getDataBundle() {
        return mBundle;
    }

    public static Bundle getDialogBundle(String title, String message, String positive, String negative, boolean isCancelable) {
        Bundle bundle = new Bundle();
        bundle.putString(ARGUMENT_DIALOG_TITLE, title);
        bundle.putString(ARGUMENT_DIALOG_MESSAGE, message);
        bundle.putString(ARGUMENT_DIALOG_POSITIVE, positive);
        bundle.putString(ARGUMENT_DIALOG_NEGATIVE, negative);
        bundle.putBoolean(ARGUMENT_DIALOG_CANCELABLE, isCancelable);
        return bundle;
    }

    protected String getDialogTitle() {
        return mBundle == null ? null : mBundle.getString(ARGUMENT_DIALOG_TITLE);
    }

    protected String getDialogMessage() {
        return mBundle == null ? null : mBundle.getString(ARGUMENT_DIALOG_MESSAGE);
    }

    protected String getDialogPositive() {
        return mBundle == null ? null : mBundle.getString(ARGUMENT_DIALOG_POSITIVE);
    }

    protected String getDialogNegative() {
        return mBundle == null ? null : mBundle.getString(ARGUMENT_DIALOG_NEGATIVE);
    }

    protected boolean cancelable() {
        return mBundle == null ? true : mBundle.getBoolean(ARGUMENT_DIALOG_CANCELABLE, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 设置自定义主题
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        // 设置是否可以取消
        setCancelable(cancelable());
        mBundle = getArguments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        if (getLayoutResId() != 0) {
            view = inflater.inflate(getLayoutResId(), container, false);
        } else {
            view = new View(getContext());
        }
        setDialogContentView(view);
        // 设置window透明，防止四个角出现棱角
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return view;
    }

    /**
     * 自定义dialog的布局文件资源id
     */
    protected abstract int getLayoutResId();

    /**
     * 设置dialog的内容
     *
     * @param view the dialog content view
     */
    protected abstract void setDialogContentView(View view);

}
