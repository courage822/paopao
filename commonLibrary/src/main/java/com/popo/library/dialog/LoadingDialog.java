package com.popo.library.dialog;

import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.popo.library.R;

/**
 * A common loading dialog
 * Created by zhangdroid on 2017/5/23.
 */
public class LoadingDialog extends BaseDialogFragment {
    private static LoadingDialog mLoadingDialog;

    private static LoadingDialog newInstance(String message, boolean isCancelabled) {
        LoadingDialog loadingDialog = new LoadingDialog();
        loadingDialog.setArguments(getDialogBundle(null, message, null, null, isCancelabled));
        return loadingDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_loading;
    }

    @Override
    protected void setDialogContentView(View view) {
        String message = getDialogMessage();
        if (!TextUtils.isEmpty(message)) {
            TextView textView = (TextView) view.findViewById(R.id.dialog_loading_msg);
            textView.setText(message);
        }
    }

    public static void show(FragmentManager fragmentManager) {
        show(fragmentManager, null, true);
    }

    public static void showNoCanceled(FragmentManager fragmentManager) {
        show(fragmentManager, null, false);
    }

    public static void show(FragmentManager fragmentManager, String msg, boolean isCancelable) {
        mLoadingDialog = newInstance(msg, isCancelable);
        mLoadingDialog.show(fragmentManager, "loading");
    }

    public static void hide() {
        if (null != mLoadingDialog && mLoadingDialog.isVisible()) {
            mLoadingDialog.dismiss();
        }
    }

}
