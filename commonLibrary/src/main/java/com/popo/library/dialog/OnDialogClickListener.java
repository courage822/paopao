package com.popo.library.dialog;

import android.view.View;

/**
 * Created by zhangdroid on 2017/5/26.
 */
public interface OnDialogClickListener {
    /**
     * Called when the negative button is clicked.
     *
     * @param view
     */
    void onNegativeClick(View view);

    /**
     * Called when the positive button is clicked.
     *
     * @param view
     */
    void onPositiveClick(View view);
}
