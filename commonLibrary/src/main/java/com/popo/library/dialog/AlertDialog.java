package com.popo.library.dialog;

import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.popo.library.R;

/**
 * A common alert dialog
 * Created by zhangdroid on 2017/5/26.
 */
public class AlertDialog extends BaseDialogFragment {
    private static AlertDialog mAlertDialog;
    private OnDialogClickListener mOnDialogClickListener;

    public static AlertDialog newInstance(String title, String message, String positive, String negative,
                                           boolean isCancelable, OnDialogClickListener listener) {
        AlertDialog alertDialog = new AlertDialog();
        alertDialog.setArguments(getDialogBundle(title, message, positive, negative, isCancelable));
        alertDialog.mOnDialogClickListener = listener;
        return alertDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_alert;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tvTitle = (TextView) view.findViewById(R.id.alert_dialog_title);
        TextView tvMessage = (TextView) view.findViewById(R.id.alert_dialog_message);
        Button btnNegative = (Button) view.findViewById(R.id.alert_dialog_negative);
        Button btnPositive = (Button) view.findViewById(R.id.alert_dialog_positive);
        String title = getDialogTitle();
        String message = getDialogMessage();
        String negative = getDialogNegative();
        String positive = getDialogPositive();
        if (!TextUtils.isEmpty(title)) {
            tvTitle.setText(title);
        }
        if (!TextUtils.isEmpty(message)) {
            tvMessage.setText(message);
        }
        if (!TextUtils.isEmpty(negative)) {
            btnNegative.setText(negative);
        }
        if (!TextUtils.isEmpty(positive)) {
            btnPositive.setText(positive);
        }
        if (null != mOnDialogClickListener) {
            btnNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    mOnDialogClickListener.onNegativeClick(v);
                }
            });
            btnPositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    mOnDialogClickListener.onPositiveClick(v);
                }
            });
        }
    }

    public static void show(FragmentManager fragmentManager, String title, String message, String positive, String negative,
                            OnDialogClickListener listener) {
        show(fragmentManager, title, message, positive, negative, true, listener);
    }

    public static void showNoCanceled(FragmentManager fragmentManager, String title, String message, String positive, String negative,
                                     OnDialogClickListener listener) {
        show(fragmentManager, title, message, positive, negative, false, listener);
    }

    private static void show(FragmentManager fragmentManager, String title, String message, String positive, String negative,
                            boolean isCancelable, OnDialogClickListener listener) {
        mAlertDialog = newInstance(title, message, positive, negative, isCancelable, listener);
        mAlertDialog.show(fragmentManager, "alert");
    }

}
