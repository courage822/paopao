package com.popo.library.util;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

/**
 * A utility of Android device and app version
 * Created by zhangdroid on 2017/5/11.
 */
public class DeviceUtil {

    private DeviceUtil() {
    }

    // ************************************** 屏幕size、location相关 **************************************

    public static DisplayMetrics getDisplayMeTrics(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    /**
     * 获取屏幕宽度
     */
    public static int getScreenWidth(Context context) {
        return getDisplayMeTrics(context).widthPixels;
    }

    /**
     * 获取屏幕高度
     */
    public static int getScreenHeight(Context context) {
        return getDisplayMeTrics(context).heightPixels;
    }

    /**
     * 获取设备的dpi
     */
    public static int getDeviceDpi(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return (int) (metrics.density * 160f);
    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 将sp值转换为px值，保证文字大小不变
     */
    public static int sp2px(Context context, float spValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

    /**
     * 将px值转换为sp值
     */
    public static int px2sp(Context context, float pxValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (pxValue / fontScale + 0.5f);
    }

    /**
     * 获取视图在屏幕中的位置
     *
     * @param view 要获取坐标的视图
     * @return 返回一个含有2个元素的数组, 第一个元素是x坐标、第二个为y坐标
     */
    public static int[] getViewLocationInScreen(View view) {
        int[] loc = new int[2];
        view.getLocationOnScreen(loc);
        return loc;
    }

    /**
     * 获取视图在一个Window中的位置
     *
     * @param view 要获取坐标的视图
     * @return 返回一个含有2个元素的数组, 第一个元素是x坐标、第二个为y坐标
     */
    public static int[] getViewLocationInWindow(View view) {
        int[] loc = new int[2];
        view.getLocationInWindow(loc);
        return loc;
    }

    /**
     * 获取状态栏的高度
     *
     * @param context Context
     * @return 状态栏高度
     */
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    // ************************************** 获取设备标识和相关信息 **************************************

    public static TelephonyManager getTelephonyManager(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager;
    }

    /**
     * 获取设备id(the IMEI for GSM and the MEID or ESN for CDMA phones)
     */
    public static String getDeviceId(Context context) {
        return getTelephonyManager(context).getDeviceId();
    }

    /**
     * 获取手机号码
     */
    public static String getPhoneNumber(Context context) {
        return getTelephonyManager(context).getLine1Number();
    }

    /**
     * 获取Android id
     */
    public static String getAndroidId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    /**
     * 获取手机型号
     */
    public static String getDeviceModel() {
        return Build.MODEL;
    }

    // ************************************** 获取APP版本信息 **************************************

    /**
     * 获得当前应用包名
     *
     * @param context 上下文对象
     */
    public static String getPackageName(Context context) {
        return context.getApplicationContext().getPackageName();
    }

    /**
     * 获得当前应用版本号（code）
     *
     * @param context 上下文对象
     */
    public static int getVersionCode(Context context) {
        return getPackageInfo(context).versionCode;
    }

    /**
     * 获得当前应用版本编号（name）
     *
     * @param context 上下文对象
     */
    public static String getVersionName(Context context) {
        return getPackageInfo(context).versionName;
    }

    /**
     * 获取应用名称
     *
     * @param context 上下文对象
     */
    public static String getAppName(Context context) {
        return (String) context.getApplicationInfo().loadLabel(getPackageManager(context));
    }

    public static PackageManager getPackageManager(Context context) {
        return context.getApplicationContext().getPackageManager();
    }

    public static PackageInfo getPackageInfo(Context context) {
        PackageManager packageManager = getPackageManager(context);
        try {
            return packageManager.getPackageInfo(getPackageName(context), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    // ************************************** 设备相关公用方法 **************************************

    /**
     * 检测是否安装了某个应用
     *
     * @param packageName 包名
     */
    public static boolean checkAPKExist(Context context, String packageName) {
        return (getPackageInfo(context) != null);
    }

    /**
     * 隐藏键盘
     */
    public static void hideKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 显示键盘
     *
     * @param context
     */
    public static void showKeyboard(Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * 拨打电话
     *
     * @param context  上下文对象
     * @param phoneNum 需要拨打的电话号码
     */
    public static void callPhone(Context context, String phoneNum) {
        if (!TextUtils.isEmpty(phoneNum)) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNum));
            context.startActivity(intent);
        }
    }

    /**
     * 跳转APP详情页面
     *
     * @param activity 上下文对象
     */
    public static void showAppDetail(Activity activity, int requestCode) {
        Uri packageURI = Uri.parse("package:" + getPackageName(activity));
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, packageURI);
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * 跳转设置页面
     *
     * @param activity 上下文对象
     */
    public static void showSysSetting(Activity activity) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
            intent.setAction(Settings.ACTION_SETTINGS);
        } else {
            ComponentName component = new ComponentName("com.android.settings", "com.android.settings.WirelessSettings");
            intent.setComponent(component);
            intent.setAction("android.intent.action.VIEW");
        }
        activity.startActivity(intent);
    }

    /**
     * 跳转应用市场
     *
     * @param context 上下文对象
     */
    public static void showGooglePlayDetail(Context context) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=" + getPackageName(context)));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (checkAPKExist(context, "com.android.vending")) {
            // 直接跳转Google商店
            intent.setPackage("com.android.vending");
        }
        context.startActivity(intent);
    }

}
