package com.popo.library.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * A utility for SharedPreference, save data type of String, boolean and int
 * Created by zhangdroid on 2017/5/11.
 */
public class SharedPreferenceUtil {

    private SharedPreferenceUtil() {
    }

    public static SharedPreferences getSharePreferences(Context context, String name) {
        if (context == null)
            return null;
        return context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public static boolean getBooleanValue(Context context, String name, String key, boolean defaultValue) {
        if (context == null) {
            return defaultValue;
        }
        return getSharePreferences(context, name).getBoolean(key, defaultValue);
    }

    public static void setBooleanValue(Context context, String name, String key, boolean value) {
        if (context == null || TextUtils.isEmpty(name)) {
            return;
        }
        getSharePreferences(context, name).edit().putBoolean(key, value).apply();
    }

    public static String getStringValue(Context context, String name, String key, String defaultValue) {
        if (context == null || TextUtils.isEmpty(name)) {
            return defaultValue;
        }
        return getSharePreferences(context, name).getString(key, defaultValue);
    }

    public static void setStringValue(Context context, String name, String key, String value) {
        if (context == null || TextUtils.isEmpty(name))
            return;
        getSharePreferences(context, name).edit().putString(key, value).apply();
    }

    public static int getIntValue(Context context, String name, String key, int defaultValue) {
        if (context == null || TextUtils.isEmpty(name)) {
            return defaultValue;
        }
        return getSharePreferences(context, name).getInt(key, defaultValue);
    }

    public static void setIntValue(Context context, String name, String key, int value) {
        if (context == null || TextUtils.isEmpty(name))
            return;
        getSharePreferences(context, name).edit().putInt(key, value).apply();
    }

    public static void clearXml(Context context, String name) {
        if (context == null || TextUtils.isEmpty(name))
            return;
        getSharePreferences(context, name).edit().clear().apply();
    }

    public static void removeKey(Context context, String name, String key) {
        if (context == null || TextUtils.isEmpty(name) || TextUtils.isEmpty(key))
            return;
        getSharePreferences(context, name).edit().remove(key).apply();
    }

}
