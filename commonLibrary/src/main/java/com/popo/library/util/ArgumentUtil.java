package com.popo.library.util;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;

/**
 * A util for Fragment arguments.
 * Created by zhangdroid on 2017/6/2.
 */
public class ArgumentUtil {
    private static final String ARGUMENT_KEY = "fragment_argument";

    private ArgumentUtil() {
    }

    /**
     * 设置Fragment需要传递的参数，统一使用Parcelable
     *
     * @param parcelable 需要传递的参数
     * @return
     */
    public static Bundle setArgumentBundle(Parcelable parcelable) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARGUMENT_KEY, parcelable);
        return bundle;
    }

    /**
     * 获得传递过来的Parcelable参数
     *
     * @param bundle {@link Fragment#getArguments()}
     * @return
     */
    public static Parcelable getArgumentParcelable(Bundle bundle) {
        if (null != bundle) {
            return bundle.getParcelable(ARGUMENT_KEY);
        }
        return null;
    }

}
