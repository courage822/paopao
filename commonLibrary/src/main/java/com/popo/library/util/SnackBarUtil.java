package com.popo.library.util;

import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.popo.library.R;

/**
 * A Utility for snackbar
 * Created by zhangdroid on 2017/5/24.
 */
public class SnackBarUtil {

    public static Snackbar showShort(View view, String msg) {
        Snackbar snackbar = Snackbar.make(view, TextUtils.isEmpty(msg) ? "" : msg, Snackbar.LENGTH_SHORT);
        snackbar.show();
        return snackbar;
    }

    public static Snackbar showLong(View view, String msg) {
        Snackbar snackbar = Snackbar.make(view, TextUtils.isEmpty(msg) ? "" : msg, Snackbar.LENGTH_LONG);
        snackbar.show();
        return snackbar;
    }

    /**
     * @param view            The view to find a parent from.
     * @param msg             The message to show.
     * @param actionMsg       The message to show on the action
     * @param onClickListener On action click callback
     * @return
     */
    public static Snackbar showWithAction(View view, String msg, String actionMsg, View.OnClickListener onClickListener) {
        Snackbar snackbar = Snackbar.make(view, TextUtils.isEmpty(msg) ? "" : msg, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(actionMsg, onClickListener);
        snackbar.show();
        return snackbar;
    }

    /**
     * 设置SnackBar文字颜色
     */
    public static void setTextColor(Snackbar snackbar, int color) {
        View view = snackbar.getView();
        TextView textView = (TextView) view.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
    }

    /**
     * 设置SnackBar背景颜色
     */
    public static void setBackgroundColor(Snackbar snackbar, int color) {
        View view = snackbar.getView();
        view.setBackgroundColor(color);
    }

}
