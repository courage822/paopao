package com.popo.library.util;

import android.util.Log;

/**
 * A log util
 * Created by zhangdroid on 2017/5/11.
 */
public class LogUtil {
    private static final boolean sIsDebugEnabled = true;

    private LogUtil() {
    }

    public static void v(String tag, String msg) {
        if (sIsDebugEnabled) {
            Log.v(tag, msg);
        }
    }

    public static void d(String tag, String msg) {
        if (sIsDebugEnabled) {
            Log.d(tag, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (sIsDebugEnabled) {
            Log.i(tag, msg);
        }
    }

    public static void w(String tag, String msg) {
        if (sIsDebugEnabled) {
            Log.w(tag, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (sIsDebugEnabled) {
            Log.e(tag, msg);
        }
    }

}
