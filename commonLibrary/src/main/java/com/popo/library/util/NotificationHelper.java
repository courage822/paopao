package com.popo.library.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;

/**
 * 通知栏帮助类
 * Created by zhangdroid on 2017/7/5.
 */
public class NotificationHelper {
    private static NotificationHelper sInstance;
    private Context mContext;
    private NotificationCompat.Builder mNotificationBuilder;
    private NotificationManager mNotificationManager;

    public NotificationHelper(Context context) {
        this.mContext = context;
        this.mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        this.mNotificationBuilder = new NotificationCompat.Builder(context);
    }

    public static NotificationHelper getInstance(Context context) {
        if (null == sInstance) {
            synchronized (NotificationHelper.class) {
                if (null == sInstance) {
                    sInstance = new NotificationHelper(context.getApplicationContext());
                }
            }
        }
        return sInstance;
    }

    public Context getContext() {
        return mContext;
    }

    /**
     * 设置通知图标
     *
     * @param iconResId
     * @return
     */
    public NotificationHelper setIcon(int iconResId) {
        if (iconResId != 0) {
            mNotificationBuilder.setSmallIcon(iconResId);
        }
        return this;
    }

    /**
     * 设置通知标题
     *
     * @param title 待设置的标题
     * @return
     */
    public NotificationHelper setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            mNotificationBuilder.setContentTitle(title);
        }
        return this;
    }

    /**
     * 设置通知消息
     *
     * @param message 待设置的消息内容
     * @return
     */
    public NotificationHelper setMessage(String message) {
        if (!TextUtils.isEmpty(message)) {
            mNotificationBuilder.setContentText(message);
        }
        return this;
    }

    /**
     * 设置通知显示时间戳
     *
     * @param timeMillis
     * @return
     */
    public NotificationHelper setTime(long timeMillis) {
        if (timeMillis != 0) {
            mNotificationBuilder.setWhen(timeMillis);
        } else {
            mNotificationBuilder.setWhen(System.currentTimeMillis());
        }
        return this;
    }

    /**
     * 设置通知在状态栏显示的提示消息
     *
     * @param tickerText 提示消息内容
     * @return
     */
    public NotificationHelper setTicker(String tickerText) {
        if (!TextUtils.isEmpty(tickerText)) {
            mNotificationBuilder.setTicker(tickerText);
        }
        return this;
    }

    /**
     * 设置默认效果（提示音、震动和LED灯光）
     *
     * @return
     */
    public NotificationHelper setDefaults() {
        mNotificationBuilder.setDefaults(NotificationCompat.DEFAULT_ALL);
        return this;
    }

    /**
     * 设置默认提示音
     *
     * @return
     */
    public NotificationHelper setDefaultSound() {
        mNotificationBuilder.setDefaults(NotificationCompat.DEFAULT_SOUND);
        return this;
    }

    /**
     * 设置默认震动
     *
     * @return
     */
    public NotificationHelper setDefaultVibrate() {
        mNotificationBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);
        return this;
    }

    /**
     * 设置默认LED灯光
     *
     * @return
     */
    public NotificationHelper setDefaultLight() {
        mNotificationBuilder.setDefaults(NotificationCompat.DEFAULT_LIGHTS);
        return this;
    }

    /**
     * 设置点击通知栏事件，通过Intent启动Activity
     *
     * @param requestCode 请求码，用来区分不同的通知
     * @param intent      启动Activity的Intent
     * @param flag        {@link PendingIntent#FLAG_CANCEL_CURRENT}、{@link PendingIntent#FLAG_UPDATE_CURRENT }等
     * @return
     */
    public NotificationHelper setPendingIntentActivity(int requestCode, Intent intent, int flag) {
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, requestCode, intent, flag);
        mNotificationBuilder.setContentIntent(pendingIntent);
        return this;
    }

    /**
     * 设置点击通知栏事件，通过Intent启动Service
     *
     * @param requestCode 请求码，用来区分不同的通知
     * @param intent      启动Service的Intent
     * @param flag        {@link PendingIntent#FLAG_CANCEL_CURRENT}、{@link PendingIntent#FLAG_UPDATE_CURRENT }等
     * @return
     */
    public NotificationHelper setPendingIntentService(int requestCode, Intent intent, int flag) {
        PendingIntent pendingIntent = PendingIntent.getService(mContext, requestCode, intent, flag);
        mNotificationBuilder.setContentIntent(pendingIntent);
        return this;
    }

    /**
     * 设置通知是否可以自动取消
     *
     * @param autoCancel
     * @return
     */
    public NotificationHelper setAutoCancel(boolean autoCancel) {
        mNotificationBuilder.setAutoCancel(autoCancel);
        return this;
    }

    /**
     * 设置通知优先级
     *
     * @param priority
     * @return
     */
    public NotificationHelper setPriority(int priority) {
        mNotificationBuilder.setPriority(priority);
        return this;
    }

    /**
     * 设置是否正在进行（通常用来显示正在进行的后台任务）
     *
     * @param isOngoing
     * @return
     */
    public NotificationHelper setOngoing(boolean isOngoing) {
        mNotificationBuilder.setOngoing(isOngoing);
        return this;
    }

    /**
     * 设置通知ticker、通知音、震动等提示是否只提示一次
     *
     * @param onlyAlertOnce
     * @return
     */
    public NotificationHelper setOnlyAlertOnce(boolean onlyAlertOnce) {
        mNotificationBuilder.setOnlyAlertOnce(onlyAlertOnce);
        return this;
    }

    /**
     * 显示指定id的通知
     *
     * @param id
     */
    public void notify(int id) {
        Notification notification = mNotificationBuilder.build();
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(id, notification);
    }
    /**
     * 设置通知的跳转的界面
     *
     *
     */
    public NotificationHelper setContentIntent( PendingIntent pendingIntent ) {
        mNotificationBuilder.setContentIntent(pendingIntent);
        return this;
    }
    /**
     * 取消指定id的通知
     *
     * @param id
     */
    public void cancel(int id) {
        mNotificationManager.cancel(id);
    }

    /**
     * 取消所有的通知
     */
   public void cancelAll(){
       mNotificationManager.cancelAll();
   }

}
