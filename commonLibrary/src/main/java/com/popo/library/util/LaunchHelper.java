package com.popo.library.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

/**
 * Activity跳转帮助类，单例模式
 * <li>1、直接跳转</li>
 * <li>2、直接跳转，跳转后finish掉前一个Activity</li>
 * <li>3、跳转的同时传递参数（统一使用Parcelable）</li>
 * <li>4、跳转的同时传递参数（统一使用Parcelable），跳转后finish掉前一个Activity</li>
 * <li>5、支持{@link Activity#startActivityForResult(Intent, int)}方式</li>
 * <li>6、支持{@link Activity#setResult(int, Intent)}方法</li>
 * <li></li>
 * Created by zhangdroid on 2017/5/18.
 */
public class LaunchHelper {
    // 所有Activity使用Intent传参时公用的key
    public static final String INTENT_KEY_COMMON = "intent_key_common_parcelable";
    private static volatile LaunchHelper sInstance;

    private LaunchHelper() {
    }

    public static LaunchHelper getInstance() {
        if (null == sInstance) {
            synchronized (LaunchHelper.class) {
                sInstance = new LaunchHelper();
            }
        }
        return sInstance;
    }

    private boolean isActivity(Context context) {
        return context instanceof Activity;
    }

    /**
     * 设置需要传递的参数
     */
    private void putParcelable(Intent intent, Parcelable parcelable) {
        if (parcelable == null) return;
        intent.putExtra(INTENT_KEY_COMMON, parcelable);
    }

    /**
     * 获取上一个Activity传递过来的参数
     */
    public <T> T getParcelableExtra(Activity activity) {
        Parcelable parcelable = activity.getIntent().getParcelableExtra(INTENT_KEY_COMMON);
        activity = null;
        return (T) parcelable;
    }

    /**
     * 跳转Activity
     */
    public void launch(Context context, Class<? extends Activity> clz) {
        Intent intent = new Intent(context, clz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 跳转Activity后finish前一个Activity
     */
    public void launchFinish(Context context, Class<? extends Activity> clz) {
        Intent intent = new Intent(context, clz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        if (isActivity(context)) {
            ((Activity) context).finish();
        }
        context = null;
    }

    /**
     * 跳转Activity并返回结果
     */
    public void launchResult(Activity activity, Class<? extends Activity> clz, int requestCode) {
        Intent intent = new Intent(activity, clz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivityForResult(intent, requestCode);
        activity = null;
    }

    /**
     * 跳转Activity并返回结果，之后finish前一个Activity
     */
    public void launchResultFinish(Activity activity, Class<? extends Activity> clz, int requestCode) {
        Intent intent = new Intent(activity, clz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivityForResult(intent, requestCode);
        activity.finish();
        activity = null;
    }

    /**
     * 跳转Activity，同时传参
     */
    public void launch(Context context, Class<? extends Activity> clz, Parcelable parcelable) {
        Intent intent = new Intent(context, clz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        putParcelable(intent, parcelable);
        context.startActivity(intent);
        context = null;
    }

    /**
     * 跳转Activity，同时传参，之后finish前一个Activity
     */
    public void launchFinish(Context context, Class<? extends Activity> clz, Parcelable parcelable) {
        Intent intent = new Intent(context, clz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        putParcelable(intent, parcelable);
        context.startActivity(intent);
        if (isActivity(context)) {
            ((Activity) context).finish();
        }
        context = null;
    }

    /**
     * 跳转Activity并返回结果，同时传参
     */
    public void launchResult(Activity activity, Class<? extends Activity> clz, Parcelable parcelable, int requestCode) {
        Intent intent = new Intent(activity, clz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        putParcelable(intent, parcelable);
        activity.startActivityForResult(intent, requestCode);
        activity = null;
    }

    /**
     * 跳转Activity并返回结果，同时传参，之后finish前一个Activity
     */
    public void launchResultFinish(Activity activity, Class<? extends Activity> clz, Parcelable parcelable, int requestCode) {
        Intent intent = new Intent(activity, clz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        putParcelable(intent, parcelable);
        activity.startActivityForResult(intent, requestCode);
        activity.finish();
        activity = null;
    }

    public void setResult(Context context, Class<? extends Activity> targetClazz, int resultCode, Parcelable parcelable) {
        Intent intent = new Intent(context, targetClazz);
        putParcelable(intent, parcelable);
        if (isActivity(context)) {
            ((Activity) context).setResult(resultCode, intent);
            ((Activity) context).finish();
        }
    }

}
