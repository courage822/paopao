package com.popo.library.util;

import android.content.Context;
import android.text.TextUtils;

import com.popo.library.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A utility of date/time
 * Created by zhangdroid on 2017/5/11.
 */
public class DateTimeUtil {
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    public static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    private DateTimeUtil() {
    }

    public static String getCurrentDate() {
        return dateFormat.format(new Date(System.currentTimeMillis()));
    }

    public static String getCurrentTime() {
        return timeFormat.format(new Date(System.currentTimeMillis()));
    }

    /**
     * 将时间戳转换成时间字符串
     *
     * @param timeMillis
     * @return
     */
    public static String convertTimeMillis2String(long timeMillis) {
        return timeFormat.format(new Date(timeMillis));
    }

    /**
     * 显示日期，距离现在已经过了多久
     *
     * @param context 上下文对象
     * @param date    需要显示的日期
     * @param format  需要显示的格式，为空时显示默认格式
     * @return 返回指定格式的时间（默认为yyyy年MM月dd日 HH:mm），当前年不显示年份
     */
    public static String showTimeAgo(Context context, String date, String format) {
        if (TextUtils.isEmpty(format)) {
            format = "yyyy-MM-dd HH:mm";
        }
        String resultTime = null;
        try {
            Date oldTime = timeFormat.parse(date);
            resultTime = new SimpleDateFormat(format, Locale.getDefault()).format(oldTime);
            Date currentDate = new Date();
            if (oldTime.getYear() == currentDate.getYear()) {// 当前年
                long result = currentDate.getTime() - oldTime.getTime();
                if (result < 60 * 1000) {// 一分钟内
                    resultTime = context.getString(R.string.moment_ago);
                } else if (result >= 60 * 1000 && result < 60 * 60 * 1000) {// 一小时内
                    int mins = (int) (result / (60 * 1000));
                    resultTime = context.getResources().getQuantityString(R.plurals.minutes_ago, mins, mins);
                } else if (result >= 60 * 60 * 1000 && result <= 24 * 60 * 60 * 1000) {// 24小时内
                    int hours = (int) (result / (60 * 60 * 1000));
                    resultTime = context.getResources().getQuantityString(R.plurals.hours_ago, hours, hours);
                } else {// 第二日及以后
                    resultTime = pad(oldTime.getMonth() + 1) + "-" + pad(oldTime.getDate()) + " " + resultTime.substring(11);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultTime;
    }

    /**
     * 日期/时间小于10时格式补0
     */
    public static String pad(int c) {
        if (c >= 10) {
            return String.valueOf(c);
        } else {
            return "0" + String.valueOf(c);
        }
    }

}
