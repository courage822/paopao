package com.popo.library.util;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

/**
 * A Utility for toast
 * Created by zhangdroid on 2017/5/11.
 */
public class ToastUtil {
    private static Toast sDefault;

    private ToastUtil() {
    }

    /**
     * Show a short toast
     *
     * @param context the context to make the toast
     * @param message the message to show
     */
    public static void showShortToast(Context context, String message) {
        showToast(context, message, Toast.LENGTH_SHORT);
    }

    /**
     * Show a long toast
     *
     * @param context the context to make the toast
     * @param message the message to show
     */
    public static void showLongToast(Context context, String message) {
        showToast(context, message, Toast.LENGTH_LONG);
    }

    /**
     * Show custom duration toast
     *
     * @param context  the context to make the toast
     * @param message  the message to show
     * @param duration the duration of toast to show
     */
    public static void showToast(Context context, String message, int duration) {
        if (TextUtils.isEmpty(message)) {
            return;
        }
        if (sDefault == null) {
            sDefault = Toast.makeText(context, message, duration);
        } else {
            sDefault.setText(message);
        }
        sDefault.show();
    }

}
