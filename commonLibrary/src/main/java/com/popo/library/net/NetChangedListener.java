package com.popo.library.net;

/**
 * 网络状态广播监听器
 * Created by zhangdroid on 2017/5/11.
 */
public interface NetChangedListener {

    /**
     * when network connected callback
     */
    void onNetConnected(NetUtil.NetType type);

    /**
     * when network disconnected callback
     */
    void onNetDisConnected();
}
