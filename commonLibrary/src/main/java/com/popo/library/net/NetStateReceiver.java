package com.popo.library.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 网络状态广播监听器
 * Created by zhangdroid on 2017/5/11.
 */
public class NetStateReceiver extends BroadcastReceiver {
    private final static String TAG = NetStateReceiver.class.getSimpleName();
    private final static String ANDROID_NET_CHANGE_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private static NetStateReceiver mNetStateReceiver;
    private static List<NetChangedListener> mNetChangedListeners;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (!TextUtils.isEmpty(action) && ANDROID_NET_CHANGE_ACTION.equalsIgnoreCase(action)) {
            boolean isNetworkAvailable = NetUtil.isNetworkAvailable(context);
            NetUtil.NetType netType = NetUtil.getNetType(context);
            if (null != mNetChangedListeners) {
                for (NetChangedListener netChangedListener : mNetChangedListeners) {
                    if (null != netChangedListener) {
                        if (isNetworkAvailable) {
                            netChangedListener.onNetConnected(netType);
                        } else {
                            netChangedListener.onNetDisConnected();
                        }
                    }
                }
            }
        }
    }

    public static NetStateReceiver getInstance() {
        if (null == mNetStateReceiver) {
            synchronized (NetStateReceiver.class) {
                mNetStateReceiver = new NetStateReceiver();
            }
        }
        return mNetStateReceiver;
    }

    public static void registerNetworkStateReceiver(Context mContext) {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ANDROID_NET_CHANGE_ACTION);
        LocalBroadcastManager.getInstance(mContext.getApplicationContext()).registerReceiver(getInstance(), filter);
    }

    public static void unregisterNetworkStateReceiver(Context mContext) {
        if (mNetStateReceiver != null) {
            LocalBroadcastManager.getInstance(mContext.getApplicationContext()).unregisterReceiver(mNetStateReceiver);
        }
    }

    public static void setNetChangedListener(NetChangedListener netChangedListener) {
        if (null == mNetChangedListeners) {
            mNetChangedListeners = new ArrayList<>();
        }
        if (null != netChangedListener) {
            mNetChangedListeners.add(netChangedListener);
        }
    }

    public static void removeNetChangedListener(NetChangedListener netChangedListener) {
        if (null != netChangedListener && mNetChangedListeners.contains(netChangedListener)) {
            mNetChangedListeners.remove(netChangedListener);
        }
    }

}