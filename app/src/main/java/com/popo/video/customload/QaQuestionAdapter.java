package com.popo.video.customload;

import android.content.Context;

import com.popo.library.adapter.CommonRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.model.QaAnswer;
import com.popo.video.data.preference.PlatformPreference;

/**
 * QA问题展示
 * Created by WangYong on 2017/9/19.
 */
public class QaQuestionAdapter extends CommonRecyclerViewAdapter<QaAnswer> {
    public QaQuestionAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    public void convert(QaAnswer answer, int position, RecyclerViewHolder holder) {
        if(answer!=null){
            if (PlatformPreference.getPlatformInfo().getFid().equals("30108")) {//通过渠道号判断简体繁体
                holder.setText(R.id.qa_question_item_tv_answer, Util.chineseFontChanger(answer.getContent()));
            }else{
                holder.setText(R.id.qa_question_item_tv_answer, answer.getContent());
            }
        }
    }

//    tv_answer= (TextView) itemView.findViewById(R.id.qa_question_item_tv_answer);
}
