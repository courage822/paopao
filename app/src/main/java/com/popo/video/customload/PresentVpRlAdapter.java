package com.popo.video.customload;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.model.DictPayGift;
import com.popo.library.adapter.CommonRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;

/**
 * Created by WangYong on 2017/10/21.
 */

public class PresentVpRlAdapter extends CommonRecyclerViewAdapter<DictPayGift> {
    // 当前选中的Item索引
    private int mSelectedPosition = 100;

    public PresentVpRlAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    public void convert(DictPayGift dictPayGift, int position, RecyclerViewHolder holder) {
        if (dictPayGift != null) {
            ImageView iv_icon = (ImageView) holder.getView(R.id.dialog_present_item_iv);
            TextView tv_dionmads = (TextView) holder.getView(R.id.dialog_present_item_tv_dionmads);
            LinearLayout ll_bg = (LinearLayout) holder.getView(R.id.dialog_present_item_ll_bg);
            TextView tv_name = (TextView) holder.getView(R.id.dialog_present_item_tv_name);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(dictPayGift.getGiftUrl())
                    .placeHolder(0).error(0).imageView(iv_icon).build());
            tv_dionmads.setText(dictPayGift.getPrice() + mContext.getString(R.string.dialog_unit_ask_gift));
            if (Util.getLacalLanguage().equals("Simplified")) {
                tv_name.setText(dictPayGift.getGiftName());
            } else if (Util.getLacalLanguage().equals("Traditional")) {
                tv_name.setText(dictPayGift.getTraditional());
            } else {
                tv_name.setText(dictPayGift.getEnglish());
            }
            if (mSelectedPosition == position) {
                ll_bg.setBackgroundResource(R.drawable.selected_square);
            } else {
                ll_bg.setBackgroundResource(0);
            }
        }
    }

    public void setSelectedPosition(int selectedPosition) {
        this.mSelectedPosition = selectedPosition;
    }

}
