package com.popo.video.mvp;

import android.content.Context;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public interface BaseView {

    Context obtainContext();

    /**
     * Show tip message(Toast or SnackBar)
     *
     * @param msg
     * @see com.popo.library.util.ToastUtil
     * @see com.popo.library.util.SnackBarUtil
     */
    void showTip(String msg);

}
