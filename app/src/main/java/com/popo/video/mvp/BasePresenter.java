package com.popo.video.mvp;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public interface BasePresenter {
    void start();
}
