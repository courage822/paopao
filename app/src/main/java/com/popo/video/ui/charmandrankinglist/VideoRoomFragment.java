package com.popo.video.ui.charmandrankinglist;

import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.popo.library.dialog.AlertDialog;
import com.popo.library.dialog.OnDialogClickListener;
import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.ToastUtil;
import com.popo.playerlibrary.EmptyControlVideo;
import com.popo.playerlibrary.gsyvideoplayer.GSYVideoManager;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.base.BaseFragment;
import com.popo.video.common.CustomDialogAboutOther;
import com.popo.video.common.CustomDialogAboutPay;
import com.popo.video.common.Util;
import com.popo.video.common.VideoHelper;
import com.popo.video.customload.blurview.BlurView;
import com.popo.video.customload.blurview.SupportRenderScriptBlur;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.CheckStatus;
import com.popo.video.data.model.VideoSquare;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.event.GiftSendEvent;
import com.popo.video.event.IsFollow;
import com.popo.video.event.PauseEvent;
import com.popo.video.event.StartEvent;
import com.popo.video.event.UpdataFollowUser;
import com.popo.video.parcelable.ChatParcelable;
import com.popo.video.parcelable.PayParcelable;
import com.popo.video.parcelable.UserDetailParcelable;
import com.popo.video.parcelable.VideoInviteParcelable;
import com.popo.video.parcelable.VideoShowParcelable;
import com.popo.video.ui.chat.ChatActivity;
import com.popo.video.ui.detail.UserDetailActivity;
import com.popo.video.ui.main.MainActivity;
import com.popo.video.ui.pay.activity.RechargeActivity;
import com.popo.video.ui.personalcenter.AuthenticationActivity;
import com.popo.video.ui.personalcenter.VideoShowActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 待整理，优化
 * Created by xuzhaole on 2018/3/20.
 */

public class VideoRoomFragment extends BaseFragment implements View.OnClickListener {
    protected boolean isInit = false;
    protected boolean isLoad = false;
    @BindView(R.id.player_empty_room)
    EmptyControlVideo player_empty_room;
    @BindView(R.id.iv_avatar)
    ImageView ivAvatar;
    @BindView(R.id.rl_avatar)
    RelativeLayout rlAvatar;
    @BindView(R.id.ll_chat)
    LinearLayout llChat;
    @BindView(R.id.iv_video)
    ImageView ivVideo;
    @BindView(R.id.rl_video)
    RelativeLayout rlVideo;
    @BindView(R.id.iv_send_gift)
    ImageView ivSendGift;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.iv_cover_photo)
    ImageView iv_cover_photo;
    @BindView(R.id.iv_follow)
    ImageView iv_follow;
    @BindView(R.id.blurview)
    BlurView blurview;
    @BindView(R.id.btn_tovip)
    Button btn_tovip;
    @BindView(R.id.rl_videoroom_root)
    RelativeLayout rl_videoroom_root;
    private VideoSquare mData;
    private long lastClickTime = 0;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        initView();
    }

    protected void initView() {
        if (!isInit) {
            return;
        }
        if (getUserVisibleHint()) {
            if (mData != null && mData.getUserBase() != null) {
                if (PlatformPreference.getPlatformInfo().getFid().equals("30108")) {
                    tvName.setText(Util.chineseFontChanger(mData.getUserBase().getNickName()));
                    tvAccount.setText(Util.chineseFontChanger(mData.getUserBase().getOwnWords()));
                }else{
                    tvName.setText(mData.getUserBase().getNickName());
                    tvAccount.setText(mData.getUserBase().getOwnWords());
                }
            }
            if (mData != null && mData.gettUserVideoShow() != null) {
                if (mData.getIsFollow() == 1) {
                    iv_follow.setVisibility(View.GONE);
                } else {
                    iv_follow.setVisibility(View.VISIBLE);
                }
                ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder()
                        .url(mData.getUserBase().getIconUrlMininum())
                        .transform(new CropCircleTransformation(mContext))
                        .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(ivAvatar).build());

                playVideoShow();
                isLoad = true;
            }
        }
    }

    private void playVideoShow() {
            player_empty_room.setUp(mData.gettUserVideoShow().getVideoUrl(), true, "123");
            player_empty_room.setLooping(true);
            player_empty_room.startPlayLogic();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && getActivityPosition() == 2) {
            playVideoShow();
        }
        if (getArguments().getInt("position", 0) == 5 && !UserPreference.isVip() && !UserPreference.isAnchor()) {
            blurview.setVisibility(View.VISIBLE);
            final Drawable windowBackground = ((MainActivity) mContext).getWindow().getDecorView().getBackground();
            blurview.setupWith(rl_videoroom_root)
                    .windowBackground(windowBackground)
                    .blurAlgorithm(new SupportRenderScriptBlur(mContext))
                    .blurRadius(20)
                    .setHasFixedTransformationMatrix(true);
        } else {
            blurview.setVisibility(View.GONE);
        }
    }

    private int getActivityPosition() {
        MainActivity mainActivity = (MainActivity) getActivity();
        return mainActivity.getPosition();
    }

    @Override
    public void onPause() {
        if (getUserVisibleHint() && getActivityPosition() == 2){
            GSYVideoManager.instance().onPause();
        }
        super.onPause();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isLoad = false;
        isInit = false;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_video_show;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        isInit = true;
        initView();
    }

    @Override
    protected void setListeners() {
        rlAvatar.setOnClickListener(this);
        rlVideo.setOnClickListener(this);
        llChat.setOnClickListener(this);
        ivSendGift.setOnClickListener(this);
        btn_tovip.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    public void setData(VideoSquare t) {
        this.mData = t;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_avatar://头像
                if (mData == null) {
                    return;
                }
                if (UserPreference.isAnchor()) {
                    ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                    return;
                }
                if (mData.getIsFollow() == 1) {//已关注，进入到详情
                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                            new UserDetailParcelable(String.valueOf(mData.getUserBase().getGuid())));
                } else {//未关注，直接关注
                    follow();
                }
                break;
            case R.id.rl_video://视频
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    if (UserPreference.isAnchor()) {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                        return;
                    }
                    goToVideoInvite();
                }

                break;
            case R.id.ll_chat://聊天
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    if (UserPreference.isAnchor()) {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                        return;
                    }

                    //跳转写信的界面
                    goToWriteMessage();
                }
                break;
            case R.id.iv_send_gift://送礼物
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    if (UserPreference.isAnchor()) {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                        return;
                    }
                    CustomDialogAboutOther.giveGiftShow(mContext, mData.getUserBase().getGuid() + "", mData.getUserBase().getAccount(), mData.getUserBase().getIconUrlMininum(), mData.getUserBase().getNickName(), 1, false);
                }
                break;
            case R.id.btn_tovip:
                LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 0));
                break;
        }
    }

    private void follow() {
        ApiManager.follow(mData.getUserBase().getGuid() + "", new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                EventBus.getDefault().post(new IsFollow(mData.getUserBase().getAccount()));
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                ToastUtil.showShortToast(mContext, mContext.getString(R.string.follow_fail));
            }
        });
    }

    private void goToVideoInvite() {
        if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
            getStatus();
        } else {
            if (UserPreference.getStatus().equals("3")) {
                Toast.makeText(mContext, mContext.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
            } else {
                if (mData.getUserBase() != null) {
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, mData.getUserBase().getGuid(), mData.getUserBase().getAccount()
                            , mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0, 0), mContext, "1");
                }
            }
        }
    }

    private void goToWriteMessage() {
        if (mData.getUserBase() != null) {
            if (UserPreference.isAnchor()) {
                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
                        mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
            } else if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
                getStatus();
            } else {
                if (UserPreference.isVip() || UserPreference.isAnchor()) {
                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
                } else {
                    CustomDialogAboutPay.dregeVipShow(mContext);
                }
            }
        }
    }

    private void getStatus() {
        ApiManager.getAuthorCheckStatus(new IGetDataListener<CheckStatus>() {
            @Override
            public void onResult(CheckStatus checkStatus, boolean isEmpty) {
                if (checkStatus != null) {
                    if (checkStatus.getAuditStatus() == 0) {//审核中
                        if (checkStatus.getShowStatus() == 1) {//完整
                            Toast.makeText(mContext, "认证审核中.....", Toast.LENGTH_SHORT).show();
                        } else if (checkStatus.getShowStatus() == -1) {//不完整
                            AlertDialog.showNoCanceled(getFragmentManager(), "", "视频聊天需要通过真人认证哦",
                                    "去认证", "取消", new OnDialogClickListener() {
                                        @Override
                                        public void onNegativeClick(View view) {
                                        }

                                        @Override
                                        public void onPositiveClick(View view) {
                                            LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
                                        }
                                    }
                            );
                        }
                    }else{
                        AlertDialog.showNoCanceled(getFragmentManager(), "", "视频聊天需要通过真人认证哦",
                                "去认证", "取消", new OnDialogClickListener() {
                                    @Override
                                    public void onNegativeClick(View view) {
                                    }

                                    @Override
                                    public void onPositiveClick(View view) {
                                        LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                                    }
                                }
                        );
                    }

                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                ToastUtil.showShortToast(mContext,mContext.getString(R.string.tip_error));
            }
        });
    }

    @Subscribe
    public void onEvent(IsFollow follow) {
        if (follow.getAccount().equals(mData.getUserBase().getAccount())) {
            mData.setIsFollow(mData.getIsFollow() == 1 ? 0 : 1);
            if (mData.getIsFollow() == 1) {
                iv_follow.setVisibility(View.GONE);
            } else {
                iv_follow.setVisibility(View.VISIBLE);
            }
        }
    }

    @Subscribe
    public void onEvent(UpdataFollowUser follow) {
        if (follow.getGuid().equals(String.valueOf(mData.getUserBase().getGuid()))) {
            mData.setIsFollow(mData.getIsFollow() == 1 ? 0 : 1);
            if (mData.getIsFollow() == 1) {
                iv_follow.setVisibility(View.GONE);
            } else {
                iv_follow.setVisibility(View.VISIBLE);
            }
        }
    }

    @Subscribe
    public void onEvent(GiftSendEvent giftSendEvent) {
        if (getUserVisibleHint() && player_empty_room.isInPlayingState()) {
            goToWriteMessage();
        }
    }

    @Subscribe
    public void onEvent(StartEvent start) {
        if (getUserVisibleHint()) {
            GSYVideoManager.instance().onResume();
        }
    }

    @Subscribe
    public void onEvent(PauseEvent pause) {
        if (player_empty_room != null) {
            GSYVideoManager.instance().onPause();
        }
    }
}
