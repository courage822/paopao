package com.popo.video.ui.message;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.event.MessageArrive;
import com.popo.library.adapter.wrapper.OnLoadMoreListener;
import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.widget.AutoSwipeRefreshLayout;
import com.popo.library.widget.XRecyclerView;
import com.popo.video.ui.auto.AutoReplyActivity;
import com.popo.video.ui.message.adapter.ChatHistoryAdapter;
import com.popo.video.ui.message.contract.HelloPeopleContract;
import com.popo.video.ui.message.presenter.HelloPeoplePresenter;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by xuzhaole on 2017/12/2.
 */

public class HelloPeopleActivity extends BaseFragmentActivity implements HelloPeopleContract.IView {
    @BindView(R.id.auto_reply_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.auto_reply_activity_tv_title)
    TextView tv_title;
    @BindView(R.id.auto_reply_activity_rl_add)
    RelativeLayout rl_add;
    @BindView(R.id.chat_history_list)
    XRecyclerView mRecyclerView;
    @BindView(R.id.chat_history_refresh)
    AutoSwipeRefreshLayout mSwipeRefreshLayout;
    private HelloPeoplePresenter mHelloPeoplePresenter;
    private boolean mIsFirstLoad;


    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_hello_people;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
//        // 添加小秘书
//        mRecyclerView.addHeaderView(R.layout.chat_admin);
        mHelloPeoplePresenter = new HelloPeoplePresenter(this);
        mHelloPeoplePresenter.start();
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mHelloPeoplePresenter.refresh();
                }
            }
        });
        mRecyclerView.setOnLoadingMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mHelloPeoplePresenter.loadData();
            }
        });
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rl_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LaunchHelper.getInstance().launch(mContext, AutoReplyActivity.class);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mHelloPeoplePresenter.refresh();
    }

    @Override
    protected void loadData() {
        mHelloPeoplePresenter.refresh();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void setAdapter(ChatHistoryAdapter pagerAdapter, int layoutid) {
        //没有添加头布局的
        mRecyclerView.setAdapter(pagerAdapter);
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHelloPeoplePresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHelloPeoplePresenter.refresh();
            }
        });
    }

    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null != mSwipeRefreshLayout && mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void showLoadMore() {
        mRecyclerView.setVisibility(R.id.load_more_progress, true);
        mRecyclerView.setVisibility(R.id.load_more_msg, true);
        mRecyclerView.setVisibility(R.id.load_more_empty, false);
        mRecyclerView.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
        mRecyclerView.finishLoadMore();
    }

    @Override
    public void showNoMore() {
        mRecyclerView.setVisibility(R.id.load_more_empty, true);
        mRecyclerView.setVisibility(R.id.load_more_progress, false);
        mRecyclerView.setVisibility(R.id.load_more_msg, false);
    }

    @Override
    public ChatHistoryAdapter getChatHistoryAdapter() {
        return null;
    }
    @Subscribe
    public void onEvent(MessageArrive arrive){
        handler.sendEmptyMessage(1);

    }
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            mHelloPeoplePresenter.refresh();
        }
    };
}
