package com.popo.video.ui.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.model.UserPhoto;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.video.ui.detail.listener.OnItemClickListener;

import java.util.List;

/**
 * Created by xuzhaole on 2018/1/12.
 */

public class AlbumAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<UserPhoto> mList;
    private Context mContext;
    private LayoutInflater inflater;
    private static final int TYPE_ADD = 1;
    private static final int TYPE_PHOTO = 2;

    private OnItemClickListener mOnItemClickListener;
    private OnItemClickListener mAddClickListener;
    private OnItemClickListener mOnLongClickListener;

    public AlbumAdapter(Context context) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(context);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public void setOnLongClickListener(OnItemClickListener listener) {
        this.mOnLongClickListener = listener;
    }

    public void setAddClickListener(OnItemClickListener listener) {
        this.mAddClickListener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_ADD: {
                return new AddViewHolder(inflater.inflate(R.layout.album_add_item, parent, false));
            }
            case TYPE_PHOTO:
            default: {
                return new PhotoViewHolder(inflater.inflate(R.layout.album_photo_item, parent, false));
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_ADD: {
                break;
            }
            case TYPE_PHOTO:
            default: {
                PhotoViewHolder photoHolder = (PhotoViewHolder) holder;
                UserPhoto userPhoto = mList.get(position);
                photoHolder.setData(userPhoto);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            UserPhoto userPhoto = (UserPhoto) payloads.get(0);
            mList.add(userPhoto);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mList == null || mList.size() == 0) {
            return TYPE_ADD;
        } else {
            if (position == mList.size()) {
                return TYPE_ADD;
            } else {
                return TYPE_PHOTO;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mList == null ? 1 : mList.size() + 1;
    }

    private class AddViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public AddViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mAddClickListener != null) {
                mAddClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    private class PhotoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener {
        ImageView iv_photo;
        FrameLayout layout_layer;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            iv_photo = (ImageView) itemView.findViewById(R.id.item_photo_iv);
            layout_layer = (FrameLayout) itemView.findViewById(R.id.layout_layer);

            itemView.setOnLongClickListener(this);
            itemView.setOnClickListener(this);
        }

        void setData(UserPhoto userPhoto) {
            if (userPhoto != null && !TextUtils.isEmpty(userPhoto.getStatus())) {
                if (userPhoto.getStatus().equals("1")) {//已通过
                    layout_layer.setVisibility(View.GONE);
                } else {
                    layout_layer.setVisibility(View.VISIBLE);
                }
            }
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userPhoto.getFileUrlMinimum()).imageView(iv_photo)
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).build());
        }

        @Override
        public void onClick(View view) {
            if (view == itemView && mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(view, getAdapterPosition());
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (view == itemView && mOnLongClickListener != null) {
                mOnLongClickListener.onItemClick(view, getAdapterPosition());
            }
            return false;
        }
    }

    public void setUserPhoto(List<UserPhoto> list) {
        this.mList = list;
        super.notifyDataSetChanged();
    }

    public UserPhoto getUserPhoto(int position) {
        return (mList == null || mList.size() == 0) ? null : mList.get(position);
    }

    public void addUserPhoto(UserPhoto userPhoto) {
        this.mList.add(userPhoto);
        notifyItemInserted(mList.size() - 1);
    }

    public void updateUserPhoto(int positon, UserPhoto userPhoto) {
        this.mList.set(positon, userPhoto);
        notifyItemChanged(positon);
    }

    public List<UserPhoto> getPhotos() {
        return (mList == null || mList.size() == 0) ? null : mList;
    }
}
