package com.popo.video.ui.homepage;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.popo.library.util.ArgumentUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.R;
import com.popo.video.base.BaseFragment;
import com.popo.video.customload.FlingCardListener;
import com.popo.video.customload.SwipeFlingAdapterView;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.model.MatchInfo;
import com.popo.video.data.model.SearchUser;
import com.popo.video.data.model.UserBase;
import com.popo.video.db.DbModle;
import com.popo.video.event.SearchEvent;
import com.popo.video.parcelable.ListParcelable;
import com.popo.video.parcelable.MatchParcelable;
import com.popo.video.parcelable.UserDetailParcelable;
import com.popo.video.ui.detail.UserDetailActivity;
import com.popo.video.ui.homepage.contract.ListContract2;
import com.popo.video.ui.homepage.presenter.ListPresenter2;
import com.popo.video.ui.match.MatchActivity;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 首页列表
 * Created by zhangdroid on 2017/5/31.
 */
public class ListFragment2 extends BaseFragment implements ListContract2.IView, SwipeFlingAdapterView.onFlingListener, View.OnClickListener {
    @BindView(R.id.list_frament_frame)
    SwipeFlingAdapterView frame;
    @BindView(R.id.list_fragment_item_ll)
    LinearLayout ll_frame_item;
    @BindView(R.id.list_fragment_iv_big_sayhello)
    ImageView iv_big_say_hello;
    @BindView(R.id.list_fragment_iv_big_dislike)
    ImageView iv_big_dislike;
    private ListPresenter2 mListPresenter;
    private ListParcelable mListParcelable;
    private boolean isCanLoad=true;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;

    public static ListFragment2 newInstance(ListParcelable parcelable) {
        ListFragment2 listFragment = new ListFragment2();
        listFragment.setArguments(ArgumentUtil.setArgumentBundle(parcelable));
        return listFragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.list_fragment2;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return ll_frame_item;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {
        mListParcelable = (ListParcelable) parcelable;
    }

    @Override
    protected void initViews() {
        mListPresenter = new ListPresenter2(this);
        mIsFirstLoad = true;
        frame.setFlingListener(this);
        frame.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {

            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                SearchUser searchUser = (SearchUser) dataObject;
                if (null != searchUser) {
                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                            new UserDetailParcelable(String.valueOf(searchUser.getUesrId())));
                }
            }
        });
    }

    @Override
    protected void setListeners() {
        iv_big_say_hello.setOnClickListener(this);
        iv_big_dislike.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        mListPresenter.loadRecommendUserList(mListParcelable.type);
//        mListPresenter.refresh();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
    }

    @Override
    public void hideRefresh(int delaySeconds) {
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mListPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowEmpty(false, null, null);
                mListPresenter.refresh();
            }
        });
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getFragmentManager();
    }

    @Override
    public void setAdapter(ListFragmentBaseAdapter adapter, int loadMoreViewId) {
        frame.setAdapter(adapter);
    }

    @Override
    public void showLoadMore() {
    }

    @Override
    public void hideLoadMore() {
    }

    @Override
    public void showNoMore() {
    }

    @Subscribe
    public void onEvent(SearchEvent searchEvent) {
    }

    @Override
    public void removeFirstObjectInAdapter() {
        if (mListPresenter != null) {
            mListPresenter.removeItem(0);
        }
    }

    @Override
    public void onLeftCardExit(Object dataObject) {
    }

    @Override
    public void onRightCardExit(Object dataObject) {
        SearchUser searchUser= (SearchUser) dataObject;
        if(searchUser!=null){
            sayHello(String.valueOf(searchUser.getUesrId()));
        }
    }

    @Override
    public void onAdapterAboutToEmpty(int itemsInAdapter) {
        if (itemsInAdapter == 3) {
            isCanLoad=false;
            mListPresenter.loadMore();
        }else{
            mListPresenter.refresh();
        }
    }

    @Override
    public void onScroll(float scrollProgressPercent) {
        if (frame != null) {
            View selectedView = frame.getSelectedView();
            if (selectedView != null) {
                // 右滑打招呼
                ImageView disLike = (ImageView) selectedView.findViewById(R.id.list_fragment_item_iv_sayhello);
                disLike.setAlpha(scrollProgressPercent > 0 ?( scrollProgressPercent+10) : 0);
                // 左滑不喜欢
                ImageView sayhello = (ImageView) selectedView.findViewById(R.id.list_fragment_item_iv_dislike);
                sayhello.setAlpha(scrollProgressPercent < 0 ?( -scrollProgressPercent+10) : 0);
            }
        }
    }

    private void sayHello(String userId){
        ApiManager.sayHelloMatcher(userId, new IGetDataListener<MatchInfo>() {
            @Override
            public void onResult(MatchInfo matchInfo, boolean isEmpty) {
                if (matchInfo.getIsMatcher().equals("1")){
                    if (matchInfo.getMatcherUser() != null){
                        UserBase matcherUser = matchInfo.getMatcherUser();
                        if (matcherUser != null && !TextUtils.isEmpty(matcherUser.getAccount())){
                            HuanXinUser user = new HuanXinUser(matcherUser.getGuid()+"", matcherUser.getNickName(),
                                    matcherUser.getIconUrlMininum(), matcherUser.getAccount(), "1",
                                    mContext.getString(R.string.match_success), 0, System.currentTimeMillis()+"", 10);
                            DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
                            LaunchHelper.getInstance().launch(mContext, MatchActivity.class,
                                    new MatchParcelable(matcherUser.getGuid(),matcherUser.getAccount(),
                                            matcherUser.getNickName(),matcherUser.getIconUrlMininum()));
                        }
                    }
                }
            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.list_fragment_iv_big_sayhello:
                if (frame != null) {
                    // 打招呼
                    SearchUser searchUser = (SearchUser) frame.getSelectedItem();
                    if (searchUser != null) {
                        sayHello(String.valueOf(searchUser.getUesrId()));
                    }
                    // 右滑显示喜欢
                    View view = frame.getSelectedView();
                    if (view != null) {
                        ImageView ivSayHello = (ImageView) view.findViewById(R.id.list_fragment_item_iv_sayhello);
                        ivSayHello.setAlpha(1f);
                    }
                    try {
                        FlingCardListener flingCardListener = frame.getTopCardListener();
                        if (flingCardListener != null) {
                            flingCardListener.selectRight();
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.list_fragment_iv_big_dislike:
                if (frame != null) {
                    // 左滑
                    View view = frame.getSelectedView();
                    if (view != null) {
                        ImageView ivNext = (ImageView) view.findViewById(R.id.list_fragment_item_iv_dislike);
                        ivNext.setAlpha(1f);
                    }
                    try {
                        FlingCardListener flingCardListener = frame.getTopCardListener();
                        if (flingCardListener != null) {
                            flingCardListener.selectLeft();
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }
}
