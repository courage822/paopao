package com.popo.video.ui.pay.activity;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.library.net.NetUtil;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.common.YeMeiPopopUtil;
import com.popo.video.event.YeMeiMessageEvent;
import com.popo.video.ui.homepage.HomepageAdapter;
import com.popo.video.ui.pay.adapter.PayAdapter;
import com.popo.video.ui.pay.contract.RechargeContract;
import com.popo.video.ui.pay.fragment.IncomeDetailFragment;
import com.popo.video.ui.pay.presenter.RechargePresenter;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/9/2.
 */

public class MyWalletActivity extends BaseFragmentActivity implements View.OnClickListener,RechargeContract.IView {
    @BindView(R.id.mywallet_activity_vp)
    ViewPager mViewPager;
    @BindView(R.id.mywallet_activity_ll_root)
    LinearLayout mRoot;
    @BindView(R.id.mywallet_activity_tab)
    TabLayout mTab;
    @BindView(R.id.mywallet_activity_tv_explain)
    TextView tv_explain;
    @BindView(R.id.mywallet_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.mywallet_activity_tv_money)
    TextView tv_money;
    RechargePresenter mPresenter;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_mywallet;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        HomepageAdapter homepageAdapter = new HomepageAdapter(getSupportFragmentManager());
        List<Fragment> fragmentList = new ArrayList<>();
        List<String>  tabList = Arrays.asList(getResources().getString(R.string.growth_record));
        fragmentList.add(new IncomeDetailFragment());
        homepageAdapter.setData(fragmentList, tabList);
        mViewPager.setAdapter(homepageAdapter);
        mTab.setupWithViewPager(mViewPager);
        mPresenter=new RechargePresenter(this);
    }

    @Override
    protected void setListeners() {
        tv_explain.setOnClickListener(this);
        rl_back.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mywallet_activity_tv_explain:
                break;
            case R.id.mywallet_activity_rl_back:
                finish();
                break;
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mRoot, msg);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void showNetworkError() {

    }

    @Override
    public void setAdapter(PagerAdapter pagerAdapter) {

    }

    @Override
    public FragmentManager getManager() {
        return null;
    }

    @Override
    public void setAdapter(PayAdapter payAdapter) {

    }

    @Override
    public void setTextDetail(String detial) {

    }

    @Override
    public void getKeyNum(String num) {

    }

    @Override
    public void getNowMoney(String  money) {
        tv_money.setText( money);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getMyInfo();
    }
    @Subscribe
    public void onEvent(YeMeiMessageEvent evet){
        Message msg=new Message();
        msg.obj=evet;
        yeMeiHandler.sendMessage(msg);

    }
    private Handler yeMeiHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            YeMeiMessageEvent obj = (YeMeiMessageEvent) msg.obj;
            if(obj!=null){
                YeMeiPopopUtil.getInstance().showYeMei(MyWalletActivity.this,mViewPager,obj,isFinishing());
            }
        }
    };
}
