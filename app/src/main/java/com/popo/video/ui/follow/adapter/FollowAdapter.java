package com.popo.video.ui.follow.adapter;


import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.UserBase;
import com.popo.video.event.UpdataFollowUser;
import com.popo.library.adapter.CommonRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Administrator on 2017/6/14.
 */

public class FollowAdapter extends CommonRecyclerViewAdapter<UserBase> {
    private FragmentManager mFragmentManager;

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
    }

    public FollowAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    public void convert(final UserBase userBase, final int position, RecyclerViewHolder holder) {
        ImageView iv_avatar = (ImageView) holder.getView(R.id.follow_item_iv_avatar);
        TextView tv_sayhello = (TextView) holder.getView(R.id.follow_item_tv_sayhello);
        if (userBase != null) {
            holder.setText(R.id.follow_item_tv_nickname, userBase.getNickName());
            holder.setText(R.id.follow_item_tv_age, userBase.getAge() + mContext.getString(R.string.edit_info_years_old));

            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userBase.getIconUrlMiddle())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_avatar).build());
            String status = userBase.getStatus();
            if (status.equals("1")) {
                holder.setText(R.id.follow_item_tv_state, mContext.getString(R.string.edit_info_status));
            } else {
                holder.setText(R.id.follow_item_tv_state, mContext.getString(R.string.not_online));
            }
        }
        tv_sayhello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unFollowUser(String.valueOf(userBase.getGuid()));
            }
        });
    }

    public void unFollowUser(final String userId) {
        ApiManager.unFollow(userId, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                EventBus.getDefault().post(new UpdataFollowUser(userId));
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    ;

}
