package com.popo.video.ui.photo;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.popo.library.util.Utils;

import java.util.List;

import uk.co.senab.photoview.PhotoView;

/**
 * 大图适配器
 * Created by zhangdroid on 2017/6/3.
 */
public class BigPhotoAdapter extends PagerAdapter {
    private List<PhotoView> mPhotoViews;

    public BigPhotoAdapter(List<PhotoView> list) {
        this.mPhotoViews = list;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(mPhotoViews.get(position));
        return mPhotoViews.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((PhotoView) object);
    }

    @Override
    public int getCount() {
        return Utils.isListEmpty(mPhotoViews) ? 0 : mPhotoViews.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

}
