package com.popo.video.ui.pay.presenter;

import android.content.Context;

import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.IncomeRecord;
import com.popo.video.data.model.IncomeRecordList;
import com.popo.video.data.model.WithdrawRecord;
import com.popo.video.data.model.WithdrawRecordList;
import com.popo.library.adapter.wrapper.OnLoadMoreListener;
import com.popo.library.util.Utils;
import com.popo.video.ui.pay.contract.IncomeWithdrawalRecordContract;
import com.popo.video.ui.personalcenter.adapter.IncomeRecordAdapter;
import com.popo.video.ui.personalcenter.adapter.WithdrawRecordAdapter;

import java.util.List;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public class IncomeWithdrawalRecordPresenter implements IncomeWithdrawalRecordContract.IPresenter {
    private IncomeWithdrawalRecordContract.IView mIncomeWithdrawal;
    private Context mContext;
    private int pageNum1=1;
    private int pageNum2=1;
    private String pageSize1="20";
    private String pageSize2="20";
    private IncomeRecordAdapter mIncomeRecordAdapter;
    private WithdrawRecordAdapter mWithdrawRecordAdapter;
    public IncomeWithdrawalRecordPresenter(IncomeWithdrawalRecordContract.IView mIncomeWithdrawal) {
        this.mIncomeWithdrawal = mIncomeWithdrawal;
        mContext=mIncomeWithdrawal.obtainContext();
        mIncomeRecordAdapter = new IncomeRecordAdapter(mContext, R.layout.item_income_record);
        mIncomeWithdrawal.setAdapter(mIncomeRecordAdapter);
        mWithdrawRecordAdapter = new WithdrawRecordAdapter(mContext, R.layout.item_withdraw_record);
        mIncomeWithdrawal.setAdapter(mWithdrawRecordAdapter);
        mIncomeWithdrawal.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                loadMore2();
            }
        });
    }

    @Override
    public void start() {

    }

    @Override
    public void incomeHistory() {
        ApiManager.getIncomeRecord(pageNum1, pageSize1, new IGetDataListener<IncomeRecordList>() {
            public void onResult(IncomeRecordList recordList, boolean isEmpty) {
                if (isEmpty) {
                    if (pageNum1 == 1) {
                        mIncomeWithdrawal.toggleShowEmpty(true, null);
                    } else if (pageNum1 > 1) {
                        mIncomeWithdrawal.showNoMore();
                    }
                } else {
                    if (null != recordList) {
                        List<IncomeRecord> listRecord = recordList.getIncomeFlowList();
                        if (listRecord!=null&&listRecord.size()>0) {
                            if (pageNum1 == 1) {
                                mIncomeRecordAdapter.bind(listRecord);
                            } else if (pageNum1 > 1) {
                                mIncomeRecordAdapter.appendToList(listRecord);
                            }
                            mIncomeWithdrawal.hideLoadMore();
                        }
                    }
                }
                mIncomeWithdrawal.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mIncomeWithdrawal.hideRefresh(1);
                if (!isNetworkError) {
                    mIncomeWithdrawal.toggleShowError(true, msg);
                }
            }
        });
    }

    @Override
    public void withdrawalHistroy() {
        ApiManager.getWithdrawRecord(pageNum2, pageSize2, new IGetDataListener<WithdrawRecordList>() {

            @Override
            public void onResult(WithdrawRecordList recordList, boolean isEmpty) {
                if (isEmpty) {
                    if (pageNum2 == 1) {
                        mIncomeWithdrawal.toggleShowEmpty(true, null);
                    } else if (pageNum2 > 1) {
                        mIncomeWithdrawal.showNoMore();
                    }
                } else {
                    if (null != recordList) {
                        List<WithdrawRecord> list = recordList.getListRecord();
                        if (!Utils.isListEmpty(list)) {
                            if (pageNum2 == 1) {
                                mWithdrawRecordAdapter.bind(list);
                            } else if (pageNum2 > 1) {
                               mWithdrawRecordAdapter.appendToList(list);
                            }
                            mIncomeWithdrawal.hideLoadMore();
                        }
                    }
                }
                mIncomeWithdrawal.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mIncomeWithdrawal.hideRefresh(1);
                if (!isNetworkError) {
                    mIncomeWithdrawal.toggleShowError(true, msg);
                }
            }
        });
    }

    @Override
    public void refresh1() {
        pageNum1 = 1;
        incomeHistory();
    }

    @Override
    public void refresh2() {
        pageNum2 = 1;
        withdrawalHistroy();
    }

    @Override
    public void loadMore1() {
        mIncomeWithdrawal.showLoadMore();
        pageNum1++;
        incomeHistory();
    }

    @Override
    public void loadMore2() {
        mIncomeWithdrawal.showLoadMore();
        pageNum2++;
        withdrawalHistroy();
    }


}
