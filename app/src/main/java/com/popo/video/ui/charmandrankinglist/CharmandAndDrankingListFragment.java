package com.popo.video.ui.charmandrankinglist;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/10/25.
 */

public class CharmandAndDrankingListFragment extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.charmand_and_rangkinglist_iv_ranking)
    ImageView iv_ranking;
    @BindView(R.id.charmand_and_rangkinglist_iv_charmand)
    ImageView iv_charmand;
    @BindView(R.id.charmand_and_rangkinglist_tv_ranking)
    TextView tv_ranking;
    @BindView(R.id.charmand_and_rangkinglist_tv_charmand)
    TextView tv_charmand;
    @BindView(R.id.charmand_and_rangkinglist_framelayout_container)
    FrameLayout fl_container;
    private List<Fragment> list_fragment=new ArrayList<>();
    private int current = 0;
    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_charmand_and_rankinglist;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        CharmandFragment charmandFragment=new CharmandFragment();
        RrankingListFragment rrankingListFragment=new RrankingListFragment();
        list_fragment.add(charmandFragment);
        list_fragment.add(rrankingListFragment);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.charmand_and_rangkinglist_framelayout_container,charmandFragment).commit();
    }

    @Override
    protected void setListeners() {
        tv_ranking.setOnClickListener(this);
        tv_charmand.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }
    private void switchFragment(int position) {
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        Fragment targetFragment = list_fragment.get(position);
        Fragment currentFragment = list_fragment.get(current);

        if (targetFragment.isAdded()) {
            transaction.show(targetFragment).hide(currentFragment).commit();
        } else {
            transaction.add(R.id.charmand_and_rangkinglist_framelayout_container, targetFragment).hide(currentFragment).commit();
        }
        current = position;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.charmand_and_rangkinglist_tv_ranking:
                if (current != 0){
                    iv_charmand.setVisibility(View.GONE);
                    iv_ranking.setVisibility(View.VISIBLE);
                    switchFragment(0);
                }
                break;
            case R.id.charmand_and_rangkinglist_tv_charmand:
                if (current != 1){
                    iv_ranking.setVisibility(View.GONE);
                    iv_charmand.setVisibility(View.VISIBLE);
                    switchFragment(1);
                }
                break;
        }
    }
}
