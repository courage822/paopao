package com.popo.video.ui.chat;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.adapter.provider.ItemViewProvider;
import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.util.DateTimeUtil;
import com.popo.library.util.DeviceUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.SharedPreferenceUtil;
import com.popo.library.widget.ChatImageView;
import com.popo.video.R;
import com.popo.video.common.GiftGivingDialog;
import com.popo.video.common.GivePrivateMessageGiftDialog;
import com.popo.video.common.Util;
import com.popo.video.common.VideoHelper;
import com.popo.video.data.model.NettyMessage;
import com.popo.video.data.model.PicInfo;
import com.popo.video.data.model.QaMessage;
import com.popo.video.data.model.QaMsg;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.SwitchPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.event.QAEvent;
import com.popo.video.event.SeeDetailEvent;
import com.popo.video.parcelable.PrivateMessageParcelable;
import com.popo.video.parcelable.ShortPlayParcelable;
import com.popo.video.parcelable.VideoInviteParcelable;
import com.popo.video.ui.detail.BlurTransformation;
import com.popo.video.ui.detail.ShortPlayActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Map;

/**
 * 聊天文字消息（接收方）
 * Created by zhangdroid on 2017/6/29.
 */
public class TextMessageLeftProvider implements ItemViewProvider<EMMessage> {
    private Context mContext;
    private String mAvatarUrl;
    private List<EMMessage> messageList;
    private CountDownTimer countDownTimer1;
    private CountDownTimer countDownTimer2;
    private FragmentManager fragmentManager;
    private long lastClickTime = 0;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                case 2:
                    if (countDownTimer1 != null) {
                        countDownTimer1.cancel();
                        countDownTimer1.onFinish();
                        countDownTimer1 = null;
                    }
                    if (countDownTimer2 != null) {
                        countDownTimer2.cancel();
                        countDownTimer2.onFinish();
                        countDownTimer1 = null;
                    }
                    break;
            }
        }
    };

    public TextMessageLeftProvider(Context context, String url, FragmentManager fragmentManager, List<EMMessage> messageList) {
        this.mContext = context;
        this.mAvatarUrl = url;
        this.fragmentManager = fragmentManager;
        this.messageList = messageList;
    }

    @Override
    public int getItemViewLayoutResId() {
        return R.layout.item_chat_text_left;
    }

    @Override
    public boolean isViewType(EMMessage item, int position) {
        return (item.getType() == EMMessage.Type.TXT && item.direct() == EMMessage.Direct.RECEIVE);
    }

    @Override
    public void convert(final EMMessage emMessage, int position, final RecyclerViewHolder holder) {
        // 头像
        ImageView ivAvatar = (ImageView) holder.getView(R.id.item_chat_text_avatar_left);
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                .error(Util.getDefaultImageCircle()).url(mAvatarUrl).imageView(ivAvatar).build());
        //视频、语音邀请类型
        final TextView tv_time = (TextView) holder.getView(R.id.item_chat_send_video_tv_countdown_left);
        final ImageView iv_voice_or_video = (ImageView) holder.getView(R.id.item_chat_send_video_iv_left);
        final TextView tv_price = (TextView) holder.getView(R.id.item_chat_send_video_tv_price_left);
        final TextView tv_charge = (TextView) holder.getView(R.id.item_chat_send_video_tv_charge_left);
        final TextView tv_refuse = (TextView) holder.getView(R.id.item_chat_send_video_tv_refuse_left);
        final TextView tv_callback = (TextView) holder.getView(R.id.item_chat_send_video_tv_callback_left);
        final TextView tv_accept = (TextView) holder.getView(R.id.item_chat_send_video_tv_accept_left);
        final TextView tv_video_or_voice = (TextView) holder.getView(R.id.item_chat_send_video_tv_left);


        //私密消息类型，加锁状态
        final ImageView iv_private_message_type = (ImageView) holder.getView(R.id.item_chat_private_message_iv_left);
        final ImageView iv_send_text = (ImageView) holder.getView(R.id.iv_send_text);
        final TextView tv_private_message_type = (TextView) holder.getView(R.id.item_chat_private_message_tv_left);
        final TextView tv_send_type = (TextView) holder.getView(R.id.tv_send_type);
        final TextView tv_send_text_click = (TextView) holder.getView(R.id.tv_send_text_click);
        final TextView tv_private_callback = (TextView) holder.getView(R.id.item_chat_private_message_tv_callback_left);
        //私密消息类型，解锁状态
        //图片
        final ChatImageView chatImageView = (ChatImageView) holder.getView(R.id.item_chat_image_left);
        final RelativeLayout item_image_private = (RelativeLayout) holder.getView(R.id.item_chat_private_image_left);
        //视频
        final ImageView iv_unlock_video = (ImageView) holder.getView(R.id.private_video_lock_iv_left);
        final RelativeLayout item_video_unlock = (RelativeLayout) holder.getView(R.id.item_private_video_unlock_left);
        //语音
        final ImageView iv_voice_message = (ImageView) holder.getView(R.id.item_chat_voice_left);
        final TextView tv_voice_duration = (TextView) holder.getView(R.id.item_chat_voice_left_duration);
        final LinearLayout item_chat_voice_left_ll = (LinearLayout) holder.getView(R.id.item_chat_voice_left_ll);
        final View bottom_view = holder.getView(R.id.item_chat_view_bottom);

        LinearLayout ll_send_gifts = (LinearLayout) holder.getView(R.id.item_chat_ll_left_send_gifts);
        ImageView iv_gifts = (ImageView) holder.getView(R.id.item_chat_iv_left_gifts);
        TextView tv_male = (TextView) holder.getView(R.id.item_chat_tv_left_meinv_or_shuaige);
        TextView tv_gift_num = (TextView) holder.getView(R.id.item_chat_tv_left_gifts_num);
        ivAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 发送事件，查看用户详情
                EventBus.getDefault().post(new SeeDetailEvent());
            }
        });
        if (null != emMessage) {
            if (position > 0) {
                long msgTime = messageList.get(position - 1).getMsgTime();
                if ((emMessage.getMsgTime() - msgTime) / (1000 * 60) > 5) {
                    holder.getView(R.id.item_chat_text_time_left).setVisibility(View.VISIBLE);
                    if (!TextUtils.isEmpty(UserPreference.getCountryId())
                            && UserPreference.getCountryId().equals("97")) {
                        String s = DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime());
                        if (s.length() > 11) {
                            String substring = s.substring(0, 4);
                            String substring1 = s.substring(5, 7);
                            String substring2 = s.substring(8, 10);
                            String substring3 = s.substring(11);
                            holder.setText(R.id.item_chat_text_time_left, substring3 + " " + substring1
                                    + "/" + substring2 + "/" + substring);
                        }
                    } else {
                        String s = DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime());
                        if (s.length() > 11) {
                            String substring = s.substring(0, 4);
                            String substring1 = s.substring(5, 7);
                            String substring2 = s.substring(8, 10);
                            String substring3 = s.substring(11);
                            holder.setText(R.id.item_chat_text_time_left, substring3 + " " + substring2
                                    + "/" + substring1 + "/" + substring);
                        }
                    }

                } else {
                    holder.getView(R.id.item_chat_text_time_left).setVisibility(View.GONE);
                }
            } else {
                holder.getView(R.id.item_chat_text_time_left).setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(UserPreference.getCountryId())
                        && UserPreference.getCountryId().equals("97")) {
                    String s = DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime());
                    if (s.length() > 11) {
                        String substring = s.substring(0, 4);
                        String substring1 = s.substring(5, 7);
                        String substring2 = s.substring(8, 10);
                        String substring3 = s.substring(11);
                        holder.setText(R.id.item_chat_text_time_left, substring3 + " " + substring1
                                + "/" + substring2 + "/" + substring);
                    }
                } else {
                    String s = DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime());
                    if (s.length() > 11) {
                        String substring = s.substring(0, 4);
                        String substring1 = s.substring(5, 7);
                        String substring2 = s.substring(8, 10);
                        String substring3 = s.substring(11);
                        holder.setText(R.id.item_chat_text_time_left, substring3 + " " + substring2
                                + "/" + substring1 + "/" + substring);
                    }
                }
            }
            final EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessage.getBody();
            if (null != emTextMessageBody) {
                //礼物消息，用于Android与android之间的通讯，与ios的通讯按照拓展字段extendtype == 5；
                if (emTextMessageBody.getMessage().contains("{") && emTextMessageBody.getMessage().contains("}")) {
                    holder.setVisibility(R.id.item_chat_text_left, false);//文字类型
                    holder.setVisibility(R.id.item_chat_voice_left_ll, false);//语音类型
                    holder.setVisibility(R.id.item_chat_private_message_left, false);//加锁的私密消息
                    holder.setVisibility(R.id.item_private_video_unlock_left, false);//解锁私密视频
                    holder.setVisibility(R.id.item_chat_private_image_left, false);//解锁私密图片
                    holder.setVisibility(R.id.item_chat_send_video_left, false);//隐藏视频邀请和私密消息控件
                    ll_send_gifts.setVisibility(View.VISIBLE);
                    PicInfo picInfo = new Gson().fromJson(emTextMessageBody.getMessage(), PicInfo.class);
                    if (picInfo != null) {
                        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(picInfo.getPicIcon())
                                .placeHolder(0).error(0).imageView(iv_gifts).build());
                        if (UserPreference.isMale()) {
                            tv_male.setText(mContext.getString(R.string.hi_boy));
                        } else {
                            tv_male.setText(mContext.getString(R.string.hi_girl));
                        }
                        tv_gift_num.setText(picInfo.getPicNum() + mContext.getString(R.string.one) + picInfo.getPicName());
                    }

                } else {
                    ll_send_gifts.setVisibility(View.GONE);
                    String msgContent = "";
                    msgContent = emMessage.getStringAttribute("msg", "");
                    Log.e("msgContent", msgContent);
                    if (!TextUtils.isEmpty(msgContent)) {
                        final NettyMessage nettyMessage = setMsgContent(msgContent);
                        if (nettyMessage != null) {
                            final String userName = nettyMessage.getSendUserName();
                            final String userPic = nettyMessage.getSendUserIcon();
                            final String userAccount = nettyMessage.getSendUserAccount();
                            final String userGuid = String.valueOf(nettyMessage.getSendUserId());
                            final int notice = nettyMessage.getExtendType();
                            final int callTag = nettyMessage.getHostCallTag();
                            Map<String, String> ext = nettyMessage.getExt();
                            if (ext != null) {
                                String hostPrice = ext.get("hostPrice");
                                if (!TextUtils.isEmpty(hostPrice)) {
                                    tv_price.setText(hostPrice + mContext.getString(R.string.invite_video_unit));
                                }
                            }
                            if (notice == 9) {//QA消息
                                String qaContent = nettyMessage.getQaContent();
                                if (!TextUtils.isEmpty(qaContent)) {
                                    QaMessage qaMessage = new Gson().fromJson(qaContent, QaMessage.class);
                                    if (qaMessage != null) {
                                        QaMsg qaMsg = new QaMsg(emMessage.getMsgId(), emMessage.getFrom(), 1);
                                        if (qaMsg != null) {
                                            DbModle.getInstance().getUserAccountDao().addQaMsg(qaMsg);
                                            int isRead = DbModle.getInstance().getUserAccountDao().getIsRead(emMessage.getMsgId());
                                            if (isRead == 1) {
                                                EventBus.getDefault().post(new QAEvent(qaMessage, emMessage.getFrom(), userGuid, qaMsg));
                                            }
                                        }
                                    }
                                }
                            }
                            if (notice == 5) {//礼物消息，用于同ios通讯
                                holder.setVisibility(R.id.item_chat_text_left, false);//文字类型
                                holder.setVisibility(R.id.item_chat_voice_left_ll, false);//语音类型
                                holder.setVisibility(R.id.item_chat_private_message_left, false);//加锁的私密消息
                                holder.setVisibility(R.id.item_private_video_unlock_left, false);//解锁私密视频
                                holder.setVisibility(R.id.item_chat_private_image_left, false);//解锁私密图片
                                holder.setVisibility(R.id.item_chat_send_video_left, false);//隐藏视频邀请和私密消息控件
                                ll_send_gifts.setVisibility(View.VISIBLE);
                                PicInfo picInfo = DbModle.getInstance().getUserAccountDao().getPicInfo(nettyMessage.getGiftId());
                                if (picInfo != null) {
                                    ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(picInfo.getPicIcon())
                                            .placeHolder(0).error(0).imageView(iv_gifts).build());
                                    if (UserPreference.isMale()) {
                                        tv_male.setText(mContext.getString(R.string.hi_boy));
                                    } else {
                                        tv_male.setText(mContext.getString(R.string.hi_girl));
                                    }
                                    tv_gift_num.setText(nettyMessage.getContent() + mContext.getString(R.string.one) + picInfo.getPicName());
                                }
                            } else if (notice == 2 || notice == 4) {
                                holder.setVisibility(R.id.item_chat_text_left, false);
                                holder.setVisibility(R.id.item_chat_private_message_left, false);
                                holder.setVisibility(R.id.item_private_video_unlock_left, false);//解锁私密视频
                                holder.setVisibility(R.id.item_chat_private_image_left, false);//解锁私密图片
                                holder.setVisibility(R.id.item_chat_send_video_left, true);
                                if ((System.currentTimeMillis() - emMessage.getMsgTime()) > 30000) {
                                    tv_refuse.setVisibility(View.GONE);
                                    tv_accept.setVisibility(View.GONE);
                                    tv_callback.setVisibility(View.VISIBLE);
                                    tv_time.setText(mContext.getString(R.string.invalable));
                                } else {
                                    if ((System.currentTimeMillis() - emMessage.getMsgTime()) <= 2000) {
                                        countDownTimer1 = new CountDownTimer(30000, 1000) {
                                            @Override
                                            public void onTick(long millisUntilFinished) {
                                                tv_time.setText(millisUntilFinished / 1000 + "s");
                                                tv_refuse.setVisibility(View.VISIBLE);
                                                tv_accept.setVisibility(View.VISIBLE);
                                                tv_callback.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onFinish() {
                                                tv_refuse.setVisibility(View.GONE);
                                                tv_accept.setVisibility(View.GONE);
                                                tv_callback.setVisibility(View.VISIBLE);
                                                tv_time.setText(mContext.getString(R.string.invalable));
                                            }
                                        };
                                        countDownTimer1.start();
                                        tv_refuse.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                tv_refuse.setVisibility(View.GONE);
                                                handler.sendEmptyMessage(1);
                                                tv_accept.setVisibility(View.GONE);
                                                tv_callback.setVisibility(View.VISIBLE);
                                                tv_time.setText(mContext.getString(R.string.invalable));
                                                countDownTimer1.cancel();
                                                countDownTimer1.onFinish();
                                            }
                                        });
                                    } else if ((30000 - (System.currentTimeMillis() - emMessage.getMsgTime())) >= 0) {
                                        countDownTimer2 = new CountDownTimer((30000 - (System.currentTimeMillis() - emMessage.getMsgTime())), 1000) {
                                            @Override
                                            public void onTick(long millisUntilFinished) {
                                                tv_time.setText(millisUntilFinished / 1000 + "s");
                                                tv_refuse.setVisibility(View.VISIBLE);
                                                tv_accept.setVisibility(View.VISIBLE);
                                                tv_callback.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onFinish() {
                                                tv_refuse.setVisibility(View.GONE);
                                                tv_accept.setVisibility(View.GONE);
                                                tv_callback.setVisibility(View.VISIBLE);
                                                tv_time.setText(mContext.getString(R.string.invalable));
                                            }
                                        };
                                        countDownTimer2.start();
                                        tv_refuse.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                tv_refuse.setVisibility(View.GONE);
                                                handler.sendEmptyMessage(2);
                                                tv_accept.setVisibility(View.GONE);
                                                tv_callback.setVisibility(View.VISIBLE);
                                                tv_time.setText(mContext.getString(R.string.invalable));
                                                countDownTimer2.cancel();
                                                countDownTimer2.onFinish();
                                            }
                                        });
                                    }
                                }
                                if (SwitchPreference.getAllPay() == 1) {
                                    tv_charge.setVisibility(View.VISIBLE);
                                    tv_price.setVisibility(View.VISIBLE);
                                } else {
                                    tv_charge.setVisibility(View.GONE);
                                    tv_price.setVisibility(View.GONE);
                                }
                                tv_accept.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (System.currentTimeMillis() - lastClickTime > 1000) {
                                            lastClickTime = System.currentTimeMillis();
                                            if (callTag == 0 && notice == 4) {
//                                            VideoHelper.immediatelyVideo(UserPreference.getId() + userGuid, userAccount, mAvatarUrl, userGuid, 1, mContext);
                                                VideoHelper.immediatelyVideo(userGuid + UserPreference.getId(), userAccount, userGuid, mAvatarUrl, 1, mContext, "2");
                                                tv_video_or_voice.setText(mContext.getString(R.string.user_detail_voice));
                                                iv_voice_or_video.setImageResource(R.drawable.icon_voice_invite);
                                            } else if (callTag == 0 && notice == 2) {
//                                            VideoHelper.immediatelyVideo(UserPreference.getId() + userGuid, userAccount, mAvatarUrl, userGuid, 0, mContext);
                                                VideoHelper.immediatelyVideo(userGuid + UserPreference.getId(), userAccount, userGuid, mAvatarUrl, 0, mContext, "2");
                                                tv_video_or_voice.setText(mContext.getString(R.string.video_video));
                                                iv_voice_or_video.setImageResource(R.drawable.icon_vido_invite);
                                            }
                                            if (callTag == 1 && notice == 4) {
                                                setAccetpOnclick(userName, userPic, userAccount, userGuid, 1);
                                                tv_video_or_voice.setText(mContext.getString(R.string.user_detail_voice));
                                                iv_voice_or_video.setImageResource(R.drawable.icon_voice_invite);
                                            } else if (callTag == 1 && notice == 2) {
                                                setAccetpOnclick(userName, userPic, userAccount, userGuid, 0);
                                                tv_video_or_voice.setText(mContext.getString(R.string.video_video));
                                                iv_voice_or_video.setImageResource(R.drawable.icon_vido_invite);
                                            }
                                        }
                                    }
                                });
                                tv_callback.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (System.currentTimeMillis() - lastClickTime > 1000) {
                                            lastClickTime = System.currentTimeMillis();
                                            if (notice == 4) {
                                                setAccetpOnclick(userName, userPic, userAccount, userGuid, 1);
                                                tv_video_or_voice.setText(mContext.getString(R.string.user_detail_voice));
                                                iv_voice_or_video.setImageResource(R.drawable.icon_voice_invite);
                                            } else if (notice == 2) {
                                                setAccetpOnclick(userName, userPic, userAccount, userGuid, 0);
                                                tv_video_or_voice.setText(mContext.getString(R.string.video_video));
                                                iv_voice_or_video.setImageResource(R.drawable.icon_vido_invite);
                                            }
                                        }
                                    }
                                });
                                if (notice == 4) {
                                    tv_video_or_voice.setText(mContext.getString(R.string.user_detail_voice));
                                    iv_voice_or_video.setImageResource(R.drawable.icon_voice_invite);
                                } else {
                                    tv_video_or_voice.setText(mContext.getString(R.string.video_video));
                                    iv_voice_or_video.setImageResource(R.drawable.icon_vido_invite);
                                }
                            } else if (notice == 3) {
                                //文字、语音类私密消息，展示并索要礼物
                                if (nettyMessage.getBaseType() == 1 || nettyMessage.getBaseType() == 2) {//文字、语音类
                                    holder.setVisibility(R.id.item_chat_text_left, false);//文字类型
                                    holder.setVisibility(R.id.item_chat_voice_left_ll, false);//语音类型
                                    holder.setVisibility(R.id.item_send_text_left, true);//文字、语音类型为赠送礼物
                                    holder.setVisibility(R.id.item_chat_private_message_left, false);//加锁的私密消息
                                    holder.setVisibility(R.id.item_private_video_unlock_left, false);//解锁私密视频
                                    holder.setVisibility(R.id.item_chat_private_image_left, false);//解锁私密图片
                                    holder.setVisibility(R.id.item_chat_send_video_left, false);//隐藏视频邀请和私密消息控件
                                    holder.setText(R.id.tv_send_type, mContext.getString(R.string.one_one) + DbModle.getInstance().getUserAccountDao().getPicInfo(nettyMessage.getGiftId()).getPicName());
                                    ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage())
                                            .url(DbModle.getInstance().getUserAccountDao().getPicInfo(nettyMessage.getGiftId()).getPicIcon()).imageView(iv_send_text).build());
                                    tv_send_text_click.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            GiftGivingDialog dialog = new GiftGivingDialog();
                                            dialog.givePrivateMessageGiftShow(mContext, nettyMessage.getMsgId(), nettyMessage.getGiftId(),
                                                    String.valueOf(nettyMessage.getSendUserId()),
                                                    nettyMessage.getSendUserAccount(), nettyMessage.getSendUserName(),
                                                    nettyMessage.getSendUserIcon(), nettyMessage.getBaseType());
                                        }
                                    });

                                } else {//图片、视频类
                                    String stringValue = SharedPreferenceUtil.getStringValue(mContext, nettyMessage.getMsgId(), nettyMessage.getMsgId(), null);
                                    if (stringValue != null) {
                                        if (nettyMessage.getBaseType() == 3) {//图片类
                                            holder.setVisibility(R.id.item_send_text_left, false);//文字、语音类型为赠送礼物
                                            holder.setVisibility(R.id.item_chat_text_left, false);//文字类型
                                            holder.setVisibility(R.id.item_chat_voice_left_ll, false);//语音类型
                                            holder.setVisibility(R.id.item_chat_private_message_left, false);//加锁的私密消息
                                            holder.setVisibility(R.id.item_private_video_unlock_left, false);//解锁私密视频
                                            holder.setVisibility(R.id.item_chat_private_image_left, true);//解锁私密图片
                                            holder.setVisibility(R.id.item_chat_send_video_left, false);//隐藏视频邀请和私密消息控件

                                            int width = DeviceUtil.getScreenWidth(mContext) / 3;
                                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, (int) ((4 / 3.0f) * width));
                                            chatImageView.setLayoutParams(layoutParams);
                                            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage())
                                                    .url(nettyMessage.getUrl()).imageView(chatImageView).build());


                                            item_image_private.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    LaunchHelper.getInstance().launch(mContext, VideoPlayerActivity.class,
                                                            new PrivateMessageParcelable(nettyMessage.getBaseType(),
                                                                    nettyMessage.getContent(), nettyMessage.getUrl(),
                                                                    nettyMessage.getAudioSeconds()));
                                                }
                                            });

                                        }
                                        if (nettyMessage.getBaseType() == 4) {//短视频
                                            holder.setVisibility(R.id.item_send_text_left, false);//文字、语音类型为赠送礼物
                                            holder.setVisibility(R.id.item_chat_text_left, false);//文字类型
                                            holder.setVisibility(R.id.item_chat_voice_left_ll, false);//语音类型
                                            holder.setVisibility(R.id.item_chat_private_message_left, false);//加锁的私密消息
                                            holder.setVisibility(R.id.item_private_video_unlock_left, true);//解锁私密视频
                                            holder.setVisibility(R.id.item_chat_private_image_left, false);//解锁私密图片
                                            holder.setVisibility(R.id.item_chat_send_video_left, false);//隐藏视频邀请和私密消息控件
                                            item_video_unlock.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    LaunchHelper.getInstance().launch(mContext, ShortPlayActivity.class, new ShortPlayParcelable(nettyMessage.getUrl(), nettyMessage.getThumbnail()));
                                                }
                                            });
                                        }
                                    } else {
                                        if (nettyMessage.getBaseType() == 3) {
                                            holder.setVisibility(R.id.item_send_text_left, false);//文字、语音类型为赠送礼物
                                            holder.setVisibility(R.id.item_chat_text_left, false);//文字类型
                                            holder.setVisibility(R.id.item_chat_voice_left_ll, false);//语音类型
                                            holder.setVisibility(R.id.item_chat_private_message_left, false);//加锁的私密消息
                                            holder.setVisibility(R.id.item_private_video_unlock_left, false);//解锁私密视频
                                            holder.setVisibility(R.id.item_chat_private_image_left, true);//解锁私密图片
                                            holder.setVisibility(R.id.item_chat_send_video_left, false);//隐藏视频邀请和私密消息控件

                                            int width = DeviceUtil.getScreenWidth(mContext) / 3;
                                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, (int) ((4 / 3.0f) * width));
                                            chatImageView.setLayoutParams(layoutParams);
                                            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage())
                                                    .url(nettyMessage.getUrl()).transform(new BlurTransformation(mContext)).imageView(chatImageView).build());

                                            item_image_private.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    GivePrivateMessageGiftDialog dialog = new GivePrivateMessageGiftDialog();
                                                    dialog.givePrivateMessageGiftShow(mContext, nettyMessage.getMsgId(), nettyMessage.getGiftId(),
                                                            String.valueOf(nettyMessage.getSendUserId()),
                                                            nettyMessage.getSendUserAccount(), nettyMessage.getSendUserName(),
                                                            nettyMessage.getSendUserIcon(), nettyMessage.getBaseType());
                                                }
                                            });
                                        }
                                        if (nettyMessage.getBaseType() == 4) {
                                            holder.setVisibility(R.id.item_send_text_left, false);//文字、语音类型为赠送礼物
                                            holder.setVisibility(R.id.item_chat_text_left, false);//文字类型
                                            holder.setVisibility(R.id.item_chat_voice_left_ll, false);//语音类型
                                            holder.setVisibility(R.id.item_chat_private_message_left, true);//加锁的私密消息
                                            holder.setVisibility(R.id.item_private_video_unlock_left, false);//解锁私密视频
                                            holder.setVisibility(R.id.item_chat_private_image_left, false);//解锁私密图片
                                            holder.setVisibility(R.id.item_chat_send_video_left, false);//隐藏视频邀请和私密消息控件

                                            iv_private_message_type.setImageResource(R.drawable.icon_private_video);
                                            tv_private_message_type.setText(mContext.getString(R.string.video_video));
                                            tv_private_callback.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    GivePrivateMessageGiftDialog dialog = new GivePrivateMessageGiftDialog();
                                                    dialog.givePrivateMessageGiftShow(mContext, nettyMessage.getMsgId(), nettyMessage.getGiftId(),
                                                            String.valueOf(nettyMessage.getSendUserId()),
                                                            nettyMessage.getSendUserAccount(), nettyMessage.getSendUserName(),
                                                            nettyMessage.getSendUserIcon(), nettyMessage.getBaseType());

                                                }
                                            });
                                        }
                                    }
                                }
                            } else {
                                holder.setVisibility(R.id.item_send_text_left, false);//文字、语音类型为赠送礼物
                                holder.setVisibility(R.id.item_chat_text_left, true);//文字类型
                                holder.setVisibility(R.id.item_chat_voice_left_ll, false);//语音类型
                                holder.setVisibility(R.id.item_chat_private_message_left, false);//加锁的私密消息
                                holder.setVisibility(R.id.item_private_video_unlock_left, false);//解锁私密视频
                                holder.setVisibility(R.id.item_chat_private_image_left, false);//解锁私密图片
                                holder.setVisibility(R.id.item_chat_send_video_left, false);//隐藏视频邀请和私密消息控件
                                if (TextUtils.isEmpty(emTextMessageBody.getMessage())) {
                                    holder.setText(R.id.item_chat_text_left, Util.decode(nettyMessage.getContent()));
                                } else {
                                    if (emTextMessageBody.getMessage().contains("视频通话")
                                            || emTextMessageBody.getMessage().contains("視頻通話")
                                            || emTextMessageBody.getMessage().contains("Video Call")) {
                                        if (emTextMessageBody.getMessage().contains("已拒绝")
                                                || emTextMessageBody.getMessage().contains("已拒絕")
                                                || emTextMessageBody.getMessage().contains("Refused")) {
                                            holder.setText(R.id.item_chat_text_left,
                                                    mContext.getString(R.string.video_call) + mContext.getString(R.string.refused));
                                        } else if (emTextMessageBody.getMessage().contains("已取消")
                                                || emTextMessageBody.getMessage().contains("Cancelled")) {
                                            holder.setText(R.id.item_chat_text_left,
                                                    mContext.getString(R.string.video_call) + mContext.getString(R.string.canceled));
                                        } else {
                                            if (emTextMessageBody.getMessage().contains(":")) {
                                                String[] split = emTextMessageBody.getMessage().split(":");
                                                String replace = emTextMessageBody.getMessage().replace(split[0], mContext.getString(R.string.video_call));
                                                holder.setText(R.id.item_chat_text_left, replace);
                                            }
                                        }
                                    } else if (emTextMessageBody.getMessage().contains("语音通话")
                                            || emTextMessageBody.getMessage().contains("語音通話")
                                            || emTextMessageBody.getMessage().contains("Voice Call")) {
                                        if (emTextMessageBody.getMessage().contains("已拒绝")
                                                || emTextMessageBody.getMessage().contains("已拒絕")
                                                || emTextMessageBody.getMessage().contains("Refused")) {
                                            holder.setText(R.id.item_chat_text_left,
                                                    mContext.getString(R.string.voice_call) + mContext.getString(R.string.refused));
                                        } else if (emTextMessageBody.getMessage().contains("已取消")
                                                || emTextMessageBody.getMessage().contains("Cancelled")) {
                                            holder.setText(R.id.item_chat_text_left,
                                                    mContext.getString(R.string.voice_call) + mContext.getString(R.string.canceled));
                                        } else {
                                            if (emTextMessageBody.getMessage().contains(":")) {
                                                String[] split = emTextMessageBody.getMessage().split(":");
                                                String replace = emTextMessageBody.getMessage().replace(split[0], mContext.getString(R.string.video_call));
                                                holder.setText(R.id.item_chat_text_left, replace);
                                            }
                                        }
                                    } else {
                                        if (PlatformPreference.getPlatformInfo().getFid().equals("30108")) {
                                            holder.setText(R.id.item_chat_text_left, Util.chineseFontChanger(emTextMessageBody.getMessage()));
                                        } else {
                                            holder.setText(R.id.item_chat_text_left, emTextMessageBody.getMessage());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }//不是json数据的情况，有可能是视频邀请信，也有可能是文字消息


            }//null != emTextMessageBody
        }//null != emMessage
    }

    //解析发过来的数据
    private NettyMessage setMsgContent(String msgContent) {
        if (!TextUtils.isEmpty(msgContent)) {
            NettyMessage nettyMessage = new Gson().fromJson(msgContent, NettyMessage.class);
            return nettyMessage;
        } else {
            return null;
        }
    }

    private void setAccetpOnclick(final String userName, final String userPic, final String userAccount, final String userGuid, final int type) {
        //跳转视频聊天的界面
        if (fragmentManager != null) {
            VideoHelper.startVideoInvite(new VideoInviteParcelable(false, Long.parseLong(userGuid), userAccount
                    , userName, userPic, type, 0), mContext, "1");
        }
    }
}
