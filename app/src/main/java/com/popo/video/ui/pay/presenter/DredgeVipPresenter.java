package com.popo.video.ui.pay.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.popo.library.util.Utils;
import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.PayDict;
import com.popo.video.data.model.PayWay;
import com.popo.video.data.model.UserVip;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.ui.pay.adapter.DredgeVipAdapter;
import com.popo.video.ui.pay.contract.DredgeVipContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public class DredgeVipPresenter implements DredgeVipContract.IPresenter {
    private static final String TAG = DredgeVipPresenter.class.getSimpleName();
    private DredgeVipContract.IView mDredgeVipview;
    private Context mContext;
    private DredgeVipAdapter adapter;
    public DredgeVipPresenter(DredgeVipContract.IView view) {
        this.mDredgeVipview = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void getPayWay(String fromTag) {
        if(!DataPreference.getPayWayJsonString().equals("0")){
            PayWay payWay=new Gson().fromJson(DataPreference.getPayWayJsonString(),PayWay.class);
            if(payWay!=null){
                List<UserVip> vipList = payWay.getVipList();
                if(vipList!=null&&vipList.size()>0){
                    List<String> content=new ArrayList<String>();
                    List<String> name=new ArrayList<String>();
                    for (UserVip userVip : vipList) {
                        name.add(userVip.getNickName());
                        content.add(mContext.getString(R.string.start_time)+userVip.getServiceName()+mContext.getString(R.string.day)+"VIP");
                    }
                    mDredgeVipview.getGunDongText(name,content);
                }
                mDredgeVipview.getVipCount(String.valueOf(payWay.getVipCount()));
                PayDict payDict1 = payWay.getDictPayList().get(0);
                if(payDict1!=null){
                    mDredgeVipview.setInfoString1(payDict1.getServiceName(),payDict1.getPrice(),payDict1.getServiceDesc());
                    mDredgeVipview.getPayType1(payDict1.getServiceId());
                }
                PayDict payDict2 = payWay.getDictPayList().get(1);
                if(payDict1!=null){
                    mDredgeVipview.setInfoString2(payDict2.getServiceName(),payDict2.getPrice(),payDict2.getServiceDesc());
                    mDredgeVipview.getPayType2(payDict2.getServiceId());
                }
                if(payWay.getDescList()!=null&&payWay.getDescList().size()>0){
                    adapter=new DredgeVipAdapter(mContext,payWay.getDescList());
                    mDredgeVipview.setAdapter(adapter);
                }

            }
        }else{
            ApiManager.getPayWay("1", "2", new IGetDataListener<PayWay>() {
                @Override
                public void onResult(PayWay payWay, boolean isEmpty) {
                    if(payWay!=null){
                        List<UserVip> vipList = payWay.getVipList();
                        if(vipList!=null&&vipList.size()>0){
                            List<String> content=new ArrayList<String>();
                            List<String> name=new ArrayList<String>();
                            for (UserVip userVip : vipList) {
                                name.add(userVip.getNickName());
                               content.add(mContext.getString(R.string.start_time)+userVip.getServiceName()+mContext.getString(R.string.day)+"VIP");
//                               content.add(userVip.getStartTime()+mContext.getString(R.string.start_time)+userVip.getServiceName()+mContext.getString(R.string.day)+"VIP");
                            }
                            mDredgeVipview.getGunDongText(name,content);
                        }
                        mDredgeVipview.getVipCount(String.valueOf(payWay.getVipCount()));
                        PayDict payDict1 = payWay.getDictPayList().get(0);
                        if(payDict1!=null){
                            mDredgeVipview.setInfoString1(payDict1.getServiceName(),payDict1.getPrice(),payDict1.getServiceDesc());
                            mDredgeVipview.getPayType1(payDict1.getServiceId());
                        }
                        PayDict payDict2 = payWay.getDictPayList().get(1);
                        if(payDict1!=null){
                            mDredgeVipview.setInfoString2(payDict2.getServiceName(),payDict2.getPrice(),payDict2.getServiceDesc());
                            mDredgeVipview.getPayType2(payDict2.getServiceId());
                        }
                        if(payWay.getDescList()!=null&&payWay.getDescList().size()>0){
                            adapter=new DredgeVipAdapter(mContext,payWay.getDescList());
                            mDredgeVipview.setAdapter(adapter);
                        }

                    }
                }
                @Override
                public void onError(String msg, boolean isNetworkError) {
                }
            });
        }

    }

    /**
     * 检测后台商品是否可用
     *
     * @param list 后台返回的商品信息列表
     * @return 可用的商品列表
     */
    private List<PayDict> checkProductValid(List<PayDict> list) {
        List<PayDict> validList = new ArrayList<>();
        if (!Utils.isListEmpty(list)) {
            for (PayDict item : list) {
                if ("1".equals(item.getIsvalid())) {
                    validList.add(item);
                }
            }
        }
        return validList;
    }




    private void showSuccessTip() {
        mDredgeVipview.showTip(mContext.getString(R.string.pay_success));
    }
public static List<String> getListString(){
    List<String> list=new ArrayList<>();
    for (int i = 0; i < 10; i++) {
        list.add("你好"+i);
    }
    return list;
}
}
