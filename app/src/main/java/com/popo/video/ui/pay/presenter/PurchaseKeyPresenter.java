package com.popo.video.ui.pay.presenter;

import android.content.Context;

import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.PayDict;
import com.popo.video.data.model.PayWay;
import com.popo.video.data.model.UserBean;
import com.popo.video.data.model.UserDetail;
import com.popo.video.data.model.UserDetailforOther;
import com.popo.video.data.model.UserKey;
import com.popo.video.data.preference.PayPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.library.util.Utils;
import com.popo.video.ui.pay.adapter.PayAdapter;
import com.popo.video.ui.pay.contract.RechargeContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WangYong on 2017/8/30.
 */

public class PurchaseKeyPresenter implements RechargeContract.IPresenter {
     private RechargeContract.IView mPurchaseKey;
     private Context mContext;
     private PayAdapter mPayAdapter;

    public PurchaseKeyPresenter(RechargeContract.IView mPurchaseKey) {
        this.mPurchaseKey = mPurchaseKey;
        this.mContext = mPurchaseKey.obtainContext();
    }

    @Override
    public void start() {
        ApiManager.getUserInfo(UserPreference.getId(), new IGetDataListener<UserDetailforOther>() {
            @Override
            public void onResult(UserDetailforOther userDetailforOther, boolean isEmpty) {
                if(userDetailforOther!=null){
                    UserDetail userDetail = userDetailforOther.getUserDetail();
                   if(userDetail!=null){
                       UserKey userKey = userDetail.getUserKey();
                       UserBean userBean = userDetail.getUserBean();
                       if(userBean!=null){
                           PayPreference.saveDionmadsNum(userBean.getCounts());
                       }
                       if(userKey!=null){
                            mPurchaseKey.getKeyNum(String.valueOf(userKey.getCounts()));
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    @Override
    public void getPayWay(String fromTag) {
        mPurchaseKey.showLoading();
        ApiManager.getPayWay(fromTag,"3",new IGetDataListener<PayWay>() {
            @Override
            public void onResult(PayWay payWay, boolean isEmpty) {
                if (null != payWay) {
                    mPayAdapter = new PayAdapter(mContext, R.layout.item_pay_list,
                            checkProductValid(payWay.getDictPayList()));
                    mPurchaseKey.setAdapter(mPayAdapter);
                }
                if(payWay.getDescList()!=null&&payWay.getDescList().size()>0){
                    mPurchaseKey.setTextDetail(payWay.getDescList().get(0));
                }
                mPurchaseKey.dismissLoading();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (isNetworkError) {
                    mPurchaseKey.showNetworkError();
                }
                mPurchaseKey.dismissLoading();
            }
        });
    }

    @Override
    public void addTabs() {

    }

    @Override
    public void getMyInfo() {

    }

    /**
     * 检测后台商品是否可用
     *
     * @param list 后台返回的商品信息列表
     * @return 可用的商品列表
     */
    private List<PayDict> checkProductValid(List<PayDict> list) {
        List<PayDict> validList = new ArrayList<>();
        if (!Utils.isListEmpty(list)) {
            for (PayDict item : list) {
                if ("1".equals(item.getIsvalid())) {
                    validList.add(item);
                }
            }
        }
        return validList;
    }

}
