package com.popo.video.ui.pay.contract;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;
import com.popo.video.ui.pay.adapter.MyDiamondAdapter;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public interface MyDiamondContract {

    interface IView extends BaseView {

        /**
         * 显示加载
         */
        void showLoading();

        /**
         * 隐藏加载
         */
        void dismissLoading();

        void showNetworkError();
        /**
         * 设置商品信息适配器
         */
        void setAdapter(MyDiamondAdapter adapter);
        /**
         * 获取当前钻石的数量
         */
        void getDionmads(String num);
    }

    interface IPresenter extends BasePresenter {

        /**
         * @return 从后台获取支付渠道信息
         */
        void getPayWay(String fromTag);

    }

}
