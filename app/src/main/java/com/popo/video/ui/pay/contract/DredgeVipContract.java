package com.popo.video.ui.pay.contract;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;
import com.popo.video.ui.pay.adapter.DredgeVipAdapter;

import java.util.List;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public interface DredgeVipContract {

    interface IView extends BaseView {


        /**
         * 显示加载
         */
        void showLoading();

        /**
         * 隐藏加载
         */
        void dismissLoading();
        /**
         * 获取支付信息1
         */
        void setInfoString1(String serviceName1, String price1, String onedayprice1);
        /**
         * 获取支付信息2
         */
        void setInfoString2(String serviceName2, String price2, String onedayprice2);
        /**
         * 获取vip的数量
         */
        void getVipCount(String vipCount);
        /**
         * 设置adapter
         */
        void setAdapter(DredgeVipAdapter adapter);
        /**
         * 获取payType1
         */
        void getPayType1(String payType);
        /**
         * 获取payType2
         */
        void getPayType2(String payType);
        /**
         * 获取服务的名称1
         */
        String getSerViceName1();
        /**
         * 获取服务的名称2
         */
        String getSerViceName2();
        /**
         * 获取服务类型1
         */
        String getServiceId1();
        /**
         * 获取服务类型2
         */
        String getServiceId2();
        /**
         * 获取价格1
         */
        String getPrice1();
        /**
         * 获取价格2
         */
        String getPrice2();
        /**
         * 获取滚动的字幕
         */
        void getGunDongText(List<String> title, List<String> content);

    }

    interface IPresenter extends BasePresenter {

        /**
         * @return 从后台获取支付渠道信息
         */
        void getPayWay(String fromTag);
    }

}
