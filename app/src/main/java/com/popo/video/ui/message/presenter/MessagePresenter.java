package com.popo.video.ui.message.presenter;

import android.content.Context;

import com.popo.video.ui.message.adapter.MessageAdapter;
import com.popo.video.ui.message.contract.MessageContract;

/**
 * Created by zhangdroid on 2017/6/10.
 */
public class MessagePresenter implements MessageContract.IPresenter {
    private MessageContract.IView mMessageView;
    private Context mContext;

    public MessagePresenter(MessageContract.IView view) {
        this.mMessageView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {

    }


    @Override
    public void addTabs() {
        MessageAdapter messageAdapter = new MessageAdapter(mMessageView.getManager());
//        List<Fragment> fragmentList = new ArrayList<>();
//        fragmentList.add(new ChatHistoryFragment());
//        fragmentList.add(new VideoHistoryFragment());
//        messageAdapter.setData(fragmentList, Arrays.asList(mContext.getString(R.string.message_chat_history),
//                mContext.getString(R.string.message_video_history)));
     //   mMessageView.setAdapter(messageAdapter);
    }

}
