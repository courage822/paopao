package com.popo.video.ui.detail.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.danikula.videocache.HttpProxyCacheServer;
import com.popo.video.R;
import com.popo.video.base.BaseApplication;
import com.popo.video.common.ParamsUtils;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.MyInfo;
import com.popo.video.data.model.TUserVideoShow;
import com.popo.video.data.model.UpLoadMyInfo;
import com.popo.video.data.model.UploadInfoParams;
import com.popo.video.data.model.UserBase;
import com.popo.video.data.model.UserBean;
import com.popo.video.data.model.UserDetail;
import com.popo.video.data.model.UserMend;
import com.popo.video.data.model.UserPhoto;
import com.popo.video.data.model.VideoOrImage;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.PayPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.event.UserInfoChangedEvent;
import com.popo.video.ui.detail.contract.EditInfoContract;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/6/9.
 */
public class EditInfoPresenter implements EditInfoContract.IPresenter {
    private EditInfoContract.IView mEditInfoView;
    private Context mContext;

    public EditInfoPresenter(EditInfoContract.IView mEditInfoView) {
        this.mEditInfoView = mEditInfoView;
        mContext = mEditInfoView.obtainContext();
    }

    @Override
    public void start() {
        mEditInfoView.isMale(UserPreference.isMale());
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    if (!TextUtils.isEmpty(myInfo.getIslj())){
                        UserPreference.setIslj(myInfo.getIslj());
                    }
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        List<VideoOrImage> list = new ArrayList<VideoOrImage>();
                        List<TUserVideoShow> userVideoShows = userDetail.getUserVideoShows();
                        if (userVideoShows != null && userVideoShows.size() != 0) {
                            TUserVideoShow tUserVideoShow = userVideoShows.get(0);
                            list.add(new VideoOrImage(tUserVideoShow.getThumbnailUrl(), true, tUserVideoShow.getVideoUrl()));
                        }
                        UserBase userBase = userDetail.getUserBase();
                        if (userBase != null) {
                            list.add(new VideoOrImage(userBase.getIconUrlMiddle(), false, null));
                            mEditInfoView.getUserId(String.valueOf(userBase.getAccount()));
                            mEditInfoView.getAge(String.valueOf(userBase.getAge()));
                            mEditInfoView.getNickNmae(userBase.getNickName());
                            mEditInfoView.getOwnWords(userBase.getOwnWords());
                            mEditInfoView.getSign(ParamsUtils.getSignValue(String.valueOf(userBase.getSign())));
                            String state = userBase.getStatus();
                            if (!TextUtils.isEmpty(state)) {
                                if (state.equals("3")) {
                                    mEditInfoView.setStatus(mContext.getString(R.string.message_state_nodistrub));
                                } else if (state.equals("1")) {
                                    mEditInfoView.setStatus(mContext.getString(R.string.edit_info_status));
                                } else {
                                    mEditInfoView.setStatus(mContext.getString(R.string.not_online));
                                }
                            }
                        }
                        List<UserPhoto> userPhotos = userDetail.getUserPhotos();
                        if (userPhotos != null && userPhotos.size() > 0) {
                            for (UserPhoto userPhoto : userPhotos) {
                                list.add(new VideoOrImage(userPhoto.getFileUrlMiddle(), false, null));
                            }
                        }
                        mEditInfoView.startBanner(list);

                        UserBean userBean = userDetail.getUserBean();
                        if (userBean != null) {
                            PayPreference.saveDionmadsNum(userBean.getCounts());
                        }

                        UserMend userMend = userDetail.getUserMend();
                        if (userMend != null) {
                            if (!TextUtils.isEmpty(userMend.getHeight())) {
                                if (UserPreference.getCountryId().equals("97")) {
                                    mEditInfoView.getHeight(DataPreference.getInchCmByCm(userMend.getHeight()));
                                } else {
                                    mEditInfoView.getHeight(userMend.getHeight() + "cm");
                                }
                            } else {
                                if (UserPreference.getCountryId().equals("97")) {
                                    mEditInfoView.getHeight("5'7\"");
                                } else {
                                    mEditInfoView.getHeight("173cm");
                                }
                            }
                            if (!TextUtils.isEmpty(userMend.getWeight())) {
                                mEditInfoView.getWeight(userMend.getWeight());
                            }
                            if (!TextUtils.isEmpty(userMend.getRelationshipId())) {
                                mEditInfoView.getMarriage(DataPreference.getValueByKey(userMend.getRelationshipId(), 1));
                            } else {
                                mEditInfoView.getMarriage(mContext.getString(R.string.edit_info_default_emotion));
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }


    @Override
    public void setLocalInfo() {
//        mEditInfoView.setUserAvator(UserPreference.getMiddleImage());
        mEditInfoView.setIntroducation(UserPreference.getIntroducation());
    }

    @Override
    public void upLoadMyInfo() {
        mEditInfoView.showLoading();
        UploadInfoParams uploadInfoParams = new UploadInfoParams();
        if (!TextUtils.isEmpty(mEditInfoView.stringNickName())) {
            uploadInfoParams.setNickName(mEditInfoView.stringNickName());
        }
        if (!TextUtils.isEmpty(mEditInfoView.stringOwnWords())) {
            uploadInfoParams.setOwnWords(mEditInfoView.stringOwnWords());
        }
        if (!TextUtils.isEmpty(mEditInfoView.stringHeight())) {
            String height;
            if (UserPreference.getCountryId().equals("97")) {
                height = DataPreference.getCmByInch(mEditInfoView.stringHeight());
            } else {
                height = mEditInfoView.stringHeight().replace("cm", "");
            }
            uploadInfoParams.setHeight(height);
        }
        if (!TextUtils.isEmpty(mEditInfoView.stringRelation())) {
            uploadInfoParams.setRelationshipId(DataPreference.getPersonMapKey(mEditInfoView.stringRelation(), 1));
        }
        if (!TextUtils.isEmpty(mEditInfoView.stringWeight())) {
            String kg = mEditInfoView.stringWeight().replace("kg", "");
            uploadInfoParams.setWeight(kg);
        }

        if (!TextUtils.isEmpty(mEditInfoView.stringSign())) {
            uploadInfoParams.setSign(ParamsUtils.getSignkey(mEditInfoView.stringSign()));
        }
        if (!TextUtils.isEmpty(mEditInfoView.stringAge())) {
            String substring;
            if (mEditInfoView.stringAge().length() == 2) {
                substring = mEditInfoView.stringAge().substring(0, 1);
            } else {
                substring = mEditInfoView.stringAge().substring(0, 2);
            }
            uploadInfoParams.setAge(substring);
        }
        ApiManager.upLoadMyInfo(uploadInfoParams, new IGetDataListener<UpLoadMyInfo>() {
            @Override
            public void onResult(UpLoadMyInfo upLoadMyInfo, boolean isEmpty) {
                // mEditInfoView.dismissLoading();
                // 发送用户资料改变事件
                EventBus.getDefault().post(new UserInfoChangedEvent());
                if (upLoadMyInfo != null) {
                    UserBase userBase = upLoadMyInfo.getUserBase();
                    if (userBase != null) {
                        // 更新本地用户信息
                        UserPreference.saveUserInfo(userBase);
                    }
                }
                // finish
                mEditInfoView.finishActivity();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mEditInfoView.dismissLoading();
                if (isNetworkError) {
                    mEditInfoView.showNetworkError();
                } else {
                    mEditInfoView.showTip(msg);
                }
            }
        });
    }

    @Override
    public void getUserInfo() {
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    if (!TextUtils.isEmpty(myInfo.getIslj())){
                        UserPreference.setIslj(myInfo.getIslj());
                    }
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        List<VideoOrImage> list = new ArrayList<VideoOrImage>();
                        List<TUserVideoShow> userVideoShows = userDetail.getUserVideoShows();
                        if (userVideoShows != null && userVideoShows.size() != 0) {
                            TUserVideoShow tUserVideoShow = userVideoShows.get(0);
                            HttpProxyCacheServer proxy = BaseApplication.getProxy(mContext);
                            String proxyUrl = proxy.getProxyUrl(tUserVideoShow.getVideoUrl());
                            list.add(new VideoOrImage(tUserVideoShow.getThumbnailUrl(), true, proxyUrl));
                        }
                        UserBase userBase = userDetail.getUserBase();
                        if (userBase != null) {
                            list.add(new VideoOrImage(userBase.getIconUrlMiddle(), false, null));
                        }
                        List<UserPhoto> userPhotos = userDetail.getUserPhotos();
                        if (userPhotos != null && userPhotos.size() > 0) {
                            for (UserPhoto userPhoto : userPhotos) {
                                list.add(new VideoOrImage(userPhoto.getFileUrlMiddle(), false, null));
                            }
                        }
                        mEditInfoView.upBanner(list);

                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

}
