package com.popo.video.ui.personalcenter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.popo.library.dialog.LoadingDialog;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.image.RoundedCornersTransformation;
import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.SnackBarUtil;
import com.popo.library.widget.PlayView;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.base.BaseAppCompatActivity;
import com.popo.video.common.TimeUtils;
import com.popo.video.common.YeMeiPopopUtil;
import com.popo.video.data.model.NettyMessage;
import com.popo.video.event.SingleLoginFinishEvent;
import com.popo.video.event.YeMeiMessageEvent;
import com.popo.video.parcelable.PlayViewParcelable;
import com.popo.video.ui.personalcenter.contract.AuthenticationContract;
import com.popo.video.ui.personalcenter.presenter.AuthenticationPresenter;
import com.popo.video.ui.photo.GetPhotoActivity;

import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import butterknife.BindView;

import static com.popo.video.C.Video.REQUEST_CODE;

/**
 * 认证主播页面
 * Created by zhangdroid on 2017/5/27.
 */
public class AuthenticationActivity extends BaseAppCompatActivity implements View.OnClickListener, AuthenticationContract.IView {
    @BindView(R.id.activity_authentication_rl_root)
    LinearLayout mLlRoot;
    @BindView(R.id.activity_authentication_tv_commit)
    TextView tv_submit;//提交按钮
    @BindView(R.id.activity_authentication_iv_avatar)
    ImageView iv_avatar;//头像
    @BindView(R.id.activity_authentication_playview)
    PlayView playView;//播放的view
    @BindView(R.id.activity_authentication_iv_record)
    ImageView iv_record;//开始录制视频按钮
    @BindView(R.id.activity_authentication_iv_re_record)
    ImageView iv_re_record;//重新录制按钮
    @BindView(R.id.activity_authentication_rl_playview)
    RelativeLayout rl_playView;
    @BindView(R.id.authentication_activity_rl_back)
    RelativeLayout rl_back;
    private long playPostion = -1;
    private long duration = -1;
    private File videoFile;
    String stringExtra=null;
    public final static String DATA = "URL";
    private String   videoNumber=null;
    private AuthenticationPresenter mAuthticationPresenter;
    private String text;
    private File avatarFile;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_authentication;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }



    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return mLlRoot;
    }

    @Override
    protected void initViews() {
        mAuthticationPresenter = new AuthenticationPresenter(this);
        if(TimeUtils.timeIsPast()){//判断页脚是否过期
            msgHandler.sendEmptyMessage(1);
        }else if(!TimeUtils.timeIsPast()){
            msgHandler.sendEmptyMessage(2);
        }
    }

    @Override
    protected void setListeners() {
        tv_submit.setOnClickListener(this);
        iv_avatar.setOnClickListener(this);
        iv_record.setOnClickListener(this);
        iv_re_record.setOnClickListener(this);
        playView.setOnClickListener(this);
        rl_playView.setOnClickListener(this);
        rl_back.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        mAuthticationPresenter.getFaceBookAccount();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_authentication_tv_commit:
                //提交审核
                mAuthticationPresenter.submitCheck(videoNumber,videoFile,avatarFile);
                break;
            case R.id.activity_authentication_iv_avatar:
                GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        avatarFile=file;
                        Glide.with(AuthenticationActivity.this).load(file).into(iv_avatar);
                    }
                });
                //上传头像
                break;
            case R.id.activity_authentication_iv_record:
                //开始录制
                //LaunchHelper.getInstance().launchResult(AuthenticationActivity.this,RecordActivity.class,new PlayViewParcelable(videoFile.getAbsolutePath()), REQUEST_CODE);
                Intent intent = new Intent(this, RecordActivity.class);
                videoDir();
                intent.putExtra(DATA, videoFile.getAbsolutePath());
                startActivityForResult(intent,REQUEST_CODE);
                break;
            case R.id.activity_authentication_rl_playview:
                //开始播放
                LaunchHelper.getInstance().launch(mContext, PlayActivity.class,new PlayViewParcelable(videoFile.getAbsolutePath()));
                break;
            case R.id.activity_authentication_iv_re_record:
                //重新录制
                Intent intent3 = new Intent(this, RecordActivity.class);
                videoDir();
                intent3.putExtra(DATA, videoFile.getAbsolutePath());
                startActivityForResult(intent3,REQUEST_CODE);
                break;
            case R.id.authentication_activity_rl_back:
                finish();
                break;
        }
    }
    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        playView.stopPlayback();
    }
    private void initPlayView() {
        if(!TextUtils.isEmpty(stringExtra)){
            playView.setVideoURI(Uri.parse(stringExtra));
            playView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    playView.seekTo(1);
                }
            });
            playView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    //获取视频资源的宽度
                    int videoWidth = mp.getVideoWidth();
                    //获取视频资源的高度
                    int videoHeight = mp.getVideoHeight();
                    playView.setSizeH(videoHeight);
                    playView.setSizeW(videoWidth);
                    playView.requestLayout();
                    duration = mp.getDuration();
                }
            });
        }
    }

    public void videoDir() {
        videoFile = new File(C.Video.SD_PATH);
        // 创建文件
        try {
            if (!videoFile.exists()) {
                videoFile.createNewFile();
                videoFile.setWritable(Boolean.TRUE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        playView.seekTo((int) ((playPostion > 0 && playPostion < duration) ? playPostion : 1));
    }


    @Override
    protected void onPause() {
        super.onPause();
        playView.pause();
        playPostion = playView.getCurrentPosition();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //判断请求码
        if(requestCode == REQUEST_CODE){
            //判断结果码
            if(resultCode == RecordActivity.RESULT_CODE){
                stringExtra = data.getStringExtra(C.Video.DATA);
                String s = data.getStringExtra("videoNumber");
                videoNumber = s.replaceAll(" ", "");
                iv_record.setVisibility(View.GONE);
                iv_re_record.setVisibility(View.VISIBLE);
                playView.setVisibility(View.VISIBLE);
                rl_playView.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initPlayView();
    }

    @Override
    public void getAvatarUrl(String url) {
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(url).imageView(iv_avatar).error(R.drawable.chick_pic_video).transform(new RoundedCornersTransformation(this,20,20)).build());
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void setFaceBookAccount(String account) {
        text=account;
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public String copyFacebookAccount() {
        return "123";
    }
    @Subscribe
    public void onEvent(SingleLoginFinishEvent event){
        finish();//单点登录销毁的activity
    }
    @Subscribe
    public void onEvent(NettyMessage event) {
        msgHandler.sendEmptyMessage(1);
    }
    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    closeShowPop();
                    break;
            }
        }
    };

    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }

    @Override
    protected void closeShowPop() {
        super.closeShowPop();

    }
    @Subscribe
    public void onEvent(YeMeiMessageEvent evet){
        Message msg=new Message();
        msg.obj=evet;
        yeMeiHandler.sendMessage(msg);

    }
    private Handler yeMeiHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            YeMeiMessageEvent obj = (YeMeiMessageEvent) msg.obj;
            if(obj!=null){
                YeMeiPopopUtil.getInstance().showYeMei(AuthenticationActivity.this,mLlRoot,obj,isFinishing());
            }
        }
    };
}
