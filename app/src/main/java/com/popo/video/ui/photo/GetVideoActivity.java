package com.popo.video.ui.photo;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.library.util.FileUtil;
import com.popo.library.util.ToastUtil;
import com.popo.video.common.BitmapUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 录像/从本地选择视频
 * Created by xu on 2017/11/29.
 */
public class GetVideoActivity extends Activity {
    @BindView(R.id.photo_root)
    LinearLayout mLlRoot;
    @BindView(R.id.choose_from_album)
    TextView mTvChooseFromAlbum;
    @BindView(R.id.take_photo)
    TextView mTvTakePhoto;
    @BindView(R.id.tv_type)
    TextView tv_type;

    private static final int PERMISSION_CODE_READ_EXTERNAL_STORAGE = 0;
    public static final int RECORD_SYSTEM_VIDEO = 1;
    private static final int REQUEST_CODE_TAKE_VIDEO = 0;
    private static OnGetVideoListener sOnGetVideoListener;

    public interface OnGetVideoListener {

        /**
         * 返回选择后的视频文件
         *
         * @param filePath       视频地址
         * @param bitmapFilePath 截图地址
         * @param duration       视频的时长
         */
        void getSelectedVideo(String filePath, String duration, String bitmapFilePath);
    }

    // 视频临时保存路径
    private static boolean mIsShowUi = true;
    private static final String INTENT_KEY_IS_TAKE_PHOTO = "is_take_video";

    public static void toGetVideoActivity(Context context, boolean isTakePhoto, OnGetVideoListener listener) {
        sOnGetVideoListener = listener;
        mIsShowUi = false;
        Intent intent = new Intent(context, GetVideoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


    /**
     * 获取本地视频（拍照/从相册选取）
     *
     * @param context  上下文
     * @param listener 图片监听器
     */
    public static void toGetVideoActivity(Context context, OnGetVideoListener listener) {
        sOnGetVideoListener = listener;
        mIsShowUi = true;
        Intent intent = new Intent(context, GetVideoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_photo);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this);
        tv_type.setText(getString(R.string.upload_video));
        mTvChooseFromAlbum.setText(getString(R.string.video_video));
        mTvTakePhoto.setText(getString(R.string.video_shoot));

        addListener();
        if (!mIsShowUi) {
            mTvTakePhoto.setVisibility(View.GONE);
        }
    }

    protected void addListener() {
        //从本地获取视频
        mTvChooseFromAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 隐藏对话框
                mLlRoot.setVisibility(View.GONE);
                // 从本地获取视频
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_CODE_TAKE_VIDEO);

            }
        });
        //录制视频
        mTvTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 隐藏对话框
                mLlRoot.setVisibility(View.GONE);

                Intent intent = new Intent(GetVideoActivity.this, ShortRecordActivity.class);
                startActivityForResult(intent, RECORD_SYSTEM_VIDEO);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CODE_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                takePhoto();
            } else {
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_TAKE_VIDEO:// 获取视频
                    if (data != null) {
                        Uri uri = data.getData();
                        if (uri != null) {
                            ContentResolver cr = this.getContentResolver();
                            Cursor cursor = cr.query(uri, null, null,
                                    null, MediaStore.Video.Media.DEFAULT_SORT_ORDER);
                            if (cursor != null) {
                                if (cursor.moveToFirst()) {

                                    final String v_path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));

                                    // 视频时长：MediaStore.Audio.Media.DURATION
                                    long durationInt = 10000;
//                                    try {
//                                        durationInt = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media.DURATION));
//                                        Log.e("durationint", durationInt + "");
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
                                    MediaPlayer mediaPlayer = new MediaPlayer();
                                    try {
                                        mediaPlayer.setDataSource(Uri.decode(v_path));
                                        mediaPlayer.prepare();
                                        durationInt = mediaPlayer.getDuration();
                                        mediaPlayer.release();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        mediaPlayer.release();
                                    }
                                    final String v_duration = durationInt / 1000 + "";
                                    long size = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE));

                                    if (size > 3 * 1024 * 1024) {
                                        mLlRoot.setVisibility(View.VISIBLE);
                                        if (!mIsShowUi) {
                                            mTvTakePhoto.setVisibility(View.GONE);
                                        }
                                        ToastUtil.showShortToast(GetVideoActivity.this, getString(R.string.limit_3m));
                                        return;
                                    } else if (durationInt / 1000 < 5) {
                                        mLlRoot.setVisibility(View.VISIBLE);
                                        if (!mIsShowUi) {
                                            mTvTakePhoto.setVisibility(View.GONE);
                                        }
                                        ToastUtil.showShortToast(GetVideoActivity.this, getString(R.string.limit_5s_minimum));
                                    } else {
                                        File file = new File(v_path);
                                        if (file.exists()) {
                                            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                                            mmr.setDataSource(v_path);
                                            final Bitmap bitmap = mmr.getFrameAtTime();
                                            mmr.release();
                                            new AsyncTask<Void, Void, File>() {
                                                @Override
                                                protected File doInBackground(Void... voids) {
                                                    return BitmapUtils.compressImage(GetVideoActivity.this, bitmap);
                                                }

                                                @Override
                                                protected void onPostExecute(File file) {
                                                    sOnGetVideoListener.getSelectedVideo(v_path, v_duration, file.getAbsolutePath());
                                                }
                                            }.execute();
                                        }
                                    }
                                }
                            }
                            cursor.close();
                        }
                    }
                    finish();
                    break;
                case RECORD_SYSTEM_VIDEO://录制视频
                    if (null != data) {
                        String bitmapFilePath = data.getStringExtra("bitmapFilePath");
                        String videoFilePath = data.getStringExtra("videoFilePath");
                        String duration = data.getStringExtra("duration");
                        sOnGetVideoListener.getSelectedVideo(videoFilePath, duration, bitmapFilePath);
                    }
                    finish();
                    break;
            }
        } else {
            finish();
        }
    }
}
