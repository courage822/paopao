package com.popo.video.ui.homepage.contract;

import android.support.v4.app.FragmentManager;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;
import com.popo.video.ui.homepage.ListFragmentBaseAdapter;

/**
 * Created by zhangdroid on 2017/6/2.
 */
public interface ListContract2 {

    interface IView extends BaseView {

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        void toggleShowError(boolean toggle, String msg);

        void toggleShowEmpty(boolean toggle, String msg);

        /**
         * @return 获得FragmentManager，用于显示对话框
         */
        FragmentManager obtainFragmentManager();

        /**
         * 设置adapter
         *
         * @param adapter        adapter
         * @param loadMoreViewId 加载更多View
         */
        void setAdapter(ListFragmentBaseAdapter adapter, int loadMoreViewId);
        /**
         * 显示加载更多
         */
        void showLoadMore();

        /**
         * 隐藏加载更多
         */
        void hideLoadMore();

        /**
         * 显示没有更多（滑动到最底部）
         */
        void showNoMore();

    }

    interface IPresenter extends BasePresenter {

        /**
         * 加载推荐用户列表
         *
         * @param type {@link com.popo.video.C.homepage}
         */
        void loadRecommendUserList(int type);

        /**
         * 下拉刷新
         */
        void refresh();
        /**
         * 移除指定的item
         */

    }

}
