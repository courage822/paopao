package com.popo.video.ui.dialog;

import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.library.dialog.BaseDialogFragment;

/**
 * 找回密码提示框
 * Created by zhangdroid on 2017/6/1.
 */
public class FindPasswordDialog extends BaseDialogFragment {
    private String mMessage;

    private static FindPasswordDialog newInstance(String message) {
        FindPasswordDialog findPasswordDialog = new FindPasswordDialog();
        findPasswordDialog.mMessage = message;
        return findPasswordDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_find_password;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tvMessage = (TextView) view.findViewById(R.id.dialog_find_pwd_message);
        if (!TextUtils.isEmpty(mMessage)) {
            tvMessage.setText(mMessage);
        }
        Button btnClose = (Button) view.findViewById(R.id.dialog_find_pwd_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public static void show(String message, FragmentManager fragmentManager) {
        newInstance(message).show(fragmentManager, "find_password");
    }

}
