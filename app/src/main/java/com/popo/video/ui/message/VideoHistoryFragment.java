package com.popo.video.ui.message;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.FrameLayout;

import com.popo.video.R;
import com.popo.video.base.BaseFragment;
import com.popo.video.data.model.UserBase;
import com.popo.video.data.model.VideoRecord;
import com.popo.library.adapter.MultiTypeRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.adapter.wrapper.OnLoadMoreListener;
import com.popo.library.util.SnackBarUtil;
import com.popo.library.widget.AutoSwipeRefreshLayout;
import com.popo.library.widget.XRecyclerView;
import com.popo.video.ui.message.adapter.VideoHistoryAdapter;
import com.popo.video.ui.message.contract.VideoHistoryContract;
import com.popo.video.ui.message.presenter.VideoHistoryPresenter;

import butterknife.BindView;

/**
 * 视频聊天记录列表
 * Created by zhangdroid on 2017/6/10.
 */
public class VideoHistoryFragment extends BaseFragment implements VideoHistoryContract.IView {
    @BindView(R.id.video_history_root)
    FrameLayout mFlRoot;
    @BindView(R.id.video_history_refresh)
    AutoSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.video_history_list)
    XRecyclerView mRecyclerView;

    private VideoHistoryPresenter mVideoHistoryPresenter;
    private VideoHistoryAdapter mVideoHistoryAdapter;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_video_history;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return mSwipeRefreshLayout;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {
    }

    @Override
    protected void initViews() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mVideoHistoryAdapter = new VideoHistoryAdapter(mContext, R.layout.item_video_history,getFragmentManager());
        mRecyclerView.setAdapter(mVideoHistoryAdapter, R.layout.common_load_more);
        mVideoHistoryPresenter = new VideoHistoryPresenter(this);
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        // 下拉刷新和上拉加载更多
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mVideoHistoryPresenter.refresh();
                }
            }
        });
        mRecyclerView.setOnLoadingMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mVideoHistoryPresenter.loadMore();
            }
        });
        mVideoHistoryAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                VideoRecord videoRecord = mVideoHistoryAdapter.getItemByPosition(position);
                if (null != videoRecord) {
                    UserBase userBase = videoRecord.getUserBase();
//                    if (null != userBase) {
//                        VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount(),
//                                userBase.getNickName(), userBase.getIconUrlMininum()), getChildFragmentManager());
//                    }
                }
            }
        });
    }

    @Override
    protected void loadData() {
        mVideoHistoryPresenter.loadHistoryList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mFlRoot, msg);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mVideoHistoryPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowEmpty(false, null, null);
                mVideoHistoryPresenter.refresh();
            }
        });
    }

    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null != mSwipeRefreshLayout && mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void showLoadMore() {
        mRecyclerView.setVisibility(R.id.load_more_progress, true);
        mRecyclerView.setVisibility(R.id.load_more_msg, true);
        mRecyclerView.setVisibility(R.id.load_more_empty, false);
        mRecyclerView.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
        mRecyclerView.finishLoadMore();
    }

    @Override
    public void showNoMore() {
        mRecyclerView.setVisibility(R.id.load_more_empty, true);
        mRecyclerView.setVisibility(R.id.load_more_progress, false);
        mRecyclerView.setVisibility(R.id.load_more_msg, false);
    }

    @Override
    public VideoHistoryAdapter getVideoHistoryAdapter() {
        return mVideoHistoryAdapter;
    }
}
