package com.popo.video.ui.detail;

import android.os.Parcelable;
import android.view.View;

import com.popo.library.net.NetUtil;
import com.popo.playerlibrary.EmptyControlVideo;
import com.popo.playerlibrary.gsyvideoplayer.GSYVideoManager;
import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.parcelable.ShortPlayParcelable;

import butterknife.BindView;

/**
 * 短视频播放
 */

public class ShortPlayActivity extends BaseFragmentActivity {
    @BindView(R.id.player_empty)
    EmptyControlVideo player_empty;
    private ShortPlayParcelable mShortPlayParcelable;
    private String stringExtra;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_short_play;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mShortPlayParcelable = (ShortPlayParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        stringExtra = mShortPlayParcelable.urlData;
        player_empty.setUp(stringExtra,true,"234");
        player_empty.setLooping(true);
        player_empty.startPlayLogic();
    }


    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        GSYVideoManager.instance().onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player_empty.release();
    }

    @Override
    protected void onPause() {
        super.onPause();
        GSYVideoManager.instance().onPause();

    }
}
