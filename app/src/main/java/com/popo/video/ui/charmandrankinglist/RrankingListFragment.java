package com.popo.video.ui.charmandrankinglist;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.base.BaseFragment;
import com.popo.video.common.Util;
import com.popo.library.adapter.wrapper.HeaderAndFooterWrapper;
import com.popo.library.adapter.wrapper.OnLoadMoreListener;
import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.widget.AutoSwipeRefreshLayout;
import com.popo.library.widget.XRecyclerView;
import com.popo.video.ui.charmandrankinglist.contract.CharmandListContract;
import com.popo.video.ui.charmandrankinglist.presenter.RrankingListPresenter;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/10/25.
 * 富豪榜fragment第二个页面
 */

public class RrankingListFragment extends BaseFragment implements CharmandListContract.IView {
    @BindView(R.id.ranking_list_recyclerview)
    XRecyclerView recyclerView2;
    @BindView(R.id.ranking_list_swiperefresh)
    AutoSwipeRefreshLayout mSwiperefresh2;
    private RrankingListPresenter mPresenter2;
    private ImageView iv_avatar1;
    private ImageView iv_avatar2;
    private ImageView iv_avatar3;
    private TextView tv_name1;
    private TextView tv_name2;
    private TextView tv_name3;
    private TextView tv_gift1;
    private TextView tv_gift2;
    private TextView tv_gift3;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;

    @Override
    protected int getLayoutResId() {

        return R.layout.fragment_rankinglist;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }


    @Override
    protected void initViews() {
        mSwiperefresh2.setColorSchemeResources(R.color.main_color);
        mSwiperefresh2.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        recyclerView2.setLayoutManager(linearLayoutManager);
        mPresenter2 = new RrankingListPresenter(this);
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        mSwiperefresh2.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mPresenter2.refresh2();
                }
            }
        });
    }

    @Override
    protected void loadData() {
        mPresenter2.loadRichUserList();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void hideRefresh(int delaySeconds) {
        mSwiperefresh2.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSwiperefresh2.isRefreshing()) {
                    mSwiperefresh2.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);

    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {

    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {

    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return null;
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter, int loadMoreViewId) {
        // 添加HeaderView
        HeaderAndFooterWrapper headerAndFooterWrapper = new HeaderAndFooterWrapper(adapter);
        View view = LayoutInflater.from(mContext).inflate(R.layout.charmand_and_ranking_head_item, null);
        headerAndFooterWrapper.addHeaderView(view, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        getViewId(view);
        recyclerView2.setAdapter(headerAndFooterWrapper, loadMoreViewId);
    }

    private void getViewId(View view) {
        iv_avatar1 = (ImageView) view.findViewById(R.id.charmand_and_rangkinglist_head_item_iv1);
        iv_avatar2 = (ImageView) view.findViewById(R.id.charmand_and_rangkinglist_head_item_iv2);
        iv_avatar3 = (ImageView) view.findViewById(R.id.charmand_and_rangkinglist_head_item_iv3);
        tv_name1 = (TextView) view.findViewById(R.id.charmand_and_rangkinglist_head_item_tv_name1);
        tv_name2 = (TextView) view.findViewById(R.id.charmand_and_rangkinglist_head_item_tv_name2);
        tv_name3 = (TextView) view.findViewById(R.id.charmand_and_rangkinglist_head_item_tv_name3);
        tv_gift1 = (TextView) view.findViewById(R.id.charmand_and_rangkinglist_head_item_tv_gift1);
        tv_gift2 = (TextView) view.findViewById(R.id.charmand_and_rangkinglist_head_item_tv_gift2);
        tv_gift3 = (TextView) view.findViewById(R.id.charmand_and_rangkinglist_head_item_tv_gift3);
    }

    @Override
    public void setAvatarAndGift1(String avatar, String name, int gift) {
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(avatar).transform(new CropCircleTransformation(mContext))
                .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(iv_avatar1).build());
        tv_name1.setText(name);
        tv_gift1.setText(mContext.getString(R.string.send) + gift + mContext.getString(R.string.number_gifts));
    }

    @Override
    public void setAvatarAndGift2(String avatar, String name, int gift) {
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(avatar).transform(new CropCircleTransformation(mContext))
                .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(iv_avatar2).build());
        tv_name2.setText(name);
        tv_gift2.setText(mContext.getString(R.string.send) + gift + mContext.getString(R.string.number_gifts));
    }

    @Override
    public void setAvatarAndGift3(String avatar, String name, int gift) {
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(avatar).transform(new CropCircleTransformation(mContext))
                .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(iv_avatar3).build());
        tv_name3.setText(name);
        tv_gift3.setText(mContext.getString(R.string.send) + gift + mContext.getString(R.string.number_gifts));
    }

    @Override
    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        recyclerView2.setOnLoadingMoreListener(onLoadMoreListener);
    }

    @Override
    public void showLoadMore() {
        recyclerView2.setVisibility(R.id.load_more_progress, true);
        recyclerView2.setVisibility(R.id.load_more_msg, true);
        recyclerView2.setVisibility(R.id.load_more_empty, false);
        recyclerView2.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
        recyclerView2.finishLoadMore();
    }

    @Override
    public void showNoMore() {
        recyclerView2.setVisibility(R.id.load_more_empty, true);
        recyclerView2.setVisibility(R.id.load_more_progress, false);
        recyclerView2.setVisibility(R.id.load_more_msg, false);
    }
}


