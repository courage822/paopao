package com.popo.video.ui.message;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.base.BaseFragment;
import com.popo.video.common.TimeUtils;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.db.DbModle;
import com.popo.video.event.MessageArrive;
import com.popo.library.util.SnackBarUtil;
import com.popo.library.widget.AutoSwipeRefreshLayout;
import com.popo.video.ui.match.MatchAdapter;
import com.popo.video.ui.message.adapter.ChatHistoryAdapter;
import com.popo.video.ui.message.adapter.NewRecyclerAdapter;
import com.popo.video.ui.message.contract.ChatHistoryContract;
import com.popo.video.ui.message.presenter.ChatHistoryPresenter;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 聊天消息记录列表
 * Created by zhangdroid on 2017/6/10.
 */
public class ChatHistoryFragment extends BaseFragment implements ChatHistoryContract.IView {
    @BindView(R.id.chat_history_root)
    FrameLayout mFlRoot;
    @BindView(R.id.chat_history_refresh)
    AutoSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.chat_history_list)
    RecyclerView mRecyclerView;
    private ChatHistoryPresenter mChatHistoryPresenter;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;
    private boolean flag=true;
    private String huxinid="";
    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_chat_history;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return mSwipeRefreshLayout;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {
    }

    @Override
    protected void initViews() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mChatHistoryPresenter = new ChatHistoryPresenter(this);
        mChatHistoryPresenter.start();
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        // 下拉刷新和上拉加载更多
        mSwipeRefreshLayout.setDistanceToTriggerSync(250);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mChatHistoryPresenter.refresh();
                }
            }
        });
    }

    @Override
    protected void loadData() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mFlRoot, msg);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                   mChatHistoryPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChatHistoryPresenter.refresh();
            }
        });
    }

    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null != mSwipeRefreshLayout && mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void showLoadMore() {
    }

    @Override
    public void hideLoadMore() {
    }

    @Override
    public void showNoMore() {

    }

    @Override
    public ChatHistoryAdapter getChatHistoryAdapter() {
        return null;
    }

    @Override
    public void setAdapter(NewRecyclerAdapter adapter, MatchAdapter matchAdapter){
        //没有添加头布局的
         mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void hideMatch(boolean hide) {

    }

    private void getViewId(View view) {
        ImageView iv_avatar = (ImageView) view.findViewById(R.id.item_chat_history_avatar);
        TextView tv_msgNum = (TextView) view.findViewById(R.id.item_chat_history_tv_msgnum);
        TextView tv_lastMsg = (TextView) view.findViewById(R.id.item_chat_history_message);
        TextView tv_nickname = (TextView) view.findViewById(R.id.item_chat_history_nickname);
        TextView tv_time = (TextView) view.findViewById(R.id.item_chat_is_tv_time);
         iv_avatar.setImageResource(R.drawable.chat_admin);
          tv_msgNum.setVisibility(View.GONE);
        HuanXinUser huanXinUser = DbModle.getInstance().getUserAccountDao().getAccountByHyID("10000");
        if(huanXinUser!=null){
            int msgNum = huanXinUser.getMsgNum();
            if (msgNum==0) {
                tv_msgNum.setVisibility(View.GONE);
            }else{
                tv_msgNum.setVisibility(View.VISIBLE);
                tv_msgNum.setText(String.valueOf(huanXinUser.getMsgNum()));
            }
            tv_lastMsg.setText(huanXinUser.getLastMsg());
            tv_time.setText(TimeUtils.getLocalTime(mContext,System.currentTimeMillis(),Long.parseLong(huanXinUser.getMsgTime())));
        }
        tv_nickname.setText(mContext.getString(R.string.kefu_desc));

    }


    @Override
    public void onResume() {
        super.onResume();
        mChatHistoryPresenter.refresh();
    }
    @Subscribe
    public void onEvent(MessageArrive arrive){
        handler.sendEmptyMessage(1);

    }
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            mChatHistoryPresenter.refresh();
        }
    };
}
