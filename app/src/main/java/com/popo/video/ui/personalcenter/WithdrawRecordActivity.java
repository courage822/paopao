package com.popo.video.ui.personalcenter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.FrameLayout;

import com.popo.video.R;
import com.popo.video.base.BaseTopBarActivity;
import com.popo.library.adapter.wrapper.OnLoadMoreListener;
import com.popo.library.net.NetUtil;
import com.popo.library.util.SnackBarUtil;
import com.popo.library.widget.AutoSwipeRefreshLayout;
import com.popo.library.widget.XRecyclerView;
import com.popo.video.ui.personalcenter.adapter.WithdrawRecordAdapter;
import com.popo.video.ui.personalcenter.contract.WithdrawRecordContract;
import com.popo.video.ui.personalcenter.presenter.WithdrawRecordPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WithdrawRecordActivity extends BaseTopBarActivity implements WithdrawRecordContract.IView{



    @BindView(R.id.withdraw_record)
    FrameLayout mFlRecord;
    @BindView(R.id.withdraw_record_refresh)
    AutoSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.withdraw_record_list)
    XRecyclerView mRecyclerView;
    private WithdrawRecordAdapter mWithdrawRecordAdapter;
    private WithdrawRecordPresenter mWithdrawRecordPresenter;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_withdraw_record;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.person_withdraw_record);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);

         mWithdrawRecordAdapter = new WithdrawRecordAdapter(mContext, R.layout.item_withdraw_record);
        mRecyclerView.setAdapter(mWithdrawRecordAdapter, R.layout.common_load_more);
         mWithdrawRecordPresenter = new WithdrawRecordPresenter(this);
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        // 下拉刷新和上拉加载更多
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mWithdrawRecordPresenter.refresh();
                }
            }
        });
        mRecyclerView.setOnLoadingMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mWithdrawRecordPresenter.loadMore();
            }
        });
//        mWithdrawRecordAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
//                VideoRecord videoRecord = mWithdrawRecordAdapter.getItemByPosition(position);
//                if (null != videoRecord) {
//                    UserBase userBase = videoRecord.getUserBase();
//                    if (null != userBase) {
//                        VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount(),
//                                userBase.getNickName(), userBase.getIconUrlMininum()), getChildFragmentManager());
//                    }
//                }
//            }
//        });

    }

    @Override
    protected void loadData() {
        mWithdrawRecordPresenter.loadHistoryList();

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mFlRecord, msg);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mWithdrawRecordPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowEmpty(false, null, null);
                mWithdrawRecordPresenter.refresh();
            }
        });
    }

    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null != mSwipeRefreshLayout && mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void showLoadMore() {
        mRecyclerView.setVisibility(R.id.load_more_progress, true);
        mRecyclerView.setVisibility(R.id.load_more_msg, true);
        mRecyclerView.setVisibility(R.id.load_more_empty, false);
        mRecyclerView.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
        mRecyclerView.finishLoadMore();
    }

    @Override
    public void showNoMore() {
        mRecyclerView.setVisibility(R.id.load_more_empty, true);
        mRecyclerView.setVisibility(R.id.load_more_progress, false);
        mRecyclerView.setVisibility(R.id.load_more_msg, false);
    }
    @Override
    public WithdrawRecordAdapter getWithdrawRecordAdapter() {
        return mWithdrawRecordAdapter;
    }
}
