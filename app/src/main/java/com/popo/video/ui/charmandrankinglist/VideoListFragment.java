package com.popo.video.ui.charmandrankinglist;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;


import com.popo.library.widget.VerticalViewPager;
import com.popo.video.R;
import com.popo.video.base.BaseFragment;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.ui.charmandrankinglist.contract.VideoListContract;
import com.popo.video.ui.charmandrankinglist.presenter.VideoListPresenter;

import butterknife.BindView;

public class VideoListFragment extends BaseFragment implements VideoListContract.IView {
    @BindView(R.id.vvp_video_list)
    VerticalViewPager viewpager;
    @BindView(R.id.rl_more)
    RelativeLayout rl_more;
    VideoListPresenter videoListPresenter;
    private int mPostion = 1;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_video_list_layout;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return viewpager;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        videoListPresenter = new VideoListPresenter(VideoListFragment.this);
        videoListPresenter.start();
    }

    @Override
    protected void setListeners() {
        rl_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoListPresenter.reportShow(viewpager.getCurrentItem());
            }
        });
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (!UserPreference.isAnchor()
                        && !UserPreference.isVip()
                        && viewpager.getAdapter().getCount() == 5
                        && position ==4){
                    videoListPresenter.loadOneShow();
                }else if ((UserPreference.isAnchor() || UserPreference.isVip())&& viewpager.getAdapter().getCount() - 1 == position) {
                    videoListPresenter.loadVideoShowList(mPostion);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void loadData() {
        videoListPresenter.loadVideoShowList(1);
    }

//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if (!hidden) {
//            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
//            if (viewpager.getAdapter().getCount() == 0 ||fragmentTransaction.isEmpty()) {
//                videoListPresenter.loadVideoShowList(1);
//            }
//        }
//    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void setAdapter(FragmentStatePagerAdapter adapter) {
        viewpager.setAdapter(adapter);

    }

    @Override
    public void setEmptyView(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowEmpty(false, null, null);
                videoListPresenter.loadVideoShowList(1);
            }
        });
    }

    @Override
    public void setErrorView(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                videoListPresenter.loadVideoShowList(1);
            }
        });
    }

    @Override
    public void setPostion() {
        mPostion++;
    }

    @Override
    public Fragment getFragment() {
        return VideoListFragment.this;
    }
}
