package com.popo.video.ui.video.presenter;

import android.Manifest;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.hyphenate.chat.EMMessage;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.common.AgoraHelper;
import com.popo.video.common.HyphenateHelper;
import com.popo.video.common.VideoHelper;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.model.VideoStop;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.event.AgoraEvent;
import com.popo.library.permission.PermissionCallback;
import com.popo.library.permission.PermissionManager;
import com.popo.library.permission.PermissonItem;
import com.popo.library.util.LaunchHelper;
import com.popo.video.parcelable.VideoParcelable;
import com.popo.video.ui.video.VideoActivity;
import com.popo.video.ui.video.contract.VideoInviteContract;

import java.io.IOException;

/**
 * Created by zhangdroid on 2017/6/7.
 */
public class VideoInvitePresenter implements VideoInviteContract.IPresenter {
    private VideoInviteContract.IView mVideoInviteView;
    private Context mContext;
    private long mGuid;
    private String mAccount;
    private String mNickname;
    private String mHostPrice;
    private Camera mCamera;
    private int type;
    private String mUserPic;

    public VideoInvitePresenter(VideoInviteContract.IView view, long guid, String account, String nickname, String hostPrice, int type, String userPic) {
        this.mVideoInviteView = view;
        this.mContext = view.obtainContext();
        this.mGuid = guid;
        this.mAccount = account;
        this.mNickname = nickname;
        this.mHostPrice = hostPrice;
        this.type = type;
        this.mUserPic = userPic;
    }

    @Override
    public void start() {
    }

    @Override
    public void startPreView() {
        // 检查录音和摄像头权限
        PermissionManager.getInstance(mContext)
                .addPermission(new PermissonItem(C.permission.PERMISSION_CAMERA, mContext.getString(R.string.permission_camera), R.drawable.camera_permission))
                .addPermission(new PermissonItem(C.permission.PERMISSION_RECORD_AUDIO, mContext.getString(R.string.permission_record), R.drawable.voice_permission))
                .checkMutiPermission(new PermissionCallback() {
                    @Override
                    public void onGuaranteed(String permisson, int position) {
                        if (Manifest.permission.CAMERA.equals(permisson)) {
                            openCamera();
                            setPreview();
                        }
                    }

                    @Override
                    public void onDenied(String permisson, int position) {
                        if (Manifest.permission.CAMERA.equals(permisson)) {
                            mVideoInviteView.hideSurfaceView();
                            mVideoInviteView.showTip(mContext.getString(R.string.video_permission_camera));
                        } else if (Manifest.permission.RECORD_AUDIO.equals(permisson)) {
                            mVideoInviteView.showTip(mContext.getString(R.string.video_permission_record));
                        }
                        mVideoInviteView.finishActivity(2);
                    }

                    @Override
                    public void onFinished() {
                        mVideoInviteView.hideSurfaceView();
                    }

                    @Override
                    public void onClosed() {
                        mVideoInviteView.hideSurfaceView();
                    }
                });
    }

    @Override
    public void stopPreview() {
        releasePreview();
    }

    @Override
    public void startInvite(String guid, String account, String userInfo) {
        AgoraHelper.getInstance().startInvite(guid, account, userInfo);
    }

    @Override
    public void cancelInVite(String guid, String account, String id) {
        AgoraHelper.getInstance().stopInvite(guid, account);
        closePhone(guid, id);
        cancelMessage(1);
    }

    public void cancelMessage(final int choice) {
        if (!TextUtils.isEmpty(mAccount))
            ApiManager.interruptText(Long.valueOf(mGuid), (type == 1 ? mContext.getString(R.string.voice_call) : mContext.getString(R.string.video_call))
                    + (choice == 1 ? mContext.getString(R.string.canceled) : mContext.getString(R.string.refused)), new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                    HyphenateHelper.getInstance().sendTextMessage(mAccount, (type == 1 ? mContext.getString(R.string.voice_call) : mContext.getString(R.string.video_call))
                                    + (choice == 1 ? mContext.getString(R.string.canceled) : mContext.getString(R.string.refused)),
                            new HyphenateHelper.OnMessageSendListener() {
                                @Override
                                public void onSuccess(EMMessage emMessage) {
                                    Log.e("cancel",emMessage.toString());
                                }

                                @Override
                                public void onError() {
                                }
                            });
                    HyphenateHelper.getInstance().markMsgAsRead(mAccount);
                    saveDataToSqlit(mNickname, String.valueOf(mGuid), mAccount, mUserPic,
                            (type == 1 ? mContext.getString(R.string.voice_call)
                                    : mContext.getString(R.string.video_call))
                                    + (choice == 1 ? mContext.getString(R.string.canceled)
                                    : mContext.getString(R.string.refused)),
                            String.valueOf(System.currentTimeMillis()), 1);
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }
            });
    }

    private void saveDataToSqlit(String name, String id, String account, String pic, String msg, String time, int extendType) {
        if (!TextUtils.isEmpty(id)) {
            HuanXinUser user = new HuanXinUser(id, name, pic, account, "1", msg, 0, time, extendType);
            DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
        }
    }

    @Override
    public void acceptInvite(final String channelId, final String account) {
        VideoHelper.acceptVideoInvite(new VideoParcelable(mGuid, mAccount, mUserPic, mNickname, channelId, "0", type, true), channelId, mContext,"1");
    }

    @Override
    public void refuseInvite(String channelId, String account, String guid) {
        AgoraHelper.getInstance().refuseInvite(channelId, account);
        AgoraHelper.getInstance().leaveChannel(channelId);
        mVideoInviteView.finishActivity(1);
        closePhone(channelId, guid);
        cancelMessage(2);
    }

    @Override
    public void handleAgoraEvent(AgoraEvent agoraEvent) {
        if (null != agoraEvent) {
            switch (agoraEvent.eventCode) {
                case AgoraHelper.EVENT_CODE_INVITE_FAILED:// 发起视频呼叫失败

                    AgoraHelper.getInstance().leaveChannel(agoraEvent.channelId);
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            if (mVideoInviteView != null) {
                                mVideoInviteView.changeInviteState(mContext.getString(R.string.video_invite_failed));
                                mVideoInviteView.finishActivity(2);
                            }
                        }
                    }, 50_000);
                    break;
                case AgoraHelper.EVENT_CODE_INVITE_SUCCESS:// 发起视频呼叫成功
                    mVideoInviteView.changeInviteState(mContext.getString(R.string.video_inviting));
                    break;
                case AgoraHelper.EVENT_CODE_INVITE_CANCEL:// 取消已经发起的视频呼叫
                    AgoraHelper.getInstance().leaveChannel(agoraEvent.channelId);
                    mVideoInviteView.finishActivity(1);
                    break;
                case AgoraHelper.EVENT_CODE_INVITE_ACCEPT:// 对方接受了视频呼叫
                    LaunchHelper.getInstance().launchFinish(mContext, VideoActivity.class,
                            new VideoParcelable(mGuid, mAccount, mUserPic, mNickname, agoraEvent.channelId, mHostPrice, type, false));
                    break;
                case AgoraHelper.EVENT_CODE_INVITE_REFUSE:// 对方拒绝了视频呼叫
                    AgoraHelper.getInstance().leaveChannel(agoraEvent.channelId);
                    if (type == 1) {
                        mVideoInviteView.changeInviteState(mContext.getString(R.string.video_invite_refused2));
                    } else {
                        mVideoInviteView.changeInviteState(mContext.getString(R.string.video_invite_refused));
                    }
                    mVideoInviteView.finishActivity(2);
                    break;

                case AgoraHelper.EVENT_CODE_INVITE_END_PEER:// 发起方取消了视频呼叫
                    if (type == 1) {
                        mVideoInviteView.changeInviteState(mContext.getString(R.string.video_invite_canceled2));
                    } else {
                        mVideoInviteView.changeInviteState(mContext.getString(R.string.video_invite_canceled));
                    }
                    mVideoInviteView.finishActivity(2);
                    break;
            }
        }
    }

    private void openCamera() {
        // 打开相机并设置默认为前置摄像头
        int numberOfCameras = Camera.getNumberOfCameras();
        CameraInfo cameraInfo = new CameraInfo();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
                mCamera = Camera.open(i);
                break;
            }
        }
    }

    private void setPreview() {
        if (null != mCamera) {
            // 设置SurfaceView并开始预览
            try {
                mCamera.setPreviewDisplay(mVideoInviteView.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }
            // 竖屏需要旋转90度预览
            mCamera.setDisplayOrientation(90);
            mCamera.startPreview();
        }
    }

    private void releasePreview() {
        if (null != mCamera) {
            // 关闭预览并释放资源
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    private void closePhone(final String channelId, String guid) {
        // 挂断视频前先通知后台
        ApiManager.videoStop(guid, UserPreference.getId(), new IGetDataListener<VideoStop>() {
            @Override
            public void onResult(VideoStop videoStop, boolean isEmpty) {

                AgoraHelper.getInstance().leaveChannel(channelId);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                AgoraHelper.getInstance().leaveChannel(channelId);
            }
        });
    }

    public void iosPush(String id, String status, String callType) {
        ApiManager.iosCallPush(id, status, callType, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });

    }
}
