package com.popo.video.ui.personalcenter.contract;


import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

/**
 * Created by Administrator on 2017/6/16.
 */

public interface VideoShowContract {
    interface IPresenter extends BasePresenter {

        /**
         * 传递参数
         */
        void sendParams(String videoNumber, String videoFile, String author,
                        String showDuration, String videoShowFile, String bitmapFile,
                        String voiceDuration, String voiceFile, String ownWord
        );

        /**
         * 提交真人认证
         */
        void submitAuth(String videoNumber, String videoFile, String author);

        /**
         * 提交视频秀
         */
        void submitShow(String ownWord, String videoFilePath, String bitmapFilePath, String videoDuration,
                        String voiceFilePath, String voiceDuration);


    }

    interface IView extends BaseView {

        void showLoading();

        void dismissLoading();

        void showNetworkError();

        /**
         * 销毁Activity
         */
        void finishActivity();

        void isShowFail();

    }
}
