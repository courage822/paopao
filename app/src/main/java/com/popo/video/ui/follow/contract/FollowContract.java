package com.popo.video.ui.follow.contract;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;

import com.popo.library.adapter.wrapper.OnLoadMoreListener;
import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

/**
 * Created by Administrator on 2017/6/14.
 */

public interface FollowContract {
    interface IView extends BaseView {
        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        void toggleShowError(boolean toggle, String msg);

        void toggleShowEmpty(boolean toggle, String msg);

        /**
         * @return 获得FragmentManager，用于显示对话框
         */
        FragmentManager obtainFragmentManager();

        /**
         * 设置adapter
         *
         * @param adapter        adapter
         * @param loadMoreViewId 加载更多View
         */
        void setAdapter(RecyclerView.Adapter adapter, int loadMoreViewId);

        /**
         * 设置加载更多监听器
         *
         * @param onLoadMoreListener
         */
        void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener);

        /**
         * 显示加载更多
         */
        void showLoadMore();

        /**
         * 隐藏加载更多
         */
        void hideLoadMore();

        /**
         * 显示没有更多（滑动到最底部）
         */
        void showNoMore();
    }

    interface IPresenter extends BasePresenter {

        /**
         * 加载关注用户列表
         *
         */
        void loadFollowUserList();

        /**
         * 下拉刷新
         */
        void refresh();
        /**
         * 跳转搜索界面
         */
        void goToSearchPage();
        /**
         * 初始化数据
         */
        void initFirstData();
    }
}
