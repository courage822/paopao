package com.popo.video.ui.register.contract;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/5/26.
 */
public interface RegisterContract {

    interface IView extends BaseView {

        String getNickname();

        boolean getSex();

        String getAge();

        void showLoading();

        void dismissLoading();

        void showNetworkError();

        void setRegisterEnable();

    }

    interface IPresenter extends BasePresenter {
        /**
         * 注册
         */
        void register();

        /**
         * 跳转登录页面
         */
        void goLoginPage();

        /**
         * 显示协议网页
         *
         * @param title 需要显示的协议标题
         * @param url   需要显示的协议url
         */
        void showAgreement(String title, String url);

        /**
         * 检测是否可以注册
         */
        void checkValid();
    }

}
