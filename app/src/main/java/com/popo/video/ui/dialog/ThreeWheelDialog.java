package com.popo.video.ui.dialog;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.library.dialog.BaseDialogFragment;
import com.popo.library.widget.WheelView;


/**
 * 显示两个滚轮的对话框
 * Created by zhangdroid on 2016/6/24.
 */
public abstract class ThreeWheelDialog extends BaseDialogFragment {
    private OnThreeWheelDialogClickListener mOnDialogClickListener;

    /**
     * 设置WheelView1属性
     *
     * @param wheelView
     */
    protected abstract void setWheelView1(WheelView wheelView);

    /**
     * 设置WheelView2属性
     *
     * @param wheelView
     */
    protected abstract void setWheelView2(WheelView wheelView);

    /**
     * 设置WheelView3属性
     *
     * @param wheelView
     */
    protected abstract void setWheelView3(WheelView wheelView);

    protected abstract OnThreeWheelDialogClickListener setOnDialogClickListener();

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_three_wheel;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tvTitle = (TextView) view.findViewById(R.id.dialog_three_wheel_title);
        View dividerView = view.findViewById(R.id.dialog_three_wheel_divider);// 标题栏下分割线
        final WheelView wheelView1 = (WheelView) view.findViewById(R.id.dialog_three_wheelview1);
        final WheelView wheelView2 = (WheelView) view.findViewById(R.id.dialog_three_wheelview2);
        final WheelView wheelView3 = (WheelView) view.findViewById(R.id.dialog_three_wheelview3);
        Button btnPositive = (Button) view.findViewById(R.id.dialog_three_wheel_sure);
        Button btnNegative = (Button) view.findViewById(R.id.dialog_three_wheel_cancel);

        setWheelView1(wheelView1);
        setWheelView2(wheelView2);
        setWheelView3(wheelView3);
        mOnDialogClickListener = setOnDialogClickListener();

        String title = getDialogTitle();
        if (!TextUtils.isEmpty((title))) {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
            dividerView.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
        }

        String positive = getDialogPositive();
        if (!TextUtils.isEmpty((positive))) {
            btnPositive.setText(positive);
        }

        String negative = getDialogNegative();
        if (!TextUtils.isEmpty((negative))) {
            btnNegative.setText(negative);
        }

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDialogClickListener != null) {
                    mOnDialogClickListener.onPositiveClick(v, wheelView1.getSelectedText(), wheelView2.getSelectedText(), wheelView3.getSelectedText());
                }
                dismiss();
            }
        });
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDialogClickListener != null) {
                    mOnDialogClickListener.onNegativeClick(v);
                }
                dismiss();
            }
        });
    }

    public interface OnThreeWheelDialogClickListener {
        /**
         * Called when the dialog positive button has been clicked.
         *
         * @param view          The View that was clicked.
         * @param selectedText1 The first wheelview selected item text.
         * @param selectedText2 The second wheelview selected item text.
         * @param selectedText3 The third wheelview selected item text.
         */
        void onPositiveClick(View view, String selectedText1, String selectedText2, String selectedText3);

        /**
         * Called when the dialog negative button has been clicked.
         *
         * @param view The View that was clicked.
         */
        void onNegativeClick(View view);
    }

}
