package com.popo.video.ui.chat;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.common.Util;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.net.NetUtil;
import com.popo.library.util.ToastUtil;
import com.popo.video.parcelable.PrivateMessageParcelable;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

/**
 * Created by xuzhaole on 2017/12/6.
 */

public class VideoPlayerActivity extends BaseFragmentActivity implements MediaPlayer.OnBufferingUpdateListener,
        MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener,
        SurfaceHolder.Callback {
    @BindView(R.id.chat_activity_iv_back)
    ImageView chatActivityIvBack;
    @BindView(R.id.chat_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.chat_activity_tv_nickname)
    TextView tv_title;
    @BindView(R.id.chat_activity_rl_set)
    RelativeLayout rl_set;
    @BindView(R.id.chat_activity_toobar_rl)
    RelativeLayout chatActivityToobarRl;
    @BindView(R.id.iv_picture)
    ImageView iv_picture;
    @BindView(R.id.tv_content)
    TextView tv_content;
    @BindView(R.id.ll_photo)
    LinearLayout ll_photo;
    @BindView(R.id.ll_play)
    LinearLayout ll_play;
    @BindView(R.id.surface)
    SurfaceView surfaceView;
    @BindView(R.id.iv_play)
    ImageView iv_play;
    @BindView(R.id.rl_video)
    RelativeLayout rl_video;
    @BindView(R.id.tv_time)
    TextView tv_time;
    @BindView(R.id.tv_loading)
    TextView tv_loading;

    private PrivateMessageParcelable mParcelable;
    private int videoWidth;
    private int videoHeight;
    public MediaPlayer mediaPlayer;
    private SurfaceHolder surfaceHolder;
    private Timer mTimer = new Timer();

    TimerTask mTimerTask = new TimerTask() {
        @Override
        public void run() {
            if (mediaPlayer == null)
                return;
        }
    };
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_video_player;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mParcelable = (PrivateMessageParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        if (mParcelable != null) {
            if (mParcelable.getBaseType() == 3) {//图片
                ll_photo.setVisibility(View.VISIBLE);
                rl_video.setVisibility(View.GONE);
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage())
                        .url(mParcelable.getUrl()).imageView(iv_picture).build());
                tv_content.setText(TextUtils.isEmpty(mParcelable.getContent()) ? "" : Util.decode(mParcelable.getContent()));
            } else {//视频
                rl_video.setVisibility(View.VISIBLE);
                ll_photo.setVisibility(View.GONE);
                Integer audioSeconds = mParcelable.getAudioSeconds();
                String time = null;
                if (audioSeconds >= 60) {
                    if (audioSeconds % 60 > 9){
                        time = (audioSeconds / 60) + ":" + (audioSeconds % 60);
                    }else{
                        time = (audioSeconds / 60) + ":0" + (audioSeconds % 60);
                    }
                } else {
                    if (audioSeconds > 9){
                        time = "00:" + audioSeconds;
                    }else{
                        time = "00:0" + audioSeconds;
                    }
                }
                tv_time.setText(time);
                surfaceHolder = surfaceView.getHolder();
                surfaceHolder.addCallback(this);
                surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
                mTimer.schedule(mTimerTask, 0, 1000);
            }
        }
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(mParcelable.getUrl())) {
                    ll_play.setVisibility(View.GONE);
                    tv_loading.setVisibility(View.VISIBLE);
                    try {
                        mediaPlayer.reset();
                        mediaPlayer.setDataSource(mParcelable.getUrl());
                        mediaPlayer.prepare();//prepare之后自动播放
//            mediaPlayer.start();
                    } catch (IllegalArgumentException e) {
                        // TODO Auto-generated catch block

                        e.printStackTrace();
                    } catch (IllegalStateException e) {
                        // TODO Auto-generated catch block

                        e.printStackTrace();

                    } catch (IOException e) {
                        // TODO Auto-generated catch block

                        e.printStackTrace();
                    }
                } else {
                    ToastUtil.showShortToast(mContext, mContext.getString(R.string.user_detail_voice));
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stop();
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder arg0) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDisplay(surfaceHolder);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnBufferingUpdateListener(this);
            mediaPlayer.setOnPreparedListener(this);
        } catch (Exception e) {
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        Log.e("mediaPlayer", "surface destroyed");
    }

    @Override
    /**
     * 通过onPrepared播放
     */
    public void onPrepared(MediaPlayer arg0) {
        videoWidth = mediaPlayer.getVideoWidth();
        videoHeight = mediaPlayer.getVideoHeight();
        if (videoHeight != 0 && videoWidth != 0) {
            arg0.start();
            tv_loading.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCompletion(MediaPlayer arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onBufferingUpdate(MediaPlayer arg0, int bufferingProgress) {

    }


}
