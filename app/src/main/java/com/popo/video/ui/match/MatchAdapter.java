package com.popo.video.ui.match;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.popo.library.adapter.base.BaseQuickAdapter;
import com.popo.library.adapter.base.BaseViewHolder;
import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.parcelable.ChatParcelable;
import com.popo.video.parcelable.MatchParcelable;
import com.popo.video.ui.chat.ChatActivity;
import com.popo.video.ui.detail.UserDetailActivity;

import java.util.List;

/**
 * Created by xuzhaole on 2018/4/28.
 */

public class MatchAdapter extends BaseQuickAdapter<HuanXinUser, BaseViewHolder> {

    public MatchAdapter(@Nullable List<HuanXinUser> data) {
        super(R.layout.item_match, data);
    }

    @Override
    protected void convert(BaseViewHolder holder, final HuanXinUser user) {
        if (user != null && !TextUtils.isEmpty(user.getHxId())) {
            ImageLoaderUtil.getInstance().loadImage(mContext,
                    new com.popo.library.image.ImageLoader.Builder().url(user.getHxIcon())
                            .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle())
                            .imageView((ImageView) holder.getView(R.id.iv_match)).build());

            TextView tv_name = holder.getView(R.id.tv_name);
            tv_name.setText(user.getHxName());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LaunchHelper.getInstance().launch(mContext, MatchChatActivity.class,
                            new MatchParcelable(Long.parseLong(user.getHxId()),
                                    user.getAccount(), user.getHxName(), user.getHxIcon()));
                }
            });
        }
    }
}
