package com.popo.video.ui.register.contract;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

import java.io.File;

/**
 * Created by zhangdroid on 2017/5/26.
 */
public interface CompleteInfoContract {

    interface IView extends BaseView {
        void showLoading();
        void dismissLoading();
        void showNetworkError();
        String getHeigth();
        String getQingGan();
        String getSign();
        void setUserAvator(String url);
        void canOnClick();
        void getPhotoFile(File file);
        void isSeeZheZhao(boolean flag);
        void uploadFail();
    }

    interface IPresenter extends BasePresenter {
           void completeInfo();
           void upLoadAvator(File file, boolean flag);
           void loadInfoData();
    }

}
