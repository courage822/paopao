package com.popo.video.ui.charmandrankinglist.contract;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;

import com.popo.library.adapter.wrapper.OnLoadMoreListener;
import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

/**
 * Created by Administrator on 2017/6/15.
 */

public interface CharmandListContract {
    interface IView extends BaseView {

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        void toggleShowError(boolean toggle, String msg);

        void toggleShowEmpty(boolean toggle, String msg);

        /**
         * @return 获得FragmentManager，用于显示对话框
         */
        FragmentManager obtainFragmentManager();

        /**
         * 设置加载更多监听器
         *
         * @param onLoadMoreListener
         */
        void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener);

        /**
         * 显示加载更多
         */
        void showLoadMore();

        /**
         * 隐藏加载更多
         */
        void hideLoadMore();

        /**
         * 显示没有更多（滑动到最底部）
         */
        void showNoMore();
        /**
         * 设置charmandadapter
         */
        void setAdapter(RecyclerView.Adapter adapter, int loadMoreViewId);
        /**
         * 头布局中的头像和名称礼物
         */
        void setAvatarAndGift1(String avatar, String name, int gift);
        /**
         * 头布局中的头像和名称礼物
         */
        void setAvatarAndGift2(String avatar, String name, int gift);
        /**
         * 头布局中的头像和名称礼物
         */
        void setAvatarAndGift3(String avatar, String name, int gift);


    }

    interface IPresenter extends BasePresenter {
        /**
         * 加载魅力榜用户列表
         */
        void loadCharmandUserList();

        /**
         * 下拉刷新
         */
        void refresh1();

    }
}
