package com.popo.video.ui.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.popo.library.net.NetUtil;
import com.popo.library.util.DeviceUtil;
import com.popo.library.widget.tab.ItemBadge;
import com.popo.library.widget.tab.OnTabSelectedListener;
import com.popo.library.widget.tab.TabLayout;
import com.popo.library.widget.tab.TabView;
import com.popo.video.R;
import com.popo.video.base.BaseAppCompatActivity;
import com.popo.video.common.AgoraHelper;
import com.popo.video.common.CustomDialogAboutPay;
import com.popo.video.common.HyphenateHelper;
import com.popo.video.common.TimeUtils;
import com.popo.video.common.YeMeiPopopUtil;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.model.NettyMessage;
import com.popo.video.data.model.SearchUser;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.event.FinishEvent;
import com.popo.video.event.FollowEvent;
import com.popo.video.event.PauseEvent;
import com.popo.video.event.RefreshEvent;
import com.popo.video.event.RegisterAndLoginFinish;
import com.popo.video.event.SingleLoginFinishEvent;
import com.popo.video.event.StartEvent;
import com.popo.video.event.UnreadMsgChangedEvent;
import com.popo.video.event.YeMeiMessageEvent;
import com.popo.video.ui.charmandrankinglist.VideoListFragment;
import com.popo.video.ui.follow.FollowFragment;
import com.popo.video.ui.homepage.HomepageFragment;
import com.popo.video.ui.main.contract.MainActivityContract;
import com.popo.video.ui.main.presenter.MainActivityPresenter;
import com.popo.video.ui.message.MessageFragment;
import com.popo.video.ui.personalcenter.PersonFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;

/**
 * 主Activity，包含所有tab
 * Created by zhangdroid on 2017/5/12.
 */
public class MainActivity extends BaseAppCompatActivity implements MainActivityContract.IView {
    @BindView(R.id.main_tab)
    TabLayout mTabLayout;
    @BindView(R.id.main_activity_tv_tab)
    TextView tv_tab;
    @BindView(R.id.rl_all)
    RelativeLayout rl_all;
    @BindView(R.id.rl_root)
    RelativeLayout rl_root;
    @BindView(R.id.fl_main)
    FrameLayout fl_main;
    private int position = 0;

    // 消息tab角标
    private ItemBadge mTabMessageBadge;
    private int loginNum = 0;
    private static boolean flag = false;
    private boolean isVipUser = false;
    private MainActivityPresenter mainActivityPresenter;
    private boolean isCanShowPop = true;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return fl_main;
    }

    @Override
    protected void initViews() {
        setMarginTop(1);

        // 设置消息tab角标
        mTabMessageBadge = new ItemBadge(Color.parseColor("#ff0000"), 12, Gravity.TOP | Gravity.RIGHT).hide();
        mTabMessageBadge.setTextColor(Color.WHITE);
        // 设置tab
        mTabLayout.addTabView(new TabView(getString(R.string.tab_homepage), R.drawable.tab_homepage_normal, R.drawable.tab_homepage_selected))
                .addTabView(new TabView(getString(R.string.tab_message), R.drawable.tab_message_normal, R.drawable.tab_message_selected))
                .addTabView(new TabView(getString(R.string.tab_squarelist), R.drawable.tab_square_normal, R.drawable.tab_square_selected))
                .addTabView(new TabView(getString(R.string.tab_follow), R.drawable.tab_follow_normal, R.drawable.tab_follow_selected))
                .addTabView(new TabView(getString(R.string.tab_person), R.drawable.tab_person_normal, R.drawable.tab_person_selected))
                .setFirstSelectedPosition(0)
                .initialize();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fl_main, new HomepageFragment(), "homepage");
        fragmentTransaction.add(R.id.fl_main, new MessageFragment(), "message");
        fragmentTransaction.add(R.id.fl_main, new VideoListFragment(), "video");
        fragmentTransaction.add(R.id.fl_main, new FollowFragment(), "follow");
        fragmentTransaction.add(R.id.fl_main, new PersonFragment(), "person");
        fragmentTransaction.commit();


        HyphenateHelper.getInstance().setOnClick();
        EventBus.getDefault().post(new RegisterAndLoginFinish());
        mainActivityPresenter = new MainActivityPresenter(this);
        mainActivityPresenter.loadData();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (UserPreference.isRegister() && UserPreference.isMale()) {
//                    mainActivityPresenter.makeFreeVideo();
//                }
//            }
//        }, 2000);
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<HuanXinUser> allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
                if (allAcount != null && allAcount.size() != 0) {
                    for (HuanXinUser user : allAcount) {
                        int converUnreadMsgCount = HyphenateHelper.getInstance().getConverUnreadMsgCount(user.getAccount());
                        if (converUnreadMsgCount > 0) {
                            List<EMMessage> emMessages = HyphenateHelper.getInstance().getUnReadMsg(user.getAccount());
                            if (emMessages != null && emMessages.size() > 0) {
                                user.setMsgNum(converUnreadMsgCount);
                                String lastMsg = getString(R.string.how_are_you);
                                if (emMessages.get(emMessages.size() - 1).getType() == EMMessage.Type.TXT) {
                                    NettyMessage nettyMessage = setMsgContent(emMessages.get(emMessages.size() - 1).getStringAttribute("msg", ""));
                                    if (nettyMessage != null) {
                                        EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessages.get(emMessages.size() - 1).getBody();
                                        if (nettyMessage.getExtendType() == 2 || emTextMessageBody.getMessage().equals("msgCall")) {
                                            lastMsg = getString(R.string.video_invitations);
                                        } else if (nettyMessage.getExtendType() == 3) {
                                            lastMsg = getString(R.string.one_private_message);
                                        } else if (nettyMessage.getExtendType() == 5) {
                                            lastMsg = getString(R.string.gift);
                                        } else if(nettyMessage.getExtendType() == 19){
                                            lastMsg = getString(R.string.voice_message);
                                        } else {
                                            if(emTextMessageBody.getMessage().contains(".mp3")){
                                                lastMsg = getString(R.string.voice_message);
                                            }else{
                                                lastMsg = emTextMessageBody.getMessage();
                                            }
                                        }
                                    }
                                } else if (emMessages.get(emMessages.size() - 1).getType() == EMMessage.Type.VOICE) {
                                    lastMsg = getString(R.string.voice_message);
                                } else if (emMessages.get(emMessages.size() - 1).getType() == EMMessage.Type.IMAGE) {
                                    lastMsg = getString(R.string.photo_message);
                                }
                                user.setLastMsg(lastMsg);
                                user.setMsgTime(String.valueOf(emMessages.get(emMessages.size() - 1).getMsgTime()));
                                DbModle.getInstance().getUserAccountDao().addAccount(user);
                            }
                        }
                    }
                }
            }
        }).start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        changeFragment(position);
    }

    private void changeFragment(int i) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        HomepageFragment homepageFragment = (HomepageFragment) manager.findFragmentByTag("homepage");
        MessageFragment messageFragment = (MessageFragment) manager.findFragmentByTag("message");
        VideoListFragment videoListFragment = (VideoListFragment) manager.findFragmentByTag("video");
        FollowFragment followFragment = (FollowFragment) manager.findFragmentByTag("follow");
        PersonFragment personFragment = (PersonFragment) manager.findFragmentByTag("person");
        if (i == 0) {
            position = 0;
            transaction.show(homepageFragment)
                    .hide(messageFragment).hide(videoListFragment).hide(followFragment).hide(personFragment);
            EventBus.getDefault().post(new PauseEvent());
        } else if (i == 1) {
            position = 1;
            transaction.show(messageFragment)
                    .hide(homepageFragment).hide(videoListFragment).hide(followFragment).hide(personFragment);
            EventBus.getDefault().post(new PauseEvent());
        } else if (i == 2) {
            position = 2;
            transaction.show(videoListFragment)
                    .hide(messageFragment).hide(homepageFragment).hide(followFragment).hide(personFragment);
            EventBus.getDefault().post(new StartEvent());

        } else if (i == 3) {
            position = 3;
            transaction.show(followFragment)
                    .hide(messageFragment).hide(videoListFragment).hide(homepageFragment).hide(personFragment);
            EventBus.getDefault().post(new PauseEvent());

        } else {
            position = 4;
            transaction.show(personFragment)
                    .hide(messageFragment).hide(videoListFragment).hide(followFragment).hide(homepageFragment);
            EventBus.getDefault().post(new PauseEvent());

        }

        transaction.commit();
    }

    public int getPosition() {
        return position;
    }


    private void setMarginTop(int top) {
        if (top == 0) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup
                    .LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, 0, 0, 0);
            rl_root.setLayoutParams(layoutParams);
        } else {
            int statusBarHeight = DeviceUtil.getStatusBarHeight(mContext);
            int barHeight = DeviceUtil.px2dip(mContext, statusBarHeight);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup
                    .LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, barHeight, 0, 0);
            rl_root.setLayoutParams(layoutParams);
        }

    }

    //解析发过来的数据
    private NettyMessage setMsgContent(String msgContent) {
        if (!TextUtils.isEmpty(msgContent)) {
            NettyMessage nettyMessage = new Gson().fromJson(msgContent, NettyMessage.class);
            return nettyMessage;
        } else {
            return null;
        }
    }

    @Override
    protected void setListeners() {
        mTabLayout.setOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position) {
                if (HyphenateHelper.getInstance().getUnreadMsgCount() == 0) {
                    mTabMessageBadge.setText("").hide();
                }
                if (position == 2) {
                    mTabLayout.setBackgroundColor(Color.TRANSPARENT);
                    rl_all.setBackgroundColor(Color.TRANSPARENT);
                    setMarginTop(0);
                } else {
                    mTabLayout.setBackgroundColor(Color.WHITE);
                    rl_all.setBackgroundColor(Color.parseColor("#fd698b"));
//                    rl_all.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.main_color));
                    setMarginTop(1);
                }
                changeFragment(position);
                EventBus.getDefault().post(new RefreshEvent());
            }
        });
    }

    @Override
    protected void loadData() {
        // 登陆声网信令系统
//        AgoraHelper.getInstance().login();
        HyphenateHelper.getInstance().isNotChatActivity();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        flag = isFinishing();
    }

    @Override
    public void onBackPressed() {

    }

    // 用来计算返回键的点击间隔时间
    private long exitTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                //弹出提示，可以有多种方式
                Toast.makeText(getApplicationContext(), getString(R.string.exit), Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
            }
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Subscribe
    public void onEvent(UnreadMsgChangedEvent event) {
        //判断，如果存在没有未读消息，却存在未读数量的情况清空
        if (DbModle.getInstance().getUserAccountDao().selectSqlit() == 0) {
            HyphenateHelper.getInstance().clearAllUnReadMsg();
        }
        Message msg = Message.obtain();
        msg.obj = event;
        handler.sendMessage(msg);
    }

    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        flag = isFinishing();
        if (DbModle.getInstance().getUserAccountDao().queryMsgNum() > 0) {
            if (DbModle.getInstance().getUserAccountDao().queryMsgNum() >= 99) {
                tv_tab.setText("99+");
            } else {
                tv_tab.setText(String.valueOf(DbModle.getInstance().getUserAccountDao().queryMsgNum()));
            }
            tv_tab.setVisibility(View.VISIBLE);
        } else {
            tv_tab.setVisibility(View.GONE);
        }
        // 登陆声网信令系统
        AgoraHelper.getInstance().login();
        // 登录过环信，加载
        if (HyphenateHelper.getInstance().isLoggedIn()) {
            // 登录成功后加载聊天会话
            new Thread(new Runnable() {
                @Override
                public void run() {
                    EMClient.getInstance().chatManager().loadAllConversations();
                }
            }).start();
        } else {
            HyLogin();
        }
        if (TimeUtils.timeIsPast()) {//判断页脚是否过期
            if (isCanShowPop) {
                msgHandler.sendEmptyMessage(1);
            }
        } else if (!TimeUtils.timeIsPast()) {
            msgHandler.sendEmptyMessage(2);
        }
    }

    private void HyLogin() {
        HyphenateHelper.getInstance().login(UserPreference.getAccount(), UserPreference.getPassword(), new HyphenateHelper.OnLoginCallback() {
            public void onSuccess() {

            }

            @Override
            public void onFailed() {
                Log.e("AAAAAAA", "onFailed: 登录失败---------======" + loginNum);
                ++loginNum;
                if (loginNum >= 2) {
                    return;
                }
                HyLogin();//登录失败5次之后不再登录
            }
        });
    }

    @Override
    public void onActionModeFinished(ActionMode mode) {
        super.onActionModeFinished(mode);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int msgNum = DbModle.getInstance().getUserAccountDao().queryMsgNum();
            if (msgNum == 0) {
                tv_tab.setVisibility(View.GONE);
                mTabMessageBadge.setText("").hide();
            } else {
                tv_tab.setVisibility(View.VISIBLE);
                if (msgNum >= 99) {
                    tv_tab.setText("99+");
                } else {
                    tv_tab.setText(String.valueOf(msgNum));
                }
                mTabMessageBadge.setText(String.valueOf(msgNum));
            }
        }
    };

    public static boolean nowIsFinish() {
        return flag;
    }


    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void sayHellAll(List<SearchUser> searchUserList1) {
        CustomDialogAboutPay.sayHelloShow(MainActivity.this, searchUserList1);
    }

    @Override
    public void isVipUser() {
        isVipUser = true;
    }

    @Subscribe
    public void onEvent(NettyMessage event) {
        msgHandler.sendEmptyMessage(1);
        isCanShowPop = false;//只让悬浮页脚显示一次
    }

    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    closeShowPop();
                    break;
            }
        }
    };

    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }

    @Override
    protected void closeShowPop() {
        super.closeShowPop();
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }

    @Subscribe
    public void onEvent(FollowEvent event) {
        changeFragment(0);
        mTabLayout.setFirstSelectedPosition(0).initialize();
    }
    @Subscribe
    public void onEvent(YeMeiMessageEvent evet){
        Message msg=new Message();
        msg.obj=evet;
        yeMeiHandler.sendMessage(msg);

    }
    private Handler yeMeiHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            YeMeiMessageEvent obj = (YeMeiMessageEvent) msg.obj;
            if(obj!=null){
                YeMeiPopopUtil.getInstance().showYeMei(MainActivity.this,fl_main,obj,isFinishing());
            }
        }
    };
}
