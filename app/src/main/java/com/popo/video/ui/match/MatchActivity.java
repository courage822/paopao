package com.popo.video.ui.match;

import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.R;
import com.popo.video.base.BaseAppCompatActivity;
import com.popo.video.common.Util;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.parcelable.MatchParcelable;

import butterknife.BindView;

public class MatchActivity extends BaseAppCompatActivity {
    @BindView(R.id.tv_match_with)
    TextView tv_match_with;
    @BindView(R.id.tv_finish)
    TextView tv_finish;
    @BindView(R.id.iv_match_mine)
    ImageView iv_match_mine;
    @BindView(R.id.iv_match_person)
    ImageView iv_match_person;
    @BindView(R.id.btn_match_chat)
    Button btn_match_chat;
    @BindView(R.id.rl_back_match)
    RelativeLayout rl_back_match;

    private MatchParcelable mMatchParcelable;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_match;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mMatchParcelable = (MatchParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        if (mMatchParcelable != null) {
            tv_match_with.setText(getString(R.string.match_chat_with, mMatchParcelable.nickName));

            ImageLoaderUtil.getInstance().loadImage(this,
                    new com.popo.library.image.ImageLoader.Builder().url(mMatchParcelable.iconUrlMininum).transform(new CropCircleTransformation(mContext))
                            .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(iv_match_person).build());

            ImageLoaderUtil.getInstance().loadImage(this,
                    new com.popo.library.image.ImageLoader.Builder().url(UserPreference.getSmallImage()).transform(new CropCircleTransformation(mContext))
                            .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(iv_match_mine).build());

        }
    }

    @Override
    protected void setListeners() {
        btn_match_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LaunchHelper.getInstance().launch(mContext, MatchChatActivity.class, mMatchParcelable);
            }
        });
        rl_back_match.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tv_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }
}
