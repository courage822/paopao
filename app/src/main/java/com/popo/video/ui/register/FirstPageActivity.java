package com.popo.video.ui.register;

import android.os.Parcelable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;

import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.event.RegisterAndLoginFinish;
import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.ui.login.LoginActivity;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Administrator on 2017/8/26.
 */

public class FirstPageActivity extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.firstpage_activity_btn_register)
    Button btn_register;
    @BindView(R.id.firstpage_activity_btn_logn)
    Button btn_logn;
    @BindView(R.id.firstpage_activity_iv_bg)
    ImageView iv_bg;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_firstpage;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        setAnimation();
    }

    @Override
    protected void setListeners() {
        btn_register.setOnClickListener(this);
        btn_logn.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.firstpage_activity_btn_register:
                LaunchHelper.getInstance().launch(mContext, LocationActivity.class);

                break;
            case R.id.firstpage_activity_btn_logn:
                LaunchHelper.getInstance().launch(mContext, LoginActivity.class);
                break;
        }
    }
    @Subscribe
    public void onEvent(RegisterAndLoginFinish event){
        finish();
    }
    private void setAnimation() {
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1, 1.2f, 1, 1.2f,
                Animation.RELATIVE_TO_SELF, 0.1f, Animation.RELATIVE_TO_SELF, 0.1f);
//        //3秒完成动画
        scaleAnimation.setDuration(7000);
//        //将AlphaAnimation这个已经设置好的动画添加到 AnimationSet中
        scaleAnimation.setRepeatMode(ScaleAnimation.REVERSE);
        //设置动画播放次数
        scaleAnimation.setRepeatCount(ScaleAnimation.INFINITE);
        animationSet.addAnimation(scaleAnimation);
        //从当前位置，向下和向右各平移300px
        TranslateAnimation animation = new TranslateAnimation(0.0f, -100f, 0.0f, 0.0f);
        animation.setDuration(7000);
        animation.setRepeatMode(TranslateAnimation.REVERSE);
        //设置动画播放次数
        animation.setRepeatCount(TranslateAnimation.INFINITE);
        animationSet.addAnimation(animation);
        //启动动画
        iv_bg.startAnimation(animationSet);
    }
}
