package com.popo.video.ui.pay.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.popo.video.R;
import com.popo.video.data.model.PayDict;
import com.popo.video.data.preference.UserPreference;
import com.popo.library.adapter.CommonRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.util.LaunchHelper;
import com.popo.video.parcelable.PayInfoParcelable;
import com.popo.video.ui.pay.activity.PayActivity;
import com.popo.video.ui.pay.presenter.PurchaseKeyPresenter;

import java.util.List;

/**
 * 支付项适配器
 * Created by zhangdroid on 2017/6/10.
 */
public class PayAdapter extends CommonRecyclerViewAdapter<PayDict> {
    private PurchaseKeyPresenter mRechargePresenter;
    private Context context;
    public void setPayPresenter(PurchaseKeyPresenter rechargePresenter) {
        this.mRechargePresenter = rechargePresenter;
    }

    public PayAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public PayAdapter(Context context, int layoutResId, List<PayDict> dataList) {
        super(context, layoutResId, dataList);
        this.context=context;
    }

    @Override
    public void convert(final PayDict payDict, final int position, RecyclerViewHolder holder) {
        if (null != payDict) {
            if (!"1".equals(payDict.getIsvalid())) {// 商品不可用
                holder.getConvertView().setVisibility(View.GONE);
                removeItem(position);
            } else {
                holder.setText(R.id.item_pay_name, payDict.getServiceName()+mContext.getString(R.string.number_keys));
                holder.setText(R.id.item_pay_onekey_howmoney,payDict.getServiceDesc());
                switch (UserPreference.getCountryId()) {
                    case "97"://美国
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("$", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "16"://澳大利亚
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("$", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "67"://加拿大
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("$", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "174"://香港
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("HK$", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "161"://印度
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("Rs", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "162"://印度尼西亚
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("Rp", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "10"://爱尔兰
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("€", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "152"://新西兰
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("NZ$", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "20"://巴基斯坦
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("Rs", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "46"://菲律宾
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("₱", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "150"://新加坡
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("S$", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "108"://南非
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("R", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "163"://英国
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("￡", String.valueOf(payDict.getPrice())).toString());
                        break;
                    case "173"://台湾
                        holder.setText(R.id.item_pay_purchase, TextUtils.concat("TWD", String.valueOf(payDict.getPrice())).toString());
                        break;
//            case "171"://中国
//                break;
                }
                holder.setOnClickListener(R.id.item_pay_purchase, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 购买
                        LaunchHelper.getInstance().launch(context, PayActivity.class, new PayInfoParcelable(payDict.getServiceId(),payDict.getServiceName(),3,payDict.getPrice(),position));

                    }
                });
            }
        }
    }

}
