package com.popo.video.ui.follow.presenter;

import android.content.Context;
import android.view.View;

import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.FollowUser;
import com.popo.video.data.model.UserBase;
import com.popo.library.adapter.MultiTypeRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.adapter.wrapper.OnLoadMoreListener;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.Utils;
import com.popo.video.parcelable.UserDetailParcelable;
import com.popo.video.ui.detail.UserDetailActivity;
import com.popo.video.ui.follow.FindUserActivity;
import com.popo.video.ui.follow.adapter.FollowAdapter;
import com.popo.video.ui.follow.contract.FollowContract;

import java.util.List;

/**
 * Created by Administrator on 2017/6/14.
 */

public class FollowPresenter implements FollowContract.IPresenter {
    private FollowContract.IView mFollowIview;
    private Context mContext;
    private FollowAdapter mFollowAdater;
    private int pageNum = 1;
    private String pageSize = "15";
    private List<UserBase> list;

    public FollowPresenter(FollowContract.IView mFollowIview) {
        this.mFollowIview = mFollowIview;
        this.mContext = mFollowIview.obtainContext();
    }


    @Override
    public void start() {

    }


    @Override
    public void loadFollowUserList() {
        mFollowIview.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                loadMore();
            }
        });
    }

    public void initData() {
        mFollowAdater = new FollowAdapter(mContext, R.layout.follow_list_item);
        mFollowAdater.setFragmentManager(mFollowIview.obtainFragmentManager());
        mFollowAdater.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                UserBase itemByPosition = mFollowAdater.getItemByPosition(position);
                if (null != itemByPosition) {
                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                            new UserDetailParcelable(String.valueOf(itemByPosition.getGuid())));
                }
            }
        });
        // 设置加载更多
        mFollowIview.setAdapter(mFollowAdater, R.layout.common_load_more);
    }

    @Override
    public void refresh() {
        if (mFollowAdater != null) {
            mFollowAdater.clear();
            mFollowAdater = null;
        }
        if (list != null && list.size() > 0) {
            list.clear();
        }
        pageNum = 1;
        load();
    }


    @Override
    public void goToSearchPage() {
        LaunchHelper.getInstance().launch(mContext, FindUserActivity.class);
    }

    @Override
    public void initFirstData() {
        load();
    }

    private void load() {
        ApiManager.getFollowList(String.valueOf(pageNum), pageSize, new IGetDataListener<FollowUser>() {
            @Override
            public void onResult(FollowUser followUser, boolean isEmpty) {
                if (isEmpty) {
                    if (pageNum == 1) {
                        mFollowIview.toggleShowEmpty(true, null);
                    } else if (pageNum > 1) {
                        mFollowIview.showNoMore();
                    }
                } else {
                    if (null != followUser) {
                        list = followUser.getUserBaseList();
                        if (!Utils.isListEmpty(list)) {
                            if (pageNum == 1) {
                                initData();
                                mFollowAdater.clear();
                                mFollowAdater.bind(list);
                                mFollowIview.toggleShowEmpty(false, null);
                            } else if (pageNum > 1) {
                                mFollowAdater.appendToList(list);
                            }
                            mFollowIview.hideLoadMore();
                        }
                    }
                }
                mFollowIview.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mFollowIview.hideRefresh(1);
                if (!isNetworkError) {
                    mFollowIview.toggleShowError(true, msg);
                }
            }
        });
    }

    /**
     * 上拉加载更多
     */
    private void loadMore() {
        mFollowIview.showLoadMore();
        mFollowIview.toggleShowError(false, null);
        pageNum++;
        load();
    }
}
