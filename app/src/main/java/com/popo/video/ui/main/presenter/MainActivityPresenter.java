package com.popo.video.ui.main.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.MyInfo;
import com.popo.video.data.model.SearchUser;
import com.popo.video.data.model.SearchUserList;
import com.popo.video.data.model.UserDetail;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.PayPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.ui.main.contract.MainActivityContract;

import java.util.List;

/**
 * Created by WangYong on 2017/12/12.
 */

public class MainActivityPresenter implements MainActivityContract.IPresenter {
    private Context context;
    private MainActivityContract.IView mMainActiviyIview;

    public MainActivityPresenter(MainActivityContract.IView mMainActiviyIview) {
        this.mMainActiviyIview = mMainActiviyIview;
        context = mMainActiviyIview.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void loadData() {
        ApiManager.sayMoneyHello(new IGetDataListener<SearchUserList>() {
            @Override
            public void onResult(SearchUserList searchUserList, boolean isEmpty) {
                if (searchUserList != null) {
                    List<SearchUser> searchUserList1 = searchUserList.getSearchUserList();
                    mMainActiviyIview.sayHellAll(searchUserList1);
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
        ApiManager.getDiamondPayWay("", new IGetDataListener<String>() {
            @Override
            public void onResult(String s, boolean isEmpty) {
                PayPreference.saveDiamondInfo(s);
            }

            @Override
            public void onError(String msg, boolean isNtworkError) {
            }
        });
        ApiManager.getVipPayWay("", new IGetDataListener<String>() {
            @Override
            public void onResult(String s, boolean isEmpty) {
                PayPreference.saveVipInfo(s);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
        ApiManager.getGiftDic(new IGetDataListener<String>() {
            @Override
            public void onResult(String str, boolean isEmpty) {
                PayPreference.saveSendGifts(str);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    if (!TextUtils.isEmpty(myInfo.getIslj())){
                        UserPreference.setIslj(myInfo.getIslj());
                    }
                    UserDetail userDetail = myInfo.getUserDetail();
                    String vipDays = userDetail.getVipDays();
                    if (!TextUtils.isEmpty(vipDays) && vipDays.length() > 0) {
                        int i = Integer.parseInt(vipDays);
                        if (i > 0) {
                            mMainActiviyIview.isVipUser();
                        }
                        UserPreference.setIsVip(i);
                    } else {
                        UserPreference.setIsVip(false);
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
        ApiManager.getPayWayString("1", "2", new IGetDataListener<String>() {
            @Override
            public void onResult(String s, boolean isEmpty) {
                DataPreference.savePayWayJson(s);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }
}
