package com.popo.video.ui;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import com.popo.library.permission.PermissionCallback;
import com.popo.library.permission.PermissionManager;
import com.popo.library.permission.PermissonItem;
import com.popo.library.util.DeviceUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.ToastUtil;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.common.AgoraHelper;
import com.popo.video.common.Util;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.Countries;
import com.popo.video.data.model.Country;
import com.popo.video.data.model.HostInfo;
import com.popo.video.data.model.Login;
import com.popo.video.data.model.PlatformInfo;
import com.popo.video.data.model.UserBase;
import com.popo.video.data.model.UserBean;
import com.popo.video.data.model.UserDetail;
import com.popo.video.data.preference.AnchorPreference;
import com.popo.video.data.preference.BeanPreference;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.event.FinishEvent;
import com.popo.video.ui.main.MainActivity;
import com.popo.video.ui.register.CompleteInfoActivity;
import com.popo.video.ui.register.FirstPageActivity;

import org.greenrobot.eventbus.EventBus;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * 启动页面
 * Created by zhangdroid on 2017/5/12.
 */
public class SplashActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        // 初始化平台信息
        initPlatformInfo();
        // 激活
        activation();

        checkPermission(this);

        //测量屏幕宽高像素
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widthPixels = dm.widthPixels;
        int heightPixels = dm.heightPixels;
        float density = dm.density;
        int screenWidth = (int) (widthPixels * density);
        int screenHeight = (int) (heightPixels * density);
        DataPreference.saveScreenWidth(screenWidth);
        DataPreference.saveScreenHeight(screenHeight);
    }

    private void login() {
        String account = UserPreference.getAccount();
        String password = UserPreference.getPassword();
        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(password)) {
            ApiManager.login(account, password, new IGetDataListener<Login>() {
                @Override
                public void onResult(Login login, boolean isEmpty) {
                    if (!TextUtils.isEmpty(login.getIslj())){
                        UserPreference.setIslj(login.getIslj());
                    }
                    UserDetail userDetail = login.getUserDetail();
                    if (null != userDetail) {
                        // 更新用户相关信息
                        UserBase userBase = userDetail.getUserBase();
                        if (null != userBase) {
                            UserPreference.saveUserInfo(userBase);
                            PlatformInfo platformInfo = PlatformPreference.getPlatformInfo();
                            platformInfo.setFid(userBase.getFromChannel() + "");
                            platformInfo.setCountry(DataPreference.getCountry(userBase.getCountry()));
                            UserPreference.setCountryId(userBase.getCountry());
                            PlatformPreference.setPlatfromInfo(platformInfo);
                        }
                        if (!TextUtils.isEmpty(userDetail.getVipDays())) {
                            int i = Integer.parseInt(userDetail.getVipDays());
                            UserPreference.setIsVip(i);
                        } else {
                            UserPreference.setIsVip(false);
                        }
                        UserBean userBean = userDetail.getUserBean();
                        if (null != userBean) {
                            BeanPreference.saveUserBean(userBean);
                        }
                        HostInfo hostInfo = userDetail.getHostInfo();
                        if (null != hostInfo) {
                            AnchorPreference.saveHostInfo(hostInfo);
                        }
                        if (!TextUtils.isEmpty(userBase.getIconUrl())) {
                            launchMainActivity();
                            AgoraHelper.getInstance().login();
                        } else {
                            EventBus.getDefault().post(new FinishEvent());
                            LaunchHelper.getInstance().launchFinish(SplashActivity.this, CompleteInfoActivity.class);
                        }
                    }
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                    LaunchHelper.getInstance().launchFinish(SplashActivity.this, FirstPageActivity.class);//这个是跳转到登录界面
                }

            });
        } else {
            LaunchHelper.getInstance().launchFinish(SplashActivity.this, FirstPageActivity.class);//这个是跳转到注册界面
        }
    }

    private void launchMainActivity() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ToastUtil.showShortToast(SplashActivity.this, getString(R.string.login_success));
                LaunchHelper.getInstance().launchFinish(SplashActivity.this, MainActivity.class);
            }
        });
    }

    private void activation() {
        if (!UserPreference.isSplashMactived()){
            ApiManager.userActivityTag("", "", "17",  "", new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                    UserPreference.mSplashActivation();
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {

                }
            });
        }

    }

    private void initPlatformInfo() {
        if (PlatformPreference.getPlatformInfo() == null
                || (PlatformPreference.getPlatformInfo() != null && TextUtils.isEmpty(PlatformPreference.getPlatformInfo().getPhonetype()))) {
            PlatformInfo platformInfo = new PlatformInfo();
            platformInfo.setW(String.valueOf(DeviceUtil.getScreenWidth(this)));
            platformInfo.setH(String.valueOf(DeviceUtil.getScreenHeight(this)));
            platformInfo.setVersion(DeviceUtil.getVersionName(this));
            platformInfo.setPhonetype(Build.MODEL);
            platformInfo.setSystemVersion(Build.VERSION.RELEASE);
            platformInfo.setPlatform("2");
            platformInfo.setProduct(C.PRODUCT_ID);
            platformInfo.setLanguage(Util.getLacalLanguage());
            platformInfo.setPid(getAndroidId());
            platformInfo.setImsi(getAndroidId());
            platformInfo.setNetType(getNetType());
            platformInfo.setCountry("United State");
            platformInfo.setMobileIP(getMobileIP());
            platformInfo.setFid("30199");
            platformInfo.setRelease(String.valueOf(DeviceUtil.getVersionCode(this)));

            // 保存platform信息
            PlatformPreference.setPlatfromInfo(platformInfo);
        } else {
            PlatformInfo platformInfo = PlatformPreference.getPlatformInfo();
            platformInfo.setLanguage(Util.getLacalLanguage());
            PlatformPreference.setPlatfromInfo(platformInfo);
        }

        ApiManager.getCountries(new IGetDataListener<Countries>() {
            @Override
            public void onResult(Countries countries, boolean isEmpty) {
                if (countries != null && countries.getCountrys() != null && countries.getCountrys().size() != 0) {
                    List<Country> countrys = countries.getCountrys();
                    DataPreference.saveCountries(countrys);
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });


    }

    private String getAndroidId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    private String getNetType() {
        String type = "0";
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) {
            type = "0";
        } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {
            type = "2";
        } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            String extraInfo = info.getExtraInfo();
            if (extraInfo.equals("cmwap")) {
                type = "3";
            } else if (extraInfo.equals("cmnet")) {
                type = "4";
            } else if (extraInfo.equals("ctnet")) {
                type = "5";
            } else if (extraInfo.equals("ctwap")) {
                type = "6";
            } else if (extraInfo.equals("3gwap")) {
                type = "7";
            } else if (extraInfo.equals("3gnet")) {
                type = "8";
            } else if (extraInfo.equals("uniwap")) {
                type = "9";
            } else if (extraInfo.equals("uninet")) {
                type = "10";
            }
        }
        return type;
    }

    private String getMobileIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static List<String> findDeniedPermissions(Activity activity, String... permission) {
        //存储没有授权的权限
        List<String> denyPermissions = new ArrayList<>();
        for (String value : permission) {
            if (ContextCompat.checkSelfPermission(activity, value) != PackageManager.PERMISSION_GRANTED) {
                //没有权限 就添加
                denyPermissions.add(value);
            }
        }
        return denyPermissions;
    }

    public void checkPermission(final Activity activity) {
        String[] permissions = {C.permission.PERMISSION_PHONE, C.permission.PERMISSION_WRITE_EXTERNAL_STORAGE,
                C.permission.PERMISSION_READ_EXTERNAL_STORAGE, C.permission.PERMISSION_CAMERA, C.permission.PERMISSION_RECORD_AUDIO,
                C.permission.PERMISSION_LOCATION_COARSE};
        List<String> deniedPermissions = findDeniedPermissions(activity, permissions);
        if (deniedPermissions != null && deniedPermissions.size() > 0) {
            //大于0,表示有权限没申请

            // 请求所有高危权限
            PermissionManager.getInstance(this)
                    .addPermission(new PermissonItem(C.permission.PERMISSION_PHONE, getString(R.string.permission_phone), R.drawable.phone_permission))
                    .addPermission(new PermissonItem(C.permission.PERMISSION_WRITE_EXTERNAL_STORAGE, getString(R.string.permission_write_file), R.drawable.write_permission))
                    .addPermission(new PermissonItem(C.permission.PERMISSION_READ_EXTERNAL_STORAGE, getString(R.string.permission_read_file), R.drawable.read_permission))
                    .addPermission(new PermissonItem(C.permission.PERMISSION_CAMERA, getString(R.string.permission_camera), R.drawable.camera_permission))
                    .addPermission(new PermissonItem(C.permission.PERMISSION_RECORD_AUDIO, getString(R.string.permission_record), R.drawable.voice_permission))
                    .addPermission(new PermissonItem(C.permission.PERMISSION_LOCATION_COARSE, getString(R.string.permission_location), R.drawable.location_permission))
                    .checkMutiPermission(new PermissionCallback() {
                        @Override
                        public void onGuaranteed(String permisson, int position) {
                        }

                        @Override
                        public void onDenied(String permisson, int position) {
                        }

                        @Override
                        public void onFinished() {
                            login();
                        }

                        @Override
                        public void onClosed() {
//                        login();
                            finish();
                        }
                    });
        } else {
            //拥有权限
            login();
        }
    }
}
