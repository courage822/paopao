package com.popo.video.ui.auto.presenter;

import android.content.Context;

import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.HostAutoReplyBean;
import com.popo.video.data.model.THostAutoReply;
import com.popo.video.event.AutoReplyEvent;
import com.popo.library.util.Utils;
import com.popo.video.ui.auto.adapter.ReplyAdapter;
import com.popo.video.ui.auto.contract.ReplyContract;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Administrator on 2017/6/14.
 */

public class ReplyPresenter implements ReplyContract.IPresenter {
    private ReplyContract.IView mReplyIview;
    private Context mContext;
    private ReplyAdapter mReplyAdapter;
    private List<THostAutoReply> list;


    public ReplyPresenter(ReplyContract.IView mReplyIview) {
        this.mReplyIview = mReplyIview;
        this.mContext = mReplyIview.obtainContext();
    }


    @Override
    public void start() {

    }


    public void initData() {
        if (mReplyAdapter == null) {
            mReplyAdapter = new ReplyAdapter(mContext, R.layout.reply_list_item);
            mReplyIview.setAdapter(mReplyAdapter);
        }

        mReplyAdapter.setOnDelListener(new ReplyAdapter.onSwipeListener() {
            @Override
            public void onDel(int pos) {
                if (list != null && list.size() > 0) {
                    list.remove(pos);
                    EventBus.getDefault().post(new AutoReplyEvent());
                    mReplyAdapter.notifyItemRemoved(pos);//推荐用这个
                    if (list.size() == 0) {
                        mReplyAdapter.removeAll();
                        mReplyIview.toggleShowEmpty(true, null);
                    } else {
                        mReplyAdapter.replaceAll(list);
                        mReplyIview.deleteItem(pos);
                    }
                } else {
                    mReplyIview.toggleShowEmpty(true, null);
                }
            }
        });

    }

    @Override
    public void loadReplyList() {
        mReplyIview.toggleShowError(false, null);
        load();
    }

    @Override
    public void refresh() {
        if (mReplyAdapter != null) {
            mReplyAdapter.removeAll();
            mReplyAdapter = null;
        }
        if (list != null && list.size() > 0) {
            list.clear();
        }
        load();
    }

    private void load() {
        ApiManager.getHostAutoReply(new IGetDataListener<HostAutoReplyBean>() {
            @Override
            public void onResult(HostAutoReplyBean bean, boolean isEmpty) {
                mReplyIview.hideRefresh(1);
                if (isEmpty || bean.getType1Reply().size() == 0) {
                    mReplyIview.toggleShowEmpty(true, null);
                } else {
                    if (null != bean) {
                        mReplyIview.hindEmpty(true);
                        list = bean.getType1Reply();
                        if (!Utils.isListEmpty(list)) {
                            initData();
                            mReplyAdapter.replaceAll(list);
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mReplyIview.hideRefresh(1);
                if (!isNetworkError) {
                    mReplyIview.toggleShowError(true, msg);
                }
            }
        });
    }
}
