package com.popo.video.ui.detail.contract;

import android.support.v4.app.FragmentManager;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

/**
 * Created by Administrator on 2017/6/14.
 */
public interface ReportContract {

    interface IView extends BaseView {
        void showLoading();

        void dismissLoading();

        void showNetworkError();

        FragmentManager obtainFragmentManager();

    }

    interface IPresenter extends BasePresenter {
        void startReport(String userId, String resonCode);
    }

}
