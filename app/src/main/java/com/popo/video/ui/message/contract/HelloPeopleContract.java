package com.popo.video.ui.message.contract;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;
import com.popo.video.ui.message.adapter.ChatHistoryAdapter;

/**
 * Created by xuzhaole on 2017/12/2.
 */

public interface HelloPeopleContract {
    interface IView extends BaseView {

        void setAdapter(ChatHistoryAdapter pagerAdapter, int layoutid);

        void toggleShowEmpty(boolean toggle, String msg);

        void toggleShowError(boolean toggle, String msg);

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        /**
         * 显示加载更多
         */
        void showLoadMore();

        /**
         * 隐藏加载更多
         */
        void hideLoadMore();

        /**
         * 显示没有更多（滑动到最底部）
         */
        void showNoMore();

        ChatHistoryAdapter getChatHistoryAdapter();
    }

    interface IPresenter extends BasePresenter {
        void loadData();

        void refresh();
    }
}
