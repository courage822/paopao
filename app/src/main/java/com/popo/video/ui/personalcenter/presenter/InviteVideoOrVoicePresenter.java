package com.popo.video.ui.personalcenter.presenter;

import android.content.Context;

import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.HostInfo;
import com.popo.video.data.model.SendVideoOrVoice;
import com.popo.video.data.model.SendVideoOrVoiceHostInfor;
import com.popo.video.data.preference.AnchorPreference;
import com.popo.video.ui.personalcenter.contract.InviteVideoOrVoiceContract;

/**
 * Created by WangYong on 2017/11/25.
 */

public class InviteVideoOrVoicePresenter implements InviteVideoOrVoiceContract.IPresenter{
    private Context context;
    private InviteVideoOrVoiceContract.IView mInviteVideoOrVoice;

    public InviteVideoOrVoicePresenter(InviteVideoOrVoiceContract.IView mInviteVideoOrVoice) {
        this.mInviteVideoOrVoice = mInviteVideoOrVoice;
        context=mInviteVideoOrVoice.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void startInvite(String type) {
        ApiManager.sendVideoOrVoice(type, new IGetDataListener<SendVideoOrVoice>() {
            @Override
            public void onResult(SendVideoOrVoice sendVideoOrVoice, boolean isEmpty) {
                if(sendVideoOrVoice!=null){
                    mInviteVideoOrVoice.getSendNum(sendVideoOrVoice.getCount());
                }
            }
            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    @Override
    public void getCountDown(final String type) {
        mInviteVideoOrVoice.getAnchorPrice(AnchorPreference.getPrice());
        ApiManager.getVideoOrVoiceTime(type, new IGetDataListener<SendVideoOrVoiceHostInfor>() {
            @Override
            public void onResult(SendVideoOrVoiceHostInfor sendVideoOrVoice, boolean isEmpty) {
              if(sendVideoOrVoice!=null){
                  mInviteVideoOrVoice.getButtonCountDown(sendVideoOrVoice.getCountDown());
                  HostInfo hostInfo = sendVideoOrVoice.getHostInfo();
                  if(hostInfo!=null){
                      if (type.equals("2")) {
                          mInviteVideoOrVoice.getSendNum(hostInfo.getTotalSendCallAudio());
                      } else{
                          mInviteVideoOrVoice.getSendNum(hostInfo.getTotalSendCallVideo());
                      }
                  }
              }


            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

}
