package com.popo.video.ui.personalcenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.library.dialog.LoadingDialog;
import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.SnackBarUtil;
import com.popo.library.util.ToastUtil;
import com.popo.library.util.Utils;
import com.popo.library.widget.PlayView;
import com.popo.video.R;
import com.popo.video.base.BaseAppCompatActivity;
import com.popo.video.common.BitmapUtils;
import com.popo.video.common.RecordUtil;
import com.popo.video.common.ShowVoiceDialog;
import com.popo.video.common.TimeUtils;
import com.popo.video.common.YeMeiPopopUtil;
import com.popo.video.data.model.NettyMessage;
import com.popo.video.event.SingleLoginFinishEvent;
import com.popo.video.event.YeMeiMessageEvent;
import com.popo.video.parcelable.PlayViewParcelable;
import com.popo.video.parcelable.VideoShowParcelable;
import com.popo.video.ui.personalcenter.contract.VideoShowContract;
import com.popo.video.ui.personalcenter.presenter.VideoShowPresenter;
import com.popo.video.ui.photo.GetVideoActivity;

import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 认证主播页面
 * Created by zhangdroid on 2017/5/27.
 */
public class VideoShowActivity extends BaseAppCompatActivity implements View.OnClickListener, VideoShowContract.IView {
    @BindView(R.id.ll_root)
    LinearLayout mLlRoot;
    @BindView(R.id.rl_sound)
    RelativeLayout rl_sound;
    @BindView(R.id.tv_commit)
    TextView tv_submit;//提交按钮
    @BindView(R.id.playview)
    PlayView playView;//播放的view
    @BindView(R.id.iv_record)
    ImageView iv_record;//开始录制视频按钮
    @BindView(R.id.iv_re_record)
    ImageView iv_re_record;//重新录制按钮
    @BindView(R.id.rl_playview)
    RelativeLayout rl_playView;
    @BindView(R.id.rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.iv_reply_sound)
    ImageView iv_reply_sound;
    @BindView(R.id.rl_voice)
    RelativeLayout rl_voice;
    @BindView(R.id.tv_reply_sound)
    TextView tv_reply_sound;
    @BindView(R.id.et_ownwords)
    EditText et_ownwords;
    @BindView(R.id.tv_number)
    TextView tv_number;
    private String voiceFilePath;
    private long mVoiceDuration;
    private String mBitmapFilePath;
    private String videoShowDuration;
    String mVideoShowFilePath = null;
    private VideoShowPresenter mVideoShowPresenter;
    private static AnimationDrawable soundanimationDrawable;
    private VideoShowParcelable mParcelable;
    private boolean isShowFail = false;
    private long lastClickTime = 0;


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_video_show;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }


    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mParcelable = (VideoShowParcelable) parcelable;
        if (TextUtils.isEmpty(mParcelable.getDuration())
                || TextUtils.isEmpty(mParcelable.getVideoFilePaht())
                || TextUtils.isEmpty(mParcelable.getAvatarFilePath())) {
            isShowFail = true;
        }
    }

    @Override
    protected View getNoticeView() {
        return mLlRoot;
    }

    @Override
    protected void initViews() {
        mVideoShowPresenter = new VideoShowPresenter(this);
        if (TimeUtils.timeIsPast()) {//判断页脚是否过期
            msgHandler.sendEmptyMessage(1);
        } else if (!TimeUtils.timeIsPast()) {
            msgHandler.sendEmptyMessage(2);
        }
    }

    @Override
    protected void setListeners() {
        tv_submit.setOnClickListener(this);
        iv_record.setOnClickListener(this);
        iv_re_record.setOnClickListener(this);
        playView.setOnClickListener(this);
        rl_playView.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_voice.setOnClickListener(this);
        rl_sound.setOnClickListener(this);
        et_ownwords.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tv_number.setText(charSequence.length() + "/20");
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        playView.seekTo(1);
    }

    @Override
    public void isShowFail() {
        isShowFail = true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_commit:
                if (TextUtils.isEmpty(et_ownwords.getText().toString().trim()) || TextUtils.isEmpty(mVideoShowFilePath) || TextUtils.isEmpty(mBitmapFilePath) || TextUtils.isEmpty(voiceFilePath)
                        || TextUtils.isEmpty(videoShowDuration) || mVoiceDuration < 1000) {
                    ToastUtil.showShortToast(mContext, mContext.getString(R.string.improve_to_upload));
                    return;
                }

                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();

                    if (isShowFail) {
                        mVideoShowPresenter.submitShow(et_ownwords.getText().toString().trim(), mVideoShowFilePath,
                                mBitmapFilePath, videoShowDuration, voiceFilePath, mVoiceDuration + "");
                    } else {
                        mVideoShowPresenter.sendParams(
                                mParcelable.getDuration(), mParcelable.getVideoFilePaht(), mParcelable.getAvatarFilePath(),
                                videoShowDuration, mVideoShowFilePath, mBitmapFilePath
                                , mVoiceDuration + "", voiceFilePath, et_ownwords.getText().toString().trim()
                        );
                    }
                }


                break;
            case R.id.iv_record://选择视频秀
            case R.id.iv_re_record://重新选择视频秀
                GetVideoActivity.toGetVideoActivity(mContext, new GetVideoActivity.OnGetVideoListener() {
                    @Override
                    public void getSelectedVideo(String filePath, String showDuration, String bitmapFilePath) {
                        if (!TextUtils.isEmpty(filePath)) {
                            if (TextUtils.isEmpty(bitmapFilePath)) {
                                final Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Video.Thumbnails.MINI_KIND);
                                new AsyncTask<Void, Void, File>() {

                                    @Override
                                    protected File doInBackground(Void... voids) {
                                        return BitmapUtils.compressImage(mContext, bitmap);
                                    }

                                    @Override
                                    protected void onPostExecute(File file) {
                                        if (file != null)
                                            mBitmapFilePath = file.getAbsolutePath();
                                    }
                                }.execute();
                            } else {
                                mBitmapFilePath = bitmapFilePath;
                            }
                            mVideoShowFilePath = filePath;
                            videoShowDuration = showDuration;
                            iv_record.setVisibility(View.GONE);
                            iv_re_record.setVisibility(View.VISIBLE);
                            playView.setVisibility(View.VISIBLE);
                            rl_playView.setVisibility(View.VISIBLE);
                            playView.setVideoURI(Uri.parse(mVideoShowFilePath));
                            playView.seekTo(1);
                        }
                    }
                });
                break;
            case R.id.rl_playview:
                //开始播放视频秀
                LaunchHelper.getInstance().launch(mContext, PlayActivity.class, new PlayViewParcelable(mVideoShowFilePath));
                break;

            case R.id.rl_sound:
                rl_sound.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //其他条目,释放资源
                        if (soundanimationDrawable != null) {
                            soundanimationDrawable.selectDrawable(2);//恢复到第一帧
                            soundanimationDrawable.stop();
                        }
                        RecordUtil.getInstance().release();

                        //点击条目开始播放动画和录音
                        startVoiceAnimation(iv_reply_sound, mContext);
                        // 播放动画
                        if (soundanimationDrawable != null) {
                            soundanimationDrawable.start();
                        }
                        if (!TextUtils.isEmpty(voiceFilePath)) {
                            RecordUtil.getInstance().play(voiceFilePath, new RecordUtil.OnPlayerListener() {
                                @Override
                                public void onCompleted() {//播放完成，停止动画
                                    soundanimationDrawable.stop();
                                    soundanimationDrawable.selectDrawable(2);

                                }

                                @Override
                                public void onPaused() {
                                    soundanimationDrawable.stop();
                                    soundanimationDrawable.selectDrawable(2);
                                }
                            });
                        }
                    }
                });
                break;
            case R.id.rl_back:
                finish();
                break;
            case R.id.rl_voice:
                RecordUtil.getInstance().release();
                if (soundanimationDrawable != null) {
                    soundanimationDrawable.selectDrawable(2);//恢复到第一帧
                    soundanimationDrawable.stop();
                }
                ShowVoiceDialog showVoiceDialog = new ShowVoiceDialog();
                showVoiceDialog.setOnVoiceRecordListener(new ShowVoiceDialog.OnVoiceRecordListener() {
                    @Override
                    public void onRecord(String filePath, long voiceDuration) {
                        rl_sound.setVisibility(View.VISIBLE);
                        mVoiceDuration = voiceDuration;
                        voiceFilePath = filePath;
                        tv_reply_sound.setText(voiceDuration / 1000 + "\"");
                    }
                });
                showVoiceDialog.recordAndMsgShow(mContext);
                break;
        }
    }

    /**
     * 播放录音动画
     *
     * @param iv_sound_voice
     * @param context
     */
    private void startVoiceAnimation(ImageView iv_sound_voice, Context context) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_pink_left1));
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_pink_left2));
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_pink_left3));
        soundanimationDrawable = Utils.getFrameAnim(drawableList, true, 150);
        iv_sound_voice.setImageDrawable(soundanimationDrawable);
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        playView.stopPlayback();
    }

    private void initPlayView() {
        if (!TextUtils.isEmpty(mVideoShowFilePath)) {
            playView.setVideoURI(Uri.parse(mVideoShowFilePath));
            playView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    playView.seekTo(1);
                }
            });
            playView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    //获取视频资源的宽度
                    int videoWidth = mp.getVideoWidth();
                    //获取视频资源的高度
                    int videoHeight = mp.getVideoHeight();
                    playView.setSizeH(videoHeight);
                    playView.setSizeW(videoWidth);
                    playView.requestLayout();
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        playView.pause();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        initPlayView();
    }

    @Override
    public void showLoading() {
        LoadingDialog.showNoCanceled(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }

    @Subscribe
    public void onEvent(NettyMessage event) {
        msgHandler.sendEmptyMessage(1);
    }

    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    closeShowPop();
                    break;
            }
        }
    };

    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }

    @Override
    protected void closeShowPop() {
        super.closeShowPop();

    }
    @Subscribe
    public void onEvent(YeMeiMessageEvent evet){
        Message msg=new Message();
        msg.obj=evet;
        yeMeiHandler.sendMessage(msg);

    }
    private Handler yeMeiHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            YeMeiMessageEvent obj = (YeMeiMessageEvent) msg.obj;
            if(obj!=null){
                YeMeiPopopUtil.getInstance().showYeMei(VideoShowActivity.this,mLlRoot,obj,isFinishing());
            }
        }
    };
}
