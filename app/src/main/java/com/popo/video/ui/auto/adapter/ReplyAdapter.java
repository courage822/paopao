package com.popo.video.ui.auto.adapter;


import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.common.RecordUtil;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.THostAutoReply;
import com.popo.library.adapter.recyclerview.RefreshComRecyclerViewAdapter;
import com.popo.library.adapter.recyclerview.RefreshRecyclerViewHolder;
import com.popo.library.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/6/14.
 */

public class ReplyAdapter extends RefreshComRecyclerViewAdapter<THostAutoReply> {
    private FragmentManager mFragmentManager;
    private static AnimationDrawable soundanimationDrawable;
    private int mPosition;

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
    }

    public ReplyAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }


    @Override
    public void convert(final int position, final RefreshRecyclerViewHolder holder, final THostAutoReply typereply) {
        final ImageView iv_reply_sound = (ImageView) holder.getView(R.id.iv_reply_sound);
        TextView tv_reply_text = (TextView) holder.getView(R.id.tv_reply_text);
        TextView tv_reply_sound = (TextView) holder.getView(R.id.tv_reply_sound);
        TextView item_del_text = (TextView) holder.getView(R.id.item_del_text);
        final ImageView iv_state_selected = (ImageView) holder.getView(R.id.iv_state_selected);
        ImageView iv_under_review = (ImageView) holder.getView(R.id.iv_under_review);
        RelativeLayout rl_sound = (RelativeLayout) holder.getView(R.id.rl_sound);
        RelativeLayout rl_content = (RelativeLayout) holder.getView(R.id.rl_content);

        if (typereply != null) {
            if (typereply.getContentType().equals("1")) {//1、文本类型
                rl_sound.setVisibility(View.GONE);
                tv_reply_text.setVisibility(View.VISIBLE);
                tv_reply_text.setText(typereply.getContent());
            } else {//2、语音类型
                rl_sound.setVisibility(View.VISIBLE);
                tv_reply_text.setVisibility(View.GONE);
                try {
                    holder.setText(R.id.tv_reply_sound, typereply.getAudioSecond()+ "''");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (typereply.getAuditStatus().equals("1")) {//审核成功
                iv_state_selected.setVisibility(View.VISIBLE);
                iv_under_review.setVisibility(View.GONE);
                if (typereply.getUseStatus().equals("1")) {//选中状态
                    iv_state_selected.setImageResource(R.drawable.reply_selected);
                } else if (typereply.getUseStatus().equals("0")) {//未选中状态
                    iv_state_selected.setImageResource(R.drawable.reply_unselected);
                }
            } else if (typereply.getAuditStatus().equals("0")) {//审核中
                iv_state_selected.setVisibility(View.GONE);
                iv_under_review.setVisibility(View.VISIBLE);
            } else if (typereply.getAuditStatus().equals("-1")) {//审核失败（没有这个状态，按审核中处理）
                iv_state_selected.setVisibility(View.GONE);
                iv_under_review.setVisibility(View.VISIBLE);
            }

            rl_sound.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //其他条目,释放资源
                    if (soundanimationDrawable != null) {
                        soundanimationDrawable.selectDrawable(0);//恢复到第一帧
                        soundanimationDrawable.stop();
                    }
                    RecordUtil.getInstance().release();

                    //点击条目开始播放动画和录音
                    startVoiceAnimation(iv_reply_sound, mContext);
                    // 播放动画
                    if (soundanimationDrawable != null) {
                        soundanimationDrawable.start();
                    }
                    RecordUtil.getInstance().play(typereply.getContent(), new RecordUtil.OnPlayerListener() {
                        @Override
                        public void onCompleted() {//播放完成，停止动画
                            soundanimationDrawable.stop();
                        }

                        @Override
                        public void onPaused() {
                            soundanimationDrawable.stop();
                        }
                    });
                }
            });

            if (typereply.getUseStatus().equals("1")) {
                iv_state_selected.setImageResource(R.drawable.reply_selected);
            } else if (typereply.getUseStatus().equals("0")) {
                iv_state_selected.setImageResource(R.drawable.reply_unselected);
            }
            rl_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!typereply.getUseStatus().equals("1")) {
                        ApiManager.updateAutoReplyStatus(typereply.getGuid(), "1", new IGetDataListener<BaseModel>() {
                            @Override
                            public void onResult(BaseModel baseModel, boolean isEmpty) {
                                for (int i = 0; i < mDataList.size(); i++) {
                                    if (position == i) {
                                        mDataList.get(i).setUseStatus("1");
                                    } else {
                                        mDataList.get(i).setUseStatus("0");
                                    }
                                }
                                notifyDataSetChanged();
                            }

                            @Override
                            public void onError(String msg, boolean isNetworkError) {
                            }
                        });
                    }

                }
            });
            item_del_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ApiManager.updateAutoReplyStatus(typereply.getGuid(), "-1", new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {
                            if (baseModel.getIsSucceed().equals("1")) {
                                //删除成功
                                if (mOnSwipeListener != null)
                                    mOnSwipeListener.onDel(position);
                            }
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                        }
                    });
                }
            });
        }
    }

    /**
     * 播放录音动画
     *
     * @param iv_sound_voice
     * @param context
     */
    private void startVoiceAnimation(ImageView iv_sound_voice, Context context) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_pink_left1));
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_pink_left2));
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_pink_left3));
        soundanimationDrawable = Utils.getFrameAnim(drawableList, true, 150);
        iv_sound_voice.setImageDrawable(soundanimationDrawable);
    }

    /**
     * 和Activity通信的接口
     */
    public interface onSwipeListener {
        void onDel(int pos);

    }

    private onSwipeListener mOnSwipeListener;

    public onSwipeListener getOnDelListener() {
        return mOnSwipeListener;
    }

    public void setOnDelListener(onSwipeListener mOnDelListener) {
        this.mOnSwipeListener = mOnDelListener;
    }

}
