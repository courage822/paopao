package com.popo.video.ui.auto.presenter;

import android.content.Context;

import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.HostAutoReplyBean;
import com.popo.video.data.model.THostAutoReply;
import com.popo.video.event.AutoReplyEvent;
import com.popo.library.util.Utils;
import com.popo.video.ui.auto.adapter.HelloAdapter;
import com.popo.video.ui.auto.adapter.ReplyAdapter;
import com.popo.video.ui.auto.contract.HelloContract;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Administrator on 2017/6/14.
 */

public class HelloPresenter implements HelloContract.IPresenter {
    private HelloContract.IView mHelloIview;
    private Context mContext;
    private HelloAdapter mHelloAdapter;
    private List<THostAutoReply> list;


    public HelloPresenter(HelloContract.IView mHelloIview) {
        this.mHelloIview = mHelloIview;
        this.mContext = mHelloIview.obtainContext();
    }


    @Override
    public void start() {

    }


    public void initData() {
        if (mHelloAdapter == null) {
            mHelloAdapter = new HelloAdapter(mContext, R.layout.reply_list_item);
            mHelloIview.setAdapter(mHelloAdapter);
        }
        mHelloAdapter.setOnDelListener(new ReplyAdapter.onSwipeListener() {
            @Override
            public void onDel(int pos) {
                if (list != null && list.size() > 0) {
                    list.remove(pos);
                    EventBus.getDefault().post(new AutoReplyEvent());
                    mHelloAdapter.notifyItemRemoved(pos);//推荐用这个
                    if (list.size() == 0) {
                        mHelloAdapter.removeAll();
                        mHelloIview.toggleShowEmpty(true, null);
                    } else {
                        mHelloAdapter.replaceAll(list);
                        mHelloIview.deleteItem(pos);
                    }
                } else {
                    mHelloIview.toggleShowEmpty(true, null);
                }
            }
        });
    }

    @Override
    public void loadHelloList() {
        mHelloIview.toggleShowError(false, null);
        load();
    }

    @Override
    public void refresh() {
        if (mHelloAdapter != null) {
            mHelloAdapter.removeAll();
            mHelloAdapter = null;
        }
        if (list != null && list.size() > 0) {
            list.clear();
        }
        load();
    }

    @Override
    public void deleteItem(final int position) {//删除条目
        THostAutoReply itemByPosition = mHelloAdapter.getItemByPosition(position);
        ApiManager.updateAutoReplyStatus(itemByPosition.getGuid(), "-1", new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                if (baseModel.getIsSucceed().equals("1")) {
                    //删除成功
                    mHelloAdapter.removeItem(position);
                    mHelloIview.deleteItem(position);
                    if (mHelloAdapter.getItemCount() ==0){//没有条目时
                        mHelloIview.toggleShowEmpty(true, null);
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    private void load() {
        ApiManager.getHostAutoReply(new IGetDataListener<HostAutoReplyBean>() {
            @Override
            public void onResult(HostAutoReplyBean bean, boolean isEmpty) {
                mHelloIview.hideRefresh(1);
                if (isEmpty || bean.getType2Reply().size() == 0) {
                    mHelloIview.toggleShowEmpty(true, null);
                } else {
                    if (null != bean) {
                        mHelloIview.hindEmpty(true);
                        list = bean.getType2Reply();
                        if (!Utils.isListEmpty(list)) {
                            initData();
                            mHelloAdapter.replaceAll(list);
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mHelloIview.hideRefresh(1);
                if (!isNetworkError) {
                    mHelloIview.toggleShowError(true, msg);
                }
            }
        });
    }
}
