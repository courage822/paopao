package com.popo.video.ui.personalcenter.contract;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

/**
 * Created by Administrator on 2017/7/5.
 */

public interface SetPriceConteact {

    interface IView extends BaseView {
        void showLoading();

        void dismissLoading();

        void showNetworkError();

        String getVideoPrice();

        void finishActivity();

        String getAudioPrice();

        void modificationPrice();

        void getVideoTime();

        void getVoiceTime();
    }

    interface IPresenter extends BasePresenter {
        //        void setTextString(String string);
        void savePrice();
        /**
         * 开始呼叫
         */
        void startInvite(String type);
        /**
         * 获取video的时间
         */
        void getVideoCountDown();
        /**
         * 获取voice的时间
         */
        void getVoiceCountDown();
    }
}
