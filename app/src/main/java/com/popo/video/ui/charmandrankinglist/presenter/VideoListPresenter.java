package com.popo.video.ui.charmandrankinglist.presenter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;


import com.popo.library.util.ToastUtil;
import com.popo.video.R;
import com.popo.video.common.CustomDialogAboutPay;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.VideoSquare;
import com.popo.video.data.model.VideoSquareList;
import com.popo.video.ui.charmandrankinglist.VideoListFragment;
import com.popo.video.ui.charmandrankinglist.adapter.VideoListAdapter;
import com.popo.video.ui.charmandrankinglist.contract.VideoListContract;

import java.util.List;

/**
 * Created by xuzhaole on 2018/3/20.
 */

public class VideoListPresenter implements VideoListContract.IPresenter {
    private VideoListAdapter adapter;
    private VideoListContract.IView mVideoListView;
    private Context mContext;


    public VideoListPresenter(VideoListContract.IView videoListView) {
        this.mVideoListView = videoListView;
        mContext = videoListView.obtainContext();
    }

    @Override
    public void start() {

        adapter = new VideoListAdapter(mVideoListView.getFragment().getChildFragmentManager());
        mVideoListView.setAdapter(adapter);
    }

    @Override
    public void loadVideoShowList(final int num) {
        ApiManager.VideoShowList(num + "", "5", new IGetDataListener<VideoSquareList>() {
            @Override
            public void onResult(VideoSquareList videoSquareList, boolean isEmpty) {
                Log.e("videoSquareList",videoSquareList.toString());
                if (videoSquareList != null){
                    if (videoSquareList.getVideoSquareList() == null || videoSquareList.getVideoSquareList().size() == 0) {
                        if (num == 1) {
                            mVideoListView.setEmptyView(true, null);
                        } else {
                            ToastUtil.showShortToast(mContext, mContext.getString(R.string.tip_no_more));
                        }
                    } else {
                        mVideoListView.setPostion();
                        if (num == 1) {
                            adapter.setData(videoSquareList.getVideoSquareList());
                        } else {
                            adapter.addData(videoSquareList.getVideoSquareList());
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mVideoListView.setErrorView(true, msg);
            }
        });
    }

    @Override
    public void loadOneShow() {
        ApiManager.VideoShowList("6", "1", new IGetDataListener<VideoSquareList>() {
            @Override
            public void onResult(VideoSquareList videoSquareList, boolean isEmpty) {
                if (videoSquareList.getVideoSquareList() == null || videoSquareList.getVideoSquareList().size() == 0) {
                    ToastUtil.showShortToast(mContext, mContext.getString(R.string.tip_no_more));
                } else {
                    mVideoListView.setPostion();
                    adapter.addData(videoSquareList.getVideoSquareList());
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mVideoListView.setErrorView(true, msg);
            }
        });
    }

    @Override
    public void reportShow(int currentPosition) {
        List<VideoSquare> data = adapter.getData();
        if (data != null && currentPosition < data.size()) {
            VideoSquare videoSquare = data.get(currentPosition);
            long guid = videoSquare.getUserBase().getGuid();
            CustomDialogAboutPay.reportShow(mContext, guid + "");
        }
    }
}
