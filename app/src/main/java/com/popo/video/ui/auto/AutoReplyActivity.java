package com.popo.video.ui.auto;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;

import com.popo.library.net.NetUtil;
import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.common.AutoReplyDialog;
import com.popo.video.common.YeMeiPopopUtil;
import com.popo.video.event.AutoReplyEvent;
import com.popo.video.event.ReplyEvent;
import com.popo.video.event.SingleLoginFinishEvent;
import com.popo.video.event.YeMeiMessageEvent;
import com.popo.video.ui.auto.contract.AutoReplyContract;
import com.popo.video.ui.auto.presenter.AutoReplyPresenter;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/11/2.
 */

public class AutoReplyActivity extends BaseFragmentActivity implements View.OnClickListener, AutoReplyContract.IView {
    @BindView(R.id.auto_reply_activity_rl_add)
    RelativeLayout rl_add;
    @BindView(R.id.auto_reply_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.auto_reply_activity_tablyout)
    TabLayout mTabLayout;
    @BindView(R.id.auto_reply_activity_viewpager)
    ViewPager mViewPager;
    private AutoReplyPresenter mAutoReplyPresenter;
    private int replyCount = 0;
    private int helloCount = 0;


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_auto_reply;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        mViewPager.setOffscreenPageLimit(1);
        mAutoReplyPresenter = new AutoReplyPresenter(this);
        mAutoReplyPresenter.addTabs();
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
        rl_add.setOnClickListener(this);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    if (replyCount == 5) {
                        rl_add.setVisibility(View.GONE);
                    } else {
                        rl_add.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (helloCount == 5) {
                        rl_add.setVisibility(View.GONE);
                    } else {
                        rl_add.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void loadData() {
        mAutoReplyPresenter.getAddCount();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void showNetworkError() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.auto_reply_activity_rl_add:
                int currentItem = mViewPager.getCurrentItem();
                AutoReplyDialog autoReplyDialog = new AutoReplyDialog();
                autoReplyDialog.setContext(mContext);
                autoReplyDialog.setCheckItem(currentItem == 0 ? 1 : 2);
                autoReplyDialog.recordAndMsgShow();
                break;
            case R.id.auto_reply_activity_rl_back:
                finish();
                break;
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void setAdapter(PagerAdapter pagerAdapter) {
        mViewPager.setAdapter(pagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public FragmentManager getManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void showAddCount(int replyCount, int helloCount) {
        this.replyCount = replyCount;
        this.helloCount = helloCount;
        if (mTabLayout.getSelectedTabPosition() == 0) {
            if (replyCount == 5) {
                rl_add.setVisibility(View.GONE);
            } else {
                rl_add.setVisibility(View.VISIBLE);
            }
        } else {
            if (helloCount == 5) {
                rl_add.setVisibility(View.GONE);
            } else {
                rl_add.setVisibility(View.VISIBLE);
            }
        }
    }

    @Subscribe
    public void onEvent(AutoReplyEvent event) {
        mAutoReplyPresenter.getAddCount();
    }

    @Subscribe
    public void onEvent(ReplyEvent event) {
        mAutoReplyPresenter.getAddCount();
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }
    @Subscribe
    public void onEvent(YeMeiMessageEvent evet){
        Message msg=new Message();
        msg.obj=evet;
        yeMeiHandler.sendMessage(msg);

    }
    private Handler yeMeiHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            YeMeiMessageEvent obj = (YeMeiMessageEvent) msg.obj;
            if(obj!=null){
                YeMeiPopopUtil.getInstance().showYeMei(AutoReplyActivity.this,mViewPager,obj,isFinishing());
            }
        }
    };
}
