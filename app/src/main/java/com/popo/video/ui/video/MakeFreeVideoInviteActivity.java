package com.popo.video.ui.video;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.common.Util;
import com.popo.video.parcelable.MakeFreeVideoParcelable;

import butterknife.BindView;

/**
 * Created by WangYong on 2018/1/23.
 */

public class MakeFreeVideoInviteActivity extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.make_free_video_invite_avatar)
    ImageView iv_avatar;
    @BindView(R.id.make_free_video_invite_nickname)
    TextView tv_nickname;
    @BindView(R.id.make_free_video_invite_tip)
    TextView tv_tip;
    @BindView(R.id.make_free_video_cancel)
    TextView tv_cancle;
    private MakeFreeVideoParcelable makeFreeVideoParcelable;
    Ringtone ringtone;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    LaunchHelper.getInstance().launchFinish(MakeFreeVideoInviteActivity.this, MakeFreeVideoActivity.class, makeFreeVideoParcelable);
                    break;
                case 2:
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                    ringtone = RingtoneManager.getRingtone(MakeFreeVideoInviteActivity.this, notification);
                    ringtone.play();
                    break;
                case 3:
                    if (ringtone != null && ringtone.isPlaying()) {
                        ringtone.stop();
                        ringtone = null;
                    }
                    break;
            }

        }
    };

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_make_free_invite;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        makeFreeVideoParcelable = (MakeFreeVideoParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        if (makeFreeVideoParcelable != null) {
            tv_nickname.setText(makeFreeVideoParcelable.nickname);
//            tv_tip.setText("正在呼叫" + makeFreeVideoParcelable.nickname + ".....");
            tv_tip.setText(getString(R.string.inviting_make_free, makeFreeVideoParcelable.nickname));
            ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(makeFreeVideoParcelable.icon).transform(new CropCircleTransformation(MakeFreeVideoInviteActivity.this))
                    .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(iv_avatar).build());
            handler.sendEmptyMessage(2);
            handler.sendEmptyMessageDelayed(1, 2000);
        }

    }

    @Override
    protected void setListeners() {
        iv_avatar.setOnClickListener(this);
        tv_nickname.setOnClickListener(this);
        tv_tip.setOnClickListener(this);
        tv_cancle.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.make_free_video_cancel:

                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.sendEmptyMessage(3);
    }

    @Override
    public void onBackPressed() {
        // do nothing 屏蔽返回键
    }
}
