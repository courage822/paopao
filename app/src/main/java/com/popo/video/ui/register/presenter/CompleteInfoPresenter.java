package com.popo.video.ui.register.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.popo.library.util.LaunchHelper;
import com.popo.video.common.ParamsUtils;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.PlatformInfo;
import com.popo.video.data.model.UpLoadMyInfo;
import com.popo.video.data.model.UpLoadMyPhoto;
import com.popo.video.data.model.UploadInfoParams;
import com.popo.video.data.model.UserBase;
import com.popo.video.data.model.UserPhoto;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.event.UserInfoChangedEvent;
import com.popo.video.ui.main.MainActivity;
import com.popo.video.ui.register.contract.CompleteInfoContract;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

/**
 * Created by zhangdroid on 2017/5/31.
 */
public class CompleteInfoPresenter implements CompleteInfoContract.IPresenter {
    private CompleteInfoContract.IView mCompleteInfo;
    private Context mContext;

    public CompleteInfoPresenter(CompleteInfoContract.IView mCompleteInfo) {
        this.mCompleteInfo = mCompleteInfo;
        mContext = mCompleteInfo.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void completeInfo() {
        mCompleteInfo.showLoading();
        UploadInfoParams uploadInfoParams = new UploadInfoParams();
        if (!TextUtils.isEmpty(mCompleteInfo.getHeigth())) {
            String height;
            if (mCompleteInfo.getHeigth().contains("cm")) {
                height = mCompleteInfo.getHeigth().replace("cm", "");
            } else {
                height = DataPreference.getCmByInch(mCompleteInfo.getHeigth());
            }
            uploadInfoParams.setHeight(height);
        }
        if (!TextUtils.isEmpty(mCompleteInfo.getQingGan())) {
            uploadInfoParams.setRelationshipId(DataPreference.getPersonMapKey(mCompleteInfo.getQingGan(), 1));
        }

        if (!TextUtils.isEmpty(mCompleteInfo.getSign())) {
            uploadInfoParams.setSign(ParamsUtils.getSignkey(mCompleteInfo.getSign()));
        }

        ApiManager.upLoadMyInfo(uploadInfoParams, new IGetDataListener<UpLoadMyInfo>() {
            @Override
            public void onResult(UpLoadMyInfo upLoadMyInfo, boolean isEmpty) {
                LaunchHelper.getInstance().launchFinish(mContext, MainActivity.class);
                // mCompleteInfo.dismissLoading();
                // 发送用户资料改变事件
                EventBus.getDefault().post(new UserInfoChangedEvent());
                if (upLoadMyInfo != null) {
                    UserBase userBase = upLoadMyInfo.getUserBase();
                    PlatformInfo platformInfo = PlatformPreference.getPlatformInfo();
                    platformInfo.setFid(userBase.getFromChannel() + "");
                    platformInfo.setCountry(DataPreference.getCountry(userBase.getCountry()));
                    UserPreference.setCountryId(userBase.getCountry());
                    PlatformPreference.setPlatfromInfo(platformInfo);
                    if (userBase != null) {
                        // 更新本地用户信息
                        UserPreference.saveUserInfo(userBase);
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mCompleteInfo.dismissLoading();
                if (isNetworkError) {
                    mCompleteInfo.showNetworkError();
                } else {
                    mCompleteInfo.showTip(msg);
                }
            }
        });
    }

    @Override
    public void upLoadAvator(File file, boolean flag) {
        mCompleteInfo.isSeeZheZhao(true);
        ApiManager.upLoadMyPhotoOrAvator(file, flag, new IGetDataListener<UpLoadMyPhoto>() {
            @Override
            public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                mCompleteInfo.canOnClick();
                if (upLoadMyPhoto != null) {
                    UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                    if (userPhoto != null) {
                        final String stringFile = userPhoto.getFileUrlMiddle();
                        String fileUrl = userPhoto.getFileUrl();
                        String fileUrlMinimum = userPhoto.getFileUrlMinimum();
                        if (!TextUtils.isEmpty(stringFile)) {
                            UserPreference.setOriginalImage(fileUrl);
                            UserPreference.setMiddleImage(stringFile);
                            UserPreference.setSmallImage(fileUrlMinimum);
//                            mCompleteInfo.setUserAvator(stringFile);
                        }
                    }
                    mCompleteInfo.isSeeZheZhao(false);
//                    mCompleteInfo.showTip(mContext.getString(R.string.upload_success));
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
//                mCompleteInfo.isSeeZheZhao(false);
                mCompleteInfo.canOnClick();
                mCompleteInfo.uploadFail();
//                mCompleteInfo.setUserAvator(null);
//                mCompleteInfo.showTip(mContext.getString(R.string.upload_fail));
                if (isNetworkError) {
                    mCompleteInfo.showNetworkError();
                } else {
                    mCompleteInfo.showTip(msg);
                }
            }
        });
    }

    @Override
    public void loadInfoData() {
        ApiManager.getFaceBookAccount(new IGetDataListener<String>() {
            @Override
            public void onResult(String faceBookAccount, boolean isEmpty) {
                if (faceBookAccount != null) {
                    DataPreference.saveDataJson(faceBookAccount);
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }
}
