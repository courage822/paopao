package com.popo.video.ui.charmandrankinglist.presenter;

import android.content.Context;

import com.popo.video.R;
import com.popo.video.data.api.ApiConstant;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.CharmandAndDranking;
import com.popo.video.data.model.RankList;
import com.popo.video.data.model.UserBase;
import com.popo.video.data.model.UserMend;
import com.popo.video.ui.charmandrankinglist.adapter.CharmandAndDrankingAdapter;
import com.popo.video.ui.charmandrankinglist.contract.CharmandListContract;
import com.popo.video.ui.charmandrankinglist.contract.RrankingListContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WangYong on 2017/10/31.
 */

public class RrankingListPresenter implements RrankingListContract.IPresenter{
    private CharmandListContract.IView mCharmandAndDranking1;
    private Context mContext;
    private CharmandAndDrankingAdapter charmandAdapter2;
    private  List<RankList> rankList2=new ArrayList<>();
    public RrankingListPresenter(CharmandListContract.IView mCharmandAndDranking) {
        this.mCharmandAndDranking1 = mCharmandAndDranking;
        mContext=mCharmandAndDranking.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void loadRichUserList() {
        charmandAdapter2=new CharmandAndDrankingAdapter(mContext, R.layout.charmand_dranking_item,1);
        mCharmandAndDranking1.setAdapter(charmandAdapter2,R.layout.common_load_more);
        ApiManager.getCharmandUser(ApiConstant.URL_RICH_USER,new IGetDataListener<CharmandAndDranking>() {
            @Override
            public void onResult(CharmandAndDranking charmandAndDranking, boolean isEmpty) {
                List<RankList> rankList = charmandAndDranking.getRankList();
                if(rankList!=null&&rankList.size()>0){
                    UserBase userBase1 = rankList.get(1).getUserBase();
                    UserBase userBase2 = rankList.get(0).getUserBase();
                    UserBase userBase3 = rankList.get(2).getUserBase();
                    UserMend userMend1 = rankList.get(1).getUserMend();
                    UserMend userMend2 = rankList.get(0).getUserMend();
                    UserMend userMend3 = rankList.get(2).getUserMend();
                    if(userBase1!=null&&userMend1!=null){
                        mCharmandAndDranking1.setAvatarAndGift1(userBase1.getIconUrlMiddle(),userBase1.getNickName(),userMend1.getSendGiftCount());
                    }
                    if(userBase2!=null&&userMend2!=null){
                        mCharmandAndDranking1.setAvatarAndGift2(userBase2.getIconUrlMiddle(),userBase2.getNickName(),userMend2.getSendGiftCount());
                    }
                    if(userBase3!=null&&userMend3!=null){
                        mCharmandAndDranking1.setAvatarAndGift3(userBase3.getIconUrlMiddle(),userBase3.getNickName(),userMend3.getSendGiftCount());
                    }

                    for (int i = 3; i < rankList.size(); i++) {
                        rankList2.add(rankList.get(i));
                    }
                    charmandAdapter2.bind(rankList2);
                }
                mCharmandAndDranking1.hideLoadMore();
                mCharmandAndDranking1.showNoMore();
                mCharmandAndDranking1.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });

    }

    @Override
    public void refresh2() {
        mCharmandAndDranking1.hideRefresh(1);
    }
}
