package com.popo.video.ui.dialog;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.library.dialog.BaseDialogFragment;


/**
 * 两个按钮的对话框
 * Created by zhangdroid on 2016/6/24.
 */
public class DoubleButtonDialog extends BaseDialogFragment {
    private OnDoubleDialogClickListener mOnDialogClickListener;

    public static DoubleButtonDialog newInstance(String title, String message, String positive, String negative, boolean isCancelable, OnDoubleDialogClickListener listener) {
        DoubleButtonDialog doubleButtonDialog = new DoubleButtonDialog();
        doubleButtonDialog.mOnDialogClickListener = listener;
        doubleButtonDialog.setArguments(getDialogBundle(title, message, positive, negative, isCancelable));
        return doubleButtonDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_double_btn;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tvTitle = (TextView) view.findViewById(R.id.dialog_two_btn_title);
        View dividerView = view.findViewById(R.id.dialog_two_btn_divider);// 标题栏下分割线
        TextView tvMessage = (TextView) view.findViewById(R.id.dialog_two_btn_content);
        Button btnPositive = (Button) view.findViewById(R.id.dialog_two_btn_sure);
        Button btnNegative = (Button) view.findViewById(R.id.dialog_two_btn_cancel);

        String title = getDialogTitle();
        if (!TextUtils.isEmpty((title))) {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
            dividerView.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
        }

        String message = getDialogMessage();
        if (!TextUtils.isEmpty((message))) {
            tvMessage.setText(message);
            tvMessage.setVisibility(View.VISIBLE);
        } else {
            tvMessage.setVisibility(View.GONE);
        }

        String positive = getDialogPositive();
        if (!TextUtils.isEmpty((positive))) {
            btnPositive.setText(positive);
        }

        String negative = getDialogNegative();
        if (!TextUtils.isEmpty((negative))) {
            btnNegative.setText(negative);
        }

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDialogClickListener != null) {
                    mOnDialogClickListener.onPositiveClick(v);
                }
                dismiss();
            }
        });
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDialogClickListener != null) {
                    mOnDialogClickListener.onNegativeClick(v);
                }
                dismiss();
            }
        });
    }

}
