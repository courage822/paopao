package com.popo.video.ui.pay.activity;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.popo.library.net.NetUtil;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.common.YeMeiPopopUtil;
import com.popo.video.event.YeMeiMessageEvent;
import com.popo.video.parcelable.PayParcelable;
import com.popo.video.ui.pay.adapter.PayAdapter;
import com.popo.video.ui.pay.contract.RechargeContract;
import com.popo.video.ui.pay.presenter.RechargePresenter;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 支付页面
 * Created by zhangdroid on 2017/5/25.
 */
public class RechargeActivity extends BaseFragmentActivity implements RechargeContract.IView, View.OnClickListener {
    @BindView(R.id.recharge_root)
    LinearLayout mLlRoot;
    @BindView(R.id.recharge_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.recharge_activity_viewpager)
    ViewPager mViewPager;
    private RechargePresenter mRechargePresenter;
    private PayParcelable mPayParcelable;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_recharge;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mPayParcelable = (PayParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        mViewPager.setOffscreenPageLimit(1);
        mRechargePresenter = new RechargePresenter(this);
        mRechargePresenter.addTabs();
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void loadData() {
        mRechargePresenter.getPayWay(mPayParcelable.fromTag);
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
        mRechargePresenter.getPayWay(mPayParcelable.fromTag);
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void dismissLoading() {
        toggleShowLoading(false, null);
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void setAdapter(PagerAdapter pagerAdapter) {
        mViewPager.setAdapter(pagerAdapter);
        if(mPayParcelable.type==1){
            mViewPager.setCurrentItem(2);
        }
    }

    @Override
    public FragmentManager getManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void setAdapter(PayAdapter payAdapter) {

    }

    @Override
    public void setTextDetail(String detial) {

    }

    @Override
    public void getKeyNum(String num) {

    }

    @Override
    public void getNowMoney(String money) {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.recharge_activity_rl_back:
                finish();
                break;
        }
    }
    @Subscribe
    public void onEvent(YeMeiMessageEvent evet){
        Message msg=new Message();
        msg.obj=evet;
        yeMeiHandler.sendMessage(msg);

    }
    private Handler yeMeiHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.e("AAAAAAA", "handleMessage: 222222222222222222222222222222222222");
            YeMeiMessageEvent obj = (YeMeiMessageEvent) msg.obj;
            if(obj!=null){
                YeMeiPopopUtil.getInstance().showYeMei(RechargeActivity.this,mLlRoot,obj,isFinishing());

            }
        }
    };
}
