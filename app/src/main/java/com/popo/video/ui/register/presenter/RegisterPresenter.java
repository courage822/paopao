package com.popo.video.ui.register.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.popo.video.base.CommonWebActivity;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.Register;
import com.popo.video.data.model.UserBase;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.event.FinishEvent;
import com.popo.library.util.LaunchHelper;
import com.popo.video.parcelable.WebParcelable;
import com.popo.video.ui.login.LoginActivity;
import com.popo.video.ui.register.CompleteInfoActivity;
import com.popo.video.ui.register.contract.RegisterContract;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by zhangdroid on 2017/5/31.
 */
public class RegisterPresenter implements RegisterContract.IPresenter {
    private RegisterContract.IView mRegisterView;
    private Context mContext;

    public RegisterPresenter(RegisterContract.IView view) {
        this.mRegisterView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void register() {
        if (TextUtils.isEmpty(mRegisterView.getAge().toString())){
            return;
        }
        mRegisterView.showLoading();
        Log.e("platform", PlatformPreference.getPlatformJsonString());
        ApiManager.register(mRegisterView.getNickname(), mRegisterView.getSex(), mRegisterView.getAge(),
                new IGetDataListener<Register>() {
                    @Override
                    public void onResult(Register register, boolean isEmpty) {
                        Log.e("register", register.toString());
                        // 保存用户信息到本地
                        UserBase userBase = register.getUserBase();
                        if (null != userBase) {
                            UserPreference.saveUserInfo(userBase);
                            handleHyphenateLoginResult();
                            DbModle.getInstance().getUserAccountDao().deleteAllData();//删除数据库中的所有数据
                        } else {
                            handleHyphenateLoginResult();
                            DbModle.getInstance().getUserAccountDao().deleteAllData();//删除数据库中的所有数据
                        }
                        UserPreference.guided();//第一次使用时的标记
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                        mRegisterView.dismissLoading();
                        if (isNetworkError) {
                            mRegisterView.showNetworkError();
                        } else {
                            mRegisterView.showTip(msg);
                        }
                    }
                });
    }

    @Override
    public void goLoginPage() {
        LaunchHelper.getInstance().launch(mContext, LoginActivity.class);
    }

    @Override
    public void showAgreement(String title, String url) {
        LaunchHelper.getInstance().launch(mContext, CommonWebActivity.class,
                new WebParcelable(title, url, false));
    }

    @Override
    public void checkValid() {
        mRegisterView.setRegisterEnable();
    }

    private void handleHyphenateLoginResult() {
        mRegisterView.dismissLoading();
        // 关闭之前打开的页面
        EventBus.getDefault().post(new FinishEvent());
        //删除数据库中所有内容
        // 跳转主页面
        LaunchHelper.getInstance().launchFinish(mContext, CompleteInfoActivity.class);
    }

}
