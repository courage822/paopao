package com.popo.video.ui.personalcenter;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Parcelable;
import android.os.PowerManager;
import android.text.TextUtils;
import android.view.View;

import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.library.net.NetUtil;
import com.popo.library.widget.PlayView;
import com.popo.video.parcelable.PlayViewParcelable;

import butterknife.BindView;

/**
 * Created by Administrator on 2017/6/20.
 * 播放视频的Activity，时间有限暂时不做抽取
 */

public class PlayActivity extends BaseFragmentActivity{
    @BindView(R.id.play_activity_palayview)
    PlayView playView;
    private String stringExtra;
    private long playPostion = -1;
    private long duration = -1;
    private PlayViewParcelable mPlayViewParcelable;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_play;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mPlayViewParcelable= (PlayViewParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
//        stringExtra=getIntent().getStringExtra("key");
        stringExtra=mPlayViewParcelable.urlData;
        initPlayView();
        play();
    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }
    private void initPlayView() {
        if(!TextUtils.isEmpty(stringExtra)){
            playView.setVideoURI(Uri.parse(stringExtra));
            playView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    playView.seekTo(1);
                    finish();
                }
            });
            playView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    //获取视频资源的宽度
                    int videoWidth = mp.getVideoWidth();
                    //获取视频资源的高度
                    int videoHeight = mp.getVideoHeight();
                    playView.setSizeH(videoHeight);
                    playView.setSizeW(videoWidth);
                    playView.requestLayout();
                    duration = mp.getDuration();
                }
            });
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            boolean isScreenOn = pm.isScreenOn();//如果为true，则表示屏幕“亮”了，否则屏幕“暗”了。
            if (!isScreenOn) {
                pauseVideo();
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (playPostion > 0) {
            pauseVideo();
        }
        playView.seekTo((int) ((playPostion > 0 && playPostion < duration) ? playPostion : 1));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        playView.stopPlayback();
    }

    @Override
    protected void onPause() {
        super.onPause();
        playView.pause();
        playPostion = playView.getCurrentPosition();
        pauseVideo();

    }
    private void pauseVideo() {
        playView.pause();
    }
    private void startVideo() {
        playView.start();

    }
    /**
     * 播放
     */
    private void play() {
        if (playView.isPlaying()) {
            pauseVideo();
        } else {
            if (playView.getCurrentPosition() == playView.getDuration()) {
                playView.seekTo(0);
            }
            startVideo();
        }
    }

}
