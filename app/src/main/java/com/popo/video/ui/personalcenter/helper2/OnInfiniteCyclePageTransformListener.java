package com.popo.video.ui.personalcenter.helper2;

import android.view.View;

/**
 * Created by xuzhaole on 2017/12/28.
 */

public interface OnInfiniteCyclePageTransformListener {
    void onPreTransform(final View page, final float position);

    void onPostTransform(final View page, final float position);
}
