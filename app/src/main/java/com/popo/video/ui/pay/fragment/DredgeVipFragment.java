package com.popo.video.ui.pay.fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.base.BaseFragment;
import com.popo.video.common.MarqueeView;
import com.popo.video.data.preference.UserPreference;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.parcelable.PayInfoParcelable;
import com.popo.video.ui.pay.activity.PayActivity;
import com.popo.video.ui.pay.adapter.DredgeVipAdapter;
import com.popo.video.ui.pay.contract.DredgeVipContract;
import com.popo.video.ui.pay.presenter.DredgeVipPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.popo.video.R.id.fragment_dregevip_ll_30;

/**
 * Created by WangYong on 2017/8/29.
 */

public class DredgeVipFragment extends BaseFragment implements View.OnClickListener,DredgeVipContract.IView{
    @BindView(R.id.fragment_dregevip_ll_root)
    RelativeLayout mLlRoot;
    @BindView(fragment_dregevip_ll_30)
    LinearLayout ll_30;
    @BindView(R.id.fragment_dregevip_ll_90)
    LinearLayout ll_90;
    @BindView(R.id.fragment_dregevip_iv_check_icon_30)
    ImageView iv_check_icon_30;
    @BindView(R.id.fragment_dregevip_iv_check_icon_90)
    ImageView iv_check_icon_90;
    @BindView(R.id.fragment_dregevip_tv_pay)
    TextView tv_pay;
    @BindView(R.id.fragment_dregevip_tv_vip_num)
    TextView tv_vip_num;
    @BindView(R.id.fragment_dregevip_tv_servicename1)
    TextView tv_servicename1;
    @BindView(R.id.fragment_dregevip_tv_servicename2)
    TextView tv_servicename2;
    @BindView(R.id.fragment_dregevip_tv_price1)
    TextView tv_price1;
    @BindView(R.id.fragment_dregevip_tv_price2)
    TextView tv_price2;
    @BindView(R.id.fragment_dregevip_tv_onedayprice1)
    TextView tv_onedayprice1;
    @BindView(R.id.fragment_dregevip_tv_onedayprice2)
    TextView tv_onedayprice2;
    @BindView(R.id.fragment_dregevip_recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.fragment_dregevip_tv_serviceid1)
    TextView tv_serviceId1;
    @BindView(R.id.fragment_dregevip_tv_serviceid2)
    TextView tv_serviceId2;
    @BindView(R.id.fragment_dregevip_tv_gundong)
    MarqueeView tv_gundong;
    @BindView(R.id.fragment_dregevip_tv_flag1)
    TextView tv_flag1;
    @BindView(R.id.fragment_dregevip_tv_flag2)
    TextView tv_flag2;
    private String serviceId = "";
    private DredgeVipPresenter mDredgeVipPresenter;
    private LinearLayoutManager linearLayoutManager;
    private String serviceName = "";
    private String price = "";
    private int pos=0;
    List<View> views1 = new ArrayList<>();

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_dregevip_layout;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mDredgeVipPresenter = new DredgeVipPresenter(this);
        mDredgeVipPresenter.getPayWay("1");
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        serviceId = getServiceId1();
        serviceName = getSerViceName1();
        price = getPrice1();

        switch (UserPreference.getCountryId()) {
            case "97"://美国
                tv_flag1.setText("$");
                tv_flag2.setText("$");
                break;
            case "16"://澳大利亚
                tv_flag1.setText("AU$");
                tv_flag2.setText("AU$");
                break;
            case "67"://加拿大
                tv_flag1.setText("CA$");
                tv_flag2.setText("CA$");
                break;
            case "174"://香港
                tv_flag1.setText("HK$");
                tv_flag2.setText("HK$");
                break;
            case "161"://印度
                tv_flag1.setText("INR");
                tv_flag2.setText("INR");
                break;
            case "162"://印度尼西亚
                tv_flag1.setText("IDR");
                tv_flag2.setText("IDR");
                break;
            case "10"://爱尔兰
                tv_flag1.setText("€");
                tv_flag2.setText("€");
                break;
            case "152"://新西兰
                tv_flag1.setText("NZ$");
                tv_flag2.setText("NZ$");
                break;
            case "20"://巴基斯坦
                tv_flag1.setText("PKR");
                tv_flag2.setText("PKR");
                break;
            case "46"://菲律宾
                tv_flag1.setText("PHP");
                tv_flag2.setText("PHP");
                break;
            case "150"://新加坡
                tv_flag1.setText("S$");
                tv_flag2.setText("S$");
                break;
            case "108"://南非
                tv_flag1.setText("ZAR");
                tv_flag2.setText("ZAR");
                break;
            case "163"://英国
                tv_flag1.setText("￡");
                tv_flag2.setText("￡");
                break;
            case "173"://台湾
                tv_flag1.setText("NT$");
                tv_flag2.setText("NT$");
                break;
//            case "171"://中国
//                break;
        }
    }

    @Override
    protected void setListeners() {
        ll_30.setOnClickListener(this);
        ll_90.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_dregevip_ll_30:
                setBackground(30);
                serviceId = getServiceId1();
                serviceName = getSerViceName1();
                price = getPrice1();
                pos=0;
                break;
            case R.id.fragment_dregevip_ll_90:
                setBackground(90);
                serviceId = getServiceId2();
                serviceName = getSerViceName2();
                price = getPrice2();
                pos=1;
                break;
//            case R.id.fragment_dregevip_tv_pay:
//                if(!TextUtils.isEmpty(serviceId)){
//                    LaunchHelper.getInstance().launch(mContext,PayActivity.class,new PayInfoParcelable(serviceId,serviceName,2,getPrice1()));
//                }else{
//                    LaunchHelper.getInstance().launch(mContext,PayActivity.class,new PayInfoParcelable(getServiceId1(),serviceName,2,price));
//                }
//                break;
        }
    }

    private void setBackground(int i) {
        Drawable drawable = mContext.getDrawable(R.drawable.dregevip_tab_select_main_color);
        Drawable drawable2 = mContext.getDrawable(R.drawable.dregevip_tab_select_gray_color);
        if (i == 30) {
            ll_30.setBackground(drawable);
            ll_90.setBackground(drawable2);
            iv_check_icon_30.setVisibility(View.VISIBLE);
            iv_check_icon_90.setVisibility(View.GONE);
        } else {
            ll_90.setBackground(drawable);
            ll_30.setBackground(drawable2);
            iv_check_icon_90.setVisibility(View.VISIBLE);
            iv_check_icon_30.setVisibility(View.GONE);
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void dismissLoading() {
        toggleShowLoading(false, null);
    }

    @Override
    public void setInfoString1(String serviceName1, final String price3, String onedayprice1) {
        price = price3;
        tv_servicename1.setText(serviceName1 + mContext.getString(R.string.day));
        tv_price1.setText(price3);
        tv_onedayprice1.setText("（" + onedayprice1 + "）");
        tv_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(price)) {
                    LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayInfoParcelable(serviceId, serviceName, 2, price,pos));
                }
            }
        });
    }

    @Override
    public void setInfoString2(String serviceName2, String price2, String onedayprice2) {
        tv_servicename2.setText(serviceName2 + mContext.getString(R.string.day));
        tv_price2.setText(price2);
        tv_onedayprice2.setText("（" + onedayprice2 + "）");
    }


    @Override
    public void getVipCount(String vipCount) {
        tv_vip_num.setText(vipCount);
    }

    @Override
    public void setAdapter(DredgeVipAdapter adapter) {
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void getPayType1(String payType) {
        serviceId = payType;
        tv_serviceId1.setText(payType);
    }

    @Override
    public void getPayType2(String payType) {
        tv_serviceId2.setText(payType);
    }

    @Override
    public String getSerViceName1() {
        return tv_servicename1.getText().toString();
    }

    @Override
    public String getSerViceName2() {
        return tv_servicename2.getText().toString();
    }

    @Override
    public String getServiceId1() {
        return tv_serviceId1.getText().toString();
    }

    @Override
    public String getServiceId2() {
        return tv_serviceId2.getText().toString();
    }

    @Override
    public String getPrice1() {
        return tv_price1.getText().toString();
    }

    @Override
    public String getPrice2() {
        return tv_price2.getText().toString();
    }

    @Override
    public void getGunDongText(List<String> title, List<String> content) {
        setViewTwoLines(title, content);
        tv_gundong.setViews(views1);
    }

    private void setViewTwoLines(List<String> title, List<String> content) {
        views1.clear();//记得加这句话，不然可能会产生重影现象
        for (int i = 0; i < content.size(); i = i + 2) {
            final int position = i;
            //设置滚动的单个布局
            LinearLayout moreView = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.item_view, null);
            //初始化布局的控件
            TextView tv1 = (TextView) moreView.findViewById(R.id.tv1);
            TextView tv2 = (TextView) moreView.findViewById(R.id.tv2);
            TextView tv3 = (TextView) moreView.findViewById(R.id.tv3);
            TextView tv_title1 = (TextView) moreView.findViewById(R.id.title_tv1);
            TextView tv_title2 = (TextView) moreView.findViewById(R.id.title_tv2);
            TextView tv_title3 = (TextView) moreView.findViewById(R.id.title_tv3);

            //进行对控件赋值

            tv1.setText(content.get(i).toString());
            tv_title1.setText("[" + title.get(i) + "]");
            if (content.size() > i + 2) {//奇数条
                tv2.setText(content.get(i + 1).toString());
                tv3.setText(content.get(i + 2).toString());
                tv_title2.setText("[" + title.get(i + 1) + "]");
                tv_title3.setText("[" + title.get(i + 2) + "]");
            } else {//偶数条
                //因为淘宝那儿是两条数据，但是当数据是奇数时就不需要赋值第二个，所以加了一个判断，还应该把第二个布局给隐藏掉
                //moreView.findViewById(R.id.rl2).setVisibility(View.GONE);
                //修改了最后一个没有 将第一个拼接到最后显示
                tv2.setText(content.get(0).toString());
                tv3.setText(content.get(1).toString());
                tv_title2.setText("[" + title.get(0) + "]");
                tv_title3.setText("[" + title.get(1) + "]");
            }

            //添加到循环滚动数组里面去
            views1.add(moreView);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (tv_gundong != null) {
                tv_gundong.startFlipping();
            }
        } else {
            if (tv_gundong != null) {
                tv_gundong.stopFlipping();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        tv_gundong.startFlipping();
    }

    @Override
    public void onStop() {
        super.onStop();
        tv_gundong.stopFlipping();
    }
}
