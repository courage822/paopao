package com.popo.video.ui.video;

import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Parcelable;
import android.provider.Settings;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.common.TimeUtils;
import com.popo.video.customload.DesktopLayout;
import com.popo.video.customload.WaterRadarView;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.event.FinishMassVideoOrVoiceActivity;
import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.parcelable.ListParcelable;
import com.popo.video.ui.dialog.DialogUtil;
import com.popo.video.ui.dialog.OnDoubleDialogClickListener;
import com.popo.video.ui.video.contract.MassVideoOrVoiceContract;
import com.popo.video.ui.video.presenter.MassVideoOrVoicePresenter;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/11/25.
 */

public class MassVideoOrVoiceActivity extends BaseFragmentActivity implements View.OnClickListener, MassVideoOrVoiceContract.IView {
    @BindView(R.id.mass_activity_iv_shrink)
    ImageView iv_shrink;
    @BindView(R.id.mass_activity_iv_avatar)
    WaterRadarView view;
    @BindView(R.id.mass_activity_tv_time)
    TextView tv_time;
    @BindView(R.id.mass_activity_iv_miss)
    ImageView iv_miss;
    @BindView(R.id.mass_activity_tv_person_num)
    TextView tv_person_num;
    @BindView(R.id.mass_activity_tv_price)
    TextView tv_price;
    @BindView(R.id.mass_activity_tv_cancle)
    TextView tv_cancle;
    @BindView(R.id.mass_activity_tv_video_or_voice)
    TextView tv_video_or_voice;
    @BindView(R.id.mass_activity_tv_video_or_voice2)
    TextView tv_video_or_voice2;
    private WindowManager mWindowManager;
    private WindowManager.LayoutParams mLayout;
    private DesktopLayout mDesktopLayout;
    private boolean isCanShow = false;//能否打开悬浮窗
    private boolean flag = false;//进入权限界面是否打开权限的监听
    private boolean isOpen = false;//是否已经打开悬浮窗
    private boolean isClick = false;//是否点击了悬浮窗
    private boolean isStop = false;//是否能够挂断
    // 声明屏幕的宽高
    float x, y;
    int top;
    private MassVideoOrVoicePresenter massVideoOrVoicePresenter;
    Ringtone ringtone;
    private ListParcelable listParcelable;

    /**
     * 创建悬浮窗体
     */
    private void createDesktopLayout() {
        mDesktopLayout = new DesktopLayout(this);
        mDesktopLayout.setOnTouchListener(new View.OnTouchListener() {
            float mTouchStartX;
            float mTouchStartY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // 获取相对屏幕的坐标，即以屏幕左上角为原点
                x = event.getRawX();
                y = event.getRawY() - top; // 25是系统状态栏的高度
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // 获取相对View的坐标，即以此View左上角为原点
                        mTouchStartX = event.getX();
                        mTouchStartY = event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        // 更新浮动窗口位置参数
                        mLayout.x = (int) (x - mTouchStartX);
                        mLayout.y = (int) (y - mTouchStartY);
                        mWindowManager.updateViewLayout(v, mLayout);
                        break;
                    case MotionEvent.ACTION_UP:

                        // 更新浮动窗口位置参数
                        mLayout.x = (int) (x - mTouchStartX);
                        mLayout.y = (int) (y - mTouchStartY);
                        mWindowManager.updateViewLayout(v, mLayout);
                        // 可以在此记录最后一次的位置
                        mTouchStartX = mTouchStartY = 0;
                        break;
                }
                return false;
            }
        });
        mDesktopLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LaunchHelper.getInstance().launch(MassVideoOrVoiceActivity.this, MassVideoOrVoiceActivity.class, new ListParcelable(listParcelable.type));
                closeDesk();
                isClick = true;
            }
        });
        mDesktopLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return true;//处理点击事件的冲突
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Rect rect = new Rect();
        // /取得整个视图部分,注意，如果你要设置标题样式，这个必须出现在标题样式之后，否则会出错
        getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        top = rect.top;//状态栏的高度，所以rect.height,rect.width分别是系统的高度的宽度
    }

    /**
     * 显示DesktopLayout
     */
    private void showDesk() {
//        mWindowManager.removeViewImmediate(mDesktopLayout);
        if (ringtone.isPlaying()) {
            ringtone.stop();
        }
        mWindowManager.addView(mDesktopLayout, mLayout);
        isOpen = true;
        finish();
    }

    /**
     * 关闭DesktopLayout
     */
    private void closeDesk() {
        mWindowManager.removeView(mDesktopLayout);
    }

    /**
     * 设置WindowManager
     */
    private void createWindowManager() {
        // 取得系统窗体
        mWindowManager = (WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE);

        // 窗体的布局样式
        mLayout = new WindowManager.LayoutParams();

        // 设置窗体显示类型——TYPE_SYSTEM_ALERT(系统提示)
        mLayout.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;

        // 设置窗体焦点及触摸：
        // FLAG_NOT_FOCUSABLE(不能获得按键输入焦点)
        mLayout.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;

        // 设置显示的模式
        mLayout.format = PixelFormat.RGBA_8888;

        // 设置对齐的方法
        mLayout.gravity = Gravity.TOP | Gravity.LEFT;

        // 设置窗体宽度和高度
        mLayout.width = WindowManager.LayoutParams.WRAP_CONTENT;
        mLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
    }

    @Override
    protected void onResume() {
        super.onResume();
        view.start();
        view.setmImageUrl(UserPreference.getMiddleImage());
        if (flag) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (Settings.canDrawOverlays(MassVideoOrVoiceActivity.this)) {
                    showDesk();
                }
            }
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_mass_video_or_voice;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        listParcelable = (ListParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        massVideoOrVoicePresenter = new MassVideoOrVoicePresenter(this);
        createWindowManager();
        createDesktopLayout();
        checkMessagePermission();
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        ringtone = RingtoneManager.getRingtone(MassVideoOrVoiceActivity.this, notification);
        ringtone.play();
    }

    @Override
    protected void setListeners() {
        iv_shrink.setOnClickListener(this);
        iv_miss.setOnClickListener(this);

    }

    @Override
    protected void loadData() {
        if (listParcelable.type == 1) {
            massVideoOrVoicePresenter.initData("1");
            tv_video_or_voice.setText(getString(R.string.video_video));
            tv_video_or_voice2.setText(getString(R.string.video_video));
        } else {
            massVideoOrVoicePresenter.initData("2");
            tv_video_or_voice.setText(getString(R.string.user_detail_voice));
            tv_video_or_voice2.setText(getString(R.string.user_detail_voice));
        }
        massVideoOrVoicePresenter.startCountTime();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mass_activity_iv_shrink:
                if (isCanShow) {
                    showDesk();
                } else {
                    finish();
                }
                break;
            case R.id.mass_activity_iv_miss:
                if (isStop) {
                    if (ringtone.isPlaying()) {
                        ringtone.stop();
                    }
                   DataPreference.saveTime(0);
                    DataPreference.saveCurrentTime(0);
                    finish();
                } else {
                    Toast.makeText(MassVideoOrVoiceActivity.this, getString(R.string.message_cancel_invite), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void checkMessagePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(MassVideoOrVoiceActivity.this)) {
                DialogUtil.showDoubleBtnDialog(getSupportFragmentManager(), getString(R.string.permission_confirm), getString(R.string.open_suspension_permission), getString(R.string.confirm), getString(R.string.cancel), false, new OnDoubleDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                        intent.setData(Uri.parse("package:" + getPackageName()));
                        MassVideoOrVoiceActivity.this.startActivityForResult(intent, 100);
                        flag = true;
                    }

                    @Override
                    public void onNegativeClick(View view) {
                        finish();
                    }
                });
            } else {
                isCanShow = true;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isCanShow) {
            showDesk();
        } else {
            if (ringtone.isPlaying()) {
                ringtone.stop();
            }
            finish();
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void getSendNum(int num) {
        tv_person_num.setText(num + "");
    }

    @Override
    public void getSendTime(long time) {
        tv_time.setText(TimeUtils.fromSecondToTime(time / 1000, 2));
    }

    @Override
    public void finishActivity() {
        if (ringtone.isPlaying()) {
            ringtone.stop();
        }
        finish();
    }

    @Override
    public void timeOnFinish() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (Settings.canDrawOverlays(MassVideoOrVoiceActivity.this)) {
                if (mDesktopLayout != null && mWindowManager != null) {
                    if (isOpen && !isClick) {
                        closeDesk();
                    }
                }
            }
        }
    }

    @Override
    public void getAnchorPrice(String price) {
        tv_price.setText(price);
    }

    @Override
    public void clickStop() {
        isStop = true;
        iv_miss.setImageResource(R.drawable.stop_icon_red);
        tv_cancle.setVisibility(View.GONE);
    }

    @Override
    public void videoOrVoice(String voice) {

    }
    @Subscribe
    public void onEvent(FinishMassVideoOrVoiceActivity event){
        if (ringtone.isPlaying()) {
            ringtone.stop();
        }
        finish();
    }
}
