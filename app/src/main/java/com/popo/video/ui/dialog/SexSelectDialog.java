package com.popo.video.ui.dialog;

import android.support.v4.app.FragmentManager;
import android.view.View;

import com.popo.library.dialog.BaseDialogFragment;

/**
 * 性别选择框
 * Created by zhangdroid on 2017/5/31.
 */
public class SexSelectDialog extends BaseDialogFragment {
    private boolean mIsMale;
    private OnSexSelectedListener mOnSexSelectedListener;

    private static SexSelectDialog newInstance(boolean isMale, OnSexSelectedListener listener) {
        SexSelectDialog sexSelectDialog = new SexSelectDialog();
        sexSelectDialog.mIsMale = isMale;
        sexSelectDialog.mOnSexSelectedListener = listener;
        return sexSelectDialog;
    }

    @Override
    protected int getLayoutResId() {
        return 0;
    }

    @Override
    protected void setDialogContentView(View view) {

    }

    public static void show(FragmentManager fragmentManager, boolean isMale, OnSexSelectedListener onSexSelectedListener) {
    }

    public interface OnSexSelectedListener {
        void onSelected(boolean isMale);
    }

}
