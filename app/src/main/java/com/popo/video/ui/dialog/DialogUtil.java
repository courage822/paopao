package com.popo.video.ui.dialog;

import android.support.v4.app.FragmentManager;

import java.util.List;

/**
 * Created by zhangdroid on 2016/6/24.
 */
public class DialogUtil {
    private static final String TAG_DOUBLE_BUTTON = "doubleBtnDialog";
    private static final String TAG_SINGLE_BUTTON = "singleBtnDialog";
    private static final String TAG_DATE_SELECT = "dateSelectDialog";
    private static final String TAG_AGE_SELECT = "ageSelectDialog";
    private static final String TAG_TWO_AGE_SELECT = "twoAgeSelectDialog";

    /**
     * Show a double button dialog
     *
     * @param fragmentManager FragmentManager to show the dialog
     * @param title           title of dialog , it may be null
     * @param message         message of dialog ,it may be null
     * @param positive        positive button text ,if it is null, use the default
     * @param negative        negative button text ,if it is null, use the default
     * @param isCancelable    the dialog is cancelable, default is true
     * @param listener        the {@link OnDoubleDialogClickListener} listener for the double button dialog
     */
    public static void showDoubleBtnDialog(FragmentManager fragmentManager, String title, String message, String positive, String negative,
                                           boolean isCancelable, OnDoubleDialogClickListener listener) {
        DoubleButtonDialog.newInstance(title, message, positive, negative, isCancelable, listener).show(fragmentManager, TAG_DOUBLE_BUTTON);
    }

    public static void showSingleWheelDialog(FragmentManager fragmentManager, boolean flag,List<String> stringList, String title, String positive, String negative,
                                             boolean isCancelable, OneWheelDialog.OnOneWheelDialogClickListener listener) {
        SingleWheelDialog.newInstance(flag,stringList,title, positive, negative, isCancelable, listener).show(fragmentManager, TAG_AGE_SELECT);
    }

    /**
     * Show date select wheelview dialog
     *
     * @param fragmentManager FragmentManager to show the dialog
     * @param title           title of dialog , it may be null
     * @param positive        positive button text ,if it is null, use the default
     * @param isCancelable    the dialog is cancelable, default is true
     * @param listener        the {@link ThreeWheelDialog.OnThreeWheelDialogClickListener} listener for the wheelview dialog
     */
    public static void showDateSelectDialog(FragmentManager fragmentManager, String title, String positive, String negative,
                                            boolean isCancelable, ThreeWheelDialog.OnThreeWheelDialogClickListener listener) {
        DateSelectDialog.newInstance(title, positive, negative, isCancelable, listener).show(fragmentManager, TAG_DATE_SELECT);
    }
    /**
     * Show a double button dialog
     *
     * @param fragmentManager FragmentManager to show the dialog
     * @param title           title of dialog , it may be null
     * @param message         message of dialog ,it may be null
     * @param positive        positive button text ,if it is null, use the default
     * @param negative        negative button text ,if it is null, use the default
     * @param isCancelable    the dialog is cancelable, default is true
     * @param listener        the {@link OnDoubleDialogClickListener} listener for the double button dialog
     */
    public static void showEditDoubleBtnDialog(FragmentManager fragmentManager, String title, String message, String positive, String negative,
                                               boolean isCancelable, OnEditDoubleDialogClickListener listener) {
        EditDoubleButtonDialog.newInstance(title, message, positive, negative, isCancelable, listener).show(fragmentManager, TAG_DOUBLE_BUTTON);
    }

}
