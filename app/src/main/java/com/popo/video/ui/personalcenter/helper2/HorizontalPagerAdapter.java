package com.popo.video.ui.personalcenter.helper2;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.model.DictPayGift;
import com.popo.video.data.model.PicInfo;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;

import java.util.List;

/**
 * Created by xuzhaole on 2017/12/28.
 */

public class HorizontalPagerAdapter extends PagerAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<PicInfo> mList;

    public HorizontalPagerAdapter(final Context context, List<PicInfo> list) {
        mContext = context;
        this.mList = list;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = mLayoutInflater.inflate(R.layout.item_ask_gifts, container, false);
        ImageView iv_gift = (ImageView) view.findViewById(R.id.ask_for_gifts_iv_rose);
        TextView tv_price = (TextView) view.findViewById(R.id.tv_price);
        TextView tv_name = (TextView) view.findViewById(R.id.tv_name);

        DictPayGift giftBean = Util.getGiftBean(mList.get(position).getPicId());
        if (giftBean != null) {
            if (Util.getLacalLanguage().equals("Simplified")) {
                tv_name.setText(giftBean.getGiftName());
            } else if (Util.getLacalLanguage().equals("Traditional")) {
                tv_name.setText(giftBean.getTraditional());
            } else {
                tv_name.setText(giftBean.getEnglish());
            }
        }
        tv_price.setText(mList.get(position).getPicPrice() + mContext.getString(R.string.dialog_unit_ask_gift));
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(mList.get(position).getPicIcon())
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_gift).build());

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
