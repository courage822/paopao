package com.popo.video.ui.personalcenter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2017/7/11.
 * 用来从个人中心页面向提现界面传递账户余额的参数
 */

public class TvBalanceParcelable implements Parcelable {
    public int balance;
    public TvBalanceParcelable(int balance) {
        this.balance=balance;
    }
    public TvBalanceParcelable(Parcel in) {
        balance=in.readInt();
    }

    public static final Creator<TvBalanceParcelable> CREATOR = new Creator<TvBalanceParcelable>() {
        @Override
        public TvBalanceParcelable createFromParcel(Parcel in) {
            return new TvBalanceParcelable(in);
        }

        @Override
        public TvBalanceParcelable[] newArray(int size) {
            return new TvBalanceParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(balance);
    }
}
