package com.popo.video.ui.personalcenter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.R;
import com.popo.video.base.BaseTopBarActivity;
import com.popo.video.common.YeMeiPopopUtil;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.event.YeMeiMessageEvent;
import com.popo.video.parcelable.ListParcelable;
import com.popo.video.parcelable.TvPriceParcelable;
import com.popo.video.ui.personalcenter.contract.SetPriceConteact;
import com.popo.video.ui.personalcenter.presenter.SetPricePresenter;
import com.popo.video.ui.video.MassVideoOrVoiceActivity;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 价格设置页面
 * Created by zhangdroid on 2017/5/27.
 */
public class SetPriceActivity extends BaseTopBarActivity implements SetPriceConteact.IView, View.OnClickListener {
    @BindView(R.id.set_price_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.activity_set_price_ll_video_zhuanqian)
    LinearLayout ll_video_zhuanqian;
    @BindView(R.id.activity_set_price_ll_voice_zhuanqian)
    LinearLayout ll_voice_zhuanqian;
    @BindView(R.id.activity_set_price_ll_gifts_zhuanqian)
    LinearLayout ll_gifts_zhunaqian;
    @BindView(R.id.tv_audio_desc)
    TextView tv_audio_desc;
    @BindView(R.id.tv_video_desc)
    TextView tv_video_desc;
    private SetPricePresenter mSetPricePresenter;
    private TvPriceParcelable priceParcelable;
    private boolean videoCanClick = true;
    private boolean voiceCanClick = true;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_set_price;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.fate);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        priceParcelable = (TvPriceParcelable) parcelable;
    }

    @Override
    protected void initViews() {
        mSetPricePresenter = new SetPricePresenter(this);
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
        ll_video_zhuanqian.setOnClickListener(this);
        ll_voice_zhuanqian.setOnClickListener(this);
        ll_gifts_zhunaqian.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void getVideoTime() {
        videoCanClick = false;
    }

    @Override
    public void getVoiceTime() {
        voiceCanClick = false;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public Context obtainContext() {
        return null;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public String getVideoPrice() {
        return priceParcelable.price;
    }

    @Override
    public void finishActivity() {

    }

    @Override
    public String getAudioPrice() {
        return String.valueOf(priceParcelable.audioPrice);
    }

    @Override
    public void modificationPrice() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.set_price_rl_back:
                finish();
                break;
            case R.id.activity_set_price_ll_video_zhuanqian:
//                if (!TextUtils.isEmpty(priceParcelable.price) && priceParcelable.price.length() > 0) {
//                    LaunchHelper.getInstance().launch(mContext, InviteVedioOrVoiceActivity.class, new SendVideoOrVoiceParcelable(getString(R.string.active_invite_video), Integer.parseInt(priceParcelable.price), getString(R.string.obligate), 0));
//                }
                if (videoCanClick) {
                    String video_price = priceParcelable.price;
                    if (!TextUtils.isEmpty(video_price) && video_price.length() > 0) {
                        //群发视频
                        long currentTime1 = DataPreference.getCurrentTime();
                        if (currentTime1 == 0) {
                            //群发视频邀请
                            massVideoInvite();
                            DataPreference.saveCurrentTime(System.currentTimeMillis());
                        } else {
                            if (System.currentTimeMillis() - currentTime1 < 120 * 1000) {
                                Toast.makeText(SetPriceActivity.this, getString(R.string.sending_voice), Toast.LENGTH_SHORT).show();
                            } else {
                                //群发视频邀请
                                massVideoInvite();
                                DataPreference.saveCurrentTime(0);
                            }
                        }
                    }
                } else {
                    Toast.makeText(SetPriceActivity.this, "半小时内限制发一次，请稍后再试", Toast.LENGTH_SHORT).show();
                }


                break;
            case R.id.activity_set_price_ll_voice_zhuanqian:
//                if (!TextUtils.isEmpty(priceParcelable.price) && priceParcelable.price.length() > 0)
//                    LaunchHelper.getInstance().launch(mContext, InviteVedioOrVoiceActivity.class, new SendVideoOrVoiceParcelable(getString(R.string.invite_voice), priceParcelable.audioPrice, getString(R.string.obligate), 1));
                if (voiceCanClick) {
                    String voice_price = String.valueOf(priceParcelable.audioPrice);
                    if (!TextUtils.isEmpty(voice_price) && voice_price.length() > 0) {
                        //群发语音
                        long currentTime2 = DataPreference.getCurrentTime();
                        if (currentTime2 == 0) {
                            //群发语音邀请
                            massVoiceInvite();
                            DataPreference.saveCurrentTime(System.currentTimeMillis());
                        } else {
                            if (System.currentTimeMillis() - currentTime2 < 120 * 1000) {
                                Toast.makeText(SetPriceActivity.this, getString(R.string.sending_voice), Toast.LENGTH_SHORT).show();
                            } else {
                                //群发语音邀请
                                massVoiceInvite();
                                DataPreference.saveCurrentTime(0);
                            }
                        }
                    }
                } else {
                    Toast.makeText(SetPriceActivity.this, "半小时内限制发一次，请稍后再试", Toast.LENGTH_SHORT).show();
                }


                break;
            case R.id.activity_set_price_ll_gifts_zhuanqian:
                LaunchHelper.getInstance().launch(mContext, AskGiftsActivity.class);
                break;
        }
    }

    //群发语音邀请
    private void massVoiceInvite() {
        mSetPricePresenter.startInvite("2");
        DataPreference.saveTime(0);
        //ListParcelable是用来回调fragment,数据类型是一样的这里就用这个吧
        LaunchHelper.getInstance().launch(this, MassVideoOrVoiceActivity.class, new ListParcelable(2));
    }

    //群发视频邀请
    private void massVideoInvite() {
        mSetPricePresenter.startInvite("1");
        DataPreference.saveTime(0);
        //ListParcelable是用来回调fragment,数据类型是一样的这里就用这个吧
        LaunchHelper.getInstance().launch(this, MassVideoOrVoiceActivity.class, new ListParcelable(1));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSetPricePresenter.getVideoCountDown();
        mSetPricePresenter.getVoiceCountDown();
    }
    @Subscribe
    public void onEvent(YeMeiMessageEvent evet){
        Message msg=new Message();
        msg.obj=evet;
        yeMeiHandler.sendMessage(msg);

    }
    private Handler yeMeiHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            YeMeiMessageEvent obj = (YeMeiMessageEvent) msg.obj;
            if(obj!=null){
                YeMeiPopopUtil.getInstance().showYeMei(SetPriceActivity.this,rl_back,obj,isFinishing());
            }
        }
    };

}
