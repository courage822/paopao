package com.popo.video.ui.register.contract;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;
import com.popo.video.ui.register.adapter.LocationAdapter;

/**
 * Created by zhangdroid on 2017/5/26.
 */
public interface LocationContract {

    interface IView extends BaseView {

        void showLoading();

        void dismissLoading();

        void toggleShowEmpty(boolean toggle, String msg);

        void toggleShowError(boolean toggle, String msg);

        LocationAdapter getLocationAdapter();


    }

    interface IPresenter extends BasePresenter {
        /**
         * 获取地理位置
         *
         * @return location
         */
        void getLocation();

        void register();
    }
}
