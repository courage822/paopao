package com.popo.video.ui.personalcenter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.base.BaseTopBarActivity;
import com.popo.video.common.BitmapUtils;
import com.popo.video.common.RecordUtil;
import com.popo.video.common.Util;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.PicInfo;
import com.popo.video.db.DbModle;
import com.popo.library.net.NetUtil;
import com.popo.library.util.FileUtil;
import com.popo.library.util.ToastUtil;
import com.popo.library.util.Utils;
import com.popo.video.ui.personalcenter.helper2.HorizontalInfiniteCycleViewPager;
import com.popo.video.ui.personalcenter.helper2.HorizontalPagerAdapter;
import com.popo.video.ui.photo.GetPhotoActivity;
import com.popo.video.ui.photo.GetVideoActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/11/7.
 */

public class AskGiftsActivity extends BaseTopBarActivity implements View.OnClickListener {


    @BindView(R.id.activity_ask_gifts_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.ask_for_gifts_chat_switch)
    ImageView iv_switch;
    @BindView(R.id.iv_sound_voice)
    ImageView iv_sound_voice;
    @BindView(R.id.tv_time_voice)
    TextView tv_time_voice;
    @BindView(R.id.ll_record_voice)
    LinearLayout ll_record;
    @BindView(R.id.ask_for_gifts_chat_voice)
    Button btn_record;
    @BindView(R.id.ask_for_gifts_text_input)
    EditText et_msg;
    @BindView(R.id.tv_number)
    TextView tv_number;
    @BindView(R.id.tv_add_text)
    TextView tv_add_text;
    @BindView(R.id.framelayout_et)
    FrameLayout framelayout_et;
    @BindView(R.id.send_private_photo_iv_add_photo)
    ImageView iv_add;
    @BindView(R.id.rl_add)
    RelativeLayout rl_add;
    @BindView(R.id.ll_root)
    LinearLayout ll_root;
    @BindView(R.id.send_private_photo_btn_sure)
    Button btn_sure;
    List<PicInfo> allPic;
    @BindView(R.id.hicvp)
    HorizontalInfiniteCycleViewPager hic_vp;

    private static AnimationDrawable animationDrawable;
    private static AnimationDrawable soundanimationDrawable;
    // 录音文件存放目录
    private String mRecordDirectory;
    // 录音文件路径
    private String mRecordOutputPath;
    // 是否正在录音
    private boolean mIsRecording;
    // 录音是否被取消
    private boolean mIsRecordCanceled;
    // 录音时长（毫秒）
    private long mRecordDuration = 0;
    // 录音计时刷新间隔（毫秒）
    private final long mRefreshInterval = 250;
    PopupWindow mRecordPopupWindow;

    private File mFile;
    private File mBitmapFile;
    private String mVideoDuration;
    // 异步任务
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == C.message.MSG_TYPE_VOICE_UI_TIME) {
                mRecordDuration++;
                mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_VOICE_UI_TIME, 1_000);
            } else {
                handleAsyncTask(msg);
            }
        }
    };

    private void handleAsyncTask(Message msg) {
        switch (msg.what) {
            case C.message.MSG_TYPE_TIMER:// 计时
                if (mIsRecording) {
                    mRecordDuration += mRefreshInterval;
                    mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_TIMER, mRefreshInterval);
                }
                break;
            case C.message.MSG_TYPE_DELAYED:// 延时结束录音
                RecordUtil.getInstance().stopRecord();
                mRecordDuration = 0;
                break;
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_ask_for_gifts;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return null;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        allPic = DbModle.getInstance().getUserAccountDao().getAllPic();
        HorizontalPagerAdapter adapter = new HorizontalPagerAdapter(mContext, allPic);
        hic_vp.setAdapter(adapter);
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
        btn_sure.setOnClickListener(this);
        iv_switch.setOnClickListener(this);
        rl_add.setOnClickListener(this);
        ll_record.setOnClickListener(this);
        et_msg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                tv_number.setText(charSequence.length() + "/40");
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        btn_record.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:// 按下时开始录音
                        btn_record.setText(R.string.chat_loose_to_end);
                        showRecordPopupWindow(C.message.STATE_RECORDING);
                        startRecord();
                        mIsRecordCanceled = false;
                        // 开始计时
                        mRecordDuration = 0;
                        // 播放动画
                        if (animationDrawable != null) {
                            animationDrawable.start();
                        }
                        mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_VOICE_UI_TIME, 1000);
                        break;

                    case MotionEvent.ACTION_MOVE:// 判断是否上滑取消
                        if (animationDrawable != null) {
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        handleTouchEventMove(event, ll_root);
                        break;

                    case MotionEvent.ACTION_UP:// 松开时停止录音并发送
                        btn_record.setText(getString(R.string.chat_press_to_speak));
                        showRecordPopupWindow(C.message.STATE_IDLE);
                        if (null != mRecordPopupWindow && mRecordPopupWindow.isShowing()) {
                            mRecordPopupWindow.dismiss();
                        }
                        if (mIsRecordCanceled) {
                            cancelRecord();
                        } else {
                            stopRecordAndSend();
                        }
                        if (animationDrawable != null) {
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        // 结束计时
                        mHandler.removeMessages(C.message.MSG_TYPE_VOICE_UI_TIME);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    protected void loadData() {
        mRecordDirectory = FileUtil.getExternalFilesDir(mContext, Environment.DIRECTORY_MUSIC) + File.separator;
    }


    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_ask_gifts_rl_back://返回
                finish();
                break;
            case R.id.send_private_photo_btn_sure://发送
                String picId = allPic.get(hic_vp.getRealItem()).getPicId();

                final ProgressDialog progressDialog = new ProgressDialog(mContext);
                progressDialog.setMessage(getString(R.string.upload_photo_wait));
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();

                if (mFile != null) {//视频/图片
                    if (!TextUtils.isEmpty(mVideoDuration)) {
                        if (!TextUtils.isEmpty(mRecordOutputPath) && new File(mRecordOutputPath).exists()) {//语音+视频
                            ApiManager.sendVoiceVideo(picId, String.valueOf(mRecordDuration / 1000), new File(mRecordOutputPath), mVideoDuration, mFile, mBitmapFile, new IGetDataListener<String>() {
                                @Override
                                public void onResult(String s, boolean isEmpty) {
                                    progressDialog.dismiss();
                                    ToastUtil.showShortToast(mContext, getString(R.string.dialog_success_ask_gift));
                                    finish();
                                }

                                @Override
                                public void onError(String msg, boolean isNetworkError) {
                                    progressDialog.dismiss();
                                    ToastUtil.showShortToast(mContext, getString(R.string.dialog_success_ask_gift));
                                    finish();
                                }
                            });
                        } else {//视频 + 文字（可为空）
                            String content = Util.encodeHeadInfo(et_msg.getText().toString().trim());
                            ApiManager.sendTextVideo(picId, content, mVideoDuration, mFile, mBitmapFile, new IGetDataListener<String>() {
                                @Override
                                public void onResult(String s, boolean isEmpty) {
                                    progressDialog.dismiss();
                                    ToastUtil.showShortToast(mContext, getString(R.string.dialog_success_ask_gift));
                                    finish();
                                }

                                @Override
                                public void onError(String msg, boolean isNetworkError) {
                                    progressDialog.dismiss();
                                    ToastUtil.showShortToast(mContext, getString(R.string.dialog_success_ask_gift));
                                    finish();
                                }
                            });
                        }
                    } else {//图片
                        if (!TextUtils.isEmpty(mRecordOutputPath) && new File(mRecordOutputPath).exists()) {//语音+图片
                            ApiManager.sendVoicePicture(picId, String.valueOf(mRecordDuration / 1000), new File(mRecordOutputPath), mFile, new IGetDataListener<String>() {
                                @Override
                                public void onResult(String s, boolean isEmpty) {
                                    progressDialog.dismiss();
                                    ToastUtil.showShortToast(mContext, getString(R.string.dialog_success_ask_gift));
                                    finish();
                                }

                                @Override
                                public void onError(String msg, boolean isNetworkError) {
                                    progressDialog.dismiss();
                                    ToastUtil.showShortToast(mContext, getString(R.string.dialog_success_ask_gift));
                                    finish();
                                }
                            });
                        } else {//图片 + 文字（可为空）
                            String content = Util.encodeHeadInfo(et_msg.getText().toString().trim());
                            ApiManager.sendTextPicture(picId, content, mFile, new IGetDataListener<String>() {
                                @Override
                                public void onResult(String s, boolean isEmpty) {
                                    progressDialog.dismiss();
                                    ToastUtil.showShortToast(mContext, getString(R.string.dialog_success_ask_gift));
                                    finish();
                                }

                                @Override
                                public void onError(String msg, boolean isNetworkError) {
                                    progressDialog.dismiss();
                                    ToastUtil.showShortToast(mContext, getString(R.string.dialog_success_ask_gift));
                                    finish();
                                }
                            });
                        }
                    }
                } else {
                    if (!TextUtils.isEmpty(et_msg.getText().toString().trim())) {//文字
                        String content = Util.encodeHeadInfo(et_msg.getText().toString().trim());
                        ApiManager.sendText(picId, content, new IGetDataListener<String>() {
                            @Override
                            public void onResult(String s, boolean isEmpty) {
                                progressDialog.dismiss();
                                ToastUtil.showShortToast(mContext, getString(R.string.dialog_success_ask_gift));
                                finish();
                            }

                            @Override
                            public void onError(String msg, boolean isNetworkError) {
                                progressDialog.dismiss();
                                ToastUtil.showShortToast(mContext, getString(R.string.dialog_success_ask_gift));
                                finish();
                            }
                        });
                    } else if (!TextUtils.isEmpty(mRecordOutputPath) && new File(mRecordOutputPath).exists()) {//语音
                        ApiManager.sendVoice(picId, String.valueOf(mRecordDuration / 1000), new File(mRecordOutputPath), new IGetDataListener<String>() {
                            @Override
                            public void onResult(String s, boolean isEmpty) {
                                progressDialog.dismiss();
                                ToastUtil.showShortToast(mContext, getString(R.string.dialog_success_ask_gift));
                                finish();
                            }

                            @Override
                            public void onError(String msg, boolean isNetworkError) {
                                ToastUtil.showShortToast(mContext, getString(R.string.dialog_success_ask_gift));
                                finish();
                            }
                        });
                    } else {//什么都没有
                        progressDialog.dismiss();
                        ToastUtil.showShortToast(mContext, getString(R.string.dialog_hint_ask_gift));
                    }
                }
                break;
            case R.id.rl_add://添加视频或照片
                addDialog();
                break;
            case R.id.ask_for_gifts_chat_switch://语音文字切换
                iv_switch.setSelected(!iv_switch.isSelected());
                if (iv_switch.isSelected()) {
                    btn_record.setVisibility(View.VISIBLE);
                    btn_record.setText(getString(R.string.chat_press_to_speak));
                    framelayout_et.setVisibility(View.GONE);
                    et_msg.setEnabled(false);
                    et_msg.setText("");
                } else {
                    framelayout_et.setVisibility(View.VISIBLE);
                    et_msg.setEnabled(true);
                    btn_record.setVisibility(View.GONE);
                    ll_record.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(mRecordOutputPath)) {
                        File file = new File(mRecordOutputPath);
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                }
                break;
            case R.id.ll_record_voice:
                startVoiceAnimation(iv_sound_voice, this);
                // 播放动画
                if (soundanimationDrawable != null) {
                    soundanimationDrawable.start();
                }
                RecordUtil.getInstance().play(mRecordOutputPath, new RecordUtil.OnPlayerListener() {
                    @Override
                    public void onCompleted() {//播放完成，停止动画
                        soundanimationDrawable.stop();
                    }

                    @Override
                    public void onPaused() {
                        soundanimationDrawable.stop();
                    }
                });
                break;
        }
    }

    private void addDialog() {
        final Dialog bottomDialog = new Dialog(mContext, R.style.BottomDialog);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_ask_gifts, null);
        bottomDialog.setContentView(contentView);
        TextView pick_image = (TextView) contentView.findViewById(R.id.pick_image);
        TextView pick_video = (TextView) contentView.findViewById(R.id.pick_video);

        pick_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialog.dismiss();
                GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        if (null != file) {
                            try {
                                tv_add_text.setVisibility(View.GONE);
                                String canonicalPath = file.getCanonicalPath();
                                Bitmap bitmap = BitmapFactory.decodeFile(canonicalPath, null);
                                iv_add.setImageBitmap(bitmap);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            mFile = file;
                            mVideoDuration = "";
                        }
                    }
                });
            }
        });

        pick_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialog.dismiss();
                GetVideoActivity.toGetVideoActivity(mContext, new GetVideoActivity.OnGetVideoListener() {
                    @Override
                    public void getSelectedVideo(String filePath, String duration, String bitmapFilePath) {
                        if (null != filePath) {
                            if (TextUtils.isEmpty(bitmapFilePath)) {
                                final Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Video.Thumbnails.MINI_KIND);
                                new AsyncTask<Void, Void, File>() {

                                    @Override
                                    protected File doInBackground(Void... voids) {
                                        return BitmapUtils.compressImage(mContext, bitmap);
                                    }

                                    @Override
                                    protected void onPostExecute(File file) {
                                        Glide.with(mContext).load(file).error(Util.getDefaultImage())
                                                .placeholder(Util.getDefaultImage()).into(iv_add);
                                        mBitmapFile = file;
                                    }
                                }.execute();
                            } else {
                                Glide.with(mContext).load(new File(bitmapFilePath)).error(Util.getDefaultImage())
                                        .placeholder(Util.getDefaultImage()).into(iv_add);
                                mBitmapFile = new File(bitmapFilePath);
                            }
                            mFile = new File(filePath);
                            mVideoDuration = duration;

                        }
                    }
                });
            }
        });
        dialogShow(mContext, bottomDialog, contentView, Gravity.CENTER);
    }

    /**
     * 播放录音动画
     *
     * @param iv_sound_voice
     * @param context
     */
    private void startVoiceAnimation(ImageView iv_sound_voice, Context context) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_left1));
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_left2));
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_left3));
        soundanimationDrawable = Utils.getFrameAnim(drawableList, true, 150);
        iv_sound_voice.setImageDrawable(soundanimationDrawable);
    }

    private void startRecord() {
        // 录音开始前震动一下
        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(88);
        // 根据系统时间生成文件名
        String recordFileName = FileUtil.createFileNameByTime() + ".aac";
        // 录音文件临时保存路径：data下包名music目录
        mRecordOutputPath = mRecordDirectory + recordFileName;
        // 开始录音
        RecordUtil.getInstance().startRecord(mRecordOutputPath);
        mIsRecording = true;
        mRecordDuration = 0;
        // 计时，间隔250毫秒刷新
        mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_TIMER, mRefreshInterval);
    }

    private void cancelRecord() {
        if (mIsRecordCanceled) {
            RecordUtil.getInstance().stopRecord();
            mIsRecordCanceled = false;
            FileUtil.deleteFile(mRecordOutputPath);
        }
    }

    private void stopRecordAndSend() {
        if (mIsRecording) {
            mIsRecording = false;
            if (mRecordDuration < 2000 || mRecordDuration > 60000) {// 录音时长小于1秒的不发送
                // 删除小于2秒的文件
                RecordUtil.getInstance().stopRecord();
                FileUtil.deleteFile(mRecordOutputPath);
                ToastUtil.showShortToast(this, getString(R.string.voice_limit));
            } else {// 发送语音
                RecordUtil.getInstance().stopRecord();
                btn_record.setVisibility(View.GONE);
                ll_record.setVisibility(View.VISIBLE);
                tv_time_voice.setText(mRecordDuration / 1000 + getString(R.string.second));
            }
        }
    }

    private void handleTouchEventMove(MotionEvent event, LinearLayout ll_dialog) {
        if (event.getY() < -100) { // 上滑取消发送
            mIsRecordCanceled = true;
            showRecordPopupWindow(C.message.STATE_CANCELED);
        } else {
            mIsRecordCanceled = false;
            showRecordPopupWindow(C.message.STATE_RECORDING);
        }
    }

    public void showRecordPopupWindow(int state) {
        if (null == mRecordPopupWindow) {
            View contentView = LayoutInflater.from(mContext).inflate(R.layout.popup_chat_record, null);
            mRecordPopupWindow = new PopupWindow(contentView, WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT, true);
            // 设置背景透明
            mRecordPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            if (null != mRecordPopupWindow) {
                ImageView iv_animation = (ImageView) mRecordPopupWindow.getContentView().findViewById(R.id.popup_record_iv_duration);
                startAnimation(iv_animation);
            }
        }

        View view = mRecordPopupWindow.getContentView();
        if (null != view) {
            // 录音中
            LinearLayout llRecording = (LinearLayout) view.findViewById(R.id.popup_recording_container);
            // 上滑取消
            LinearLayout ll_cancle = (LinearLayout) view.findViewById(R.id.popup_recording_container_cancle);
            switch (state) {
                case C.message.STATE_RECORDING: // 正在录音
                    llRecording.setVisibility(View.VISIBLE);
                    ll_cancle.setVisibility(View.GONE);
                    break;

                case C.message.STATE_CANCELED: // 取消录音
                    llRecording.setVisibility(View.GONE);
                    ll_cancle.setVisibility(View.VISIBLE);
                    break;

                case C.message.STATE_IDLE:// 录音结束
                    llRecording.setVisibility(View.GONE);
                    ll_cancle.setVisibility(View.GONE);
                    break;
            }
        }
        // 居中显示
        mRecordPopupWindow.showAtLocation(ll_root, Gravity.CENTER, 0, 0);
    }

    public void dismissPopupWindow() {
        if (null != mRecordPopupWindow && mRecordPopupWindow.isShowing()) {
            mRecordPopupWindow.dismiss();
        }
    }

    private void startAnimation(ImageView iv_animation) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_1));
        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_2));
        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_3));
        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_4));
        animationDrawable = Utils.getFrameAnim(drawableList, true, 150);
        iv_animation.setImageDrawable(animationDrawable);
    }


    //对话框初始化显示的方法
    private static void dialogShow(Context context, Dialog bottomDialog, View contentView, int location) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = context.getResources().getDisplayMetrics().widthPixels - dp2px(context, 16f);
        params.bottomMargin = dp2px(context, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.getWindow().setGravity(location);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal,
                context.getResources().getDisplayMetrics());
    }
}
