package com.popo.video.ui.detail.banner.loader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.popo.video.R;


public abstract class ImageLoader implements ImageLoaderInterface<View> {

    @Override
    public View createImageView(Context context) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.item_banner, null);
        return inflate;
    }

}
