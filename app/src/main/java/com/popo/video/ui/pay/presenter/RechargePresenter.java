package com.popo.video.ui.pay.presenter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.HostInfo;
import com.popo.video.data.model.MyInfo;
import com.popo.video.data.model.UserDetail;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.ui.homepage.HomepageAdapter;
import com.popo.video.ui.pay.contract.RechargeContract;
import com.popo.video.ui.pay.fragment.DredgeVipFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public class RechargePresenter implements RechargeContract.IPresenter {
    private static final String TAG = RechargePresenter.class.getSimpleName();
    private RechargeContract.IView mPayView;
    private Context mContext;

    public RechargePresenter(RechargeContract.IView view) {
        this.mPayView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }


    @Override
    public void getPayWay(String fromTag) {

    }

    @Override
    public void addTabs() {
        HomepageAdapter homepageAdapter = new HomepageAdapter(mPayView.getManager());
        List<Fragment> fragmentList = new ArrayList<>();
        List<String> tabList = Arrays.asList(mContext.getString(R.string.open_vip), mContext.getString(R.string.buy_keys));
        fragmentList.add(new DredgeVipFragment());
        homepageAdapter.setData(fragmentList, tabList);
        mPayView.setAdapter(homepageAdapter);
    }

    @Override
    public void getMyInfo() {
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    if (!TextUtils.isEmpty(myInfo.getIslj())){
                        UserPreference.setIslj(myInfo.getIslj());
                    }
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        HostInfo hostInfo = userDetail.getHostInfo();
                        if (hostInfo != null) {
                            mPayView.getNowMoney(String.valueOf(hostInfo.getBalance()));
                        }
                        String vipDays = userDetail.getVipDays();
                        if (!TextUtils.isEmpty(vipDays)) {
                            int i = Integer.parseInt(vipDays);
                            UserPreference.setIsVip(i);
                        } else {
                            UserPreference.setIsVip(false);
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

}
