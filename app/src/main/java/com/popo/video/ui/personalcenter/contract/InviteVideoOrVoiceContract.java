package com.popo.video.ui.personalcenter.contract;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

/**
 * Created by Administrator on 2017/6/16.
 */

public interface InviteVideoOrVoiceContract {
    interface IPresenter extends BasePresenter {
        /**
         * 开始呼叫
         */
        void startInvite(String type);
        /**
         * 获取倒计时
         */
        void getCountDown(String type);
    }
    interface IView extends BaseView {
        /**
         * 获取显示的倒计时
         */
        void getButtonCountDown(long time);
        /**
         * 获取发送的人数
         */
        void getSendNum(int num);
        /**
         * 获取当前主播的价格
         */
        void getAnchorPrice(String price);
    }
}
