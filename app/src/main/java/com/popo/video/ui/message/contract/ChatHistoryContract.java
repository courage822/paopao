package com.popo.video.ui.message.contract;

import com.popo.video.data.model.HuanXinUser;
import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;
import com.popo.video.ui.match.MatchAdapter;
import com.popo.video.ui.message.adapter.ChatHistoryAdapter;
import com.popo.video.ui.message.adapter.NewRecyclerAdapter;

import java.util.List;

/**
 * Created by zhangdroid on 2017/7/6.
 */
public interface ChatHistoryContract {

    interface IView extends BaseView {

        void toggleShowEmpty(boolean toggle, String msg);

        void toggleShowError(boolean toggle, String msg);

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        /**
         * 显示加载更多
         */
        void showLoadMore();

        /**
         * 隐藏加载更多
         */
        void hideLoadMore();

        /**
         * 显示没有更多（滑动到最底部）
         */
        void showNoMore();

        ChatHistoryAdapter getChatHistoryAdapter();
        /**
         * 设置Adapter
         *
         */
        void setAdapter(NewRecyclerAdapter adapter, MatchAdapter matchAdapter);

        void hideMatch(boolean hide);
    }

    interface IPresenter extends BasePresenter {

        void loadConversationList();

        void refresh();

        void loadMore();
        void initloadData();
        List<HuanXinUser> getAllAcount();
    }

}
