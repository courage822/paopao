package com.popo.video.ui.dialog.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.widget.RelativeLayout;

import com.popo.video.R;
import com.popo.video.data.model.PayDict;
import com.popo.video.data.preference.UserPreference;
import com.popo.library.adapter.CommonRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;

import java.util.List;

/**
 * Created by WangYong on 2017/9/15.
 */

public class DialogKeyAndDiamondsAdapter extends CommonRecyclerViewAdapter<PayDict> {
    // 当前选中的Item索引
    private int mSelectedPosition;
    private boolean isDiamonds=false;
    public void setIsDiamonds(){
        this.isDiamonds=true;
    }
    public DialogKeyAndDiamondsAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public DialogKeyAndDiamondsAdapter(Context context, int layoutResId, List<PayDict> dataList) {
        super(context, layoutResId, dataList);
    }

    @Override
    public void convert(PayDict payDict, int position, RecyclerViewHolder holder) {
        if(isDiamonds){
            holder.setText(R.id.dialog_purchase_diamonds_item_tv_diamonds, payDict.getServiceName());
            RelativeLayout rl_diamonds_bg = (RelativeLayout) holder.getView(R.id.dialog_purchase_diamonds_item_rl_bg);
            rl_diamonds_bg.setBackgroundResource(R.drawable.purchase_diamond_un_selected);
            if(mSelectedPosition==position){
                rl_diamonds_bg.setBackgroundResource(R.drawable.purchase_diamond_selected);
            }
            switch (UserPreference.getCountryId()) {
                case "97"://美国
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("$", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "16"://澳大利亚
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("AU$", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "67"://加拿大
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("CA$", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "174"://香港
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("HK$", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "161"://印度
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("IDR", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "162"://印度尼西亚
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("IDR", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "10"://爱尔兰
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("€", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "152"://新西兰
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("NZ$", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "20"://巴基斯坦
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("PKR", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "46"://菲律宾
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("PHP", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "150"://新加坡
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("S$", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "108"://南非
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("ZAR", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "163"://英国
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("￡", String.valueOf(payDict.getPrice())).toString());
                    break;
                case "173"://台湾
                    holder.setText(R.id.dialog_purchase_diamonds_item_tv_money, TextUtils.concat("NT$", String.valueOf(payDict.getPrice())).toString());
                    break;
//            case "171"://中国
//                break;
            }
        }
    }
    public void setSelectedPosition(int selectedPosition) {
        this.mSelectedPosition = selectedPosition;
    }
}
