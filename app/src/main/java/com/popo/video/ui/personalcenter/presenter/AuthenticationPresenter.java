package com.popo.video.ui.personalcenter.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.popo.library.util.LaunchHelper;
import com.popo.video.R;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.parcelable.VideoShowParcelable;
import com.popo.video.ui.personalcenter.VideoShowActivity;
import com.popo.video.ui.personalcenter.contract.AuthenticationContract;

import java.io.File;

/**
 * Created by Administrator on 2017/6/16.
 */

public class AuthenticationPresenter implements AuthenticationContract.IPresenter{
    private AuthenticationContract.IView mAuthenticationIview;
    private Context mContext;
    private File videoFile;

    public AuthenticationPresenter(AuthenticationContract.IView mAuthenticationIview) {
        this.mAuthenticationIview = mAuthenticationIview;
        this.mContext = mAuthenticationIview.obtainContext();
    }

    @Override
    public void start() {

    }


    @Override
    public void goVideoRecordPage() {

    }

    @Override
    public void getUserInfo() {
    }


    @Override
    public void startRecord() {


    }

    @Override
    public void startPlay() {

    }

    @Override
    public void reRecord() {

    }

    @Override
    public void getFaceBookAccount() {
//        String middleImage = UserPreference.getMiddleImage();
//        if(!TextUtils.isEmpty(middleImage)){
//            mAuthenticationIview.getAvatarUrl(middleImage);
//        }
        mAuthenticationIview.setFaceBookAccount(DataPreference.getfacebookaccount());
    }

    @Override
    public void submitCheck(String videoNumber, File videoFile, File avatarFile) {
        if (!TextUtils.isEmpty(videoNumber) && videoFile.exists() && avatarFile.exists()) {
            LaunchHelper.getInstance().launchFinish(mContext, VideoShowActivity.class,
                    new VideoShowParcelable(videoNumber, videoFile.getAbsolutePath(), avatarFile.getAbsolutePath()));
        } else {
            mAuthenticationIview.showTip(mContext.getString(R.string.upload_video_first));
        }
    }
}
