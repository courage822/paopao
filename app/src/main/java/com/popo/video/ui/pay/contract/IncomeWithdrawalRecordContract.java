package com.popo.video.ui.pay.contract;

import android.support.v4.app.FragmentManager;

import com.popo.library.adapter.wrapper.OnLoadMoreListener;
import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;
import com.popo.video.ui.personalcenter.adapter.IncomeRecordAdapter;
import com.popo.video.ui.personalcenter.adapter.WithdrawRecordAdapter;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public interface IncomeWithdrawalRecordContract {


    interface IView extends BaseView {

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        void toggleShowError(boolean toggle, String msg);

        void toggleShowEmpty(boolean toggle, String msg);

        /**
         * @return 获得FragmentManager，用于显示对话框
         */
        FragmentManager obtainFragmentManager();




        /**
         * 显示加载更多
         */
        void showLoadMore();

        /**
         * 隐藏加载更多
         */
        void hideLoadMore();

        /**
         * 显示没有更多（滑动到最底部）
         */
        void showNoMore();
        WithdrawRecordAdapter getWithdrawRecordAdapter();
        IncomeRecordAdapter getIncomeRecordAdapter();
        void setAdapter(IncomeRecordAdapter mIncomeRecordAdapter);
        void setAdapter(WithdrawRecordAdapter mWithdrawRecordAdapter);
        /**
         * 设置加载更多监听器
         *
         * @param onLoadMoreListener
         */
        void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener);
    }

    interface IPresenter extends BasePresenter {

        /**
         * 收入记录
         *
         *
         */
        void incomeHistory();
        /**
         * 提现记录
         */
        void withdrawalHistroy();

        /**
         * 下拉刷新
         */
        void refresh1();
        /**
         * 下拉刷新
         */
        void refresh2();
        /**
         * jiazaigenduo
         */
        void loadMore1();
        /**
         * jiazaigengduo
         */
        void loadMore2();

    }

}
