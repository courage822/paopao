package com.popo.video.ui.pay.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.data.preference.UserPreference;
import com.popo.library.net.NetUtil;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.parcelable.PayInfoParcelable;
import com.popo.video.ui.pay.adapter.PayAdapter;
import com.popo.video.ui.pay.contract.PayContract;
import com.popo.video.ui.pay.presenter.PayPresenter;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/8/30.
 */

public class PayActivity extends BaseFragmentActivity implements View.OnClickListener, PayContract.IView {
    @BindView(R.id.activity_pay_root)
    LinearLayout rlRoot;
    @BindView(R.id.pay_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.activity_pay_tv_service_name)
    TextView tv_service_name;
    @BindView(R.id.activity_pay_tv_service_price)
    TextView tv_service_price;
    @BindView(R.id.pay_activity_btn_sure_pay)
    Button btn_sure;
    @BindView(R.id.activity_pay_tv_service_icon)
    TextView tv_service_icon;
    @BindView(R.id.pay_activity_tv_service_id)
    TextView tv_service_id;
    @BindView(R.id.pay_activity_tv_service_type)
    TextView tv_service_type;
    private PayInfoParcelable mPayInfoParcelabe;
    private PayPresenter mPayPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_pay;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mPayInfoParcelabe = (PayInfoParcelable) parcelable;
        if (mPayInfoParcelabe != null) {
            if (mPayInfoParcelabe.type == 1) {   // 1钻石
                tv_service_name.setText(":" + mPayInfoParcelabe.serviceName);
                tv_service_icon.setBackgroundResource(R.drawable.f1_diamonds_ico);
                tv_service_type.setText("2");//钻石
            } else if (mPayInfoParcelabe.type == 2) {// 2 vip
                tv_service_name.setText(":" + mPayInfoParcelabe.serviceName);
                tv_service_icon.setText("VIP");
                tv_service_type.setText("1");//1包月
            } else if (mPayInfoParcelabe.type == 3) { // 3 钥匙
                tv_service_name.setText(":" + mPayInfoParcelabe.serviceName);
                tv_service_icon.setBackgroundResource(R.drawable.pay_info_key_ic);
                tv_service_type.setText("3");//钥匙
            }
            tv_service_id.setText(mPayInfoParcelabe.serviceId);
            switch (UserPreference.getCountryId()) {
                case "97"://美国
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "$" + mPayInfoParcelabe.price);
                    break;
                case "16"://澳大利亚
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "AU$" + mPayInfoParcelabe.price);
                    break;
                case "67"://加拿大
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "CA$" + mPayInfoParcelabe.price);
                    break;
                case "174"://香港
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "HK$" + mPayInfoParcelabe.price);
                    break;
                case "161"://印度
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "INR" + mPayInfoParcelabe.price);
                    break;
                case "162"://印度尼西亚
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "IDR" + mPayInfoParcelabe.price);
                    break;
                case "10"://爱尔兰
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "€" + mPayInfoParcelabe.price);
                    break;
                case "152"://新西兰
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "NZ$" + mPayInfoParcelabe.price);
                    break;
                case "20"://巴基斯坦
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "PKR" + mPayInfoParcelabe.price);
                    break;
                case "46"://菲律宾
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "PHP" + mPayInfoParcelabe.price);
                    break;
                case "150"://新加坡
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "S$" + mPayInfoParcelabe.price);
                    break;
                case "108"://南非
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "ZAR" + mPayInfoParcelabe.price);
                    break;
                case "163"://英国
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "￡" + mPayInfoParcelabe.price);
                    break;
                case "173"://台湾
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "NT$" + mPayInfoParcelabe.price);
                    break;
                default:
                    tv_service_price.setText(PayActivity.this.getString(R.string.pay_need) + "$" + mPayInfoParcelabe.price);
                    break;
//            case "171"://中国
//
//                break;
            }
        }
    }

    @Override
    protected View getNoticeView() {
        return rlRoot;
    }

    @Override
    protected void initViews() {
        mPayPresenter = new PayPresenter(this);
        mPayPresenter.start();
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
        btn_sure.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_activity_btn_sure_pay:
                String serviceId = tv_service_id.getText().toString();
                if (!TextUtils.isEmpty(serviceId)) {
                    Log.e("AAAAAAA", "onClick:+++++++++++++++++++++++++++++++++++++++++++++ "+mPayInfoParcelabe.type +"=================="+mPayInfoParcelabe.pos);
                    if (mPayInfoParcelabe.type == 1) {//钻石
                        switch (mPayInfoParcelabe.pos) {
                            //钻石区
                            case 5://10000钻石
                                mPayPresenter.purchase(C.pay.SKU9);
                                break;
                            case 4://5000钻石
                                mPayPresenter.purchase(C.pay.SKU10);
                                break;
                            case 3://3000钻石
                                mPayPresenter.purchase(C.pay.SKU11);
                                break;
                            case 2://1000钻石
                                mPayPresenter.purchase(C.pay.SKU12);
                                break;
                            case 1://600钻石
                                mPayPresenter.purchase(C.pay.SKU13);
                                break;
                            case 0://100钻石
                                mPayPresenter.purchase(C.pay.SKU14);
                                break;
                        }
                    } else if (mPayInfoParcelabe.type == 2) {//VIP
                        switch (mPayInfoParcelabe.pos) {
                            //VIP区
                            case 0://VIP30天
                                mPayPresenter.purchase(C.pay.SKU8);
                                break;
                            case 1://VIP90天
                                mPayPresenter.purchase(C.pay.SKU7);
                                break;
                        }
                    } else if (mPayInfoParcelabe.type == 3) {//钥匙
                        switch (mPayInfoParcelabe.pos) {
                            //钥匙区
                            case 5://1000钥匙
                                mPayPresenter.purchase(C.pay.SKU1);
                                break;
                            case 4://500钥匙
                                mPayPresenter.purchase(C.pay.SKU2);
                                break;
                            case 3://150钥匙
                                mPayPresenter.purchase(C.pay.SKU3);
                                break;
                            case 2://150钥匙
                                mPayPresenter.purchase(C.pay.SKU4);
                                break;
                            case 1://30钥匙
                                mPayPresenter.purchase(C.pay.SKU5);
                                break;
                            case 0://10钥匙
                                mPayPresenter.purchase(C.pay.SKU6);
                                break;
                        }
                    }
                }
                break;
            case R.id.pay_activity_rl_back:
                finish();
                break;
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(rlRoot, msg);
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void dismissLoading() {
        toggleShowLoading(false, null);
    }

    @Override
    public void showNetworkError() {

    }

    @Override
    public void setAdapter(PayAdapter payAdapter) {

    }

    @Override
    public PayActivity getPayActivity() {
        return PayActivity.this;
    }

    @Override
    public String getServiceId() {
        return tv_service_id.getText().toString();
    }

    @Override
    public String getPayType() {
        return tv_service_type.getText().toString();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mPayPresenter.handleActivityResult(requestCode, resultCode, data)) {
            Log.e("AAAAAAA", "222222222222222onActivityResult: "+data);
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPayPresenter.finish();
    }
}
