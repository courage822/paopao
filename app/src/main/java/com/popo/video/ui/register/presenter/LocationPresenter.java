package com.popo.video.ui.register.presenter;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import com.popo.library.util.LaunchHelper;
import com.popo.library.util.ToastUtil;
import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.AddressComponent;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.Countries;
import com.popo.video.data.model.Country;
import com.popo.video.data.model.GoogleApiModel;
import com.popo.video.data.model.PlatformInfo;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.ui.register.RegisterActivity;
import com.popo.video.ui.register.contract.LocationContract;

import java.util.List;
import java.util.Locale;

/**
 * Created by zhangdroid on 2017/5/31.
 */
public class LocationPresenter implements LocationContract.IPresenter {
    Location locationGps;
    Location locationNet;
    List<Country> countrys;
    private LocationContract.IView mLocationView;
    private Context mContext;
    private String country, language;


    public LocationPresenter(LocationContract.IView view) {
        this.mLocationView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
        getLocation();
    }


    private void getAssetsCountry(){
        countrys = Util.getCountryFromJson("Countries.json");
        mLocationView.getLocationAdapter().bind(countrys);
    }

    @Override
    public void getLocation() {
        getAssetsCountry();

        country = Locale.getDefault().getCountry();
        language = Locale.getDefault().getLanguage();
        //获取地理位置管理器
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (locationGps != null
                && (locationGps.getLongitude() > -90 || locationGps.getLongitude() < 90)
                && (locationGps.getLatitude() > -90 || locationGps.getLatitude() < 90)) {
            getCountryByLatLng(locationGps.getLatitude(), locationGps.getLongitude());
        } else if (locationNet != null
                && (locationNet.getLongitude() > -90 || locationNet.getLongitude() < 90)
                && (locationNet.getLatitude() > -90 || locationNet.getLatitude() < 90)) {
            getCountryByLatLng(locationNet.getLatitude(), locationNet.getLongitude());
        } else {//定位失败，可以选择
            mLocationView.getLocationAdapter().setCountry(country,true);
            userLoactionTag(1);
        }
    }

    @Override
    public void register() {
        int countryPosition = mLocationView.getLocationAdapter().getCountryPosition();
        if (countrys != null && countrys.size() != 0 && countryPosition != -1) {
            PlatformInfo platformInfo = PlatformPreference.getPlatformInfo();
            platformInfo.setCountry(countrys.get(countryPosition).getCountryEnglish());
            platformInfo.setFid("301" + countrys.get(countryPosition).getCountryNum());
            PlatformPreference.setPlatfromInfo(platformInfo);
            LaunchHelper.getInstance().launch(mContext, RegisterActivity.class);
        } else {
            ToastUtil.showShortToast(mContext, mContext.getString(R.string.location));
        }
    }

    private void getCountryByLatLng(double latitude, double longitude) {
        mLocationView.showLoading();
        ApiManager.getCountryByLatLng(latitude, longitude, new IGetDataListener<GoogleApiModel>() {
            @Override
            public void onResult(GoogleApiModel googleApiModel, boolean isEmpty) {
                if (googleApiModel.getResults() != null && googleApiModel.getResults().size() != 0) {
                    List<AddressComponent> address_components = googleApiModel.getResults().get(0).getAddress_components();
                    if (address_components != null && address_components.size() != 0) {
                        for (int i = 0; i < address_components.size(); i++) {
                            AddressComponent addressComponent = address_components.get(i);
                            if (addressComponent.getTypes() != null && addressComponent.getTypes().get(0).equals("country")) {
                                if (addressComponent.getShort_name().equals("CN")) {
                                    mLocationView.getLocationAdapter().setCountry(addressComponent.getShort_name(),true);
                                    userLoactionTag(3);
                                } else {
                                    mLocationView.getLocationAdapter().setCountry(addressComponent.getShort_name(),false);
                                    userLoactionTag(3);
                                }
                            }
                        }
                    } else {
                        mLocationView.getLocationAdapter().setCountry(country,true);
                        userLoactionTag(2);
                    }
                } else {
                    mLocationView.getLocationAdapter().setCountry(country,true);
                    userLoactionTag(2);
                }
                mLocationView.dismissLoading();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mLocationView.dismissLoading();
                mLocationView.getLocationAdapter().setCountry(country,true);
                userLoactionTag(2);
            }
        });
    }

    private void userLoactionTag(int tag) {
        String coutryName = "United States";
        String fid = "30101";
        String guid = "97";
        if (countrys != null && countrys.size() != 0) {
            for (int i = 0; i < countrys.size(); i++) {
                if (country.equals(countrys.get(i).getCountryShort())) {
                    coutryName = countrys.get(i).getCountryEnglish();
                    fid = "301" + countrys.get(i).getCountryNum();
                    guid = countrys.get(i).getGuid();
                    if (tag == 3) {
                        tag = 4;
                    }
                }
            }
        }
        PlatformInfo platformInfo = new PlatformInfo();
        platformInfo.setCountry(coutryName);
        platformInfo.setFid(fid);
        PlatformPreference.setPlatfromInfo(platformInfo);
        locationTag(guid, tag);
    }

    private void locationTag(String guid, int tag) {
        if (!UserPreference.isMactived()) {
            ApiManager.activation(new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                    // 记录已经激活过
                    UserPreference.mActivation();
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }
            });
        }

        ApiManager.userActivityTag("", guid, "15", tag + "", new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }
}
