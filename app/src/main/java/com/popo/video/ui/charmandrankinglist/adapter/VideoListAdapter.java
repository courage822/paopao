package com.popo.video.ui.charmandrankinglist.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;


import com.popo.video.data.model.VideoSquare;
import com.popo.video.ui.charmandrankinglist.VideoRoomFragment;

import java.util.List;

/**
 * Created by xuzhaole on 2018/3/20.
 */

public class VideoListAdapter extends FragmentStatePagerAdapter {
    private List<VideoSquare> mList;

    public VideoListAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        VideoRoomFragment videoRoomFragment = new VideoRoomFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position",position);
        videoRoomFragment.setArguments(bundle);
        videoRoomFragment.setData(mList.get(position));
        return videoRoomFragment;
    }

    @Override
    public int getCount() {
        return mList == null || mList.size() == 0 ? 0 : mList.size();
    }


    public void setData(List<VideoSquare> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    public void addData(List<VideoSquare> list) {
        this.mList.addAll(list);
        notifyDataSetChanged();
    }

    public List<VideoSquare> getData() {
        return mList;
    }
}
