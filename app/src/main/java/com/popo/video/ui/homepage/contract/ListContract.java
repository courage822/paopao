package com.popo.video.ui.homepage.contract;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;

import com.popo.library.adapter.wrapper.OnLoadMoreListener;
import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/6/2.
 */
public interface ListContract {

    interface IView extends BaseView {

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        void toggleShowError(boolean toggle, String msg);

        void toggleShowEmpty(boolean toggle, String msg);


        /**
         * 设置adapter
         *
         * @param adapter adapter
         */
        void setAdapter(RecyclerView.Adapter adapter);
    }

    interface IPresenter extends BasePresenter {

        /**
         * 加载推荐用户列表
         *
         */
        void loadRecommendUserList(int type);

        /**
         * 下拉刷新
         */
        void refresh();

        /**
         * 修改打招呼成功后的状态
         */
        void sayHelloStatus();

        void start(RecyclerView recyclerView);
    }
}
