package com.popo.video.ui.follow;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.popo.video.R;
import com.popo.video.base.BaseFragment;
import com.popo.video.event.IsFollow;
import com.popo.video.event.UpdataFollowUser;
import com.popo.library.adapter.wrapper.OnLoadMoreListener;
import com.popo.library.widget.AutoSwipeRefreshLayout;
import com.popo.library.widget.XRecyclerView;
import com.popo.video.ui.follow.contract.FollowContract;
import com.popo.video.ui.follow.presenter.FollowPresenter;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 关注
 * Created by zhangdroid on 2017/5/23.
 */
public class FollowFragment extends BaseFragment implements FollowContract.IView {
    @BindView(R.id.fragment_follow_xrecyerview)
    XRecyclerView mRecyclerView;
    @BindView(R.id.fragment_follow_swiperefresh)
    AutoSwipeRefreshLayout mSwipeRefresh;
    @BindView(R.id.fragment_follow_ll)
    LinearLayout ll_follow;
    private FollowPresenter mFollowPresenter;
    private LinearLayoutManager linearLayoutManager;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_follow;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return ll_follow;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mSwipeRefresh.setColorSchemeResources(R.color.main_color);
        mSwipeRefresh.setProgressBackgroundColorSchemeColor(Color.WHITE);
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mFollowPresenter = new FollowPresenter(this);
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mFollowPresenter.refresh();
            }
        });
    }

    @Override
    protected void loadData() {
        mFollowPresenter.loadFollowUserList();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }


    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefresh.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSwipeRefresh.isRefreshing()) {
                    mSwipeRefresh.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mFollowPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                toggleShowEmpty(false, null, null);
//                mFollowPresenter.refresh();
            }
        });
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getFragmentManager();
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter, int loadMoreViewId) {
        mRecyclerView.setAdapter(adapter, loadMoreViewId);
    }

    @Override
    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        mRecyclerView.setOnLoadingMoreListener(onLoadMoreListener);
    }

    @Override
    public void showLoadMore() {
        mRecyclerView.setVisibility(R.id.load_more_progress, true);
        mRecyclerView.setVisibility(R.id.load_more_msg, true);
        mRecyclerView.setVisibility(R.id.load_more_empty, false);
        mRecyclerView.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
        mRecyclerView.finishLoadMore();
    }

    @Override
    public void showNoMore() {
        mRecyclerView.setVisibility(R.id.load_more_empty, true);
        mRecyclerView.setVisibility(R.id.load_more_progress, false);
        mRecyclerView.setVisibility(R.id.load_more_msg, false);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Subscribe
    public void onEvent(UpdataFollowUser updataFollowUser) {
        handler.sendEmptyMessage(1);
    }

    @Subscribe
    public void onEvent(IsFollow follow) {
        mFollowPresenter.refresh();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Toast.makeText(mContext, mContext.getString(R.string.user_detail_follow_cancle), Toast.LENGTH_SHORT).show();
            mFollowPresenter.refresh();
        }
    };
}
