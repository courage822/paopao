package com.popo.video.ui.message.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.common.HyphenateHelper;
import com.popo.video.common.TimeUtils;
import com.popo.video.common.Util;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.model.IsFollow;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.event.UnreadMsgChangedEvent;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.parcelable.ChatParcelable;
import com.popo.video.parcelable.UserDetailParcelable;
import com.popo.video.ui.chat.ChatActivity;
import com.popo.video.ui.detail.UserDetailActivity;
import com.popo.video.ui.message.HelloPeopleActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xuzhaole on 2017/12/1.
 */

public class NewRecyclerAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private boolean mIsAnchor;
    private LayoutInflater inflater;
    private int count = 1;

    private List<HuanXinUser> videoList;
    private List<HuanXinUser> priList;
    private List<HuanXinUser> norList;
    private List<HuanXinUser> helloList;

    private int TYPE_KEFU = 1;
    private int TYPE_VIDEO = 3;
    private int TYPE_PRIVATE = 4;
    private int TYPE_NORMAL = 5;
    private int TYPE_HELLO = 2;
    private int TYPE_VIDEO_COUNT = 6;
    private int TYPE_PRIVATE_COUNT = 7;
    private int TYPE_NORMAL_TOP = 8;

    private List<HuanXinUser> tempVideoList;
    private List<HuanXinUser> tempPrivateList;

    private boolean isEditable = false;
    private OnSelectStateListener mOnSelectStateListener;


    public NewRecyclerAdapter(Context context, boolean mIsAnchor) {
        this.mContext = context;
        this.mIsAnchor = mIsAnchor;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_KEFU) {
            View inflate = inflater.inflate(R.layout.item_chat_history, parent, false);
            return new TypeKefuHolder(inflate);
        } else if (viewType == TYPE_PRIVATE) {
            View inflate = inflater.inflate(R.layout.item_chat_history, parent, false);
            return new TypePrivateHolder(inflate);
        } else if (viewType == TYPE_VIDEO) {
            View inflate = inflater.inflate(R.layout.item_chat_video, parent, false);
            return new TypeVideoHolder(inflate);
        } else if (viewType == TYPE_HELLO) {//打招呼的人，主播界面
            View inflate = inflater.inflate(R.layout.item_chat_history, parent, false);
            return new TypeHelloHolder(inflate);
        } else if (viewType == TYPE_PRIVATE_COUNT) {//私密消息标题栏
            View inflate = inflater.inflate(R.layout.item_private_count, parent, false);
            return new TypePriCountHolder(inflate);
        } else if (viewType == TYPE_VIDEO_COUNT) {//视频邀请标题栏
            View inflate = inflater.inflate(R.layout.item_private_count, parent, false);
            return new TypeVideoCountHolder(inflate);
        } else if (viewType == TYPE_NORMAL_TOP) {
            View inflate = inflater.inflate(R.layout.item_normal_top, parent, false);
            return new TypeNormalTopHolder(inflate);
        } else {
            View inflate = inflater.inflate(R.layout.item_chat_history, parent, false);
            return new TypeNormalHolder(inflate);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        if (holder instanceof TypeKefuHolder) {
            initKefu((TypeKefuHolder) holder, position);
        } else if (holder instanceof TypeHelloHolder) {
            initHello((TypeHelloHolder) holder, position);
        } else if (holder instanceof TypePrivateHolder) {
            initPrivate((TypePrivateHolder) holder, position);
        } else if (holder instanceof TypeVideoHolder) {
            initVideo((TypeVideoHolder) holder, position);
        } else if (holder instanceof TypePriCountHolder) {
            initPriCount((TypePriCountHolder) holder, position);
        } else if (holder instanceof TypeVideoCountHolder) {
            initVideoCount((TypeVideoCountHolder) holder, position);
        } else if (holder instanceof TypeNormalTopHolder) {
            initNormalTop((TypeNormalTopHolder) holder, position);
        } else {
            initNormal((TypeNormalHolder) holder, position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (UserPreference.isAnchor()) {
            if (helloList == null || helloList.size() == 0) {
                if (position == 0) {
                    return TYPE_NORMAL_TOP;
                } else {
                    return TYPE_NORMAL;
                }
            } else {
                if (position == 0) {
                    return TYPE_NORMAL_TOP;
                } else if (position == 1) {
                    return TYPE_HELLO;
                } else if (position == 2) {
                    return TYPE_NORMAL_TOP;
                } else {
                    return TYPE_NORMAL;
                }
            }
        } else {
            if ((videoList == null || videoList.size() == 0)
                    && (priList == null || priList.size() == 0)) {
                if (position == 0) {
                    return TYPE_NORMAL_TOP;
                } else {
                    return TYPE_NORMAL;
                }
            } else if ((videoList != null && videoList.size() != 0)
                    && (priList == null || priList.size() == 0)) {
                if (position == 0) {
                    return TYPE_VIDEO_COUNT;
                } else if (position >= 1 && position <= videoList.size()) {
                    return TYPE_VIDEO;
                } else if (position == videoList.size() + 1) {
                    return TYPE_NORMAL_TOP;
                } else {
                    return TYPE_NORMAL;
                }
            } else if ((videoList == null || videoList.size() == 0)
                    && (priList != null && priList.size() != 0)) {
                if (position == 0) {
                    return TYPE_PRIVATE_COUNT;
                } else if (position <= priList.size()) {
                    return TYPE_PRIVATE;
                } else if (position == priList.size() + 1) {
                    return TYPE_NORMAL_TOP;
                } else {
                    return TYPE_NORMAL;
                }
            } else {
                if (position == 0) {
                    return TYPE_VIDEO_COUNT;
                } else if (position <= videoList.size()) {
                    return TYPE_VIDEO;
                } else if (position == videoList.size() + 1) {
                    return TYPE_PRIVATE_COUNT;
                } else if (position >= videoList.size() + 2 && position <= priList.size() + 1 + videoList.size()) {
                    return TYPE_PRIVATE;
                } else if (position == priList.size() + videoList.size() + 3) {
                    return TYPE_NORMAL_TOP;
                } else {
                    return TYPE_NORMAL;
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return count;
    }

    private void initKefu(TypeKefuHolder holder, final int position) {
        holder.nickname.setText(mContext.getString(R.string.kefu_desc));
        final HuanXinUser user = DbModle.getInstance().getUserAccountDao().getAccountByHyID("10000");
        if (user != null) {
            DataPreference.setSystemIcon(user.getHxIcon());
            if (user.getMsgNum() > 0) {
                holder.msgnum.setVisibility(View.VISIBLE);
                holder.msgnum.setText(user.getMsgNum() + "");
            } else {
                holder.msgnum.setVisibility(View.GONE);
            }
            holder.message.setText(user.getLastMsg());
        } else {
            holder.msgnum.setVisibility(View.GONE);
            holder.message.setText(mContext.getString(R.string.user_hello));
        }
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(DataPreference.getSystemIcon())
                .placeHolder(R.drawable.chat_admin).error(R.drawable.chat_admin).imageView(holder.avatar).build());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(10000,
                        "100010000", mContext.getString(R.string.kefu_desc), DataPreference.getSystemIcon(), 0));
            }
        });
    }

    private void initHello(final TypeHelloHolder holder, int position) {
        holder.avatar.setImageResource(R.drawable.hi_icon);
        holder.nickname.setText(mContext.getString(R.string.hello_people));
        if (helloList == null || helloList.size() == 0) {
            holder.msgnum.setVisibility(View.GONE);
            holder.message.setText(mContext.getString(R.string.no_hello_people));
        } else {
            if (helloList.size() >= 1) {
                holder.message.setText(mContext.getString(R.string.have) + helloList.size() + mContext.getString(R.string.hello_people_count));
                holder.msgnum.setVisibility(View.VISIBLE);
                int helloCount = 0;
                for (HuanXinUser user : helloList) {
                    helloCount += user.getMsgNum();
                }
                if (helloCount > 0) {
                    holder.msgnum.setText(helloCount + "");
                } else {
                    holder.msgnum.setVisibility(View.GONE);
                }
            } else {
                holder.msgnum.setVisibility(View.GONE);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LaunchHelper.getInstance().launch(mContext, HelloPeopleActivity.class);
                }
            });
        }
        final HuanXinUser user;
        if (helloList != null && helloList.size() != 0) {
            user = helloList.get(position - 1);
            if (user != null) {
                if (isEditable) {
                    holder.checkbox.setVisibility(View.VISIBLE);
                    if (user.isSelect()) {
                        holder.checkbox.setImageResource(R.drawable.message_check);
                    } else {
                        holder.checkbox.setImageResource(R.drawable.message_uncheck);
                    }
                } else {
                    holder.checkbox.setVisibility(View.GONE);
                }
                holder.checkbox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (user.isSelect()) {
                            user.setSelect(false);
                            holder.checkbox.setImageResource(R.drawable.message_uncheck);
                            mOnSelectStateListener.removeHxId(user.getHxId());
                        } else {
                            user.setSelect(true);
                            holder.checkbox.setImageResource(R.drawable.message_check);
                            mOnSelectStateListener.addHxId(user.getHxId());

                        }
                    }
                });
            }
        }
    }

    private void initPrivate(final TypePrivateHolder holder, final int position) {
        if (priList == null || priList.size() == 0) {
            return;
        }
        final HuanXinUser user;
        if (videoList == null || videoList.size() == 0) {
            user = priList.get(position - 1);
        } else {
            user = priList.get(position - 2 - videoList.size());
        }
        if (user != null) {
            holder.nickname.setText(user.getHxName());
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(user.getHxIcon())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(holder.avatar).build());
            if (user.getMsgNum() > 0) {
                holder.msgnum.setVisibility(View.VISIBLE);
                holder.msgnum.setText(user.getMsgNum() + "");
//                holder.message.setText(user.getMsgNum() + mContext.getString(R.string.private_message_number));
            } else {
                holder.msgnum.setVisibility(View.GONE);
            }
            holder.time.setText(TimeUtils.getLocalTime(mContext, System.currentTimeMillis(), Long.parseLong(user.getMsgTime())));

            if (isEditable) {
                holder.checkbox.setVisibility(View.VISIBLE);
                if (user.isSelect()) {
                    holder.checkbox.setImageResource(R.drawable.message_check);
                } else {
                    holder.checkbox.setImageResource(R.drawable.message_uncheck);
                }
            } else {
                holder.checkbox.setVisibility(View.GONE);
            }
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (user.isSelect()) {
                        user.setSelect(false);
                        holder.checkbox.setImageResource(R.drawable.message_uncheck);
                        mOnSelectStateListener.removeHxId(user.getHxId());
                    } else {
                        user.setSelect(true);
                        holder.checkbox.setImageResource(R.drawable.message_check);
                        mOnSelectStateListener.addHxId(user.getHxId());

                    }
                }
            });


            holder.avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                            new UserDetailParcelable(user.getHxId()));
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                    DbModle.getInstance().getUserAccountDao().setState(user, true);//把标记设置为已经读取
                    String hxId = user.getHxId();
                    if (!TextUtils.isEmpty(hxId)) {
                        long guid = Long.parseLong(hxId);
                        LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(guid,
                                user.getAccount(), user.getHxName(), user.getHxIcon(), 0));
                    }
                }
            });

        }
    }

    private void initVideo(final TypeVideoHolder holder, int position) {
        if (videoList == null || videoList.size() == 0) {
            return;
        }
        final HuanXinUser user = videoList.get(position - 1);
        if (user != null) {
            holder.nickname.setText(user.getHxName());
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(user.getHxIcon())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(holder.avatar).build());
            if (user.getExtendType() == 2) {//视频邀请
//                holder.message.setText(mContext.getString(R.string.video_type));
                holder.iv_type.setImageResource(R.drawable.video_recall);
            } else {
//                holder.message.setText(mContext.getString(R.string.voice_type));
                holder.iv_type.setImageResource(R.drawable.phone_recall);
            }
            if (user.getMsgNum() > 0) {
                holder.msgnum.setVisibility(View.VISIBLE);
                holder.msgnum.setText(user.getMsgNum() + "");
            } else {
                holder.msgnum.setVisibility(View.GONE);
            }

            if (isEditable) {
                holder.checkbox.setVisibility(View.VISIBLE);
                if (user.isSelect()) {
                    holder.checkbox.setImageResource(R.drawable.message_check);
                } else {
                    holder.checkbox.setImageResource(R.drawable.message_uncheck);
                }
            } else {
                holder.checkbox.setVisibility(View.GONE);
            }
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (user.isSelect()) {
                        user.setSelect(false);
                        holder.checkbox.setImageResource(R.drawable.message_uncheck);
                        mOnSelectStateListener.removeHxId(user.getHxId());

                    } else {
                        user.setSelect(true);
                        holder.checkbox.setImageResource(R.drawable.message_check);
                        mOnSelectStateListener.addHxId(user.getHxId());
                    }
                }
            });

            holder.avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                            new UserDetailParcelable(user.getHxId()));
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                    DbModle.getInstance().getUserAccountDao().setState(user, true);//把标记设置为已经读取
                    String hxId = user.getHxId();
                    if (!TextUtils.isEmpty(hxId)) {
                        long guid = Long.parseLong(hxId);
                        LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(guid,
                                user.getAccount(), user.getHxName(), user.getHxIcon(), 0));
                    }
                }
            });
        }
    }

    private void initPriCount(TypePriCountHolder holder, int position) {
        holder.tv_type.setText(mContext.getString(R.string.private_news));
        if (tempPrivateList.size() == 1) {
            holder.tv_count.setText(mContext.getString(R.string.private_news_count, tempPrivateList.size()));
        } else {
            holder.tv_count.setText(mContext.getString(R.string.private_news_counts, tempPrivateList.size()));
        }
        holder.rl_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (priList != null && priList.size() > 0) {
                    if (tempPrivateList == null || tempPrivateList.size() == 0) {
                        return;
                    }
                    if (priList.size() > 1) {
                        priList.clear();
                        count -= tempPrivateList.size();
                        HuanXinUser huanXinUser = tempPrivateList.get(0);
                        priList.add(huanXinUser);
                        count += 1;
                        notifyDataSetChanged();
                    } else {
                        priList.clear();
                        count -= 1;
                        priList.addAll(tempPrivateList);
                        count += tempPrivateList.size();
                        notifyDataSetChanged();
                    }
                }
            }
        });
    }

    private void initVideoCount(TypeVideoCountHolder holder, int position) {
        holder.tv_type.setText(mContext.getString(R.string.video_sound_invite));
        if (tempVideoList.size() == 1) {
            holder.tv_count.setText(mContext.getString(R.string.video_sound_invite_count, tempVideoList.size()));
        } else {
            holder.tv_count.setText(mContext.getString(R.string.video_sound_invite_counts, tempVideoList.size()));
        }
        holder.rl_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (videoList != null && videoList.size() > 0) {
                    if (videoList.size() > 1) {
                        videoList.clear();
                        count -= tempVideoList.size();
                        HuanXinUser huanXinUser = tempVideoList.get(0);
                        videoList.add(huanXinUser);
                        count += 1;
                        notifyDataSetChanged();
                    } else {
                        videoList.clear();
                        count -= 1;
                        videoList.addAll(tempVideoList);
                        count += tempVideoList.size();
                        notifyDataSetChanged();
                    }
                }
            }
        });
    }

    private void initNormalTop(TypeNormalTopHolder holder, int position) {

    }

    private void initNormal(final TypeNormalHolder holder, final int position) {
        if (norList == null || norList.size() == 0) {
            return;
        }

        final HuanXinUser user;
        if (UserPreference.isAnchor()) {
            if (helloList != null && helloList.size() != 0) {
                user = norList.get(position - 3);
            } else {
                user = norList.get(position - 1);
            }
        } else {
            if ((videoList == null || videoList.size() == 0) && (priList == null || priList.size() == 0)) {
                user = norList.get(position - 1);
            } else if ((videoList != null && videoList.size() != 0) && (priList == null || priList.size() == 0)) {
                user = norList.get(position - 2 - videoList.size());
            } else if ((videoList == null || videoList.size() == 0) && (priList != null && priList.size() != 0)) {
                user = norList.get(position - 2 - priList.size());
            } else {
                user = norList.get(position - videoList.size() - priList.size() - 4);
            }
        }
        if (user != null) {
            if (user.getHxId().equals("10000")) {
                holder.nickname.setText(mContext.getString(R.string.kefu_desc));
                if (user != null) {
                    DataPreference.setSystemIcon(user.getHxIcon());
                    if (user.getMsgNum() > 0) {
                        holder.msgnum.setVisibility(View.VISIBLE);
                        holder.msgnum.setText(user.getMsgNum() + "");
                    } else {
                        holder.msgnum.setVisibility(View.GONE);
                    }
                    holder.message.setText(user.getLastMsg());
                } else {
                    holder.msgnum.setVisibility(View.GONE);
                    holder.message.setText(mContext.getString(R.string.user_hello));
                }
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(DataPreference.getSystemIcon())
                        .placeHolder(R.drawable.chat_admin).error(R.drawable.chat_admin).imageView(holder.avatar).build());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(10000,
                                "100010000", mContext.getString(R.string.kefu_desc), DataPreference.getSystemIcon(), 0));
                    }
                });
            } else {


                holder.nickname.setText(user.getHxName());
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(user.getHxIcon())
                        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(holder.avatar).build());
                if (user.getMsgNum() > 0) {
                    holder.msgnum.setVisibility(View.VISIBLE);
                    holder.msgnum.setText(user.getMsgNum() + "");
                } else {
                    holder.msgnum.setVisibility(View.GONE);
                }
                if (!TextUtils.isEmpty(user.getMsgTime())){
                    holder.time.setText(TimeUtils.getLocalTime(mContext, System.currentTimeMillis(), Long.parseLong(user.getMsgTime())));
                }

                if (isEditable) {
                    holder.checkbox.setVisibility(View.VISIBLE);
                    if (user.isSelect()) {
                        holder.checkbox.setImageResource(R.drawable.message_check);
                    } else {
                        holder.checkbox.setImageResource(R.drawable.message_uncheck);
                    }
                } else {
                    holder.checkbox.setVisibility(View.GONE);
                }
                holder.checkbox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (user.isSelect()) {
                            user.setSelect(false);
                            holder.checkbox.setImageResource(R.drawable.message_uncheck);
                            mOnSelectStateListener.removeHxId(user.getHxId());
                        } else {
                            user.setSelect(true);
                            holder.checkbox.setImageResource(R.drawable.message_check);
                            mOnSelectStateListener.addHxId(user.getHxId());
                        }
                    }
                });


                holder.avatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                                new UserDetailParcelable(user.getHxId()));
                    }
                });
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                        DbModle.getInstance().getUserAccountDao().setState(user, true);//把标记设置为已经读取
                        String hxId = user.getHxId();
                        if (!TextUtils.isEmpty(hxId)) {
                            long guid = Long.parseLong(hxId);
                            LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(guid,
                                    user.getAccount(), user.getHxName(), user.getHxIcon(), 0));
                        }
                    }
                });
            }
        }
    }

    public void setVideoList(List<HuanXinUser> list) {
        this.tempVideoList = list;
        if (tempVideoList.size() > 0) {
            this.videoList = new ArrayList<>();
            HuanXinUser huanXinUser = tempVideoList.get(0);
            this.videoList.add(huanXinUser);
            count += 1;
        }
        notifyDataSetChanged();
    }

    public void setPrivateList(List<HuanXinUser> list) {
        this.priList = list;
        this.tempPrivateList = new ArrayList<>();
        this.tempPrivateList.addAll(list);
        if (tempPrivateList.size() > 0) {
            if (tempVideoList != null && tempVideoList.size() != 0) {
                this.count += (tempPrivateList.size() + 1);
            } else {
                this.count += tempPrivateList.size();
            }
        }
        notifyDataSetChanged();
    }

    public void setNormalList(List<HuanXinUser> list) {
        this.norList = list;
        if (norList.size() > 0) {
            if (!UserPreference.isAnchor()){
                if ((tempVideoList != null && tempVideoList.size() != 0)
                        || (priList != null && tempPrivateList.size() != 0)) {
                    this.count += (norList.size() + 1);
                } else {
                    this.count += norList.size();
                }
            }else{
                if (helloList != null && helloList.size() != 0){
                    this.count += (norList.size() + 1);
                }else{
                    this.count += norList.size();
                }
            }
        }
        notifyDataSetChanged();
    }

    //打招呼
    public void setHellolList(List<HuanXinUser> list) {
        this.helloList = list;
        if (helloList != null && helloList.size() != 0){
            this.count += 1;
        }
        notifyDataSetChanged();
    }

    public void reSet() {
        if (videoList != null) {
            videoList.clear();
            videoList = null;
        }
        if (priList != null) {
            priList.clear();
            priList = null;
        }
        if (norList != null) {
            norList.clear();
            norList = null;
        }
        if (helloList != null) {
            helloList.clear();
            helloList = null;
        }
        this.count = 1;
        notifyDataSetChanged();
    }

    //设置编辑状态
    public void setEditable(boolean b) {
        this.isEditable = b;
        notifyDataSetChanged();
    }

    public void setOnSelectStateListener(OnSelectStateListener mOnSelectStateListener) {
        this.mOnSelectStateListener = mOnSelectStateListener;
    }

    public interface OnSelectStateListener {
        void addHxId(String hxid);

        void removeHxId(String hxid);
    }

    public class TypeKefuHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        TextView nickname, message, time, msgnum;

        public TypeKefuHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.item_chat_history_avatar);
            nickname = (TextView) itemView.findViewById(R.id.item_chat_history_nickname);
            message = (TextView) itemView.findViewById(R.id.item_chat_history_message);
            time = (TextView) itemView.findViewById(R.id.item_chat_is_tv_time);
            msgnum = (TextView) itemView.findViewById(R.id.item_chat_history_tv_msgnum);
        }
    }

    public class TypeHelloHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        ImageView checkbox;
        TextView nickname, message, time, msgnum;

        public TypeHelloHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.item_chat_history_avatar);
            nickname = (TextView) itemView.findViewById(R.id.item_chat_history_nickname);
            message = (TextView) itemView.findViewById(R.id.item_chat_history_message);
            time = (TextView) itemView.findViewById(R.id.item_chat_is_tv_time);
            msgnum = (TextView) itemView.findViewById(R.id.item_chat_history_tv_msgnum);
            checkbox = (ImageView) itemView.findViewById(R.id.checkbox);

        }
    }

    public class TypeNormalTopHolder extends RecyclerView.ViewHolder {

        public TypeNormalTopHolder(View itemView) {
            super(itemView);
        }
    }

    public class TypePrivateHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        TextView nickname, message, time, msgnum;
        ImageView checkbox;

        public TypePrivateHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.item_chat_history_avatar);
            nickname = (TextView) itemView.findViewById(R.id.item_chat_history_nickname);
            message = (TextView) itemView.findViewById(R.id.item_chat_history_message);
            time = (TextView) itemView.findViewById(R.id.item_chat_is_tv_time);
            msgnum = (TextView) itemView.findViewById(R.id.item_chat_history_tv_msgnum);
            checkbox = (ImageView) itemView.findViewById(R.id.checkbox);

        }
    }

    public class TypePriCountHolder extends RecyclerView.ViewHolder {
        TextView tv_count, tv_type;
        RelativeLayout rl_video;

        public TypePriCountHolder(View itemView) {
            super(itemView);
            tv_count = (TextView) itemView.findViewById(R.id.tv_count);
            tv_type = (TextView) itemView.findViewById(R.id.tv_type);
            rl_video = (RelativeLayout) itemView.findViewById(R.id.rl_video);
        }
    }

    public class TypeVideoCountHolder extends RecyclerView.ViewHolder {
        TextView tv_count, tv_type;
        RelativeLayout rl_video;

        public TypeVideoCountHolder(View itemView) {
            super(itemView);
            tv_count = (TextView) itemView.findViewById(R.id.tv_count);
            tv_type = (TextView) itemView.findViewById(R.id.tv_type);
            rl_video = (RelativeLayout) itemView.findViewById(R.id.rl_video);
        }
    }

    public class TypeVideoHolder extends RecyclerView.ViewHolder {
        ImageView avatar, iv_type;
        TextView nickname, message, msgnum;
        ImageView checkbox;


        public TypeVideoHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.item_chat_history_avatar);
            nickname = (TextView) itemView.findViewById(R.id.item_chat_history_nickname);
            message = (TextView) itemView.findViewById(R.id.item_chat_history_message);
            iv_type = (ImageView) itemView.findViewById(R.id.iv_type);
            msgnum = (TextView) itemView.findViewById(R.id.item_chat_history_tv_msgnum);
            checkbox = (ImageView) itemView.findViewById(R.id.checkbox);

        }
    }

    public class TypeNormalHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        TextView nickname, message, time, msgnum;
        ImageView checkbox;

        public TypeNormalHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.item_chat_history_avatar);
            nickname = (TextView) itemView.findViewById(R.id.item_chat_history_nickname);
            message = (TextView) itemView.findViewById(R.id.item_chat_history_message);
            time = (TextView) itemView.findViewById(R.id.item_chat_is_tv_time);
            msgnum = (TextView) itemView.findViewById(R.id.item_chat_history_tv_msgnum);
            checkbox = (ImageView) itemView.findViewById(R.id.checkbox);
        }
    }

}
