package com.popo.video.ui.main.contract;

import com.popo.video.data.model.SearchUser;
import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

import java.util.List;

/**
 * Created by WangYong on 2017/12/12.
 */
public interface MainActivityContract {

    interface IView extends BaseView {
        /**
         * 群打招呼
         */
        void sayHellAll(List<SearchUser> searchUserList1);
        /**
         * 是否开通vip
         */
        void isVipUser();

    }

    interface IPresenter extends BasePresenter {
        /**
         * 下载数据
         */
        void loadData();
    }

}
