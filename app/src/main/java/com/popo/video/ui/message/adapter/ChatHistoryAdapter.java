package com.popo.video.ui.message.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.common.TimeUtils;
import com.popo.video.common.Util;
import com.popo.video.data.model.HuanXinUser;
import com.popo.library.adapter.CommonRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.parcelable.UserDetailParcelable;
import com.popo.video.ui.detail.UserDetailActivity;

/**
 * 聊天记录适配器
 * Created by zhangdroid on 2017/7/6.
 */
public class ChatHistoryAdapter extends CommonRecyclerViewAdapter<HuanXinUser> {

    public ChatHistoryAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    public void convert(final HuanXinUser user, int position, RecyclerViewHolder holder) {
        if (user != null) {
            String hxId = user.getHxId();
            if (!TextUtils.isEmpty(hxId)) {
                ImageView ivAvatar = (ImageView) holder.getView(R.id.item_chat_history_avatar);
                RelativeLayout view = (RelativeLayout) holder.getView(R.id.item_chat_history_rl_view);
                if (user.getHxId().equals("10000")) {
                    view.setVisibility(View.GONE);
                } else {
                    view.setVisibility(View.VISIBLE);
                }
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(user.getHxIcon())
                        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(ivAvatar).build());
                holder.setText(R.id.item_chat_history_nickname, user.getHxName());
                holder.setText(R.id.item_chat_history_message, String.valueOf(user.getLastMsg()));
                TextView tv_msgNum = (TextView) holder.getView(R.id.item_chat_history_tv_msgnum);
                if (user.getMsgNum() > 0) {
                    tv_msgNum.setVisibility(View.VISIBLE);
                    tv_msgNum.setText(user.getMsgNum() + "");
                } else {
                    tv_msgNum.setVisibility(View.GONE);
                }
                holder.setText(R.id.item_chat_is_tv_time, TimeUtils.getLocalTime(mContext, System.currentTimeMillis(), Long.parseLong(user.getMsgTime())));
                ivAvatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                                new UserDetailParcelable(user.getHxId()));
                    }
                });
            } else {
            }
        }

    }


}

// if(list!=null&&list.size()>0){
//final HuanXinUser user = list.get(position);
//        if (user!=null) {
//        String hxId = user.getHxId();
//        if (!TextUtils.isEmpty(hxId)) {
//        ImageView ivAvatar = (ImageView) holder.getView(R.id.item_chat_history_avatar);
//        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(user.getHxIcon())
//        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(ivAvatar).build());
//        holder.setText(R.id.item_chat_history_nickname,user.getHxName());
//        holder.setText(R.id.item_chat_history_message,user.getLastMsg());
//        TextView tv_msgNum = (TextView) holder.getView(R.id.item_chat_history_tv_msgnum);
//        if(!TextUtils.isEmpty(user.getMsgNum())&&user.getMsgNum().length()>0){
//        int i = Integer.parseInt(user.getMsgNum());
//        if(i>0){
//        tv_msgNum.setVisibility(View.VISIBLE);
//        tv_msgNum.setText(user.getMsgNum());
//        }else{
//        tv_msgNum.setVisibility(View.GONE);
//        }
//        }
//        holder.setText(R.id.item_chat_is_tv_time,  DateTimeUtil.convertTimeMillis2String(Long.parseLong(user.getMsgTime())));
//        ivAvatar.setOnClickListener(new View.OnClickListener() {
//@Override
//public void onClick(View v) {
//        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
//        new UserDetailParcelable(user.getHxId()));
//        }
//        });
//        }else{
//        FrameLayout view = (FrameLayout) holder.getView(R.id.item_chat_history_card_view);
//        view.setVisibility(View.GONE);
//        }
//        }
//        }