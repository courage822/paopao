package com.popo.video.ui.register;

import android.content.Context;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.base.BaseTopBarActivity;
import com.popo.video.common.AgreenmentDialog;
import com.popo.video.event.FinishEvent;
import com.popo.library.dialog.LoadingDialog;
import com.popo.library.net.NetUtil;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.ui.dialog.AgeSelectDialog;
import com.popo.video.ui.register.contract.RegisterContract;
import com.popo.video.ui.register.presenter.RegisterPresenter;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 注册页面
 * Created by zhangdroid on 2017/5/12.
 */
public class RegisterActivity extends BaseTopBarActivity implements View.OnClickListener, RegisterContract.IView {
    @BindView(R.id.register_root)
    RelativeLayout mLlRoot;
    @BindView(R.id.register)
    Button mBtnRegister;
    @BindView(R.id.register_login)
    TextView mTvLogin;
    @BindView(R.id.register_activity_ll_select_age)
    RelativeLayout ll_select_age;
    @BindView(R.id.register_activity_et_nickname)
    EditText et_nickname;
    @BindView(R.id.register_activity_rl_male)
    RelativeLayout rl_male;
    @BindView(R.id.register_activity_rl_famale)
    RelativeLayout rl_famale;
    @BindView(R.id.register_activity_iv_male)
    ImageView iv_male;
    @BindView(R.id.register_activity_iv_famale)
    ImageView iv_famale;
    @BindView(R.id.register_activity_tv_age)
    TextView tv_age;
    @BindView(R.id.register_activity_iv_back)
    ImageView iv_back;
    @BindView(R.id.register_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.register_activity_rl_toobar)
    RelativeLayout registerActivityRlToobar;
    @BindView(R.id.register_activity_ll_nickname)
    LinearLayout registerActivityLlNickname;
    @BindView(R.id.register_activity_rg_sex)
    LinearLayout registerActivityRgSex;
    private String sexText = "";
    InputMethodManager imm;
    private RegisterPresenter mRegisterPresenter;
    private List<String> mAgeList = new ArrayList<>();

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_register;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return null;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        sexText = mContext.getResources().getString(R.string.register_male);
        for (int i = 18; i < 66; i++) {
            mAgeList.add(String.valueOf(i));
        }
        mRegisterPresenter = new RegisterPresenter(this);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        AgreenmentDialog.agreenmentShow(mContext);
    }


    @Override
    protected void setListeners() {
        mBtnRegister.setOnClickListener(this);
        mTvLogin.setOnClickListener(this);
        ll_select_age.setOnClickListener(this);
        rl_male.setOnClickListener(this);
        rl_famale.setOnClickListener(this);
        rl_back.setOnClickListener(this);

        et_nickname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence != null && charSequence.length() > 0) {
                    mBtnRegister.setEnabled(true);
                } else {
                    mBtnRegister.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_activity_ll_select_age:// 设置年龄
                if (!TextUtils.isEmpty(tv_age.getText().toString())) {
                    AgeSelectDialog.show(getSupportFragmentManager(), tv_age.getText().toString(), new AgeSelectDialog.OnAgeSelectListener() {
                        @Override
                        public void onSelected(String age) {
                            tv_age.setText(age);
                        }
                    });
                } else {
                    AgeSelectDialog.show(getSupportFragmentManager(), "22", new AgeSelectDialog.OnAgeSelectListener() {
                        @Override
                        public void onSelected(String age) {
                            tv_age.setText(age);
                        }
                    });
                }
                break;

            case R.id.register:// 注册
                if (!TextUtils.isEmpty(et_nickname.getText().toString())) {
                    mRegisterPresenter.register();
                }
                break;

            case R.id.register_login:// 跳转登录页面
                mRegisterPresenter.goLoginPage();
                break;
            case R.id.register_activity_rl_back:
                finish();
                break;
            case R.id.register_activity_rl_male:
                iv_male.setImageResource(R.drawable.ic_radio_male_sel);
                iv_famale.setImageResource(R.drawable.ic_radio_nor);
                sexText = getString(R.string.register_male);
                mRegisterPresenter.checkValid();
                break;
            case R.id.register_activity_rl_famale:
                iv_famale.setImageResource(R.drawable.ic_radio_sel);
                iv_male.setImageResource(R.drawable.ic_radio_nor);
                sexText = getString(R.string.register_female);
                mRegisterPresenter.checkValid();
                break;
        }
    }

    @Override
    protected void loadData() {
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public String getNickname() {
        return et_nickname.getText().toString();
    }

    @Override
    public boolean getSex() {
        return getString(R.string.register_male).equals(sexText);
    }

    @Override
    public String getAge() {
            return tv_age.getText().toString();
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void setRegisterEnable() {
        mBtnRegister.setEnabled(true);
    }

    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }

    //触摸屏幕，可以让软键盘消失
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (this.getCurrentFocus() != null) {
                if (this.getCurrentFocus().getWindowToken() != null) {
                    imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
        return super.onTouchEvent(event);
    }
}
