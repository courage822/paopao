package com.popo.video.ui.chat;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.hyphenate.chat.EMImageMessageBody;
import com.hyphenate.chat.EMMessage;
import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.preference.UserPreference;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.adapter.provider.ItemViewProvider;
import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.util.DateTimeUtil;
import com.popo.library.util.DeviceUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.widget.ChatImageView;
import com.popo.video.parcelable.BigPhotoParcelable;
import com.popo.video.ui.photo.BigPhotoActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 聊天图片消息（发送方）
 * Created by zhangdroid on 2017/6/29.
 */
public class ImageMessageRightProvider implements ItemViewProvider<EMMessage> {
    private Context mContext;
    private List<EMMessage> messageList;

    public ImageMessageRightProvider(Context context, List<EMMessage> messageList) {
        this.mContext = context;
        this.messageList = messageList;

    }

    @Override
    public int getItemViewLayoutResId() {
        return R.layout.item_chat_image_right;
    }

    @Override
    public boolean isViewType(EMMessage item, int position) {
        return (item.getType() == EMMessage.Type.IMAGE && item.direct() == EMMessage.Direct.SEND);
    }

    @Override
    public void convert(EMMessage emMessage, int position, RecyclerViewHolder holder) {
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                .error(Util.getDefaultImageCircle()).url(UserPreference.getSmallImage()).imageView((ImageView) holder.getView(R.id.item_chat_image_avatar_right)).build());
        if (null != emMessage) {
            if (position > 0) {
                long msgTime = messageList.get(position - 1).getMsgTime();
                if ((emMessage.getMsgTime() - msgTime) / (1000 * 60) > 5) {
                    holder.getView(R.id.item_chat_image_time_right).setVisibility(View.VISIBLE);

                    if (!TextUtils.isEmpty(UserPreference.getCountryId()) && UserPreference.getCountryId().equals("97")) {
                        String s = DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime());
                        if (s.length() > 11) {
                            String substring = s.substring(0, 4);
                            String substring1 = s.substring(5,7);
                            String substring2 = s.substring(8, 10);
                            String substring3 = s.substring(11);
                            holder.setText(R.id.item_chat_image_time_right, substring3 + " " + substring1
                                    + "/" + substring2+"/"+ substring);
                        }
                    } else {
                        String s = DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime());
                        if (s.length() > 11) {
                            String substring = s.substring(0, 4);
                            String substring1 = s.substring(5,7);
                            String substring2 = s.substring(8, 10);
                            String substring3 = s.substring(11);
                            holder.setText(R.id.item_chat_image_time_right, substring3 + " " + substring2
                                    + "/" + substring1+"/"+ substring);
                        }
                    }

                } else {
                    holder.getView(R.id.item_chat_image_time_right).setVisibility(View.GONE);
                }
            } else {
                holder.getView(R.id.item_chat_image_time_right).setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(UserPreference.getCountryId()) && UserPreference.getCountryId().equals("97")) {
                    String s = DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime());
                    if (s.length() > 11) {
                        String substring = s.substring(0, 4);
                        String substring1 = s.substring(5,7);
                        String substring2 = s.substring(8, 10);
                        String substring3 = s.substring(11);
                        holder.setText(R.id.item_chat_image_time_right, substring3 + " " + substring1
                                + "/" + substring2+"/"+ substring);
                    }
                } else {
                    String s = DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime());
                    if (s.length() > 11) {
                        String substring = s.substring(0, 4);
                        String substring1 = s.substring(5,7);
                        String substring2 = s.substring(8, 10);
                        String substring3 = s.substring(11);
                        holder.setText(R.id.item_chat_image_time_right, substring3 + " " + substring2
                                + "/" + substring1+"/"+ substring);
                    }
                }
            }


            final EMImageMessageBody emImageMessageBody = (EMImageMessageBody) emMessage.getBody();
            if (null != emImageMessageBody) {
                ChatImageView chatImageView = (ChatImageView) holder.getView(R.id.item_chat_image_right);
                // 设置图片规格：1/3屏幕宽度，宽高比3：4
                int width = DeviceUtil.getScreenWidth(mContext) / 3;
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, (int) ((4 / 3.0f) * width));
                chatImageView.setLayoutParams(layoutParams);
                Glide.with(mContext).load(new File(emImageMessageBody.getLocalUrl())).error(Util.getDefaultImage())
                        .placeholder(Util.getDefaultImage()).into(chatImageView);
                chatImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 点击查看大图
                        List<String> list = new ArrayList<String>();
                        list.add(emImageMessageBody.getLocalUrl());
                        LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(0, list));
                    }
                });
            }
        }
    }

}
