package com.popo.video.ui.pay.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.popo.library.util.DeviceUtil;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.event.PaySuccessEvent;
import com.popo.video.ui.pay.contract.PayContract;
import com.popo.video.ui.pay.util.IabHelper;
import com.popo.video.ui.pay.util.IabResult;
import com.popo.video.ui.pay.util.Purchase;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by WangYong on 2018/2/9.
 */
public class PayPresenter implements PayContract.IPresenter {
    private PayContract.IView mPayView;
    private Context mContext;
    private IabHelper mHelper;
    private boolean mIsInitialized;
    private static final int REQUEST_CODE = 0;
    public PayPresenter(PayContract.IView view) {
        this.mPayView = view;
        this.mContext = view.obtainContext();
    }
    @Override
    public void start() {
        mHelper = new IabHelper(mContext, C.KEY_GOOGLE_APP);
        mHelper.enableDebugLogging(true);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    return;
                }
                // 初始化成功
                mIsInitialized = true;
            }
        });
    }

    @Override
    public void getPayWay(String fromTag) {

    }

    @Override
    public void purchase(String sku) {
        if (checkPayEnabled()) {
            mHelper.launchPurchaseFlow((Activity) mContext, sku, REQUEST_CODE, mPurchaseFinishedListener);
        } else {
            mPayView.showTip(mContext.getString(R.string.pay_disabled));
        }
    }

    @Override
    public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("AAAAAAA", "handleActivityResult: ========================="+data);
        return mHelper.handleActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        mIsInitialized = false;
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }

    /**
     * @return 检查Google支付是否可用
     */
    private boolean checkPayEnabled() {
        return DeviceUtil.checkAPKExist(mPayView.obtainContext(), "com.android.vending") && mHelper != null && mIsInitialized;
    }
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                Log.e("AAAAAAA", "onIabPurchaseFinished: 支付失败----------");
                return;
            }
            Log.e("AAAAAAA", "onIabPurchaseFinished: 支付陈宫========"+purchase.getSku());
            switch (purchase.getSku()) {
                case C.pay.SKU1:
                    payment(purchase);//int为serviceID
                    break;
                case C.pay.SKU2:
                    payment(purchase);
                    break;
                case C.pay.SKU3:
                    payment(purchase);
                    break;
                case C.pay.SKU4:
                    payment(purchase);
                    break;
                case C.pay.SKU5:
                    payment(purchase);
                    break;
                case C.pay.SKU6:
                    payment(purchase);
                    break;
                case C.pay.SKU7:
                    payment(purchase);
                    break;
                case C.pay.SKU8:
                    payment(purchase);
                    break;
                case C.pay.SKU9:
                    payment(purchase);
                    break;
                case C.pay.SKU10:
                    payment(purchase);
                    break;
                case C.pay.SKU11:
                    payment(purchase);
                    break;
                case C.pay.SKU12:
                    payment(purchase);
                    break;
                case C.pay.SKU13:
                    payment(purchase);
                    break;
                case C.pay.SKU14:
                    payment(purchase);
                    break;
            }
            // 消耗掉已购买的商品
            mHelper.consumeAsync(purchase, mConsumeFinishedListener);
        }
    };
    /**
     * 向后台发送支付结果
     */
    private void payment(Purchase purchase) {//paytype 包月6
        String serviceId = mPayView.getServiceId();
        String payType = mPayView.getPayType();
        if(!TextUtils.isEmpty(serviceId)&&!TextUtils.isEmpty(payType)){
            ApiManager.payment(purchase,serviceId, payType, new IGetDataListener<String>() {
                @Override
                public void onResult(String s, boolean isEmpty) {
//                    if (!TextUtils.isEmpty(s) && "success".equals(s)) {
//                        // 后台支付成功，刷新用户商品信息
//
//                    } else {
//                        // 后台支付结果请求失败
//                    }
                    EventBus.getDefault().post(new PaySuccessEvent());
                }
                @Override
                public void onError(String msg, boolean isNetworkError) {
                    // 后台支付结果请求失败
                }
            });
        }
    }
    private void showSuccessTip() {
        mPayView.showTip(mContext.getString(R.string.pay_success));
    }
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (result.isSuccess()) {
                // 支付成功
                switch (purchase.getSku()) {
                    case C.pay.SKU1:
                        showSuccessTip();
                        break;
                    case C.pay.SKU2:
                        showSuccessTip();
                        break;
                    case C.pay.SKU3:
                        showSuccessTip();
                        break;
                    case C.pay.SKU4:
                        showSuccessTip();
                        break;
                    case C.pay.SKU5:
                        showSuccessTip();
                        break;
                    case C.pay.SKU6:
                        showSuccessTip();
                        break;
                    case C.pay.SKU7:
                        showSuccessTip();
                        break;
                    case C.pay.SKU8:
                        showSuccessTip();
                        break;
                    case C.pay.SKU9:
                        showSuccessTip();
                        break;
                    case C.pay.SKU10:
                        showSuccessTip();
                        break;
                    case C.pay.SKU11:
                        showSuccessTip();
                        break;
                    case C.pay.SKU12:
                        showSuccessTip();
                        break;
                    case C.pay.SKU13:
                        showSuccessTip();
                        break;
                    case C.pay.SKU14:
                        showSuccessTip();
                        break;
                }
            }
        }
    };
}
