package com.popo.video.ui.follow;

import android.content.Context;
import android.os.Parcelable;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.popo.video.R;
import com.popo.video.base.BaseTopBarActivity;
import com.popo.library.net.NetUtil;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.ui.follow.contract.FindUserContract;
import com.popo.video.ui.follow.presenter.FindUserPresenter;

import butterknife.BindView;

/**
 * 查找用户页面
 * Created by zhangdroid on 2017/5/27.
 */
public class FindUserActivity extends BaseTopBarActivity implements FindUserContract.IView,View.OnClickListener{
    @BindView(R.id.find_user_activity_et_id)
    EditText et_id;
    @BindView(R.id.find_user_activity_rl_root)
    LinearLayout mLlRoot;
    @BindView(R.id.find_user_activity_btn_find)
    Button btn_find;
    @BindView(R.id.find_activity_rl_back)
    RelativeLayout rl_back;
    private FindUserPresenter mFindUserPresenter;
    InputMethodManager imm;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_find_user;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.follow_find_user);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mFindUserPresenter=new FindUserPresenter(this);
        imm= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Override
    protected void setListeners() {
        btn_find.setOnClickListener(this);
        rl_back.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public String getRemoteId() {
        return et_id.getText().toString().trim();
    }


    @Override
    public void canGoToUserDetailActivity(String id) {
         mFindUserPresenter.goToUserDetailActivity(id);
    }

    @Override
    public void showIdOrMessageError(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.find_user_activity_btn_find:
                mFindUserPresenter.setOnclick();
                if (this.getCurrentFocus() != null) {
                    if (this.getCurrentFocus().getWindowToken() != null) {
                        imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                }
                break;
            case R.id.find_activity_rl_back:
                 finish();
                break;
        }

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    //触摸屏幕，可以让软键盘消失
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (this.getCurrentFocus() != null) {
                if (this.getCurrentFocus().getWindowToken() != null) {
                    imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
        return super.onTouchEvent(event);
    }
}
