package com.popo.video.ui.detail;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.library.dialog.LoadingDialog;
import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.R;
import com.popo.video.base.BaseAppCompatActivity;
import com.popo.video.common.RingManage;
import com.popo.video.common.TimeUtils;
import com.popo.video.common.YeMeiPopopUtil;
import com.popo.video.data.constant.DataDictManager;
import com.popo.video.data.model.NettyMessage;
import com.popo.video.data.model.VideoOrImage;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.event.RecordImageEvent;
import com.popo.video.event.SingleLoginFinishEvent;
import com.popo.video.event.YeMeiMessageEvent;
import com.popo.video.parcelable.ShortPlayParcelable;
import com.popo.video.ui.detail.banner.Banner;
import com.popo.video.ui.detail.banner.listener.OnBannerListener;
import com.popo.video.ui.detail.banner.loader.GlideImageLoader;
import com.popo.video.ui.detail.contract.EditInfoContract;
import com.popo.video.ui.detail.presenter.EditInfoPresenter;
import com.popo.video.ui.dialog.AgeSelectDialog;
import com.popo.video.ui.dialog.DialogUtil;
import com.popo.video.ui.dialog.OnDoubleDialogClickListener;
import com.popo.video.ui.dialog.OnEditDoubleDialogClickListener;
import com.popo.video.ui.dialog.OneWheelDialog;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;

/**
 * 编辑个人信息页面
 * Created by zhangdroid on 2017/5/27.
 */
public class EditInfoActivity extends BaseAppCompatActivity implements View.OnClickListener, EditInfoContract.IView {
    @BindView(R.id.edit_info_root_layout)
    RelativeLayout mLlRoot;//根布局
    @BindView(R.id.edit_info_rl_age_sex)
    RelativeLayout rl_sex;//根布局
    @BindView(R.id.edit_info_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.banner)
    Banner banner;

    @BindView(R.id.edit_info_rl_album)
    RelativeLayout rl_album;//相册
    @BindView(R.id.edit_info_base_rl_nickname)
    RelativeLayout rl_nickname;
    @BindView(R.id.edit_info_base_tv_nickname)
    TextView et_nickname;//昵称
    @BindView(R.id.edit_info_base_rl_height)
    RelativeLayout rl_height;
    @BindView(R.id.edit_info_base_tv_height)
    TextView tv_height;//身高

    @BindView(R.id.edit_info_base_rl_marital_status)
    RelativeLayout rl_marital_status;
    @BindView(R.id.edit_info_base_tv_marital_status)
    TextView tv_marital_status;//婚姻状况

    @BindView(R.id.edit_info_rl_weight)
    RelativeLayout rl_weight;
    @BindView(R.id.edit_info_detail_tv_weight)
    TextView tv_weight;
    @BindView(R.id.edit_info_rl_sign)
    RelativeLayout rl_sign;
    @BindView(R.id.edit_info_detail_tv_sign)
    TextView tv_sign;
    @BindView(R.id.edit_info_rl_toobar)
    RelativeLayout rl_toobar;
    @BindView(R.id.edit_info_tv_nickname)
    TextView tv_nickname;
    @BindView(R.id.tv_id)
    TextView tv_id;
    @BindView(R.id.edit_info_tv_age)
    TextView tv_age;
    @BindView(R.id.edit_info_base_tv_sex)
    TextView tv_sex;
    @BindView(R.id.edit_info_base_tv_age)
    TextView tv_age2;
    @BindView(R.id.edit_info_tv_height)
    TextView tv_height2;
    @BindView(R.id.edit_info_tv_status)
    TextView tv_status;
    @BindView(R.id.et_ownwords)
    EditText et_ownwords;
    @BindView(R.id.edit_info_base_rl_age)
    RelativeLayout rl_age;
    private EditInfoPresenter mEditInfoPresenter;
    InputMethodManager imm;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_edit_info;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected View getNoticeView() {
        return mLlRoot;
    }

    @Override
    protected void initViews() {
        mEditInfoPresenter = new EditInfoPresenter(this);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mEditInfoPresenter.getUserInfo();
        if (TimeUtils.timeIsPast()) {//判断页脚是否过期
            msgHandler.sendEmptyMessage(1);
        } else if (!TimeUtils.timeIsPast()) {
            msgHandler.sendEmptyMessage(2);
        }
    }

    @Override
    protected void setListeners() {
        rl_album.setOnClickListener(this);
        rl_nickname.setOnClickListener(this);
        rl_height.setOnClickListener(this);
        rl_marital_status.setOnClickListener(this);
        rl_weight.setOnClickListener(this);
        rl_sign.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_age.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        mEditInfoPresenter.setLocalInfo();
        mEditInfoPresenter.start();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    public void upBanner(List<VideoOrImage> list) {
        banner.update(list);
    }

    @Override
    public void startBanner(final List<VideoOrImage> list) {
        if (list != null && list.size() != 0) {

            banner.setOnBannerListener(new OnBannerListener() {
                @Override
                public void OnBannerClick(int position) {
                    if (list.get(position).isVideo() && !TextUtils.isEmpty(list.get(position).getVideoUrl())) {
                        LaunchHelper.getInstance().launch(mContext, ShortPlayActivity.class,
                                new ShortPlayParcelable(list.get(position).getVideoUrl(), list.get(position).getBitmapUrl()));
                    }
                }
            });
            banner.isAutoPlay(false).setImages(list).setImageLoader(new GlideImageLoader())
                    .start();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_info_rl_back:
                mEditInfoPresenter.upLoadMyInfo();
                break;
            case R.id.edit_info_rl_album:// 跳转到相册页面
                LaunchHelper.getInstance().launch(mContext, AlbumActivity.class);
                break;
            case R.id.edit_info_base_rl_nickname:// 昵称
                DialogUtil.showEditDoubleBtnDialog(getSupportFragmentManager(), mContext.getString(R.string.register_name), mContext.getString(R.string.video_input_hint), mContext.getString(R.string.positive), mContext.getString(R.string.cancel), true, new OnEditDoubleDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String content) {
                        if (!TextUtils.isEmpty(content.trim())) {
                            et_nickname.setText(content);
                            tv_nickname.setText(content);
                        }
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_base_rl_height:// 身高
                if (!TextUtils.isEmpty(UserPreference.getCountryId()) && UserPreference.getCountryId().equals("97")) {
                    DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), true, DataPreference.getInchList(), getString(R.string.complete_height), getString(R.string.positive), getString(R.string.cancel), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
                        @Override
                        public void onPositiveClick(View view, String selectedText) {
                            tv_height.setText(selectedText);
                        }

                        @Override
                        public void onNegativeClick(View view) {

                        }
                    });
                } else {
                    DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), true, DataPreference.getInchCmList(), getString(R.string.complete_height), getString(R.string.positive), getString(R.string.cancel), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
                        @Override
                        public void onPositiveClick(View view, String selectedText) {
                            tv_height.setText(selectedText);
                        }

                        @Override
                        public void onNegativeClick(View view) {

                        }
                    });
                }
                break;

            case R.id.edit_info_base_rl_marital_status:// 婚姻状况
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataPreference.getRelationshipMapValue(), mContext.getString(R.string.marraige), mContext.getString(R.string.positive), mContext.getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_marital_status.setText(selectedText);
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_rl_weight:// 体重
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataPreference.getWeight(), mContext.getString(R.string.edit_info_weight), mContext.getString(R.string.positive), mContext.getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_weight.setText(selectedText + "kg");
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_rl_sign:// 星座
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataDictManager.getSignList(), mContext.getString(R.string.complete_signs), mContext.getString(R.string.positive), mContext.getString(R.string.cancel), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_sign.setText(selectedText);
                    }

                    @Override
                    public void onNegativeClick(View view) {

                    }
                });
                break;
            case R.id.edit_info_base_rl_age:
                AgeSelectDialog.show(getSupportFragmentManager(), "22", new AgeSelectDialog.OnAgeSelectListener() {
                    @Override
                    public void onSelected(String age) {
                        tv_age.setText(age + mContext.getString(R.string.edit_info_years_old));
                        tv_age2.setText(age + mContext.getString(R.string.edit_info_years_old));
                    }
                });
                break;
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override

    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void setIntroducation(String introducation) {

    }


    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public String stringNickName() {
        return tv_nickname.getText().toString();
    }

    @Override
    public String stringHeight() {
        return tv_height.getText().toString();
    }

    @Override
    public String stringRelation() {
        return tv_marital_status.getText().toString();
    }

    @Override
    public String stringWeight() {
        return tv_weight.getText().toString();
    }

    @Override
    public String stringSign() {
        return tv_sign.getText().toString();
    }

    @Override
    public String stringAge() {
        return tv_age.getText().toString();
    }

    @Override
    public void isMale(boolean male) {
        if (male) {
            rl_sex.setBackgroundResource(R.drawable.icon_male);
            tv_sex.setText(mContext.getString(R.string.register_male));
        } else {
            rl_sex.setBackgroundResource(R.drawable.icon_famle);
            tv_sex.setText(mContext.getString(R.string.register_female));
        }
    }

    @Override
    public void getNickNmae(String name) {
        tv_nickname.setText(name);
        et_nickname.setText(name);
    }

    @Override
    public void getUserId(String id) {
        tv_id.setText("ID:" + id);
    }

    @Override
    public void getAge(String age) {
        tv_age.setText(age + mContext.getString(R.string.edit_info_years_old));
        tv_age2.setText(age + mContext.getString(R.string.edit_info_years_old));
    }

    @Override
    public void getHeight(String height) {
        tv_height.setText(height);
        tv_height2.setText(height);

    }

    @Override
    public void getMarriage(String marriage) {
        tv_marital_status.setText(marriage);
    }

    @Override
    public void getWeight(String weight) {
        tv_weight.setText(weight + "kg");
    }

    @Override
    public void getSign(String sign) {
        tv_sign.setText(sign);
    }

    @Override
    public void setStatus(String stauts) {
        tv_status.setText(stauts);
    }

    @Override
    public String stringOwnWords() {
        return et_ownwords.getText().toString().trim();
    }

    @Override
    public void getOwnWords(String ownWords) {
        if (!TextUtils.isEmpty(ownWords)) {
            et_ownwords.setText(ownWords);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        banner.releaseBanner();
    }

    //触摸屏幕，可以让软键盘消失
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (this.getCurrentFocus() != null) {
                if (this.getCurrentFocus().getWindowToken() != null) {
                    imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
        return super.onTouchEvent(event);
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }

    @Override
    public void onBackPressed() {
        DialogUtil.showDoubleBtnDialog(getSupportFragmentManager(), getString(R.string.edit_profile), getString(R.string.edit_profile_comfire), getString(R.string.confirm), getString(R.string.cancel), false, new OnDoubleDialogClickListener() {
            @Override
            public void onPositiveClick(View view) {
                mEditInfoPresenter.upLoadMyInfo();
            }

            @Override
            public void onNegativeClick(View view) {
                finish();
            }
        });
    }

    @Subscribe
    public void onEvent(RecordImageEvent event) {
        mEditInfoPresenter.getUserInfo();
    }

    @Subscribe
    public void onEvent(NettyMessage event) {
        msgHandler.sendEmptyMessage(1);
    }

    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    RingManage.getInstance().closeRing();
                    break;
            }
        }
    };

    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }
    @Subscribe
    public void onEvent(YeMeiMessageEvent evet){
        Message msg=new Message();
        msg.obj=evet;
        yeMeiHandler.sendMessage(msg);

    }
    private Handler yeMeiHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            YeMeiMessageEvent obj = (YeMeiMessageEvent) msg.obj;
            if(obj!=null){
                YeMeiPopopUtil.getInstance().showYeMei(EditInfoActivity.this,mLlRoot,obj,isFinishing());
            }
        }
    };
}
