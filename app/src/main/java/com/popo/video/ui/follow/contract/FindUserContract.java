package com.popo.video.ui.follow.contract;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

/**
 * Created by Administrator on 2017/6/15.
 */

public interface FindUserContract {
    interface IView extends BaseView {
        String getRemoteId();
        /**
         * 可以跳转对方详情页面
         */
        void canGoToUserDetailActivity(String id);
        /**
         * 出现ID错误或者查找不到的情况
         */
        void showIdOrMessageError(String msg);
    }

    interface IPresenter extends BasePresenter {
        /**
         * 跳转对方详情页面
         */
         void goToUserDetailActivity(String id);
        /**
         * 获取查找按钮的点击事件
         */
        void setOnclick();


    }
}
