package com.popo.video.ui.detail.banner.loader;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.popo.video.common.Util;

/**
 * Created by xuzhaole on 2018/3/1.
 */

public class GlideImageLoader extends ImageLoader {
    @Override
    public void displayImage(final Context context, final Object path, final ImageView imageView) {


        Glide.with(context)
                .load(path)
                .placeholder(Util.getDefaultImage())
                .error(Util.getDefaultImage())
                .centerCrop()
                .override(640,640)
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .priority(Priority.HIGH)
                .listener(new RequestListener<Object, GlideDrawable>() {

                    @Override
                    public boolean onException(Exception e, Object model, Target<GlideDrawable> target, boolean isFirstResource) {
                        displayImage(context, path, imageView);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, Object model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);
    }
}
