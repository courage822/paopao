package com.popo.video.ui.homepage.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.MyInfo;
import com.popo.video.data.model.UserDetail;
import com.popo.video.data.preference.SwitchPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.library.dialog.AlertDialog;
import com.popo.library.dialog.OnDialogClickListener;
import com.popo.library.util.LaunchHelper;
import com.popo.video.parcelable.PayParcelable;
import com.popo.video.ui.follow.FindUserActivity;
import com.popo.video.ui.homepage.contract.HomepageContract;
import com.popo.video.ui.pay.activity.RechargeActivity;

/**
 * Created by zhangdroid on 2017/5/31.
 */
public class HomepagePresenter implements HomepageContract.IPresenter {
    private HomepageContract.IView mHomepageView;
    private Context mContext;

    public HomepagePresenter(HomepageContract.IView view) {
        this.mHomepageView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }


    @Override
    public void addTabs() {
    }

    @Override
    public void isVipUser() {
        if (SwitchPreference.getVipPay()==1) {
            if(UserPreference.isAnchor()){
                LaunchHelper.getInstance().launch(mContext, FindUserActivity.class);
            }else{
                ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
                    @Override
                    public void onResult(MyInfo myInfo, boolean isEmpty) {
                        if(myInfo!=null) {
                            if (!TextUtils.isEmpty(myInfo.getIslj())){
                                UserPreference.setIslj(myInfo.getIslj());
                            }
                            UserDetail userDetail = myInfo.getUserDetail();
                            if(userDetail!=null){
                                String vipDays = userDetail.getVipDays();
                                if(!TextUtils.isEmpty(vipDays)&&vipDays.length()>0){
                                    int i = Integer.parseInt(vipDays);
                                    if(i==0){
                                        AlertDialog.showNoCanceled(mHomepageView.getManager(), "", mContext.getString(R.string.not_vip_not_find),
                                                mContext.getString(R.string.to_open), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                                                    @Override
                                                    public void onNegativeClick(View view) {
                                                    }
                                                    @Override
                                                    public void onPositiveClick(View view) {
                                                        // 充值
                                                        LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 0));
                                                    }
                                                }
                                        );
                                    }else{
                                        LaunchHelper.getInstance().launch(mContext, FindUserActivity.class);
                                    }

                                }else{
                                    LaunchHelper.getInstance().launch(mContext, FindUserActivity.class);
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {

                    }
                });

            }
        }else{
            LaunchHelper.getInstance().launch(mContext, FindUserActivity.class);
        }


    }

}
