package com.popo.video.ui.login;

import android.content.Context;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.data.model.AccountPwdInfo;
import com.popo.video.db.DbModle;
import com.popo.video.event.FinishEvent;
import com.popo.library.dialog.LoadingDialog;
import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.ui.dialog.FindPasswordDialog;
import com.popo.video.ui.register.RegisterActivity;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;

/**
 * 登录页面
 * Created by zhangdroid on 2017/5/12.
 */
public class LoginActivity extends BaseFragmentActivity implements View.OnClickListener, LoginContract.IView {
    @BindView(R.id.login_root)
    RelativeLayout mLlRoot;
    @BindView(R.id.login)
    Button mBtnLogin;
    @BindView(R.id.login_register)
    TextView mTvRegister;
    @BindView(R.id.find_password)
    TextView mTvFindPassword;
    @BindView(R.id.login_activity_et_account)
    EditText et_account;
    @BindView(R.id.login_activity_et_pwd)
    EditText et_pwd;
    @BindView(R.id.login_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.iv_more)
    ImageView iv_more;
    @BindView(R.id.rl_root)
    RelativeLayout rl_root;


    InputMethodManager imm;
    private LoginPresenter mLoginPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        mLoginPresenter = new LoginPresenter(this);
        mLoginPresenter.start();
        checkValid();
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Override
    protected void setListeners() {
        mBtnLogin.setOnClickListener(this);
        mTvRegister.setOnClickListener(this);
        mTvFindPassword.setOnClickListener(this);
        et_pwd.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        iv_more.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:// 登录
                mLoginPresenter.login();
                break;

            case R.id.login_register:// 跳转注册页面
                LaunchHelper.getInstance().launch(mContext, RegisterActivity.class);
                break;

            case R.id.find_password:// 忘记密码
                mLoginPresenter.findPassword();
                break;
            case R.id.login_activity_et_pwd:
                checkValid();
                break;
            case R.id.login_activity_rl_back:
                finish();
                break;
            case R.id.iv_more:
                final List<AccountPwdInfo> allAccountPwd = DbModle.getInstance().getUserAccountDao().getAllAccountPwd();
                if (allAccountPwd == null || allAccountPwd.size() == 0) {
                    return;
                }
                showPopupWindow(allAccountPwd);
                break;
        }
    }

    private void showPopupWindow(final List<AccountPwdInfo> allAccountPwd) {
        //设置contentView
        View contentView = LayoutInflater.from(LoginActivity.this).inflate(R.layout.popuplayout, null);
        final PopupWindow mPopWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        mPopWindow.setContentView(contentView);
        //设置各个控件的点击响应
        ListView listview = (ListView) contentView.findViewById(R.id.listview);
        String[] mItems = new String[allAccountPwd.size()];
        for (int i = 0; i < allAccountPwd.size(); i++) {
            mItems[i] = allAccountPwd.get(i).getAccount();
        }
        MyAdapter adapter = new MyAdapter(mContext, allAccountPwd);
        listview.setAdapter(adapter);

        mPopWindow.setFocusable(true);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mPopWindow.dismiss();
                setAccount(allAccountPwd.get(i).getAccount());
                setPassword(allAccountPwd.get(i).getPwd());
            }
        });
        //显示PopupWindow
        mPopWindow.showAsDropDown(rl_root, 50, 10, Gravity.CENTER);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void setAccount(String account) {
        et_account.setText(account);
    }

    @Override
    public void setPassword(String password) {
        et_pwd.setText(password);
    }

    @Override
    public String getAccount() {
        return et_account.getText().toString();
    }

    @Override
    public String getPassword() {
        return et_pwd.getText().toString();
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void showFindPwdDialog(String account, String password) {
        FindPasswordDialog.show((String) TextUtils.concat(getString(R.string.find_pwd_account, account), "\n",
                getString(R.string.find_pwd_password, password)), getSupportFragmentManager());
    }

    /**
     * 检测是否可以登录
     */
    private void checkValid() {
        mBtnLogin.setEnabled(true);
    }

    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }

    //触摸屏幕，可以让软键盘消失
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (this.getCurrentFocus() != null) {
                if (this.getCurrentFocus().getWindowToken() != null) {
                    imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
        return super.onTouchEvent(event);
    }
}
