package com.popo.video.ui.video;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.Vibrator;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.net.NetUtil;
import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.common.Util;
import com.popo.video.common.VideoHelper;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.event.FinishVideoActivity2;
import com.popo.video.event.InviteEvent2;
import com.popo.video.parcelable.VideoInviteParcelable;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 视频邀请页面（邀请/被邀请）(群发视频/语音)
 * Created by zhangdroid on 2017/5/27.
 */
public class VideoInviteActivity2 extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.video_invite_avatar)
    ImageView mIvAvatar;
    @BindView(R.id.video_invite_nickname)
    TextView mTvNickname;
    @BindView(R.id.video_invite_tip)
    TextView mTvVideoTip;
    @BindView(R.id.video_cancel)
    TextView mTvVideoCancel;
    @BindView(R.id.video_reject)
    TextView mTvVideoReject;
    @BindView(R.id.video_accept)
    TextView mTvVideoAccept;
    // 遮罩层
    @BindView(R.id.video_invite_avatar_big)
    ImageView mIvAvatarBig;
    @BindView(R.id.video_invite_mask)
    View mMaskView;
    // 相机预览
    @BindView(R.id.video_invite_preview)
    SurfaceView mSurfaceView;

    private VideoInviteParcelable mVideoInviteParcelable;
    private SurfaceHolder mSurfaceHolder;
    // 发起视频时的频道id，生成规则：邀请人guid+被邀请人guid
    private String mChannelId;
    Ringtone ringtone;
    private long lastClickTime = 0;
    private boolean flag = true;
    private boolean flag2 = false;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 3:
                    if (flag2) {
                        if (ringtone != null && ringtone.isPlaying()) {
                            ringtone.stop();
                            ringtone = null;
                        }
                    }
                    break;
                case 1:
                    if (flag) {

                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                        ringtone = RingtoneManager.getRingtone(VideoInviteActivity2.this, notification);
                        ringtone.play();
                        flag = false;
                        flag2 = true;
                    }
                    break;
                case 4:
                    if (flag2) {
                        if (ringtone != null && ringtone.isPlaying()) {
                            ringtone.stop();
                            ringtone = null;
                        }
                    }
                    finish();
                    break;
                case 5:
                    Toast.makeText(VideoInviteActivity2.this, getString(R.string.on_the_phone), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_video_invite;
    }

    @Override
    protected void setWakeAndLack() {
        super.setWakeAndLack();
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mVideoInviteParcelable = (VideoInviteParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        if (null != mVideoInviteParcelable) {
            ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(mVideoInviteParcelable.imgageUrl).transform(new CropCircleTransformation(mContext))
                    .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(mIvAvatar).build());
            mTvNickname.setText(mVideoInviteParcelable.nickname);
            if (mVideoInviteParcelable.type == 1) {
                mTvVideoTip.setText(mVideoInviteParcelable.isInvited ? getString(R.string.video_invited2) : getString(R.string.video_invite2));
            } else {
                mTvVideoTip.setText(mVideoInviteParcelable.isInvited ? getString(R.string.video_invited) : getString(R.string.video_invite));
            }
            mTvVideoCancel.setVisibility(mVideoInviteParcelable.isInvited ? View.GONE : View.VISIBLE);
            mTvVideoReject.setVisibility(mVideoInviteParcelable.isInvited ? View.VISIBLE : View.GONE);
            mTvVideoAccept.setVisibility(mVideoInviteParcelable.isInvited ? View.VISIBLE : View.GONE);
            if (mVideoInviteParcelable.isInvited) {
                ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(mVideoInviteParcelable.imgageUrl)
                        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(mIvAvatarBig).build());
                mIvAvatarBig.setVisibility(View.VISIBLE);
                mMaskView.setVisibility(View.VISIBLE);
                mSurfaceView.setVisibility(View.GONE);
                // 被邀请加入的频道id
                mChannelId = mVideoInviteParcelable.channelId;
                handler.sendEmptyMessage(1);
            } else {
                mIvAvatarBig.setVisibility(View.GONE);
                mMaskView.setVisibility(View.GONE);
                mSurfaceView.setVisibility(View.VISIBLE);
                // 生成channelId
                mChannelId = UserPreference.getId() + mVideoInviteParcelable.uId;
            }
        }
        handler.sendEmptyMessageDelayed(4, 50 * 1000);

    }

    @Override
    protected void setListeners() {
        mTvVideoCancel.setOnClickListener(this);
        mTvVideoReject.setOnClickListener(this);
        mTvVideoAccept.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.video_cancel:// 取消

                break;

            case R.id.video_reject:// 拒绝
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    shake(VideoInviteActivity2.this);
                    handler.sendEmptyMessage(3);
                    finish();
                }
                break;

            case R.id.video_accept:// 接受
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    shake(VideoInviteActivity2.this);
                    VideoHelper.immediatelyVideo(mChannelId, mVideoInviteParcelable.account, String.valueOf(mVideoInviteParcelable.uId), mVideoInviteParcelable.imgageUrl, mVideoInviteParcelable.type, VideoInviteActivity2.this, "2");
                    handler.sendEmptyMessage(3);
                }
                break;
        }
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.sendEmptyMessage(3);
    }

    @Subscribe
    public void onEvent(FinishVideoActivity2 event) {
        finish();
    }

    /**
     * 接通或者挂断震动一下
     *
     * @param context
     */
    public static void shake(Context context) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(88);
    }

    @Subscribe
    public void onEvent(InviteEvent2 event) {
        handler.sendEmptyMessage(5);
    }
}
