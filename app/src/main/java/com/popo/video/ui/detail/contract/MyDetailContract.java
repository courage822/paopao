package com.popo.video.ui.detail.contract;

import com.popo.video.data.model.UserPhoto;
import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

import java.util.List;

/**
 * Created by zhangdroid on 2017/6/2.
 */
public interface MyDetailContract {

    interface IView extends BaseView {
        void showLoading();

        void dismissLoading();

        void showNetworkError();

        /**
         * 设置用户照片墙
         *
         * @param list
         */
        void setUserPhotos(List<UserPhoto> list);

        /**
         * 设置用户昵称
         *
         * @param nickname 昵称
         */
        void setNickname(String nickname);

        /**
         * 设置用户性别
         *
         * @param isMale true表示男性，false表示女性
         */
        void setGender(boolean isMale);

        /**
         * 设置用户年龄、国家和城市
         *
         * @param age     年龄
         * @param country 国家
         * @param city    城市
         */
        void setUserInfo(String age, String country, String city);

        /**
         * 设置用户id
         *
         * @param id 用户id
         */
        void setUserId(String id);

        /**
         * 设置内心独白
         *
         * @param introducation 自我介绍
         */
        void setIntroducation(String introducation);

    }

    interface IPresenter extends BasePresenter {

        /**
         * 获取个人空间页信息
         */
        void getMyInfo();

        /**
         * 更新个人空间页信息
         */
        void updateMyInfo();

    }

}
