package com.popo.video.ui.message.presenter;

import android.content.Context;
import android.util.Log;

import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.ui.match.MatchAdapter;
import com.popo.video.ui.message.adapter.NewRecyclerAdapter;
import com.popo.video.ui.message.contract.ChatHistoryContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangdroid on 2017/7/6.
 */
public class ChatHistoryPresenter implements ChatHistoryContract.IPresenter {
    List<HuanXinUser> allAcount = null;
    private ChatHistoryContract.IView mChatHistoryView;
    private Context mContext;
    private NewRecyclerAdapter mAdapter;
    private MatchAdapter mMatchAdapter;


    public ChatHistoryPresenter(ChatHistoryContract.IView view) {
        this.mChatHistoryView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
        mAdapter = new NewRecyclerAdapter(mContext, UserPreference.isAnchor());
        mMatchAdapter = new MatchAdapter(null);
        mChatHistoryView.setAdapter(mAdapter, mMatchAdapter);
    }


    @Override
    public void loadConversationList() {
        mAdapter.reSet();
        // 查询未读消息数
        allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
        if (allAcount != null && allAcount.size() > 0) {

            List<HuanXinUser> matchList = new ArrayList<>();
            if (!UserPreference.isAnchor()) {
                List<HuanXinUser> priList = new ArrayList<>();
                List<HuanXinUser> videoList = new ArrayList<>();
                List<HuanXinUser> normalList = new ArrayList<>();
                for (HuanXinUser user : allAcount) {
                    if (user.getExtendType() == 3) {
                        priList.add(user);
                    } else if (user.getExtendType() == 2 || user.getExtendType() == 4) {
                        videoList.add(user);
                    } else if (user.getExtendType() == 10) {
                        matchList.add(user);
                    } else {
                        normalList.add(user);
                    }

                }
                Log.e("size", "videoList==" + videoList.size() + "&&priList==" + priList.size()
                        + "&&normalList==" + normalList.size());
                mAdapter.setVideoList(videoList);
                mAdapter.setPrivateList(priList);
                mAdapter.setNormalList(normalList);

            } else {
                List<HuanXinUser> normalList = new ArrayList<>();
                List<HuanXinUser> helloList = new ArrayList<>();

                for (HuanXinUser user : allAcount) {
                    if (user.getExtendType() == 8) {
                        helloList.add(user);
                    } else if (user.getExtendType() == 10) {
                        matchList.add(user);
                    } else {
                        normalList.add(user);
                    }
                }
                Log.e("size", "helloListsize" + helloList.size() + "normalsize" + normalList.size());
                mAdapter.setHellolList(helloList);
                mAdapter.setNormalList(normalList);
            }
            Log.e("size", "matchsize==" + (matchList == null ? "null" : matchList.size() + ""));
            if (matchList == null || matchList.size() == 0) {
                mChatHistoryView.hideMatch(true);
            } else {
                Log.e("size", "matchsize" + matchList.size());
                mChatHistoryView.hideMatch(false);
                mMatchAdapter.setNewData(matchList);
            }
            mChatHistoryView.showNoMore();
            mChatHistoryView.hideLoadMore();
        }
//        else {
//            mChatHistoryView.showNoMore();
//            mChatHistoryView.hideLoadMore();
//            mChatHistoryView.toggleShowEmpty(true, null);
//        }
        mChatHistoryView.hideRefresh(1);

    }

    @Override
    public void refresh() {
        mChatHistoryView.toggleShowEmpty(false, null);
        initloadData();
    }

    @Override
    public void loadMore() {
        mChatHistoryView.hideRefresh(1);
    }

    @Override
    public void initloadData() {
        if (allAcount != null && allAcount.size() > 0) {
            allAcount.clear();
            allAcount = null;
        }
        loadConversationList();
    }

    @Override
    public List<HuanXinUser> getAllAcount() {
        return allAcount;
    }
}
