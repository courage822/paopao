package com.popo.video.ui.homepage;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.ToastUtil;
import com.popo.library.util.Utils;
import com.popo.video.R;
import com.popo.video.common.CustomDialogAboutPay;
import com.popo.video.common.Util;
import com.popo.video.common.VideoHelper;
import com.popo.video.customload.BaseAdapterHelper;
import com.popo.video.customload.CommonAbsListViewAdapter;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.SearchUser;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.SwitchPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.parcelable.ChatParcelable;
import com.popo.video.parcelable.VideoInviteParcelable;
import com.popo.video.ui.chat.ChatActivity;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/8/18.
 */

public class ListFragmentBaseAdapter extends CommonAbsListViewAdapter<SearchUser> {
    private Context mContext;
    private FragmentManager mFragmentManager;
    private long prelongTim = 0;//定义上一次单击的时间

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
    }

    public void removeItem(int index) {
        List<SearchUser> list = getDataList();
        if (!Utils.isListEmpty(list)) {
            list.remove(index);
            notifyDataSetChanged();
        }
    }

    public ListFragmentBaseAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
        this.mContext = context;
    }

    @Override
    protected void convert(int position, BaseAdapterHelper helper, final SearchUser bean) {
        if (bean != null) {
            ImageView iv_avatar = (ImageView) helper.getView(R.id.list_fragment_item_iv);
            ViewGroup.LayoutParams params = iv_avatar.getLayoutParams();
            params.width =ViewGroup.LayoutParams.MATCH_PARENT;

            int screenHeight = DataPreference.getScreenHeight();

            if(screenHeight==2416){

            }
            if(screenHeight==2560){//小米3x

            }
            if(screenHeight==4709){//谷歌
                params.height= 900;
            }
            if(screenHeight==5040){

            }
            if(screenHeight==5760){//小米4
                params.height= 900;
            }
            if(screenHeight<2416){//华为

            }else if(screenHeight>2416&&screenHeight<2560){

            }else if(screenHeight>2560&&screenHeight<4709){

            }else if(screenHeight>4709&&screenHeight<5040){

            }else if(screenHeight>5040&&screenHeight<5760){

            }else if(screenHeight>5760){

            }

            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(bean.getIconUrlMiddle())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_avatar).build());
            TextView tv_nickname = (TextView) helper.getView(R.id.list_fragment_item_tv_nickname);

            TextView tv_age_state = (TextView) helper.getView(R.id.list_fragment_item_tv_age_and_state);
            int status = bean.getStatus();
            int onlineStatus = bean.getOnlineStatus();
            if (onlineStatus == -1) {
                tv_age_state.setText(bean.getAge() + mContext.getString(R.string.edit_info_years_old) + "  " + mContext.getString(R.string.not_online));
            } else {
                tv_age_state.setText(bean.getAge() + mContext.getString(R.string.edit_info_years_old) + "  " + mContext.getString(R.string.edit_info_status));
            }
            if (status == 3) {
                tv_age_state.setText(bean.getAge() + mContext.getString(R.string.edit_info_years_old) + "  " + mContext.getString(R.string.message_state_nodistrub));
            }
            if (PlatformPreference.getPlatformInfo().getFid().equals("30108")) {
                tv_nickname.setText(Util.chineseFontChanger(bean.getNickName()));
            }else{
                tv_nickname.setText(bean.getNickName());
            }
//            tv_age_state.setText(bean.get);
            //打招呼
            ImageView iv_sendMsg = (ImageView) helper.getView(R.id.list_fragment_item_iv_sendmessage);
            ImageView iv_sendgifts = (ImageView) helper.getView(R.id.list_fragment_item_iv_sendgifts);
            ImageView iv_sendVideo = (ImageView) helper.getView(R.id.list_fragment_item_iv_sendvideo);
            ImageView iv_sendVoice = (ImageView) helper.getView(R.id.list_fragment_item_iv_sendvoice);
            prelongTim = 0;
            iv_sendMsg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prelongTim == 0) {//第一次单击，初始化为本次单击的时间
                        prelongTim = (new Date()).getTime();
                        goToWriteMsg(bean);
                    } else {
                        long curTime = (new Date()).getTime();//本地单击的时间
                        if (curTime - prelongTim > 1000) {
                            goToWriteMsg(bean);
                        }
                        prelongTim = curTime;//当前单击事件变为上次时间
                    }


                }
            });

            iv_sendVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prelongTim == 0) {//第一次单击，初始化为本次单击的时间
                        prelongTim = (new Date()).getTime();
                        if (SwitchPreference.getAllPay() == -1) {
                            VideoHelper.startVideoInvite(new VideoInviteParcelable(false, bean.getUesrId(), bean.getAccount()
                                    , bean.getNickName(), bean.getIconUrlMininum(), 0, 0), mContext, "1");
                        } else {
                            goToViewPage(bean, 0);
                        }
                    } else {
                        long curTime = (new Date()).getTime();//本地单击的时间
                        if (curTime - prelongTim > 1000) {
                            if (SwitchPreference.getAllPay() == -1) {
                                VideoHelper.startVideoInvite(new VideoInviteParcelable(false, bean.getUesrId(), bean.getAccount()
                                        , bean.getNickName(), bean.getIconUrlMininum(), 0, 0), mContext, "1");
                            } else {
                                goToViewPage(bean, 0);
                            }
                        }
                        prelongTim = curTime;//当前单击事件变为上次时间
                    }

                }
            });

            iv_sendgifts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prelongTim == 0) {//第一次单击，初始化为本次单击的时间
                        prelongTim = (new Date()).getTime();
                        //发礼物
//                        CustomDialogAboutOther.giveGiftShow(mContext, String.valueOf(bean.getUesrId()), bean.getAccount(), bean.getIconUrlMininum(), bean.getNickName(), 1, false);
                    } else {
                        long curTime = (new Date()).getTime();//本地单击的时间
                        if (curTime - prelongTim > 1000) {
                            //发礼物
//                            CustomDialogAboutOther.giveGiftShow(mContext, String.valueOf(bean.getUesrId()), bean.getAccount(), bean.getIconUrlMininum(), bean.getNickName(), 1, false);
                        }
                        prelongTim = curTime;//当前单击事件变为上次时间
                    }

                }
            });

            iv_sendVoice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prelongTim == 0) {//第一次单击，初始化为本次单击的时间
                        prelongTim = (new Date()).getTime();
                        if (SwitchPreference.getAllPay() == -1) {
                            VideoHelper.startVideoInvite(new VideoInviteParcelable(false, bean.getUesrId(), bean.getAccount()
                                    , bean.getNickName(), bean.getIconUrlMininum(), 1, 0), mContext, "1");
                        } else {
                            goToViewPage(bean, 1);
                        }
                    } else {
                        long curTime = (new Date()).getTime();//本地单击的时间
                        if (curTime - prelongTim > 1000) {
                            if (SwitchPreference.getAllPay() == -1) {
                                VideoHelper.startVideoInvite(new VideoInviteParcelable(false, bean.getUesrId(), bean.getAccount()
                                        , bean.getNickName(), bean.getIconUrlMininum(), 1, 0), mContext, "1");
                            } else {
                                goToViewPage(bean, 1);
                            }
                        }
                        prelongTim = curTime;//当前单击事件变为上次时间
                    }

                }
            });
        }

    }

    private void sayHello2(final SearchUser searchUser) {
        ApiManager.sayHello(String.valueOf(searchUser.getUesrId()), new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel videoStop, boolean isEmpty) {
                ToastUtil.showShortToast(mContext, mContext.getString(R.string.hello_success));
                searchUser.setIsSayHello(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    private void goToWriteMsg(final SearchUser searchUser) {
        //跳转写信的界面
        if (searchUser != null) {
            if (UserPreference.isVip() || UserPreference.isAnchor()) {
                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(searchUser.getUesrId(),
                        searchUser.getAccount(), searchUser.getNickName(), searchUser.getIconUrlMininum(), 0));

            } else {
                CustomDialogAboutPay.dregeVipShow(mContext);
            }
        }
    }

    private void goToViewPage(final SearchUser searchUser, int type) {
        VideoHelper.startVideoInvite(new VideoInviteParcelable(false, searchUser.getUesrId(), searchUser.getAccount()
                , searchUser.getNickName(), searchUser.getIconUrlMininum(), type, 0), mContext, "1");
    }
}
