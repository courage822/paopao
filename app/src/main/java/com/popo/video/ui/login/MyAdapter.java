package com.popo.video.ui.login;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.data.model.AccountPwdInfo;

import java.util.List;

/**
 * Created by xuzhaole on 2017/12/18.
 */

public class MyAdapter extends BaseAdapter {
    private Context context;
    private List<AccountPwdInfo> list;
    public MyAdapter(Context context,List<AccountPwdInfo> list){
        this.context=context;
        this.list=list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder vh;
        if (convertView == null) {
            vh = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.spinner, null);
            vh.textview = (TextView) convertView.findViewById(R.id.tv);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        vh.textview.setText(list.get(position).getAccount() + "");
        return convertView;
    }
    static class ViewHolder {
        TextView textview;
    }
}
