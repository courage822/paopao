package com.popo.video.ui.video.presenter;

import android.content.Context;
import android.os.CountDownTimer;
import android.text.TextUtils;

import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.HostInfo;
import com.popo.video.data.model.MyInfo;
import com.popo.video.data.model.UserDetail;
import com.popo.video.data.preference.AnchorPreference;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.ui.video.contract.MassVideoOrVoiceContract;

/**
 * Created by WangYong on 2017/11/28.
 */

public class MassVideoOrVoicePresenter implements MassVideoOrVoiceContract.IPresenter {
    private MassVideoOrVoiceContract.IView mMassVideoOrVoice;
    private Context context;
    private CountDownTimer countDownTimer;
    private CountDownTimer countDownTimer1;

    public MassVideoOrVoicePresenter(MassVideoOrVoiceContract.IView mMassVideoOrVoice) {
        this.mMassVideoOrVoice = mMassVideoOrVoice;
        this.context = mMassVideoOrVoice.obtainContext();
    }

    @Override
    public void start() {

    }



    @Override
    public void initData(final String type) {
        if (type.equals("1")) {
            mMassVideoOrVoice.getAnchorPrice(AnchorPreference.getPrice());
        } else {
            mMassVideoOrVoice.getAnchorPrice(String.valueOf(AnchorPreference.getAudioPrice()));
        }


        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    if (!TextUtils.isEmpty(myInfo.getIslj())){
                        UserPreference.setIslj(myInfo.getIslj());
                    }
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        HostInfo hostInfo = userDetail.getHostInfo();
                        if (hostInfo != null) {
                            if (type.equals("2")) {
                                mMassVideoOrVoice.getSendNum(hostInfo.getTotalSendCallVideo());
                            } else {
                                mMassVideoOrVoice.getSendNum(hostInfo.getTotalSendCallAudio());
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });

    }

    @Override
    public void startCountTime() {
        long time = DataPreference.getTime();
        if (time == 0) {
            DataPreference.saveTime(System.currentTimeMillis());
            countDownTimer = new CountDownTimer(120000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mMassVideoOrVoice.getSendTime(millisUntilFinished);
                    if (millisUntilFinished <= 60000) {
                        mMassVideoOrVoice.clickStop();
                    }
                    if (DataPreference.getTime() == 0) {
                        countDownTimer.onFinish();
                        countDownTimer.cancel();
                    }
                }

                @Override
                public void onFinish() {
                    DataPreference.saveTime(0);
                    mMassVideoOrVoice.timeOnFinish();
                    mMassVideoOrVoice.finishActivity();
                }
            };
            countDownTimer.start();
        } else {
            long time1 = System.currentTimeMillis() - time;
            if (time1 > 1000 && time1 < 120000) {
                countDownTimer1 = new CountDownTimer(120000 - time1, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        mMassVideoOrVoice.getSendTime(millisUntilFinished);
                        if (millisUntilFinished <= 60000) {
                            mMassVideoOrVoice.clickStop();
                        }
                        if (DataPreference.getTime() == 0) {
                            countDownTimer1.onFinish();
                            countDownTimer1.cancel();
                        }
                    }

                    @Override
                    public void onFinish() {
                        DataPreference.saveTime(0);
                        mMassVideoOrVoice.timeOnFinish();
                        mMassVideoOrVoice.finishActivity();
                    }
                };
                countDownTimer1.start();
            }
        }
    }
}
