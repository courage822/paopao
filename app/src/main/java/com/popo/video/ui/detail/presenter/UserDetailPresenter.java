package com.popo.video.ui.detail.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.popo.library.dialog.AlertDialog;
import com.popo.library.dialog.OnDialogClickListener;
import com.popo.library.util.LaunchHelper;
import com.popo.video.R;
import com.popo.video.common.CustomDialogAboutPay;
import com.popo.video.common.ParamsUtils;
import com.popo.video.common.Util;
import com.popo.video.common.VideoHelper;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.CheckStatus;
import com.popo.video.data.model.HostInfo;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.model.MatchInfo;
import com.popo.video.data.model.TUserVideoShow;
import com.popo.video.data.model.UserBase;
import com.popo.video.data.model.UserDetail;
import com.popo.video.data.model.UserDetailforOther;
import com.popo.video.data.model.UserMend;
import com.popo.video.data.model.UserPhoto;
import com.popo.video.data.model.VideoOrImage;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.event.SayHelloEvent;
import com.popo.video.parcelable.BigPhotoParcelable;
import com.popo.video.parcelable.ChatParcelable;
import com.popo.video.parcelable.MatchParcelable;
import com.popo.video.parcelable.VideoInviteParcelable;
import com.popo.video.parcelable.VideoShowParcelable;
import com.popo.video.ui.chat.ChatActivity;
import com.popo.video.ui.detail.contract.UserDetailContract;
import com.popo.video.ui.match.MatchActivity;
import com.popo.video.ui.personalcenter.AuthenticationActivity;
import com.popo.video.ui.personalcenter.VideoShowActivity;
import com.popo.video.ui.photo.BigPhotoActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/6/7.
 */

public class UserDetailPresenter implements UserDetailContract.IPresenter {
    private UserDetailContract.IView mUserDetailView;
    private Context mContext;
    private UserBase userBase;
    private int pos = 0;//主播的审核状态判断
    private int showStatus = 0;

    public UserDetailPresenter(UserDetailContract.IView mUserDetailView) {
        this.mUserDetailView = mUserDetailView;
        this.mContext = mUserDetailView.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void getUserInfoData() {
        mUserDetailView.showLoading();
        mUserDetailView.meIsAuthor(UserPreference.isAnchor());
        ApiManager.getUserInfo(mUserDetailView.getUserId(), new IGetDataListener<UserDetailforOther>() {
            @Override
            public void onResult(UserDetailforOther userDetailforOther, boolean isEmpty) {
                if (userDetailforOther != null) {
                    UserDetail userDetail = userDetailforOther.getUserDetail();
                    if (userDetail != null) {
                        List<VideoOrImage> list = new ArrayList<VideoOrImage>();
                        List<TUserVideoShow> userVideoShows = userDetail.getUserVideoShows();
                        if (userVideoShows != null && userVideoShows.size() != 0) {
                            TUserVideoShow tUserVideoShow = userVideoShows.get(0);
                            list.add(new VideoOrImage(tUserVideoShow.getThumbnailUrl(), true, tUserVideoShow.getVideoUrl()));
                        }
                        UserBase userBase1 = userDetail.getUserBase();
                        if (userBase1 != null) {
                            userBase = userBase1;
                            list.add(new VideoOrImage(userBase1.getIconUrlMiddle(), false, null));
                            if (PlatformPreference.getPlatformInfo().getFid().equals("30108")) {
                                Log.e("AAAAAAA", "onResult: 11111111111111111111111111"+userBase1.getNickName());
                                mUserDetailView.setNickName(Util.chineseFontChanger(userBase1.getNickName()));
                            }else{
                                Log.e("AAAAAAA", "onResult: 222222222222222"+userBase1.getNickName());
                                mUserDetailView.setNickName(userBase1.getNickName());
                            }
                            mUserDetailView.setAge(userBase1.getAge() + mContext.getString(R.string.edit_info_years_old));
                            mUserDetailView.setSign(ParamsUtils.getSignValue(String.valueOf(userBase1.getSign())));
                            mUserDetailView.setUserId(userBase1.getAccount());
                            mUserDetailView.setAvatar(userBase1.getIconUrlMininum());

                            int userType = userBase1.getUserType();
                            if (userType == 1) {
                                mUserDetailView.isAnchor();
                                int gender = userBase1.getGender();
                                if (gender == 0) {
                                    mUserDetailView.setSex(mContext.getString(R.string.register_male));
                                } else {
                                    mUserDetailView.setSex(mContext.getString(R.string.register_female));
                                }
                            } else {
                                int gender = userBase1.getGender();
                                if (gender == 0) {
                                    mUserDetailView.setSex(mContext.getString(R.string.register_male));
                                    mUserDetailView.isMail(true);
                                } else {
                                    mUserDetailView.setSex(mContext.getString(R.string.register_female));
                                    if (userBase1.getUserType() == 0) {
                                        mUserDetailView.isMail(false);
                                    }
                                }
                            }
                        }


                        List<UserPhoto> userPhotos = userDetail.getUserPhotos();
                        if (userPhotos != null && userPhotos.size() > 0) {
                            for (UserPhoto userPhoto : userPhotos) {
                                list.add(new VideoOrImage(userPhoto.getFileUrlMiddle(), false, null));
                            }
                        }
                        mUserDetailView.startBanner(list);
                        int isSayHello = userDetail.getIsSayHello();
                        if (isSayHello == 1) {
                            mUserDetailView.isSayHello();
                        }
                        UserMend userMend = userDetail.getUserMend();
                        if (userMend != null) {
                            String isLogin = userMend.getIsLogin();
                            if (!TextUtils.isEmpty(isLogin) && isLogin.length() > 0) {
                                int status = Integer.parseInt(isLogin);
                                switch (status) {
                                    case 1:
                                        mUserDetailView.getStatus(mContext.getString(R.string.edit_info_status));
                                        break;
                                    case -1:
                                        mUserDetailView.getStatus(mContext.getString(R.string.not_online));
                                        break;
                                }

                            }
                            if (!TextUtils.isEmpty(userMend.getHeight())) {
                                if (!TextUtils.isEmpty(UserPreference.getCountryId()) && UserPreference.getCountryId().equals("97")) {
                                    mUserDetailView.setHeight(DataPreference.getInchCmByCm(userMend.getHeight()));
                                } else {
                                    mUserDetailView.setHeight(userMend.getHeight() + "cm");
                                }
                            } else {
                                if (!TextUtils.isEmpty(UserPreference.getCountryId()) && UserPreference.getCountryId().equals("97")) {
                                    mUserDetailView.setHeight(DataPreference.getInchCmByCm("173"));
                                } else {
                                    mUserDetailView.setHeight("173cm");
                                }
                            }

                            if (!TextUtils.isEmpty(userMend.getRelationshipId()) && !userMend.getRelationshipId().equals("31")) {
                                mUserDetailView.setMerital_status(DataPreference.getValueByKey(userMend.getRelationshipId(), 1));
                            } else {
                                mUserDetailView.setMerital_status(mContext.getString(R.string.keep_secret));
                            }

                            if (!TextUtils.isEmpty(userMend.getWeight())) {
                                mUserDetailView.setWeight(userMend.getWeight() + "kg");
                            } else {
                                mUserDetailView.setWeight(mContext.getString(R.string.keep_secret));

                            }
                        }
                        HostInfo hostInfo = userDetail.getHostInfo();
                        if (hostInfo != null) {
                            int price = (int) hostInfo.getPrice();
                            mUserDetailView.setAnchorPrice(price);
                        } else {
                            mUserDetailView.setAnchorPrice(0);

                        }
                        mUserDetailView.getFollowOrUnFollow(userDetail);

                    }
                }
                mUserDetailView.dismissLoading();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (mUserDetailView != null) {
//                    mUserDetailView.dismissLoading();
                }
                if (isNetworkError) {
                    mUserDetailView.showNetworkError();
                } else {
//                    mUserDetailView.showTip(mContext.getString(R.string.load_fail));
                }
            }
        });
    }

    //关注
    @Override
    public void follow(String remoteUid) {
        ApiManager.follow(remoteUid, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                mUserDetailView.followSucceed();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (isNetworkError) {
                    mUserDetailView.showNetworkError();
                } else {
                    //   mUserDetailView.showTip(msg);
                }
            }
        });
    }

    //取消关注
    @Override
    public void unFollow(String remoteUid) {
        ApiManager.unFollow(remoteUid, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                mUserDetailView.followSucceed();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (isNetworkError) {
                    mUserDetailView.showNetworkError();
                } else {
                    mUserDetailView.showTip(msg);
                }
            }
        });
    }


    @Override
    public void goVideoCallPage() {
        if (userBase != null) {
            if (UserPreference.isAnchor() && userBase.getUserType() == 1) {
//                mUserDetailView.innerAnchor(mContext.getString(R.string.user_detail_other_is_author));
                //跳转视频聊天的界面
                VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                        , userBase.getNickName(), userBase.getIconUrlMininum(), 0, 0), mContext, "1");
            } else {
                if (!TextUtils.isEmpty(userBase.getStatus())) {
                    //跳转视频聊天的界面
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                            , userBase.getNickName(), userBase.getIconUrlMininum(), 0, 0), mContext, "1");
                }
            }
        }
    }

    @Override
    public void goWriteMessagePage() {
        //跳转写信的界面
        if (userBase != null) {
            if (UserPreference.isAnchor()) {
                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(userBase.getGuid(),
                        userBase.getAccount(), userBase.getNickName(), userBase.getIconUrlMininum(), 0));
            } else if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
                if (pos == 0) {//审核中
                    if (showStatus == 1) {//完整
                        Toast.makeText(mContext, mContext.getString(R.string.under_review), Toast.LENGTH_SHORT).show();
                    } else if (showStatus == -1) {//不完整
                        AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "", mContext.getString(R.string.chat_need_real),
                                mContext.getString(R.string.person_authentication), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                                    @Override
                                    public void onNegativeClick(View view) {
                                    }

                                    @Override
                                    public void onPositiveClick(View view) {
                                        LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
                                    }
                                }
                        );
                    }
                } else {
                    AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "", mContext.getString(R.string.chat_need_real),
                            mContext.getString(R.string.person_authentication), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                                @Override
                                public void onNegativeClick(View view) {
                                }

                                @Override
                                public void onPositiveClick(View view) {
                                    LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                                }
                            }
                    );
                }
            } else {
                if (UserPreference.isVip() || UserPreference.isAnchor()) {
                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(userBase.getGuid(),
                            userBase.getAccount(), userBase.getNickName(), userBase.getIconUrlMininum(), 0));
                } else {
                    CustomDialogAboutPay.dregeVipShow(mContext);
                }
            }
        }
    }

    @Override
    public void goToAvatarViewPager() {
        List<String> list_photo = new ArrayList<>();
        if (userBase != null) {
            list_photo.add(userBase.getIconUrlMiddle());
        }
        if (list_photo != null && list_photo.size() > 0) {
            // 点击图片查看大图
            LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(0,
                    list_photo));
        }
    }

    @Override
    public void sayHelloClick() {
        if (userBase != null && !TextUtils.isEmpty(String.valueOf(userBase.getGuid()))) {
            ApiManager.sayHelloMatcher(String.valueOf(userBase.getGuid()), new IGetDataListener<MatchInfo>() {
                @Override
                public void onResult(MatchInfo matchInfo, boolean isEmpty) {
                    EventBus.getDefault().post(new SayHelloEvent());
                    mUserDetailView.isSayHello();
                    if (matchInfo.getIsMatcher().equals("1")){
                        UserBase matcherUser = matchInfo.getMatcherUser();
                        if (matcherUser != null && !TextUtils.isEmpty(matcherUser.getAccount())){
                            HuanXinUser user = new HuanXinUser(matcherUser.getGuid()+"", matcherUser.getNickName(),
                                    matcherUser.getIconUrlMininum(), matcherUser.getAccount(), "1",
                                    mContext.getString(R.string.match_success), 0, System.currentTimeMillis()+"", 10);
                            DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
                            LaunchHelper.getInstance().launch(mContext, MatchActivity.class,
                                    new MatchParcelable(matcherUser.getGuid(),matcherUser.getAccount(),
                                            matcherUser.getNickName(),matcherUser.getIconUrlMininum()));
                        }
                    }
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {

                }
            });
        }
    }

    @Override
    public void sendVideoInvite(String remoteId) {
        Log.e("AAAAAAA", "sendVideoInvite: 11111111111111111111111111");
        if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
            Log.e("AAAAAAA", "sendVideoInvite: 222222222222222222222222");
            if (pos == 0) {//审核中
                if (showStatus == 1) {//完整
                    Toast.makeText(mContext, mContext.getString(R.string.under_review), Toast.LENGTH_SHORT).show();
                } else if (showStatus == -1) {//不完整
                    AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "", mContext.getString(R.string.invite_video_auth),
                            mContext.getString(R.string.person_authentication), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                                @Override
                                public void onNegativeClick(View view) {
                                }

                                @Override
                                public void onPositiveClick(View view) {
                                    LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
                                }
                            }
                    );
                }
            } else {
                AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "", mContext.getString(R.string.invite_video_auth),
                        mContext.getString(R.string.person_authentication), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                            @Override
                            public void onNegativeClick(View view) {
                            }

                            @Override
                            public void onPositiveClick(View view) {
                                LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                            }
                        }
                );
            }
        } else {
            if (UserPreference.getStatus().equals("3")) {
                Toast.makeText(mContext, mContext.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
            } else {
                if (userBase != null) {
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                            , userBase.getNickName(), userBase.getIconUrlMininum(), 0, 0), mContext, "1");
                }
            }
        }
    }

    @Override
    public void sendVoiceInvite(String remoteId) {//语音邀请
        if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
            if (pos == 0) {//审核中
                if (showStatus == 1) {//完整
                    Toast.makeText(mContext, mContext.getString(R.string.under_review), Toast.LENGTH_SHORT).show();
                } else if (showStatus == -1) {//不完整
                    AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "",mContext.getString(R.string.invite_audio_auth),
                            mContext.getString(R.string.person_authentication), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                                @Override
                                public void onNegativeClick(View view) {
                                }

                                @Override
                                public void onPositiveClick(View view) {
                                    LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
                                }
                            }
                    );

                }
            } else {
                AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "", mContext.getString(R.string.invite_video_auth),
                        mContext.getString(R.string.person_authentication), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                            @Override
                            public void onNegativeClick(View view) {
                            }

                            @Override
                            public void onPositiveClick(View view) {
                                LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                            }
                        }
                );
            }

        } else {
            if (UserPreference.getStatus().equals("3")) {
                Toast.makeText(mContext, mContext.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
            } else {
                if (userBase != null) {
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                            , userBase.getNickName(), userBase.getIconUrlMininum(), 1, 0), mContext, "1");
                }
            }
        }
    }

    @Override
    public void getCheckStatus() {
        ApiManager.getAuthorCheckStatus(new IGetDataListener<CheckStatus>() {
            @Override
            public void onResult(CheckStatus checkStatus, boolean isEmpty) {
                if (checkStatus != null) {
                    pos = checkStatus.getAuditStatus();
                    showStatus = checkStatus.getShowStatus();
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

}
