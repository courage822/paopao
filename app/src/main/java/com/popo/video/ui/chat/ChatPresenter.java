package com.popo.video.ui.chat;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.text.TextUtils;
import android.view.MotionEvent;

import com.hyphenate.chat.EMMessage;
import com.popo.library.util.FileUtil;
import com.popo.library.util.LogUtil;
import com.popo.library.util.Utils;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.common.CustomDialogAboutPay;
import com.popo.video.common.HyphenateHelper;
import com.popo.video.common.RecordUtil;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.BlockBean;
import com.popo.video.data.model.ChatStatus;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.ui.photo.GetPhotoActivity;

import java.io.File;
import java.util.List;

/**
 * Created by zhangdroid on 2017/6/27.
 */
public class ChatPresenter implements ChatContract.IPresenter {
    private static final String TAG = ChatPresenter.class.getSimpleName();
    private ChatContract.IView mChatView;
    private Context mContext;
    // 接收方用户account
    private String mAccount;
    // 接收方用户guid
    private long mGuid;
    // 消息业务扩展类型
    private int mExtendType;
    // 录音文件存放目录
    private String mRecordDirectory;
    // 录音文件路径
    private String mRecordOutputPath;
    // 是否正在录音
    private boolean mIsRecording;
    // 录音是否被取消
    private boolean mIsRecordCanceled;
    // 录音时长（毫秒）
    private long mRecordDuration;
    // 录音计时刷新间隔（毫秒）
    private final long mRefreshInterval = 250;
    // 每页消息数
    private final int mPageSize = 20;
    private boolean canSendMsg = true;
    private String userName = "";
    private String userPic = "";
    private boolean isBlock = true;
    private int sendMsgNum=0;//0为可以发送消息，1为不可以发送消息
    public ChatPresenter(ChatContract.IView view, String account, long guid, String name, String pic, int extendType) {
        this.mChatView = view;
        this.mContext = view.obtainContext();
        this.mAccount = account;
        this.mGuid = guid;
        this.userName = name;
        this.userPic = pic;
        this.mExtendType = extendType;

    }

    @Override
    public void start() {
        if (UserPreference.isVip() || UserPreference.isAnchor() || mGuid == 10000) {
            canSendMsg = true;
            sendMsgNum=0;
        } else {
            ApiManager.getIsCanSendMsg(String.valueOf(mGuid), new IGetDataListener<ChatStatus>() {
                @Override
                public void onResult(ChatStatus chatStatus, boolean isEmpty) {
                    if (chatStatus!=null) {
                        if (chatStatus.getPayIntercept()==0) {
                            canSendMsg=true;
                            sendMsgNum=0;
                        }else{
                            canSendMsg = false;
                            sendMsgNum=1;
                        }
                    }
                }
                @Override
                public void onError(String msg, boolean isNetworkError) {
                }
            });
        }
        // 录音文件临时存放目录，内部存储，不需要权限
        mRecordDirectory = FileUtil.getExternalFilesDir(mContext, Environment.DIRECTORY_MUSIC) + File.separator;
    }

    @Override
    public void finish() {
//        mChatView = null;
//        mContext = null;
        // 释放录音和播放器资源
        RecordUtil.getInstance().releaseRecord();
        if (RecordUtil.getInstance().isPlaying()) {// 如果正在播放语音，需要停止
            RecordUtil.getInstance().stop();
        }
        RecordUtil.getInstance().release();
    }

    @Override
    public void msgRead(String guid) {
        ApiManager.msgRead(guid, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    @Override
    public void isBlock() {
        ApiManager.getIsBlock(String.valueOf(mGuid), new IGetDataListener<BlockBean>() {
            @Override
            public void onResult(BlockBean blockBean, boolean isEmpty) {
                if (blockBean != null) {
                    if (blockBean.getIsBlock() == 0) {//未拉黑
                    } else {//已拉黑
                        isBlock = false;
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    @Override
    public void handleAsyncTask(Message message) {
        switch (message.what) {
            case C.message.MSG_TYPE_TIMER:// 计时
                if (mIsRecording) {
                    mRecordDuration += mRefreshInterval;
                    // 录音超过1分钟自动发送
                    if (mRecordDuration > 60_000) {
                        stopRecordAndSend();
                    } else {
                        mChatView.sendMessage(mRefreshInterval, C.message.MSG_TYPE_TIMER);
                    }
                }
                break;
            case C.message.MSG_TYPE_DELAYED:// 延时结束录音
                RecordUtil.getInstance().stopRecord();
                mRecordDuration = 0;
                break;

            case C.message.MSG_TYPE_INIT:
                List<EMMessage> list = (List<EMMessage>) message.obj;
                if (Utils.isListEmpty(list)) {
                    mChatView.toggleShowEmpty(true, null);
                } else {
                    if (mChatView != null) {
                        mChatView.getChatAdapter().bind(list);

                    }
                }
                mChatView.hideRefresh();
                mChatView.scroollToBottom();
                break;
            case C.message.MSG_TYPE_ADD:
                List<EMMessage> adapterDataList = mChatView.getChatAdapter().getAdapterDataList();
                if (adapterDataList != null && adapterDataList.size() > 0) {
                    String msgId = adapterDataList.get(0).getMsgId();
                    List<EMMessage> emMessages = HyphenateHelper.getInstance().addConversation(mAccount, msgId, mPageSize);
                    if (emMessages != null && emMessages.size() > 0) {
                        for (int i = emMessages.size() - 1; i >= 0; i--) {
                            mChatView.getChatAdapter().insertItem(0, emMessages.get(i));
                        }
                    }
                }

                mChatView.hideRefresh();
                break;
            case C.message.MSG_TYPE_LOAD_MORE:
                List<EMMessage> emMessagesMore = (List<EMMessage>) message.obj;
                if (!Utils.isListEmpty(emMessagesMore)) {

                    mChatView.getChatAdapter().appendToList(emMessagesMore);
                }
                mChatView.hideRefresh();
                break;
            case C.message.MSG_TYPE_SEND_TXT:
                if (!TextUtils.isEmpty(mAccount)) {

                    EMMessage txtSendMessage = EMMessage.createTxtSendMessage(mChatView.getInputText(), mAccount);
                    Message msg = Message.obtain();
                    msg.what = C.message.MSG_TYPE_UPDATE;
                    msg.obj = txtSendMessage;
                    mChatView.sendMessage(msg);

                    HyphenateHelper.getInstance().sendTextMessage(mAccount, mChatView.getInputText(),
                            new HyphenateHelper.OnMessageSendListener() {
                                @Override
                                public void onSuccess(EMMessage emMessage) {
                                }

                                @Override
                                public void onError() {
                                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                                }
                            });
                } else {
                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                }
                mChatView.clearInput();
                break;
            case C.message.MSG_TYPE_SEND_VOICE:
                if (!TextUtils.isEmpty(mAccount)) {

                    EMMessage txtSendMessage = EMMessage.createVoiceSendMessage(mRecordOutputPath, (int) (mRecordDuration / 1000), mAccount);
                    Message msg = Message.obtain();
                    msg.what = C.message.MSG_TYPE_UPDATE;
                    msg.obj = txtSendMessage;
                    mChatView.sendMessage(msg);

                    HyphenateHelper.getInstance().sendVoiceMessage(mAccount, mRecordOutputPath, (int) (mRecordDuration / 1000),
                            new HyphenateHelper.OnMessageSendListener() {
                                @Override
                                public void onSuccess(EMMessage emMessage) {
                                }

                                @Override
                                public void onError() {
                                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                                }
                            });
                } else {
                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                }
                break;
            case C.message.MSG_TYPE_SEND_IMAGE:
                if (!TextUtils.isEmpty(mAccount)) {

                    String path = (String) message.obj;
                    EMMessage imageSendMessage = EMMessage.createImageSendMessage(path, false, mAccount);
                    Message msg = Message.obtain();
                    msg.what = C.message.MSG_TYPE_UPDATE;
                    msg.obj = imageSendMessage;
                    mChatView.sendMessage(msg);

                    HyphenateHelper.getInstance().sendImageMessage(mAccount, (String) message.obj,
                            new HyphenateHelper.OnMessageSendListener() {
                                @Override
                                public void onSuccess(EMMessage emMessage) {
                                }

                                @Override
                                public void onError() {
                                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                                }
                            });
                } else {
                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                }
                break;
            case C.message.MSG_TYPE_UPDATE:// 更新消息
                ChatAdapter chatAdapter = mChatView.getChatAdapter();
                if (chatAdapter.getItemCount() == 0) {
                    mChatView.toggleShowEmpty(false, null);
                }
                EMMessage emMessage = (EMMessage) message.obj;
                if (null != chatAdapter && null != emMessage) {
                    chatAdapter.insertItem(chatAdapter.getItemCount(), emMessage);
                }
                mChatView.scroollToBottom();
                break;
            case 100:
                mChatView.scroollToBottom();
                break;
        }
    }

    @Override
    public void initChatConversation() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message message = Message.obtain();
                message.what = C.message.MSG_TYPE_INIT;
                message.obj = HyphenateHelper.getInstance().initConversation(mAccount, mPageSize);
                mChatView.sendMessage(message);
            }
        }).start();
    }

    @Override
    public void refresh() {
        // 加载当前消息列表之前的消息
        LogUtil.i(TAG, "refresh()");

        // 当前显示聊天记录列表条数
        final int listCount = mChatView.getChatAdapter().getItemCount();
        // 全部聊天记录条数
        final int allCount = HyphenateHelper.getInstance().getAllMsgCount(mAccount);
        if (listCount < allCount) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    int pageSize = mPageSize;
                    if (allCount - listCount < 20) {
                        pageSize = allCount - listCount;
                    }
                    Message message = Message.obtain();
                    message.what = C.message.MSG_TYPE_LOAD_MORE;
                    message.obj = HyphenateHelper.getInstance().loadMoreMessages(mAccount, pageSize);
                    mChatView.sendMessage(message);
                }
            }).start();
        } else {
            mChatView.hideRefresh();
        }
    }

    @Override
    public void sendTextMessage() {
        if (!TextUtils.isEmpty(mChatView.getInputText())) {
            if (canSendMsg&&sendMsgNum==0) {
                ApiManager.interruptText(mGuid, mChatView.getInputText(), new IGetDataListener<BaseModel>() {
                    @Override
                    public void onResult(BaseModel baseModel, boolean isEmpty) {
                        if (baseModel.getIsSucceed().equals("-31")) {

                            mChatView.showInterruptDialog(mContext.getString(R.string.chat_who_interrupt));
                        } else if (baseModel.getIsSucceed().equals("-32")) {
                            mChatView.showInterruptDialog(mContext.getString(R.string.chat_interrupt_who));

                        }
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
//                    mChatView.showInterruptDialog(mContext.getString(R.string.message_interrupt));
                    }
                });
                if (isBlock) {
                    mChatView.sendMessage(0, C.message.MSG_TYPE_SEND_TXT);
                    saveDataToSqlit(userName, String.valueOf(mGuid), mAccount, userPic, mChatView.getInputText(), String.valueOf(System.currentTimeMillis()), mExtendType);
                }
                if(!UserPreference.isAnchor()){
                    if(!UserPreference.isVip()&&(mGuid != 10000)){
                        sendMsgNum=1;
                    }else if(!UserPreference.isVip()&&(mGuid == 10000)){
                        sendMsgNum=0;
                    }
                }
            } else {
                CustomDialogAboutPay.dregeVipShow(mContext);
            }

        }
    }

    @Override
    public void sendImageMessage(final boolean isTakePhoto) {
        GetPhotoActivity.toGetPhotoActivity(mContext, isTakePhoto, new GetPhotoActivity.OnGetPhotoPathListener() {
            @Override
            public void getSelectedPhotoPath(final String path) {

                if (!TextUtils.isEmpty(path)) {
                    if (canSendMsg&&sendMsgNum==0) {
                        ApiManager.interruptImage(mGuid, new File(path), new IGetDataListener<BaseModel>() {
                            @Override
                            public void onResult(BaseModel baseModel, boolean isEmpty) {

                                if (baseModel.getIsSucceed().equals("-32")) {
                                    mChatView.showInterruptDialog(mContext.getString(R.string.chat_who_interrupt));

                                } else if (baseModel.getIsSucceed().equals("-31")) {

                                    mChatView.showInterruptDialog(mContext.getString(R.string.chat_interrupt_who));
                                }
                            }

                            @Override
                            public void onError(String msg, boolean isNetworkError) {
                            }
                        });
                        if (isBlock) {
                            Message message = Message.obtain();
                            message.what = C.message.MSG_TYPE_SEND_IMAGE;
                            message.obj = path;
                            mChatView.sendMessage(message);
                            saveDataToSqlit(userName, String.valueOf(mGuid), mAccount, userPic, mContext.getString(R.string.photo_message), String.valueOf(System.currentTimeMillis()), mExtendType);
                        }
                    } else {
                        CustomDialogAboutPay.dregeVipShow(mContext);
                    }
                    if(!UserPreference.isAnchor()){
                        if(!UserPreference.isVip()&&(mGuid != 10000)){
                            sendMsgNum=1;
                        }else if(!UserPreference.isVip()&&(mGuid == 10000)){
                            sendMsgNum=0;
                        }
                    }
                } else {
                    mChatView.showTip(mContext.getString(R.string.chat_image_empty));
                }
            }
        });
    }

    @Override
    public void handleTouchEventDown() {
        mChatView.showRecordPopupWindow(C.message.STATE_RECORDING);
        startRecord();
        mIsRecordCanceled = false;
    }

    @Override
    public void handleTouchEventMove(MotionEvent event) {
        if (event.getY() < -100) { // 上滑取消发送
            mIsRecordCanceled = true;
            mChatView.showRecordPopupWindow(C.message.STATE_CANCELED);
        } else {
            mIsRecordCanceled = false;
            mChatView.showRecordPopupWindow(C.message.STATE_RECORDING);
        }
    }

    @Override
    public void handleTouchEventUp() {
        mChatView.showRecordPopupWindow(C.message.STATE_IDLE);
        mChatView.dismissPopupWindow();
        if (mIsRecordCanceled) {
            cancelRecord();
        } else {
            stopRecordAndSend();
        }
    }

    private void startRecord() {
        // 录音开始前震动一下
        Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(88);
        // 根据系统时间生成文件名
        String recordFileName = FileUtil.createFileNameByTime() + ".aac";
        // 录音文件临时保存路径：data下包名music目录
        mRecordOutputPath = mRecordDirectory + recordFileName;
        // 开始录音
        RecordUtil.getInstance().startRecord(mRecordOutputPath);
        mIsRecording = true;
        mRecordDuration = 0;
        // 计时，间隔250毫秒刷新
        mChatView.sendMessage(mRefreshInterval, C.message.MSG_TYPE_TIMER);
    }

    private void stopRecordAndSend() {
        if (mIsRecording) {
            mIsRecording = false;
            if (mRecordDuration < 1000) {// 录音时长小于1秒的不发送
                // 删除小于1秒的文件
                FileUtil.deleteFile(mRecordOutputPath);
                // 延时500毫秒调用MediaRecorder#stop()，防止出现start()之后立马调用stop()的异常
                mChatView.sendMessage(500, C.message.MSG_TYPE_DELAYED);
            } else {// 发送语音
                RecordUtil.getInstance().stopRecord();
                sendVoice();
            }
        }
    }

    private void cancelRecord() {
        if (mIsRecordCanceled) {
            RecordUtil.getInstance().stopRecord();
            mIsRecordCanceled = false;
            FileUtil.deleteFile(mRecordOutputPath);
        }
    }

    private void sendVoice() {
        if (canSendMsg&&sendMsgNum==0) {
            ApiManager.interruptVoice(mGuid, new File(mRecordOutputPath), String.valueOf(mRecordDuration / 1000), new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                    if (baseModel.getIsSucceed().equals("-32")) {
                        mChatView.showInterruptDialog(mContext.getString(R.string.chat_who_interrupt));
                        FileUtil.deleteFile(mRecordOutputPath);

                    } else if (baseModel.getIsSucceed().equals("-31")) {
                        mChatView.showInterruptDialog(mContext.getString(R.string.chat_interrupt_who));
                        FileUtil.deleteFile(mRecordOutputPath);
                    }
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                    // 删除已经录制的音频文件
                    FileUtil.deleteFile(mRecordOutputPath);
                }
            });
            if(!UserPreference.isAnchor()){
                if(!UserPreference.isVip()&&(mGuid != 10000)){
                    sendMsgNum=1;
                }else if(!UserPreference.isVip()&&(mGuid == 10000)){
                    sendMsgNum=0;
                }
            }
            if (isBlock) {
                mChatView.sendMessage(0, C.message.MSG_TYPE_SEND_VOICE);
                saveDataToSqlit(userName, String.valueOf(mGuid), mAccount, userPic, mContext.getString(R.string.voice_message), String.valueOf(System.currentTimeMillis()), mExtendType);
            }
        } else {
            CustomDialogAboutPay.dregeVipShow(mContext);
        }
    }

    public void saveDataToSqlit(String name, String id, String account, String pic, String msg, String time, int extendType) {
        if (!TextUtils.isEmpty(id)) {
            HuanXinUser user = new HuanXinUser(id, name, pic, account, "1", msg, 0, time, extendType);
            DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
        }
    }

    @Override
    public void setRefresh2() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                addChatConversation();
            }
        }, 2000);
    }

    private void addChatConversation() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message message = Message.obtain();
                message.what = C.message.MSG_TYPE_ADD;
                mChatView.sendMessage(message);
            }
        }).start();
    }
}
