package com.popo.video.ui.register.adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.model.Country;
import com.popo.library.adapter.CommonRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by xuzhaole on 2017/12/26.
 */

public class LocationAdapter extends CommonRecyclerViewAdapter<Country> {
    private String mCountry;
    private boolean mEnable;
    private int mPosition = -1;


    public LocationAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    public void convert(final Country country, final int position, RecyclerViewHolder holder) {
        if (country != null) {
            holder.setIsRecyclable(false);
            holder.setText(R.id.tv_name, country.getCountryShort());
            ImageView iv_icon = (ImageView) holder.getView(R.id.iv_icon);
            ImageView iv_checked = (ImageView) holder.getView(R.id.iv_checked);
            LinearLayout ll = (LinearLayout) holder.getView(R.id.ll);

            Bitmap imageFromAssetsFile = getImageFromAssetsFile(mContext, country.getCountryShort());
            iv_icon.setImageBitmap(imageFromAssetsFile);
//            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(country.getUrl()).transform(new CropCircleTransformation(mContext))
//                    .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(iv_icon).build());
            if (mCountry != null && mCountry.equals(country.getCountryShort())) {
                iv_checked.setVisibility(View.VISIBLE);
                mPosition = position;
                holder.getView(R.id.iv_checked).setVisibility(View.VISIBLE);
            } else {
                holder.getView(R.id.iv_checked).setVisibility(View.GONE);
            }
            if (mEnable && mCountry != country.getCountryShort()) {
                holder.getView(R.id.ll).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mCountry = country.getCountryShort();
                        mPosition = position;
                        notifyDataSetChanged();
                    }
                });
            }
        }
    }

    /* 读取Assets文件夹中的图片资源
     * @param context
     * @param fileName 图片名称
     * @return
     */
    public static Bitmap getImageFromAssetsFile(Context context, String fileName) {
        Bitmap image = null;
        AssetManager am = context.getResources().getAssets();
        try {
            InputStream is = am.open("country/" + fileName + ".png");
            image = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    public void setCountry(String country, boolean enable) {
        this.mCountry = country;
        this.mEnable = enable;
        notifyDataSetChanged();
    }

    public int getCountryPosition() {
        return mPosition;
    }
}
