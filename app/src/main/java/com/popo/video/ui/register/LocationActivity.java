package com.popo.video.ui.register;

import android.content.Context;
import android.os.Parcelable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.base.BaseAppCompatActivity;
import com.popo.video.event.FinishEvent;
import com.popo.library.dialog.LoadingDialog;
import com.popo.library.net.NetUtil;
import com.popo.video.ui.register.adapter.LocationAdapter;
import com.popo.video.ui.register.contract.LocationContract;
import com.popo.video.ui.register.presenter.LocationPresenter;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 注册页面
 * Created by zhangdroid on 2017/5/12.
 */
public class LocationActivity extends BaseAppCompatActivity implements View.OnClickListener, LocationContract.IView {
    LocationPresenter mLocationPresenter;
    @BindView(R.id.register_activity_iv_back)
    ImageView iv_back;
    @BindView(R.id.register_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.register_activity_rl_toobar)
    RelativeLayout tl_toolbar;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.ll_recyclerview)
    LinearLayout ll_recyclerview;
    @BindView(R.id.register)
    Button btn_register;
    @BindView(R.id.rl_button)
    LinearLayout rl_button;
    @BindView(R.id.register_root)
    RelativeLayout registerRoot;
    @BindView(R.id.tv_error)
    TextView tv_error;

    private LocationAdapter mAdapter;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register:
                mLocationPresenter.register();
                break;
            case R.id.register_activity_rl_back:
                finish();
                break;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_location;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        GridLayoutManager linearLayoutManager = new GridLayoutManager(mContext, 3);
        recyclerview.setLayoutManager(linearLayoutManager);
        recyclerview.setHasFixedSize(true);
        mAdapter = new LocationAdapter(mContext, R.layout.item_loaction);
        recyclerview.setAdapter(mAdapter);

        mLocationPresenter = new LocationPresenter(this);
        mLocationPresenter.start();


    }

    @Override
    protected void setListeners() {
        btn_register.setOnClickListener(this);
        rl_back.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
    }


    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        tv_error.setVisibility(View.VISIBLE);
        recyclerview.setVisibility(View.GONE);
        tv_error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerview.setVisibility(View.VISIBLE);
                mLocationPresenter.getLocation();
            }
        });
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        tv_error.setVisibility(View.VISIBLE);
        tv_error.setText(msg);
        recyclerview.setVisibility(View.GONE);
        tv_error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerview.setVisibility(View.VISIBLE);
                mLocationPresenter.getLocation();
            }
        });
    }

    @Override
    public LocationAdapter getLocationAdapter() {
        return mAdapter;
    }

    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }
}
