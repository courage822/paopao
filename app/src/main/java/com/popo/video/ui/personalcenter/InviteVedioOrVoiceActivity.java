package com.popo.video.ui.personalcenter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.popo.video.R;
import com.popo.video.base.BaseTopBarActivity;
import com.popo.video.common.TimeUtils;
import com.popo.video.data.preference.DataPreference;
import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.parcelable.ListParcelable;
import com.popo.video.parcelable.SendVideoOrVoiceParcelable;
import com.popo.video.ui.personalcenter.contract.InviteVideoOrVoiceContract;
import com.popo.video.ui.personalcenter.presenter.InviteVideoOrVoicePresenter;
import com.popo.video.ui.video.MassVideoOrVoiceActivity;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/11/7.
 */

public class InviteVedioOrVoiceActivity extends BaseTopBarActivity implements View.OnClickListener,InviteVideoOrVoiceContract.IView {
    @BindView(R.id.invite_video_or_voice_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.invite_video_or_voice_tv_title)
    TextView tv_title;
    @BindView(R.id.invite_video_or_voice_tv_msg)
    TextView tv_msg;
    @BindView(R.id.invite_video_or_voice_ll_voice)
    LinearLayout ll_voice;
    @BindView(R.id.invite_video_or_voice_ll_video)
    LinearLayout ll_video;
    @BindView(R.id.invite_video_or_voice_tv_peoples)
    TextView tv_persons;
    @BindView(R.id.invite_video_or_voice_btn_send_video)
    Button btn_send_video;
    @BindView(R.id.invite_video_or_voice_btn_send_voice)
    Button btn_send_voice;
    @BindView(R.id.invite_video_or_voice_tv_video_price)
    TextView tv_price;
    private SendVideoOrVoiceParcelable mVideoOrVoiceParcelable;
    private InviteVideoOrVoicePresenter mPresenter;
    private long recLen = 0;
    Timer timer = new Timer();
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_invite_video_or_voice;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return null;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mVideoOrVoiceParcelable= (SendVideoOrVoiceParcelable) parcelable;
    }

    @Override
    protected void initViews() {
        mPresenter=new InviteVideoOrVoicePresenter(this);
    tv_title.setText(mVideoOrVoiceParcelable.title);
        if (mVideoOrVoiceParcelable.pos==0) {
            ll_video.setVisibility(View.VISIBLE);
            ll_voice.setVisibility(View.GONE);
            btn_send_video.setVisibility(View.VISIBLE);
            btn_send_voice.setVisibility(View.GONE);
            tv_msg.setText(getString(R.string.invite_video));
            mPresenter.getCountDown("1");
        }else{
            ll_video.setVisibility(View.GONE);
            ll_voice.setVisibility(View.VISIBLE);
            btn_send_video.setVisibility(View.GONE);
            btn_send_voice.setVisibility(View.VISIBLE);
            tv_msg.setText(getString(R.string.invite_voice));
            mPresenter.getCountDown("2");
        }
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
        btn_send_video.setOnClickListener(this);
        btn_send_voice.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.invite_video_or_voice_rl_back:
                finish();
                break;
            case R.id.invite_video_or_voice_btn_send_video:
                long currentTime1 = DataPreference.getCurrentTime();
                if(currentTime1==0){
                    //群发视频邀请
                    massVideoInvite();
                    DataPreference.saveCurrentTime(System.currentTimeMillis());
                }else{
                    if(System.currentTimeMillis()-currentTime1<120*1000){
                        Toast.makeText(InviteVedioOrVoiceActivity.this, getString(R.string.sending_voice), Toast.LENGTH_SHORT).show();
                    }else{
                        //群发视频邀请
                        massVideoInvite();
                        DataPreference.saveCurrentTime(0);
                    }
                }

                break;
            case R.id.invite_video_or_voice_btn_send_voice:
                long currentTime2 = DataPreference.getCurrentTime();
                if(currentTime2==0){
                    //群发语音邀请
                    massVoiceInvite();
                    DataPreference.saveCurrentTime(System.currentTimeMillis());
                }else{
                    if(System.currentTimeMillis()-currentTime2<120*1000){
                        Toast.makeText(InviteVedioOrVoiceActivity.this, getString(R.string.sending_video), Toast.LENGTH_SHORT).show();
                    }else{
                        //群发语音邀请
                        massVoiceInvite();
                        DataPreference.saveCurrentTime(0);
                    }
                }

                break;
        }
    }
    //群发语音邀请
    private void massVoiceInvite() {
        mPresenter.startInvite("2");
        DataPreference.saveTime(0);
        btn_send_voice.setEnabled(false);
        //ListParcelable是用来回调fragment,数据类型是一样的这里就用这个吧
        LaunchHelper.getInstance().launch(this,MassVideoOrVoiceActivity.class,new ListParcelable(2));
    }

    //群发视频邀请
    private void massVideoInvite() {
        mPresenter.startInvite("1");
        DataPreference.saveTime(0);
        btn_send_video.setEnabled(false);
        //ListParcelable是用来回调fragment,数据类型是一样的这里就用这个吧
        LaunchHelper.getInstance().launch(this,MassVideoOrVoiceActivity.class,new ListParcelable(1));
        saveCurrentTime();
    }

    private void saveCurrentTime() {


    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void getButtonCountDown(long time) {
        recLen=time;
        if(time>0){
            if(mVideoOrVoiceParcelable.pos==0){
               btn_send_video.setEnabled(false);
            }else{
                btn_send_voice.setEnabled(false);
            }
             timer.schedule(task, 1000, 1000);       // timeTask
        }
    }

    @Override
    public void getSendNum(int num) {
        tv_persons.setText(String.valueOf(num));
    }

    @Override
    public void getAnchorPrice(String price) {
        tv_price.setText(price);
    }

    final Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            switch (msg.what) {
                case 1:
                    if (mVideoOrVoiceParcelable.pos==0) {
                        btn_send_video.setText(TimeUtils.fromSecondToTime(recLen/1000,1));
                    }else{
                        btn_send_voice.setText(TimeUtils.fromSecondToTime(recLen/1000,1));
                    }
                    if(recLen <=0){
                        timer.cancel();
                        if(mVideoOrVoiceParcelable.pos==0){
                            btn_send_video.setText(getString(R.string.invite_video));
                            btn_send_video.setEnabled(true);
                        }else{
                            btn_send_voice.setText(getString(R.string.invite_voice));
                            btn_send_voice.setEnabled(true);
                        }
                    }
            }
        }
    };

    TimerTask task = new TimerTask() {
        @Override
        public void run() {
            recLen=recLen-1000;
            if(recLen<=0){
                timer.cancel();
                if(mVideoOrVoiceParcelable.pos==0){
                    btn_send_video.setText(getString(R.string.invite_video));
                    btn_send_video.setEnabled(true);
                }else{
                    btn_send_voice.setText(getString(R.string.invite_voice));
                    btn_send_voice.setEnabled(true);
                }
            }else{
                Message message = new Message();
                message.what = 1;
                handler.sendMessage(message);
            }

        }
    };

}
