package com.popo.video.ui.detail.listener;

import android.view.View;

/**
 * Created by xuzhaole on 2018/1/12.
 */

public interface OnItemClickListener {

    void onItemClick(View view, int position);
}
