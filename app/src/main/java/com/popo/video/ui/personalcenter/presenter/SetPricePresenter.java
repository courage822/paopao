package com.popo.video.ui.personalcenter.presenter;

import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.SendVideoOrVoice;
import com.popo.video.data.model.SendVideoOrVoiceHostInfor;
import com.popo.video.ui.personalcenter.contract.SetPriceConteact;

/**-
 * Created by Administrator on 2017/7/5.
 */

public class SetPricePresenter implements SetPriceConteact.IPresenter{
    private SetPriceConteact.IView mSetPriceView;
    private boolean flag=true;
    public SetPricePresenter(SetPriceConteact.IView mSetPriceView) {
        this.mSetPriceView = mSetPriceView;
    }
    @Override
    public void savePrice() {
        mSetPriceView.showLoading();
        flag=true;
        final String videoPrice = mSetPriceView.getVideoPrice();
        final String audioPrice = mSetPriceView.getAudioPrice();
        modificationPrice("1",videoPrice);
        modificationPrice("2",audioPrice);
    }

    @Override
    public void startInvite(String type) {
        ApiManager.sendVideoOrVoice(type, new IGetDataListener<SendVideoOrVoice>() {
            @Override
            public void onResult(SendVideoOrVoice sendVideoOrVoice, boolean isEmpty) {
            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    @Override
    public void getVideoCountDown() {
        ApiManager.getVideoOrVoiceTime("1", new IGetDataListener<SendVideoOrVoiceHostInfor>() {
            @Override
            public void onResult(SendVideoOrVoiceHostInfor sendVideoOrVoice, boolean isEmpty) {
                if(sendVideoOrVoice!=null){
                    if(sendVideoOrVoice.getCountDown()>0){
                        mSetPriceView.getVideoTime();
                    }
                }


            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    @Override
    public void getVoiceCountDown() {
        ApiManager.getVideoOrVoiceTime("2", new IGetDataListener<SendVideoOrVoiceHostInfor>() {
            @Override
            public void onResult(SendVideoOrVoiceHostInfor sendVideoOrVoice, boolean isEmpty) {
                if(sendVideoOrVoice!=null){
                    if(sendVideoOrVoice.getCountDown()>0){
                        mSetPriceView.getVideoTime();
                    }
                }


            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    private void modificationPrice(String type, String videoPrice) {
        ApiManager.savePrice(type,videoPrice,new IGetDataListener<String>(){
            @Override
            public void onResult(String s, boolean isEmpty) {
                mSetPriceView.dismissLoading();
                if (flag) {
                    mSetPriceView.modificationPrice();
                    flag=false;
                }

            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
                mSetPriceView.dismissLoading();
                if (isNetworkError) {
                    mSetPriceView.showNetworkError();
                } else {
                    mSetPriceView.showTip(msg);
                }
            }
        });
    }

    @Override
    public void start() {

    }

}
