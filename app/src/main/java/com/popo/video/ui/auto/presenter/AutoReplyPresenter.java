package com.popo.video.ui.auto.presenter;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.HostAutoReplyBean;
import com.popo.video.ui.auto.contract.AutoReplyContract;
import com.popo.video.ui.auto.fragment.AutoReplyFragment;
import com.popo.video.ui.auto.fragment.AutonomousHelloFragment;
import com.popo.video.ui.homepage.HomepageAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public class AutoReplyPresenter implements AutoReplyContract.IPresenter {
    private static final String TAG = AutoReplyPresenter.class.getSimpleName();
    private AutoReplyContract.IView mAutoReply;
    private Context mContext;

    public AutoReplyPresenter(AutoReplyContract.IView view) {
        this.mAutoReply = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }


    @Override
    public void getAddCount() {
        ApiManager.getHostAutoReply(new IGetDataListener<HostAutoReplyBean>() {
            @Override
            public void onResult(HostAutoReplyBean bean, boolean isEmpty) {
                if (bean != null) {
                    mAutoReply.showAddCount(bean.getType1Reply().size(), bean.getType2Reply().size());
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mAutoReply.showAddCount(0, 0);
            }
        });
    }

    @Override
    public void addTabs() {
        HomepageAdapter homepageAdapter = new HomepageAdapter(mAutoReply.getManager());
        List<Fragment> fragmentList = new ArrayList<>();
        List<String> tabList = Arrays.asList(mContext.getString(R.string.auto_type), mContext.getString(R.string.hello_type));
        fragmentList.add(new AutoReplyFragment());
        fragmentList.add(new AutonomousHelloFragment());
        homepageAdapter.setData(fragmentList, tabList);
        mAutoReply.setAdapter(homepageAdapter);
    }


}
