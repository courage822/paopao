package com.popo.video.ui.video;

import android.content.Context;
import android.graphics.Color;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.data.model.VideoMsg;
import com.popo.library.adapter.CommonRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;

/**
 * 视频时消息列表适配器
 * Created by zhangdroid on 2017/6/22.
 */
public class VideoMsgAdapter extends CommonRecyclerViewAdapter<VideoMsg> {

    public VideoMsgAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    public void convert(VideoMsg videoMsg, int position, RecyclerViewHolder holder) {
        if (null != videoMsg) {
            TextView tv_name = (TextView) holder.getView(R.id.item_video_nickname);
            TextView tv_msg = (TextView) holder.getView(R.id.item_video_msg);
            if(videoMsg.getType()==1){
                tv_name.setTextColor(Color.parseColor("#fd698b"));
                tv_msg.setTextColor(Color.parseColor("#fd698b"));
            }else if(videoMsg.getType()==0){
                tv_name.setTextColor(Color.WHITE);
                tv_msg.setTextColor(Color.WHITE);
            }
            tv_name.setText( videoMsg.getNickname());
            tv_msg.setText( videoMsg.getMessage());
        }
    }

}
