package com.popo.video.ui.message.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.popo.video.R;
import com.popo.video.common.HyphenateHelper;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.db.DbModle;
import com.popo.video.event.UnreadMsgChangedEvent;
import com.popo.library.adapter.MultiTypeRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.util.LaunchHelper;
import com.popo.video.parcelable.ChatParcelable;
import com.popo.video.ui.chat.ChatActivity;
import com.popo.video.ui.message.adapter.ChatHistoryAdapter;
import com.popo.video.ui.message.contract.HelloPeopleContract;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xuzhaole on 2017/12/2.
 */

public class HelloPeoplePresenter implements HelloPeopleContract.IPresenter {
    private HelloPeopleContract.IView mHelloView;
    private Context mContext;
    private ChatHistoryAdapter mChatHistoryAdapter;
    List<HuanXinUser> allAcount = null;
    List<HuanXinUser> helloList =null;

    public HelloPeoplePresenter(HelloPeopleContract.IView view) {
        this.mHelloView = view;
        this.mContext = view.obtainContext();
    }
    @Override
    public void start() {

    }


    @Override
    public void loadData() {
        mChatHistoryAdapter = new ChatHistoryAdapter(mContext, R.layout.item_chat_history);
        mHelloView.setAdapter(mChatHistoryAdapter, R.layout.common_load_more);
        mChatHistoryAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                if (helloList != null && helloList.size() > 0) {
                    HuanXinUser huanXinUser = helloList.get(position);
                    if (huanXinUser != null) {
                        EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                        DbModle.getInstance().getUserAccountDao().setState(huanXinUser, true);//把标记设置为已经读取
                        String hxId = huanXinUser.getHxId();
                        if (!TextUtils.isEmpty(hxId)) {
                            long guid = Long.parseLong(hxId);
                            LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(guid,
                                    huanXinUser.getAccount(), huanXinUser.getHxName(), huanXinUser.getHxIcon(), 0));
                        }
                    }
                }
            }
        });
        // 查询未读消息数
        allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
        if (allAcount != null && allAcount.size() > 0) {
            helloList = new ArrayList<>();
            for (HuanXinUser user:allAcount) {
                if (user.getExtendType() == 8){
                    helloList.add(user);
                }
            }
            if (mChatHistoryAdapter != null && helloList.size() > 0) {
                mChatHistoryAdapter.bind(helloList);
                mHelloView.showNoMore();
                mHelloView.hideLoadMore();
            }else{
                mHelloView.showNoMore();
                mHelloView.hideLoadMore();
                mHelloView.toggleShowEmpty(true, null);
            }
        } else {
            mHelloView.showNoMore();
            mHelloView.hideLoadMore();
            mHelloView.toggleShowEmpty(true, null);
        }
        mHelloView.hideRefresh(1);
    }

    @Override
    public void refresh() {
        mHelloView.toggleShowEmpty(false, null);
        loadData();
    }
}
