package com.popo.video.ui.homepage.adapter;

import android.support.annotation.LayoutRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.popo.library.adapter.base.BaseQuickAdapter;
import com.popo.library.adapter.base.BaseViewHolder;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.SearchUser;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.UserPreference;

import java.util.List;

/**
 * Created by xuzhaole on 2018/1/9.
 */

public class HomeListAdapter extends BaseQuickAdapter<SearchUser, BaseViewHolder> {

    public HomeListAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position, List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            holder.getView(R.id.item_homepage_big_iv_sayhello).setBackgroundResource(R.drawable.say_helloed);
        }
    }

    @Override
    protected void convert(final BaseViewHolder holder, final SearchUser searchUser) {
        if (searchUser != null) {
            final ImageView imageView = holder.getView(R.id.item_homepage_iv_avatar);
            TextView tv_heard_word = holder.getView(R.id.item_homepage_tv_heard_word);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(searchUser.getIconUrlMiddle())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(imageView).build());
            if(PlatformPreference.getPlatformInfo().getFid().equals("30108")){
                holder.setText(R.id.item_homepage_tv_nickname, Util.chineseFontChanger(searchUser.getNickName()));
                tv_heard_word.setText(Util.chineseFontChanger(searchUser.getOwnWords()));
            }else{
                holder.setText(R.id.item_homepage_tv_nickname, searchUser.getNickName());
                tv_heard_word.setText(searchUser.getOwnWords());
            }
            holder.setText(R.id.item_homepage_tv_age_and_height, searchUser.getAge() + mContext.getString(R.string.edit_info_years_old) + "  " + mContext.getString(R.string.distance));
            TextView tv_state = holder.getView(R.id.item_homepage_tv_state);
            TextView tv_age = holder.getView(R.id.item_homepage_tv_age);
            TextView tv_online = holder.getView(R.id.item_homepage_big_tv_online);

            RelativeLayout rl_sex = holder.getView(R.id.item_homepage_rl_age_sex);
            TextView tv_big_state = holder.getView(R.id.item_homepage_big_tv_state);
            // 视频
            ImageView iBVideo = holder.getView(R.id.item_homepage_iv_state);
            final ImageView iv_sayhello = holder.getView(R.id.item_homepage_big_iv_sayhello);
            if (UserPreference.isMale()) {
                rl_sex.setBackgroundResource(R.drawable.icon_famle);
            } else {
                rl_sex.setBackgroundResource(R.drawable.icon_male);
            }
            tv_age.setText(searchUser.getAge());
            final int status = searchUser.getStatus();
            int onlineStatus = searchUser.getOnlineStatus();
            if (onlineStatus == -1) {
                tv_big_state.setText(mContext.getString(R.string.not_online));
                tv_online.setVisibility(View.GONE);
            } else {
                tv_big_state.setText(mContext.getString(R.string.edit_info_status));
                tv_online.setVisibility(View.VISIBLE);
            }
            if (searchUser.getVipDays() > 0) {
                holder.getView(R.id.iv_vip).setVisibility(View.VISIBLE);
            } else {
                holder.getView(R.id.iv_vip).setVisibility(View.GONE);
            }
            if (UserPreference.isAnchor()) {
                iv_sayhello.setVisibility(View.GONE);
                iv_sayhello.setBackgroundResource(R.drawable.say_hello);
            } else {
                iv_sayhello.setVisibility(View.GONE);
            }
            int isSayHello = searchUser.getIsSayHello();
            if (isSayHello == 1) {
                iv_sayhello.setBackgroundResource(R.drawable.say_helloed);
            } else {
                iv_sayhello.setBackgroundResource(R.drawable.say_hello);
            }
            iv_sayhello.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, mContext.getString(R.string.hello_success), Toast.LENGTH_SHORT).show();
                    searchUser.setIsSayHello(1);
                    iv_sayhello.setBackgroundResource(R.drawable.say_helloed);
                    notifyItemChanged(holder.getAdapterPosition(), searchUser);
                    ApiManager.sayHello(String.valueOf(searchUser.getUesrId()), new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {

                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {

                        }
                    });
                }
            });
            switch (status) {
                case C.homepage.STATE_FREE:// 空闲
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_free);
                    tv_state.setText(mContext.getString(R.string.invitable));
                    break;
                case C.homepage.STATE_BUSY:// 忙线中
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_busy);
                    tv_state.setText(mContext.getString(R.string.talking));
                    break;
                case C.homepage.STATE_NO_DISTRUB:// 勿扰
                    iBVideo.setVisibility(View.GONE);
                    tv_state.setVisibility(View.GONE);
                    tv_big_state.setText(mContext.getString(R.string.message_state_nodistrub));
                    break;
                default:
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_free);
                    tv_state.setText(mContext.getString(R.string.invitable));
                    break;
            }
        }
    }
}
