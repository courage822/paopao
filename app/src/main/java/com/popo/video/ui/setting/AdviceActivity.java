package com.popo.video.ui.setting;

import android.content.Context;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.event.AdviceEvent;
import com.popo.library.dialog.OnDialogClickListener;
import com.popo.library.net.NetUtil;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 设置页面
 * Created by WangYong on 2017/5/12.
 */
public class AdviceActivity extends BaseFragmentActivity implements View.OnClickListener, SettingContract.IView {
    @BindView(R.id.advice_activity_et_detial)
    EditText et_detail;
    @BindView(R.id.advice_activity_et_msg)
    EditText et_msg;
    @BindView(R.id.advice_activity_btn_send)
    Button btn_send;
    @BindView(R.id.advice_activity_rl_back)
    RelativeLayout rl_back;
    private SettingPresenter mSettingPresenter;

    @Override
    public void onClick(View v) {
      switch (v.getId()){
          case R.id.advice_activity_btn_send:
                mSettingPresenter.getAdviceAndContact(et_detail.getText().toString(),et_msg.getText().toString());
              break;
          case R.id.advice_activity_rl_back:
              finish();
              break;
      }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void toggleNoDistrub(boolean toggle) {

    }

    @Override
    public void setCacheSize(String cacheSize) {

    }

    @Override
    public void setVersion(String versionName) {

    }

    @Override
    public void showAlertDialog(String message, OnDialogClickListener listener) {

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_advice;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        mSettingPresenter=new SettingPresenter(this);
    }

    @Override
    protected void setListeners() {
        btn_send.setOnClickListener(this);
        rl_back.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {
    }
    @Subscribe
    public void onEvent(AdviceEvent author){
      finish();
    }
}
