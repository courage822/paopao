package com.popo.video.ui.detail;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.popo.video.R;
import com.popo.video.base.BaseTopBarActivity;
import com.popo.library.dialog.LoadingDialog;
import com.popo.library.net.NetUtil;
import com.popo.library.util.SnackBarUtil;
import com.popo.library.widget.XRecyclerView;
import com.popo.video.event.SingleLoginFinishEvent;
import com.popo.video.ui.detail.adapter.AlbumAdapter;
import com.popo.video.ui.detail.adapter.NewAlbumAdapter;
import com.popo.video.ui.detail.contract.AlbumContract;
import com.popo.video.ui.detail.presenter.AlbumPresenter;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

import static com.popo.video.common.Util.getContext;

/**
 * 相册
 * Created by zhangdroid on 2017/5/27.
 */
public class AlbumActivity extends BaseTopBarActivity implements AlbumContract.IView, View.OnClickListener, View.OnLongClickListener {
    @BindView(R.id.activity_album_rl_root)
    LinearLayout rl_root;
    @BindView(R.id.album_activity_rl_back)
    RelativeLayout rl_bak;
    @BindView(R.id.album_activity_relcverview)
    RecyclerView mReclerview;
    private AlbumPresenter albumPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_album;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.person_album);
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected void initViews() {
        albumPresenter = new AlbumPresenter(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        mReclerview.setLayoutManager(gridLayoutManager);
        mReclerview.setHasFixedSize(true);
        albumPresenter.start(mReclerview);
    }

    @Override
    protected void setListeners() {
        rl_bak.setOnClickListener(this);

    }

    @Override
    protected void loadData() {
        albumPresenter.getPhotoInfo();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(rl_root, msg);
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void setAdapter(NewAlbumAdapter adater) {
        mReclerview.setAdapter(adater);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.album_activity_rl_back:
                finish();
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {

        return true;
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }
}
