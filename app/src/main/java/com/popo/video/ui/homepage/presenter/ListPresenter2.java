package com.popo.video.ui.homepage.presenter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.data.api.ApiConstant;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.SearchCriteria;
import com.popo.video.data.model.SearchUser;
import com.popo.video.data.model.SearchUserList;
import com.popo.video.data.preference.SearchPreference;
import com.popo.library.util.Utils;
import com.popo.video.ui.homepage.ListFragmentBaseAdapter;
import com.popo.video.ui.homepage.contract.ListContract;
import com.popo.video.ui.homepage.contract.ListContract2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangdroid on 2017/6/2.
 */
public class ListPresenter2 implements ListContract.IPresenter {
    private ListContract2.IView mListView;
    private Context mContext;
    private int pageNum = 1;
    private static final String pageSize = "50";
    private String url;
    private ListFragmentBaseAdapter mListAdapter;

    public ListPresenter2(ListContract2.IView view) {
        this.mListView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void loadRecommendUserList(int type) {
        switch (type) {
            case C.homepage.TYPE_GODDESS:
                url = ApiConstant.URL_HOMEPAGE_INCOME;
                break;
            case C.homepage.TYPE_ACTIVE_ANCHOR:
                url = ApiConstant.URL_HOMEPAGE_ACTIVE;
                break;
            case C.homepage.TYPE_NEW:
                url = ApiConstant.URL_HOMEPAGE_NEW;
                break;
            case C.homepage.TYPE_ACTIVE_FRIEND:
                url = ApiConstant.URL_HOMEPAGE_ACTIVE;
                break;
            case C.homepage.TYPE_NEW_FRIEND:
                url = ApiConstant.URL_HOMEPAGE_NEW;
                break;
        }
        mListAdapter = new ListFragmentBaseAdapter(mContext, R.layout.list_fragment2_item);
        mListAdapter.setFragmentManager(mListView.obtainFragmentManager());
//        mListAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
//                SearchUser searchUser = mListAdapter.getItemByPosition(position);
//                if (null != searchUser) {
//                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
//                            new UserDetailParcelable(String.valueOf(searchUser.getUesrId())));
//                }
//            }
//        });
        // 设置加载更多
        mListView.setAdapter(mListAdapter, R.layout.common_load_more);
        load();
    }

    @Override
    public void refresh() {
        pageNum = 1;
        load();
    }

    @Override
    public void sayHelloStatus() {

    }

    @Override
    public void start(RecyclerView recyclerView) {

    }

    /**
     * 上拉加载更多
     */
    public void loadMore() {
        mListView.showLoadMore();
        pageNum++;
        load();
    }

    private void load() {
        // 设置搜索条件
        Map<String, SearchCriteria> criteria = new HashMap<>();
        SearchCriteria searchCriteria = SearchPreference.getSearchCriteria();
        if (null != searchCriteria) {
            criteria.put("criteria", searchCriteria);
        }
        ApiManager.getHomepageRecommend(ApiConstant.URL_HOMEPAGE_HOT, pageNum, pageSize, new Gson().toJson(criteria), new IGetDataListener<SearchUserList>() {
            @Override
            public void onResult(SearchUserList searchUserList, boolean isEmpty) {
                if (isEmpty) {
                    if (pageNum == 1) {
                        mListView.toggleShowEmpty(true, null);
                    } else if (pageNum > 1) {
                        mListView.showNoMore();
                    }
                } else {
                    if (null != searchUserList) {
                        List<SearchUser> list = searchUserList.getSearchUserList();
                        if (!Utils.isListEmpty(list)) {
                            if (pageNum == 1) {
                                mListAdapter.replaceAll(list);
                            } else if (pageNum > 1) {
                                mListAdapter.appendToList(list);
                            }
                            mListView.hideLoadMore();
                        }
                    }
                }
                mListView.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mListView.hideRefresh(1);
                if (!isNetworkError) {
                    mListView.toggleShowError(true, msg);
                }
            }

        });
    }
    public void removeItem(int pos){
        if (mListAdapter != null) {
            mListAdapter.removeItem(0);
        }
    }
}
