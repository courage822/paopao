package com.popo.video.ui.dialog;

import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.popo.video.R;
import com.popo.library.dialog.BaseDialogFragment;
import com.popo.library.widget.WheelView;

import java.util.ArrayList;
import java.util.List;

/**
 * 年龄选择框
 * Created by zhangdroid on 2017/5/31.
 */
public class AgeSelectDialog extends BaseDialogFragment {
    private List<String> mAgeList = new ArrayList<>();
    private String mSelectedText;
    private OnAgeSelectListener mOnAgeSelectListener;

    private static AgeSelectDialog newInstance(String selectedText, OnAgeSelectListener listener) {
        AgeSelectDialog ageSelectDialog = new AgeSelectDialog();
        ageSelectDialog.mSelectedText = selectedText;
        ageSelectDialog.mOnAgeSelectListener = listener;
        return ageSelectDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_select_age;
    }

    @Override
    protected void setDialogContentView(View view) {
        final WheelView wheelView = (WheelView) view.findViewById(R.id.dialog_age_wheelview);
        for (int i = 18; i < 66; i++) {
            mAgeList.add(String.valueOf(i));
        }
        wheelView.setData(mAgeList);
        if (!TextUtils.isEmpty(mSelectedText)) {
            wheelView.setDefaultIndex(mAgeList.indexOf(mSelectedText));
        }
        Button btnSure = (Button) view.findViewById(R.id.dialog_age_sure);
        btnSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mOnAgeSelectListener) {
                    mOnAgeSelectListener.onSelected(wheelView.getSelectedText());
                }
                dismiss();
            }
        });
    }

    public static void show(FragmentManager fragmentManager, String selectedText, OnAgeSelectListener onSexSelectedListener) {
        newInstance(selectedText, onSexSelectedListener).show(fragmentManager, "age_select");
    }

    public interface OnAgeSelectListener {
        void onSelected(String age);
    }

}
