package com.popo.video.ui.charmandrankinglist;

import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.RelativeLayout;


import com.popo.library.net.NetUtil;
import com.popo.video.R;
import com.popo.video.base.BaseAppCompatActivity;

import butterknife.BindView;

/**
 * Created by WangYong on 2018/320.
 */

public class CharmandAndDrankingListActivity extends BaseAppCompatActivity implements View.OnClickListener {
@BindView(R.id.activity_charmand_dranking_rl_back)
    RelativeLayout rl_back;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_charmand_ranking_layout;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_charmand_dranking_ll,new CharmandAndDrankingListFragment()).commit();
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
      switch (v.getId()){
          case R.id.activity_charmand_dranking_rl_back:
             finish();
              break;
      }
    }
}
