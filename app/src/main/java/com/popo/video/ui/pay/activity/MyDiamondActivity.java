package com.popo.video.ui.pay.activity;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.library.net.NetUtil;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.common.YeMeiPopopUtil;
import com.popo.video.event.YeMeiMessageEvent;
import com.popo.video.ui.pay.adapter.MyDiamondAdapter;
import com.popo.video.ui.pay.contract.MyDiamondContract;
import com.popo.video.ui.pay.presenter.MyDiamondPresenter;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/8/30.
 */

public class MyDiamondActivity extends BaseFragmentActivity implements View.OnClickListener,MyDiamondContract.IView{
    @BindView(R.id.mydiamond_activity_ll_root)
    LinearLayout mLlRoot;
    @BindView(R.id.mydiamond_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.mydiamond_activity_recyclerview)
    RecyclerView mRecyelerView;
    @BindView(R.id.mydiamond_activity_tv_dionmads)
    TextView tv_dionmads;
    private MyDiamondPresenter mMyDiamondPresenter;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_mydiamond;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyelerView.setLayoutManager(linearLayoutManager);
        mRecyelerView.setHasFixedSize(true);
        mRecyelerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mMyDiamondPresenter=new MyDiamondPresenter(this);
        mMyDiamondPresenter.getPayWay(C.pay.FROM_TAG_PERSON);
        mMyDiamondPresenter.start();
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
             switch (v.getId()){
                 case R.id.mydiamond_activity_rl_back:
                     finish();
                     break;
             }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void dismissLoading() {
        toggleShowLoading(false, null);
    }

    @Override
    public void showNetworkError() {

    }

    @Override
    public void setAdapter(MyDiamondAdapter adapter) {
        if (null != adapter) {
            adapter.setPayPresenter(mMyDiamondPresenter);
            mRecyelerView.setAdapter(adapter);
        }
    }

    @Override
    public void getDionmads(String num) {
        tv_dionmads.setText(num);
    }
    @Subscribe
    public void onEvent(YeMeiMessageEvent evet){
        Message msg=new Message();
        msg.obj=evet;
        yeMeiHandler.sendMessage(msg);

    }
    private Handler yeMeiHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            YeMeiMessageEvent obj = (YeMeiMessageEvent) msg.obj;
            if(obj!=null){
                YeMeiPopopUtil.getInstance().showYeMei(MyDiamondActivity.this,mLlRoot,obj,isFinishing());

            }
        }
    };
}
