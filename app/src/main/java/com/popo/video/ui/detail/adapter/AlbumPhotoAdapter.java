package com.popo.video.ui.detail.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageView;

import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.UpLoadMyPhoto;
import com.popo.video.data.model.UserPhoto;
import com.popo.library.adapter.CommonRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.dialog.LoadingDialog;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.video.ui.photo.GetPhotoActivity;

import java.io.File;

public class AlbumPhotoAdapter extends CommonRecyclerViewAdapter<UserPhoto> {
    private FragmentManager fragmentManager;

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    private int pos = 0;

    public AlbumPhotoAdapter(Context context, int layoutResId, int pos) {
        super(context, layoutResId);
        this.pos = pos;
    }

    @Override
    public void convert(final UserPhoto userPhoto, int position, final RecyclerViewHolder holder) {
        if (userPhoto != null) {
            if (pos == 0) {
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userPhoto.getFileUrlMinimum()).imageView((ImageView) holder.getView(R.id.item_photo_iv))
                        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).build());
                if (userPhoto.getStatus().equals("1")) {
                    holder.getView(R.id.layout_layer).setVisibility(View.GONE);
                } else {
                    holder.getView(R.id.layout_layer).setVisibility(View.VISIBLE);
                }

            } else {
                final ImageView iv_avatar = (ImageView) holder.getView(R.id.item_photo_iv);
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userPhoto.getFileUrlMinimum()).imageView(iv_avatar)
                        .placeHolder(R.drawable.chick_pic_video).error(R.drawable.chick_pic_video).build());
                iv_avatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        uploadImage(iv_avatar);
                    }
                });
            }

        }
    }

    public void uploadImage(final ImageView imageView) {
        GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
            @Override
            public void getSelectedPhoto(File file) {
                if (null != file) {
                    LoadingDialog.show(fragmentManager);
                    ApiManager.upLoadMyPhotoOrAvator(file, false, new IGetDataListener<UpLoadMyPhoto>() {
                        @Override
                        public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                            LoadingDialog.hide();
                            if (upLoadMyPhoto != null) {
                                final UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                                imageView.setImageResource(R.drawable.icon_check_my);
                            }
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                            LoadingDialog.hide();
                            if (isNetworkError) {
                            } else {
                            }
                        }
                    });
                }
            }
        });
    }
}
