package com.popo.video.ui.detail.presenter;

import android.content.Context;

import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.library.util.LaunchHelper;
import com.popo.video.ui.detail.ReportSuccessActivity;
import com.popo.video.ui.detail.contract.ReportContract;

/**
 * Created by Administrator on 2017/6/14.
 */
public class ReportPresenter implements ReportContract.IPresenter {
   private Context mContext;
   private ReportContract.IView mReportIview;

    public ReportPresenter(ReportContract.IView mReportIview) {
        this.mReportIview = mReportIview;
        mContext=mReportIview.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void startReport(String userId,String resonCode) {
        ApiManager.report(userId, resonCode,new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel aSwitch, boolean isEmpty) {
                LaunchHelper.getInstance().launchFinish(mContext, ReportSuccessActivity.class);
            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
}
