package com.popo.video.ui.pay.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.popo.video.R;

import java.util.List;

/**
 * 支付项适配器
 * Created by WangYong on 2017/9/19.
 */
public class DredgeVipAdapter extends RecyclerView.Adapter<DredgeVipAdapter.MviewHolder> {
 private Context context;
 private List<String> list;

    public DredgeVipAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public MviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.dredgevip_item,null);
        return new MviewHolder(view);
    }

    @Override
    public void onBindViewHolder(MviewHolder holder, int position) {
       holder.tv_detail.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
       class MviewHolder extends RecyclerView.ViewHolder{
       private TextView tv_detail;
        public MviewHolder(View itemView) {
            super(itemView);
            tv_detail= (TextView) itemView.findViewById(R.id.dredgevip_item_tv_detail);
        }
    }
}
