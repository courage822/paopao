package com.popo.video.ui.follow.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.UserBase;
import com.popo.video.data.model.UserDetail;
import com.popo.video.data.model.UserDetailforOther;
import com.popo.library.util.LaunchHelper;
import com.popo.video.parcelable.UserDetailParcelable;
import com.popo.video.ui.detail.UserDetailActivity;
import com.popo.video.ui.follow.contract.FindUserContract;

/**
 * Created by Administrator on 2017/6/15.
 */

public class FindUserPresenter implements FindUserContract.IPresenter{
    private FindUserContract.IView mFildUserView;
    private Context mContext;

    public FindUserPresenter(FindUserContract.IView mFildUserView) {
        this.mFildUserView = mFildUserView;
        this.mContext=mFildUserView.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void goToUserDetailActivity(String id) {
        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class, new UserDetailParcelable(id));
    }

    @Override
    public void setOnclick() {
        final String remoteId = mFildUserView.getRemoteId();
        if(!TextUtils.isEmpty(remoteId)){
            ApiManager.findUserInfo(remoteId, new IGetDataListener<UserDetailforOther>() {
                @Override
                public void onResult(UserDetailforOther userDetailforOther, boolean isEmpty) {
                    if(userDetailforOther!=null){
                        String isSucced = userDetailforOther.getIsSucced();
                        if (isSucced.equals("1")) {
                            UserDetail userDetail = userDetailforOther.getUserDetail();
                            if(userDetail!=null){
                                UserBase userBase = userDetail.getUserBase();
                                if(userBase!=null){
                                    int gender = userBase.getGender();
                                        mFildUserView.canGoToUserDetailActivity(String.valueOf(userBase.getGuid()));
                                }
                            }
                        }else{
                            mFildUserView.showIdOrMessageError(mContext.getString(R.string.find_account_miss));
                        }
                    }
                }
                @Override
                public void onError(String msg, boolean isNetworkError) {
                    mFildUserView.showIdOrMessageError(mContext.getString(R.string.find_account_miss));
                }
            });
        }else{
          mFildUserView.showTip(mContext.getString(R.string.find_edit_id));
        }
    }
}
