package com.popo.video.ui.match;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.NotificationHelper;
import com.popo.library.util.SnackBarUtil;
import com.popo.library.util.ToastUtil;
import com.popo.library.util.Utils;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.base.BaseAppCompatActivity;
import com.popo.video.common.CustomDialogAboutOther;
import com.popo.video.common.CustomDialogAboutPay;
import com.popo.video.common.HyphenateHelper;
import com.popo.video.common.Util;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.model.NettyMessage;
import com.popo.video.db.DbModle;
import com.popo.video.event.MessageArrive;
import com.popo.video.event.QAEvent;
import com.popo.video.event.SingleLoginFinishEvent;
import com.popo.video.event.UnreadMsgChangedEvent;
import com.popo.video.parcelable.MatchParcelable;
import com.popo.video.parcelable.PayParcelable;
import com.popo.video.ui.chat.ChatActivity;
import com.popo.video.ui.chat.ChatAdapter;
import com.popo.video.ui.main.MainActivity;
import com.popo.video.ui.pay.activity.RechargeActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MatchChatActivity extends BaseAppCompatActivity implements MatchChatContract.IView, View.OnClickListener {
    @BindView(R.id.chat_root)
    RelativeLayout mRlRoot;
    @BindView(R.id.iv_back_match_chat)
    ImageView iv_back_mc;
    @BindView(R.id.tv_title_match_chat)
    TextView tv_title_mc;
    @BindView(R.id.rl_bar_match_chat)
    RelativeLayout rl_bar_mc;
    @BindView(R.id.chat_switch)
    ImageView mIvSwitch;
    @BindView(R.id.chat_more)
    ImageView mIvMore;
    @BindView(R.id.chat_send)
    Button mBtnSend;
    @BindView(R.id.chat_voice)
    Button mBtnVoiceSend;
    @BindView(R.id.chat_text_input)
    EditText mEtInput;
    @BindView(R.id.chat_album)
    TextView mTvAlbum;
    @BindView(R.id.chat_camera)
    TextView mTvCamera;
    @BindView(R.id.chat_more_container)
    LinearLayout mLlMore;
    @BindView(R.id.chat_bottom)
    LinearLayout ll_bottom;
    @BindView(R.id.ll_chat_bottom_match)
    LinearLayout mLlChatBottomMatch;
    @BindView(R.id.chat_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_match_chat_with)
    TextView tv_match_mc;
    @BindView(R.id.tv_time_match_chat)
    TextView tv_time_mc;
    @BindView(R.id.iv_who_match_chat)
    ImageView iv_who_mc;
    @BindView(R.id.chat_activity_bottom_view)
    TextView view;
    @BindView(R.id.ll_empty_match_chat)
    LinearLayout ll_empty_mc;
    AnimationDrawable animationDrawable;
    LinearLayoutManager mLinearLayoutManager;
    // 异步任务
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == C.message.MSG_TYPE_VOICE_UI_TIME) {
                mRecordDuration++;
                mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_VOICE_UI_TIME, 1_000);
            } else {
                if (mChatPresenter != null) {
                    mChatPresenter.handleAsyncTask(msg);
                }
            }
        }
    };

    private MatchParcelable mChatParcelable;
    private ChatAdapter mChatAdapter;
    private MatchChatPresenter mChatPresenter;
    // 录音框
    private PopupWindow mRecordPopupWindow;
    private int mRecordDuration = 0;
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.chat_switch:// 切换文字/语音
                if (mChatParcelable.guid == 10000) {
                    Toast.makeText(MatchChatActivity.this, getString(R.string.not_chat_kefu_voice), Toast.LENGTH_SHORT).show();
                } else {
                    mBtnSend.setVisibility(View.GONE);
                    mIvMore.setVisibility(View.VISIBLE);
                    mLlMore.setVisibility(View.GONE);
                    // 触摸聊天列表时若键盘是打开的，则隐藏键盘
                    Util.hideKeyboard(MatchChatActivity.this, mEtInput);
                    mIvSwitch.setSelected(!mIvSwitch.isSelected());
                    if (mIvSwitch.isSelected()) { // 显示发送语音
                        mBtnVoiceSend.setText(getString(R.string.chat_press_to_speak));
                        mBtnVoiceSend.setVisibility(View.VISIBLE);
                        mEtInput.setVisibility(View.GONE);
                        mBtnSend.setEnabled(false);
                        mBtnSend.setTextColor(getResources().getColor(R.color.drak_gray));
                    } else { // 显示输入框
                        mBtnVoiceSend.setVisibility(View.GONE);
                        mEtInput.setVisibility(View.VISIBLE);
                        if (mEtInput.getText().length() > 0) {// 已经输入文字
                            mBtnSend.setEnabled(true);
                            mBtnSend.setTextColor(getResources().getColor(R.color.main_color));
                        } else {
                            mBtnSend.setEnabled(false);
                            mBtnSend.setTextColor(getResources().getColor(R.color.drak_gray));
                        }
                    }
                }

                break;

            case R.id.chat_send:// 发送
                mBtnSend.setVisibility(View.GONE);
                mIvMore.setVisibility(View.VISIBLE);
                mChatPresenter.sendTextMessage();
                break;

            case R.id.chat_more:// 显示更多
                // 触摸聊天列表时若键盘是打开的，则隐藏键盘
                Util.hideKeyboard(MatchChatActivity.this, mEtInput);
                if (mLlMore.isShown()) {
                    mLlMore.setVisibility(View.GONE);
                    mEtInput.requestFocus();
                } else {
                    mEtInput.clearFocus();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            mLlMore.setVisibility(View.VISIBLE);
                        }
                    }, 300);
                }
                break;

            case R.id.chat_album:// 相册
                mLlMore.setVisibility(View.GONE);
                mChatPresenter.sendImageMessage(false);
                break;

            case R.id.chat_camera:// 拍照
                mLlMore.setVisibility(View.GONE);
                mChatPresenter.sendImageMessage(true);
                break;
            case R.id.chat_root:
                mEtInput.clearFocus();
                break;
            case R.id.iv_back_match_chat:
                if (MainActivity.nowIsFinish()) {//Activity已经销毁了
                    LaunchHelper.getInstance().launchFinish(MatchChatActivity.this, MainActivity.class);
                } else {
                    finish();
                }
                break;
        }

    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mRlRoot, msg);
    }

    @Override
    public void showRecordPopupWindow(int state) {
        if (null == mRecordPopupWindow) {
            View contentView = LayoutInflater.from(mContext).inflate(R.layout.popup_chat_record, null);
            mRecordPopupWindow = new PopupWindow(contentView, WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT, true);
            // 设置背景透明
            mRecordPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            if (null != mRecordPopupWindow) {
                ImageView iv_animation = (ImageView) mRecordPopupWindow.getContentView().findViewById(R.id.popup_record_iv_duration);
                startAnimation(iv_animation);
            }
        }

        View view = mRecordPopupWindow.getContentView();
        if (null != view) {
            // 录音中
            LinearLayout llRecording = (LinearLayout) view.findViewById(R.id.popup_recording_container);
            // 上滑取消
            LinearLayout ll_cancle = (LinearLayout) view.findViewById(R.id.popup_recording_container_cancle);
            switch (state) {
                case C.message.STATE_RECORDING: // 正在录音
                    llRecording.setVisibility(View.VISIBLE);
                    ll_cancle.setVisibility(View.GONE);
                    break;

                case C.message.STATE_CANCELED: // 取消录音
                    llRecording.setVisibility(View.GONE);
                    ll_cancle.setVisibility(View.VISIBLE);
                    break;

                case C.message.STATE_IDLE:// 录音结束
                    llRecording.setVisibility(View.GONE);
                    ll_cancle.setVisibility(View.GONE);
                    break;
            }
        }
        // 居中显示
        mRecordPopupWindow.showAtLocation(mRlRoot, Gravity.CENTER, 0, 0);
    }

    private void startAnimation(ImageView iv_animation) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_1));
        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_2));
        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_3));
        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_4));
        animationDrawable = Utils.getFrameAnim(drawableList, true, 150);
        iv_animation.setImageDrawable(animationDrawable);
    }

    @Override
    public void dismissPopupWindow() {
        if (null != mRecordPopupWindow && mRecordPopupWindow.isShowing()) {
            mRecordPopupWindow.dismiss();
        }
    }

    @Override
    public String getInputText() {
        return mEtInput.getText().toString();
    }

    @Override
    public void clearInput() {
        mEtInput.setText("");
    }

    @Override
    public void hideRefresh() {

    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {

    }

    @Override
    public void scroollToBottom() {
        mLlMore.setVisibility(View.GONE);
        mLinearLayoutManager.scrollToPosition(mChatAdapter.getItemCount() - 1);
    }

    @Override
    public ChatAdapter getChatAdapter() {
        return mChatAdapter;
    }

    @Override
    public void sendMessage(long delayedMills, int msgType) {
        mHandler.sendEmptyMessageDelayed(msgType, delayedMills);
    }

    @Override
    public void sendMessage(Message message) {
        mHandler.sendMessage(message);
    }

    @Override
    public void showInterruptDialog(String tip) {
        ToastUtil.showShortToast(mContext, tip);
    }

    @Override
    protected void onResume() {
        super.onResume();
        HyphenateHelper.getInstance().isChatActivity();
    }

    @Override
    protected void onPause() {//当离开聊天界面打开通知的形式展示
        super.onPause();
        HyphenateHelper.getInstance().isNotChatActivity();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_match_chat;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mChatParcelable = (MatchParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return mRecyclerView;
    }

    @Override
    protected void initViews() {
        Util.hideKeyboard(MatchChatActivity.this, mEtInput);
        tv_match_mc.setText(getString(R.string.match_chat_with,mChatParcelable.nickName));
        ImageLoaderUtil.getInstance().loadImage(this,
                new com.popo.library.image.ImageLoader.Builder().url(mChatParcelable.iconUrlMininum).transform(new CropCircleTransformation(mContext))
                        .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(iv_who_mc).build());

        mLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        if (mChatParcelable != null) {
            mChatAdapter = new ChatAdapter(this, mChatParcelable.iconUrlMininum, getSupportFragmentManager());
            mRecyclerView.setAdapter(mChatAdapter);
            mChatPresenter = new MatchChatPresenter(this, mChatParcelable.account, mChatParcelable.guid, mChatParcelable.nickName, mChatParcelable.iconUrlMininum, 10);
            mChatPresenter.start();
            tv_title_mc.setText(mChatParcelable.nickName);
        }
        if (mChatParcelable != null) {
            NotificationHelper.getInstance(MatchChatActivity.this).cancel((int) mChatParcelable.guid);
            //判断，如果存在没有未读消息，却存在未读数量的情况清空
            if (DbModle.getInstance().getUserAccountDao().selectSqlit() == 0) {
                HyphenateHelper.getInstance().clearAllUnReadMsg();
            }
            EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
            List<HuanXinUser> allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
            if (allAcount != null && allAcount.size() > 0) {
                for (HuanXinUser huanXinUser : allAcount) {
                    if (huanXinUser != null) {
                        if (huanXinUser.getAccount().equals(mChatParcelable.account)) {
                            HuanXinUser accountByHyID = DbModle.getInstance().getUserAccountDao().getAccountByHyID(huanXinUser.getHxId());
                            if (accountByHyID.getMsgNum() > 0) {
                                mChatPresenter.msgRead(String.valueOf(mChatParcelable.guid));
                            }
                            DbModle.getInstance().getUserAccountDao().setMsgNum(huanXinUser);//把消息数量至为空
                            DbModle.getInstance().getUserAccountDao().setState(huanXinUser, true);//把标记设置为已经读取
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void setListeners() {
        iv_back_mc.setOnClickListener(this);
        mIvSwitch.setOnClickListener(this);
        mBtnSend.setOnClickListener(this);
        mIvMore.setOnClickListener(this);
        mTvAlbum.setOnClickListener(this);
        mTvCamera.setOnClickListener(this);

        mEtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mEtInput.getText().length() > 0) {
                    mBtnSend.setVisibility(View.VISIBLE);
                    mIvMore.setVisibility(View.GONE);
                    mBtnSend.setEnabled(true);
                    mBtnSend.setTextColor(getResources().getColor(R.color.main_color));
                    mBtnSend.setBackground(getResources().getDrawable(R.drawable.shape_round_rectangle_gray_border2));
                } else {
                    mBtnSend.setEnabled(false);
                    mBtnSend.setBackground(getResources().getDrawable(R.drawable.shape_round_rectangle_gray_border));
                    mBtnSend.setTextColor(getResources().getColor(R.color.drak_gray));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mBtnVoiceSend.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:// 按下时开始录音
                        mBtnVoiceSend.setText(getString(R.string.chat_loose_to_end));
                        mChatPresenter.handleTouchEventDown();
                        // 开始计时
                        mRecordDuration = 0;
                        // 播放动画
                        if (animationDrawable != null) {
                            animationDrawable.start();
                        }
                        mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_VOICE_UI_TIME, 1000);
                        break;

                    case MotionEvent.ACTION_MOVE:// 判断是否上滑取消
                        if (animationDrawable != null) {
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        mChatPresenter.handleTouchEventMove(event);
                        break;

                    case MotionEvent.ACTION_UP:// 松开时停止录音并发送
                        mBtnVoiceSend.setText(getString(R.string.chat_press_to_speak));
                        mChatPresenter.handleTouchEventUp();
                        if (animationDrawable != null) {
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        // 结束计时
                        mHandler.removeMessages(C.message.MSG_TYPE_VOICE_UI_TIME);
                        break;
                }
                return true;
            }
        });
        mEtInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                sendMessage(250, 100);
                return false;
            }
        });
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Subscribe
    public void onEvent(NettyMessage event) {
        Message msg = Message.obtain();
        msg.obj = event;
        msg.what = 1;
        msgHandler.sendMessage(msg);
    }
    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    closeShowPop();
                    break;
            }
        }
    };
    @Subscribe
    public void onEvent(QAEvent qaEvent) {
        CustomDialogAboutOther.qaMessageShow(MatchChatActivity.this, qaEvent.getQaMessage(), qaEvent.getUserAccount(), qaEvent.getGuid(), qaEvent.getQaMsg(), view);
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }

    @Subscribe
    public void onEvent(MessageArrive event) {
        if (event.getHunxinid().equals(String.valueOf(mChatParcelable.guid))) {
            mChatPresenter.initChatConversation();
            DbModle.getInstance().getUserAccountDao().setMsgNum(DbModle.getInstance().getUserAccountDao().getAccountByHyID(event.getHunxinid()));//把消息数量至为空
        }
    }

    @Override
    public LinearLayout getEmptyView() {
        return ll_empty_mc;
    }
}
