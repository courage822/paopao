package com.popo.video.ui.pay.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.RelativeLayout;

import com.popo.video.R;
import com.popo.video.base.BaseFragment;
import com.popo.library.adapter.wrapper.OnLoadMoreListener;
import com.popo.library.util.SnackBarUtil;
import com.popo.library.widget.AutoSwipeRefreshLayout;
import com.popo.library.widget.XRecyclerView;
import com.popo.video.ui.pay.contract.IncomeWithdrawalRecordContract;
import com.popo.video.ui.pay.presenter.IncomeWithdrawalRecordPresenter;
import com.popo.video.ui.personalcenter.adapter.IncomeRecordAdapter;
import com.popo.video.ui.personalcenter.adapter.WithdrawRecordAdapter;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/9/13.
 */

public class IncomeDetailFragment extends BaseFragment implements IncomeWithdrawalRecordContract.IView{
    @BindView(R.id.fragment_incomedetail_swiperefresh)
    AutoSwipeRefreshLayout mSwipeRefresh;
    @BindView(R.id.fragment_incomedetail_xrecyerview)
    XRecyclerView mRecyclerView;
    @BindView(R.id.fragment_incomedetail_rl_root)
    RelativeLayout rl_root;
    private IncomeWithdrawalRecordPresenter mPresenter;
    private boolean mIsFirstLoad;
    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_incomedetail;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return mSwipeRefresh;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mSwipeRefresh.setColorSchemeResources(R.color.main_color);
        mSwipeRefresh.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mPresenter=new IncomeWithdrawalRecordPresenter(this);
        mIsFirstLoad=true;
    }

    @Override
    protected void setListeners() {
        // 下拉刷新和上拉加载更多
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mPresenter.refresh1();
                }
            }
        });
//        mRecyclerView.setOnLoadingMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore() {
//                mPresenter.loadMore1();
//            }
//        });
    }

    @Override
    protected void loadData() {
        mPresenter.incomeHistory();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(rl_root, msg);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mPresenter.refresh1();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowEmpty(false, null, null);
                mPresenter.refresh1();
            }
        });
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return null;
    }



    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefresh.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null != mSwipeRefresh && mSwipeRefresh.isRefreshing()) {
                    mSwipeRefresh.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void showLoadMore() {
        mRecyclerView.setVisibility(R.id.load_more_progress, true);
        mRecyclerView.setVisibility(R.id.load_more_msg, true);
        mRecyclerView.setVisibility(R.id.load_more_empty, false);
        mRecyclerView.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
        mRecyclerView.finishLoadMore();
    }

    @Override
    public void showNoMore() {
        mRecyclerView.setVisibility(R.id.load_more_empty, true);
        mRecyclerView.setVisibility(R.id.load_more_progress, false);
        mRecyclerView.setVisibility(R.id.load_more_msg, false);
    }
    @Override
    public WithdrawRecordAdapter getWithdrawRecordAdapter() {
        return null;
    }

    @Override
    public IncomeRecordAdapter getIncomeRecordAdapter() {
        return null;
    }

    @Override
    public void setAdapter(IncomeRecordAdapter mIncomeRecordAdapter) {
        mRecyclerView.setAdapter(mIncomeRecordAdapter, R.layout.common_load_more);
    }

    @Override
    public void setAdapter(WithdrawRecordAdapter mWithdrawRecordAdapter) {

    }

    @Override
    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        mRecyclerView.setOnLoadingMoreListener(onLoadMoreListener);
    }
}
