package com.popo.video.ui.login;

import android.content.Context;
import android.text.TextUtils;

import com.popo.video.R;
import com.popo.video.common.HyphenateHelper;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.FindPassword;
import com.popo.video.data.model.HostInfo;
import com.popo.video.data.model.Login;
import com.popo.video.data.model.PlatformInfo;
import com.popo.video.data.model.UserBase;
import com.popo.video.data.model.UserBean;
import com.popo.video.data.model.UserDetail;
import com.popo.video.data.preference.AnchorPreference;
import com.popo.video.data.preference.BeanPreference;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.event.FinishEvent;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.ToastUtil;
import com.popo.video.ui.main.MainActivity;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by zhangdroid on 2017/6/1.
 */
public class LoginPresenter implements LoginContract.IPresenter {
    private LoginContract.IView mLoginView;
    private Context mContext;

    public LoginPresenter(LoginContract.IView view) {
        this.mLoginView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
        // 获取本地保存的账号密码并设置
        String account = UserPreference.getAccount();
        String password = UserPreference.getPassword();
        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(password)) {
            mLoginView.setAccount(account);
            mLoginView.setPassword(password);
        }
    }

    @Override
    public void login() {
        mLoginView.showLoading();
        PlatformInfo platformInfo = PlatformPreference.getPlatformInfo();
        if (TextUtils.isEmpty(platformInfo.getCountry())) {
            platformInfo.setCountry("United States");
            platformInfo.setFid("30101");
            PlatformPreference.setPlatfromInfo(platformInfo);
        }
        ApiManager.login(mLoginView.getAccount(), mLoginView.getPassword(), new IGetDataListener<Login>() {
            @Override
            public void onResult(Login login, boolean isEmpty) {
                if (!TextUtils.isEmpty(login.getIslj())){
                    UserPreference.setIslj(login.getIslj());
                }
                UserDetail userDetail = login.getUserDetail();
                if (null != userDetail) {
                    if (!mLoginView.getAccount().equals(UserPreference.getAccount())) {
                        HyphenateHelper.getInstance().clearAllUnReadMsg();//清空所有未读消息
                        DbModle.getInstance().getUserAccountDao().deleteAllData();//删除数据库中的所有数据
                        DbModle.getInstance().getUserAccountDao().deleteQaAllData();//删除QA
                    }
                    // 保存用户相关信息
                    UserBean userBean = userDetail.getUserBean();
                    if (null != userBean) {
                        BeanPreference.saveUserBean(userBean);
                    }
                    HostInfo hostInfo = userDetail.getHostInfo();
                    if (null != hostInfo) {
                        AnchorPreference.saveHostInfo(hostInfo);
                    }
                    UserBase userBase = userDetail.getUserBase();
                    if (null != userBase) {
                        // 保存用户信息到本地
                        UserPreference.saveUserInfo(userBase);
                        PlatformInfo platformInfo = PlatformPreference.getPlatformInfo();
                        platformInfo.setFid(userBase.getFromChannel() + "");
                        platformInfo.setCountry(DataPreference.getCountry(userBase.getCountry()));
                        UserPreference.setCountryId(userBase.getCountry());
                        PlatformPreference.setPlatfromInfo(platformInfo);
                        handleHyphenateLoginResult();
                    } else {
                        handleHyphenateLoginResult();
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mLoginView.dismissLoading();
                if (isNetworkError) {
                    mLoginView.showNetworkError();
                } else {
                    mLoginView.showTip(msg);
                }
            }
        });
    }

    @Override
    public void findPassword() {
        mLoginView.showLoading();
        ApiManager.findPassword(new IGetDataListener<FindPassword>() {
            @Override
            public void onResult(FindPassword findPassword, boolean isEmpty) {
                mLoginView.dismissLoading();
                if (!TextUtils.isEmpty(findPassword.getAccount())) {
                    mLoginView.showFindPwdDialog(findPassword.getAccount(), findPassword.getPassword());
                } else {
                    mLoginView.showFindPwdDialog(mContext.getString(R.string.nothing), mContext.getString(R.string.nothing));
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mLoginView.dismissLoading();
            }

        });
    }

    private void handleHyphenateLoginResult() {
        mLoginView.dismissLoading();
        ToastUtil.showShortToast(mContext, mContext.getString(R.string.login_success));
        // 关闭之前打开的页面
        EventBus.getDefault().post(new FinishEvent());
        // 跳转主页面
        LaunchHelper.getInstance().launchFinish(mContext, MainActivity.class);
    }

}
