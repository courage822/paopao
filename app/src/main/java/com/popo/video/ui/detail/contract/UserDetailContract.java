package com.popo.video.ui.detail.contract;

import android.support.v4.app.FragmentManager;

import com.popo.video.data.model.UserDetail;
import com.popo.video.data.model.UserDetailforOther;
import com.popo.video.data.model.UserPhoto;
import com.popo.video.data.model.VideoOrImage;
import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;
import com.popo.video.ui.detail.adapter.UserAlbumPhotoAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/6/7.
 */

public interface UserDetailContract {
    interface IView extends BaseView {
        String getUserId();

        void showLoading();

        void dismissLoading();

        void showNetworkError();

        void followSucceed();

        //获取是否关注的判断字段
        void getFollowOrUnFollow(UserDetail userDetail);

        //获取现在的状态的回调
        void getStatus(String status);

        /**
         * @return 获得FragmentManager，用于显示对话框
         */
        FragmentManager obtainFragmentManager();

        /**
         * 判断是否是主播的方法
         */
        void isAnchor();

        /**
         * 拦截主播
         */
        void innerAnchor(String tip);

        /**
         * 设置主播的价格
         */
        void setAnchorPrice(int price);

        /**
         * 昵称
         */
        void setNickName(String nickName);

        /**
         * 性别
         */
        void setSex(String sex);

        /**
         * 年龄
         */
        void setAge(String age);

        /**
         * 身高
         */
        void setHeight(String height);

        /**
         * 情感状态
         */
        void setMerital_status(String status);

        /**
         * 体重
         */
        void setWeight(String weight);

        /**
         * 星座
         */
        void setSign(String sign);

        /**
         * 头像
         */
        void setAvatar(String avatar);

        /**
         * 用户ID
         */
        void setUserId(String userId);

        /**
         * 是否是男用户
         */
        void isMail(boolean flag);

        /**
         * 视频或者邀视频按钮的字样
         */
        void videoInvite(String video);

        /**
         * 我是否是主播
         */
        void meIsAuthor(boolean flag);

        /**
         * 是否已经打招呼了
         */
        void isSayHello();

        /**
         * banner展示
         */
        void startBanner(List<VideoOrImage> list);

        /**
         * 展示签名
         *
         * @param ownWords
         */
        void getOwnWords(String ownWords);
    }

    interface IPresenter extends BasePresenter {

        /**
         * 获取用户信息
         */
        void getUserInfoData();

        /**
         * 关注
         */
        void follow(String remoteUid);

        /**
         * 取消关注
         */
        void unFollow(String remoteUid);

        /**
         * 跳转视频聊天界面
         */
        void goVideoCallPage();

        /**
         * 跳转写信界面
         */
        void goWriteMessagePage();

        /**
         * 点击头像跳转到图片浏览界面
         */
        void goToAvatarViewPager();

        /**
         * 打招呼
         */
        void sayHelloClick();

        /**
         * 视频邀请
         */
        void sendVideoInvite(String remoteId);

        /**
         * 语音邀请
         */
        void sendVoiceInvite(String remoteId);
        /**
         * 获取审核的状态
         */
        void getCheckStatus();

    }
}
