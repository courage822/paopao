package com.popo.video.ui.register;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.library.dialog.LoadingDialog;
import com.popo.library.net.NetUtil;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.R;
import com.popo.video.base.BaseTopBarActivity;
import com.popo.video.common.Util;
import com.popo.video.data.constant.DataDictManager;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.ui.dialog.DialogUtil;
import com.popo.video.ui.dialog.OneWheelDialog;
import com.popo.video.ui.photo.GetPhotoActivity;
import com.popo.video.ui.register.contract.CompleteInfoContract;
import com.popo.video.ui.register.presenter.CompleteInfoPresenter;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/10/24.
 */

public class CompleteInfoActivity extends BaseTopBarActivity implements View.OnClickListener, CompleteInfoContract.IView {
    @BindView(R.id.complete_info_iv_avatar)
    ImageView iv_avatar;
    @BindView(R.id.complete_info_tv_height)
    TextView tv_height;
    @BindView(R.id.complete_info_tv_qinggan)
    TextView tv_qinggan;
    @BindView(R.id.complete_info_tv_sign)
    TextView tv_sign;
    @BindView(R.id.complete_info_btn_finish)
    Button btn_finish;
    @BindView(R.id.complete_info_ll_height)
    LinearLayout ll_height;
    @BindView(R.id.complete_info_rl_qinggan)
    RelativeLayout rl_qinggan;
    @BindView(R.id.complete_info_ll_sign)
    LinearLayout ll_sign;
    @BindView(R.id.iv_avatar_desc)
    ImageView iv_avatar_desc;
    @BindView(R.id.complete_info_ll_root)
    LinearLayout ll_root;
    @BindView(R.id.complete_info_iv_zhezhao)
    ImageView iv_zhezhao;
    @BindView(R.id.complete_info_tv_shangchaunzhong)
    TextView tv_shangchaunzhong;
    private CompleteInfoPresenter completeInfoPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_complete_info;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return null;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        completeInfoPresenter = new CompleteInfoPresenter(this);
        if (Util.getLacalLanguage().equals("English")) {
            iv_avatar_desc.setVisibility(View.GONE);
        } else {
            iv_avatar_desc.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void setListeners() {
        ll_height.setOnClickListener(this);
        rl_qinggan.setOnClickListener(this);
        //      tv_sign.setOnClickListener(this);
        btn_finish.setOnClickListener(this);
        ll_sign.setOnClickListener(this);
        iv_avatar.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        completeInfoPresenter.loadInfoData();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.complete_info_ll_height:
                if (!TextUtils.isEmpty(UserPreference.getCountryId()) && UserPreference.getCountryId().equals("97")) {
                    DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), true, DataPreference.getInchList(), getString(R.string.complete_height), getString(R.string.positive), getString(R.string.cancel), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
                        @Override
                        public void onPositiveClick(View view, String selectedText) {
                            tv_height.setText(selectedText);
                        }

                        @Override
                        public void onNegativeClick(View view) {

                        }
                    });
                } else {
                    DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), true, DataPreference.getInchCmList(), getString(R.string.complete_height), getString(R.string.positive), getString(R.string.cancel), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
                        @Override
                        public void onPositiveClick(View view, String selectedText) {
                            tv_height.setText(selectedText);
                        }

                        @Override
                        public void onNegativeClick(View view) {

                        }
                    });
                }
                break;
            case R.id.complete_info_rl_qinggan:
                if (DataPreference.getRelationshipMapValue() != null && DataPreference.getRelationshipMapValue().size() != 0) {
                    DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataPreference.getRelationshipMapValue(), getString(R.string.marraige), getString(R.string.positive), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                        @Override
                        public void onPositiveClick(View view, String selectedText) {
                            tv_qinggan.setText(selectedText);
                        }

                        @Override
                        public void onNegativeClick(View view) {
                        }
                    });
                }
                break;
            case R.id.complete_info_ll_sign:
                if (DataDictManager.getSignList() != null && DataDictManager.getSignList().size() != 0) {
                    DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataDictManager.getSignList(), getString(R.string.complete_signs), getString(R.string.positive), getString(R.string.cancel), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
                        @Override
                        public void onPositiveClick(View view, String selectedText) {
                            tv_sign.setText(selectedText);
                        }

                        @Override
                        public void onNegativeClick(View view) {

                        }
                    });
                }
                break;
            case R.id.complete_info_iv_avatar:
                GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        completeInfoPresenter.upLoadAvator(file, true);
                        String canonicalPath = null;
                        try {
                            canonicalPath = file.getCanonicalPath();
                            Bitmap bitmap = BitmapFactory.decodeFile(canonicalPath, null);
                            iv_avatar.setImageBitmap(bitmap);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                });
                break;
            case R.id.complete_info_btn_finish:
                completeInfoPresenter.completeInfo();
                break;
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(ll_root,msg);
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public String getHeigth() {
        return tv_height.getText().toString();
    }

    @Override
    public String getQingGan() {
        return tv_qinggan.getText().toString();
    }

    @Override
    public String getSign() {
        return tv_sign.getText().toString();
    }

    @Override
    public void setUserAvator(String url) {
        iv_avatar.setImageResource(R.drawable.check_circle_icon);
    }

    @Override
    public void canOnClick() {
        btn_finish.setEnabled(true);
    }

    @Override
    public void getPhotoFile(File file) {

    }

    @Override
    public void isSeeZheZhao(boolean flag) {
        if (flag) {
            tv_shangchaunzhong.setVisibility(View.VISIBLE);
        }else{
            tv_shangchaunzhong.setVisibility(View.GONE);
            iv_zhezhao.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void uploadFail() {
        tv_shangchaunzhong.setVisibility(View.GONE);
        iv_zhezhao.setVisibility(View.GONE);
        iv_avatar.setImageResource(R.drawable.upload_myicon);
    }
}
