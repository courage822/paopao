package com.popo.video.ui.pay.fragment;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.base.BaseFragment;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.ui.pay.adapter.PayAdapter;
import com.popo.video.ui.pay.contract.RechargeContract;
import com.popo.video.ui.pay.presenter.PurchaseKeyPresenter;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/8/29.
 */

public class PurchaseKeyFragment extends BaseFragment implements RechargeContract.IView {
    @BindView(R.id.fragment_purchase_product_ll_root)
    LinearLayout mLlRoot;
    @BindView(R.id.fragment_purchase_product_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.fragment_purchase_product_list_tv_detail)
    TextView tv_detail;
    @BindView(R.id.fragment_purchase_product_tv_num)
    TextView tv_num;
    private PurchaseKeyPresenter mPurchaseKeyPresenter;
    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_purchase_layout;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mPurchaseKeyPresenter=new PurchaseKeyPresenter(this);
        mPurchaseKeyPresenter.getPayWay(C.pay.FROM_TAG_PERSON);
        mPurchaseKeyPresenter.start();
    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void dismissLoading() {
        toggleShowLoading(false, null);
    }

    @Override
    public void showNetworkError() {
    }

    @Override
    public void setAdapter(PagerAdapter pagerAdapter) {

    }

    @Override
    public FragmentManager getManager() {
        return null;
    }

    @Override
    public void setAdapter(PayAdapter payAdapter) {
        if (null != payAdapter) {
            payAdapter.setPayPresenter(mPurchaseKeyPresenter);
            mRecyclerView.setAdapter(payAdapter);
        }
    }

    @Override
    public void setTextDetail(String detial) {
        tv_detail.setText(detial);
    }

    @Override
    public void getKeyNum(String num) {
        tv_num.setText(num);
    }

    @Override
    public void getNowMoney(String money) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
