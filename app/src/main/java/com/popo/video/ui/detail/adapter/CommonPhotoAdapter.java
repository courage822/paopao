package com.popo.video.ui.detail.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.popo.library.util.Utils;

import java.util.List;

/**
 * 通用照片墙适配器（详情页）
 * Created by zhangdroid on 2017/6/2.
 */
public class CommonPhotoAdapter extends PagerAdapter {
    private List<ImageView> mPhotoViews;

    public CommonPhotoAdapter(List<ImageView> list) {
        this.mPhotoViews = list;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = mPhotoViews.get(position);
        if (container.getChildAt(position) == imageView) {
            container.removeViewAt(position);
        }
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(mPhotoViews.get(position));
    }

    @Override
    public int getCount() {
        return Utils.isListEmpty(mPhotoViews) ? 0 : mPhotoViews.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

}
