package com.popo.video.ui.chat;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.popo.library.net.NetUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.NotificationHelper;
import com.popo.library.util.SnackBarUtil;
import com.popo.library.util.ToastUtil;
import com.popo.library.util.Utils;
import com.popo.library.widget.AutoSwipeRefreshLayout;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.base.BaseAppCompatActivity;
import com.popo.video.common.CustomDialogAboutOther;
import com.popo.video.common.CustomDialogAboutPay;
import com.popo.video.common.HyphenateHelper;
import com.popo.video.common.TimeUtils;
import com.popo.video.common.Util;
import com.popo.video.customload.blurview.BlurView;
import com.popo.video.customload.blurview.SupportRenderScriptBlur;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.model.NettyMessage;
import com.popo.video.data.preference.PayPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.event.MessageArrive;
import com.popo.video.event.QAEvent;
import com.popo.video.event.SeeDetailEvent;
import com.popo.video.event.SingleLoginFinishEvent;
import com.popo.video.event.UnreadMsgChangedEvent;
import com.popo.video.parcelable.ChatParcelable;
import com.popo.video.parcelable.PayParcelable;
import com.popo.video.parcelable.UserDetailParcelable;
import com.popo.video.parcelable.VideoShowParcelable;
import com.popo.video.ui.detail.UserDetailActivity;
import com.popo.video.ui.main.MainActivity;
import com.popo.video.ui.pay.activity.RechargeActivity;
import com.popo.video.ui.personalcenter.VideoShowActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 聊天页面
 * Created by zhangdroid on 2017/5/27.
 */
public class ChatActivity extends BaseAppCompatActivity implements ChatContract.IView, View.OnClickListener {
    @BindView(R.id.chat_root)
    RelativeLayout mRlRoot;
    @BindView(R.id.chat_refresh)
    AutoSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.chat_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.chat_switch)
    ImageView mIvSwitch;
    @BindView(R.id.chat_text_input)
    EditText mEtInput;
    @BindView(R.id.chat_voice)
    Button mBtnVoiceSend;
    @BindView(R.id.chat_send)
    Button mBtnSend;
    @BindView(R.id.chat_more)
    ImageView mIvMore;
    @BindView(R.id.chat_more_container)
    LinearLayout mLlMore;
    @BindView(R.id.chat_album)
    TextView mTvAlbum;
    @BindView(R.id.chat_camera)
    TextView mTvCamera;
    @BindView(R.id.chat_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.chat_activity_tv_nickname)
    TextView tv_name;
    @BindView(R.id.chat_bottom)
    LinearLayout ll_bottom;
    @BindView(R.id.chat_activity_rl_set)
    RelativeLayout rl_set;
    @BindView(R.id.chat_activity_iv_send_gifts)
    ImageView iv_send_gifts;
    @BindView(R.id.chat_activity_bottom_view)
    TextView view;
    @BindView(R.id.blurview)
    BlurView blurview;
    @BindView(R.id.btn_tovip)
    Button btn_tovip;
    // 录音框
    private PopupWindow mRecordPopupWindow;
    private int mRecordDuration = 0;
    // 标记是否初次加载
    private boolean mIsFirstLoad;
    private boolean mIsBlock = false;
    private LinearLayoutManager mLinearLayoutManager;
    private ChatAdapter mChatAdapter;
    private ChatParcelable mChatParcelable;
    private ChatPresenter mChatPresenter;
    AnimationDrawable animationDrawable;
    private long lastClickTime = 0;
    // 异步任务
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == C.message.MSG_TYPE_VOICE_UI_TIME) {
                mRecordDuration++;
                mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_VOICE_UI_TIME, 1_000);
            } else {
                if (mChatPresenter != null) {
                    mChatPresenter.handleAsyncTask(msg);
                }
            }
        }
    };

    private void startAnimation(ImageView iv_animation) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_1));
        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_2));
        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_3));
        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_4));
        animationDrawable = Utils.getFrameAnim(drawableList, true, 150);
        iv_animation.setImageDrawable(animationDrawable);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_chat;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }


    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mChatParcelable = (ChatParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return mSwipeRefreshLayout;
    }


    @Override
    protected void initViews() {
        // 设置Notice替换View
        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
        mLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        if (UserPreference.isVip() || UserPreference.isAnchor() || mChatParcelable.guid == 10000){
            blurview.setVisibility(View.GONE);
        }else{
            final Drawable windowBackground = getWindow().getDecorView().getBackground();
            blurview.setupWith(mRlRoot)
                    .windowBackground(windowBackground)
                    .blurAlgorithm(new SupportRenderScriptBlur(this))
                    .blurRadius(1)
                    .setHasFixedTransformationMatrix(true);
        }

        if (mChatParcelable != null) {
            mChatAdapter = new ChatAdapter(this, mChatParcelable.imageUrl, getSupportFragmentManager());
            mRecyclerView.setAdapter(mChatAdapter);
            mChatPresenter = new ChatPresenter(this, mChatParcelable.account, mChatParcelable.guid, mChatParcelable.nickname, mChatParcelable.imageUrl, 1);
            mChatPresenter.start();
            tv_name.setText(mChatParcelable.nickname);
            if (UserPreference.isAnchor() || mChatParcelable.guid == 10000) {
                iv_send_gifts.setVisibility(View.GONE);
            }
        }
        if (mChatParcelable != null) {
            NotificationHelper.getInstance(ChatActivity.this).cancel((int) mChatParcelable.guid);
            //判断，如果存在没有未读消息，却存在未读数量的情况清空
            if (DbModle.getInstance().getUserAccountDao().selectSqlit() == 0) {
                HyphenateHelper.getInstance().clearAllUnReadMsg();
            }
            EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
            List<HuanXinUser> allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
            if (allAcount != null && allAcount.size() > 0) {
                for (HuanXinUser huanXinUser : allAcount) {
                    if (huanXinUser != null) {
                        if (huanXinUser.getAccount().equals(mChatParcelable.account)) {
                            HuanXinUser accountByHyID = DbModle.getInstance().getUserAccountDao().getAccountByHyID(huanXinUser.getHxId());
                            if (accountByHyID.getMsgNum() > 0) {
                                mChatPresenter.msgRead(String.valueOf(mChatParcelable.guid));
                            }
                            DbModle.getInstance().getUserAccountDao().setMsgNum(huanXinUser);//把消息数量至为空
                            DbModle.getInstance().getUserAccountDao().setState(huanXinUser, true);//把标记设置为已经读取
                        }
                    }
                }
            }
            if (mChatParcelable.guid == 10000) {
                rl_set.setVisibility(View.GONE);
            }
        }
        mIsFirstLoad = true;
        if (TimeUtils.timeIsPast()) {//判断页脚是否过期
            msgHandler.sendEmptyMessage(1);
        } else if (!TimeUtils.timeIsPast()) {
            msgHandler.sendEmptyMessage(2);
        }
        mChatPresenter.isBlock();//进入界面先判断是否拉黑或者被拉黑
    }

    @Override
    protected void setListeners() {
        mIvSwitch.setOnClickListener(this);
        mBtnSend.setOnClickListener(this);
        mIvMore.setOnClickListener(this);
        mTvAlbum.setOnClickListener(this);
        mTvCamera.setOnClickListener(this);
        mRlRoot.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_set.setOnClickListener(this);
        iv_send_gifts.setOnClickListener(this);
        btn_tovip.setOnClickListener(this);
        // 下拉刷新，加载之前的聊天记录
        mSwipeRefreshLayout.setDistanceToTriggerSync(250);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mChatPresenter.setRefresh2();
            }
        });
        mEtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mEtInput.getText().length() > 0) {
                    mBtnSend.setVisibility(View.VISIBLE);
                    mIvMore.setVisibility(View.GONE);
                    mBtnSend.setEnabled(true);
                    mBtnSend.setTextColor(getResources().getColor(R.color.main_color));
                    mBtnSend.setBackground(getResources().getDrawable(R.drawable.shape_round_rectangle_gray_border2));
                } else {
                    mBtnSend.setEnabled(false);
                    mBtnSend.setBackground(getResources().getDrawable(R.drawable.shape_round_rectangle_gray_border));
                    mBtnSend.setTextColor(getResources().getColor(R.color.drak_gray));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mBtnVoiceSend.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:// 按下时开始录音
                        mBtnVoiceSend.setText(getString(R.string.chat_loose_to_end));
                        mChatPresenter.handleTouchEventDown();
                        // 开始计时
                        mRecordDuration = 0;
                        // 播放动画
                        if (animationDrawable != null) {
                            animationDrawable.start();
                        }
                        mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_VOICE_UI_TIME, 1000);
                        break;

                    case MotionEvent.ACTION_MOVE:// 判断是否上滑取消
                        if (animationDrawable != null) {
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        mChatPresenter.handleTouchEventMove(event);
                        break;

                    case MotionEvent.ACTION_UP:// 松开时停止录音并发送
                        mBtnVoiceSend.setText(getString(R.string.chat_press_to_speak));
                        mChatPresenter.handleTouchEventUp();
                        if (animationDrawable != null) {
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        // 结束计时
                        mHandler.removeMessages(C.message.MSG_TYPE_VOICE_UI_TIME);
                        break;
                }
                return true;
            }
        });
        // 处理滑动冲突
        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // 触摸聊天列表时若键盘是打开的，则隐藏键盘
                Util.hideKeyboard(ChatActivity.this, mEtInput);
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        // 禁用下拉刷新
                        mSwipeRefreshLayout.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        // 恢复下拉刷新
                        mSwipeRefreshLayout.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
        mEtInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                sendMessage(250, 100);
                return false;
            }
        });
        // 处理滑动冲突
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // 判断是否滑动到顶部
                int scrolledPosition = (mRecyclerView == null || mRecyclerView.getChildCount() == 0) ? 0 : mRecyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(scrolledPosition >= 0);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chat_switch:// 切换文字/语音
                if (mChatParcelable.guid == 10000) {
                    Toast.makeText(ChatActivity.this, getString(R.string.not_chat_kefu_voice), Toast.LENGTH_SHORT).show();
                } else {
                    mBtnSend.setVisibility(View.GONE);
                    mIvMore.setVisibility(View.VISIBLE);
                    mLlMore.setVisibility(View.GONE);
                    // 触摸聊天列表时若键盘是打开的，则隐藏键盘
                    Util.hideKeyboard(ChatActivity.this, mEtInput);
                    mIvSwitch.setSelected(!mIvSwitch.isSelected());
                    if (mIvSwitch.isSelected()) { // 显示发送语音
                        mBtnVoiceSend.setText(getString(R.string.chat_press_to_speak));
                        mBtnVoiceSend.setVisibility(View.VISIBLE);
                        mEtInput.setVisibility(View.GONE);
                        mBtnSend.setEnabled(false);
                        mBtnSend.setTextColor(getResources().getColor(R.color.drak_gray));
                    } else { // 显示输入框
                        mBtnVoiceSend.setVisibility(View.GONE);
                        mEtInput.setVisibility(View.VISIBLE);
                        if (mEtInput.getText().length() > 0) {// 已经输入文字
                            mBtnSend.setEnabled(true);
                            mBtnSend.setTextColor(getResources().getColor(R.color.main_color));
                        } else {
                            mBtnSend.setEnabled(false);
                            mBtnSend.setTextColor(getResources().getColor(R.color.drak_gray));
                        }
                    }
                }

                break;

            case R.id.chat_send:// 发送
                mBtnSend.setVisibility(View.GONE);
                mIvMore.setVisibility(View.VISIBLE);
                mChatPresenter.sendTextMessage();
                break;

            case R.id.chat_more:// 显示更多
                // 触摸聊天列表时若键盘是打开的，则隐藏键盘
                Util.hideKeyboard(ChatActivity.this, mEtInput);
                if (mLlMore.isShown()) {
                    mLlMore.setVisibility(View.GONE);
                    mEtInput.requestFocus();
                } else {
                    mEtInput.clearFocus();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            mLlMore.setVisibility(View.VISIBLE);
                        }
                    }, 300);
                }
                break;

            case R.id.chat_album:// 相册
                mLlMore.setVisibility(View.GONE);
                mChatPresenter.sendImageMessage(false);
                break;

            case R.id.chat_camera:// 拍照
                mLlMore.setVisibility(View.GONE);
                mChatPresenter.sendImageMessage(true);
                break;
            case R.id.chat_root:
                mEtInput.clearFocus();
                break;
            case R.id.chat_activity_rl_back:
                if (MainActivity.nowIsFinish()) {//Activity已经销毁了
                    LaunchHelper.getInstance().launchFinish(ChatActivity.this, MainActivity.class);
                } else {
                    finish();
                }
                break;
            case R.id.chat_activity_rl_set:
                if (mChatParcelable != null) {
                    CustomDialogAboutPay.reportShow(ChatActivity.this, String.valueOf(mChatParcelable.guid));
                }
                break;
            case R.id.chat_activity_iv_send_gifts:
                if (mChatParcelable != null) {
                    if (System.currentTimeMillis() - lastClickTime > 1000) {
                        lastClickTime = System.currentTimeMillis();
                        CustomDialogAboutOther.giveGiftShow(ChatActivity.this, String.valueOf(mChatParcelable.guid), mChatParcelable.account, mChatParcelable.imageUrl, mChatParcelable.nickname, 0, false);
                    }
                }
                break;
            case R.id.btn_tovip:
                LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 0));
                break;
        }
    }

    @Override
    protected void loadData() {
        mChatPresenter.initChatConversation();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mChatPresenter.finish();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mRlRoot, msg);
    }

    @Override
    public void showRecordPopupWindow(int state) {
        if (null == mRecordPopupWindow) {
            View contentView = LayoutInflater.from(mContext).inflate(R.layout.popup_chat_record, null);
            mRecordPopupWindow = new PopupWindow(contentView, WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT, true);
            // 设置背景透明
            mRecordPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            if (null != mRecordPopupWindow) {
                ImageView iv_animation = (ImageView) mRecordPopupWindow.getContentView().findViewById(R.id.popup_record_iv_duration);
                startAnimation(iv_animation);
            }
        }

        View view = mRecordPopupWindow.getContentView();
        if (null != view) {
            // 录音中
            LinearLayout llRecording = (LinearLayout) view.findViewById(R.id.popup_recording_container);
            // 上滑取消
            LinearLayout ll_cancle = (LinearLayout) view.findViewById(R.id.popup_recording_container_cancle);
            switch (state) {
                case C.message.STATE_RECORDING: // 正在录音
                    llRecording.setVisibility(View.VISIBLE);
                    ll_cancle.setVisibility(View.GONE);
                    break;

                case C.message.STATE_CANCELED: // 取消录音
                    llRecording.setVisibility(View.GONE);
                    ll_cancle.setVisibility(View.VISIBLE);
                    break;

                case C.message.STATE_IDLE:// 录音结束
                    llRecording.setVisibility(View.GONE);
                    ll_cancle.setVisibility(View.GONE);
                    break;
            }
        }
        // 居中显示
        mRecordPopupWindow.showAtLocation(mRlRoot, Gravity.CENTER, 0, 0);

    }

    @Override
    public void dismissPopupWindow() {
        if (null != mRecordPopupWindow && mRecordPopupWindow.isShowing()) {
            mRecordPopupWindow.dismiss();
        }
    }

    @Override
    public String getInputText() {
        return mEtInput.getText().toString();
    }

    @Override
    public void clearInput() {
        mEtInput.setText("");
    }

    @Override
    public void hideRefresh() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
//        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mChatPresenter.refresh();
//            }
//        });
    }

    @Override
    public void scroollToBottom() {
        mLlMore.setVisibility(View.GONE);
        mLinearLayoutManager.scrollToPosition(mChatAdapter.getItemCount() - 1);
    }

    @Override
    public ChatAdapter getChatAdapter() {
        return mChatAdapter;
    }

    @Override
    public void sendMessage(long delayedMills, int msgType) {
        mHandler.sendEmptyMessageDelayed(msgType, delayedMills);
    }

    @Override
    public void sendMessage(Message message) {
        mHandler.sendMessage(message);
    }

    @Override
    public void showInterruptDialog(String tip) {
        ToastUtil.showShortToast(mContext, tip);
    }

    @Subscribe
    public void onEvent(SeeDetailEvent event) {
        if (mChatParcelable != null) {
            if (mChatParcelable.guid == 10000) {

            } else {
                LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                        new UserDetailParcelable(String.valueOf(mChatParcelable.guid)));
            }
        }
    }

    @Subscribe
    public void onEvent(MessageArrive event) {
        if (event.getHunxinid().equals(String.valueOf(mChatParcelable.guid))) {
            mChatPresenter.initChatConversation();
            DbModle.getInstance().getUserAccountDao().setMsgNum(DbModle.getInstance().getUserAccountDao().getAccountByHyID(event.getHunxinid()));//把消息数量至为空
        }
    }

    @Override
    protected void onResume() {//当离开聊天界面的时候消息提醒以通知的形式展示
        super.onResume();
        HyphenateHelper.getInstance().isChatActivity();
        if (!UserPreference.islj()
                || (UserPreference.islj() &&UserPreference.isVip())
                || UserPreference.isAnchor()
                || mChatParcelable.guid == 10000){
            blurview.setVisibility(View.GONE);
        }else{
            blurview.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onPause() {//当离开聊天界面打开通知的形式展示
        super.onPause();
        HyphenateHelper.getInstance().isNotChatActivity();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (MainActivity.nowIsFinish()) {//Activity已经销毁了
            LaunchHelper.getInstance().launchFinish(ChatActivity.this, MainActivity.class);
        } else {
            finish();
        }
    }

    @Subscribe
    public void onEvent(QAEvent qaEvent) {
        CustomDialogAboutOther.qaMessageShow(ChatActivity.this, qaEvent.getQaMessage(), qaEvent.getUserAccount(), qaEvent.getGuid(), qaEvent.getQaMsg(), view);
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }

    @Subscribe
    public void onEvent(NettyMessage event) {
        Message msg = Message.obtain();
        msg.obj = event;
        msg.what = 1;
        msgHandler.sendMessage(msg);
    }

    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    closeShowPop();
                    break;
            }
        }
    };

    @Override
    protected void closeShowPop() {
        super.closeShowPop();
    }


    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }
}