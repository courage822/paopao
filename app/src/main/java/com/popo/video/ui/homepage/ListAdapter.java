package com.popo.video.ui.homepage;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.SearchUser;
import com.popo.video.data.preference.UserPreference;
import com.popo.library.adapter.CommonRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;

import java.util.List;

/**
 * 首页推荐用户适配器
 * Created by zhangdroid on 2017/6/3.
 */
public class ListAdapter extends CommonRecyclerViewAdapter<SearchUser> {
    private FragmentManager mFragmentManager;
    private Context mContext;
    private int pos = 0;

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
    }

    public ListAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
        mContext = context;
    }

    public ListAdapter(Context context, int layoutResId, List<SearchUser> dataList) {
        super(context, layoutResId, dataList);
    }

    @Override
    public void convert(final SearchUser searchUser, final int position, RecyclerViewHolder holder) {
        if (null != searchUser) {
            ImageView imageView = (ImageView) holder.getView(R.id.item_homepage_iv_avatar);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(searchUser.getIconUrlMiddle())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(imageView).build());
            holder.setText(R.id.item_homepage_tv_nickname, searchUser.getNickName());
            if (UserPreference.getCountryId().equals("97") && Util.getLacalLanguage().equals("English")) {
                holder.setText(R.id.item_homepage_tv_age_and_height, searchUser.getAge() + mContext.getString(R.string.edit_info_years_old) + "  " + ">5 miles");
            }else{
                holder.setText(R.id.item_homepage_tv_age_and_height, searchUser.getAge() + mContext.getString(R.string.edit_info_years_old) + "  " + mContext.getString(R.string.distance));
            }
            TextView tv_state = (TextView) holder.getView(R.id.item_homepage_tv_state);
            TextView tv_big_state = (TextView) holder.getView(R.id.item_homepage_big_tv_state);
            // 视频
            ImageView iBVideo = (ImageView) holder.getView(R.id.item_homepage_iv_state);
            final ImageView iv_sayhello = (ImageView) holder.getView(R.id.item_homepage_big_iv_sayhello);
            final int status = searchUser.getStatus();
            int onlineStatus = searchUser.getOnlineStatus();
            if (onlineStatus == -1) {
                tv_big_state.setText(mContext.getString(R.string.not_online));
            } else {
                tv_big_state.setText(mContext.getString(R.string.edit_info_status));
            }
            if (UserPreference.isAnchor()) {
                tv_big_state.setVisibility(View.GONE);
                iv_sayhello.setVisibility(View.VISIBLE);
                iv_sayhello.setBackgroundResource(R.drawable.say_hello);
            } else {
                tv_big_state.setVisibility(View.VISIBLE);
                iv_sayhello.setVisibility(View.GONE);
            }
            int isSayHello = searchUser.getIsSayHello();
            if (isSayHello == 1) {
                iv_sayhello.setBackgroundResource(R.drawable.say_helloed);
            } else {
                iv_sayhello.setBackgroundResource(R.drawable.say_hello);
            }
            iv_sayhello.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, mContext.getString(R.string.hello_success), Toast.LENGTH_SHORT).show();
                    searchUser.setIsSayHello(1);
                    iv_sayhello.setBackgroundResource(R.drawable.say_helloed);
                    updateItem(position, searchUser);
                    ApiManager.sayHello(String.valueOf(searchUser.getUesrId()), new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {

                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {

                        }
                    });
                }
            });
            switch (status) {
                case C.homepage.STATE_FREE:// 空闲
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_free);
                    tv_state.setText(mContext.getString(R.string.invitable));
                    break;
                case C.homepage.STATE_BUSY:// 忙线中
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_busy);
                    tv_state.setText(mContext.getString(R.string.talking));
                    break;
                case C.homepage.STATE_NO_DISTRUB:// 勿扰
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_busy);
                    tv_state.setText(mContext.getString(R.string.message_state_nodistrub));
                    break;
                default:
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_free);
                    tv_state.setText(mContext.getString(R.string.not_online));
                    break;
            }
        }
    }
}
