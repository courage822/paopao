package com.popo.video.ui.personalcenter.presenter;

import android.content.Context;

import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.WithdrawRecord;
import com.popo.video.data.model.WithdrawRecordList;
import com.popo.library.util.Utils;
import com.popo.video.ui.personalcenter.contract.WithdrawRecordContract;

import java.util.List;

/**
 * Created by Administrator on 2017/7/14.
 */

public class WithdrawRecordPresenter implements WithdrawRecordContract.IPresenter{
   private WithdrawRecordContract.IView mWithdrawRecordView;
    private Context mContext;
    private int pageNum = 1;
    private static final String pageSize = "20";
    public WithdrawRecordPresenter(WithdrawRecordContract.IView view){
        this.mWithdrawRecordView=view;
        this.mContext=view.obtainContext();
    }

    @Override
    public void start() {

    }


    @Override
    public void loadHistoryList() {
        load();
    }

    @Override
    public void refresh() {
        pageNum = 1;
        load();
    }

    @Override
    public void loadMore() {
        mWithdrawRecordView.showLoadMore();
        pageNum++;
        load();
    }

    private void load() {
        ApiManager.getWithdrawRecord(pageNum, pageSize, new IGetDataListener<WithdrawRecordList>() {

            @Override
            public void onResult(WithdrawRecordList recordList, boolean isEmpty) {
                if (isEmpty) {
                    if (pageNum == 1) {
                        mWithdrawRecordView.toggleShowEmpty(true, null);
                    } else if (pageNum > 1) {
                        mWithdrawRecordView.showNoMore();
                    }
                } else {
                    if (null != recordList) {
                        List<WithdrawRecord> list = recordList.getListRecord();
                        if (!Utils.isListEmpty(list)) {
                            if (pageNum == 1) {
                                mWithdrawRecordView.getWithdrawRecordAdapter().bind(list);
                            } else if (pageNum > 1) {
                                mWithdrawRecordView.getWithdrawRecordAdapter().appendToList(list);
                            }
                            mWithdrawRecordView.hideLoadMore();
                        }
                    }
                }
                mWithdrawRecordView.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mWithdrawRecordView.hideRefresh(1);
                if (!isNetworkError) {
                    mWithdrawRecordView.toggleShowError(true, msg);
                }
            }
        });
    }

}
