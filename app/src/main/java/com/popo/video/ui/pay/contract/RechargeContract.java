package com.popo.video.ui.pay.contract;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;
import com.popo.video.ui.pay.adapter.PayAdapter;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public interface RechargeContract {

    interface IView extends BaseView {

        /**
         * 显示加载
         */
        void showLoading();

        /**
         * 隐藏加载
         */
        void dismissLoading();

        void showNetworkError();

        void setAdapter(PagerAdapter pagerAdapter);
        FragmentManager getManager();
        /**
         * 设置商品信息适配器
         */
        void setAdapter(PayAdapter payAdapter);
        /**
         *获取描述信息
         */
        void setTextDetail(String detial);
        /**
         * 获取钥匙数量
         */
        void getKeyNum(String num);
        /**
         * 当前余额钱数
         */
        void getNowMoney(String money);
    }

    interface IPresenter extends BasePresenter {

        /**
         * @return 从后台获取支付渠道信息
         */
        void getPayWay(String fromTag);

        void addTabs();
        /**
         * 获取用户的个人信息为的是我的钱包界面
         */
        void getMyInfo();
    }

}
