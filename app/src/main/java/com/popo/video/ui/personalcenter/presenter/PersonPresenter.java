package com.popo.video.ui.personalcenter.presenter;

import android.text.TextUtils;
import android.view.View;

import com.popo.video.R;
import com.popo.video.common.Util;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.CheckStatus;
import com.popo.video.data.model.HostInfo;
import com.popo.video.data.model.MyInfo;
import com.popo.video.data.model.Switch;
import com.popo.video.data.model.UpLoadMyPhoto;
import com.popo.video.data.model.UserBase;
import com.popo.video.data.model.UserBean;
import com.popo.video.data.model.UserDetail;
import com.popo.video.data.model.UserPhoto;
import com.popo.video.data.preference.AnchorPreference;
import com.popo.video.data.preference.BeanPreference;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.SwitchPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.library.adapter.MultiTypeRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.util.LaunchHelper;
import com.popo.video.parcelable.BigPhotoParcelable;
import com.popo.video.ui.detail.adapter.AlbumPhotoAdapter;
import com.popo.video.ui.personalcenter.contract.PersonContract;
import com.popo.video.ui.photo.BigPhotoActivity;

import java.io.File;
import java.util.List;

import static com.popo.video.common.TimeUtils.mContext;

/**
 * Created by zhangdroid on 2017/5/27.
 */
public class PersonPresenter implements PersonContract.IPresenter {
    private PersonContract.IView mPersonView;
    private AlbumPhotoAdapter mAlbumPhotoAdapter;

    public PersonPresenter(PersonContract.IView view) {
        this.mPersonView = view;
    }

    private boolean isCanLoad = true;

    @Override
    public void start() {
        mAlbumPhotoAdapter = new AlbumPhotoAdapter(mContext, R.layout.album_recyclerview_item, 0);
        mPersonView.setAdapter(mAlbumPhotoAdapter);
        mAlbumPhotoAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                final List<UserPhoto> adapterDataList = mAlbumPhotoAdapter.getAdapterDataList();
                if (adapterDataList != null && adapterDataList.size() > 0) {
                    // 点击图片查看大图
                    LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(position,
                            Util.convertPhotoUrl(adapterDataList)));
                }
            }
        });
    }

    @Override
    public void getUserInfo() {

        ApiManager.kaiguan(new IGetDataListener<Switch>() {
            @Override
            public void onResult(Switch aSwitch, boolean isEmpty) {
                SwitchPreference.saveSwitch(aSwitch);
                if (aSwitch != null) {
                    if (aSwitch.getAllpaySwitch() == -1) {
                        mPersonView.switchAllPay();
                        isCanLoad = false;
                    } else {
                        isCanLoad = true;
                    }
                    if (aSwitch.getAllpaySwitch() == -1) {
                        mPersonView.switchAllPay();
                    }
                    if (aSwitch.getKeypaySwitch() == -1) {
                        mPersonView.switchKey();
                    }
                    if (aSwitch.getBeanpaySwitch() == -1) {
                        mPersonView.switchDionmads();
                    }
                    if (aSwitch.getWalletSwitch() == -1) {
                        mPersonView.switchWallet();
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    @Override
    public void getUploadInfo() {
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    if (!TextUtils.isEmpty(myInfo.getIslj())){
                        UserPreference.setIslj(myInfo.getIslj());
                    }
                    UserDetail userDetail = myInfo.getUserDetail();
                    String vipDays = userDetail.getVipDays();
                    List<UserPhoto> userPhotos = userDetail.getUserPhotos();
                    if (userPhotos != null && userPhotos.size() > 0) {
                        mAlbumPhotoAdapter.bind(userPhotos);
                        mPersonView.picGone();
                    }
                    if (!TextUtils.isEmpty(vipDays) && vipDays.length() > 0) {
                        int i = Integer.parseInt(vipDays);
                        mPersonView.vipDays(i);
                        UserPreference.setIsVip(i);
                    } else {
                        mPersonView.vipDays(0);
                        UserPreference.setIsVip(false);
                    }
                    if (userDetail != null) {
                        UserBean userBean = userDetail.getUserBean();
                        if (userBean != null) {
                            BeanPreference.saveUserBean(userBean);
                        }
                        UserBase userBase = userDetail.getUserBase();
                        if (userBase != null) {
                            mPersonView.setUserAvatar(userBase.getIconUrlMininum(), userBase.getIconStatus());
                            mPersonView.setUserName(userBase.getNickName());
                            mPersonView.setUserId(String.valueOf(userBase.getAccount()));
                            int userType = userBase.getUserType();
                            if (isCanLoad) {
                                if (userType == 0) {
                                    mPersonView.setIsAnchor(false);
                                } else {
                                    mPersonView.setIsAnchor(true);

                                }
                            }
                        }
                        HostInfo hostInfo = userDetail.getHostInfo();
                        if (hostInfo != null) {
                            AnchorPreference.saveHostInfo(hostInfo);
                            mPersonView.setAccumulatedIncome(String.valueOf(hostInfo.getMonthlyIncome()));
                            mPersonView.setAnchorCurrentIncome(String.valueOf(hostInfo.getIncomeBalance()));
                            mPersonView.setPrice(String.valueOf((int) hostInfo.getPrice()), hostInfo.getAudioPrice());
                        }
                    }
                    mPersonView.setMassDesc(myInfo.getMassVideoDesc(), myInfo.getMassAudioDesc());
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    public void loadPersonInfor() {
        ApiManager.getFaceBookAccount(new IGetDataListener<String>() {
            @Override
            public void onResult(String faceBookAccount, boolean isEmpty) {
                if (faceBookAccount != null) {
                    DataPreference.saveDataJson(faceBookAccount);
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    @Override
    public void getCheckStatus() {
        ApiManager.getAuthorCheckStatus(new IGetDataListener<CheckStatus>() {
            @Override
            public void onResult(CheckStatus checkStatus, boolean isEmpty) {
                if (checkStatus != null) {
                    mPersonView.getCheckStatus(checkStatus.getAuditStatus(), checkStatus.getShowStatus());
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    @Override
    public void upLoadAvator(File file, boolean photoType) {
        mPersonView.showLoading();
        ApiManager.upLoadMyPhotoOrAvator(file, photoType, new IGetDataListener<UpLoadMyPhoto>() {
            @Override
            public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                mPersonView.hideLoading();
                if (upLoadMyPhoto != null) {
                    final UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                    if (userPhoto != null) {
                        final String stringFile = userPhoto.getFileUrlMiddle();
                        String fileUrl = userPhoto.getFileUrl();
                        String fileUrlMinimum = userPhoto.getFileUrlMinimum();
                        if (!TextUtils.isEmpty(stringFile)) {
                            UserPreference.setOriginalImage(fileUrl);
                            UserPreference.setMiddleImage(stringFile);
                            UserPreference.setSmallImage(fileUrlMinimum);
                            mPersonView.setUserAvatar(userPhoto.getFileUrlMinimum(), userPhoto.getStatus());
                        }
                    }
                }
                mPersonView.showTip(mContext.getString(R.string.person_picture_check));
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mPersonView.hideLoading();
                mPersonView.showTip(msg);
            }
        });
    }
}
