package com.popo.video.ui.dialog;

import android.view.View;

/**
 * Double button dialog click listener
 * Created by zhangdroid on 2016/6/25.
 */
public interface OnDoubleDialogClickListener {

    /**
     * Called when the dialog positive button has been clicked.
     *
     * @param view The View that was clicked.
     */
    void onPositiveClick(View view);

    /**
     * Called when the dialog negative button has been clicked.
     *
     * @param view The View that was clicked.
     */
    void onNegativeClick(View view);

}
