package com.popo.video.ui.dialog;


import com.popo.library.widget.WheelView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 日期选择对话框
 * Created by zhangdroid on 2016/6/25.
 */
public class DateSelectDialog extends ThreeWheelDialog {
    private OnThreeWheelDialogClickListener mOnDialogClickListener;
    /**
     * 每个月份对应的天数
     */
    private Map<String, Integer> mMonthDayMap;
    private WheelView mWheelViewDay;
    private static String year;
    private static String month;
    private static String day;

    public DateSelectDialog() {
        mMonthDayMap = new HashMap<>();
        mMonthDayMap.put("01", 31);
        mMonthDayMap.put("02", 28);
        mMonthDayMap.put("03", 31);
        mMonthDayMap.put("04", 30);
        mMonthDayMap.put("05", 31);
        mMonthDayMap.put("06", 30);
        mMonthDayMap.put("07", 31);
        mMonthDayMap.put("08", 31);
        mMonthDayMap.put("09", 30);
        mMonthDayMap.put("10", 31);
        mMonthDayMap.put("11", 30);
        mMonthDayMap.put("12", 31);
    }

    public static DateSelectDialog newInstance(String title, String positive, String negative, String date, boolean isCancelable, OnThreeWheelDialogClickListener listener) {
        DateSelectDialog dateSelectDialog = new DateSelectDialog();
        dateSelectDialog.mOnDialogClickListener = listener;
        dateSelectDialog.setArguments(getDialogBundle(title, null, positive, negative, isCancelable));
        if(date!=null){
            String[] spit=date.split("-");
            year=spit[0];
            month=spit[1];
            day=spit[2];
        }
        return dateSelectDialog;
    }
    public static DateSelectDialog newInstance(String title, String positive, String negative, boolean isCancelable, OnThreeWheelDialogClickListener listener) {
        DateSelectDialog dateSelectDialog = new DateSelectDialog();
        dateSelectDialog.mOnDialogClickListener = listener;
        dateSelectDialog.setArguments(getDialogBundle(title, null, positive, negative, isCancelable));
        return dateSelectDialog;
    }

    @Override
    protected void setWheelView1(WheelView wheelView) {
        wheelView.setData(getYearList());
        if(year!=null){
            for (int i=0;i<getYearList().size();i++){
                if(getYearList().get(i).equals(year)){
                    wheelView.setDefaultIndex(i);
                }
            }
        }else{
            wheelView.setDefaultIndex(5);
        }

    }

    @Override
    protected void setWheelView2(WheelView wheelView) {
        wheelView.setData(getMonthList());
        if(month!=null){
            for (int i=0;i<getMonthList().size();i++){
                if(getMonthList().get(i).equals(month)){
                    wheelView.setDefaultIndex(i);
                }
            }
        }else{
            wheelView.setDefaultIndex(0);
        }

        wheelView.setOnItemSelectedListener(new WheelView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(int id, String text) {
                mWheelViewDay.setData(getDayList(mMonthDayMap.get(text)));
            }
        });
    }

    @Override
    protected void setWheelView3(WheelView wheelView) {
        mWheelViewDay = wheelView;
        int dayCount=31;
        if(month!=null){
            if(month.equals("04")||month.equals("06")||month.equals("09")||month.equals("11")){
                dayCount=30;
            }else if(month.equals("02")){
                if(year!=null&& Integer.parseInt(year)/4==0){
                    dayCount=29;
                }else{
                    dayCount=28;
                }
            }else{
                dayCount=31;
            }
        }
        wheelView.setData(getDayList(dayCount));
        if(day!=null){
            for (int i=0;i<getDayList(dayCount).size();i++){
                if(getDayList(dayCount).get(i).equals(day)){
                    wheelView.setDefaultIndex(i);
                }
            }
        }else{
            wheelView.setDefaultIndex(0);
        }
    }

    @Override
    protected OnThreeWheelDialogClickListener setOnDialogClickListener() {
        return mOnDialogClickListener;
    }
    /**
     * @return 获得年龄列表
     */
    public static List<String> getAgeList() {
        List<String> ageList = new ArrayList<String>();
        for (int i = 0; i < 45; i++) {
            ageList.add(String.valueOf(i + 18));
        }
        return ageList;
    }

    /**
     * @return 获得年份列表
     */
    public static List<String> getYearList() {
        List<String> yearList = new ArrayList<String>();
        for (int i = 1998; i >= 1950; i--) {
            yearList.add(String.valueOf(i));
        }
        return yearList;
    }

    /**
     * @return 获得月份列表
     */
    public static List<String> getMonthList() {
        List<String> monthList = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            monthList.add(pad(i));
        }
        return monthList;
    }

    /**
     * 获得天数列表
     *
     * @param maxDays 该月的最大天数
     */
    public static List<String> getDayList(int maxDays) {
        List<String> dayList = new ArrayList<String>();
        for (int i = 1; i <= maxDays; i++) {
            dayList.add(pad(i));
        }
        return dayList;
    }

    /**
     * 日期/时间小于10时格式补0
     */
    public static String pad(int c) {
        if (c >= 10) {
            return String.valueOf(c);
        } else {
            return "0" + String.valueOf(c);
        }
    }
}
