package com.popo.video.ui.video;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.PowerManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hyphenate.chat.EMMessage;
import com.popo.library.net.NetUtil;
import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.video.common.HyphenateHelper;
import com.popo.video.common.RingManage;
import com.popo.video.common.Util;
import com.popo.video.customload.FullScreenVideoView;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.model.VideoMsg;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.event.MessageArrive;
import com.popo.video.parcelable.MakeFreeVideoParcelable;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by WangYong on 2018/1/23.
 */

public class MakeFreeVideoActivity extends BaseFragmentActivity implements View.OnClickListener, SurfaceHolder.Callback {
    @BindView(R.id.make_free_video_activity_rl_root)
    RelativeLayout mroot;
    @BindView(R.id.make_free_video_activity_surfaceview)
    SurfaceView video_local;
    @BindView(R.id.make_free_video_activity_iv_switch)
    ImageView iv_switch;
    @BindView(R.id.make_free_video_activity_iv_exit)
    ImageView iv_exit;
    @BindView(R.id.make_free_video_activity_iv_msg)
    ImageView iv_msg;
    @BindView(R.id.make_free_video_activity_iv_close_voice)
    ImageView iv_close_video;
    @BindView(R.id.make_free_video_activity_videoview)
    FullScreenVideoView videoView;
    @BindView(R.id.make_free_video_activity_btn_send)
    Button btn_send;
    @BindView(R.id.make_free_video_activity_et_input)
    EditText et_content;
    @BindView(R.id.make_free_video_activity_rl_bottom)
    RelativeLayout rl_bottom;
    @BindView(R.id.make_free_video_activity_chat_list)
    RecyclerView recyclerview;
    @BindView(R.id.make_free_video_activity_tv_video_time)
    TextView tv_time;
    SurfaceHolder surfaceHolder;
    Camera camera;
    int frontRotate;
    int frontOri;
    int cameraType = 1;
    int cameraFlag = 0; //1为后置
    InputMethodManager imm;
    PowerManager powerManager = null;
    PowerManager.WakeLock wakeLock = null;
    private VideoMsgAdapter mVideoMsgAdapter;
    private MediaPlayer videoPlayer;
    private boolean isVoice = true;
    private long lastClickTime = 0;
    private int time;
    private MakeFreeVideoParcelable makeFreeVideoParcelable;
    private boolean isCanSee = true;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            ++time;
            tv_time.setText(Util.convertSecondsToString(time));
            handler.sendEmptyMessageDelayed(1, 1000);
        }
    };

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_make_free_video;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        makeFreeVideoParcelable = (MakeFreeVideoParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return mroot;
    }

    @Override
    protected void initViews() {
        if (makeFreeVideoParcelable != null) {
            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);
            final Uri uri = Uri.parse(makeFreeVideoParcelable.videoUrl);
            //播放完成回调
            videoView.setOnCompletionListener(new MyPlayerOnCompletionListener());
            //设置视频路径
            videoView.setVideoURI(uri);
            videoView.requestFocus();
            videoView.setZOrderOnTop(false);
            videoView.start();
            videoPlayer = MediaPlayer.create(MakeFreeVideoActivity.this, uri);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    handler.sendEmptyMessage(1);
                }
            }, 2000);
        }
        RingManage.getInstance().closeRing();
        initView();
    }

    @Override
    protected void setListeners() {
        iv_switch.setOnClickListener(this);
        iv_exit.setOnClickListener(this);
        iv_msg.setOnClickListener(this);
        iv_close_video.setOnClickListener(this);
        btn_send.setOnClickListener(this);
        mroot.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }


    @Override
    protected void onPause() {
        super.onPause();
        wakeLock.release();
        releaseCamera();
        handler.removeCallbacks(null);
        if (isCanSee) {
            saveVideoTime("00:10");
        }
        releaseCamera();
        finish();
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.make_free_video_activity_iv_switch:
                switchCamera();
                break;
            case R.id.make_free_video_activity_iv_msg:
                rl_bottom.setVisibility(View.VISIBLE);
                iv_msg.setVisibility(View.GONE);
                iv_close_video.setVisibility(View.GONE);
                break;
            case R.id.make_free_video_activity_iv_close_voice:
                if (isVoice) {
                    isVoice = false;
                    setVolume(0, videoView);
                    iv_close_video.setImageResource(R.drawable.vioce_close);
                } else {
                    isVoice = true;
                    setVolume(10, videoView);
                    iv_close_video.setImageResource(R.drawable.vioce_open);
                }
                break;
            case R.id.make_free_video_activity_iv_exit:
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    saveVideoTime(tv_time.getText().toString());
                    releaseCamera();
                    isCanSee = false;
                    finish();
                    Toast.makeText(MakeFreeVideoActivity.this, getString(R.string.leave_room_make_free, ""), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.make_free_video_activity_btn_send:
                String content = et_content.getText().toString().trim();
                if (!TextUtils.isEmpty(content)) {
                    et_content.setText("");
                    List<VideoMsg> list = new ArrayList<>();
                    VideoMsg videoMsg = new VideoMsg(UserPreference.getNickname() + "：", content, 0);
                    list.add(videoMsg);
                    mVideoMsgAdapter.appendToList(list);
                    // 更新后自动滚动到列表底部
                    recyclerview.scrollToPosition(list.size() - 1);
                }
                break;
            case R.id.make_free_video_activity_rl_root:
                rl_bottom.setVisibility(View.GONE);
                iv_msg.setVisibility(View.VISIBLE);
                iv_close_video.setVisibility(View.VISIBLE);
                imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                break;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceHolder = holder;
        initCamera(cameraType);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        surfaceHolder = holder;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        releaseCamera();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initView() {
        SurfaceHolder holder = video_local.getHolder();
        holder.addCallback(this);
        // setType必须设置，要不出错.
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        holder.setFormat(PixelFormat.TRANSPARENT);
        video_local.setZOrderOnTop(true);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        powerManager = (PowerManager) this.getSystemService(this.POWER_SERVICE);
        wakeLock = this.powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Lock");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(linearLayoutManager);
        mVideoMsgAdapter = new VideoMsgAdapter(this, R.layout.item_video_msg_list);
        recyclerview.setAdapter(mVideoMsgAdapter);
    }

    /**
     * 初始化相机
     *
     * @param type 前后的类型
     */
    private void initCamera(int type) {

        if (camera != null) {
            //如果已经初始化过，就先释放
            releaseCamera();
        }

        try {
            camera = Camera.open(type);
            if (camera == null) {
                return;
            }
            camera.lock();
            Camera.Parameters parameters = camera.getParameters();
            if (type == 0) {
                //基本是都支持这个比例
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);//1连续对焦
                camera.cancelAutoFocus();// 2如果要实现连续的自动对焦，这一句必须加上
            }
            camera.setParameters(parameters);
            if (cameraType == 1) {
                frontCameraRotate();
                camera.setDisplayOrientation(frontRotate);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    camera.setDisplayOrientation(270);//90
                } else {
                    camera.setDisplayOrientation(90);
                }
            }
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
            camera.unlock();
        } catch (Exception e) {
            e.printStackTrace();
            releaseCamera();
        }
    }

    /**
     * 释放摄像头资源
     */
    private void releaseCamera() {
        try {
            if (camera != null) {
                camera.setPreviewCallback(null);
                camera.stopPreview();
                camera.lock();
                camera.release();
                camera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 切换摄像头
     */
    public void switchCamera() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras();//得到摄像头的个数0或者1;

        try {
            for (int i = 0; i < cameraCount; i++) {
                Camera.getCameraInfo(i, cameraInfo);//得到每一个摄像头的信息
                if (cameraFlag == 1) {
                    //后置到前置
                    if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {//代表摄像头的方位，CAMERA_FACING_FRONT前置      CAMERA_FACING_BACK后置
                        frontCameraRotate();//前置旋转摄像头度数
                        switchCameraLogic(i, 0, frontRotate);
                        break;
                    }
                } else {
                    //前置到后置
                    if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {//代表摄像头的方位，CAMERA_FACING_FRONT前置      CAMERA_FACING_BACK后置
                        switchCameraLogic(i, 1, 90);
                        break;
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /***
     * 处理摄像头切换逻辑
     *
     * @param i           哪一个，前置还是后置
     * @param flag        切换后的标志
     * @param orientation 旋转的角度
     */
    private void switchCameraLogic(int i, int flag, int orientation) {
        if (camera != null) {
            camera.lock();
        }
        releaseCamera();
        camera = Camera.open(i);//打开当前选中的摄像头
        try {
            camera.setDisplayOrientation(orientation);
            camera.setPreviewDisplay(surfaceHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        cameraFlag = flag;
        camera.startPreview();
        cameraType = i;
        camera.unlock();
    }

    /**
     * 旋转前置摄像头为正的
     */
    private void frontCameraRotate() {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(1, info);
        int degrees = getDisplayRotation(this);
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        frontOri = info.orientation;
        frontRotate = result;
    }

    /**
     * 获取旋转角度
     */
    private int getDisplayRotation(Activity activity) {
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return 0;
            case Surface.ROTATION_90:
                return 90;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_270:
                return 270;
        }
        return 0;
    }

    private class MyPlayerOnCompletionListener implements MediaPlayer.OnCompletionListener {

        @Override
        public void onCompletion(MediaPlayer mp) {
            String otherPerson = getString(R.string.other_person);
            Toast.makeText(MakeFreeVideoActivity.this, getString(R.string.leave_room_make_free, otherPerson), Toast.LENGTH_SHORT).show();
            handler.removeCallbacks(null);
//            saveVideoTime("00:10");
            releaseCamera();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        wakeLock.acquire();
    }

    @Override
    public void onBackPressed() {
        // do nothing 屏蔽返回键
    }

    /**
     * @param volume 音量大小
     * @param object VideoView实例
     */
    private void setVolume(float volume, Object object) {
        try {
            Class<?> forName = Class.forName("android.widget.VideoView");
            Field field = forName.getDeclaredField("mMediaPlayer");
            field.setAccessible(true);
            MediaPlayer mMediaPlayer = (MediaPlayer) field.get(object);
            mMediaPlayer.setVolume(volume, volume);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 保存视频终止的时间
     *
     * @param time 要保存的时间
     */
    private void saveVideoTime(final String time) {
        if (makeFreeVideoParcelable != null) {
            if (!TextUtils.isEmpty(makeFreeVideoParcelable.account)) {
                HyphenateHelper.getInstance().sendTextMessage(makeFreeVideoParcelable.account,
                        (mContext.getString(R.string.video_call))
                                + time,
                        new HyphenateHelper.OnMessageSendListener() {
                            @Override
                            public void onSuccess(EMMessage emMessage) {
                                HyphenateHelper.getInstance().markMsgAsRead(makeFreeVideoParcelable.account);
                                saveDataToSqlit(makeFreeVideoParcelable.nickname, makeFreeVideoParcelable.guid, makeFreeVideoParcelable.account, makeFreeVideoParcelable.icon,
                                        (mContext.getString(R.string.video_call))
                                                + time, String.valueOf(System.currentTimeMillis()), 1);
                                EventBus.getDefault().post(new MessageArrive(makeFreeVideoParcelable.guid));
                            }

                            @Override
                            public void onError() {
                            }
                        });
            }
        }
    }

    private void saveDataToSqlit(String name, String id, String account, String pic, String msg, String time, int extendType) {
        if (!TextUtils.isEmpty(id)) {
            HuanXinUser user = new HuanXinUser(id, name, pic, account, "1", msg, 0, time, extendType);
            DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
        }
    }

}
