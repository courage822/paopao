package com.popo.video.ui.detail;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.base.BaseFragmentActivity;
import com.popo.library.net.NetUtil;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.parcelable.ReportParcelable;
import com.popo.video.ui.detail.contract.ReportContract;
import com.popo.video.ui.detail.presenter.ReportPresenter;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/10/25.
 */

public class ReportActivity extends BaseFragmentActivity implements ReportContract.IView, View.OnClickListener {
    @BindView(R.id.activity_report_ll)
    LinearLayout ll_root;
    @BindView(R.id.report_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.report_activity_ll_seqingdilu)
    LinearLayout ll_seqingdilu;
    @BindView(R.id.report_activity_tv_seqingdilu)
    TextView tv_seqingdilu;
    @BindView(R.id.report_activity_ll_weifabaoli)
    LinearLayout ll_weifabaoli;
    @BindView(R.id.report_activity_tv_weifabaoli)
    TextView tv_weifabaoli;
    @BindView(R.id.report_activity_ll_baokongxuexing)
    LinearLayout ll_baokongxuexing;
    @BindView(R.id.report_activity_tv_baokongxuexing)
    TextView tv_baokongxuexing;
    @BindView(R.id.report_activity_ll_zhengzhimingan)
    LinearLayout ll_zhengzhimingan;
    @BindView(R.id.report_activity_tv_zhengzhimingan)
    TextView tv_zhengzhimingan;
    @BindView(R.id.report_activity_btn_submit)
    Button btn_submit;
    private ReportPresenter mReportPresenter;
    private String reasonCode="0";
    private String userId="";
    private ReportParcelable userReport;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_report;
    }
    @Override
    protected void initViews() {
    mReportPresenter=new ReportPresenter(this);
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
        ll_seqingdilu.setOnClickListener(this);
        ll_weifabaoli.setOnClickListener(this);
        ll_baokongxuexing.setOnClickListener(this);
        ll_zhengzhimingan.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }
    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        userReport= (ReportParcelable) parcelable;
        userId=userReport.remoteUid;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(ll_root, msg);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void showNetworkError() {

    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.report_activity_rl_back:
                 finish();
                break;
            case R.id.report_activity_btn_submit:
                mReportPresenter.startReport(userId,reasonCode);
                break;
            case R.id.report_activity_ll_seqingdilu:
                reasonCode="1";
                setTextColor(1);
                btn_submit.setEnabled(true);
                break;
            case R.id.report_activity_ll_weifabaoli:
                reasonCode="2";
                setTextColor(2);
                btn_submit.setEnabled(true);
                break;
            case R.id.report_activity_ll_baokongxuexing:
                reasonCode="3";
                setTextColor(3);
                btn_submit.setEnabled(true);
                break;
            case R.id.report_activity_ll_zhengzhimingan:
                reasonCode="4";
                setTextColor(4);
                btn_submit.setEnabled(true);
                break;
        }
    }

    private void setTextColor(int i) {
    switch (i){
        case 1:
            tv_seqingdilu.setTextColor(getResources().getColor(R.color.main_color));
            tv_weifabaoli.setTextColor(getResources().getColor(R.color.linear_gray));
            tv_baokongxuexing.setTextColor(getResources().getColor(R.color.linear_gray));
            tv_zhengzhimingan.setTextColor(getResources().getColor(R.color.linear_gray));
            break;
        case 2:
            tv_seqingdilu.setTextColor(getResources().getColor(R.color.linear_gray));
            tv_weifabaoli.setTextColor(getResources().getColor(R.color.main_color));
            tv_baokongxuexing.setTextColor(getResources().getColor(R.color.linear_gray));
            tv_zhengzhimingan.setTextColor(getResources().getColor(R.color.linear_gray));
            break;
        case 3:
            tv_seqingdilu.setTextColor(getResources().getColor(R.color.linear_gray));
            tv_weifabaoli.setTextColor(getResources().getColor(R.color.linear_gray));
            tv_baokongxuexing.setTextColor(getResources().getColor(R.color.main_color));
            tv_zhengzhimingan.setTextColor(getResources().getColor(R.color.linear_gray));
            break;
        case 4:
            tv_seqingdilu.setTextColor(getResources().getColor(R.color.linear_gray));
            tv_weifabaoli.setTextColor(getResources().getColor(R.color.linear_gray));
            tv_baokongxuexing.setTextColor(getResources().getColor(R.color.linear_gray));
            tv_zhengzhimingan.setTextColor(getResources().getColor(R.color.main_color));
            break;
    }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
