package com.popo.video.ui.dialog;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.library.dialog.BaseDialogFragment;
import com.popo.library.widget.WheelView;


/**
 * 只显示单个滚轮的对话框
 * Created by zhangdroid on 2016/6/24.
 */
public abstract class OneWheelDialog extends BaseDialogFragment {
    private OnOneWheelDialogClickListener mOneWheelDialogClickListener;

    /**
     * 设置WheelView属性
     *
     * @param wheelView
     */
    protected abstract void setWheelView(WheelView wheelView);

    protected abstract OnOneWheelDialogClickListener setOnDialogClickListener();

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_one_wheel;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tvTitle = (TextView) view.findViewById(R.id.dialog_one_wheel_title);
        View dividerView = view.findViewById(R.id.dialog_one_wheel_divider);// 标题栏下分割线
        final WheelView wheelView = (WheelView) view.findViewById(R.id.dialog_one_wheelview);
        Button btnPositive = (Button) view.findViewById(R.id.dialog_one_wheel_sure);
        Button btnNegative = (Button) view.findViewById(R.id.dialog_one_wheel_cancel);

        setWheelView(wheelView);
        mOneWheelDialogClickListener = setOnDialogClickListener();

        String title = getDialogTitle();
        if (!TextUtils.isEmpty((title))) {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
            dividerView.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
        }

        String positive = getDialogPositive();
        if (!TextUtils.isEmpty((positive))) {
            btnPositive.setText(positive);
        }

        String negative = getDialogNegative();
        if (!TextUtils.isEmpty((negative))) {
            btnNegative.setText(negative);
        }

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOneWheelDialogClickListener != null) {
                    mOneWheelDialogClickListener.onPositiveClick(v, wheelView.getSelectedText());
                }
                dismiss();
            }
        });
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOneWheelDialogClickListener != null) {
                    mOneWheelDialogClickListener.onNegativeClick(v);
                }
                dismiss();
            }
        });
    }

    public interface OnOneWheelDialogClickListener {
        /**
         * Called when the dialog positive button has been clicked.
         *
         * @param view         The View that was clicked.
         * @param selectedText The selected item text.
         */
        void onPositiveClick(View view, String selectedText);

        /**
         * Called when the dialog negative button has been clicked.
         *
         * @param view The View that was clicked.
         */
        void onNegativeClick(View view);
    }

}
