package com.popo.video.ui.homepage.contract;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/5/31.
 */
public interface HomepageContract {

    interface IView extends BaseView {

        FragmentManager getManager();

        void setAdapter(PagerAdapter pagerAdapter);
    }

    interface IPresenter extends BasePresenter {

        void addTabs();
        /**
         * 判断是否开通vip
         */
        void isVipUser();
    }

}
