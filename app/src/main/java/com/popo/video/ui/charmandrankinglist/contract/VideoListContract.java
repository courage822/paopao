package com.popo.video.ui.charmandrankinglist.contract;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.popo.video.mvp.BasePresenter;
import com.popo.video.mvp.BaseView;


/**
 * Created by Administrator on 2017/6/15.
 */

public interface VideoListContract {
    interface IView extends BaseView {
        /**
         * 设置VideoListAdapter
         */
        void setAdapter(FragmentStatePagerAdapter adapter);

        /**
         * 空界面
         */
        void setEmptyView(boolean toggle, String msg);

        /**
         * 错误界面
         */
        void setErrorView(boolean toggle, String msg);
        /**
         * 当前请求页数
         */
        void setPostion();

        Fragment getFragment();
    }

    interface IPresenter extends BasePresenter {

        /**
         * 加载视频秀列表
         */
        void loadVideoShowList(int num);

        /**
         * vip拦截
         */
        void loadOneShow();

        /**
         * 举报拉黑
         */
        void reportShow(int position);
    }
}
