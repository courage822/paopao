package com.popo.video.data.model;

/**
 * 用户上传的图片对象
 * Created by zhangdroid on 2017/6/2.
 */
public class UserPhoto {
    private long guid;// 图片id
    private long userId;// 用户id
    private int gender;// 用户性别
    private String file;// 图片本地路径
    private String fileUrl;// 图片访问路径
    private String fileUrlMiddle;// 中图图片访问路径
    private String fileUrlMinimum;// 小图访问路径
    private String photoType;// 图片类型 1、普通 2、头像
    private String fileSize;// 照片文件大小
    private int width;// 照片宽度
    private int height;// 照片高度
    private String fromChannel;// 渠道号
    private String product;// 产品号
    private String status;// 图片状态（后续审核相关）
    private String sortIndex;//相册排序时使用

    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileUrlMiddle() {
        return fileUrlMiddle;
    }

    public void setFileUrlMiddle(String fileUrlMiddle) {
        this.fileUrlMiddle = fileUrlMiddle;
    }

    public String getFileUrlMinimum() {
        return fileUrlMinimum;
    }

    public void setFileUrlMinimum(String fileUrlMinimum) {
        this.fileUrlMinimum = fileUrlMinimum;
    }

    public String getPhotoType() {
        return photoType;
    }

    public void setPhotoType(String photoType) {
        this.photoType = photoType;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getFromChannel() {
        return fromChannel;
    }

    public void setFromChannel(String fromChannel) {
        this.fromChannel = fromChannel;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(String sortIndex) {
        this.sortIndex = sortIndex;
    }

    @Override
    public String toString() {
        return "UserPhoto{" +
                "guid=" + guid +
                ", userId=" + userId +
                ", gender=" + gender +
                ", file='" + file + '\'' +
                ", fileUrl='" + fileUrl + '\'' +
                ", fileUrlMiddle='" + fileUrlMiddle + '\'' +
                ", fileUrlMinimum='" + fileUrlMinimum + '\'' +
                ", photoType='" + photoType + '\'' +
                ", fileSize='" + fileSize + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", fromChannel='" + fromChannel + '\'' +
                ", product='" + product + '\'' +
                ", status='" + status + '\'' +
                ", sortIndex='" + sortIndex + '\'' +
                '}';
    }
}
