package com.popo.video.data.model;

/**

 * Created by zhangdroid on 2017/5/24.
 */
public class MatchInfo extends BaseModel{

    private String isMatcher;

    private UserBase matcherUser;

    public String getIsMatcher() {
        return isMatcher == null ? "" : isMatcher;
    }

    public void setIsMatcher(String isMatcher) {
        this.isMatcher = isMatcher;
    }

    public UserBase getMatcherUser() {
        return matcherUser;
    }

    public void setMatcherUser(UserBase matcherUser) {
        this.matcherUser = matcherUser;
    }

    @Override
    public String toString() {
        return "MatchInfo{" +
                "isMatcher='" + isMatcher + '\'' +
                ", matcherUser=" + matcherUser +
                '}';
    }
}
