package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/11/8.
 */

public class SendGiftsType extends BaseModel {
    private int beanCount;
    private int beanStatus;

    public int getBeanCount() {
        return beanCount;
    }

    public void setBeanCount(int beanCount) {
        this.beanCount = beanCount;
    }

    public int getBeanStatus() {
        return beanStatus;
    }

    public void setBeanStatus(int beanStatus) {
        this.beanStatus = beanStatus;
    }

    @Override
    public String toString() {
        return "SendGiftsType{" +
                "beanCount=" + beanCount +
                ", beanStatus=" + beanStatus +
                '}';
    }
}
