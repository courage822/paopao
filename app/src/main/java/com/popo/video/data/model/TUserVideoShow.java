package com.popo.video.data.model;


public class TUserVideoShow extends BaseModel {
    private String id;
    private String guid;//视频秀id
    private String sex;
    private String iconUrl;//图片地址
    private String videoUrl;//视频地址
    private int videoSize; //视频大小
    private int videoSeconds; //视频时长
    private short auditStatus;
    private String auditDesc;
    private String auditAdmin;
    private String nickName;
    private String thumbnailUrl;
    private String recommend;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public int getVideoSize() {
        return videoSize;
    }

    public void setVideoSize(int videoSize) {
        this.videoSize = videoSize;
    }

    public int getVideoSeconds() {
        return videoSeconds;
    }

    public void setVideoSeconds(int videoSeconds) {
        this.videoSeconds = videoSeconds;
    }

    public short getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(short auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditDesc() {
        return auditDesc;
    }

    public void setAuditDesc(String auditDesc) {
        this.auditDesc = auditDesc;
    }

    public String getAuditAdmin() {
        return auditAdmin;
    }

    public void setAuditAdmin(String auditAdmin) {
        this.auditAdmin = auditAdmin;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }

    @Override
    public String toString() {
        return "TUserVideoShow{" +
                "id='" + id + '\'' +
                ", guid='" + guid + '\'' +
                ", sex='" + sex + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                ", videoSize=" + videoSize +
                ", videoSeconds=" + videoSeconds +
                ", auditStatus=" + auditStatus +
                ", auditDesc='" + auditDesc + '\'' +
                ", auditAdmin='" + auditAdmin + '\'' +
                ", nickName='" + nickName + '\'' +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                ", recommend='" + recommend + '\'' +
                '}';
    }
}
