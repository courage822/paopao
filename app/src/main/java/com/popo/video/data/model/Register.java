package com.popo.video.data.model;

/**
 * 注册对象
 * Created by zhangdroid on 2017/5/24.
 */
public class Register extends BaseModel {
    private UserBase userBase;

    public UserBase getUserBase() {
        return userBase;
    }

    public void setUserBase(UserBase userBase) {
        this.userBase = userBase;
    }

    @Override
    public String toString() {
        return "Register{" +
                "userBase=" + userBase +
                '}';
    }
}
