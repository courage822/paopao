package com.popo.video.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by xuzhaole on 2018/4/3.
 */

public class GoogleApiModel implements Parcelable {
    private String status;
    private List<GoogleModel> results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<GoogleModel> getResults() {
        return results;
    }

    public void setResults(List<GoogleModel> results) {
        this.results = results;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeTypedList(this.results);
    }

    public GoogleApiModel() {
    }

    protected GoogleApiModel(Parcel in) {
        this.status = in.readString();
        this.results = in.createTypedArrayList(GoogleModel.CREATOR);
    }

    public static final Parcelable.Creator<GoogleApiModel> CREATOR = new Parcelable.Creator<GoogleApiModel>() {
        @Override
        public GoogleApiModel createFromParcel(Parcel source) {
            return new GoogleApiModel(source);
        }

        @Override
        public GoogleApiModel[] newArray(int size) {
            return new GoogleApiModel[size];
        }
    };

    @Override
    public String toString() {
        return "GoogleApiModel{" +
                "status='" + status + '\'' +
                ", results=" + results +
                '}';
    }
}
