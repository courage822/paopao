package com.popo.video.data.model;

/**
 * 平台信息对象
 * Created by zhangdroid on 2017/6/1.
 */
public class PlatformInfo {
    private String pid;// 手机串号（IMEI）
    private String imsi;// sim卡imsi号
    private String w;// 分辨率宽
    private String h;// 分辨率高
    private String version;// 版本号
    private String phonetype;// 手机型号
    private String fid;// 渠道号
    private String product;// 产品号
    private String netType;// 联网类型(0->无网络,2->wifi,3->cmwap,4->cmnet,5->ctnet,6->ctwap,7->3gwap,8->3gnet,9->uniwap,10->uninet)
    private String mobileIP;// 手机ip
    private String release;// 发版时间
    private String platform;// 平台(1->IOS,2->Android)
    private String systemVersion;// 系统版本
    private String language;// 语言
    private String country;// 国家

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getW() {
        return w;
    }

    public void setW(String w) {
        this.w = w;
    }

    public String getH() {
        return h;
    }

    public void setH(String h) {
        this.h = h;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getPhonetype() {
        return phonetype;
    }

    public void setPhonetype(String phonetype) {
        this.phonetype = phonetype;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getNetType() {
        return netType;
    }

    public void setNetType(String netType) {
        this.netType = netType;
    }

    public String getMobileIP() {
        return mobileIP;
    }

    public void setMobileIP(String mobileIP) {
        this.mobileIP = mobileIP;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getSystemVersion() {
        return systemVersion;
    }

    public void setSystemVersion(String systemVersion) {
        this.systemVersion = systemVersion;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "PlatformInfo{" +
                "pid='" + pid + '\'' +
                ", imsi='" + imsi + '\'' +
                ", w='" + w + '\'' +
                ", h='" + h + '\'' +
                ", version='" + version + '\'' +
                ", phonetype='" + phonetype + '\'' +
                ", fid='" + fid + '\'' +
                ", product='" + product + '\'' +
                ", netType='" + netType + '\'' +
                ", mobileIP='" + mobileIP + '\'' +
                ", release='" + release + '\'' +
                ", platform='" + platform + '\'' +
                ", systemVersion='" + systemVersion + '\'' +
                ", language='" + language + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

}
