package com.popo.video.data.model;

/**
 * Created by Administrator on 2017/6/8.
 */

public class UserDetailforOther {
    private UserDetail userDetail;
    private String isSucceed;

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    public String getIsSucced() {
        return isSucceed;
    }

    public void setIsSucced(String isSucced) {
        this.isSucceed = isSucced;
    }

    @Override
    public String toString() {
        return "UserDetailforOther{" +
                "userDetail=" + userDetail +
                ", isSucced='" + isSucceed + '\'' +
                '}';
    }
}
