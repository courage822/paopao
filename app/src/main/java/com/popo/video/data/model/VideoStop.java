package com.popo.video.data.model;

/**
 * 挂断视频结果对象
 * Created by zhangdroid on 2017/6/23.
 */
public class VideoStop extends BaseModel {
    private int beanCount;// 金币数量（普通用户）
    private float totalIncome;// 收入（播主）

    public int getBeanCount() {
        return beanCount;
    }

    public void setBeanCount(int beanCount) {
        this.beanCount = beanCount;
    }

    public float getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(float totalIncome) {
        this.totalIncome = totalIncome;
    }
}
