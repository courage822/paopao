package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/11/3.
 */

public class DictPayGift {
    private String giftId;// 礼物id

    private String giftName; // 礼物名字

    private String giftDesc; // 礼物描述

    private Short giftType; // 礼物类型

    private Integer price; // 礼物价格

    private String giftUrl; // 礼物Url

    private String country;  // 国家

    private String language; // 语言

    private String addTime; // 添加时间

    private Short isvalid; // 是否有效

    private Integer fromChannel; // 渠道号

    private String english;//礼物英语翻译

    private String traditional;//礼物繁体中文翻译

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftDesc() {
        return giftDesc;
    }

    public void setGiftDesc(String giftDesc) {
        this.giftDesc = giftDesc;
    }

    public Short getGiftType() {
        return giftType;
    }

    public void setGiftType(Short giftType) {
        this.giftType = giftType;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getGiftUrl() {
        return giftUrl;
    }

    public void setGiftUrl(String giftUrl) {
        this.giftUrl = giftUrl;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public Short getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(Short isvalid) {
        this.isvalid = isvalid;
    }

    public Integer getFromChannel() {
        return fromChannel;
    }

    public void setFromChannel(Integer fromChannel) {
        this.fromChannel = fromChannel;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getTraditional() {
        return traditional;
    }

    public void setTraditional(String traditional) {
        this.traditional = traditional;
    }

    @Override
    public String toString() {
        return "DictPayGift{" +
                "giftId='" + giftId + '\'' +
                ", giftName='" + giftName + '\'' +
                ", giftDesc='" + giftDesc + '\'' +
                ", giftType=" + giftType +
                ", price=" + price +
                ", giftUrl='" + giftUrl + '\'' +
                ", country='" + country + '\'' +
                ", language='" + language + '\'' +
                ", addTime='" + addTime + '\'' +
                ", isvalid=" + isvalid +
                ", fromChannel=" + fromChannel +
                ", english='" + english + '\'' +
                ", traditional='" + traditional + '\'' +
                '}';
    }
}
