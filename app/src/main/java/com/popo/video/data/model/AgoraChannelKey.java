package com.popo.video.data.model;

/**
 * 声网频道channel key
 * Created by zhangdroid on 2017/6/23.
 */
public class AgoraChannelKey extends BaseModel {
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "AgoraChannelKey{" +
                "key='" + key + '\'' +
                '}';
    }
}
