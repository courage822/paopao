package com.popo.video.data.model;

/**
 * 接口定义中返回的公用参数
 * Created by zhangdroid on 2017/5/24.
 */
public class BaseModel {
    private String isSucceed;// 1表示请求成功，其它均为失败

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

}
