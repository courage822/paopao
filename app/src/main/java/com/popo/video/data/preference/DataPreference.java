package com.popo.video.data.preference;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.popo.video.base.BaseApplication;
import com.popo.library.util.SharedPreferenceUtil;
import com.popo.video.data.model.Country;
import com.popo.video.data.model.FaceBookAccount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 保存播主账户信息
 * Created by zhangdroid on 2017/6/24.
 */
public class DataPreference {
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private DataPreference() {
    }

    public static final String NAME = "init_data";
    public static final String INIT_DATA_JSON = "init_data_json";
    public static final String INIT_RELATIONSHIP = "init_relationship";
    public static final String INIT_EDUCATION = "init_education";
    public static final String INIT_INCOME = "init_income";
    public static final String init_occupation = "init_occupation";
    public static final String SAVE_TIME = "save_time";
    public static final String ICON_SYSTEM = "icon_system";
    public static final String SAVE_CURRENT_VIDEO_OR_VOICE_TIME = "save_current_video_or_voice_time";
    public static final String SAVE_COUNTIES = "save_counties";
    public static final String SAVE_YEJIAO_TIME="save_yejiao_time";
    public static final String YEJIAO_JSON="yejiao_json";
    public static final String YEJIAO_INVITE_STRING="yejiao_invite_string";
    public static final String SCREEN_HEIGHT="screen_height";
    public static final String SCREEN_WIDTH="screen_width";
    public static final String PAY_WAY_JSON="pay_way_json";
    public static void saveDataJson(String dataJson) {
        if (!TextUtils.isEmpty(dataJson)) {
            setJsonData(dataJson);
        }
    }
    /**
     * 保存支付数据
     */
    public static void savePayWayJson(String id) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, PAY_WAY_JSON, id);
    }

    /**
     * @return 获取支付的数据
     */
    public static String getPayWayJsonString() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, PAY_WAY_JSON, "0");
    }
    /**
     * 保存屏幕宽度
     */
    public static void saveScreenWidth(int id) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, SCREEN_WIDTH, id);
    }

    /**
     * @return 屏幕宽度
     */
    public static int getScreenWidth() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, SCREEN_WIDTH, 0);
    }

    /**
     * 保存屏幕高度
     */
    public static void saveScreenHeight(int id) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, SCREEN_HEIGHT, id);
    }

    /**
     * @return 屏幕高度
     */
    public static int getScreenHeight() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, SCREEN_HEIGHT, 0);
    }


    /**
     * 保存系统用户的头像
     */
    public static void setSystemIcon(String icon) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, ICON_SYSTEM, icon);
    }

    /**
     * @return 获取系统用户头像
     */
    public static String getSystemIcon() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, ICON_SYSTEM, "");
    }

    /**
     * 保存页脚过来时的时间
     */
    public static void saveYeJiaoTime(long time) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, SAVE_YEJIAO_TIME, String.valueOf(time));
    }

    /**
     * 获取页脚过来时的时间
     */
    public static long getYejiaoTime() {
        String intValue = SharedPreferenceUtil.getStringValue(mContext, NAME, SAVE_YEJIAO_TIME, null);
        if (!TextUtils.isEmpty(intValue) && intValue.length() > 0) {
            return Long.parseLong(intValue);
        } else {
            return 0;
        }
    }
    /**
     *  获取页脚的json数据
     */
    public static String getYeJiaoJsonData() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, YEJIAO_JSON, null);
    }
    /**
     * 保存页脚的json数据
     */
    public static void saveYeJiaoJsonData(String json) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, YEJIAO_JSON, json);
    }

    /**
     * 保存正在建立连接的文字
     */
    public static void saveYejiaoString(int flag) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, YEJIAO_INVITE_STRING,flag==1 ?"正在建立连接请稍等......" : null);
    }

    /**
     * @return 获取正在建立连接的文字
     */
    public static String getYejiaoString() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, YEJIAO_INVITE_STRING, null);
    }


    /**
     * 保存json数据
     */
    public static void setJsonData(String json) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, INIT_DATA_JSON, json);
    }

    /**
     * @return 获取json数据
     */
    public static String getJsonData() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, INIT_DATA_JSON, "0");
    }

    /**
     * 保存第一次发送语音或者视频的当前时间
     */
    public static void saveCurrentTime(long time) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, SAVE_CURRENT_VIDEO_OR_VOICE_TIME, String.valueOf(time));
    }

    /**
     * 获取第一次发送语音或者视频的时间
     */
    public static long getCurrentTime() {
        String intValue = SharedPreferenceUtil.getStringValue(mContext, NAME, SAVE_CURRENT_VIDEO_OR_VOICE_TIME, "0");
        if (!TextUtils.isEmpty(intValue) && intValue.length() > 0) {
            return Long.parseLong(intValue);
        } else {
            return 0;
        }
    }

    /**
     * 保存时间
     */
    public static void saveTime(long time) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, SAVE_TIME, String.valueOf(time));
    }

    /**
     * 获取时间
     */
    public static long getTime() {
        String intValue = SharedPreferenceUtil.getStringValue(mContext, NAME, SAVE_TIME, "0");
        if (!TextUtils.isEmpty(intValue) && intValue.length() > 0) {
            return Long.parseLong(intValue);
        } else {
            return 0;
        }
    }

    /**
     * 获得教育Map
     *
     * @return
     */
    public static Map<String, String> getEducationMap() {
        FaceBookAccount faceBookAccount = new Gson().fromJson(getJsonData(), FaceBookAccount.class);
        if (faceBookAccount != null) {
            return faceBookAccount.getEducationMap();
        } else {
            return null;
        }
    }

    /**
     * 获得教育List
     *
     * @return
     */
    public static List<String> getEducationMapValue() {
        Map<String, String> map = getEducationMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 获得婚姻状况Map
     *
     * @return
     */
    public static Map<String, String> getRelationshipMap() {
        FaceBookAccount faceBookAccount = new Gson().fromJson(getJsonData(), FaceBookAccount.class);
        if (faceBookAccount != null) {
            return faceBookAccount.getRelationshipMap();
        } else {
            return null;
        }
    }

    /**
     * 获得婚姻状况List
     *
     * @return
     */
    public static List<String> getRelationshipMapValue() {
        Map<String, String> map = getRelationshipMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 获得收入Map
     *
     * @return
     */
    public static Map<String, String> getIncomeMap() {
        FaceBookAccount faceBookAccount = new Gson().fromJson(getJsonData(), FaceBookAccount.class);
        if (faceBookAccount != null) {
            return faceBookAccount.getIncomeMap();
        } else {
            return null;
        }
    }

    /**
     * 获得收入List
     *
     * @return
     */
    public static List<String> getIncomeMapValue() {
        Map<String, String> map = getIncomeMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 获得职业Map
     *
     * @return
     */
    public static Map<String, String> getOccupationMap() {
        FaceBookAccount faceBookAccount = new Gson().fromJson(getJsonData(), FaceBookAccount.class);
        if (faceBookAccount != null) {
            return faceBookAccount.getOccupationMap();
        } else {
            return null;
        }
    }

    /**
     * 获得职业List
     *
     * @return
     */
    public static List<String> getOccupationMapValue() {
        Map<String, String> map = getOccupationMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 获取faceboo账号
     *
     * @return
     */
    public static String getfacebookaccount() {
        FaceBookAccount faceBookAccount = new Gson().fromJson(getJsonData(), FaceBookAccount.class);
        if (faceBookAccount != null) {
            return faceBookAccount.getGoldHostFacebook();
        } else {
            return null;
        }
    }

    /**
     * 根据指定的value获取指定的key
     */
    public static String getPersonMapKey(String value, int i) {
        Map<String, String> map = null;
        String key = null;
        if (i == 0) {
            map = getEducationMap();
        } else if (i == 1) {
            map = getRelationshipMap();
        } else if (i == 2) {
            map = getIncomeMap();
        } else if (i == 3) {
            map = getOccupationMap();
        }
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                key = entry.getKey();
            }
        }
        return key;
    }

    /**
     * 将map的key排序后，生成String
     *
     * @param map
     */
    private static String getMapValue(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        String value = iterator.next().getValue();
        if (!TextUtils.isEmpty(value)) {
            return value;
        } else {

            return null;
        }
    }

    /**
     * 将map的valuse排序后，生成String
     *
     * @param map
     */
    private static String getMapKey(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        String key = iterator.next().getKey();
        if (!TextUtils.isEmpty(key)) {
            return key;
        } else {

            return null;
        }
    }

    /**
     * 生成身高英寸/厘米对应map
     */
    public static Map<String, String> getHeightMap() {
        Map<String, String> heightMap = new HashMap<String, String>();
        heightMap.put("140", "4'7\"");
        heightMap.put("141", "4'8\"");
        heightMap.put("142", "4'8\"");
        heightMap.put("143", "4'8\"");
        heightMap.put("144", "4'9\"");
        heightMap.put("145", "4'9\"");
        heightMap.put("146", "4'9\"");
        heightMap.put("147", "4'10\"");
        heightMap.put("148", "4'10\"");
        heightMap.put("149", "4'10\"");
        heightMap.put("150", "4'11\"");
        heightMap.put("151", "4'11\"");
        heightMap.put("152", "4'11\"");
        heightMap.put("153", "5'0\"");
        heightMap.put("154", "5'0\"");
        heightMap.put("155", "5'0\"");
        heightMap.put("156", "5'1\"");
        heightMap.put("157", "5'1\"");
        heightMap.put("158", "5'1\"");
        heightMap.put("159", "5'2\"");
        heightMap.put("160", "5'2\"");
        heightMap.put("161", "5'2\"");
        heightMap.put("162", "5'3\"");
        heightMap.put("163", "5'3\"");
        heightMap.put("164", "5'3\"");
        heightMap.put("165", "5'4\"");
        heightMap.put("166", "5'4\"");
        heightMap.put("167", "5'4\"");
        heightMap.put("168", "5'5\"");
        heightMap.put("169", "5'5\"");
        heightMap.put("170", "5'5\"");
        heightMap.put("171", "5'6\"");
        heightMap.put("172", "5'6\"");
        heightMap.put("173", "5'6\"");
        heightMap.put("174", "5'7\"");
        heightMap.put("175", "5'7\"");
        heightMap.put("176", "5'7\"");
        heightMap.put("177", "5'8\"");
        heightMap.put("178", "5'8\"");
        heightMap.put("179", "5'8\"");
        heightMap.put("180", "5'9\"");
        heightMap.put("181", "5'9\"");
        heightMap.put("182", "5'9\"");
        heightMap.put("183", "5'10\"");
        heightMap.put("184", "5'10\"");
        heightMap.put("185", "5'10\"");
        heightMap.put("186", "5'11\"");
        heightMap.put("187", "5'11\"");
        heightMap.put("188", "5'11\"");
        heightMap.put("189", "6'0\"");
        heightMap.put("190", "6'0\"");
        heightMap.put("191", "6'0\"");
        heightMap.put("192", "6'1\"");
        heightMap.put("193", "6'1\"");
        heightMap.put("194", "6'1\"");
        heightMap.put("195", "6'2\"");
        heightMap.put("196", "6'2\"");
        heightMap.put("197", "6'2\"");
        heightMap.put("198", "6'3\"");
        heightMap.put("199", "6'3\"");
        heightMap.put("200", "6'3\"");
        heightMap.put("201", "6'4\"");
        heightMap.put("202", "6'4\"");
        heightMap.put("203", "6'4\"");
        heightMap.put("204", "6'5\"");
        heightMap.put("205", "6'5\"");
        heightMap.put("206", "6'5\"");
        heightMap.put("207", "6'6\"");
        heightMap.put("208", "6'6\"");
        heightMap.put("209", "6'6\"");
        heightMap.put("210", "6'7\"");
        heightMap.put("211", "6'7\"");
        heightMap.put("212", "6'7\"");
        heightMap.put("213", "6'8\"");
        heightMap.put("214", "6'8\"");
        heightMap.put("215", "6'8\"");
        heightMap.put("216", "6'9\"");
        heightMap.put("217", "6'9\"");
        heightMap.put("218", "6'9\"");
        heightMap.put("219", "6'10\"");
        heightMap.put("220", "6'10\"");
        return heightMap;
    }

    /**
     * 生成英寸（厘米）List
     *
     * @return
     */
    public static List<String> getInchCmList() {
        Map<String, String> heightMap = getHeightMap();
        Iterator<Map.Entry<String, String>> iterator = heightMap.entrySet().iterator();
        // 对key排序
        List<String> heightKeyList = new ArrayList<String>();
        while (iterator.hasNext()) {
            heightKeyList.add(iterator.next().getKey());
        }
        Collections.sort(heightKeyList);
        List<String> heightList = new ArrayList<String>();
        for (int i = 0; i < heightKeyList.size(); i++) {
            String inchCm = heightKeyList.get(i);
            if (!TextUtils.isEmpty(inchCm)) {
                heightList.add(inchCm + "cm");
            }
        }
        return heightList;
    }

    /**
     * 生成英寸List
     *
     * @return
     */
    public static List<String> getInchList() {
        Map<String, String> heightMap = getHeightMap();
        Iterator<Map.Entry<String, String>> iterator = heightMap.entrySet().iterator();
        // 对key排序
        List<String> heightValueList = new ArrayList<String>();
        while (iterator.hasNext()) {
            heightValueList.add(iterator.next().getValue());
        }

        List<String> heightList = new ArrayList<String>();
        for (int i = 0; i < heightValueList.size(); i++) {
            String inchCm = heightValueList.get(i);
            if (!TextUtils.isEmpty(inchCm) && !heightList.contains(inchCm)) {
                heightList.add(inchCm);
            }
        }
        Collections.sort(heightList, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {

                int i = Integer.parseInt(s.substring(0, 1)) - Integer.parseInt(t1.substring(0, 1));
                if (i == 0) {
                    return Integer.parseInt(s.substring(2, s.length() - 1)) - Integer.parseInt(t1.substring(2, t1.length() - 1));
                }
                return i;
            }
        });
        return heightList;
    }

    /**
     * 根据厘米获得对应的"英寸（厘米）"
     */
    public static String getInchCmByCm(String cm) {
        Map<String, String> heightMap = getHeightMap();
        if (!TextUtils.isEmpty(cm)) {
            String inch = heightMap.get(cm);
            if (!TextUtils.isEmpty(inch)) {
                return inch;
            }
        }
        return "";
    }

    /**
     * 根据英寸获得厘米
     */
    public static String getCmByInch(String in) {
        Map<String, String> heightMap = getHeightMap();

        for (Map.Entry<String, String> entry : heightMap.entrySet()) {
            if (in.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return "";
    }

    /**
     * 根据key获取value
     * 0为教育
     * 1为婚姻状况
     * 2为收入
     * 3为职业
     */
    public static String getValueByKey(String key, int flag) {
        Map<String, String> map = null;
        String value = null;
        if (flag == 0) {
            map = getEducationMap();
        } else if (flag == 1) {
            map = getRelationshipMap();
        } else if (flag == 2) {
            map = getIncomeMap();
        } else if (flag == 3) {
            map = getOccupationMap();
        }
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (entry.getKey().equals(key)) {
                value = entry.getValue();
            }
        }
        return value;
    }

    /**
     * 获得体重
     */
    public static List<String> getWeight() {
        List<String> list = new ArrayList<>();
        list.add("30");
        list.add("31");
        list.add("32");
        list.add("33");
        list.add("34");
        list.add("35");
        list.add("36");
        list.add("37");
        list.add("38");
        list.add("39");
        list.add("40");
        list.add("41");
        list.add("42");
        list.add("43");
        list.add("44");
        list.add("45");
        list.add("46");
        list.add("47");
        list.add("48");
        list.add("49");
        list.add("50");
        list.add("51");
        list.add("52");
        list.add("53");
        list.add("54");
        list.add("55");
        list.add("56");
        list.add("57");
        list.add("58");
        list.add("59");
        list.add("60");
        list.add("61");
        list.add("62");
        list.add("63");
        list.add("64");
        list.add("65");
        list.add("66");
        list.add("67");
        list.add("68");
        list.add("69");
        list.add("70");
        list.add("71");
        list.add("72");
        list.add("73");
        list.add("74");
        list.add("75");
        list.add("76");
        list.add("77");
        list.add("78");
        list.add("79");
        list.add("80");
        list.add("81");
        list.add("82");
        list.add("83");
        list.add("84");
        list.add("85");
        list.add("86");
        list.add("87");
        list.add("88");
        list.add("89");
        list.add("90");
        list.add("91");
        list.add("92");
        list.add("93");
        list.add("94");
        list.add("95");
        list.add("96");
        list.add("97");
        list.add("98");
        list.add("99");
        list.add("100");
        list.add("101");
        list.add("102");
        list.add("103");
        list.add("104");
        list.add("105");
        list.add("106");
        list.add("107");
        list.add("108");
        list.add("109");
        list.add("110");
        list.add("111");
        list.add("112");
        list.add("113");
        list.add("114");
        list.add("115");
        list.add("116");
        list.add("117");
        list.add("118");
        list.add("119");
        list.add("120");
        list.add("121");
        list.add("122");
        list.add("123");
        list.add("124");
        list.add("125");
        list.add("126");
        list.add("127");
        list.add("128");
        list.add("129");
        list.add("130");
        list.add("131");
        list.add("132");
        list.add("133");
        list.add("134");
        list.add("135");
        list.add("136");
        list.add("137");
        list.add("138");
        list.add("139");
        list.add("140");
        list.add("141");
        list.add("142");
        list.add("143");
        list.add("144");
        list.add("145");
        list.add("146");
        list.add("147");
        list.add("148");
        list.add("149");
        list.add("150");
        list.add("151");
        list.add("152");
        list.add("153");
        list.add("154");
        list.add("155");
        list.add("156");
        list.add("157");
        list.add("158");
        list.add("159");
        list.add("160");
        list.add("161");
        list.add("162");
        list.add("163");
        list.add("164");
        list.add("165");
        list.add("166");
        list.add("167");
        list.add("168");
        list.add("169");
        list.add("170");
        list.add("171");
        list.add("172");
        list.add("173");
        list.add("174");
        list.add("175");
        list.add("176");
        list.add("177");
        list.add("178");
        list.add("179");
        list.add("180");
        list.add("181");
        list.add("182");
        list.add("183");
        list.add("184");
        list.add("185");
        list.add("186");
        list.add("187");
        list.add("188");
        list.add("189");
        list.add("190");
        list.add("191");
        list.add("192");
        list.add("193");
        list.add("194");
        list.add("195");
        list.add("196");
        list.add("197");
        list.add("198");
        list.add("199");
        list.add("200");
        return list;
    }

    /**
     * 保存国家信息
     */
    public static void saveCountries(List<Country> countries) {
        if (null == countries || countries.size() <= 0) {
            return;
        }
        Gson gson = new Gson();
        //转换成json数据，再保存
        String strJson = gson.toJson(countries);
        SharedPreferenceUtil.setStringValue(mContext, NAME, SAVE_COUNTIES, strJson);

    }

    public static Map<String, String> getCountryMap() {
        Map<String, String> maps = null;
        String strJson = SharedPreferenceUtil.getStringValue(mContext, NAME, SAVE_COUNTIES, null);
        if (strJson == null) {
            return maps;
        }

        List<Country> datalist = new Gson().fromJson(strJson, new TypeToken<List<Country>>() {
        }.getType());

        if (datalist != null && datalist.size() > 0) {
            maps = new HashMap<>();
            for (int i = 0; i < datalist.size(); i++) {
                maps.put(datalist.get(i).getGuid(), datalist.get(i).getCountryEnglish());
            }
        }
        return maps;
    }

    /**
     * 获取国家名称（英文）
     */
    public static String getCountry(String countryNum) {
        String coutryName = "United States";
        if (TextUtils.isEmpty(countryNum)) {
            return coutryName;
        }
        Map<String, String> countryMap = getCountryMap();
        if (countryMap == null || countryMap.size() == 0) {
            return coutryName;
        } else {
            for (String key : countryMap.keySet()) {
                if (key.equals(countryNum)) {
                    coutryName = countryMap.get(key);
                }
            }
        }
        return coutryName;
    }
}
