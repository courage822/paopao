package com.popo.video.data.model;

import java.io.Serializable;
import java.util.Map;

/**
 * 服务端向客户端推送消息 封装
 *
 * @author lxl
 * @version 1.0
 * @since 2017-8-17
 */

/**
 *
 *
 * @author lxl
 * @version 1.0
 * @since 2017-8-19
 */
public class NettyMessage implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -4714460968143025450L;

    private String msgId;

    // 发件人信息
    private Long sendUserId;//userId
    private String sendUserName;//name
    private String sendUserAccount;//account
    private String sendUserIcon;//icon
    private String sendUserImageUrl;//icon
    private int sendUserFrom;

    private String sendTime;

    // 收件人信息
    private Long recvUserId;
    private int recvUserFrom;
    private String recvUserName;
    private String recvUserAccount;
    private String recvUserIcon;

    private short baseType; // 消息类型，1 文字 2语音 3图片
    private int extendType; // 消息业务扩展类型：1 聊天信 2 视频邀请信 3 礼物4 语音邀请信 5 发送礼物信 6 系统消息 7 视频邀请弹窗
    private String content; // 消息内容
    private String url; // 多媒体消息路径
    private String thumbnail;//私密视频
    private Integer audioSeconds; // 多媒体消息路径

    private String pushToken; // ios 离线推送 pushToken
    private int pushChannel; // ios 离线推送 pushToken

    private int type; // 定义3 为视频邀请 1 普通推送 2  4 下线通知   5匹配成功
    private int status; //1 发起 2 取消
    private String giftId; //解锁礼物
    private int visible; // 0 可见 1 不可见
    private int notice; // 0 不弹出通知 1 弹出 通知（是否推送栏显示）
    private Map<String, String> ext;//userPrice
    private String qaContent;
    private int hostCallTag;//区分是后台发的策略还是视频邀请信1是，0不是

    public int getHostCallTag() {
        return hostCallTag;
    }

    public void setHostCallTag(int hostCallTag) {
        this.hostCallTag = hostCallTag;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getQaContent() {
        return qaContent;
    }

    public void setQaContent(String qaContent) {
        this.qaContent = qaContent;
    }

    public int getNotice() {
        return notice;
    }

    public void setNotice(int notice) {
        this.notice = notice;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public int getVisible() {
        return visible;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Long getSendUserId() {
        return sendUserId;
    }

    public void setSendUserId(Long sendUserId) {
        this.sendUserId = sendUserId;
    }

    public String getSendUserName() {
        return sendUserName;
    }

    public void setSendUserName(String sendUserName) {
        this.sendUserName = sendUserName;
    }

    public String getSendUserIcon() {
        return sendUserIcon;
    }

    public void setSendUserIcon(String sendUserIcon) {
        this.sendUserIcon = sendUserIcon;
    }

    public short getBaseType() {
        return baseType;
    }

    public void setBaseType(short baseType) {
        this.baseType = baseType;
    }

    public int getExtendType() {
        return extendType;
    }

    public void setExtendType(int extendType) {
        this.extendType = extendType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Map<String, String> getExt() {
        return ext;
    }

    public void setExt(Map<String, String> ext) {
        this.ext = ext;
    }

    public int getSendUserFrom() {
        return sendUserFrom;
    }

    public void setSendUserFrom(int sendUserFrom) {
        this.sendUserFrom = sendUserFrom;
    }

    public int getRecvUserFrom() {
        return recvUserFrom;
    }

    public void setRecvUserFrom(int recvUserFrom) {
        this.recvUserFrom = recvUserFrom;
    }

    public Long getRecvUserId() {
        return recvUserId;
    }

    public void setRecvUserId(Long recvUserId) {
        this.recvUserId = recvUserId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getRecvUserName() {
        return recvUserName;
    }

    public void setRecvUserName(String recvUserName) {
        this.recvUserName = recvUserName;
    }

    public String getRecvUserIcon() {
        return recvUserIcon;
    }

    public void setRecvUserIcon(String recvUserIcon) {
        this.recvUserIcon = recvUserIcon;
    }

    public String getSendUserAccount() {
        return sendUserAccount;
    }

    public void setSendUserAccount(String sendUserAccount) {
        this.sendUserAccount = sendUserAccount;
    }

    public String getRecvUserAccount() {
        return recvUserAccount;
    }

    public void setRecvUserAccount(String recvUserAccount) {
        this.recvUserAccount = recvUserAccount;
    }

    public Integer getAudioSeconds() {
        return audioSeconds;
    }

    public void setAudioSeconds(Integer audioSeconds) {
        this.audioSeconds = audioSeconds;
    }

    public int getPushChannel() {
        return pushChannel;
    }

    public void setPushChannel(int pushChannel) {
        this.pushChannel = pushChannel;
    }

    public String getSendUserImageUrl() {
        return sendUserImageUrl == null ? "" : sendUserImageUrl;
    }

    public void setSendUserImageUrl(String sendUserImageUrl) {
        this.sendUserImageUrl = sendUserImageUrl;
    }

    @Override
    public String toString() {
        return "NettyMessage{" +
                "msgId='" + msgId + '\'' +
                ", sendUserId=" + sendUserId +
                ", sendUserName='" + sendUserName + '\'' +
                ", sendUserAccount='" + sendUserAccount + '\'' +
                ", sendUserIcon='" + sendUserIcon + '\'' +
                ", sendUserFrom=" + sendUserFrom +
                ", sendTime='" + sendTime + '\'' +
                ", recvUserId=" + recvUserId +
                ", recvUserFrom=" + recvUserFrom +
                ", recvUserName='" + recvUserName + '\'' +
                ", recvUserAccount='" + recvUserAccount + '\'' +
                ", recvUserIcon='" + recvUserIcon + '\'' +
                ", baseType=" + baseType +
                ", extendType=" + extendType +
                ", content='" + content + '\'' +
                ", url='" + url + '\'' +
                ", audioSeconds=" + audioSeconds +
                ", pushToken='" + pushToken + '\'' +
                ", pushChannel=" + pushChannel +
                ", type=" + type +
                ", status=" + status +
                ", giftId='" + giftId + '\'' +
                ", visible=" + visible +
                ", notice=" + notice +
                ", ext=" + ext +
                ", qaContent='" + qaContent + '\'' +
                '}';
    }
}
