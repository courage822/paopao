package com.popo.video.data.model;

/**
 * 发起/接受视频呼叫结果对象
 * Created by zhangdroid on 2017/6/23.
 */
public class VideoCall extends BaseModel {
    private int remoteStatus;// 对方状态 1、空闲 2、通话中 3、勿扰
    private String beanStatus;// 金币状态 -1、 金币不足 -2、 警告（不足三分钟）1、充足
    private String beanCount;// 金币数量
    private String hostPrice;// 播主价格（普通用户返回）

    public int getRemoteStatus() {
        return remoteStatus;
    }

    public void setRemoteStatus(int remoteStatus) {
        this.remoteStatus = remoteStatus;
    }

    public String getBeanStatus() {
        return beanStatus;
    }

    public void setBeanStatus(String beanStatus) {
        this.beanStatus = beanStatus;
    }

    public String getBeanCount() {
        return beanCount;
    }

    public void setBeanCount(String beanCount) {
        this.beanCount = beanCount;
    }

    public String getHostPrice() {
        return hostPrice;
    }

    public void setHostPrice(String hostPrice) {
        this.hostPrice = hostPrice;
    }
}
