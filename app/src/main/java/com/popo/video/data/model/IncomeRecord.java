package com.popo.video.data.model;

/**
 * Created by Administrator on 2017/7/14.
 * 提现记录
 */

public class IncomeRecord {
    private int guid;
    private int localId;
    private int remoteId;
    private int fromChannel;
    private int type;
    private int beanAmount;
    private long addTime;
    private int callMinute;
    private int giftType;
    private int incomeStatus;
    private int withdrawStatus;
    private String incomeCurrency;
    private double incomeAmount;
    private String giftId;//礼物id
    private int quantity;//礼物数量

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public int getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(int remoteId) {
        this.remoteId = remoteId;
    }

    public int getFromChannel() {
        return fromChannel;
    }

    public void setFromChannel(int fromChannel) {
        this.fromChannel = fromChannel;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getBeanAmount() {
        return beanAmount;
    }

    public void setBeanAmount(int beanAmount) {
        this.beanAmount = beanAmount;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public int getCallMinute() {
        return callMinute;
    }

    public void setCallMinute(int callMinute) {
        this.callMinute = callMinute;
    }

    public int getGiftType() {
        return giftType;
    }

    public void setGiftType(int giftType) {
        this.giftType = giftType;
    }

    public int getIncomeStatus() {
        return incomeStatus;
    }

    public void setIncomeStatus(int incomeStatus) {
        this.incomeStatus = incomeStatus;
    }

    public int getWithdrawStatus() {
        return withdrawStatus;
    }

    public void setWithdrawStatus(int withdrawStatus) {
        this.withdrawStatus = withdrawStatus;
    }

    public String getIncomeCurrency() {
        return incomeCurrency;
    }

    public void setIncomeCurrency(String incomeCurrency) {
        this.incomeCurrency = incomeCurrency;
    }

    public double getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(double incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    @Override
    public String toString() {
        return "IncomeRecord{" +
                "guid=" + guid +
                ", localId=" + localId +
                ", remoteId=" + remoteId +
                ", fromChannel=" + fromChannel +
                ", type=" + type +
                ", beanAmount=" + beanAmount +
                ", addTime=" + addTime +
                ", callMinute=" + callMinute +
                ", giftType=" + giftType +
                ", incomeStatus=" + incomeStatus +
                ", withdrawStatus=" + withdrawStatus +
                ", incomeCurrency='" + incomeCurrency + '\'' +
                ", incomeAmount=" + incomeAmount +
                '}';
    }
}
