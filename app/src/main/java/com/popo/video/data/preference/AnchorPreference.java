package com.popo.video.data.preference;

import android.content.Context;

import com.popo.video.base.BaseApplication;
import com.popo.library.util.SharedPreferenceUtil;
import com.popo.video.data.model.HostInfo;

/**
 * 保存播主账户信息
 * Created by zhangdroid on 2017/6/24.
 */
public class AnchorPreference {
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private AnchorPreference() {
    }

    public static final String NAME = "pre_anchor";
    public static final String ANCHOR_PRICE = "anchor_price";
    public static final String ANCHOR_INCOME = "anchor_income";
    public static final String ANCHOR_BALANCE = "anchor_IncomeBalance";
    public static final String ANCHOR_MONTH_BALANCE = "anchor_month_banlance";
    public static final String ANCHOR_AUDIO_PRICE = "anchor_audio_price";
    public static final String TOTAL_SEND_TEXT = "total_send_text";
    public static final String TOTAL_SEND_PHOTO = "total_send_photo";
    public static final String TOTAL_SEND_VIDEO = "total_send_video";

    public static void saveHostInfo(HostInfo hostInfo) {
        if (null != hostInfo) {
            setPrice((int) hostInfo.getPrice());
            setIncome((int) hostInfo.getTotalIncome());
            setCurrentBalance(hostInfo.getIncomeBalance());
            setMonthBalance(hostInfo.getMonthlyIncome());
            setAudioPrice(hostInfo.getAudioPrice());
            setTotalSendText(hostInfo.getTotalSendText());
            setTotalSendPhoto(hostInfo.getTotalSendPhoto());
            setTotalSendVideo(hostInfo.getTotalSendVideo());
        }
    }

    /**
     * 发送文本索要钻石总数
     *
     * @param totalSendVideo
     */
    private static void setTotalSendVideo(int totalSendVideo) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, TOTAL_SEND_VIDEO, totalSendVideo);

    }

    public static int getTotalSendVideo() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, TOTAL_SEND_VIDEO, 0);
    }

    /**
     * 发送图片索要钻石总数
     *
     * @param totalSendPhoto
     */
    private static void setTotalSendPhoto(int totalSendPhoto) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, TOTAL_SEND_PHOTO, totalSendPhoto);

    }

    public static int getTotalSendPhoto() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, TOTAL_SEND_PHOTO, 0);
    }

    /**
     * 发送视频索要钻石总数
     *
     * @param totalSendText
     */

    private static void setTotalSendText(int totalSendText) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, TOTAL_SEND_TEXT, totalSendText);

    }

    public static int getTotalSendText() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, TOTAL_SEND_TEXT, 0);
    }

    /**
     * 保存主播语音的价格
     *
     * @param price
     */
    public static void setAudioPrice(int price) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, ANCHOR_AUDIO_PRICE, price);
    }

    public static int getAudioPrice() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, ANCHOR_AUDIO_PRICE, 0);
    }

    /**
     * 设置主播当前价格
     *
     * @param price
     */
    public static void setPrice(int price) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, ANCHOR_PRICE, String.valueOf(price));
    }

    /**
     * @return 主播的当前价格
     */
    public static String getPrice() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, ANCHOR_PRICE, "0");
    }

    /**
     * 保存用户账户累计收入金币数
     *
     * @param income
     */
    public static void setIncome(int income) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, ANCHOR_INCOME, String.valueOf(income));
    }

    /**
     * @return 用户账户累计收入金币数
     */
    public static String getIncome() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, ANCHOR_INCOME, "0");
    }

    /**
     * 保存用户账户当前金币数
     *
     * @param
     */
    public static void setCurrentBalance(float incomeBalance) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, ANCHOR_BALANCE, String.valueOf(incomeBalance));
    }

    /**
     * @return 用户账户当前金币数
     */
    public static String getCurrentBalance() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, ANCHOR_BALANCE, "0");
    }

    /**
     * 保存用户的月累计收入
     */
    public static void setMonthBalance(long monthBalance) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, ANCHOR_MONTH_BALANCE, String.valueOf(monthBalance));
    }

    /**
     * 获取用户的月累计收入
     */
    public static String getAnchorMonthBalance() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, ANCHOR_MONTH_BALANCE, "0");
    }

}
