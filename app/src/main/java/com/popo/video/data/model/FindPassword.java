package com.popo.video.data.model;

/**
 * 找回密码对象
 * Created by zhangdroid on 2017/6/1.
 */
public class FindPassword {
    private String account;// 帐号
    private String password;// 密码

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
