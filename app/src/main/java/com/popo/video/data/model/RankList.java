package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/10/31.
 */

public class RankList {
    private UserBase userBase;
    private UserMend userMend;

    public UserBase getUserBase() {
        return userBase;
    }

    public void setUserBase(UserBase userBase) {
        this.userBase = userBase;
    }

    public UserMend getUserMend() {
        return userMend;
    }

    public void setUserMend(UserMend userMend) {
        this.userMend = userMend;
    }

    @Override
    public String toString() {
        return "RankList{" +
                "userBase=" + userBase +
                ", userMend=" + userMend +
                '}';
    }
}
