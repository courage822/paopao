package com.popo.video.data.model;

import java.util.List;

/**
 * Created by Administrator on 2017/6/14.
 */

public class FollowUser extends BaseModel {
    private String pageSize;
    private String pageNum;
    private List<UserBase> listUser;

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public List<UserBase> getUserBaseList() {
        return listUser;
    }

    public void setUserBaseList(List<UserBase> userBaseList) {
        this.listUser = userBaseList;
    }

    @Override
    public String toString() {
        return "FollowUser{" +
                "pageNum='" + pageSize + '\'' +
                ", userBaseList=" + listUser +
                '}';
    }
}
