package com.popo.video.data.constant;

import com.popo.video.common.ParamsUtils;
import com.popo.video.common.Util;

import java.util.List;

/**
 * 公用数据字典常量管理类。
 * <p>所有数据字典常量从后台接口获取，若接口请求失败或返回数据为空，则使用本地数据字典</p>
 * Created by zhangdroid on 2017/6/9.
 */
public class DataDictManager {

    private DataDictManager() {
    }




    /**
     * @return 获得州信息的常量集合
     */
    public static List<String> getStatteList() {
        List<String> stateString = ParamsUtils.getMapListString(Util.getStateString());
        return stateString;
    }

    /**
     * @return 获得星座信息的常量集合
     */
    public static List<String> getSignList() {
        List<String> stateString = ParamsUtils.getMapListString(Util.getSignString());
        return stateString;
    }

}
