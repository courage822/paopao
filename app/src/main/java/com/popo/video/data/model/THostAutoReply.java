package com.popo.video.data.model;

/**
 * Created by xuzhaole on 2017/11/23.
 */

public class THostAutoReply extends BaseModel {
    private String guid;//自增ID
    private String userId;//用户ID
    private String content;//文本内容/语音url
    private String contentType;//1.文本类型,2.语音类型
    private String audioSecond;//语音时长
    private String replyType;//回复类型. 1.自动回复,2.主动打招呼
    private String auditStatus;//审核状态. 1.通过审核,0.审核中,-1.审核失败
    private String useStatus;//使用状态. 1.主播选中的自动回复/主动打招呼;0.非选中状态;-1.删除状态(查询时不显示)
    private String addTime;//添加时间
    private String updateTime;//更新时间

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getAudioSecond() {
        return audioSecond;
    }

    public void setAudioSecond(String audioSecond) {
        this.audioSecond = audioSecond;
    }

    public String getReplyType() {
        return replyType;
    }

    public void setReplyType(String replyType) {
        this.replyType = replyType;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getUseStatus() {
        return useStatus;
    }

    public void setUseStatus(String useStatus) {
        this.useStatus = useStatus;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "THostAutoReply{" +
                "guid='" + guid + '\'' +
                ", userId='" + userId + '\'' +
                ", content='" + content + '\'' +
                ", contentType='" + contentType + '\'' +
                ", audioSecond='" + audioSecond + '\'' +
                ", replyType='" + replyType + '\'' +
                ", auditStatus='" + auditStatus + '\'' +
                ", useStatus='" + useStatus + '\'' +
                ", addTime='" + addTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
