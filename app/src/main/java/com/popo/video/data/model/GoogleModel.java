package com.popo.video.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by xuzhaole on 2018/4/3.
 */

public class GoogleModel implements Parcelable {
    private List<AddressComponent> address_components;

    public List<AddressComponent> getAddress_components() {
        return address_components;
    }

    public void setAddress_components(List<AddressComponent> address_components) {
        this.address_components = address_components;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.address_components);
    }

    public GoogleModel() {
    }

    protected GoogleModel(Parcel in) {
        this.address_components = in.createTypedArrayList(AddressComponent.CREATOR);
    }

    public static final Parcelable.Creator<GoogleModel> CREATOR = new Parcelable.Creator<GoogleModel>() {
        @Override
        public GoogleModel createFromParcel(Parcel source) {
            return new GoogleModel(source);
        }

        @Override
        public GoogleModel[] newArray(int size) {
            return new GoogleModel[size];
        }
    };

    @Override
    public String toString() {
        return "GoogleModel{" +
                "address_components=" + address_components +
                '}';
    }
}
