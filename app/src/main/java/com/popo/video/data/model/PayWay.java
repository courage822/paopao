package com.popo.video.data.model;

import java.util.List;

/**
 * 支付渠道信息对象
 * Created by zhangdroid on 2017/6/10.
 */
public class PayWay extends BaseModel {
    private List<PayDict> dictPayList;
    private int vipCount;
    private List<String> descList;
    private List<UserVip> vipList;

    public List<UserVip> getVipList() {
        return vipList;
    }

    public void setVipList(List<UserVip> vipList) {
        this.vipList = vipList;
    }

    public int getVipCount() {
        return vipCount;
    }

    public void setVipCount(int vipCount) {
        this.vipCount = vipCount;
    }

    public List<String> getDescList() {
        return descList;
    }

    public void setDescList(List<String> descList) {
        this.descList = descList;
    }

    public List<PayDict> getDictPayList() {
        return dictPayList;
    }

    public void setDictPayList(List<PayDict> dictPayList) {
        this.dictPayList = dictPayList;
    }

    @Override
    public String toString() {
        return "PayWay{" +
                "dictPayList=" + dictPayList +
                ", vipCount=" + vipCount +
                ", descList=" + descList +
                ", userVips=" + vipList +
                '}';
    }
}
