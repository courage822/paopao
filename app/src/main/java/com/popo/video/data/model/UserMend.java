package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/9/13.
 */

public class UserMend {
    private String guid;
    private String userId;
    private String photoCount;
    private String followOtherCount;
    private String followMeCount;
    private String lastLoginTime;
    private String lastLogoutTime;
    private String isLogin;
    private String weight;
    private String incomeId;
    private String relationshipId;
    private String occupationId;
    private String educationId;
    private String height;
    private int sendGiftCount;
    private int receiveGiftCount;

    public int getSendGiftCount() {
        return sendGiftCount;
    }

    public void setSendGiftCount(int sendGiftCount) {
        this.sendGiftCount = sendGiftCount;
    }

    public int getReceiveGiftCount() {
        return receiveGiftCount;
    }

    public void setReceiveGiftCount(int receiveGiftCount) {
        this.receiveGiftCount = receiveGiftCount;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(String photoCount) {
        this.photoCount = photoCount;
    }

    public String getFollowOtherCount() {
        return followOtherCount;
    }

    public void setFollowOtherCount(String followOtherCount) {
        this.followOtherCount = followOtherCount;
    }

    public String getFollowMeCount() {
        return followMeCount;
    }

    public void setFollowMeCount(String followMeCount) {
        this.followMeCount = followMeCount;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastLogoutTime() {
        return lastLogoutTime;
    }

    public void setLastLogoutTime(String lastLogoutTime) {
        this.lastLogoutTime = lastLogoutTime;
    }

    public String getIsLogin() {
        return isLogin;
    }

    public void setIsLogin(String isLogin) {
        this.isLogin = isLogin;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getIncomeId() {
        return incomeId;
    }

    public void setIncomeId(String incomeId) {
        this.incomeId = incomeId;
    }

    public String getRelationshipId() {
        return relationshipId;
    }

    public void setRelationshipId(String relationshipId) {
        this.relationshipId = relationshipId;
    }

    public String getOccupationId() {
        return occupationId;
    }

    public void setOccupationId(String occupationId) {
        this.occupationId = occupationId;
    }

    public String getEducationId() {
        return educationId;
    }

    public void setEducationId(String educationId) {
        this.educationId = educationId;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "UserMend{" +
                "guid='" + guid + '\'' +
                ", userId='" + userId + '\'' +
                ", photoCount='" + photoCount + '\'' +
                ", followOtherCount='" + followOtherCount + '\'' +
                ", followMeCount='" + followMeCount + '\'' +
                ", lastLoginTime='" + lastLoginTime + '\'' +
                ", lastLogoutTime='" + lastLogoutTime + '\'' +
                ", isLogin='" + isLogin + '\'' +
                ", weight='" + weight + '\'' +
                ", incomeId='" + incomeId + '\'' +
                ", relationshipId='" + relationshipId + '\'' +
                ", occupationId='" + occupationId + '\'' +
                ", educationId='" + educationId + '\'' +
                ", height='" + height + '\'' +
                '}';
    }
}
