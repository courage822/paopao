package com.popo.video.data.model;

/**
 * Created by Administrator on 2017/6/9.
 */

public class UpLoadMyInfo extends BaseModel {
    private UserBase userBase;//个人资料

    public UserBase getUserBase() {
        return userBase;
    }

    public void setUserBase(UserBase userBase) {
        this.userBase = userBase;
    }

    @Override
    public String toString() {
        return "UpLoadMyInfo{" +
                "userBase=" + userBase +
                '}';
    }
}
