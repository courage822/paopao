package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/9/16.
 */

public class PurchaseHowMoneyKeys extends BaseModel {
    private int keyStatus;
    private int payIntercept;

    public int getKeyStatus() {
        return keyStatus;
    }

    public void setKeyStatus(int keyStatus) {
        this.keyStatus = keyStatus;
    }

    public int getPayIntercept() {
        return payIntercept;
    }

    public void setPayIntercept(int payIntercept) {
        this.payIntercept = payIntercept;
    }

}
