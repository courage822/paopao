package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/9/14.
 */

public class UserKey {
    private String guid;

    private String userId;
    private int counts;

    private String lastAddAmount;
    private String lastOrderId;

    private String addTime;
    private String updateTime;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    public String getLastAddAmount() {
        return lastAddAmount;
    }

    public void setLastAddAmount(String lastAddAmount) {
        this.lastAddAmount = lastAddAmount;
    }

    public String getLastOrderId() {
        return lastOrderId;
    }

    public void setLastOrderId(String lastOrderId) {
        this.lastOrderId = lastOrderId;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
