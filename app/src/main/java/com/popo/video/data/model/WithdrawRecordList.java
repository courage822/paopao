package com.popo.video.data.model;

import java.util.List;

/**
 * Created by Administrator on 2017/7/14.
 * 提现记录列表
 */

public class WithdrawRecordList extends BaseModel {
    private List<WithdrawRecord> withdrawFlowList;

    public List<WithdrawRecord> getListRecord() {
        return withdrawFlowList;
    }

    public void setListRecord(List<WithdrawRecord> withdrawFlowList) {
        this.withdrawFlowList = withdrawFlowList;
    }

}