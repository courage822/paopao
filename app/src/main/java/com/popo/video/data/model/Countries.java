package com.popo.video.data.model;

import java.util.List;

/**
 * Created by Administrator on 2017/7/6.
 */

public class Countries extends BaseModel {

    private List<Country> countrys;

    public List<Country> getCountrys() {
        return countrys;
    }

    public void setCountrys(List<Country> countrys) {
        this.countrys = countrys;
    }

    @Override
    public String toString() {
        return "Countries{" +
                "countrys=" + countrys +
                '}';
    }
}
