package com.popo.video.data.model;

/**
 * Created by Administrator on 2017/6/10.
 */
public class UpLoadMyPhoto extends BaseModel {
    private UserPhoto userPhoto;

    public UserPhoto getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(UserPhoto userPhoto) {
        this.userPhoto = userPhoto;
    }
}
