package com.popo.video.data.model;

import java.util.List;

/**
 * Created by WangYong on 2017/11/3.
 */

public class GiftsDictiorary extends BaseModel {
    private List<DictPayGift> giftList;

    private String[] askGiftVideo;
    private String[] askGiftPhoto;
    private String[] secretMessage;

    public String[] getAskGiftVideo() {
        return askGiftVideo;
    }

    public void setAskGiftVideo(String[] askGiftVideo) {
        this.askGiftVideo = askGiftVideo;
    }

    public String[] getAskGiftPhoto() {
        return askGiftPhoto;
    }

    public void setAskGiftPhoto(String[] askGiftPhoto) {
        this.askGiftPhoto = askGiftPhoto;
    }

    public String[] getSecretMessage() {
        return secretMessage;
    }

    public void setSecretMessage(String[] secretMessage) {
        this.secretMessage = secretMessage;
    }

    public List<DictPayGift> getGiftList() {
        return giftList;
    }

    public void setGiftList(List<DictPayGift> giftList) {
        this.giftList = giftList;
    }

    @Override
    public String toString() {
        return "GiftsDictiorary{" +
                "giftList=" + giftList +
                '}';
    }
}
