package com.popo.video.data.model;

/**
 * 登录对象
 * Created by zhangdroid on 2017/5/24.
 */
public class Login extends BaseModel {
    private UserDetail userDetail;
    private String islj;//是否看信拦截 1：看信拦截（拦截后判断isvip）  2:写信拦截

    public String getIslj() {
        return islj == null ? "" : islj;
    }

    public void setIslj(String islj) {
        this.islj = islj;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    @Override
    public String toString() {
        return "Login{" +
                "userDetail=" + userDetail +
                '}';
    }
}
