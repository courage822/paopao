package com.popo.video.data.model;

/**
 * 声网信令系统token
 * Created by zhangdroid on 2017/6/14.
 */
public class AgoraToken extends BaseModel {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
