package com.popo.video.data.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

/**
 * Created by Administrator on 2017/7/14.
 */

public class HuanXinUser implements Comparable<HuanXinUser>{
    private String hxId;
    private String hxName;
    private String hxIcon;
    private String account;
    private String isRead;
    private int extendType;//现在用于消息业务扩展类型。1 聊天信 2 视频邀请信 3 礼物4 语音邀请信 5 发送礼物信 6 系统消息 7 视频邀请弹窗
    private String lastMsg;
    private int msgNum;
    private String msgTime;
    public HuanXinUser(String hxId, String hxName, String hxIcon, String account,String isRead,String lastMsg,int msgNum,String msgTime,int extendType) {
        this.hxId = hxId;
        this.hxName = hxName;
        this.hxIcon = hxIcon;
        this.account=account;
        this.isRead = isRead;
        this.lastMsg=lastMsg;
        this.msgNum=msgNum;
        this.msgTime=msgTime;
        this.extendType = extendType;
    }

    private boolean isSelect;//是否被选中，用于消息列表编辑功能

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public HuanXinUser() {
    }

    public int getExtendType() {
        return extendType;
    }

    public void setExtendType(int extendType) {
        this.extendType = extendType;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public int getMsgNum() {
        return msgNum;
    }

    public void setMsgNum(int msgNum) {
        this.msgNum = msgNum;
    }

    public String getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(String lastMsg) {
        this.lastMsg = lastMsg;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getHxId() {
        return hxId;
    }

    public void setHxId(String hxId) {
        this.hxId = hxId;
    }

    public String getHxName() {
        return hxName;
    }

    public void setHxName(String hxName) {
        this.hxName = hxName;
    }

    public String getHxIcon() {
        return hxIcon;
    }

    public void setHxIcon(String hxIcon) {
        this.hxIcon = hxIcon;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    @Override
    public String toString() {
        return "HuanXinUser{" +
                "hxId='" + hxId + '\'' +
                ", hxName='" + hxName + '\'' +
                ", hxIcon='" + hxIcon + '\'' +
                ", account='" + account + '\'' +
                ", isRead='" + isRead + '\'' +
                ", lastMsg='" + lastMsg + '\'' +
                ", msgNum=" + msgNum +
                ", msgTime='" + msgTime + '\'' +
                '}';
    }

    @Override
    public int compareTo(@NonNull HuanXinUser o) {
        String msgTime = o.getMsgTime();
        String msgTime1 = this.getMsgTime();
        int l =0;
        int l1=0;
        if(!TextUtils.isEmpty(msgTime)&&!TextUtils.isEmpty(msgTime1)&&msgTime.length()>0&&msgTime1.length()>0){
            l = (int)Long.parseLong(msgTime);
            l1 = (int)Long.parseLong(msgTime1);
        }
        if(o.getMsgNum()==this.getMsgNum()){
            return l-l1;
        }else{
            return o.getMsgNum()-this.getMsgNum();
        }
    }
}
