package com.popo.video.data.model;

import java.util.List;

/**
 * Created by WangYong on 2017/10/31.
 */

public class CharmandAndDranking extends BaseModel {
    private List<RankList> rankList;

    public List<RankList> getRankList() {
        return rankList;
    }

    public void setRankList(List<RankList> rankList) {
        this.rankList = rankList;
    }

    @Override
    public String toString() {
        return "CharmandAndDranking{" +
                "rankList=" + rankList +
                '}';
    }
}
