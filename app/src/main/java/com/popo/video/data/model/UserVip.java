package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/10/9.
 */

public class UserVip {
    private long guid;//{自增ID}
    private long userId;//{用户ID}
    private String orderId;
    private String serviceName;//{服务名称}
    private String nickName;//{用户昵称}
    private long addTime;//{添加时间}
    private String startTime;//{服务开始时间}
    private long endTime;//{服务结束时间}
    private int isvalid;
    private String serviceId;
    private String fromChannel;

    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(int isvalid) {
        this.isvalid = isvalid;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getFromChannel() {
        return fromChannel;
    }

    public void setFromChannel(String fromChannel) {
        this.fromChannel = fromChannel;
    }

    @Override
    public String toString() {
        return "UserVip{" +
                "guid=" + guid +
                ", userId=" + userId +
                ", orderId='" + orderId + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", nickName='" + nickName + '\'' +
                ", addTime=" + addTime +
                ", startTime='" + startTime + '\'' +
                ", endTime=" + endTime +
                ", isvalid=" + isvalid +
                ", serviceId='" + serviceId + '\'' +
                ", fromChannel='" + fromChannel + '\'' +
                '}';
    }
}
