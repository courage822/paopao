package com.popo.video.data.model;

/**
 * Created by Administrator on 2017/12/5.
 */

public class QaMsg {
    private String msgId;
    private String userId;
    private int flag;
    public QaMsg(){

    }
    public QaMsg(String msgId, String userId, int flag) {
        this.msgId = msgId;
        this.userId = userId;
        this.flag = flag;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "QaMsg{" +
                "msgId='" + msgId + '\'' +
                ", userId='" + userId + '\'' +
                ", flag=" + flag +
                '}';
    }
}
