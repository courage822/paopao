package com.popo.video.data.model;

/**
 * 视频时消息对象
 * Created by zhangdroid on 2017/6/22.
 */
public class VideoMsg {
    private String nickname;// 用户昵称
    private String message;// 消息内容
    private int type;//0为我发,1为对方发

    public VideoMsg(String nickname, String message, int type) {
        this.nickname = nickname;
        this.message = message;
        this.type = type;
    }

    public VideoMsg() {
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
