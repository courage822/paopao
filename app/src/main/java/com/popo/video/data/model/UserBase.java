package com.popo.video.data.model;

/**
 * 用户基础信息对象
 * Created by zhangdroid on 2017/6/1.
 */
public class UserBase {
    private long guid; // 用户id
    private String account;// 账号
    private String password;// 密码
    private int userType;// 0是普通用户，1是播主
    private String status; // 状态(1、空闲 2、在聊 3、勿扰)
    private String email;// 邮箱
    private int age;// 年龄
    private String birthday;// 生日
    private int sign;// 星座
    private int gender;// 性别（0、男性 1、女性）
    private String nickName;// 昵称
    private String ownWords;// 签名
    private String icon;
    private String iconUrl;// 原图
    private String iconUrlMiddle;// 中图
    private String iconUrlMininum;// 小图
    private String country;// 国家
    private String iconStatus;//头像状态
    private String state;// 州
    private String city;// 城市
    private String language;
    private String spokenLanguage;// 支持的语言
    private int fromChannel;// 渠道号
    private int product;// 产品号
    private String regTime19;

    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getOwnWords() {
        return ownWords;
    }

    public void setOwnWords(String ownWords) {
        this.ownWords = ownWords;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getIconUrlMiddle() {
        return iconUrlMiddle;
    }

    public void setIconUrlMiddle(String iconUrlMiddle) {
        this.iconUrlMiddle = iconUrlMiddle;
    }

    public String getIconUrlMininum() {
        return iconUrlMininum;
    }

    public void setIconUrlMininum(String iconUrlMininum) {
        this.iconUrlMininum = iconUrlMininum;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSpokenLanguage() {
        return spokenLanguage;
    }

    public void setSpokenLanguage(String spokenLanguage) {
        this.spokenLanguage = spokenLanguage;
    }

    public int getFromChannel() {
        return fromChannel;
    }

    public void setFromChannel(int fromChannel) {
        this.fromChannel = fromChannel;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public String getRegTime19() {
        return regTime19;
    }

    public void setRegTime19(String regTime19) {
        this.regTime19 = regTime19;
    }

    public String getIconStatus() {
        return iconStatus;
    }

    public void setIconStatus(String iconStatus) {
        this.iconStatus = iconStatus;
    }

    @Override
    public String toString() {
        return "UserBase{" +
                "guid=" + guid +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", userType=" + userType +
                ", status='" + status + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", birthday='" + birthday + '\'' +
                ", sign=" + sign +
                ", gender=" + gender +
                ", nickName='" + nickName + '\'' +
                ", ownWords='" + ownWords + '\'' +
                ", icon='" + icon + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                ", iconUrlMiddle='" + iconUrlMiddle + '\'' +
                ", iconUrlMininum='" + iconUrlMininum + '\'' +
                ", country='" + country + '\'' +
                ", iconStatus='" + iconStatus + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", language='" + language + '\'' +
                ", spokenLanguage='" + spokenLanguage + '\'' +
                ", fromChannel=" + fromChannel +
                ", product=" + product +
                ", regTime19='" + regTime19 + '\'' +
                '}';
    }
}
