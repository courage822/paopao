package com.popo.video.data.model;

/**
 * Created by WangYong on 2018/1/22.
 */

public class CallVideo extends BaseModel{
    private UserBase callVidelUser;
    private String  callVidelUrl;

    public UserBase getCallVidelUser() {
        return callVidelUser;
    }

    public void setCallVidelUser(UserBase callVidelUser) {
        this.callVidelUser = callVidelUser;
    }

    public String getCallVidelUrl() {
        return callVidelUrl;
    }

    public void setCallVidelUrl(String callVidelUrl) {
        this.callVidelUrl = callVidelUrl;
    }

    @Override
    public String toString() {
        return "CallVideo{" +
                "callVidelUser=" + callVidelUser +
                ", callVidelUrl='" + callVidelUrl + '\'' +
                '}';
    }
}
