package com.popo.video.data.model;

/**
 * 视频记录对象
 * Created by zhangdroid on 2017/7/6.
 */
public class VideoRecord {
    private long guid;// 自增ID
    private String localUserId;// 本方ID
    private String remoteUserId;// 对方ID
    private long beginTime;// 开始时间
    private long endTime;// 结束时间
    private String callStatus;// 通话类型 1 拨出 2 收到 3拒接（本方拒接）
    private String totalMinute;// 通话时长
    private long recordTime;// 记录时间
    private UserBase userBase;// 用户信息

    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }

    public String getLocalUserId() {
        return localUserId;
    }

    public void setLocalUserId(String localUserId) {
        this.localUserId = localUserId;
    }

    public String getRemoteUserId() {
        return remoteUserId;
    }

    public void setRemoteUserId(String remoteUserId) {
        this.remoteUserId = remoteUserId;
    }

    public long getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(long beginTime) {
        this.beginTime = beginTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(String callStatus) {
        this.callStatus = callStatus;
    }

    public String getTotalMinute() {
        return totalMinute;
    }

    public void setTotalMinute(String totalMinute) {
        this.totalMinute = totalMinute;
    }

    public long getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(long recordTime) {
        this.recordTime = recordTime;
    }

    public UserBase getUserBase() {
        return userBase;
    }

    public void setUserBase(UserBase userBase) {
        this.userBase = userBase;
    }

    @Override
    public String toString() {
        return "VideoRecord{" +
                "guid=" + guid +
                ", localUserId='" + localUserId + '\'' +
                ", remoteUserId='" + remoteUserId + '\'' +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", callStatus='" + callStatus + '\'' +
                ", totalMinute='" + totalMinute + '\'' +
                ", recordTime=" + recordTime +
                ", userBase=" + userBase +
                '}';
    }
}