package com.popo.video.data.model;

import java.util.List;

/**
 * 用户详细信息对象
 * Created by zhangdroid on 2017/6/2.
 */
public class UserDetail {
    private UserBase userBase;// 用户基础信息
    private List<UserPhoto> userPhotos;// 用户图片列表
    private List<TUserVideoShow> userVideoShows;// 用户视频秀
    private HostInfo hostInfo;// 播主信息
    private UserBean userBean;// 普通用户金币信息
    private String isFollow;// 1为已经关注，0没有关注
    private UserMend userMend;
    private UserKey userKey;
    private String vipDays;
    private String isWriteMsgFree;
    private String isReadMsgFree;
    private int isSayHello;//1为已经打招呼，0没有打招呼
    private int isBlock;// 0为未拉黑  1为已拉黑

    public int getIsSayHello() {
        return isSayHello;
    }

    public void setIsSayHello(int isSayHello) {
        this.isSayHello = isSayHello;
    }

    public String getIsWriteMsgFree() {
        return isWriteMsgFree;
    }

    public void setIsWriteMsgFree(String isWriteMsgFree) {
        this.isWriteMsgFree = isWriteMsgFree;
    }

    public UserKey getUserKey() {
        return userKey;
    }

    public void setUserKey(UserKey userKey) {
        this.userKey = userKey;
    }

    public String getVipDays() {
        return vipDays;
    }

    public void setVipDays(String vipDays) {
        this.vipDays = vipDays;
    }

    public UserMend getUserMend() {
        return userMend;
    }

    public void setUserMend(UserMend tUserMend) {
        this.userMend = tUserMend;
    }

    public UserBase getUserBase() {
        return userBase;
    }

    public void setUserBase(UserBase userBase) {
        this.userBase = userBase;
    }

    public List<UserPhoto> getUserPhotos() {
        return userPhotos;
    }

    public void setUserPhotos(List<UserPhoto> userPhotos) {
        this.userPhotos = userPhotos;
    }

    public HostInfo getHostInfo() {
        return hostInfo;
    }

    public void setHostInfo(HostInfo hostInfo) {
        this.hostInfo = hostInfo;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public String getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(String isFollow) {
        this.isFollow = isFollow;
    }

    public int getIsBlock() {
        return isBlock;
    }

    public void setIsBlock(int isBlock) {
        this.isBlock = isBlock;
    }

    public void setUserVideoShows(List<TUserVideoShow> userVideoShows) {
        this.userVideoShows = userVideoShows;
    }

    public List<TUserVideoShow> getUserVideoShows() {
        return userVideoShows;
    }


    @Override
    public String toString() {
        return "UserDetail{" +
                "userBase=" + userBase +
                ", userPhotos=" + userPhotos +
                ", userVideoShows=" + userVideoShows +
                ", hostInfo=" + hostInfo +
                ", userBean=" + userBean +
                ", isFollow='" + isFollow + '\'' +
                ", userMend=" + userMend +
                ", userKey=" + userKey +
                ", vipDays='" + vipDays + '\'' +
                ", isWriteMsgFree='" + isWriteMsgFree + '\'' +
                ", isSayHello=" + isSayHello +
                ", isBlock=" + isBlock +
                '}';
    }
}
