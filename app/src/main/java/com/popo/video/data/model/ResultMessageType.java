package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/11/8.
 */

public class ResultMessageType extends BaseModel {

    private int beanCount;
    private int beanStatus;

    private ResultMessage resultMessage;

    public int getBeanCount() {
        return beanCount;
    }

    public void setBeanCount(int beanCount) {
        this.beanCount = beanCount;
    }

    public int getBeanStatus() {
        return beanStatus;
    }

    public void setBeanStatus(int beanStatus) {
        this.beanStatus = beanStatus;
    }

    public ResultMessage getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(ResultMessage resultMessage) {
        this.resultMessage = resultMessage;
    }

    @Override
    public String toString() {
        return "ResultMessageType{" +
                "resultMessage=" + resultMessage +
                '}';
    }
}
