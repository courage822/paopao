package com.popo.video.data.model;

/**
 * 普通用户账户余额对象
 * Modified by zhangdroid on 2017/6/24.
 */
public class UserBean {
    private long guid;
    private long userId;// 用户id
    private int counts;// 金币数
    private int lastAddAmount;// 最后一次充值数
    private String lastOrderId;// 最后一次订单id
    private long addTime;
    private long updateTime;

    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    public int getLastAddAmount() {
        return lastAddAmount;
    }

    public void setLastAddAmount(int lastAddAmount) {
        this.lastAddAmount = lastAddAmount;
    }

    public String getLastOrderId() {
        return lastOrderId;
    }

    public void setLastOrderId(String lastOrderId) {
        this.lastOrderId = lastOrderId;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "guid=" + guid +
                ", userId=" + userId +
                ", counts=" + counts +
                ", lastAddAmount=" + lastAddAmount +
                ", lastOrderId='" + lastOrderId + '\'' +
                ", addTime=" + addTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
