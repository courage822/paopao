package com.popo.video.data.model;

import java.util.Map;

/**
 * Created by WangYong on 2017/11/8.
 */

public class ResultMessage {

    private String msgId;//		  String  唯一uuid
    private String sendUserName;//	 	  String  发送人昵称
    private String sendUserIcon;//		  String  发送人头像url
    private String sendUserId;//		  Long    发送人id
    private String sendUserFrom;//	   	  Integer 发送人渠道号
    private String sendTime;//	  String  发送时间（北京时间）
    private String sendUserAccount;//		  String  发送人帐号

    private String recvUserAccount;//	  收件人帐号
    private String recvUserName;//	  	   收件人昵称
    private String recvUserIcon;//	  	   收件人头像
    private String recvUserId;//		  Long    收件人ID
    private String recvUserFrom;//		  Integer 收件人渠道号

    private String baseType;//		  Short 	  消息类型，1 文字 2语音 3图片 4 视频
    private String extendType;//		  Short    消息业务扩展类型：1 聊天信 2 视频邀请信 3 礼物4 语音邀请信 5 发送礼物信 6 系统消息 7 视频邀请弹窗 8 招呼信
    private String content;//			  String   消息内容
    private String url;//		  String   多媒体消息路径
    private String audioSeconds;//		  int	   语音消息秒数

    private Map<String, String> ext;

    @Override
    public String toString() {
        return "ResultMessage{" +
                "msgId='" + msgId + '\'' +
                ", sendUserName='" + sendUserName + '\'' +
                ", sendUserIcon='" + sendUserIcon + '\'' +
                ", sendUserId='" + sendUserId + '\'' +
                ", sendUserFrom='" + sendUserFrom + '\'' +
                ", sendTime='" + sendTime + '\'' +
                ", sendUserAccount='" + sendUserAccount + '\'' +
                ", recvUserAccount='" + recvUserAccount + '\'' +
                ", recvUserName='" + recvUserName + '\'' +
                ", recvUserIcon='" + recvUserIcon + '\'' +
                ", recvUserId='" + recvUserId + '\'' +
                ", recvUserFrom='" + recvUserFrom + '\'' +
                ", baseType='" + baseType + '\'' +
                ", extendType='" + extendType + '\'' +
                ", content='" + content + '\'' +
                ", url='" + url + '\'' +
                ", audioSeconds='" + audioSeconds + '\'' +
                ", ext=" + ext +
                '}';
    }
}
