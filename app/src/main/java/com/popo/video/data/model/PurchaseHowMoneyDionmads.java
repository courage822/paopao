package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/9/16.
 */

public class PurchaseHowMoneyDionmads extends BaseModel {
    private int payIntercept;
    private int beanStatus;
    private int beanCount;
    private int remoteStatus;
    private String callMsg;

    public String getCallMsg() {
        return callMsg;
    }

    public void setCallMsg(String callMsg) {
        this.callMsg = callMsg;
    }

    public int getPayIntercept() {
        return payIntercept;
    }

    public void setPayIntercept(int payIntercept) {
        this.payIntercept = payIntercept;
    }

    public int getBeanStatus() {
        return beanStatus;
    }

    public void setBeanStatus(int beanStatus) {
        this.beanStatus = beanStatus;
    }

    public int getBeanCount() {
        return beanCount;
    }

    public void setBeanCount(int beanCount) {
        this.beanCount = beanCount;
    }

    public int getRemoteStatus() {
        return remoteStatus;
    }

    public void setRemoteStatus(int remoteStatus) {
        this.remoteStatus = remoteStatus;
    }

    @Override
    public String toString() {
        return "PurchaseHowMoneyDionmads{" +
                "payIntercept=" + payIntercept +
                ", beanStatus=" + beanStatus +
                ", beanCount=" + beanCount +
                ", remoteStatus=" + remoteStatus +
                ", callMsg='" + callMsg + '\'' +
                '}';
    }
}
