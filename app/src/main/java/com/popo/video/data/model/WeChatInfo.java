package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/9/12.
 */

public class WeChatInfo extends BaseModel {
    private String weixinPayOrder;

    public String getWeixinPayOrder() {
        return weixinPayOrder;
    }

    public void setWeixinPayOrder(String weixinPayOrder) {
        this.weixinPayOrder = weixinPayOrder;
    }
}
