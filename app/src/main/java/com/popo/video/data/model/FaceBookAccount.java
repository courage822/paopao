package com.popo.video.data.model;

import java.util.Map;

/**
 * Created by Administrator on 2017/6/21.
 */

public class FaceBookAccount  extends BaseModel {
    public String goldHostFacebook;
    public Map<String,String> relationshipMap;
    public Map<String,String> educationMap;
    public Map<String,String> incomeMap;
    public Map<String,String> occupationMap;

    public String getGoldHostFacebook() {
        return goldHostFacebook;
    }

    public void setGoldHostFacebook(String goldHostFacebook) {
        this.goldHostFacebook = goldHostFacebook;
    }

    public Map<String, String> getRelationshipMap() {
        return relationshipMap;
    }

    public void setRelationshipMap(Map<String, String> relationshipMap) {
        this.relationshipMap = relationshipMap;
    }

    public Map<String, String> getEducationMap() {
        return educationMap;
    }

    public void setEducationMap(Map<String, String> educationMap) {
        this.educationMap = educationMap;
    }

    public Map<String, String> getIncomeMap() {
        return incomeMap;
    }

    public void setIncomeMap(Map<String, String> incomeMap) {
        this.incomeMap = incomeMap;
    }

    public Map<String, String> getOccupationMap() {
        return occupationMap;
    }

    public void setOccupationMap(Map<String, String> occupationMap) {
        this.occupationMap = occupationMap;
    }

    @Override
    public String toString() {
        return "FaceBookAccount{" +
                "relationshipMap=" + relationshipMap +
                ", educationMap=" + educationMap +
                ", incomeMap=" + incomeMap +
                ", occupationMap=" + occupationMap +
                '}';
    }
}
