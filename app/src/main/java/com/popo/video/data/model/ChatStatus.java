package com.popo.video.data.model;


public class ChatStatus extends BaseModel{
    private int keyStatus;//是否已花费钥匙开通 -1 未花费 1 已花费
    private int payIntercept;//拦截信息 0 未拦截(钥匙余额充足) 3 支付钥匙拦截

    public int getKeyStatus() {
        return keyStatus;
    }

    public void setKeyStatus(int keyStatus) {
        this.keyStatus = keyStatus;
    }

    public int getPayIntercept() {
        return payIntercept;
    }

    public void setPayIntercept(int payIntercept) {
        this.payIntercept = payIntercept;
    }

    @Override
    public String toString() {
        return "ChatStatus{" +
                "keyStatus=" + keyStatus +
                ", payIntercept=" + payIntercept +
                '}';
    }
}
