package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/9/28.
 */

public class Switch extends BaseModel {
    private int allpaySwitch;//{支付相关总开关}
    private int alipaySwitch;//{支付宝支付开关}
    private int weixinpaySwitch;//{微信支付开关}
    private int beanpaySwitch;//{钻石开关}
    private int keypaySwitch;//{钥匙开关}
    private int vippaySwitch;//{vip开关}
    private int walletSwitch;//{我的钱包开关}

    public int getAllpaySwitch() {
        return allpaySwitch;
    }

    public void setAllpaySwitch(int allpaySwitch) {
        this.allpaySwitch = allpaySwitch;
    }

    public int getAlipaySwitch() {
        return alipaySwitch;
    }

    public void setAlipaySwitch(int alipaySwitch) {
        this.alipaySwitch = alipaySwitch;
    }

    public int getWeixinpaySwitch() {
        return weixinpaySwitch;
    }

    public void setWeixinpaySwitch(int weixinpaySwitch) {
        this.weixinpaySwitch = weixinpaySwitch;
    }

    public int getBeanpaySwitch() {
        return beanpaySwitch;
    }

    public void setBeanpaySwitch(int beanpaySwitch) {
        this.beanpaySwitch = beanpaySwitch;
    }

    public int getKeypaySwitch() {
        return keypaySwitch;
    }

    public void setKeypaySwitch(int keypaySwitch) {
        this.keypaySwitch = keypaySwitch;
    }

    public int getVippaySwitch() {
        return vippaySwitch;
    }

    public void setVippaySwitch(int vippaySwitch) {
        this.vippaySwitch = vippaySwitch;
    }

    public int getWalletSwitch() {
        return walletSwitch;
    }

    public void setWalletSwitch(int walletSwitch) {
        this.walletSwitch = walletSwitch;
    }

    @Override
    public String toString() {
        return "Switch{" +
                "allpaySwitch=" + allpaySwitch +
                ", alipaySwitch=" + alipaySwitch +
                ", weixinpaySwitch=" + weixinpaySwitch +
                ", beanpaySwitch=" + beanpaySwitch +
                ", keypaySwitch=" + keypaySwitch +
                ", vippaySwitch=" + vippaySwitch +
                ", walletSwitch=" + walletSwitch +
                '}';
    }
}
