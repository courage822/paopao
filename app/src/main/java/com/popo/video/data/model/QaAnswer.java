package com.popo.video.data.model;

/**
 * Created by Administrator on 2017/12/5.
 */

public class QaAnswer {
    private String answerId;
    private String content;

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "QaAnswer{" +
                "answerId='" + answerId + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
