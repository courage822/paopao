package com.popo.video.data.model;

/**
 * 播主信息对象
 * Created by zhangdroid on 2017/6/2.
 */
public class HostInfo {


    private long guid;// id
    private long userId;// 用户id
    private String hostType;// 主播类型
    private float price;// 主播价格（视频每分钟）
    private float totalMsg;// 收到总消息数量
    private float totalMinute;// 总聊天时长
    private float totalIncome;// 总收入
    private float incomeBalance;// 当前金币数（收入余额）
    private String addTime;
    private long monthlyIncome;//月收入
    private float balanceCash;
    private int audioPrice;

    private int totalSendText;//发送文本索要钻石总数
    private int totalSendPhoto;//发送图片索要钻石总数
    private int totalSendVideo;//发送视频索要钻石总数
    private int balance;//主播收入的余额（钻石为单位）
    private int totalSendCallAudio;//群发语音的总人数
    private int totalSendCallVideo;//群发视频的总人数

    public int getTotalSendCallAudio() {
        return totalSendCallAudio;
    }

    public void setTotalSendCallAudio(int totalSendCallAudio) {
        this.totalSendCallAudio = totalSendCallAudio;
    }

    public int getTotalSendCallVideo() {
        return totalSendCallVideo;
    }

    public void setTotalSendCallVideo(int totalSendCallVideo) {
        this.totalSendCallVideo = totalSendCallVideo;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getTotalSendText() {
        return totalSendText;
    }

    public void setTotalSendText(int totalSendText) {
        this.totalSendText = totalSendText;
    }


    public int getTotalSendPhoto() {
        return totalSendPhoto;
    }

    public void setTotalSendPhoto(int totalSendPhoto) {
        this.totalSendPhoto = totalSendPhoto;
    }

    public int getTotalSendVideo() {
        return totalSendVideo;
    }

    public void setTotalSendVideo(int totalSendVideo) {
        this.totalSendVideo = totalSendVideo;
    }

    public float getBalanceCash() {
        return balanceCash;
    }

    public void setBalanceCash(float balanceCash) {
        this.balanceCash = balanceCash;
    }

    public long getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(long monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getHostType() {
        return hostType;
    }

    public void setHostType(String hostType) {
        this.hostType = hostType;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getTotalMsg() {
        return totalMsg;
    }

    public void setTotalMsg(float totalMsg) {
        this.totalMsg = totalMsg;
    }

    public float getTotalMinute() {
        return totalMinute;
    }

    public void setTotalMinute(float totalMinute) {
        this.totalMinute = totalMinute;
    }

    public float getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(float totalIncome) {
        this.totalIncome = totalIncome;
    }

    public float getIncomeBalance() {
        return incomeBalance;
    }

    public void setIncomeBalance(float incomeBalance) {
        this.incomeBalance = incomeBalance;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public int getAudioPrice() {
        return audioPrice;
    }

    public void setAudioPrice(int audioPrice) {
        this.audioPrice = audioPrice;
    }

    @Override
    public String toString() {
        return "HostInfo{" +
                "guid=" + guid +
                ", userId=" + userId +
                ", hostType='" + hostType + '\'' +
                ", price=" + price +
                ", totalMsg=" + totalMsg +
                ", totalMinute=" + totalMinute +
                ", totalIncome=" + totalIncome +
                ", incomeBalance=" + incomeBalance +
                ", addTime='" + addTime + '\'' +
                ", monthlyIncome=" + monthlyIncome +
                ", balanceCash=" + balanceCash +
                ", audioPrice=" + audioPrice +
                ", totalSendText=" + totalSendText +
                ", totalSendPhoto=" + totalSendPhoto +
                ", totalSendVideo=" + totalSendVideo +
                '}';
    }
}
