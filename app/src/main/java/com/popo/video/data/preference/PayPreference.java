package com.popo.video.data.preference;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.popo.video.base.BaseApplication;
import com.popo.video.db.DbModle;
import com.popo.library.util.SharedPreferenceUtil;
import com.popo.video.data.model.DictPayGift;
import com.popo.video.data.model.GiftsDictiorary;
import com.popo.video.data.model.PicInfo;

import java.util.List;

/**
 * 保存搜索条件
 * Created by zhangdroid on 2017/6/7.
 */
public class PayPreference {
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private PayPreference() {
    }

    public static final String NAME_VIP = "pay_vip";
    public static final String NAME_DIAMOND= "pay_diamond";
    public static final String NAME_KEY= "pay_key";
    public static final String NAME_DIONMADS="name_dionmads";
    public static final String NAME_KEYS="name_keys";
    public static final String NAME_GIFTS="name_gift";
    public static final String NAME_GIFTS_ICON="name_gifts_icon";
    /**
     * 礼物的图片
     */
    private static final String GIFTS_ICONS1="gift_icon1";
    private static final String GIFTS_ICONS2="gift_icon2";
    private static final String GIFTS_ICONS3="gift_icon3";
    private static final String GIFTS_ICONS4="gift_icon4";
    private static final String GIFTS_ICONS5="gift_icon5";
    private static final String GIFTS_ICONS6="gift_icon6";
    private static final String GIFTS_ICONS7="gift_icon7";
    private static final String GIFTS_ICONS8="gift_icon8";
    private static final String GIFTS_ICONS9="gift_icon9";
    private static final String GIFTS_ICONS10="gift_icon10";
    private static final String GIFTS_ICONS11="gift_icon11";
    private static final String GIFTS_ICONS12="gift_icon12";
    private static final String GIFTS_ICONS13="gift_icon13";
    private static final String GIFTS_ICONS14="gift_icon14";
    private static final String GIFTS_ICONS15="gift_icon15";
    private static final String GIFTS_ICONS16="gift_icon16";
    private static final String GIFTS_ICONS17="gift_icon17";
    private static final String GIFTS_ICONS18="gift_icon18";
    private static final String GIFTS_ICONS19="gift_icon19";
    /**
     * 礼物字典中的相关数据
     *
     */
    private static final String SEND_GIFTS="send_gifts";
    /**
     * 购买vip的key
     */
    private static final String PURCHASE_VIP = "pay_vip_purchase";
    /**
     * 购买钻石的key
     */
    private static final String PURCHASE_DIAMOND = "pay_diamond_purchase";
    /**
     * 购买钥匙的key
     */
    private static final String PURCHASE_KEY = "pay_diamond_purchase";
    /**
     * 购买钻石的数量
     */
    private static final String DIONMADS_NUM="dionmads_num";
    /**
     * 购买钥匙的数量
     */
    private static final String KEY_NUMS="keys_num";
    /**
     * 保存爱情鸟图片1
     */
    public static void saveLoveBird1(String loveBird){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS1, loveBird);
    }
    /**
     * 获取爱情鸟图片1
     */
    public static String getLoveBird1(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS1, null);
    }
    /**
     * 保存棒棒糖图片2
     */
    public static void saveLollipop2(String lollaipop){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS2, lollaipop);
    }
    /**
     * 获取棒棒糖图片2
     */
    public static String getLollipop2(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS2, null);
    }
    /**
     * 保存比心图片3
     */
    public static void saveBixin3(String biXin){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS3, biXin);
    }
    /**
     * 获取比心图片3
     */
    public static String getBixin3(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS3, null);
    }
    /**
     * 保存蛋糕图片4
     */
    public static void saveCake4(String cake){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS4, cake);
    }
    /**
     * 获取蛋糕图片4
     */
    public static String getCake4(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS4, null);
    }
    /**
     * 保存飞吻图片5
     */
    public static void saveKiss5(String kiss){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS5, kiss);
    }
    /**
     * 获取飞吻图片5
     */
    public static String getKiss5(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS5, null);
    }
    /**
     * 保存高跟鞋图片6
     */
    public static void savePump6(String pump){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS6, pump);
    }
    /**
     * 获取高跟鞋图片6
     */
    public static String getPump6(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS6, null);
    }
    /**
     * 保存猴赛雷图片7
     */
    public static void saveMonkey7(String monkey){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS7, monkey);
    }
    /**
     * 获取猴赛雷图片7
     */
    public static String getMonkey7(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS7, null);
    }
    /**
     * 保存花灯图片8
     */
    public static void saveLamp8(String lamp){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS8, lamp);
    }
    /**
     * 获取花灯图片8
     */
    public static String getLamp8(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS8, null);
    }
    /**
     * 保存黄瓜图片9
     */
    public static void saveCuke9(String cuke){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS9, cuke);
    }
    /**
     * 获取黄瓜图片9
     */
    public static String getCuke9(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS9, null);
    }
    /**
     * 保存香菇图片10
     */
    public static void saveMushroom10(String mushroom){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS10, mushroom);
    }
    /**
     * 获取香菇图片10
     */
    public static String getMushroom10(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS10, null);
    }
    /**
     * 保存玫瑰图片11
     */
    public static void saveRose11(String rose){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS11, rose);
    }
    /**
     * 获取玫瑰图片11
     */
    public static String getRose11(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS11, null);
    }
    /**
     * 保存南瓜图片12
     */
    public static void savePumpkin12(String pumpkin){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS12, pumpkin);
    }
    /**
     * 获取南瓜图片12
     */
    public static String getPumpkin12(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS12, null);
    }
    /**
     * 保存啤酒图片13
     */
    public static void saveBeer13(String beer){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS13, beer);
    }
    /**
     * 获取啤酒图片13
     */
    public static String getBeer13(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS13, null);
    }
    /**
     * 保存千纸鹤图片14
     */
    public static void saveCrane14(String crane){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS14, crane);
    }
    /**
     * 获取千纸鹤图片14
     */
    public static String getCrane14(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS14, null);
    }
    /**
     * 保存丘比特图片15
     */
    public static void saveCupid15(String cupid){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS15, cupid);
    }
    /**
     * 获取丘比特图片15
     */
    public static String getCupid15(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS15, null);
    }
    /**
     * 保存私照图片16
     */
    public static void savePrivatePhoto16(String photo){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS16, photo);
    }
    /**
     * 获取私照图片16
     */
    public static String getPrivatePhoto16(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS16, null);
    }
    /**
     * 保存香水图片17
     */
    public static void savePerfume17(String perfume){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS17, perfume);
    }
    /**
     * 获取香水图片17
     */
    public static String getPerfume17(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS17, null);
    }
    /**
     * 保存玉兔图片18
     */
    public static void saveRabbit18(String rabbit){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS18, rabbit);
    }
    /**
     * 获取玉兔图片18
     */
    public static String getRabbit18(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS18, null);
    }
    /**
     * 保存钻戒图片19
     */
    public static void saveDiamond19(String diamond){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS19, diamond);
    }
    /**
     * 获取钻戒图片19
     */
    public static String getDiamond19(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS_ICON, GIFTS_ICONS19, null);
    }
    //**************************************************************************************************************
    /**
     * 保存礼物字典数据
     */
    public static void saveSendGifts(String giftInfo){
        SharedPreferenceUtil.setStringValue(mContext, NAME_GIFTS, SEND_GIFTS, giftInfo);
        if(!TextUtils.isEmpty(giftInfo)){
            GiftsDictiorary giftsDictiorary = new Gson().fromJson(giftInfo, GiftsDictiorary.class);
            if(giftsDictiorary!=null){
                List<DictPayGift> giftList = giftsDictiorary.getGiftList();
                if(giftList!=null&&giftList.size()>0){
                    saveLoveBird1(giftList.get(0).getGiftUrl());
                    saveLollipop2(giftList.get(1).getGiftUrl());
                    saveBixin3(giftList.get(2).getGiftUrl());
                    saveCake4(giftList.get(3).getGiftUrl());
                    saveKiss5(giftList.get(4).getGiftUrl());
                    savePump6(giftList.get(5).getGiftUrl());
                    saveMonkey7(giftList.get(6).getGiftUrl());
                    saveLamp8(giftList.get(7).getGiftUrl());
                    saveCuke9(giftList.get(8).getGiftUrl());
                    saveMushroom10(giftList.get(9).getGiftUrl());
                    saveRose11(giftList.get(10).getGiftUrl());
                    savePumpkin12(giftList.get(11).getGiftUrl());
                    saveBeer13(giftList.get(12).getGiftUrl());
                    saveCrane14(giftList.get(13).getGiftUrl());
                    saveCupid15(giftList.get(14).getGiftUrl());
                    savePrivatePhoto16(giftList.get(15).getGiftUrl());
                    savePerfume17(giftList.get(16).getGiftUrl());
                    saveRabbit18(giftList.get(17).getGiftUrl());
                    saveDiamond19(giftList.get(18).getGiftUrl());

                    for (int i = 0; i < giftList.size(); i++) {
                        DictPayGift dictPayGift = giftList.get(i);
                        if (dictPayGift!=null) {
                            PicInfo picInfo = new PicInfo(dictPayGift.getGiftId(), dictPayGift.getGiftName(), String.valueOf(dictPayGift.getPrice()), "num",dictPayGift.getGiftUrl());//num为预留字段
                            if(picInfo!=null){
                                DbModle.getInstance().getUserAccountDao().addPicInfo(picInfo);//保存到数据库中
                            }
                        }
                    }
                }
            }
        }



    }
    /**
     * 获取礼物的数据
     */
    public static String getGiftData(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_GIFTS, SEND_GIFTS, null);
    }
    /**
     * 保存vip的支付信息
     * @param payInfo
     */
    public static void saveVipInfo(String payInfo) {
        SharedPreferenceUtil.setStringValue(mContext, NAME_VIP, PURCHASE_VIP, payInfo);
    }

    /**
     * 保存钻石的支付信息
     * @param payInfo
     */
    public static void saveDiamondInfo(String payInfo) {
        SharedPreferenceUtil.setStringValue(mContext, NAME_DIAMOND, PURCHASE_DIAMOND, payInfo);
    }

//**************************************************************************************************************
    /**
     * 获取vip的支付信息
     * @return
     */
    public static String getVipInfo(){
        return   SharedPreferenceUtil.getStringValue(mContext, NAME_VIP, PURCHASE_VIP, null);
    }

    /**
     * 获取钻石的支付信息
     * @return
     */
    public static String getDiamondInfo(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_DIAMOND, PURCHASE_DIAMOND, null);
    }

    /**
     * 获取钥匙的支付信息
     * @return
     */
    public static String getKeyInfo(){
        return  SharedPreferenceUtil.getStringValue(mContext, NAME_KEY, PURCHASE_KEY, null);
    }

//**************************************************************************************************************
    /**
     * 保存钻石的数量
     * @param num
     */
    public static void saveDionmadsNum(int num) {
        SharedPreferenceUtil.setIntValue(mContext, NAME_DIONMADS, DIONMADS_NUM, num);
    }
    /**
     * 获取钻石的数量
     */
    public static int getDionmadsNum(){
        return  SharedPreferenceUtil.getIntValue(mContext, NAME_DIONMADS, DIONMADS_NUM, 0);
    }
}
