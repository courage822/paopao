package com.popo.video.data.model;

import java.util.List;

/**
 * 首页推荐用户列表对象
 * Created by zhangdroid on 2017/6/2.
 */
public class SearchUserList extends BaseModel {
    private int totalCount;// 总数
    private int pageSize;// 该页内容数
    private int pageNum;// 页码
    private List<SearchUser> searchUserList;// 推荐用户列表

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public List<SearchUser> getSearchUserList() {
        return searchUserList;
    }

    public void setSearchUserList(List<SearchUser> searchUserList) {
        this.searchUserList = searchUserList;
    }

    @Override
    public String toString() {
        return "SearchUserList{" +
                "totalCount=" + totalCount +
                ", pageSize=" + pageSize +
                ", pageNum=" + pageNum +
                ", searchUserList=" + searchUserList +
                '}';
    }
}
