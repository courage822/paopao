package com.popo.video.data.model;

import java.util.List;

public class VideoSquareList extends BaseModel {
    private List<VideoSquare> videoSquareList;

    public List<VideoSquare> getVideoSquareList() {
        return videoSquareList;
    }

    public void setVideoSquareList(List<VideoSquare> videoSquareList) {
        this.videoSquareList = videoSquareList;
    }

    @Override
    public String toString() {
        return "VideoSquareList{" +
                "videoSquareList=" + videoSquareList +
                '}';
    }
}
