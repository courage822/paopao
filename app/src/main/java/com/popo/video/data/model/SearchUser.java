package com.popo.video.data.model;

/**
 * 推荐用户信息
 * Created by zhangdroid on 2017/6/3.
 */
public class SearchUser {

    private long uesrId;// 用户id
    private String account;// 用户account
    private String nickName;// 昵称
    private int userType;// 用户类型(0、普通用户 1、主播)
    private int status;//用户状态(1、空闲 2、在聊 3、勿打扰)
    private String iconUrl;// 头像原图
    private String iconUrlMiddle;// 头像中图
    private String iconUrlMininum;// 头像小图
    private String age;
    // 播主相关
    private int hostprice;// 主播价格
    private int hostType;// 主播类型
    private String hostAddTime;// 成为主播时间
    private float hostTotalIncome;// 主播总收入
    private float hostCurrentIncome;// 主播当前收入
    private int isSayHello;
    private int onlineStatus;private int vipDays;
    private String ownWords;//内心独白

    public int getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(int onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public int getIsSayHello() {
        return isSayHello;
    }

    public void setIsSayHello(int isSayHello) {
        this.isSayHello = isSayHello;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public long getUesrId() {
        return uesrId;
    }

    public void setUesrId(long uesrId) {
        this.uesrId = uesrId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getIconUrlMiddle() {
        return iconUrlMiddle;
    }

    public void setIconUrlMiddle(String iconUrlMiddle) {
        this.iconUrlMiddle = iconUrlMiddle;
    }

    public String getIconUrlMininum() {
        return iconUrlMininum;
    }

    public void setIconUrlMininum(String iconUrlMininum) {
        this.iconUrlMininum = iconUrlMininum;
    }

    public int getHostprice() {
        return hostprice;
    }

    public void setHostprice(int hostprice) {
        this.hostprice = hostprice;
    }

    public int getHostType() {
        return hostType;
    }

    public void setHostType(int hostType) {
        this.hostType = hostType;
    }

    public String getHostAddTime() {
        return hostAddTime;
    }

    public void setHostAddTime(String hostAddTime) {
        this.hostAddTime = hostAddTime;
    }

    public float getHostTotalIncome() {
        return hostTotalIncome;
    }

    public void setHostTotalIncome(float hostTotalIncome) {
        this.hostTotalIncome = hostTotalIncome;
    }

    public float getHostCurrentIncome() {
        return hostCurrentIncome;
    }

    public void setHostCurrentIncome(float hostCurrentIncome) {
        this.hostCurrentIncome = hostCurrentIncome;
    }

    public int getVipDays() {
        return vipDays;
    }

    public void setVipDays(int vipDays) {
        this.vipDays = vipDays;
    }

    public String getOwnWords() {
        return ownWords;
    }

    public void setOwnWords(String ownWords) {
        this.ownWords = ownWords;
    }

    @Override
    public String toString() {
        return "SearchUser{" +
                "uesrId=" + uesrId +
                ", account='" + account + '\'' +
                ", nickName='" + nickName + '\'' +
                ", userType=" + userType +
                ", status=" + status +
                ", iconUrl='" + iconUrl + '\'' +
                ", iconUrlMiddle='" + iconUrlMiddle + '\'' +
                ", iconUrlMininum='" + iconUrlMininum + '\'' +
                ", age='" + age + '\'' +
                ", hostprice=" + hostprice +
                ", hostType=" + hostType +
                ", hostAddTime='" + hostAddTime + '\'' +
                ", hostTotalIncome=" + hostTotalIncome +
                ", hostCurrentIncome=" + hostCurrentIncome +
                ", isSayHello=" + isSayHello +
                ", onlineStatus=" + onlineStatus +
                ", vipDays=" + vipDays +
                ", ownWords='" + ownWords + '\'' +
                '}';
    }
}

