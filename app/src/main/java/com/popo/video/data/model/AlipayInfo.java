package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/9/12.
 */

public class AlipayInfo extends BaseModel {
    private String aliPayOrder;

    public String getAliPayOrder() {
        return aliPayOrder;
    }

    public void setAliPayOrder(String aliPayOrder) {
        this.aliPayOrder = aliPayOrder;
    }

    @Override
    public String toString() {
        return "AlipayInfo{" +
                "aliPayOrder='" + aliPayOrder + '\'' +
                '}';
    }
}
