package com.popo.video.data.model;

import java.util.List;

/**
 * Created by Administrator on 2017/12/5.
 */

public class QaMessage {
    private List<QaAnswer> answers;
    private String questionContent;

    public List<QaAnswer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<QaAnswer> answers) {
        this.answers = answers;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    @Override
    public String toString() {
        return "QaMessage{" +
                "answers=" + answers +
                ", questionContent='" + questionContent + '\'' +
                '}';
    }
}
