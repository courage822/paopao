package com.popo.video.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by xuzhaole on 2018/4/3.
 */

public class AddressComponent implements Parcelable {
    private String long_name;
    private String short_name;
    private List<String> types;

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.long_name);
        dest.writeString(this.short_name);
        dest.writeStringList(this.types);
    }

    public AddressComponent() {
    }

    protected AddressComponent(Parcel in) {
        this.long_name = in.readString();
        this.short_name = in.readString();
        this.types = in.createStringArrayList();
    }

    public static final Parcelable.Creator<AddressComponent> CREATOR = new Parcelable.Creator<AddressComponent>() {
        @Override
        public AddressComponent createFromParcel(Parcel source) {
            return new AddressComponent(source);
        }

        @Override
        public AddressComponent[] newArray(int size) {
            return new AddressComponent[size];
        }
    };

    @Override
    public String toString() {
        return "AddressComponent{" +
                "long_name='" + long_name + '\'' +
                ", short_name='" + short_name + '\'' +
                ", types=" + types +
                '}';
    }
}
