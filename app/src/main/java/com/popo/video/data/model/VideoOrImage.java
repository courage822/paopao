package com.popo.video.data.model;

/**
 * Created by xuzhaole on 2017/11/23.
 */

public class VideoOrImage extends BaseModel {
    private String bitmapUrl;
    private boolean isVideo;
    private String videoUrl;

    public VideoOrImage(String url, boolean isVideo, String videoUrl) {
        this.bitmapUrl = url;
        this.isVideo = isVideo;
        this.videoUrl = videoUrl;
    }

    public String getBitmapUrl() {
        return bitmapUrl;
    }

    public void setBitmapUrl(String bitmapUrl) {
        this.bitmapUrl = bitmapUrl;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public void setVideo(boolean video) {
        isVideo = video;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }


}
