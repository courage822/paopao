package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/11/25.
 */

public class SendVideoOrVoice extends BaseModel {
    private int count;
    private long countDown;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getCountDown() {
        return countDown;
    }

    public void setCountDown(long countDown) {
        this.countDown = countDown;
    }

    @Override
    public String toString() {
        return "SendVideoOrVoice{" +
                "count='" + count + '\'' +
                ", countDown='" + countDown + '\'' +
                '}';
    }
}
