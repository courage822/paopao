package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/11/10.
 */

public class PicInfo {
    private String picId;
    private String picName;
    private String picPrice;
    private String picNum;//预留字段
    private String picIcon;
    public PicInfo() {
    }

    public PicInfo(String picId, String picName, String picPrice, String picNum,String picIcon) {
        this.picId = picId;
        this.picName = picName;
        this.picPrice = picPrice;
        this.picNum = picNum;
        this.picIcon=picIcon;
    }

    public String getPicIcon() {
        return picIcon;
    }

    public void setPicIcon(String picIcon) {
        this.picIcon = picIcon;
    }

    public String getPicId() {
        return picId;
    }

    public void setPicId(String picId) {
        this.picId = picId;
    }

    public String getPicName() {
        return picName;
    }

    public void setPicName(String picName) {
        this.picName = picName;
    }

    public String getPicPrice() {
        return picPrice;
    }

    public void setPicPrice(String picPrice) {
        this.picPrice = picPrice;
    }

    public String getPicNum() {
        return picNum;
    }

    public void setPicNum(String picNum) {
        this.picNum = picNum;
    }


    @Override
    public String toString() {
        return "PicInfo{" +
                "picId='" + picId + '\'' +
                ", picName='" + picName + '\'' +
                ", picPrice='" + picPrice + '\'' +
                ", picNum='" + picNum + '\'' +
                ", picIcon='" + picIcon + '\'' +
                '}';
    }
}
