package com.popo.video.data.api;

/**
 * 接口数据回调
 * Created by zhangdroid on 2017/5/20.
 */
public interface IGetDataListener<T> {

    void onResult(T t, boolean isEmpty);

    void onError(String msg, boolean isNetworkError);

}
