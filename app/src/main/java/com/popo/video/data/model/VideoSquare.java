package com.popo.video.data.model;


public class VideoSquare {
    private UserBase userBase;//基本信息
    private TUserVideoShow tUserVideoShow;//图片信息
    private int isFollow;//是否关注
    private int isBlock;//是否拉黑

    public UserBase getUserBase() {
        return userBase;
    }

    public void setUserBase(UserBase userBase) {
        this.userBase = userBase;
    }

    public TUserVideoShow gettUserVideoShow() {
        return tUserVideoShow;
    }

    public void settUserVideoShow(TUserVideoShow tUserVideoShow) {
        this.tUserVideoShow = tUserVideoShow;
    }

    public int getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(int isFollow) {
        this.isFollow = isFollow;
    }

    public int getIsBlock() {
        return isBlock;
    }

    public void setIsBlock(int isBlock) {
        this.isBlock = isBlock;
    }

    @Override
    public String toString() {
        return "VideoSquare{" +
                "userBase=" + userBase +
                ", tUserVideoShow=" + tUserVideoShow +
                ", isFollow=" + isFollow +
                ", isBlock=" + isBlock +
                '}';
    }
}
