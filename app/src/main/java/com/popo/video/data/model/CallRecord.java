package com.popo.video.data.model;

import java.util.List;

/**
 * 视频通话记录列表对象
 * Created by zhangdroid on 2017/7/6.
 */
public class CallRecord extends BaseModel {
    private List<VideoRecord> listRecord;

    public List<VideoRecord> getListRecord() {
        return listRecord;
    }

    public void setListRecord(List<VideoRecord> listRecord) {
        this.listRecord = listRecord;
    }
}
