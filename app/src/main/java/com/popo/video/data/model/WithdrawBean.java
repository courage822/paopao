package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/10/13.
 */

public class WithdrawBean extends BaseModel {
    private int beanStatus;

    public int getBeanStatus() {
        return beanStatus;
    }

    public void setBeanStatus(int beanStatus) {
        this.beanStatus = beanStatus;
    }

    @Override
    public String toString() {
        return "WithdrawBean{" +
                "beanStatus=" + beanStatus +
                '}';
    }
}
