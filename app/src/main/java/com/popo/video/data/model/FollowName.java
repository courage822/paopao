package com.popo.video.data.model;

/**
 * Created by Administrator on 2017/6/14.
 */

public class FollowName implements Comparable<FollowName>  {
    private String name;//用户姓名
    private String remoteUid;//用户id
    private String pinyin; // 姓名对应的拼音
    private String firstLetter; // 拼音的首字母
    private String icon;//用户头像
    private int age;
    private String state;
    public FollowName() {
    }

    public FollowName(String name, String remoteUid,String icon,int age,String state) {
        this.name = name;
        this.remoteUid=remoteUid;
        this.icon=icon;
        pinyin = name; // 根据姓名获取拼音
        this.age=age;
        this.state=state;
        firstLetter = pinyin.substring(0, 1).toUpperCase(); // 获取拼音首字母并转成大写
        if (!firstLetter.matches("[A-Z]")) { // 如果不在A-Z中则默认为“#”
            firstLetter = "#";
        }
    }

    public String getName() {
        return name;
    }

    public String getPinyin() {
        if (!firstLetter.matches("[A-Z]")) { // 如果不在A-Z中则默认为“#”
            firstLetter = "#";
            return  "#";
        }
        return pinyin;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFirstLetter() {
        return firstLetter;
    }

    public String getRemoteUid() {
        return remoteUid;
    }

    public void setRemoteUid(String remoteUid) {
        this.remoteUid = remoteUid;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public int compareTo(FollowName another) {
        if (firstLetter.equals("#") && !another.getFirstLetter().equals("#")) {
            return 1;
        } else if (!firstLetter.equals("#") && another.getFirstLetter().equals("#")){
            return -1;
        } else {
            return pinyin.compareToIgnoreCase(another.getPinyin());
        }
    }
}
