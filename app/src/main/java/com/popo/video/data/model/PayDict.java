package com.popo.video.data.model;

/**
 * 支付字典对象
 * Created by zhangdroid on 2017/6/10.
 */
public class PayDict {
    private String isvalid;// 是否可用
    private String serviceId;// 支付id
    private String serviceName;// 支付名称
    private String price;// 价格
    private int beanNumber;// 金币数量
    private int dayNumber;// 天数
    private String currency;// 货币
    private String country;// 国家
    private String language;// 语言
    private String productId;// 产品号
    private String fromChannel;// 渠道号
    private long addTime;// 添加时间戳
    private String serviceDesc;
    private String serviceType;

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(String isvalid) {
        this.isvalid = isvalid;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getBeanNumber() {
        return beanNumber;
    }

    public void setBeanNumber(int beanNumber) {
        this.beanNumber = beanNumber;
    }

    public int getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(int dayNumber) {
        this.dayNumber = dayNumber;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getFromChannel() {
        return fromChannel;
    }

    public void setFromChannel(String fromChannel) {
        this.fromChannel = fromChannel;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    @Override
    public String toString() {
        return "PayDict{" +
                "isvalid='" + isvalid + '\'' +
                ", serviceId='" + serviceId + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", price='" + price + '\'' +
                ", beanNumber=" + beanNumber +
                ", dayNumber=" + dayNumber +
                ", currency='" + currency + '\'' +
                ", country='" + country + '\'' +
                ", language='" + language + '\'' +
                ", productId='" + productId + '\'' +
                ", fromChannel='" + fromChannel + '\'' +
                ", addTime=" + addTime +
                '}';
    }
}
