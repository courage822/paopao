package com.popo.video.data.model;

/**
 * Created by xuzhaole on 2017/12/8.
 */

public class BlockBean extends BaseModel {
    private int isBlock;

    public int getIsBlock() {
        return isBlock;
    }

    public void setIsBlock(int isBlock) {
        this.isBlock = isBlock;
    }
}
