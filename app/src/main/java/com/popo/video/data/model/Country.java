package com.popo.video.data.model;

/**
 * Created by Administrator on 2017/7/6.
 */

public class Country {
    private String guid;
    private String name;
    private String language;
    private String url;
    private String addTime;
    private Integer type;
    private Byte used;
    private String countryShort;// 国家缩写
    private String countryEnglish;// 国家英语翻译
    private String countryNum;// 渠道号中国家编码


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Byte getUsed() {
        return used;
    }

    public void setUsed(Byte used) {
        this.used = used;
    }

    public String getCountryShort() {
        return countryShort;
    }

    public void setCountryShort(String countryShort) {
        this.countryShort = countryShort;
    }

    public String getCountryEnglish() {
        return countryEnglish;
    }

    public void setCountryEnglish(String countryEnglish) {
        this.countryEnglish = countryEnglish;
    }

    public String getCountryNum() {
        return countryNum;
    }

    public void setCountryNum(String countryNum) {
        this.countryNum = countryNum;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "Country{" +
                "guid='" + guid + '\'' +
                ", name='" + name + '\'' +
                ", language='" + language + '\'' +
                ", url='" + url + '\'' +
                ", addTime='" + addTime + '\'' +
                ", type=" + type +
                ", used=" + used +
                ", countryShort='" + countryShort + '\'' +
                ", countryEnglish='" + countryEnglish + '\'' +
                ", countryNum='" + countryNum + '\'' +
                '}';
    }
}
