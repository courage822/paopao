package com.popo.video.data.model;

public class PushMessage {
	
	private String content;
	private String title;
	private String subTitle;
	private String msgId;
	private int type;//定义 1 弹窗 type 2 透传
	private int status;
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubTitle() {
		return subTitle;
	}
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "PushMessage{" +
				"content='" + content + '\'' +
				", title='" + title + '\'' +
				", subTitle='" + subTitle + '\'' +
				", msgId='" + msgId + '\'' +
				", type=" + type +
				", status=" + status +
				'}';
	}
}
