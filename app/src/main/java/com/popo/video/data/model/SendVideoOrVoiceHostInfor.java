package com.popo.video.data.model;

/**
 * Created by WangYong on 2017/11/25.
 */

public class SendVideoOrVoiceHostInfor extends BaseModel {
    private long countDown;
    private HostInfo hostInfo;

    public long getCountDown() {
        return countDown;
    }

    public void setCountDown(long countDown) {
        this.countDown = countDown;
    }

    public HostInfo getHostInfo() {
        return hostInfo;
    }

    public void setHostInfo(HostInfo hostInfo) {
        this.hostInfo = hostInfo;
    }
}
