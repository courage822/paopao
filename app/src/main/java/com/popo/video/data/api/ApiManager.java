package com.popo.video.data.api;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.popo.video.R;
import com.popo.video.base.BaseApplication;
import com.popo.video.common.TimeUtils;
import com.popo.video.common.Util;
import com.popo.library.net.NetUtil;
import com.popo.okhttp.OkHttpHelper;
import com.popo.okhttp.callback.JsonCallback;
import com.popo.okhttp.callback.StringCallback;
import com.popo.video.data.model.ChatStatus;
import com.popo.video.data.model.CheckStatus;
import com.popo.video.data.model.GoogleApiModel;
import com.popo.video.data.model.MatchInfo;
import com.popo.video.data.model.TUserVideoShow;
import com.popo.video.data.model.VideoSquareList;
import com.popo.video.ui.pay.util.EncryptUtil;
import com.popo.video.ui.pay.util.Purchase;
import com.popo.video.data.model.AgoraChannelKey;
import com.popo.video.data.model.AgoraToken;
import com.popo.video.data.model.AlipayInfo;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.BlockBean;
import com.popo.video.data.model.CallRecord;
import com.popo.video.data.model.CharmandAndDranking;
import com.popo.video.data.model.Countries;
import com.popo.video.data.model.FindPassword;
import com.popo.video.data.model.FollowUser;
import com.popo.video.data.model.GiftsDictiorary;
import com.popo.video.data.model.HostAutoReplyBean;
import com.popo.video.data.model.IncomeRecordList;
import com.popo.video.data.model.IsFollow;
import com.popo.video.data.model.Login;
import com.popo.video.data.model.MyInfo;
import com.popo.video.data.model.PayWay;
import com.popo.video.data.model.PurchaseHowMoneyDionmads;
import com.popo.video.data.model.PurchaseHowMoneyKeys;
import com.popo.video.data.model.Register;
import com.popo.video.data.model.ResultMessageType;
import com.popo.video.data.model.SearchCriteria;
import com.popo.video.data.model.SearchUserList;
import com.popo.video.data.model.SendGiftsType;
import com.popo.video.data.model.SendVideoOrVoice;
import com.popo.video.data.model.SendVideoOrVoiceHostInfor;
import com.popo.video.data.model.Switch;
import com.popo.video.data.model.UpLoadMyInfo;
import com.popo.video.data.model.UpLoadMyPhoto;
import com.popo.video.data.model.UploadInfoParams;
import com.popo.video.data.model.UserDetailforOther;
import com.popo.video.data.model.VideoHeartBeat;
import com.popo.video.data.model.VideoStop;
import com.popo.video.data.model.WeChatInfo;
import com.popo.video.data.model.WithdrawBean;
import com.popo.video.data.model.WithdrawRecordList;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.UserPreference;

import java.io.File;

import okhttp3.Call;

/**
 * 该类中实现项目中用到的所有接口
 * Created by zhangdroid on 2017/5/20.
 */
public class ApiManager {
    /**
     * 请求成功
     */
    private static final String CODE_SUCCESS = "1";
    /**
     * 请求结果为空
     */
    private static final String CODE_EMPTY = "-8";
    private static Context mContext;
    String signTime = null;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private ApiManager() {
    }

    public void getParamsString(String param) {
//       signTime = String.valueOf(new Date().getTime());
//       OkHttpHelper.getInstance().addCommonHeader("signTime", signTime);
//       OkHttpHelper.getInstance().addCommonHeader("sign ", Util.getMD5(param,signTime));
    }

    /**
     * 检查公用参数是否为空
     */
    private static void checkCommonParams() {
//        Map<String, String> map = OkHttpHelper.getInstance().getCommonParams();
//        if (null == map || map.isEmpty() || TextUtils.isEmpty(map.get("platformInfo"))) {
//            // 设置API公用参数
//            OkHttpHelper.getInstance().addCommonParam("platformInfo", PlatformPreference.getPlatformJsonString());
//        }
    }

    // ***************************** 激活/注册/登陆 ***************************

    public static void activation(final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_ACTIVATION)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(
                            new JsonCallback<BaseModel>() {

                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(BaseModel response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed()) || "2".equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    public static void register(String nickname, boolean isMale, String age, final IGetDataListener<Register> listener) {

        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_REGISTER)
                    .addHeader("sign",
                            Util.getAes(age + (isMale ? "0" : CODE_SUCCESS) + nickname +
                                    PlatformPreference.getPlatformJsonString()))

                    .addParam("age", age)
                    .addParam("gender", isMale ? "0" : CODE_SUCCESS)
                    .addParam("nickName", nickname)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                 }

                                 @Override
                                 public void onSuccess(String reg, int id) {
                                     if (!TextUtils.isEmpty(reg)) {
                                         Register response = new Gson().fromJson(reg, Register.class);
                                         if (response != null) {
                                             if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                                 listener.onResult(response, false);
                                             } else {
                                                 listener.onError(mContext.getString(R.string.register_error), false);
                                             }
                                         }

                                     }
                                 }
                             }


                    );
        } else {
            listener.onError(null, true);
        }
    }

    public static void login(String account, String password, final IGetDataListener<Login> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_LOGIN)
                    .addHeader("sign", Util.getAes(account + password + PlatformPreference.getPlatformJsonString()))

                    .addParam("account", account)
                    .addParam("password", password)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(mContext.getString(R.string.login_error), false);
                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     if (!TextUtils.isEmpty(response)) {
                                         Login login = new Gson().fromJson(response, Login.class);
                                         if (CODE_SUCCESS.equals(login.getIsSucceed())) {
                                             listener.onResult(login, false);
                                         } else {
                                             listener.onError(mContext.getString(R.string.login_error), false);
                                         }
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    public static void findPassword(final IGetDataListener<FindPassword> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_FIND_PWD)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<FindPassword>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.find_pwd_error), false);
                        }

                        @Override
                        public void onSuccess(FindPassword response, int id) {
                            listener.onResult(response, false);
                        }
                    });
        } else {
            listener.onError(mContext.getString(R.string.find_pwd_error), true);
        }
    }

    /**
     * 退出登录，更新用户的状态
     */
    public static void switchUser(final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_USER_SWITCH)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<BaseModel>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.find_pwd_error), false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            listener.onResult(response, false);
                        }
                    });
        } else {
            listener.onError(mContext.getString(R.string.find_pwd_error), true);
        }
    }
    // ***************************** 首页 ***************************

    /**
     * 获取首页推荐用户
     *
     * @param url      推荐用户类型url
     * @param pageNum  页码
     * @param pageSize 每页用户数
     * @param criteria 搜索条件{@link SearchCriteria}
     * @param listener
     */
    public static void getHomepageRecommend(String url, int pageNum, String pageSize, String criteria, final IGetDataListener<SearchUserList> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(url)
                    .addHeader("sign", Util.getAes((TextUtils.isEmpty(criteria) ? "" : criteria)
                            + String.valueOf(pageNum) + pageSize
                            + PlatformPreference.getPlatformJsonString()))
                    .addParam("criteria", TextUtils.isEmpty(criteria) ? "" : criteria)
                    .addParam("pageNum", String.valueOf(pageNum))
                    .addParam("pageSize", pageSize)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(
                            new JsonCallback<SearchUserList>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(SearchUserList response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                        listener.onResult(response, true);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 获取魅力榜
     */
    public static void getCharmandUser(String path, final IGetDataListener<CharmandAndDranking> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(path)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(
                            new JsonCallback<CharmandAndDranking>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(CharmandAndDranking response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                        listener.onResult(response, true);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }


                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 获取礼物字典
     */
    public static void getGiftDic(final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GIFTS_DIC)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     if (!TextUtils.isEmpty(response)) {
                                         GiftsDictiorary giftsDictiorary = new Gson().fromJson(response, GiftsDictiorary.class);
                                         if (giftsDictiorary != null) {
                                             if (CODE_SUCCESS.equals(giftsDictiorary.getIsSucceed())) {
                                                 listener.onResult(response, false);
                                             } else if (CODE_EMPTY.equals(giftsDictiorary.getIsSucceed())) {
                                                 listener.onResult(response, true);
                                             } else {
                                                 listener.onError(null, false);
                                             }
                                         }
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 发送礼物
     */
    public static void sendGiftMsg(String remoteUid, String giftId, String count, final IGetDataListener<SendGiftsType> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SEND_GIFTS)
                    .addHeader("sign", Util.getAes(count + giftId + PlatformPreference.getPlatformJsonString() + remoteUid))
                    .addParam("count", count)
                    .addParam("giftId", giftId)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", remoteUid)
                    .build()
                    .execute(new JsonCallback<SendGiftsType>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(SendGiftsType response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                         listener.onResult(response, true);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }

                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 解锁礼物
     */
    public static void unlockGiftMsg(String msgId, String giftId, String remoteUid, final IGetDataListener<ResultMessageType> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UNLOCK_GIFTS)
                    .addHeader("sign", Util.getAes("2" + giftId + msgId + PlatformPreference.getPlatformJsonString() + remoteUid))
                    .addParam("msgId", msgId)
                    .addParam("fromType", "2")
                    .addParam("giftId", giftId)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", remoteUid)
                    .build()
                    .execute(
                            new JsonCallback<ResultMessageType>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(ResultMessageType response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                        listener.onResult(response, true);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }
    // ***************************** 消息 ***************************

    /**
     * 获取视频通话记录
     *
     * @param pageNum  页码
     * @param pageSize 每页用户数
     * @param listener
     */
    public static void getVideoCallRecord(int pageNum, String pageSize, final IGetDataListener<CallRecord> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MESSAGE_VIDEO_RECORD)
                    .addHeader("sign", Util.getAes(String.valueOf(pageNum) + pageSize + PlatformPreference.getPlatformJsonString()))
                    .addParam("pageNum", String.valueOf(pageNum))
                    .addParam("pageSize", pageSize)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<CallRecord>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(CallRecord response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                listener.onResult(response, true);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 聊天支付拦截（文字）
     *
     * @param uId
     * @param listener
     */
    public static void interruptText(long uId, String content, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MESSAGE_INTERRUPT_TEXT)
                    .addHeader("sign", Util.getAes(content + "2" + PlatformPreference.getPlatformJsonString() + String.valueOf(uId)))
                    .addParam("content", content)
                    .addParam("fromType", "2")
                    .addParam("remoteUid", String.valueOf(uId))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())
                                             || response.getIsSucceed().equals("-32")
                                             || response.getIsSucceed().equals("-31")) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 聊天支付拦截（语音）
     *
     * @param uId
     * @param listener
     */
    public static void interruptVoice(long uId, File file, String time, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MESSAGE_INTERRUPT_VOICE)
                    .addHeader("remoteUid", String.valueOf(uId))
                    .addHeader("fromType", "2")
                    .addHeader("audioSeconds", time)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addFile("file", file.getName(), file)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {

                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())
                                             || response.getIsSucceed().equals("-32")
                                             || response.getIsSucceed().equals("-31")) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 聊天支付拦截（图片）
     *
     * @param uId
     * @param listener
     */
    public static void interruptImage(long uId, File file, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MESSAGE_INTERRUPT_IMAGE)
                    .addHeader("remoteUid", String.valueOf(uId))
                    .addHeader("fromType", "2")
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addFile("file", file.getName(), file)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {

                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())
                                             || response.getIsSucceed().equals("-32")
                                             || response.getIsSucceed().equals("-31")) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 钥匙拦截
     *
     * @param uId
     * @param listener
     */
    public static void interruptKeys(long uId, final IGetDataListener<PurchaseHowMoneyKeys> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MESSAGE_INTERRUPT_KEY_STATUS)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + String.valueOf(uId)))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", String.valueOf(uId))
                    .build()
                    .execute(
                            new JsonCallback<PurchaseHowMoneyKeys>() {

                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(PurchaseHowMoneyKeys response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }

                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 主动拨打电话的拦截
     *
     * @param uId
     * @param listener
     */
    public static void interruptCallVideo(String callType, String uId, String fromType, final IGetDataListener<PurchaseHowMoneyDionmads> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MESSAGE_INTERRUPT_CALL_VIDEO)
                    .addHeader("sign", Util.getAes(callType + PlatformPreference.getPlatformJsonString() + uId))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", uId)
                    .addParam("fromType", fromType)
                    .addParam("callType", callType)
                    .build()
                    .execute(new JsonCallback<PurchaseHowMoneyDionmads>() {

                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(PurchaseHowMoneyDionmads response, int id) {
                                     listener.onResult(response, false);
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {

                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 视频邀请信的回拨拨打电话的拦截
     *
     * @param uId
     * @param listener
     */
    public static void acceptCallVideo(String callType, String uId, String fromType, final IGetDataListener<PurchaseHowMoneyDionmads> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MESSAGE_INTERRUPT_CALL_VIDEO_MSG)
                    .addHeader("sign", Util.getAes(callType + PlatformPreference.getPlatformJsonString() + String.valueOf(uId)))
                    .addParam("remoteUid", String.valueOf(uId))
                    .addParam("callType", callType)
                    .addParam("fromType", fromType)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<PurchaseHowMoneyDionmads>() {

                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(PurchaseHowMoneyDionmads response, int id) {
                                     listener.onResult(response, false);
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {

                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    // ***************************** 关注 ***************************

    public static void getFollowList(String pageNum, String pageSize, final IGetDataListener<FollowUser> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .addHeader("sign", Util.getAes(pageNum + pageSize + PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("pageNum", pageNum)
                    .addParam("pageSize", pageSize)
                    .url(ApiConstant.URL_FOLLOWPAGE_LIST)
                    .build()
                    .execute(new JsonCallback<FollowUser>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(FollowUser response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                         listener.onResult(response, true);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
//                            new JsonCallback<FollowUser>() {
//                                @Override
//                                public void onError(Call call, Exception e, int id) {
//                                    listener.onError(null, false);
//                                }
//
//                                @Override
//                                public void onSuccess(FollowUser response, int id) {
//                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
//                                        listener.onResult(response, false);
//                                    } else if ( CODE_EMPTY.equals(response.getIsSucceed())) {
//                                        listener.onResult(response, false);
//                                    } else {
//                                        listener.onError(null, false);
//                                    }
//                                }
//                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }
    // ***************************** 个人中心 ***************************

    public static void getMyInfo(final IGetDataListener<MyInfo> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_PERSON_MY_INFO)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<MyInfo>() {

                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(mContext.getString(R.string.my_info_error), false);
                                 }

                                 @Override
                                 public void onSuccess(MyInfo response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(mContext.getString(R.string.my_info_error), false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    public static void upLoadMyInfo(UploadInfoParams uploadInfoParams, final IGetDataListener<UpLoadMyInfo> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPLOAD_MYINFO)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + Util.getUploadInfoParamsJsonString(uploadInfoParams)))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("uploadInfoParams", Util.getUploadInfoParamsJsonString(uploadInfoParams))
                    .build()
                    .execute(new JsonCallback<UpLoadMyInfo>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(mContext.getString(R.string.upload_fail), false);
                                 }

                                 @Override
                                 public void onSuccess(UpLoadMyInfo response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(mContext.getString(R.string.upload_fail), false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(mContext.getString(R.string.upload_fail), false);
        }
    }

    /**
     * 上传图片或者头像
     *
     * @param file      图片的路径
     * @param photoType true为上传头像，false为上传普通图片
     * @param listener
     */
    public static void upLoadMyPhotoOrAvator(File file, boolean photoType, final IGetDataListener<UpLoadMyPhoto> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPLOAD_PHOTO)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addHeader("photoType", photoType == true ? "2" : "1")// 2为头像，1为普通图片
                    .addFile("file", file.getName(), file)
                    .build()
                    .execute(
                            new JsonCallback<UpLoadMyPhoto>() {

                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(mContext.getString(R.string.upload_fail), false);
                                }

                                public void onSuccess(UpLoadMyPhoto response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(mContext.getString(R.string.upload_fail), false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(mContext.getString(R.string.upload_fail), false);
        }
    }

    /**
     * 开关接口
     *
     * @param listener
     */
    public static void kaiguan(final IGetDataListener<Switch> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_KAIGUAN)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<Switch>() {

                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(mContext.getString(R.string.upload_fail), false);
                                 }

                                 public void onSuccess(Switch response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(mContext.getString(R.string.upload_fail), false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(mContext.getString(R.string.upload_fail), false);
        }
    }

    /**
     * 举报
     *
     * @param listener
     */
    public static void report(String userId, final String resonCode, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_REPORT)
                    .addHeader("sign", Util.getAes("0" + PlatformPreference.getPlatformJsonString() + resonCode + userId))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("content", "0")
                    .addParam("reasonCode", resonCode)
                    .addParam("remoteUid", userId)
                    .build()
                    .execute(
                            new JsonCallback<BaseModel>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(mContext.getString(R.string.upload_fail), false);
                                }

                                public void onSuccess(BaseModel response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(mContext.getString(R.string.upload_fail), false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(mContext.getString(R.string.upload_fail), false);
        }
    }

    /**
     * 拉黑
     *
     * @param listener
     */
    public static void lahei(String userId, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_LAHEI)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + userId))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", userId)
                    .build()
                    .execute(
                            new JsonCallback<BaseModel>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(mContext.getString(R.string.upload_fail), false);
                                }

                                public void onSuccess(BaseModel response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(mContext.getString(R.string.upload_fail), false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(mContext.getString(R.string.upload_fail), false);
        }
    }

    /**
     * 取消拉黑
     *
     * @param listener
     */
    public static void cancellahei(String userId, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_CANCEL_BLOCK)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + userId))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", userId)
                    .build()
                    .execute(
                            new JsonCallback<BaseModel>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(mContext.getString(R.string.upload_fail), false);
                                }

                                public void onSuccess(BaseModel response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(mContext.getString(R.string.upload_fail), false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(mContext.getString(R.string.upload_fail), false);
        }
    }

    /**
     * 取消拉黑
     *
     * @param listener
     */
    public static void getIsBlock(String userId, final IGetDataListener<BlockBean> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_IS_BLOCK)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + userId))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", userId)
                    .build()
                    .execute(
                            new JsonCallback<BlockBean>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(mContext.getString(R.string.upload_fail), false);
                                }

                                public void onSuccess(BlockBean response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(mContext.getString(R.string.upload_fail), false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(mContext.getString(R.string.upload_fail), false);
        }
    }

    /**
     * 获取批量打招呼的数据
     *
     * @param listener
     */
    public static void sayMoneyHello(final IGetDataListener<SearchUserList> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MONEY_SAY_HELLO)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<SearchUserList>() {

                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(mContext.getString(R.string.upload_fail), false);
                                 }

                                 public void onSuccess(SearchUserList response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(mContext.getString(R.string.upload_fail), false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(mContext.getString(R.string.upload_fail), false);
        }
    }

    /**
     * 修改用户状态
     *
     * @param status   1、空闲 2、在聊 3、勿扰
     * @param listener
     */
    public static void modifyUserStatus(String status, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MODIFFY_USER_STATUS)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + (TextUtils.isEmpty(status) ? "" : status)))
                    .addParam("status", TextUtils.isEmpty(status) ? "" : status)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<BaseModel>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 删除图片
     *
     * @param photoId  1、图片ID
     * @param listener
     */
    public static void deleteImage(String photoId, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_DELETE_IMAGE)
                    .addHeader("sign", Util.getAes(photoId + PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("photoId", photoId)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {

                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    // ***************************** 支付 ***************************

    /**
     * 获取支付渠道信息
     *
     * @param fromTag  支付拦截来源{@link com.popo.video.C.pay}
     * @param listener
     */
    public static void getPayWay(String fromTag, String type, final IGetDataListener<PayWay> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_PAY_WAY)
                    .addHeader("sign", Util.getAes((TextUtils.isEmpty(fromTag) ? "" : fromTag) + PlatformPreference.getPlatformJsonString() + type))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("fromTag", TextUtils.isEmpty(fromTag) ? "" : fromTag)
                    .addParam("serviceType", type)
                    .build()
                    .execute(new JsonCallback<PayWay>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(PayWay response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 获取支付渠道信息
     *
     * @param fromTag  支付拦截来源{@link com.popo.video.C.pay}
     * @param listener
     */
    public static void getPayWayString(String fromTag, String type, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_PAY_WAY)
                    .addHeader("sign", Util.getAes((TextUtils.isEmpty(fromTag) ? "" : fromTag) + PlatformPreference.getPlatformJsonString() + type))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("fromTag", TextUtils.isEmpty(fromTag) ? "" : fromTag)
                    .addParam("serviceType", type)
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {

                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     if (!TextUtils.isEmpty(response)) {
                                         PayWay payWay=new Gson().fromJson(response,PayWay.class);
                                         if (payWay!=null) {
                                             if (CODE_SUCCESS.equals(payWay.getIsSucceed())) {
                                                 listener.onResult(response, false);
                                             } else {
                                                 listener.onError(null, false);
                                             }
                                         }
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 获取支付的钥匙
     *
     * @param fromTag  支付拦截来源{@link com.popo.video.C.pay}
     * @param listener
     */
    public static void getKeyPayWay(String fromTag, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_PAY_WAY)
                    .addHeader("sign", Util.getAes((TextUtils.isEmpty(fromTag) ? "" : fromTag) + PlatformPreference.getPlatformJsonString() + "3"))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("fromTag", TextUtils.isEmpty(fromTag) ? "" : fromTag)
                    .addParam("serviceType", "3")
                    .build()
                    .execute(
                            new StringCallback() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(String response, int id) {
                                    if (response != null) {
                                        PayWay payWay = new Gson().fromJson(response, PayWay.class);
                                        if (payWay != null) {
                                            if (CODE_SUCCESS.equals(payWay.getIsSucceed())) {
                                                listener.onResult(response, false);
                                            } else if (CODE_EMPTY.equals(payWay.getIsSucceed())) {
                                                listener.onResult(response, true);
                                            } else {
                                                listener.onError(null, false);
                                            }
                                        }
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 获取支付的钻石
     *
     * @param fromTag  支付拦截来源{@link com.popo.video.C.pay}
     * @param listener
     */
    public static void getDiamondPayWay(String fromTag, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_PAY_WAY)
                    .addHeader("sign", Util.getAes((TextUtils.isEmpty(fromTag) ? "" : fromTag) + PlatformPreference.getPlatformJsonString() + "1"))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("fromTag", TextUtils.isEmpty(fromTag) ? "" : fromTag)
                    .addParam("serviceType", "1")
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     if (!TextUtils.isEmpty(response)) {
                                         PayWay payWay = new Gson().fromJson(response, PayWay.class);
                                         if (payWay != null) {
                                             if (CODE_SUCCESS.equals(payWay.getIsSucceed())) {
                                                 listener.onResult(response, false);
                                             } else if (CODE_EMPTY.equals(payWay.getIsSucceed())) {
                                                 listener.onResult(response, true);
                                             } else {
                                                 listener.onError(null, false);
                                             }
                                         }
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 获取vip的支付信息
     *
     * @param fromTag  支付拦截来源{@link com.popo.video.C.pay}
     * @param listener
     */
    public static void getVipPayWay(String fromTag, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_PAY_WAY)
                    .addHeader("sign", Util.getAes((TextUtils.isEmpty(fromTag) ? "" : fromTag) + PlatformPreference.getPlatformJsonString() + "2"))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("fromTag", TextUtils.isEmpty(fromTag) ? "" : fromTag)
                    .addParam("serviceType", "2")
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     listener.onResult(response, false);
                                     if (!TextUtils.isEmpty(response)) {
                                         PayWay payWay = new Gson().fromJson(response, PayWay.class);
                                         if (payWay != null) {
                                             if (CODE_SUCCESS.equals(payWay.getIsSucceed())) {
                                                 listener.onResult(response, false);
                                             } else if (CODE_EMPTY.equals(payWay.getIsSucceed())) {
                                                 listener.onResult(response, true);
                                             } else {
                                                 listener.onError(null, false);
                                             }
                                         }
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }


    /**
     * 获取提现记录
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @param listener
     */
    public static void getWithdrawRecord(int pageNum, String pageSize, final IGetDataListener<WithdrawRecordList> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_WITHDRAW_RECORD)
                    .addHeader("sign", Util.getAes(pageNum + pageSize + PlatformPreference.getPlatformJsonString()))
                    .addParam("pageNum", String.valueOf(pageNum))
                    .addParam("pageSize", pageSize)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(
                            new JsonCallback<WithdrawRecordList>() {

                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(WithdrawRecordList response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        response.getListRecord();

                                        listener.onResult(response, false);
                                    } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                        listener.onResult(response, true);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }

                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 获取收入记录
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @param listener
     */
    public static void getIncomeRecord(int pageNum, String pageSize, final IGetDataListener<IncomeRecordList> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_INCOME)
                    .addHeader("sign", Util.getAes(pageNum + pageSize + PlatformPreference.getPlatformJsonString()))
                    .addParam("pageNum", String.valueOf(pageNum))
                    .addParam("pageSize", pageSize)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(
                            new JsonCallback<IncomeRecordList>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(IncomeRecordList response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {

                                        listener.onResult(response, false);
                                    } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                        listener.onResult(response, true);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
//                            new StringCallback() {
//                                @Override
//                                public void onError(Call call, Exception e, int id) {
//
//                                }
//
//                                @Override
//                                public void onSuccess(String response, int id) {
//                                    Log.e("AAAAAAA", "onSuccess: "+response);
//                                }
//                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    // ***************************** 对方空间页 ***************************

    public static void getUserInfo(String user_uid, final IGetDataListener<UserDetailforOther> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_USER_INFO)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + (TextUtils.isEmpty(user_uid) ? "" : user_uid)))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", TextUtils.isEmpty(user_uid) ? "" : user_uid)
                    .build().execute(new JsonCallback<UserDetailforOther>() {
                                         @Override
                                         public void onError(Call call, Exception e, int id) {
                                             listener.onError(mContext.getString(R.string.load_fail), false);
                                         }

                                         @Override
                                         public void onSuccess(UserDetailforOther response, int id) {
                                             listener.onResult(response, false);
                                         }
                                     }

            );
        }
    }

    /**
     * 查找用户
     *
     * @param user_uid
     * @param listener
     */
    public static void findUserInfo(String user_uid, final IGetDataListener<UserDetailforOther> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_FIND_USER)
                    .addHeader("sign", Util.getAes((TextUtils.isEmpty(user_uid) ? "" : user_uid) + PlatformPreference.getPlatformJsonString()))
                    .addParam("account", TextUtils.isEmpty(user_uid) ? "" : user_uid)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build().execute(
                    new JsonCallback<UserDetailforOther>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.load_fail), false);
                        }

                        @Override
                        public void onSuccess(UserDetailforOther response, int id) {
                            listener.onResult(response, false);
                        }
                    }
            );
        }
    }

    /**
     * 提建议
     *
     * @param listener
     */
    public static void adviceForUs(String content, String contact, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_FIND_USER)
                    .addHeader("sign", Util.getAes(contact + content + PlatformPreference.getPlatformJsonString()))
                    .addParam("content", content)
                    .addParam("contact", contact)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build().execute(
                    new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.load_fail), false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            listener.onResult(response, false);
                        }
                    }
            );
        }
    }

    //关注
    public static void follow(String user_uid, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_FOLLOW)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + (TextUtils.isEmpty(user_uid) ? "" : user_uid)))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", TextUtils.isEmpty(user_uid) ? "" : user_uid)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.follow_fail), false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(mContext.getString(R.string.follow_fail), false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    //取消关注
    public static void unFollow(String user_uid, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UN_FOLLOW)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + (TextUtils.isEmpty(user_uid) ? "" : user_uid)))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", TextUtils.isEmpty(user_uid) ? "" : user_uid)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.ufollow_fail), false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(mContext.getString(R.string.ufollow_fail), false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 是否关注某个用户
     *
     * @param uId      用户id
     * @param listener
     */
    public static void isFollow(String uId, final IGetDataListener<IsFollow> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_IS_FOLLOW)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + (TextUtils.isEmpty(uId) ? "" : uId)))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", TextUtils.isEmpty(uId) ? "" : uId)
                    .build()
                    .execute(new JsonCallback<IsFollow>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(IsFollow response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    // ***************************** 视频 ***************************

    /**
     * 获取声网信令系统token，用于登陆信令系统
     *
     * @param listener
     */
    public static void getAgoraToken(final IGetDataListener<AgoraToken> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_TOKEN)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<AgoraToken>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(AgoraToken response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 获取声网channel key，用于加入频道
     *
     * @param listener
     */
    public static void getAgoraChannelKey(String channelId, final IGetDataListener<AgoraChannelKey> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_CHANNEL_KEY)
                    .addHeader("sign", Util.getAes((TextUtils.isEmpty(channelId) ? "" : channelId) + PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("channel", TextUtils.isEmpty(channelId) ? "" : channelId)
                    .build()
                    .execute(new JsonCallback<AgoraChannelKey>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(AgoraChannelKey response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 视频通话心跳（视频接通后轮询）
     *
     * @param uId      对方用户id
     * @param listener
     */
    public static void videoHeartBeat(String uId, final IGetDataListener<VideoHeartBeat> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_VIDEO_HEART_BEAT)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + (TextUtils.isEmpty(uId) ? "" : uId)))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", TextUtils.isEmpty(uId) ? "" : uId)
                    .build()
                    .execute(
                            new JsonCallback<VideoHeartBeat>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(VideoHeartBeat response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 挂断视频通话
     *
     * @param uId      对方用户id
     * @param listener
     */
    public static void videoStop(String uId, String myid, final IGetDataListener<VideoStop> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_VIDEO_STOP)
                    .addHeader("sign", Util.getAes((TextUtils.isEmpty(myid) ? "" : myid) + PlatformPreference.getPlatformJsonString() + (TextUtils.isEmpty(uId) ? "" : uId)))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteUid", TextUtils.isEmpty(uId) ? "" : uId)
                    .addParam("localUid", TextUtils.isEmpty(myid) ? "" : myid)
                    .build()
                    .execute(new JsonCallback<VideoStop>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(VideoStop response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 视频邀请
     *
     * @param uId      对方用户id
     * @param listener
     */
    public static void videoInvite(String uId, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_VIDEO_INVITE)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + (TextUtils.isEmpty(uId) ? "" : uId)))
                    .addParam("remoteUid", TextUtils.isEmpty(uId) ? "" : uId)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(
                            new JsonCallback<BaseModel>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(BaseModel response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 打招呼
     *
     * @param uId      对方用户id
     * @param listener
     */
    public static void sayHello(String uId, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SAY_HELLO)
                    .addHeader("sign", Util.getAes((PlatformPreference.getPlatformJsonString() + (TextUtils.isEmpty(uId) ? "" : uId))))
                    .addParam("remoteUid", TextUtils.isEmpty(uId) ? "" : uId)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 群打招呼
     *
     * @param listener
     */
    public static void sayHelloGroup(String listUid, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SAY_HELLO_GROUP)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + listUid))
                    .addParam("remoteUids", listUid)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * ios的推送需要的
     *
     * @param uId      对方用户id
     * @param listener
     */
    public static void iosCallPush(String uId, String status, String callType, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MESSAGE_INTERRUPT_CALL_FOR_ISO)
                    .addHeader("sign", Util.getAes(callType + PlatformPreference.getPlatformJsonString() + (TextUtils.isEmpty(uId) ? "" : uId) + status))
                    .addParam("remoteId", TextUtils.isEmpty(uId) ? "" : uId)
                    .addParam("status", status)
                    .addParam("callType", callType)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * QA消息回复调用的接口
     *
     * @param answerId
     * @param content
     * @param listener
     */
    public static void qaQaswer(String answerId, String content, String remoteUid, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_QA)
                    .addHeader("sign", Util.getAes(answerId + content + PlatformPreference.getPlatformJsonString() + remoteUid))
                    .addParam("answerId", answerId)
                    .addParam("content", content)
                    .addParam("remoteUid", remoteUid)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    // ***************************** 视频上传与主播认证 ***************************

    public static void getFaceBookAccount(final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_FACEBOOK_ACCOUNT)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(
                            new StringCallback() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(String response, int id) {
                                    listener.onResult(response, false);
                                }
                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    public static void submitCheck(String videoNumber, File videoFile, File avatarFile, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SUBMIT)
                    .addHeader("videoSeconds", "10")
                    .addHeader("videoNumber", videoNumber)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addFile("video", videoFile.getName(), videoFile)
                    .addFile("icon", avatarFile.getName(), avatarFile)
                    .build()
                    .execute(
                            new StringCallback() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(String response, int id) {
                                    listener.onResult(response, false);
                                }
                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 调整主播的价格
     **/
    public static void savePrice(String type, String mprice, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPDATA_PRICE)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + mprice + type))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("price", mprice)
                    .addParam("type", type)
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(String response, int id) {
                            listener.onResult(response, false);
                        }
                    });
        } else {
            listener.onError(null, true);
        }

    }

    /**
     * 提现申请
     **/
    public static void withdraw(String money, String account, String name, String payType,
                                final IGetDataListener<WithdrawBean> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_WITHDRAW_APPLY)
                    .addHeader("sign", Util.getAes(
                            account + money + "RMB" + name
                                    + PlatformPreference.getPlatformJsonString() + payType))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("amount", money)
                    .addParam("withdrawType", payType)
                    .addParam("currency", "RMB")
                    .addParam("account", account)
                    .addParam("firstName", name)
                    .build()
                    .execute(new JsonCallback<WithdrawBean>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(WithdrawBean response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 获取主播自动回复
     */
    public static void getHostAutoReply(final IGetDataListener<HostAutoReplyBean> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_HOST_AUTOREPLY)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(
                            new JsonCallback<HostAutoReplyBean>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);

                                }

                                @Override
                                public void onSuccess(HostAutoReplyBean response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
                    );
        }
    }

    /**
     * 群发视频或者语音
     */
    public static void sendVideoOrVoice(String type, final IGetDataListener<SendVideoOrVoice> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MASS_VIDEO_OR_VOICE)
                    .addHeader("sign", Util.getAes(type + PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("callType", type)
                    .build()
                    .execute(
                            new JsonCallback<SendVideoOrVoice>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(SendVideoOrVoice response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
                    );
        }
    }

    /**
     * 获取发送视频或者语音的时间
     */
    public static void getVideoOrVoiceTime(String type, final IGetDataListener<SendVideoOrVoiceHostInfor> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MASS_VIDEO_OR_VOICE_COUNTDOWN)
                    .addHeader("sign", Util.getAes(type + PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("callType", type)
                    .build()
                    .execute(new JsonCallback<SendVideoOrVoiceHostInfor>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(SendVideoOrVoiceHostInfor response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        }
    }

    /**
     * 上传文本类型自动回复/打招呼
     *
     * @param replyType 1/自动回复   2/打招呼
     * @param content
     * @param listener
     */
    public static void uploadTextAutoReply(String replyType, String content, final IGetDataListener<HostAutoReplyBean> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPLOAD_TEXT_AUTOREPLAY)
                    .addHeader("sign", Util.getAes(content + PlatformPreference.getPlatformJsonString() + replyType))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("content", content)
                    .addParam("replyType", replyType)
                    .build()
                    .execute(new JsonCallback<HostAutoReplyBean>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(HostAutoReplyBean response, int id) {
                            listener.onResult(response, false);
                        }
                    });

        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 上传语音类型自动回复/打招呼
     *
     * @param replyType 1/自动回复   2/打招呼
     * @param replyType
     * @param listener
     */
    public static void uploadSoundAutoReply(String replyType, String audioSecond, File file, final IGetDataListener<HostAutoReplyBean> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPLOAD_SOUND_AUTOREPLAY)
//                    .addHeader("sign", Util.getAes(audioSecond + PlatformPreference.getPlatformJsonString() + replyType))
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addHeader("audioSecond", audioSecond)
                    .addHeader("replyType", replyType)
                    .addFile("file", file.getName(), file)
                    .build()
                    .execute(new JsonCallback<HostAutoReplyBean>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(HostAutoReplyBean response, int id) {
                            listener.onResult(response, false);
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 修改自动回复/打招呼状态
     *
     * @param replyId
     * @param useStatus 1、选中状态  0、非选中状态  -1、删除状态，查询时不显示
     * @param listener
     */
    public static void updateAutoReplyStatus(String replyId, String useStatus, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPLOAD_STATUS_AUTOREPLAY)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + replyId + useStatus))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("replyId", replyId)
                    .addParam("useStatus", useStatus)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );

        } else {
            listener.onError(null, true);
        }

    }

    /**
     * 发送文字视频
     *
     * @param giftId       礼物id
     * @param videoSeconds 视频时长
     * @param file         视频文件
     * @param bitmapFile   视频截图
     * @param content      文字内容
     * @param listener
     */
    public static void sendTextVideo(String giftId, String content, String videoSeconds,
                                     File file, File bitmapFile, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SEND_PRIVATE_VIDEO)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addHeader("videoSeconds", videoSeconds)
                    .addHeader("giftId", giftId)
                    .addFile("bitmap", bitmapFile.getName(), bitmapFile)
                    .addHeader("content", content)
                    .addHeader("fromType", "2")
                    .addFile("file", file.getName(), file)
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     listener.onResult(response, false);
                                 }
                             }
                    );

        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 发送语音视频
     *
     * @param giftId       礼物id
     * @param videoSeconds 视频时长
     * @param audioSeconds 语音时长
     * @param file         视频文件
     * @param audioFile    语音文件
     * @param bitmapFile   视频截图
     * @param listener
     */
    public static void sendVoiceVideo(String giftId, String audioSeconds,
                                      File audioFile, String videoSeconds,
                                      File file, File bitmapFile, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SEND_PRIVATE_VIDEO)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addHeader("videoSeconds", videoSeconds)
                    .addHeader("giftId", giftId)
                    .addHeader("audioSeconds", audioSeconds)
                    .addFile("audioFile", audioFile.getName(), audioFile)
                    .addHeader("fromType", "2")
                    .addFile("file", file.getName(), file)
                    .addFile("bitmapFile", bitmapFile.getName(), bitmapFile)
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     Log.e("voicevideoerror", e.getMessage() + id);
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     listener.onResult(response, false);
                                 }
                             }
                    );

        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 发送语音图片
     *
     * @param giftId       礼物id
     * @param audioSeconds 语音时长
     * @param file         图片文件
     * @param audioFile    语音文件
     * @param listener
     */
    public static void sendVoicePicture(String giftId, String audioSeconds,
                                        File audioFile,
                                        File file, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SEND_PRIVATE_VIDEO)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addHeader("giftId", giftId)
                    .addHeader("audioSeconds", audioSeconds)
                    .addFile("audioFile", audioFile.getName(), audioFile)
                    .addHeader("fromType", "2")
                    .addFile("file", file.getName(), file)
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     listener.onResult(response, false);
                                 }
                             }
                    );

        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 发送文字图片
     *
     * @param giftId   礼物id
     * @param file     图片文件
     * @param content  文字内容
     * @param listener
     */
    public static void sendTextPicture(String giftId, String content,
                                       File file, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SEND_PRIVATE_VIDEO)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addHeader("giftId", giftId)
                    .addHeader("content", content)
                    .addHeader("fromType", "2")
                    .addFile("file", file.getName(), file)
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     listener.onResult(response, false);
                                 }
                             }
                    );

        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 发送文字私密消息
     *
     * @param giftId   礼物id
     * @param content  文字内容
     * @param listener
     */
    public static void sendText(String giftId, String content, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SEND_PRIVATE_VIDEO)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addHeader("giftId", giftId)
                    .addHeader("content", content)
                    .addHeader("fromType", "2")
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     listener.onResult(response, false);
                                 }
                             }
                    );

        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 发送语音私密消息
     *
     * @param giftId       礼物id
     * @param audioFile    语音文件
     * @param audioSeconds 语音时长
     * @param listener
     */
    public static void sendVoice(String giftId, String audioSeconds,
                                 File audioFile, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SEND_PRIVATE_VIDEO)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addHeader("giftId", giftId)
                    .addHeader("audioSeconds", audioSeconds)
                    .addHeader("fromType", "2")
                    .addFile("audioFile", audioFile.getName(), audioFile)
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     Log.e("voiceerror", e.getMessage() + id);
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     listener.onResult(response, false);
                                 }
                             }
                    );

        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 获取国家列表
     *
     * @param listener
     */
    public static void getCountries(final IGetDataListener<Countries> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_COUNTRIES)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(
                            new JsonCallback<Countries>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    Log.e("onError",e.getMessage());
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(Countries response, int id) {
                                    Log.e("onSuccess",response.toString());
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
                    );

        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 通过经纬度获取谷歌国家
     * @param listener
     */
    public static void getCountryByLatLng(Double lat, Double lng, final IGetDataListener<GoogleApiModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=false")
                    .build()
                    .execute(
                            new JsonCallback<GoogleApiModel>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(GoogleApiModel response, int id) {
                                    if (response.getStatus().equals("OK")) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
                    );

        } else {
            listener.onError(null, true);
        }
    }

    //支付宝订单
    public static void alipayOrderInfo(String userId, String serviceId,
                                       final IGetDataListener<AlipayInfo> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_ALIPAY_ORDERINFO)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + serviceId + userId))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("userId", userId)
                    .addParam("serviceId", serviceId)
                    .build()
                    .execute(new JsonCallback<AlipayInfo>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(AlipayInfo response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );

        } else {
            listener.onError(null, true);
        }
    }

    //微信订单
    public static void wxOrderInfo(String userId, String serviceId,
                                   final IGetDataListener<WeChatInfo> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_WX_ORDERINFO)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + serviceId + userId))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("userId", userId)
                    .addParam("serviceId", serviceId)
                    .build()
                    .execute(new JsonCallback<WeChatInfo>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(WeChatInfo response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 内购支付成功后向后台发送支付结果
     *
     * @param purchase
     * @param serviceId
     * @param payType
     * @param listener
     */
    public static void payment(Purchase purchase, String serviceId, String payType, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            if (purchase != null) {
//                Log.e("AAAAAAA", "payment: ---platformInfo="+PlatformPreference.getPlatformJsonString()+"----token="+PlatformPreference.getToken()+"----userId="+
//                        UserPreference.getId()+"----clientTime="+ TimeUtils.getCurrentTime()+"----transactionId="+purchase.getOrderId()+"----payTime="+String.valueOf(purchase.getPurchaseTime())
//                        +"----googleId="+purchase.getPackageName()+"----payType="+payType+"----serviceId="+serviceId+"----productId="+ purchase.getSku()
//                        +"----purchaseToken="+ purchase.getToken()+"----transactionToken="+  EncryptUtil.encrypt(PlatformPreference.getPlatformInfo().getPid(), purchase.getOrderId()));
                OkHttpHelper.post()
                        .url(ApiConstant.PAYMENT)
                        .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                        .addParam("userId", UserPreference.getId())
                        .addParam("clientTime", TimeUtils.getCurrentTime())
                        .addParam("userGoogleId", "")
                        .addParam("transactionId", purchase.getOrderId())
                        .addParam("payTime", String.valueOf(purchase.getPurchaseTime()))
                        .addParam("googleId", purchase.getPackageName())
                        .addParam("packageName", purchase.getPackageName())
                        .addParam("payType", payType)
                        .addParam("serviceId", serviceId)
                        .addParam("productId", purchase.getSku())
                        .addParam("purchaseToken", purchase.getToken())
                        .addParam("transactionToken", EncryptUtil.encrypt(PlatformPreference.getPlatformInfo().getPid(), purchase.getOrderId()))
                        .build()
                        .execute(new StringCallback() {

                            @Override
                            public void onError(Call call, Exception e, int id) {
                                listener.onError(null, false);
                            }

                            @Override
                            public void onSuccess(String response, int id) {
                                listener.onResult(response, false);
                            }
                        });
            }
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 获取视频秀列表
     *
     * @param pageNum
     * @param pageSize
     * @param listener
     */
    public static void VideoShowList(String pageNum, String pageSize, final IGetDataListener<VideoSquareList> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_VIDEO_SHOW_LIST)
                    .addHeader("platformInfo", pageNum + pageSize + PlatformPreference.getPlatformJsonString())
                    .addParam("pageNum", pageNum)
                    .addParam("pageSize", pageSize)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<VideoSquareList>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(VideoSquareList response, int id) {
                                     listener.onResult(response, false);
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    //获取主播审核的状态
    public static void getAuthorCheckStatus(final IGetDataListener<CheckStatus> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_USER_CHECK_STATUS)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString()))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<CheckStatus>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(CheckStatus response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 上传视频秀，语音，内心独白
     */
    public static void uploadAllShow(String ownWords, File videoShowFile, File bitmapFile, String videoShowDuration,
                                     File voiceShowFile, String voiceShowDuration, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPLOAD_ALL_SHOW)
                    .addHeader("ownWords", ownWords)
                    .addHeader("videoSeconds", videoShowDuration)
                    .addFile("videoFile", videoShowFile.getName(), videoShowFile)
                    .addFile("bitmapFile", bitmapFile.getName(), bitmapFile)
                    .addParam("audioSeconds", voiceShowDuration)
                    .addFile("audioFile", voiceShowFile.getName(), voiceShowFile)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     listener.onResult(response, false);
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    //应用打点回传
    public static void userActivityTag(String userId, String remoteId, String baseTag, String extendTag, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_USER_ACTIVITY_TAG)
                    .addHeader("sign", Util.getAes(baseTag + extendTag + PlatformPreference.getPlatformJsonString() + remoteId + userId))
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("remoteId", remoteId)
                    .addParam("userId", userId)
                    .addParam("basetag", baseTag)
                    .addParam("extendtag", extendTag)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     Log.e("userAcyTag",e.getMessage());
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                         Log.e("userAcyTag",response.getIsSucceed());
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 替换图片
     */
    public static void updateUserPhotoes(File file, boolean photoType, String photoId, final IGetDataListener<UpLoadMyPhoto> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPLOAD_PHOTO)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addHeader("photoType", photoType == true ? "2" : "1")// 2为头像，1为普通图片
                    .addHeader("photoId", photoId)
                    .addFile("file", file.getName(), file)
                    .build()
                    .execute(
                            new JsonCallback<UpLoadMyPhoto>() {

                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(mContext.getString(R.string.upload_fail), false);
                                }

                                public void onSuccess(UpLoadMyPhoto response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                    } else {
                                        listener.onError(mContext.getString(R.string.upload_fail), false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(mContext.getString(R.string.upload_fail), false);
        }
    }

    //上传视频秀
    public static void uploadVideoShow(String videoId, File iconIcon, File videoFile, String videoSeconds, final IGetDataListener<TUserVideoShow> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPLOAD_VIDEO_SHOW)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addHeader("videoId", videoId)
                    .addHeader("videoSeconds", videoSeconds)
                    .addFile("icon", iconIcon.getName(), iconIcon)
                    .addFile("video", videoFile.getName(), videoFile)
                    .build()
                    .execute(new JsonCallback<TUserVideoShow>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(TUserVideoShow response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    //相册重排序
    public static void recorderImage(String photoId, String targetIndex, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_RECORD_IMAGE)
                    .addHeader("sign", Util.getAes(photoId + PlatformPreference.getPlatformJsonString()) + targetIndex)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("photoId", photoId)
                    .addParam("targetIndex", targetIndex)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     Log.e("recorderImage", "error" + e.getMessage());
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     Log.e("recorderImage", "success" + response.getIsSucceed());
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    //判断是否能够发送一封信息
    public static void getIsCanSendMsg(final String remoteUid, final IGetDataListener<ChatStatus> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MSG_KEY_STATUS)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + remoteUid))
                    .addParam("remoteUid", remoteUid)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<ChatStatus>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(ChatStatus response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }

    //聊天列表信置为已读状态
    public static void msgRead(String remoteUid, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MSG_READ)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + remoteUid))
                    .addParam("remoteUid", remoteUid)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }
    //客户端收到消息后，向服务器回执，确认已送达
    public static void msgReceived(String msgId, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MSG_RECEIVED)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + msgId))
                    .addParam("msgId", msgId)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }
    //匹配好友
    public static void sayHelloMatcher(String remoteId, final IGetDataListener<MatchInfo> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SAY_HELLO_MATCHER)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + remoteId))
                    .addParam("remoteUid", remoteId)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<MatchInfo>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(MatchInfo response, int id) {
                                         listener.onResult(response, false);
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }
    //标记已阅读匹配用户
    public static void readMatch(String remoteId, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_READ_MATCHER)
                    .addHeader("sign", Util.getAes(PlatformPreference.getPlatformJsonString() + remoteId))
                    .addParam("remoteUid", remoteId)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(BaseModel response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError(null, false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }
}
