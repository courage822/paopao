package com.popo.video.data.preference;

import android.content.Context;

import com.popo.video.base.BaseApplication;
import com.popo.library.util.SharedPreferenceUtil;
import com.popo.video.data.model.UserBean;

/**
 * 保存普通用户账户信息
 * Created by zhangdroid on 2017/6/24.
 */
public class BeanPreference {
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private BeanPreference() {
    }

    public static final String NAME = "pre_bean";
    public static final String BEAN_COUNT = "bean_count";

    public static void saveUserBean(UserBean userBean) {
        if (null != userBean) {
            setBeanCount(userBean.getCounts());
        }
    }

    /**
     * 保存用户账户当前金币数
     *
     * @param count
     */
    public static void setBeanCount(int count) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, BEAN_COUNT, count);
    }

    /**
     * @return 用户账户当前金币数
     */
    public static int getBeanCount() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, BEAN_COUNT, 0);
    }

}
