package com.popo.video.data.preference;

import android.content.Context;

import com.popo.video.base.BaseApplication;
import com.popo.video.db.DbModle;
import com.popo.library.util.SharedPreferenceUtil;
import com.popo.video.data.model.UserBase;

/**
 * 保存用户相关信息
 * Created by zhangdroid on 2017/5/31.
 */
public class UserPreference {
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private UserPreference() {
    }

    public static final String NAME = "pre_user";
    public static final String NAME_LIST = "user_list";
    public static final String NAME_CHECK = "name_check";
    /**
     * 是否激活过
     */
    private static final String ACTIVATION = "activation";
    /**
     * 是否显示计费提示
     */
    private static final String IS_SHOW_CHARGE = "is_show_charge";

    /**
     * 是否播主
     */
    private static final String IS_ANCHOR = "is_anchor";
    /**
     * 是否vip
     */
    private static final String IS_VIP = "is_vip";
    /**
     * 是否vip
     */
    private static final String IS_LJ = "is_lj";
    /**
     * 我的激活
     */
    private static final String My_ACTIVATION = "my_activation";
    /**
     * splash激活
     */
    private static final String SPLASH_ACTIVATION = "splash_activation";
    /**
     * 账号
     */
    private static final String ACCOUNT = "account";
    /**
     * 密码
     */
    private static final String PASSWORD = "password";
    /**
     * 用户ID
     */
    private static final String USER_ID = "user_id";
    /**
     * 用户状态(1、空闲 2、在聊 3、勿扰)
     */
    private static final String STATUS = "status";
    /**
     * 头像-原图
     */
    private static final String IMG_ORIGINAL = "img_original";
    /**
     * 头像-中图
     */
    private static final String IMG_MIDDLE = "img_middle";
    /**
     * 头像-小图
     */
    private static final String IMG_SMALL = "img_small";
    /**
     * 昵称
     */
    private static final String NICKNAME = "nickname";
    /**
     * 年龄
     */
    private static final String AGE = "age";
    /**
     * 性别
     */
    private static final String GENDER = "gender";
    /**
     * 自我介绍
     */
    private static final String INTRODUCATION = "introducation";
    /**
     * 国家
     */
    private static final String COUNTRY = "country";
    /**
     * 州
     */
    private static final String STATE = "state";
    /**
     * 城市
     */
    private static final String CITY = "city";
    /**
     * 支持的语言
     */
    private static final String SPOKEN_LANGUAGE = "spoken_language";

    /**
     * 保存国家的ID
     */
    private static final String COUNTRY_ID = "country_id";

    /**
     * 是否正在审核主播
     */
    public static final String CHECK_AUTHOR = "check_author";
    /**
     * 视频聊天页面保存发送礼物的信息
     */
    public static final String SAVE_SEND_GIFT_MSG = "save_send_gift_msg";
    /**
     * 判断是否是第一次使用，如果是就打开注册引导
     */
    public static final String REGISTER_GUIDE="register_guide";
    // ********************************************* 保存标记 *********************************************

    //注册引导成功之后的标记
    public static void guided(){
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, REGISTER_GUIDE, true);
    }
    //标记注册引导只出现一次，下次调用注册接口时出现
    public static void guidedModification(){
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, REGISTER_GUIDE, false);
    }
    //判断注册引导是否已经打开
    public static boolean  isGuided(){
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, REGISTER_GUIDE, false);
    }



    //保存礼物的信息
    public static void saveGiftsContent(String json) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, SAVE_SEND_GIFT_MSG, json);
    }

    //获取保存礼物的信息
    public static String getGiftsContent() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, SAVE_SEND_GIFT_MSG, "0");
    }

    public static void activation() {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, ACTIVATION, true);
    }

    public static boolean isActived() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, ACTIVATION, false);
    }

    //我自定义的激活
    public static void mActivation() {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, My_ACTIVATION, true);
    }

    public static boolean isMactived() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, My_ACTIVATION, false);
    }
    //splash激活
    public static void mSplashActivation() {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, SPLASH_ACTIVATION, true);
    }

    public static boolean isSplashMactived() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, SPLASH_ACTIVATION, false);
    }

    //判断是否正在审核中
    public static void isCheckAuthoring() {
        SharedPreferenceUtil.setIntValue(mContext, NAME_CHECK, CHECK_AUTHOR, 1);
    }

    public static int isAuthoring() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME_CHECK, CHECK_AUTHOR, 0);
    }


    /**
     * 不再显示计费提示
     */
    public static void hideCharge() {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_SHOW_CHARGE, false);
    }

    public static boolean isShowCharge() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, IS_SHOW_CHARGE, true);
    }

    // ********************************************* 保存/获取用户资料项信息 *********************************************

    private static void saveIfNotEmpty(String key, String value) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, key, value);//保存的信息不能做非空判断，因为这样有可能造成数据保存不上
    }

    private static String getValue(String key) {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, key, null);
    }

    public static void saveUserInfo(UserBase userBase) {
        if (null != userBase) {
            setAccount(userBase.getAccount());
            setPassword(userBase.getPassword());
            setIsAnchor(userBase.getUserType());
            setId(String.valueOf(userBase.getGuid()));
            setStatus(userBase.getStatus());
            setOriginalImage(userBase.getIconUrl());
            setMiddleImage(userBase.getIconUrlMiddle());
            setSmallImage(userBase.getIconUrlMininum());
            setNickname(userBase.getNickName());
            setAge(String.valueOf(userBase.getAge()));
            setGender(String.valueOf(userBase.getGender()));
            setIntroducation(userBase.getOwnWords());
            setCountry(userBase.getCountry());
            setCountryId(userBase.getCountry());
            setState(userBase.getState());
            setCity(userBase.getCity());
            setSpokenLanguage(userBase.getSpokenLanguage());
            setUserList(userBase.getAccount(), userBase.getPassword());
        }
    }

    private static void setUserList(String account, String password) {
        DbModle.getInstance().getUserAccountDao().addAccountAndPwd(account, password);
    }


    public static void clearUserInfo() {//清空保存的数据
        setAccount(null);
        setPassword(null);
        setIsAnchor(-1);
        setId(null);
        setStatus(null);
        setOriginalImage(null);
        setMiddleImage(null);
        setSmallImage(null);
        setNickname(null);
        setAge(null);
        setGender(null);
        setIntroducation(null);
        setCountry(null);
        setState(null);
        setCity(null);
        setSpokenLanguage(null);
    }

    public static void setAccount(String value) {
        saveIfNotEmpty(ACCOUNT, value);
    }

    public static String getAccount() {
        return getValue(ACCOUNT);
    }

    public static void setPassword(String value) {
        saveIfNotEmpty(PASSWORD, value);
    }

    public static String getPassword() {
        return getValue(PASSWORD);
    }

    public static void setIsAnchor(int userType) {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_ANCHOR, userType == 1);
    }

    public static boolean isAnchor() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, IS_ANCHOR, false);
    }

    public static void setIsVip(int vipdays) {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_VIP, vipdays > 0);
    }

    public static void setIsVip(boolean isVip) {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_VIP, isVip);
    }

    public static boolean isVip() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, IS_VIP, false);
    }

    public static void setIslj(String islj){
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_LJ, islj.equals("1"));
    }
    public static boolean islj(){
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, IS_LJ, true);
    }


    public static void setId(String value) {
        saveIfNotEmpty(USER_ID, value);
    }

    public static String getId() {
        return getValue(USER_ID);
    }

    public static void setStatus(String value) {
        saveIfNotEmpty(STATUS, value);
    }

    public static String getStatus() {
        return getValue(STATUS);
    }

    public static void setOriginalImage(String value) {
        saveIfNotEmpty(IMG_ORIGINAL, value);
    }

    public static String getOriginalImage() {
        return getValue(IMG_ORIGINAL);
    }

    public static void setMiddleImage(String value) {
        saveIfNotEmpty(IMG_MIDDLE, value);
    }

    public static String getMiddleImage() {
        return getValue(IMG_MIDDLE);
    }

    public static void setSmallImage(String value) {
        saveIfNotEmpty(IMG_SMALL, value);
    }

    public static String getSmallImage() {
        return getValue(IMG_SMALL);
    }

    public static void setNickname(String value) {
        saveIfNotEmpty(NICKNAME, value);
    }

    public static String getNickname() {
        return getValue(NICKNAME);
    }

    public static void setAge(String value) {
        saveIfNotEmpty(AGE, value);
    }

    public static String getAge() {
        return getValue(AGE);
    }

    public static void setGender(String value) {
        saveIfNotEmpty(GENDER, value);
    }

    public static boolean isMale() {
        return "0".equals(SharedPreferenceUtil.getStringValue(mContext, NAME, GENDER, "0"));
    }

    public static void setIntroducation(String value) {
        saveIfNotEmpty(INTRODUCATION, value);
    }

    public static String getIntroducation() {
        return getValue(INTRODUCATION);
    }

    public static void setCountry(String value) {
        saveIfNotEmpty(COUNTRY, value);
    }

    public static String getCountry() {
        return getValue(COUNTRY);
    }

    public static void setState(String value) {
        saveIfNotEmpty(STATE, value);
    }

    public static String getState() {
        return getValue(STATE);
    }

    public static void setCity(String value) {
        saveIfNotEmpty(CITY, value);
    }

    public static String getCity() {
        return getValue(CITY);
    }

    public static void setSpokenLanguage(String value) {
        saveIfNotEmpty(SPOKEN_LANGUAGE, value);

    }

    public static String getSpokenLanguage() {
        return getValue(SPOKEN_LANGUAGE);
    }

    public static void setCountryId(String countryId) {

        SharedPreferenceUtil.setStringValue(mContext, COUNTRY_ID, "contry_key", countryId);
    }

    public static String getCountryId() {
        return SharedPreferenceUtil.getStringValue(mContext, COUNTRY_ID, "contry_key", null);
    }
}
