package com.popo.video.data.model;

import java.util.List;

/**
 * Created by Administrator on 2017/7/14.
 * 提现记录列表
 */

public class IncomeRecordList  extends BaseModel {

    private List<IncomeRecord> incomeFlowList;

    public List<IncomeRecord> getIncomeFlowList() {
        return incomeFlowList;
    }

    public void setIncomeFlowList(List<IncomeRecord> incomeFlowList) {
        this.incomeFlowList = incomeFlowList;
    }

    @Override
    public String toString() {
        return "IncomeRecordList{" +
                "incomeFlowList=" + incomeFlowList +
                '}';
    }
}