package com.popo.video.data.model;

/**
 * 是否关注某个用户
 * Created by zhangdroid on 2017/6/22.
 */
public class IsFollow extends BaseModel {
    private String isFollow;// 是否关注 1：已关注 0 未关注

    public String getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(String isFollow) {
        this.isFollow = isFollow;
    }
}
