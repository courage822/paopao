package com.popo.video.data.model;

/**
 * 用户信息对象
 * Created by zhangdroid on 2017/6/2.
 */
public class MyInfo extends BaseModel {
    private UserDetail userDetail;
    private String islj;//是否看信拦截 1：看信拦截（拦截后判断isvip）  2:写信拦截

    //2018/2/7添加  用于国际版群发视频/语音文字说明
    private String massVideoDesc;//群发视频说明
    private String massAudioDesc;//群发语音说明

    public String getIslj() {
        return islj == null ? "" : islj;
    }

    public void setIslj(String islj) {
        this.islj = islj;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    public String getMassVideoDesc() {
        return massVideoDesc;
    }

    public void setMassVideoDesc(String massVideoDesc) {
        this.massVideoDesc = massVideoDesc;
    }

    public String getMassAudioDesc() {
        return massAudioDesc;
    }

    public void setMassAudioDesc(String massAudioDesc) {
        this.massAudioDesc = massAudioDesc;
    }

    @Override
    public String toString() {
        return "MyInfo{" +
                "userDetail=" + userDetail +
                ", massVideoDesc='" + massVideoDesc + '\'' +
                ", massAudioDesc='" + massAudioDesc + '\'' +
                '}';
    }
}
