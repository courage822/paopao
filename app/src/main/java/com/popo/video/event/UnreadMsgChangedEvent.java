package com.popo.video.event;

/**
 * 未读消息变化事件
 * Created by zhangdroid on 2017/5/24.
 */
public class UnreadMsgChangedEvent {
    public int unreadMsg;

    public UnreadMsgChangedEvent(int unreadMsg) {
        this.unreadMsg = unreadMsg;
    }

}
