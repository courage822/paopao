package com.popo.video.event;

/**
 * 声网媒体系统事件
 * Created by zhangdroid on 2017/6/21.
 */
public class AgoraMediaEvent {
    public int eventCode;// 事件代码
    public int uId;// 声网视频用户id

    public AgoraMediaEvent(int eventCode, int uId) {
        this.eventCode = eventCode;
        this.uId = uId;
    }

}
