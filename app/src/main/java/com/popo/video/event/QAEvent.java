package com.popo.video.event;


import com.popo.video.data.model.QaMessage;
import com.popo.video.data.model.QaMsg;

/**
 * Created by WangYong on 2018/1/17.
 */

public class QAEvent {
    private QaMessage qaMessage;
    private String userAccount;
    private String guid;
    private QaMsg qaMsg;

    public QAEvent(QaMessage qaMessage, String userAccount, String guid, QaMsg qaMsg) {
        this.qaMessage = qaMessage;
        this.userAccount = userAccount;
        this.guid = guid;
        this.qaMsg = qaMsg;
    }

    public QaMessage getQaMessage() {
        return qaMessage;
    }

    public void setQaMessage(QaMessage qaMessage) {
        this.qaMessage = qaMessage;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public QaMsg getQaMsg() {
        return qaMsg;
    }

    public void setQaMsg(QaMsg qaMsg) {
        this.qaMsg = qaMsg;
    }

    @Override
    public String toString() {
        return "QAEvent{" +
                "qaMessage=" + qaMessage +
                ", userAccount='" + userAccount + '\'' +
                ", guid='" + guid + '\'' +
                ", qaMsg=" + qaMsg +
                '}';
    }
}
