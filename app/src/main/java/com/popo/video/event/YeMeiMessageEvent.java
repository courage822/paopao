package com.popo.video.event;

/**
 * Created by WangYong on 2018/4/28.
 */

public class YeMeiMessageEvent {
    private String guid;
    private String nickName;
    private int type;//0为信息，1为匹配
    private String imageUrl;
    private String account;

    public YeMeiMessageEvent(String guid, String nickName, int type, String imageUrl, String account) {
        this.guid = guid;
        this.nickName = nickName;
        this.type = type;
        this.imageUrl = imageUrl;
        this.account = account;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "YeMeiMessageEvent{" +
                "guid='" + guid + '\'' +
                ", nickName='" + nickName + '\'' +
                ", type=" + type +
                ", imageUrl='" + imageUrl + '\'' +
                ", account='" + account + '\'' +
                '}';
    }
}
