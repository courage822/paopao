package com.popo.video.event;

/**
 * 声网信令系统事件
 * Created by zhangdroid on 2017/6/15.
 */
public class AgoraEvent {
    public int eventCode;// 事件代码
    public String channelId;// 频道id
    public String account;// 用户account
    public String message;// 接收到的消息

    public int getEventCode() {
        return eventCode;
    }

    public void setEventCode(int eventCode) {
        this.eventCode = eventCode;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
