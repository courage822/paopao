package com.popo.video.common;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.AgoraParams;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.PurchaseHowMoneyDionmads;
import com.popo.video.data.preference.UserPreference;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.ToastUtil;
import com.popo.video.parcelable.VideoInviteParcelable;
import com.popo.video.parcelable.VideoParcelable;
import com.popo.video.ui.video.VideoActivity;
import com.popo.video.ui.video.VideoInviteActivity;

import static com.popo.video.common.TimeUtils.mContext;

/**
 * 视频邀请与被邀请帮助类
 * Created by zhangdroid on 2017/6/15.
 */
public class VideoHelper {
    private String mInviteType = "1";

    private VideoHelper() {
    }

    /**
     * 发起视频邀请
     *
     * @param videoInviteParcelable 视频邀请需要传递的参数
     */
    public static void startVideoInvite(final VideoInviteParcelable videoInviteParcelable, final Context context,String fromType) {
        checkUserStatus(videoInviteParcelable, context,fromType);
    }
    /**
     * @param channelId
     * @param account
     * @param type      0为视频，1为语音
     */
    public static void immediatelyVideo(final String channelId, final String account, final String guid,final String imageUrl, final int type, final Context context,String fromType) {
        String mInviteType = "1";
        if (type == 1) {
            mInviteType = "2";
        } else {
            mInviteType = "1";
        }
        final String finalMInviteType = mInviteType;
        //这里调用callanswer
        ApiManager.acceptCallVideo(finalMInviteType, guid,fromType,new IGetDataListener<PurchaseHowMoneyDionmads>() {
            @Override
            public void onResult(PurchaseHowMoneyDionmads dionmads, boolean isEmpty) {


                CustomDialogAboutOther.videoCallAlert(context, dionmads,null,null, channelId, account, guid, imageUrl,  type, 2,false);

//                if (dionmads != null) {
//                    if (dionmads.getPayIntercept() == 1) {
//                        CustomDialogAboutPay.purchaseDiamondShow(context);
//                    } else {
//                        if (UserPreference.getStatus().equals("3")) {
//                            Toast.makeText(context, mContext.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
//                        } else {
//                            LaunchHelper.getInstance().launch(context, VideoActivity.class,
//                                    new VideoParcelable(Long.parseLong(guid), account,imageUrl, UserPreference.getNickname(), channelId, "0", type,true));
//                            AgoraHelper.getInstance().startInvite(channelId, account, new Gson().toJson(new AgoraParams(Integer.parseInt(UserPreference.getId()), UserPreference.getNickname(), UserPreference.getSmallImage(), type, 1)));
//                        }
//                    }
//                }






            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
    /**
     * 视频接收
     *
     * @param videoParcelable
     */
    public static void acceptVideoInvite(final VideoParcelable videoParcelable, final String channelId,final Context context,String fromType) {
        String inviteType = "1";
        if (videoParcelable.type == 1) {
            inviteType = "2";
        } else {
            inviteType = "1";
        }
        ApiManager.acceptCallVideo(inviteType, String.valueOf(videoParcelable.guid), fromType,new IGetDataListener<PurchaseHowMoneyDionmads>() {
            @Override
            public void onResult(PurchaseHowMoneyDionmads dionmads, boolean isEmpty) {
                CustomDialogAboutOther.videoCallAlert(  context,  dionmads,null,videoParcelable, channelId, null, null, null,0,1,false);
//                if (dionmads != null) {
//                    if (dionmads.getPayIntercept() == 1) {//钻石拦截
//                        CustomDialogAboutPay.purchaseDiamondShow(context);
//                    } else {
//                        if (dionmads.getBeanStatus() == -1) {
//                            Toast.makeText(context, mContext.getString(R.string.not_enough_diaminds), Toast.LENGTH_SHORT).show();
//                            CustomDialogAboutPay.purchaseDiamondShow(context);
//                        } else if (dionmads.getBeanStatus() == -2) {
//                            Toast.makeText(context, mContext.getString(R.string.less_three_minutes), Toast.LENGTH_SHORT).show();
//                        } else {
//                            LaunchHelper.getInstance().launchFinish(context, VideoActivity.class,
//                                    videoParcelable);
//                            AgoraHelper.getInstance().acceptInvite(channelId, videoParcelable.account);
//                        }
//                    }
//                }
            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
    private static void checkUserStatus(final VideoInviteParcelable videoInviteParcelable, final Context context,String fromType) {
        String inviteType = "1";
        if (videoInviteParcelable.type == 1) {
            inviteType = "2";
        } else {
            inviteType = "1";
        }
        ApiManager.interruptCallVideo(inviteType, String.valueOf(videoInviteParcelable.uId),fromType,new IGetDataListener<PurchaseHowMoneyDionmads>() {
            @Override
            public void onResult(PurchaseHowMoneyDionmads dionmads, boolean isEmpty) {
                /**
                 * 公共的参数有这些
                 * VideoInviteParcelable videoInviteParcelable
                 * VideoParcelable videoParcelable
                 * String channelId
                 * final String account, final String guid,final String imageUrl, final int type,
                 *
                 * videoCallAlert(final Context context,PurchaseHowMoneyDionmads dionmads,VideoInviteParcelable videoInviteParcelable,VideoParcelable videoParcelable,String channelId,final String account, final String guid,final String imageUrl, final int type, final int inviteType)
                 */
                CustomDialogAboutOther.videoCallAlert(context, dionmads,videoInviteParcelable, null,null, null, null,null,0,0,true);

//                //这是正常的情况下，先判断是否是开通vip，然后判断是否具有钻石
//                if (dionmads.getPayIntercept() == 2) {
//                    CustomDialogAboutPay.dregeVipShow(context);
//                } else if (dionmads.getPayIntercept() == 1) {
//                    CustomDialogAboutPay.purchaseDiamondShow(context);
//                } else {
//                    if (UserPreference.getStatus().equals("3")) {
//                        Toast.makeText(context, mContext.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
//                    } else {
//                        int remoteStatus = dionmads.getRemoteStatus();
//                        if (remoteStatus == 1) {//空闲
//                            LaunchHelper.getInstance().launch(context, VideoInviteActivity.class, videoInviteParcelable);
//                        } else if (remoteStatus == 2) {//通话中
//                            Toast.makeText(context, mContext.getString(R.string.on_the_phone), Toast.LENGTH_SHORT).show();
//                        } else if (remoteStatus == 3) {//勿扰
//                            Toast.makeText(context, mContext.getString(R.string.quiet_hours_other), Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }




            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
                ToastUtil.showShortToast(context, mContext.getString(R.string.video_invite_failed));
            }
        });
    }
}
