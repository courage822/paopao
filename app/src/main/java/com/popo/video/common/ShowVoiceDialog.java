package com.popo.video.common;

import android.app.Dialog;
import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.popo.library.util.FileUtil;
import com.popo.library.util.ToastUtil;
import com.popo.video.C;
import com.popo.video.R;

import java.io.File;

/**
 * Created by WangYong on 2017/9/5.
 */

public class ShowVoiceDialog {
    // 录音计时刷新间隔（毫秒）
    private final long mRefreshInterval = 250;
    ImageView iv_voice;
    TextView tv_time, tv_desc;
    // 录音文件存放目录
    private String mRecordDirectory;
    // 录音文件路径
    private String mRecordOutputPath;
    // 是否正在录音
    private boolean mIsRecording;
    // 录音时长（毫秒）
    private long mRecordDuration;
    private Context mContext;

    private OnVoiceRecordListener mOnVoiceRecordListener;

    // 异步任务
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            handleAsyncTask(msg);
        }
    };

    private void handleAsyncTask(Message msg) {

        if (msg.what == C.message.MSG_TYPE_TIMER) {
            // 计时
            if (mIsRecording) {
                mRecordDuration += mRefreshInterval;
                if (mRecordDuration % 1000 == 0) {
                    tv_time.setText(toTime(mRecordDuration));
                }
                mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_TIMER, mRefreshInterval);
            }
        }
    }

    private String toTime(long mRecordDuration) {
        String time;
        if (mRecordDuration < 10000) {
            time = "00:0" + (mRecordDuration / 1000);
        } else {
            time = "00:" + (mRecordDuration / 1000);
        }
        return time;
    }

    //录音对话框
    public void recordAndMsgShow(Context context) {
        this.mContext = context;
        mRecordDirectory = FileUtil.getExternalFilesDir(mContext, Environment.DIRECTORY_MUSIC) + File.separator;

        final Dialog bottomDialog = new Dialog(mContext, R.style.BottomDialog);
        final View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_show_voice, null);
        bottomDialog.setContentView(contentView);
        tv_time = (TextView) contentView.findViewById(R.id.tv_time);
        tv_desc = (TextView) contentView.findViewById(R.id.tv_desc);
        iv_voice = (ImageView) contentView.findViewById(R.id.iv_voice);

        iv_voice.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:// 按下时开始录音
                        tv_desc.setText(R.string.voice_recording);
                        startRecord();
                        break;

                    case MotionEvent.ACTION_UP:// 松开时停止录音并发送
                        tv_desc.setText(mContext.getString(R.string.chat_press_to_speak));
                        stopRecordAndSend(bottomDialog);

                        break;
                }
                return true;
            }
        });
        dialogShow(mContext, bottomDialog, contentView, Gravity.BOTTOM);
    }

    private void startRecord() {
        // 录音开始前震动一下
        Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(88);
        // 根据系统时间生成文件名
        String recordFileName = FileUtil.createFileNameByTime() + ".aac";
        // 录音文件临时保存路径：data下包名music目录
        mRecordOutputPath = mRecordDirectory + recordFileName;
        // 开始录音
        RecordUtil.getInstance().startRecord(mRecordOutputPath);
        mIsRecording = true;
        mRecordDuration = 0;
        // 计时，间隔250毫秒刷新
        mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_TIMER, mRefreshInterval);
    }

    private void stopRecordAndSend(Dialog bottomDialog) {
        if (mIsRecording) {
            mIsRecording = false;
            if (mRecordDuration < 2000 || mRecordDuration > 60000) {// 录音时长小于2秒或大于60秒的不发送
                mHandler.removeMessages(C.message.MSG_TYPE_TIMER);
                mRecordDuration = 0;
                tv_time.setText(toTime(mRecordDuration));
                // 删除文件并提示
                FileUtil.deleteFile(mRecordOutputPath);
                ToastUtil.showShortToast(mContext, mContext.getString(R.string.voice_limit));
            } else {// 发送语音
                RecordUtil.getInstance().stopRecord();
                if (mOnVoiceRecordListener != null) {
                    mOnVoiceRecordListener.onRecord(mRecordOutputPath, mRecordDuration);
                }
                bottomDialog.dismiss();
            }
        }
    }

    //对话框初始化显示的方法
    private static void dialogShow(Context mContext, Dialog bottomDialog, View contentView, int location) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = mContext.getResources().getDisplayMetrics().widthPixels - dp2px(mContext, 16f);
        params.height = dp2px(mContext, 200);
        params.bottomMargin = dp2px(mContext, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.getWindow().setGravity(location);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    public static int dp2px(Context mContext, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal,
                mContext.getResources().getDisplayMetrics());
    }

    public void setOnVoiceRecordListener(OnVoiceRecordListener onVoiceRecordListener) {
        this.mOnVoiceRecordListener = onVoiceRecordListener;
    }

    public interface OnVoiceRecordListener {
        void onRecord(String filePath, long voiceDuration);
    }
}
