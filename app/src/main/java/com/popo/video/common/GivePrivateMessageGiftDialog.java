package com.popo.video.common;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hyphenate.chat.EMMessage;
import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.model.PicInfo;
import com.popo.video.data.model.ResultMessageType;
import com.popo.video.data.preference.PayPreference;
import com.popo.video.db.DbModle;
import com.popo.video.event.MessageArrive;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.util.SharedPreferenceUtil;
import com.popo.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by xuzhaole on 2017/12/5.
 * 解锁私密消息的dialog
 */

public class GivePrivateMessageGiftDialog {
    ImageView iv_switch, iv_sound_voice;
    LinearLayout ll_record_voice, ll_dialog;
    TextView tv_time_voice;
    Button btn_record, btn_sure;
    EditText et_msg;

    String giftId;
    String[] giftIds;
    private static String userAccount;
    private static String userIcon;
    private static String userNickName;
    private Context context;


    /**
     * @param context   上下问
     * @param msgId     消息id
     * @param giftId    礼物id
     * @param remoteUid 索要礼物人id
     * @param account   索要礼物的account
     * @param nickName  索要礼物的nickname
     * @param iconUrl   索要礼物的头像
     * @param type      私密消息类型
     */
    public void givePrivateMessageGiftShow(final Context context, final String msgId, final String giftId, final String remoteUid, String account, String nickName, String iconUrl, int type) {
        this.context = context;
        userAccount = account;
        userIcon = iconUrl;
        userNickName = nickName;
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_give_private_message_gift, null);
        bottomDialog.setContentView(contentView);
        TextView tv_private_type = (TextView) contentView.findViewById(R.id.tv_private_type);
        TextView tv_price = (TextView) contentView.findViewById(R.id.tv_price);
        Button btn_sure = (Button) contentView.findViewById(R.id.ask_for_gifts_btn_sure);
        TextView tv_gift_type = (TextView) contentView.findViewById(R.id.tv_gift_type);
        ImageView iv_cancel = (ImageView) contentView.findViewById(R.id.iv_cancel);
        ImageView iv_private_type = (ImageView) contentView.findViewById(R.id.iv_private_type);
        if (type == 2) {//语音
            tv_private_type.setText(String.format(context.getString(R.string.unlock_look_dialog), context.getString(R.string.user_detail_voice)));
        } else if (type == 3) {//图片
            tv_private_type.setText(String.format(context.getString(R.string.unlock_look_dialog), context.getString(R.string.picture)));
        } else if (type == 4) {//视频
            tv_private_type.setText(String.format(context.getString(R.string.unlock_look_dialog), context.getString(R.string.video_video)));

        } else {//语音、文字
            tv_private_type.setText(String.format(context.getString(R.string.unlock_look_dialog), context.getString(R.string.word)));
        }
        final PicInfo picInfo = DbModle.getInstance().getUserAccountDao().getPicInfo(giftId);
        ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(picInfo.getPicIcon())
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_private_type).build());
        tv_gift_type.setText(picInfo.getPicName());
        tv_price.setText(picInfo.getPicPrice() + context.getString(R.string.dialog_unit_ask_gift));

        //发送礼物，解锁私密消息
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unlockGifts(msgId, giftId, remoteUid, picInfo.getPicName(), picInfo.getPicIcon(), context);
                bottomDialog.dismiss();
            }
        });
        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialog.dismiss();
            }
        });

        dialogShow(context, bottomDialog, contentView, Gravity.BOTTOM);
    }

    //解锁礼物
    private void unlockGifts(final String msgId, final String giftId, final String remoteUid, final String giftName, final String giftIcon, final Context context) {
        ApiManager.unlockGiftMsg(msgId, giftId, remoteUid, new IGetDataListener<ResultMessageType>() {
            @Override
            public void onResult(ResultMessageType resultMessageType, boolean isEmpty) {

                if (resultMessageType != null) {
                    int beanStatus = resultMessageType.getBeanStatus();
                    if (beanStatus == -1) {
                        CustomDialogAboutPay.purchaseDiamondShow(context);
//                        Toast.makeText(context, "余额不足，请充值", Toast.LENGTH_SHORT).show();
                    } else if (beanStatus == 1) {
                        sendGiftsMsg(remoteUid, giftId, "1", giftName, giftIcon);
                        PayPreference.saveDionmadsNum(resultMessageType.getBeanCount());
                        SharedPreferenceUtil.setStringValue(context, msgId,
                                msgId, remoteUid);
                        Toast.makeText(context, context.getString(R.string.gift_send_success), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                ToastUtil.showShortToast(context, TextUtils.isEmpty(msg) ? context.getString(R.string.chat_send_failed) : msg);
            }
        });
    }

    //调用环信发送礼物成功的信息
    public void sendGiftsMsg(final String guid, String giftsId, String num, String giftName, String giftIcon) {
        PicInfo picInfo = new PicInfo(giftsId, giftName, "price", num, giftIcon);
        HyphenateHelper.getInstance().sendTextMessage(userAccount, new Gson().toJson(picInfo),
                new HyphenateHelper.OnMessageSendListener() {
                    @Override
                    public void onSuccess(EMMessage emMessage) {
                        EventBus.getDefault().post(new MessageArrive(guid));//发送完成之后更新聊天列表
                    }

                    @Override
                    public void onError() {
                    }
                });
        saveDataToSqlit(userNickName, guid, userAccount, userIcon, context.getString(R.string.gift), String.valueOf(System.currentTimeMillis()), 0);
        ApiManager.interruptText(Long.parseLong(guid), new Gson().toJson(picInfo), new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    //对话框初始化显示的方法
    private static void dialogShow(Context context, Dialog bottomDialog, View contentView, int location) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = context.getResources().getDisplayMetrics().widthPixels - dp2px(context, 16f);
        params.bottomMargin = dp2px(context, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(false);
        bottomDialog.getWindow().setGravity(location);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    public static void saveDataToSqlit(String name, String id, String account, String pic, String msg, String time, int extendType) {
        if (!TextUtils.isEmpty(id)) {
            HuanXinUser user = new HuanXinUser(id, name, pic, account, "1", msg, 0, time, extendType);
            DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
        }
    }

    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal,
                context.getResources().getDisplayMetrics());
    }
}
