package com.popo.video.common;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.BlockBean;
import com.popo.video.data.model.PayDict;
import com.popo.video.data.model.PayWay;
import com.popo.video.data.model.SearchUser;
import com.popo.video.data.model.UserPhoto;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.PayPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.event.FinishWxActivity;
import com.popo.library.adapter.MultiTypeRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.util.DateTimeUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.parcelable.ChatParcelable;
import com.popo.video.parcelable.PayInfoParcelable;
import com.popo.video.parcelable.PayParcelable;
import com.popo.video.parcelable.ReportParcelable;
import com.popo.video.ui.chat.ChatActivity;
import com.popo.video.ui.detail.ReportActivity;
import com.popo.video.ui.detail.adapter.AlbumPhotoAdapter;
import com.popo.video.ui.dialog.adapter.DialogKeyAndDiamondsAdapter;
import com.popo.video.ui.pay.activity.PayActivity;
import com.popo.video.ui.pay.activity.RechargeActivity;

import org.greenrobot.eventbus.EventBus;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by WangYong on 2017/9/5.
 */

public class CustomDialogAboutPay {
    private static String PAYFIELD = null;
    private static int vippos = 0;
    private static int keypos = 0;
    private static int diamondspos = 0;
    private static int diamondspos2 = 0;

    //举报对话框
    public static void reportShow(final Context context, final String userId) {

        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_report, null);
        bottomDialog.setContentView(contentView);
        RelativeLayout rl_report = (RelativeLayout) contentView.findViewById(R.id.dialog_rl_report);
        RelativeLayout rl_lahei = (RelativeLayout) contentView.findViewById(R.id.dialog_rl_lahei);
        final TextView tv_block = (TextView) contentView.findViewById(R.id.tv_block);
        rl_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
                LaunchHelper.getInstance().launch(context, ReportActivity.class, new ReportParcelable(userId));
            }
        });
        ApiManager.getIsBlock(userId, new IGetDataListener<BlockBean>() {
            @Override
            public void onResult(BlockBean blockBean, boolean isEmpty) {
                if (blockBean != null) {
                    if (blockBean.getIsBlock() == 0) {//未拉黑
                        tv_block.setText(context.getString(R.string.block));
                    } else {//已拉黑
                        tv_block.setText(context.getString(R.string.block_cancel));
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                tv_block.setText(context.getString(R.string.block));
            }
        });


        rl_lahei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_block.getText().toString().trim().equals(context.getString(R.string.block))) {
                    Toast.makeText(context, context.getString(R.string.block_success), Toast.LENGTH_SHORT).show();
                    bottomDialog.dismiss();
                    ApiManager.lahei(userId, new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                        }
                    });
                } else {
                    Toast.makeText(context, context.getString(R.string.block_cancel_success), Toast.LENGTH_SHORT).show();
                    bottomDialog.dismiss();
                    ApiManager.cancellahei(userId, new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                        }
                    });
                }
            }
        });
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = context.getResources().getDisplayMetrics().widthPixels - dp2px(context, 16f);
        params.bottomMargin = dp2px(context, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.getWindow().setGravity(Gravity.TOP);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }


    private static void setRecyclerView(Context context, RecyclerView recyelerview) {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        recyelerview.setLayoutManager(gridLayoutManager);
        recyelerview.setHasFixedSize(true);
    }

    //购买很多钻石
    public static void purchaseDiamondShow(final Context context) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_purchase_diamonds, null);
        bottomDialog.setContentView(contentView);
        ImageView iv_dismiss = (ImageView) contentView.findViewById(R.id.purchase_diamonds_iv_dismiss);
        TextView tv_diamonds = (TextView) contentView.findViewById(R.id.purchase_diamonds_tv_diamonds);
        RecyclerView recyclerView = (RecyclerView) contentView.findViewById(R.id.purchase_diamonds_recyclerview);
        Button btn_sure = (Button) contentView.findViewById(R.id.purchase_diamonds_btn_sure);
        setRecyclerView(context, recyclerView);
        tv_diamonds.setText(PayPreference.getDionmadsNum() + "");
        PayWay payWay = new Gson().fromJson(PayPreference.getDiamondInfo(), PayWay.class);
        final List<PayDict> dictPayList = payWay.getDictPayList();
        if (dictPayList != null && dictPayList.size() > 0) {
            diamondspos = 0;
            PAYFIELD = dictPayList.get(0).getServiceId();
        }
        final DialogKeyAndDiamondsAdapter adapter = new DialogKeyAndDiamondsAdapter(context, R.layout.dialog_purchase_diamonds_item, dictPayList);
        adapter.setIsDiamonds();//设置是钻石
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                adapter.setSelectedPosition(position);
                adapter.notifyDataSetChanged();
                diamondspos = position;
                PAYFIELD = dictPayList.get(position).getServiceId();
            }
        });
        cancelDialog(bottomDialog, iv_dismiss);
        paySureCommit(context, bottomDialog, btn_sure, dictPayList, 1);
        dialogShow(context, bottomDialog, contentView);
    }

    //批量打招呼
    public static void sayHelloShow(final Context context, List<SearchUser> searchUserList1) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_say_hello, null);
        bottomDialog.setContentView(contentView);
        bottomDialog.setCanceledOnTouchOutside(false);
        bottomDialog.setCancelable(false);
        RecyclerView recyclerView = (RecyclerView) contentView.findViewById(R.id.say_hello_recyclerview);
        Button btn_sure = (Button) contentView.findViewById(R.id.say_hello_btn_sure);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 5);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);
        AlbumPhotoAdapter adapter = new AlbumPhotoAdapter(context, R.layout.album_recyclerview_item, 0);
        recyclerView.setAdapter(adapter);
        final List<UserPhoto> list = new ArrayList<>();
        final List<String> listUser = new ArrayList<>();
        if (searchUserList1 != null && searchUserList1.size() > 0) {
            for (int i = 0; i < searchUserList1.size(); i++) {
                UserPhoto userPhoto = new UserPhoto();
                SearchUser searchUser = searchUserList1.get(i);
                if (searchUser != null) {
                    listUser.add(String.valueOf(searchUser.getUesrId()));
                    userPhoto.setFileUrlMinimum(searchUser.getIconUrlMininum());
                    list.add(userPhoto);
                    adapter.bind(list);
                }
            }
        }
        dialogShow(context, bottomDialog, contentView);
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, context.getString(R.string.hello_success), Toast.LENGTH_SHORT).show();
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < listUser.size(); i++) {
                    sb.append(listUser.get(i)).append(",");
                }
                String s = sb.toString();
                if (!TextUtils.isEmpty(s)) {
                    String substring = s.substring(0, s.length() - 1);
                    ApiManager.sayHelloGroup(substring, new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {

                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {

                        }
                    });
                }


                bottomDialog.dismiss();

            }
        });
    }


    //两个钻石的支付
    public static void diamondRadiousShow(final Context context) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_slight_diamonds, null);
        bottomDialog.setContentView(contentView);
        ImageView iv_dismiss = (ImageView) contentView.findViewById(R.id.dialog_diamond_iv_dismiss);
        final RelativeLayout rl_100 = (RelativeLayout) contentView.findViewById(R.id.dialog_diamond_rl_100);
        final RelativeLayout rl_600 = (RelativeLayout) contentView.findViewById(R.id.dialog_diamond_rl_600);
        Button btn_sure = (Button) contentView.findViewById(R.id.dialog_diamond_btn_sure);
//        PayWay payWay = new Gson().fromJson(PayPreference.getVipInfo(), PayWay.class);
//        List<PayDict> dictPayList = payWay.getDictPayList();
//        PAYFIELD=dictPayList.get(0).getServiceId();
        cancelDialog(bottomDialog, iv_dismiss);
        rl_100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDiamondDrawable(rl_100, rl_600, true);
                diamondspos2 = 0;
            }

        });
        rl_600.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDiamondDrawable(rl_100, rl_600, false);
                diamondspos2 = 1;
            }
        });
//         paySureCommit(context, bottomDialog, btn_sure,dictPayList.get(diamondspos2).getServiceName(),dictPayList.get(diamondspos2).getPrice(),3);
        dialogShow(context, bottomDialog, contentView);
    }

    //开通VIP
    public static void dregeVipShow(final Context context) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_dredge_vip, null);
        bottomDialog.setContentView(contentView);
        Button iv_dismiss = (Button) contentView.findViewById(R.id.dredge_vip_btn_cancel);
        TextView tv_desc = (TextView) contentView.findViewById(R.id.tv_desc);
        Button btn_sure = (Button) contentView.findViewById(R.id.dredge_vip_btn_sure);
        tv_desc.setText(context.getString(R.string.dredge_vip_desc));

        iv_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialog.dismiss();
                LaunchHelper.getInstance().launch(context, RechargeActivity.class,new PayParcelable(C.pay.FROM_TAG_PERSON, 0));
            }
        });
        dialogShow(context, bottomDialog, contentView);
    }

    //消费成功的提醒
    public static void paySucceedShow(final Context context, String serviceName, String price, int type) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_pay_succed, null);
        bottomDialog.setContentView(contentView);
        bottomDialog.setCanceledOnTouchOutside(false);
        bottomDialog.setCancelable(false);
        final TextView tv_keys = (TextView) contentView.findViewById(R.id.pay_succed_tv_howmaney);
        final TextView tv_fuwu = (TextView) contentView.findViewById(R.id.pay_succed_tv_howmaney_fuwu);
        final TextView tv_yuan = (TextView) contentView.findViewById(R.id.pay_succed_tv_howmaney_yuan);
        final TextView tv_time = (TextView) contentView.findViewById(R.id.pay_succed_tv_howmaney_time);
        Button btn_sure = (Button) contentView.findViewById(R.id.pay_succed_btn_sure);
        tv_yuan.setText(context.getString(R.string.money) + price + context.getString(R.string.money));
        if (type == 1) {//钻石
            tv_keys.setText(serviceName);
            tv_fuwu.setText(context.getString(R.string.number_diamonds));
            PayPreference.saveDionmadsNum(PayPreference.getDionmadsNum() + Integer.parseInt(serviceName));
        } else if (type == 2) {//vip
            tv_keys.setText(serviceName);
            tv_fuwu.setText("VIP");
            UserPreference.setIsVip(true);
        } else if (type == 3) {//钥匙
            tv_keys.setText(serviceName);
            tv_fuwu.setText(context.getString(R.string.number_keys));
        }
        tv_time.setText(DateTimeUtil.convertTimeMillis2String(System.currentTimeMillis()));
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
                EventBus.getDefault().post(new FinishWxActivity());
            }
        });
        dialogShow(context, bottomDialog, contentView);
    }

    //消费失败的提醒
    public static void payFaildShow(final Context context) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_pay_faild, null);
        bottomDialog.setContentView(contentView);
        final TextView tv_lianxi = (TextView) contentView.findViewById(R.id.pay_faild_tv_lianxi);
        Button btn_sure = (Button) contentView.findViewById(R.id.pay_faild_btn_sure);
        bottomDialog.setCanceledOnTouchOutside(false);
        bottomDialog.setCancelable(false);
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
                EventBus.getDefault().post(new FinishWxActivity());
            }
        });
        tv_lianxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LaunchHelper.getInstance().launch(context, ChatActivity.class, new ChatParcelable(10000,
                        "100010000", context.getString(R.string.kefu_desc), DataPreference.getSystemIcon(), 0));
                bottomDialog.dismiss();
                EventBus.getDefault().post(new FinishWxActivity());
            }
        });
        dialogShow(context, bottomDialog, contentView);
    }


    //对话框初始化显示的方法
    private static void dialogShow(Context context, Dialog bottomDialog, View contentView) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = context.getResources().getDisplayMetrics().widthPixels - dp2px(context, 16f);
        params.bottomMargin = dp2px(context, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(false);
        bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    //设置选中与未选中状态
    private static void setDrawable(RelativeLayout view1, RelativeLayout view2, boolean flag) {
        if (flag) {
            view1.setBackgroundResource(R.drawable.all_pay_selected);
            view2.setBackgroundResource(R.drawable.all_pay_un_selected);
        } else {
            view2.setBackgroundResource(R.drawable.all_pay_selected);
            view1.setBackgroundResource(R.drawable.all_pay_un_selected);
        }
    }

    //设置两个钻石的图片
    private static void setDiamondDrawable(RelativeLayout view1, RelativeLayout view2, boolean flag) {
        if (flag) {
            view1.setBackgroundResource(R.drawable.purchase_diamond_selected);
            view2.setBackgroundResource(R.drawable.purchase_diamond_un_selected);
        } else {
            view2.setBackgroundResource(R.drawable.purchase_diamond_selected);
            view1.setBackgroundResource(R.drawable.purchase_diamond_un_selected);
        }
    }

    //取消对话框的显示
    private static void cancelDialog(final Dialog bottomDialog, ImageView iv_dismiss) {
        iv_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
    }

    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal,
                context.getResources().getDisplayMetrics());
    }

    //最后确认提交并调用支付
    private static void paySureCommit(final Context context, final Dialog bottomDialog, Button btn_sure,   final List<PayDict> dictPayList ,final int type) {
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
                if (type == 1) {
                    LaunchHelper.getInstance().launch(context, PayActivity.class, new PayInfoParcelable(dictPayList.get(diamondspos).getServiceId(),dictPayList.get(diamondspos).getServiceName(),type,dictPayList.get(diamondspos).getPrice(),diamondspos));
                } else if (type == 2) {
                    LaunchHelper.getInstance().launch(context, PayActivity.class, new PayInfoParcelable(dictPayList.get(vippos).getServiceId(),dictPayList.get(vippos).getServiceName(),type,dictPayList.get(vippos).getPrice(),vippos));
                } else if (type == 3) {
                    LaunchHelper.getInstance().launch(context, PayActivity.class, new PayInfoParcelable(dictPayList.get(keypos).getServiceId(),dictPayList.get(keypos).getServiceName(),type,dictPayList.get(keypos).getPrice(),keypos));
                }
            }
        });
    }
    private static void setTextFlag(TextView tv_price1,TextView tv_price2,PayDict payDict1,PayDict payDict2){
        switch (UserPreference.getCountryId()) {
            case "97"://美国
                tv_price1.setText(TextUtils.concat("$", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("$", String.valueOf(payDict2.getPrice())).toString());
                break;
            case "16"://澳大利亚
                tv_price1.setText(TextUtils.concat("AU$", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("AU$", String.valueOf(payDict2.getPrice())).toString());
                break;
            case "67"://加拿大
                tv_price1.setText(TextUtils.concat("CA$", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("CA$", String.valueOf(payDict2.getPrice())).toString());
                break;
            case "174"://香港
                tv_price1.setText(TextUtils.concat("HK$", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("HK$", String.valueOf(payDict1.getPrice())).toString());
                break;
            case "161"://印度
                tv_price1.setText(TextUtils.concat("INR", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("INR", String.valueOf(payDict2.getPrice())).toString());
                break;
            case "162"://印度尼西亚
                tv_price1.setText(TextUtils.concat("IDR", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("IDR", String.valueOf(payDict2.getPrice())).toString());
                break;
            case "10"://爱尔兰
                tv_price1.setText(TextUtils.concat("€", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("€", String.valueOf(payDict2.getPrice())).toString());
                break;
            case "152"://新西兰
                tv_price1.setText(TextUtils.concat("NZ$", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("NZ$", String.valueOf(payDict2.getPrice())).toString());
                break;
            case "20"://巴基斯坦
                tv_price1.setText(TextUtils.concat("PKR", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("PKR", String.valueOf(payDict2.getPrice())).toString());
                break;
            case "46"://菲律宾
                tv_price1.setText(TextUtils.concat("PHP", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("PHP", String.valueOf(payDict2.getPrice())).toString());
                break;
            case "150"://新加坡
                tv_price1.setText(TextUtils.concat("S$", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("S$", String.valueOf(payDict2.getPrice())).toString());
                break;
            case "108"://南非
                tv_price1.setText(TextUtils.concat("ZAR", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("ZAR", String.valueOf(payDict2.getPrice())).toString());
                break;
            case "163"://英国
                tv_price1.setText(TextUtils.concat("￡", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("￡", String.valueOf(payDict2.getPrice())).toString());
                break;
            case "173"://台湾
                tv_price1.setText(TextUtils.concat("NT$", String.valueOf(payDict1.getPrice())).toString());
                tv_price2.setText(TextUtils.concat("NT$", String.valueOf(payDict2.getPrice())).toString());
                break;
//            case "171"://中国
//                break;
        }
    }
}
