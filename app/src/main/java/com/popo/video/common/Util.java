package com.popo.video.common;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.popo.library.util.Utils;
import com.popo.video.R;
import com.popo.video.base.BaseApplication;
import com.popo.video.data.model.Country;
import com.popo.video.data.model.DictPayGift;
import com.popo.video.data.model.GiftsDictiorary;
import com.popo.video.data.model.State;
import com.popo.video.data.model.UploadInfoParams;
import com.popo.video.data.model.UserPhoto;
import com.popo.video.data.preference.PayPreference;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.UserPreference;
import com.spreada.utils.chinese.ZHConverter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidAlgorithmParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;

/**
 * 公用工具类
 * Created by zhangdroid on 2017/6/3.
 */
public class Util {
    private static long time = System.currentTimeMillis();

    private Util() {
    }

    /**
     * 将List<UserPhoto>转成List<String>
     *
     * @param list
     * @return
     */
    public static List<String> convertPhotoUrl(List<UserPhoto> list) {
        List<String> stringList = new ArrayList<>();
        if (!Utils.isListEmpty(list)) {
            for (UserPhoto item : list) {
                if (null != item) {
                    stringList.add(item.getFileUrl());
                }
            }
        }
        return stringList;
    }

    /**
     * 获得UploadInfoParamsJSON字符串
     */
    public static String getUploadInfoParamsJsonString(UploadInfoParams uploadInfoParams) {
        // 创建以uploadInfoParams为Key的JSON字符串
        Map<String, UploadInfoParams> map = new HashMap<>();
        map.put("uploadInfoParams", uploadInfoParams);
        return new Gson().toJson(map);
    }

    /**
     * @return 随机返回默认图片
     */
    public static int getDefaultImage() {
        int[] defaultImgs = {R.drawable.my_photo_icon, R.drawable.my_photo_icon, R.drawable.my_photo_icon,
                R.drawable.my_photo_icon, R.drawable.my_photo_icon};
        return defaultImgs[new Random().nextInt(defaultImgs.length)];
    }

    /**
     * @return 随机返回默认圆形图片
     */
    public static int getDefaultImageCircle() {
        int[] defaultImgs = {R.drawable.my_circle_icon, R.drawable.my_circle_icon, R.drawable.my_circle_icon,
                R.drawable.my_circle_icon, R.drawable.my_circle_icon};
        return defaultImgs[new Random().nextInt(defaultImgs.length)];
    }

    /**
     * 计时：将秒数转换成时间字符串
     *
     * @param seconds
     * @return
     */
    public static String convertSecondsToString(int seconds) {
        StringBuilder stringBuilder = new StringBuilder();
        if (seconds < 60) {// 一分钟内
            stringBuilder.append("00:")
                    .append(pad(seconds));
        } else if (seconds >= 60 && seconds < 60 * 60) {// 1小时内
            stringBuilder.append(pad(seconds / 60))
                    .append(":")
                    .append(pad(seconds % 60));
        } else if (seconds >= 60 * 60 && seconds < 24 * 60 * 60) {// 1天内
            stringBuilder.append(pad(seconds / 60 * 60))
                    .append(":")
                    .append(pad((seconds % 60 * 60) / 60))
                    .append(":")
                    .append(pad((seconds % 60 * 60) % 60));
        }
        return stringBuilder.toString();
    }

    /**
     * 小于10的数前面补0
     *
     * @param number
     * @return
     */
    public static String pad(int number) {
        if (number < 10) {
            return "0" + number;
        } else {
            return String.valueOf(number);
        }
    }

    /**
     * 读取assets目录下的json文件
     *
     * @param context  上下文对象
     * @param fileName assets目录下的json文件路径
     */
    public static String getJsonStringFromAssets(Context context, String fileName) {
        String jsonStr = null;
        if (!TextUtils.isEmpty(fileName)) {
            BufferedReader bufferedReader = null;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));
                String line;
                StringBuilder buffer = new StringBuilder();
                while (!TextUtils.isEmpty((line = bufferedReader.readLine()))) {
                    buffer.append(line);
                }
                jsonStr = buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return jsonStr;
    }

    /**
     * 从assets目录下读取国家和语音 json数据结构是一样的，因此就写成Country一个类
     */
    public static List<Country> getCountryFromJson(String fileName) {
        // 解析国家json文件
        List<Country> countryStateList = stringToList(getJsonStringFromAssets(getContext(), fileName), Country.class);
        if (countryStateList != null) {
            return countryStateList;
        }
        return null;
    }

    /**
     * 从assets目录下读取州信息
     */
    public static List<State> getStateFromJson(String fileName) {
        // 解析国家json文件
        List<State> countryStateList = stringToList(getJsonStringFromAssets(getContext(), fileName), State.class);
        if (countryStateList != null) {
            return countryStateList;
        }
        return null;
    }

    /**
     * 获取所有州的数据
     *
     * @return
     */
    public static List<Map<String, String>> getStateString() {
        String userCountry = PlatformPreference.getPlatformInfo().getCountry();
        List<Map<String, String>> list = new ArrayList<>();
        if (!TextUtils.isEmpty(userCountry) && "China".equals(userCountry)) {
            List<State> countryFromJson = Util.getStateFromJson("StateChinese.json");
            for (State state : countryFromJson) {
                if (state.getCountry_id().equals(UserPreference.getCountryId())) {
                    Map<String, String> map = new HashMap<>();
                    map.put(state.getGuid(), state.getName());
                    list.add(map);
                }
            }
        }
        return list;
    }

    /**
     * 获取星座数据
     *
     * @return
     */
    public static List<Map<String, String>> getSignString() {
        String language = Util.getLacalLanguage();
        List<Map<String, String>> list = new ArrayList<>();
        if (!TextUtils.isEmpty(language)) {
            List<Country> countryFromJson;
            if ("Simplified".equals(language)) {
                countryFromJson = Util.getCountryFromJson("SignChinese.json");
            } else if ("Traditional".equals(language)) {
                countryFromJson = Util.getCountryFromJson("SignTaiwan.json");
            } else {
                countryFromJson = Util.getCountryFromJson("SignEnglish.json");
            }
            for (Country state : countryFromJson) {
                Map<String, String> map = new HashMap<>();
                map.put(state.getGuid(), state.getName());
                list.add(map);
            }
        }
        return list;
    }

    public static Context getContext() {
        return BaseApplication.getGlobalContext();
    }

    public static boolean isListEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }

    /**
     * 通过gson把json字符串转成list集合
     *
     * @param json
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> List<T> stringToList(String json, Class<T> cls) {
        Gson gson = new Gson();
        List<T> list = new ArrayList<T>();
        JsonArray array = new JsonParser().parse(json).getAsJsonArray();
        for (final JsonElement elem : array) {
            list.add(gson.fromJson(elem, cls));
        }
        return list;
    }

    public static String getLacalCountry() {
        String country = "United State";
        String CT = Locale.getDefault().getCountry();
        switch (CT) {
            case "TW":
                country = "Taiwan";
                break;
            case "HK":
                country = "Taiwan";
                break;
            case "CN":
                country = "China";
                break;
            case "US":
                country = "United State";
                break;
            case "IN":
                country = "India";
                break;
            case "UK":
                country = "UnitedKingdom";
                break;
        }
        return country;
    }

    public static String getLacalLanguage() {
        String language = "English";
        String LG = Locale.getDefault().getLanguage();
        switch (LG) {
            case "zh":
                if (getLacalCountry().equals("China")) {
                    language = "Simplified";
                } else if (getLacalCountry().equals("Taiwan")) {
                    language = "Traditional";
                }
                break;
            case "en":
                language = "English";
                break;

        }
        return language;
    }

    /**
     * 获得国家列表
     *
     * @return
     */
    public static List<String> getCountryList() {
        List<String> list = new ArrayList<String>();
        list.add("United States");
        list.add("Australia");
        list.add("India");
        list.add("Indonesia");
        list.add("United Kingdom");
        list.add("Canada");
        list.add("New Zealand");
        list.add("Ireland");
        list.add("South Africa");
        list.add("Singapore");
        list.add("Pakistan");
        list.add("Philippines");
        list.add("Hong Kong");
        return list;
    }

    public static String getMD5(String val, String salt) {
        MessageDigest md5 = null;
        byte[] m = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
            md5.update((val + salt).getBytes());
            m = md5.digest();//加密
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return getString(m);
    }

    private static String getString(byte[] b) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            sb.append(Long.toString((int) b[i] & 0xff, 16));
        }
        return sb.toString();
    }

    /**
     * 隐藏键盘
     */
    public static void hideKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 对字符串进行md5/AES加密
     */
    public static String getAes(String str) {
        StringBuffer sb = new StringBuffer();
        String md5Value = AESUtils.getMd5Value(str);
        String times = DataUtil.getTime();
        sb.append(md5Value);
        sb.append("-");
        sb.append(times);
        String encode = null;
        try {
            encode = AESUtils.encode(sb.toString());
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return encode;
    }

    /**
     * 在使用 okhttp 的时候，head 的一些项是中文，导致网络请求失败.
     * 挑出不合要求的字符，把这些字符单独转码
     *
     * @param headInfo
     * @return
     */
    public static String encodeHeadInfo(String headInfo) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0, length = headInfo.length(); i < length; i++) {
            char c = headInfo.charAt(i);
            if (c <= '\u001f' || c >= '\u007f') {
                stringBuffer.append(String.format("\\u%04x", (int) c));
            } else {
                stringBuffer.append(c);
            }
        }
        return stringBuffer.toString();
    }

    /**
     * 转中文
     *
     * @param unicodeStr
     * @return
     */
    public static String decode(String unicodeStr) {
        if (unicodeStr == null) {
            return null;
        }
        StringBuffer retBuf = new StringBuffer();
        int maxLoop = unicodeStr.length();
        for (int i = 0; i < maxLoop; i++) {
            if (unicodeStr.charAt(i) == '\\') {
                if ((i < maxLoop - 5) && ((unicodeStr.charAt(i + 1) == 'u') || (unicodeStr.charAt(i + 1) == 'U')))
                    try {
                        retBuf.append((char) Integer.parseInt(unicodeStr.substring(i + 2, i + 6), 16));
                        i += 5;
                    } catch (NumberFormatException localNumberFormatException) {
                        retBuf.append(unicodeStr.charAt(i));
                    }
                else
                    retBuf.append(unicodeStr.charAt(i));
            } else {
                retBuf.append(unicodeStr.charAt(i));
            }
        }
        return retBuf.toString();
    }


    /**
     * 根据礼物id获取礼物bean
     * @param giftId
     * @return
     */
    public static DictPayGift getGiftBean(String giftId) {
        String giftData = PayPreference.getGiftData();
        if (!TextUtils.isEmpty(giftData)) {
            GiftsDictiorary giftsDictiorary = new Gson().fromJson(giftData, GiftsDictiorary.class);
            List<DictPayGift> giftList = giftsDictiorary.getGiftList();
            for (DictPayGift gift : giftList) {
                if (gift.getGiftId().equals(giftId)) {
                    return gift;
                }
            }
        }
        return null;
    }
    public static long getCurrentTime() {
        Long aLong;
        try {
            aLong = new AsyncTask<Void, Void, Long>() {
                @Override
                protected Long doInBackground(Void... voids) {
                    Long along;
                    try {
                        URL url = new URL("http://www.baidu.com");
                        URLConnection uc = url.openConnection();//生成连接对象
                        uc.connect(); //发出连接
                        along = uc.getDate(); //取得网站日期时间
                    } catch (Exception e) {
                        along = time;
                        e.printStackTrace();
                    }
                    return along;
                }
            }.execute().get();
        } catch (InterruptedException e) {
            aLong = time;
            e.printStackTrace();
        } catch (ExecutionException e) {
            aLong = time;
            e.printStackTrace();
        }
        return aLong;
    }
    /**
     * 简体中文转化成繁体
     * @param str
     * @return
     */
    public static String chineseFontChanger(String str){
        return ZHConverter.convert(str,ZHConverter.TRADITIONAL);
    }
}
