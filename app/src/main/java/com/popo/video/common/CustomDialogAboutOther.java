package com.popo.video.common;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hyphenate.chat.EMMessage;
import com.popo.library.adapter.MultiTypeRecyclerViewAdapter;
import com.popo.library.adapter.RecyclerViewHolder;
import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.customload.PresentVpRlAdapter;
import com.popo.video.customload.QaQuestionAdapter;
import com.popo.video.customload.ViewPagerAdapter;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.AgoraParams;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.CallVideo;
import com.popo.video.data.model.DictPayGift;
import com.popo.video.data.model.GiftsDictiorary;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.model.PicInfo;
import com.popo.video.data.model.PicInfoData;
import com.popo.video.data.model.PurchaseHowMoneyDionmads;
import com.popo.video.data.model.QaAnswer;
import com.popo.video.data.model.QaMessage;
import com.popo.video.data.model.QaMsg;
import com.popo.video.data.model.SendGiftsType;
import com.popo.video.data.model.UserBase;
import com.popo.video.data.preference.PayPreference;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.event.GiftSendEvent;
import com.popo.video.event.InviteEvent2;
import com.popo.video.event.MessageArrive;
import com.popo.video.parcelable.MakeFreeVideoParcelable;
import com.popo.video.parcelable.PayParcelable;
import com.popo.video.parcelable.VideoInviteParcelable;
import com.popo.video.parcelable.VideoParcelable;
import com.popo.video.ui.pay.activity.MyDiamondActivity;
import com.popo.video.ui.video.MakeFreeVideoInviteActivity;
import com.popo.video.ui.video.VideoActivity;
import com.popo.video.ui.video.VideoInviteActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WangYong on 2017/9/5.
 */

public class CustomDialogAboutOther {
    //送礼物的参数
    private static int dionmadsNeed = 0;
    private static int momentNeed = 0;
    private static String giftsId = "";
    private static String giftName = "";
    private static String giftIcon = "";
    private static String giftPrice = "";
    //发送礼物的环信需要的参数
    private static String userAccount;
    private static String userIcon;
    private static String userNickName;

    //视频呼叫前的价格提醒                                                                                                                                                                                                                                                                                                           //是否是主动呼叫isInitiative
    public static void videoCallAlert(final Context context, final PurchaseHowMoneyDionmads dionmads, final VideoInviteParcelable videoInviteParcelable, final VideoParcelable videoParcelable, final String channelId, final String account, final String guid, final String imageUrl, final int type, final int inviteType, boolean isInitiative) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_msg_video_alert, null);
        bottomDialog.setContentView(contentView);
        final TextView tv_msg = (TextView) contentView.findViewById(R.id.dialog_msg_video_alert_tv_msg);
        TextView tv_sure = (TextView) contentView.findViewById(R.id.dialog_msg_video_alert_tv_sure);

//        tv_sure.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bottomDialog.dismiss();
//                if (inviteType == 0) {//主动呼叫邀请
//                    //这是正常的情况下，先判断是否是开通vip，然后判断是否具有钻石
//                    if (dionmads.getPayIntercept() == 2) {
//                        CustomDialogAboutPay.dregeVipShow(context,9);
//                    } else if (dionmads.getPayIntercept() == 1) {
//                        CustomDialogAboutPay.purchaseDiamondShow(context);
//                    } else {
//                        if (UserPreference.getStatus().equals("3")) {
//                            Toast.makeText(context, context.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
//                        } else {
//                            int remoteStatus = dionmads.getRemoteStatus();
//                            if (remoteStatus == 1) {//空闲
//                                LaunchHelper.getInstance().launch(context, VideoInviteActivity.class, videoInviteParcelable);
//                            } else if (remoteStatus == 2) {//通话中
//                                Toast.makeText(context, context.getString(R.string.on_the_phone), Toast.LENGTH_SHORT).show();
//                            } else if (remoteStatus == 3) {//勿扰
//                                Toast.makeText(context, context.getString(R.string.quiet_hours_other), Toast.LENGTH_SHORT).show();
//                            } else {//离线状态
//                                Toast.makeText(context, context.getString(R.string.video_invite_failed), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//
//
//                } else if (inviteType == 1) {//接收邀请
//
//                    if (dionmads != null) {
//                        if (dionmads.getPayIntercept() == 1) {//钻石拦截
//                            CustomDialogAboutPay.purchaseDiamondShow(context);
//                        } else {
//                            if (dionmads.getBeanStatus() == -1) {
//                                Toast.makeText(context, context.getString(R.string.not_enough_diaminds), Toast.LENGTH_SHORT).show();
//                                CustomDialogAboutPay.purchaseDiamondShow(context);
//                            }
////                            else if (dionmads.getBeanStatus() == -2) {
////                                Toast.makeText(context, context.getString(R.string.less_three_minutes), Toast.LENGTH_SHORT).show();
////                            }
//                            else {
//                                LaunchHelper.getInstance().launchFinish(context, VideoActivity.class,
//                                        videoParcelable);
//                                AgoraHelper.getInstance().acceptInvite(channelId, videoParcelable.account);
//                            }
//                        }
//                    }
//
//
//                } else if (inviteType == 2) {//直接进入频道
//
//                    if (dionmads.getPayIntercept() == 1) {
//                        CustomDialogAboutPay.dregeVipShow(context,6);
//                    } else {
//                        if (UserPreference.getStatus().equals("3")) {
//                            Toast.makeText(context, context.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
//                        } else {
//                            if (dionmads.getRemoteStatus() == 2) {//通话中
//                                EventBus.getDefault().post(new InviteEvent2());
//                            } else if (dionmads.getRemoteStatus() == 1) {//空闲
//
//                                LaunchHelper.getInstance().launch(context, VideoActivity.class,
//                                        new VideoParcelable(Long.parseLong(guid), account, imageUrl, UserPreference.getNickname(), channelId, "0", type, true));
//                                AgoraHelper.getInstance().startInvite(channelId, account, new Gson().toJson(new AgoraParams(Integer.parseInt(UserPreference.getId()), UserPreference.getNickname(), UserPreference.getSmallImage(), type, 1, Util.getCurrentTime())));
//                            } else if (dionmads.getRemoteStatus() == 3) {//勿扰
//
//                            }
//                        }
//                    }
//
//                }
//            }
//        });
//        tv_msg.setText(dionmads.getCallMsg());
        if (isInitiative && UserPreference.isMale()) {
            if (inviteType == 0) {//主动呼叫邀请


                //这是正常的情况下，先判断是否是开通vip，然后判断是否具有钻石
                if (dionmads.getPayIntercept() == 2) {
                    CustomDialogAboutPay.dregeVipShow(context);
                } else if (dionmads.getPayIntercept() == 1) {
                    CustomDialogAboutPay.purchaseDiamondShow(context);
                } else {
                    if (UserPreference.getStatus().equals("3")) {
                        Toast.makeText(context, context.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
                    } else {
                        int remoteStatus = dionmads.getRemoteStatus();

                        if (remoteStatus == 1) {//空闲
                            LaunchHelper.getInstance().launch(context, VideoInviteActivity.class, videoInviteParcelable);
                        } else if (remoteStatus == 2) {//通话中
                            Toast.makeText(context, context.getString(R.string.on_the_phone), Toast.LENGTH_SHORT).show();
                        } else if (remoteStatus == 3) {//勿扰
                            Toast.makeText(context, context.getString(R.string.quiet_hours_other), Toast.LENGTH_SHORT).show();
                        } else {//离线状态
                            Toast.makeText(context, context.getString(R.string.video_invite_failed), Toast.LENGTH_SHORT).show();
                        }
                    }
                }


            } else if (inviteType == 1) {//接收邀请

                if (dionmads != null) {
                    if (dionmads.getPayIntercept() == 1) {//钻石拦截
                        CustomDialogAboutPay.purchaseDiamondShow(context);
                    } else if (dionmads.getPayIntercept() == 2) {
                        CustomDialogAboutPay.dregeVipShow(context);
                    }else {
                        if (dionmads.getBeanStatus() == -1) {
                            Toast.makeText(context, context.getString(R.string.not_enough_diaminds), Toast.LENGTH_SHORT).show();
                            CustomDialogAboutPay.purchaseDiamondShow(context);
                        }
//                        else if (dionmads.getBeanStatus() == -2) {
//                            Toast.makeText(context, context.getString(R.string.less_three_minutes), Toast.LENGTH_SHORT).show();
//                        }
                        else {
                            LaunchHelper.getInstance().launchFinish(context, VideoActivity.class,
                                    videoParcelable);
                            AgoraHelper.getInstance().acceptInvite(channelId, videoParcelable.account);
                        }
                    }
                }


            } else if (inviteType == 2) {//直接进入频道


                if (dionmads.getPayIntercept() == 1) {
                    CustomDialogAboutPay.purchaseDiamondShow(context);
                } else if(dionmads.getPayIntercept() == 2){
                    CustomDialogAboutPay.dregeVipShow(context);
                }else {
                    if (UserPreference.getStatus().equals("3")) {
                        Toast.makeText(context, context.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
                    } else {
                        LaunchHelper.getInstance().launch(context, VideoActivity.class,
                                new VideoParcelable(Long.parseLong(guid), account, imageUrl, UserPreference.getNickname(), channelId, "0", type, true));
                        AgoraHelper.getInstance().startInvite(channelId, account, new Gson().toJson(new AgoraParams(Integer.parseInt(UserPreference.getId()), UserPreference.getNickname(), UserPreference.getSmallImage(), type, 1, Util.getCurrentTime())));
                    }
                }

            }
        }else{
            if (inviteType == 0) {//主动呼叫邀请
                //这是正常的情况下，先判断是否是开通vip，然后判断是否具有钻石
                if (dionmads.getPayIntercept() == 2) {
                    CustomDialogAboutPay.dregeVipShow(context);
                } else if (dionmads.getPayIntercept() == 1) {
                    CustomDialogAboutPay.purchaseDiamondShow(context);
                } else {
                    if (UserPreference.getStatus().equals("3")) {
                        Toast.makeText(context, context.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
                    } else {
                        int remoteStatus = dionmads.getRemoteStatus();
                        if (remoteStatus == 1) {//空闲
                            LaunchHelper.getInstance().launch(context, VideoInviteActivity.class, videoInviteParcelable);
                        } else if (remoteStatus == 2) {//通话中
                            Toast.makeText(context, context.getString(R.string.on_the_phone), Toast.LENGTH_SHORT).show();
                        } else if (remoteStatus == 3) {//勿扰
                            Toast.makeText(context, context.getString(R.string.quiet_hours_other), Toast.LENGTH_SHORT).show();
                        } else {//离线状态
                            Toast.makeText(context, context.getString(R.string.video_invite_failed), Toast.LENGTH_SHORT).show();
                        }
                    }
                }


            } else if (inviteType == 1) {//接收邀请

                if (dionmads != null) {
                    if (dionmads.getPayIntercept() == 1) {//钻石拦截
                        CustomDialogAboutPay.purchaseDiamondShow(context);
                    }  else if (dionmads.getPayIntercept() == 2) {
                        CustomDialogAboutPay.dregeVipShow(context);
                    }else {
                        if (dionmads.getBeanStatus() == -1) {
                            Toast.makeText(context, context.getString(R.string.not_enough_diaminds), Toast.LENGTH_SHORT).show();
                            CustomDialogAboutPay.purchaseDiamondShow(context);
                        }
//                            else if (dionmads.getBeanStatus() == -2) {
//                                Toast.makeText(context, context.getString(R.string.less_three_minute
// s), Toast.LENGTH_SHORT).show();
//                            }
                        else {
                            LaunchHelper.getInstance().launchFinish(context, VideoActivity.class,
                                    videoParcelable);
                            AgoraHelper.getInstance().acceptInvite(channelId, videoParcelable.account);
                        }
                    }
                }


            } else if (inviteType == 2) {//直接进入频道

                if (dionmads.getPayIntercept() == 2) {
                    CustomDialogAboutPay.dregeVipShow(context);
                } else if(dionmads.getPayIntercept()==1){
                    CustomDialogAboutPay.purchaseDiamondShow(context);
                }else {
                    if (UserPreference.getStatus().equals("3")) {
                        Toast.makeText(context, context.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
                    } else {
                        if (dionmads.getRemoteStatus() == 2) {//通话中
                            EventBus.getDefault().post(new InviteEvent2());
                        } else if (dionmads.getRemoteStatus() == 1) {//空闲
                            LaunchHelper.getInstance().launch(context, VideoActivity.class,
                                    new VideoParcelable(Long.parseLong(guid), account, imageUrl, UserPreference.getNickname(), channelId, "0", type, true));
                            AgoraHelper.getInstance().startInvite(channelId, account, new Gson().toJson(new AgoraParams(Integer.parseInt(UserPreference.getId()), UserPreference.getNickname(), UserPreference.getSmallImage(), type, 1, Util.getCurrentTime())));
                        } else if (dionmads.getRemoteStatus() == 3) {//勿扰

                        }
                    }
                }

            }
        }
//        else {
//            dialogShow2(context, bottomDialog, contentView, Gravity.CENTER);//女用户呼叫
//        }
    }

    //QA消息
    public static void qaMessageShow(final Context context, final QaMessage qaMessage, final String userAccount, final String guid, final QaMsg qaMsg, final View view) {
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_qa_receive, null);
        contentView.setFocusable(false); // 这个很重要
        contentView.setFocusableInTouchMode(false);
        final PopupWindow pop_window = new PopupWindow(contentView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
        TextView tv_question = (TextView) contentView.findViewById(R.id.dialog_qa_receive_tv_question);
        RecyclerView mRecyclerView = (RecyclerView) contentView.findViewById(R.id.dialog_qa_receive_recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        final QaQuestionAdapter adapter = new QaQuestionAdapter(context, R.layout.qa_question_item);
        adapter.bind(qaMessage.getAnswers());
        if (PlatformPreference.getPlatformInfo().getFid().equals("30108")) {
            tv_question.setText(Util.chineseFontChanger(qaMessage.getQuestionContent()));
        }else{
            tv_question.setText(qaMessage.getQuestionContent());
        }
        mRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                pop_window.dismiss();
                QaAnswer itemByPosition = adapter.getItemByPosition(position);
                if (itemByPosition != null) {
                    DbModle.getInstance().getUserAccountDao().setMsgFlag(qaMsg);
                    qaAnaswerQuestion(itemByPosition.getAnswerId(), itemByPosition.getContent(), guid);
                    updataHyXin(userAccount, itemByPosition.getContent(), guid);
                }
            }
        });
        pop_window.setFocusable(false);
        pop_window.showAtLocation(view, Gravity.BOTTOM, 0, 0);
    }

    //免费拨打电话
    public static void makeFreeCallShow(final Context context, final CallVideo callVideo) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_make_free_call, null);
        bottomDialog.setContentView(contentView);
        ImageView iv_avatar = (ImageView) contentView.findViewById(R.id.dialog_make_free_call_iv_avatar);
        RelativeLayout rl_refuse = (RelativeLayout) contentView.findViewById(R.id.dialog_make_free_call_rl_refuse);
        RelativeLayout rl_accept = (RelativeLayout) contentView.findViewById(R.id.dialog_make_free_call_rl_accept);
        rl_refuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
                UserBase callVidelUser = callVideo.getCallVidelUser();
                if (callVidelUser != null) {
                    isCallVideo(UserPreference.getId(), String.valueOf(callVidelUser.getGuid()), "2");
                }
            }
        });
        if (callVideo != null) {
            UserBase callVidelUser = callVideo.getCallVidelUser();
            if (callVidelUser != null) {
                ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(callVidelUser.getIconUrlMininum()).transform(new CropCircleTransformation(context))
                        .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(iv_avatar).build());
                rl_accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UserBase callVidelUser = callVideo.getCallVidelUser();
                        if (callVidelUser != null) {
                            bottomDialog.dismiss();
                            LaunchHelper.getInstance().launch(context, MakeFreeVideoInviteActivity.class, new MakeFreeVideoParcelable(callVidelUser.getNickName(), callVidelUser.getIcon(), callVideo.getCallVidelUrl(), callVidelUser.getAccount(), String.valueOf(callVidelUser.getGuid())));
                            isCallVideo(UserPreference.getId(), String.valueOf(callVidelUser.getGuid()), "1");
                        }
                    }
                });
            }
        }
        dialogShow2(context, bottomDialog, contentView, Gravity.CENTER);
    }

    //免费打电话的统计
    private static void isCallVideo(String userId, String remoteId, String extendTag) {
        ApiManager.userActivityTag(userId, remoteId, "7", extendTag, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    //送礼物
    public static void giveGiftShow(final Context context, final String guid, String account, String iconUrl, String nickName, final int flag, final boolean isVideo) {
        userAccount = account;
        userIcon = iconUrl;
        userNickName = nickName;
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_give_gift, null);
        bottomDialog.setContentView(contentView);
        ViewPager vp = (ViewPager) contentView.findViewById(R.id.dialog_give_gift_vp);
        TextView tv_my_dionmads_num = (TextView) contentView.findViewById(R.id.dialog_give_gift_tv_dionmads_num);
        ImageView iv_recharge = (ImageView) contentView.findViewById(R.id.dialog_give_gift_btn_recharge);
        final TextView tv_need_pay_dionmads_num = (TextView) contentView.findViewById(R.id.dialog_give_gift_tv_need_pay_dionmads);
        final LinearLayout ll_add_num = (LinearLayout) contentView.findViewById(R.id.dialog_give_gift_ll_add_num);
        final TextView tv_multiple = (TextView) contentView.findViewById(R.id.dialog_give_gift_tv_multiple);
        final List<PresentVpRlAdapter> list_adapter = new ArrayList<>();
        final PresentVpRlAdapter presentVpRlAdapter1 = new PresentVpRlAdapter(context, R.layout.dialog_gridview_item);
        PresentVpRlAdapter presentVpRlAdapter2 = new PresentVpRlAdapter(context, R.layout.dialog_gridview_item);
        PresentVpRlAdapter presentVpRlAdapter3 = new PresentVpRlAdapter(context, R.layout.dialog_gridview_item);
        PresentVpRlAdapter presentVpRlAdapter4 = new PresentVpRlAdapter(context, R.layout.dialog_gridview_item);
        list_adapter.add(presentVpRlAdapter1);
        list_adapter.add(presentVpRlAdapter2);
        list_adapter.add(presentVpRlAdapter3);
        list_adapter.add(presentVpRlAdapter4);
        ImageView iv_give = (ImageView) contentView.findViewById(R.id.dialog_give_gift_iv_give);
        final ImageView iv_pointer0 = (ImageView) contentView.findViewById(R.id.dialog_give_gift_iv_pointer0);
        final ImageView iv_pointer1 = (ImageView) contentView.findViewById(R.id.dialog_give_gift_iv_pointer1);
        final ImageView iv_pointer2 = (ImageView) contentView.findViewById(R.id.dialog_give_gift_iv_pointer2);
        final ImageView iv_pointer3 = (ImageView) contentView.findViewById(R.id.dialog_give_gift_iv_pointer3);
        tv_my_dionmads_num.setText(PayPreference.getDionmadsNum() + "");
//        viewPager.setOverScrollMode(viewPager.OVER_SCROLL_NEVER);
        final List<DictPayGift> list_big = new ArrayList<>();
        String giftData = PayPreference.getGiftData();
        if (!TextUtils.isEmpty(giftData)) {
            GiftsDictiorary giftsDictiorary = new Gson().fromJson(giftData, GiftsDictiorary.class);
            if (giftsDictiorary != null) {
                list_big.addAll(giftsDictiorary.getGiftList());
            }
        }
        final List<DictPayGift> list_big1 = new ArrayList<>();
        final List<DictPayGift> list_big2 = new ArrayList<>();
        final List<DictPayGift> list_big3 = new ArrayList<>();
        final List<DictPayGift> list_big4 = new ArrayList<>();
        if (list_big != null && list_big.size() > 0) {
            for (int i = 0; i < 6; i++) {
                list_big1.add(list_big.get(i));
            }
            for (int i = 6; i < 12; i++) {
                list_big2.add(list_big.get(i));
            }
            for (int i = 12; i < 18; i++) {
                list_big3.add(list_big.get(i));
            }
            for (int i = 18; i < list_big.size(); i++) {
                list_big4.add(list_big.get(i));
            }
        }
        final List<List<DictPayGift>> niubi_list = new ArrayList<>();
        niubi_list.add(list_big1);
        niubi_list.add(list_big2);
        niubi_list.add(list_big3);
        niubi_list.add(list_big4);
        final List<RecyclerView> list = new ArrayList<>();
        for (int i = 0; i < 4; i++) {//动态创建RecyclerView
            RecyclerView recyclerView = new RecyclerView(context);
            recyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3);
            gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(list_adapter.get(i));
            list_adapter.get(i).bind(niubi_list.get(i));
            list.add(recyclerView);
        }
        //余额不足充值
        iv_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                CustomDialogAboutPay.purchaseDiamondShow(context);
                //由弹框变为跳到我的钻石界面
                LaunchHelper.getInstance().launch(context, MyDiamondActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 0));
                bottomDialog.dismiss();
            }
        });
        //修改倍数
        ll_add_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPoppuWindows(ll_add_num, tv_need_pay_dionmads_num, tv_multiple);
            }
        });
        //添加选中状态
        list_adapter.get(0).setSelectedPosition(0);
        tv_need_pay_dionmads_num.setText(niubi_list.get(0).get(0).getPrice() + "");
        giftsId = niubi_list.get(0).get(0).getGiftId();
        giftName = niubi_list.get(0).get(0).getGiftName();
        giftIcon = niubi_list.get(0).get(0).getGiftUrl();
        dionmadsNeed = niubi_list.get(0).get(0).getPrice();

        //礼物的点击事件
        list_adapter.get(0).setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos, RecyclerViewHolder viewHolder) {
                list_adapter.get(0).setSelectedPosition(pos);
                list_adapter.get(0).notifyDataSetChanged();
                for (int i = 1; i < 4; i++) {
                    list_adapter.get(i).setSelectedPosition(100);
                    list_adapter.get(i).notifyDataSetChanged();
                }
                dionmadsNeed = niubi_list.get(0).get(pos).getPrice();
                tv_multiple.setText("1");
                giftsId = niubi_list.get(0).get(pos).getGiftId();
                giftName = niubi_list.get(0).get(pos).getGiftName();
                giftIcon = niubi_list.get(0).get(pos).getGiftUrl();
                tv_need_pay_dionmads_num.setText(niubi_list.get(0).get(pos).getPrice() + "");
            }
        });
        //发送
        iv_give.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(giftsId)) {
                    sendGifts(guid, giftsId, tv_multiple.getText().toString(), flag, context, isVideo);
                } else {
                    Toast.makeText(context, R.string.choose_send_gift, Toast.LENGTH_SHORT).show();
                }
                bottomDialog.dismiss();
            }
        });
        //这种情况为当ViewPager在首页的时候
        ViewPagerAdapter vpAdapter = new ViewPagerAdapter(list);
        vp.setAdapter(vpAdapter);
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                //设置指示器
                setImageResource(position, iv_pointer0, iv_pointer1, iv_pointer2, iv_pointer3);
                //礼物的点击事件
                setRecyclerItemClick(position, list_adapter, niubi_list, tv_need_pay_dionmads_num, tv_multiple);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        dialogShow(context, bottomDialog, contentView, Gravity.BOTTOM);
    }

    //调用发送礼物的接口
    private static void sendGifts(final String guid, final String giftId, final String count, final int flag, final Context context, final boolean isVideo) {
        ApiManager.sendGiftMsg(guid, giftId, count, new IGetDataListener<SendGiftsType>() {
            @Override
            public void onResult(SendGiftsType sendGiftsType, boolean isEmpty) {
                if (sendGiftsType != null) {
                    int beanStatus = sendGiftsType.getBeanStatus();
                    if (beanStatus == -1) {
                        CustomDialogAboutPay.purchaseDiamondShow(context);
//                        Toast.makeText(context, "余额不足，请充值", Toast.LENGTH_SHORT).show();
                    } else if (beanStatus == 1) {
                        sendGiftsMsg(context,guid, giftId, count, isVideo);
                        PayPreference.saveDionmadsNum(sendGiftsType.getBeanCount());
                        if (flag == 1) {
                            Toast.makeText(context, R.string.gift_send_success, Toast.LENGTH_SHORT).show();
                            EventBus.getDefault().post(new GiftSendEvent());
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    private static void setPoppuWindows(LinearLayout ll_add_num, final TextView tv_need_pay_dionmads_num, final TextView tv_multiple) {
        View contentView = LayoutInflater.from(ll_add_num.getContext()).inflate(R.layout.send_gifts_popuwindow, null);
        contentView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        final PopupWindow mInstance = new PopupWindow(contentView, contentView.getMeasuredWidth(), contentView.getMeasuredHeight(), true);
        mInstance.showAsDropDown(ll_add_num, 50, 0);
        LinearLayout ll_1 = (LinearLayout) contentView.findViewById(R.id.send_gifts_popuwindow_ll1);
        LinearLayout ll_2 = (LinearLayout) contentView.findViewById(R.id.send_gifts_popuwindow_ll2);
        LinearLayout ll_3 = (LinearLayout) contentView.findViewById(R.id.send_gifts_popuwindow_ll3);
        LinearLayout ll_4 = (LinearLayout) contentView.findViewById(R.id.send_gifts_popuwindow_ll4);
        LinearLayout ll_5 = (LinearLayout) contentView.findViewById(R.id.send_gifts_popuwindow_ll5);
        LinearLayout ll_6 = (LinearLayout) contentView.findViewById(R.id.send_gifts_popuwindow_ll6);
        LinearLayout ll_7 = (LinearLayout) contentView.findViewById(R.id.send_gifts_popuwindow_ll7);
        ll_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                momentNeed = dionmadsNeed;
                momentNeed = momentNeed * 1314;
                tv_need_pay_dionmads_num.setText(momentNeed + "");
                tv_multiple.setText("1314");
                mInstance.dismiss();
            }
        });
        ll_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                momentNeed = dionmadsNeed;
                momentNeed = momentNeed * 520;
                tv_need_pay_dionmads_num.setText(momentNeed + "");
                tv_multiple.setText("520");
                mInstance.dismiss();
            }
        });
        ll_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                momentNeed = dionmadsNeed;
                momentNeed = momentNeed * 188;
                tv_multiple.setText("188");
                mInstance.dismiss();
                tv_need_pay_dionmads_num.setText(momentNeed + "");
            }
        });
        ll_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                momentNeed = dionmadsNeed;
                tv_multiple.setText("66");
                momentNeed = momentNeed * 66;
                mInstance.dismiss();
                tv_need_pay_dionmads_num.setText(momentNeed + "");
            }
        });
        ll_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                momentNeed = dionmadsNeed;
                momentNeed = momentNeed * 30;
                tv_multiple.setText("30");
                mInstance.dismiss();
                tv_need_pay_dionmads_num.setText(momentNeed + "");
            }
        });
        ll_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                momentNeed = dionmadsNeed;
                momentNeed = momentNeed * 10;
                tv_multiple.setText("10");
                mInstance.dismiss();
                tv_need_pay_dionmads_num.setText(momentNeed + "");
            }
        });
        ll_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                momentNeed = dionmadsNeed;
                momentNeed = momentNeed * 1;
                tv_multiple.setText("1");
                mInstance.dismiss();
                tv_need_pay_dionmads_num.setText(momentNeed + "");
            }
        });
    }

    private static void setRecyclerItemClick(final int position, final List<PresentVpRlAdapter> list_adapter, final List<List<DictPayGift>> niubi_list, final TextView tv_need_pay_dionmads_num, final TextView tv_multiple) {
        list_adapter.get(position).setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos, RecyclerViewHolder viewHolder) {
                list_adapter.get(position).setSelectedPosition(pos);
                list_adapter.get(position).notifyDataSetChanged();
                dionmadsNeed = niubi_list.get(position).get(pos).getPrice();
                tv_multiple.setText("1");
                giftsId = niubi_list.get(position).get(pos).getGiftId();
                giftName = niubi_list.get(position).get(pos).getGiftName();
                giftIcon = niubi_list.get(position).get(pos).getGiftUrl();
                tv_need_pay_dionmads_num.setText(niubi_list.get(position).get(pos).getPrice() + "");
                switch (position) {
                    case 0:
                        list_adapter.get(0).setSelectedPosition(pos);
                        list_adapter.get(0).notifyDataSetChanged();
                        list_adapter.get(1).setSelectedPosition(100);
                        list_adapter.get(1).notifyDataSetChanged();
                        list_adapter.get(2).setSelectedPosition(100);
                        list_adapter.get(2).notifyDataSetChanged();
                        list_adapter.get(3).setSelectedPosition(100);
                        list_adapter.get(3).notifyDataSetChanged();
                        break;
                    case 1:
                        list_adapter.get(0).setSelectedPosition(100);
                        list_adapter.get(0).notifyDataSetChanged();
                        list_adapter.get(1).setSelectedPosition(pos);
                        list_adapter.get(1).notifyDataSetChanged();
                        list_adapter.get(2).setSelectedPosition(100);
                        list_adapter.get(2).notifyDataSetChanged();
                        list_adapter.get(3).setSelectedPosition(100);
                        list_adapter.get(3).notifyDataSetChanged();
                        break;
                    case 2:
                        list_adapter.get(0).setSelectedPosition(100);
                        list_adapter.get(0).notifyDataSetChanged();
                        list_adapter.get(1).setSelectedPosition(100);
                        list_adapter.get(1).notifyDataSetChanged();
                        list_adapter.get(2).setSelectedPosition(pos);
                        list_adapter.get(2).notifyDataSetChanged();
                        list_adapter.get(3).setSelectedPosition(100);
                        list_adapter.get(3).notifyDataSetChanged();
                        break;
                    case 3:
                        list_adapter.get(0).setSelectedPosition(100);
                        list_adapter.get(0).notifyDataSetChanged();
                        list_adapter.get(1).setSelectedPosition(100);
                        list_adapter.get(1).notifyDataSetChanged();
                        list_adapter.get(2).setSelectedPosition(100);
                        list_adapter.get(2).notifyDataSetChanged();
                        list_adapter.get(3).setSelectedPosition(pos);
                        list_adapter.get(3).notifyDataSetChanged();
                        break;
                }
            }
        });
    }

    private static void setImageResource(int position, ImageView iv_pointer0, ImageView iv_pointer1, ImageView iv_pointer2, ImageView iv_pointer3) {
        switch (position) {
            case 0:
                iv_pointer0.setImageResource(R.drawable.pink_icon);
                iv_pointer1.setImageResource(R.drawable.gray_icon);
                iv_pointer2.setImageResource(R.drawable.gray_icon);
                iv_pointer3.setImageResource(R.drawable.gray_icon);
                break;
            case 1:
                iv_pointer0.setImageResource(R.drawable.gray_icon);
                iv_pointer1.setImageResource(R.drawable.pink_icon);
                iv_pointer2.setImageResource(R.drawable.gray_icon);
                iv_pointer3.setImageResource(R.drawable.gray_icon);
                break;
            case 2:
                iv_pointer0.setImageResource(R.drawable.gray_icon);
                iv_pointer1.setImageResource(R.drawable.gray_icon);
                iv_pointer2.setImageResource(R.drawable.pink_icon);
                iv_pointer3.setImageResource(R.drawable.gray_icon);
                break;
            case 3:
                iv_pointer0.setImageResource(R.drawable.gray_icon);
                iv_pointer1.setImageResource(R.drawable.gray_icon);
                iv_pointer2.setImageResource(R.drawable.gray_icon);
                iv_pointer3.setImageResource(R.drawable.pink_icon);
                break;
        }
    }

    //对话框初始化显示的方法
    private static void dialogShow(Context context, Dialog bottomDialog, View contentView, int location) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = context.getResources().getDisplayMetrics().widthPixels - dp2px(context, 16f);
        params.bottomMargin = dp2px(context, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.getWindow().setGravity(location);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    //对话框初始化显示的方法 点击其他位置不消失
    private static void dialogShow2(Context context, Dialog bottomDialog, View contentView, int location) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = context.getResources().getDisplayMetrics().widthPixels - dp2px(context, 16f);
        params.bottomMargin = dp2px(context, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(false);
        bottomDialog.setCancelable(false);
        bottomDialog.getWindow().setGravity(location);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    //取消对话框的显示
    private static void cancelDialog(final Dialog bottomDialog, ImageView iv_dismiss) {
        iv_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
    }

    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal,
                context.getResources().getDisplayMetrics());
    }

    //调用环信发送礼物成功的信息
    public static void sendGiftsMsg(Context context,final String guid, String giftsId, String num, boolean isVideo) {
        PicInfo picInfo = new PicInfo(giftsId, giftName, "price", num, giftIcon);
        HyphenateHelper.getInstance().sendTextMessage(userAccount, new Gson().toJson(picInfo),
                new HyphenateHelper.OnMessageSendListener() {
                    @Override
                    public void onSuccess(EMMessage emMessage) {
                        EventBus.getDefault().post(new MessageArrive(guid));//发送完成之后更新聊天列表
                    }

                    @Override
                    public void onError() {
                    }
                });
        if (isVideo) {
            UserPreference.saveGiftsContent(new Gson().toJson(new PicInfoData(0, picInfo)));
            AgoraHelper.getInstance().sendTextMessage(userAccount, new Gson().toJson(new PicInfoData(0, picInfo)));
        }
        saveDataToSqlit(userNickName, guid, userAccount, userIcon, context.getString(R.string.gift), String.valueOf(System.currentTimeMillis()), 0);
    }

    public static void saveDataToSqlit(String name, String id, String account, String pic, String msg, String time, int extendType) {
        if (!TextUtils.isEmpty(id)) {
            HuanXinUser user = new HuanXinUser(id, name, pic, account, "1", msg, 0, time, extendType);
            DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
        }
    }

    //发送QA信息
    private static void qaAnaswerQuestion(String answerId, String content, String remoteUid) {
        ApiManager.qaQaswer(answerId, content, remoteUid, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    //更新换新消息
    private static void updataHyXin(final String userAccount, String content, final String guid) {
        HyphenateHelper.getInstance().sendTextMessage(userAccount, content,
                new HyphenateHelper.OnMessageSendListener() {
                    @Override
                    public void onSuccess(EMMessage emMessage) {
                        EventBus.getDefault().post(new MessageArrive(guid));//发送完成之后更新聊天列表
                    }

                    @Override
                    public void onError() {
                    }
                });
        ApiManager.interruptText(Long.parseLong(guid), content, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
}
