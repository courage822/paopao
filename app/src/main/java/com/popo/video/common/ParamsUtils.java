package com.popo.video.common;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/7/28.
 */

public class ParamsUtils {
    /**
     * 将map的key排序后，生成String
     *
     * @param map
     */
    private static String getMapValue(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        String value = iterator.next().getValue();
        if (!TextUtils.isEmpty(value)) {
            return value;
        } else {

            return null;
        }
    }

    /**
     * 将map的valuse排序后，生成String
     *
     * @param map
     */
    private static String getMapKey(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        String key = iterator.next().getKey();
        if (!TextUtils.isEmpty(key)) {
            return key;
        } else {

            return null;
        }
    }

    /**
     * 从map集合中获取listist集合生成list
     */
    public static List<String> getMapListString(List<Map<String, String>> list) {
        List<String> list2 = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Map<String, String> stringStringMap = list.get(i);
            String mapValue = getMapValue(stringStringMap);
            list2.add(mapValue);
        }
        return list2;
    }


    /**
     * 根据map集合的value获取州的key
     */
    public static String getStateKey(String sate) {
        String mapKeyByValue = null;
        List<Map<String, String>> countryString = Util.getStateString();
        for (int i = 0; i < countryString.size(); i++) {
            Map<String, String> stringStringMap = countryString.get(i);
            Iterator<Map.Entry<String, String>> iterator = stringStringMap.entrySet().iterator();
            if (getMapValue(stringStringMap).equals(sate)) {
                mapKeyByValue = iterator.next().getKey();
            }
        }
        return mapKeyByValue;
    }

    /**
     * 根据map集合的key获取州的value
     */
    public static String getStateValue(String sate) {
        String mapKeyByValue = null;
        List<Map<String, String>> countryString = Util.getStateString();
        for (int i = 0; i < countryString.size(); i++) {
            Map<String, String> stringStringMap = countryString.get(i);
            Iterator<Map.Entry<String, String>> iterator = stringStringMap.entrySet().iterator();
            if (getMapKey(stringStringMap).equals(sate)) {
                mapKeyByValue = iterator.next().getValue();
            }
        }
        return mapKeyByValue;
    }

    /**
     * 根据map集合的value获取对应的key
     */
    public static String getSignkey(String value) {
        String mapKeyByValue = null;
        List<Map<String, String>> countryString = Util.getSignString();
        for (int i = 0; i < countryString.size(); i++) {
            Map<String, String> stringStringMap = countryString.get(i);
            Iterator<Map.Entry<String, String>> iterator = stringStringMap.entrySet().iterator();
            if (getMapValue(stringStringMap).equals(value)) {
                mapKeyByValue = iterator.next().getKey();
            }
        }
        return mapKeyByValue;
    }

    /**
     * 根据map集合的key获星座的value
     */
    public static String getSignValue(String sate) {
        String mapKeyByValue = null;
        List<Map<String, String>> countryString = Util.getSignString();
        for (int i = 0; i < countryString.size(); i++) {
            Map<String, String> stringStringMap = countryString.get(i);
            for (Map.Entry<String, String> entry : stringStringMap.entrySet()) {
                if (entry.getKey().equals(sate)) {
                    mapKeyByValue = entry.getValue();
                }
            }
        }
        return mapKeyByValue;
    }

    /**
     * 把带有逗号的String字符串转成List集合
     */
    public static List<String> getLanguageList(String languagekey) {
        List<String> list = new ArrayList<>();
        String[] split = languagekey.split(",");
        for (int i = 0; i < split.length; i++) {
            list.add(split[i]);
        }
        return list;
    }
}
