package com.popo.video.common;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.annotation.IdRes;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.popo.video.C;
import com.popo.video.R;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.HostAutoReplyBean;
import com.popo.video.event.ReplyEvent;
import com.popo.library.util.FileUtil;
import com.popo.library.util.ToastUtil;
import com.popo.library.util.Utils;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by WangYong on 2017/9/5.
 */

public class AutoReplyDialog {
    ImageView iv_dismiss, iv_switch, iv_sound_voice;
    LinearLayout ll_record_voice,ll_dialog;
    TextView tv_time_voice,tv_number;
    FrameLayout framelayout_et;
    Button btn_record, btn_sure;
    EditText et_msg;
    RadioGroup radioGroup;
    RadioButton rb1, rb2;


    private static AnimationDrawable animationDrawable;
    private static AnimationDrawable soundanimationDrawable;
    // 录音文件存放目录
    private String mRecordDirectory;
    // 录音文件路径
    private String mRecordOutputPath;
    // 是否正在录音
    private boolean mIsRecording;
    // 录音是否被取消
    private boolean mIsRecordCanceled;
    // 录音时长（毫秒）
    private long mRecordDuration;
    // 录音计时刷新间隔（毫秒）
    private final long mRefreshInterval = 250;
    PopupWindow mRecordPopupWindow;

    private Context context;
    private int checkItem = 1;

    // 异步任务
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == C.message.MSG_TYPE_VOICE_UI_TIME) {
                mRecordDuration++;
                mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_VOICE_UI_TIME, 1_000);
            } else {
                handleAsyncTask(msg);
            }
        }
    };

    private void handleAsyncTask(Message msg) {
        switch (msg.what) {
            case C.message.MSG_TYPE_TIMER:// 计时
                if (mIsRecording) {
                    mRecordDuration += mRefreshInterval;
                    // 录音超过1分钟自动发送
//                    if (mRecordDuration > 60_000) {
//                        stopRecordAndSend();
//                    } else {
                        mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_TIMER, mRefreshInterval);
//                    }
                }
                break;
            case C.message.MSG_TYPE_DELAYED:// 延时结束录音
                RecordUtil.getInstance().stopRecord();
                mRecordDuration = 0;
                break;

            case C.message.MSG_TYPE_SEND_TXT://发送文字

                break;
            case C.message.MSG_TYPE_SEND_VOICE://发送语音

                break;
            case C.message.MSG_TYPE_UPDATE:// 更新消息

                break;
        }
    }

    public void setContext(Context context) {
        this.context = context;
        if (null == mRecordPopupWindow) {
            View contentView = LayoutInflater.from(context).inflate(R.layout.popup_chat_record, null);
            mRecordPopupWindow = new PopupWindow(contentView, WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT, true);
            // 设置背景透明
            mRecordPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mRecordPopupWindow.setClippingEnabled(true);
        }
        mRecordDirectory = FileUtil.getExternalFilesDir(context, Environment.DIRECTORY_MUSIC) + File.separator;
    }

    public void setCheckItem(int item){
        this.checkItem = item;
    }


    //录音对话框
    public void recordAndMsgShow() {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        final View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_record_and_msg, null);
        bottomDialog.setContentView(contentView);
        iv_dismiss = (ImageView) contentView.findViewById(R.id.recordAndMsg_dialog_iv_dismiss);
        ll_record_voice = (LinearLayout) contentView.findViewById(R.id.ll_record_voice);
        ll_dialog = (LinearLayout) contentView.findViewById(R.id.ll_dialog);
        iv_sound_voice = (ImageView) contentView.findViewById(R.id.iv_sound_voice);
        tv_time_voice = (TextView) contentView.findViewById(R.id.tv_time_voice);
        iv_switch = (ImageView) contentView.findViewById(R.id.record_msg_chat_switch);
        btn_record = (Button) contentView.findViewById(R.id.record_msg_chat_voice);
        et_msg = (EditText) contentView.findViewById(R.id.record_msg_text_input);
        radioGroup = (RadioGroup) contentView.findViewById(R.id.record_msg_rg);
        rb1 = (RadioButton) contentView.findViewById(R.id.record_msg_rb1);
        rb2 = (RadioButton) contentView.findViewById(R.id.record_msg_rb2);
        btn_sure = (Button) contentView.findViewById(R.id.record_msg_btn_sure);
        framelayout_et = (FrameLayout) contentView.findViewById(R.id.framelayout_et);
        tv_number = (TextView) contentView.findViewById(R.id.tv_number);
        if (checkItem ==1){
            rb1.setChecked(true);
        }else{
            rb2.setChecked(true);
        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.record_msg_rb1:
                        rb1.setChecked(true);
                        rb2.setChecked(false);
                        break;
                    case R.id.record_msg_rb2:
                        rb1.setChecked(false);
                        rb2.setChecked(true);
                        break;
                }
            }
        });

        ll_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)
                        context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });

        et_msg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tv_number.setText(charSequence.length() + "/40");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        iv_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_switch.setSelected(!iv_switch.isSelected());
                if (iv_switch.isSelected()) {
                    btn_record.setVisibility(View.VISIBLE);
                    btn_record.setText(context.getString(R.string.chat_press_to_speak));
                    framelayout_et.setVisibility(View.GONE);
                    et_msg.setEnabled(false);
                } else {
                    framelayout_et.setVisibility(View.VISIBLE);
                    et_msg.setEnabled(true);
                    btn_record.setVisibility(View.GONE);
                    ll_record_voice.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(mRecordOutputPath)) {
                        File file = new File(mRecordOutputPath);
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                }
            }
        });
        ll_record_voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startVoiceAnimation(iv_sound_voice, context);
                // 播放动画
                if (soundanimationDrawable != null) {
                    soundanimationDrawable.start();
                }
                RecordUtil.getInstance().play(mRecordOutputPath, new RecordUtil.OnPlayerListener() {
                    @Override
                    public void onCompleted() {//播放完成，停止动画
                        soundanimationDrawable.stop();
                    }

                    @Override
                    public void onPaused() {
                        soundanimationDrawable.stop();
                    }
                });
            }
        });


        btn_record.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:// 按下时开始录音
                        btn_record.setText(R.string.chat_loose_to_end);
                        showRecordPopupWindow(C.message.STATE_RECORDING, mRecordPopupWindow, context);
                        startRecord();
                        mIsRecordCanceled = false;
                        // 开始计时
                        mRecordDuration = 0;
                        // 播放动画
                        if (animationDrawable != null) {
                            animationDrawable.start();
                        }
                        mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_VOICE_UI_TIME, 1000);
                        break;

                    case MotionEvent.ACTION_MOVE:// 判断是否上滑取消
                        if (animationDrawable != null) {
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        handleTouchEventMove(event, ll_dialog);
                        break;

                    case MotionEvent.ACTION_UP:// 松开时停止录音并发送
                        btn_record.setText(context.getString(R.string.chat_press_to_speak));
                        showRecordPopupWindow(C.message.STATE_IDLE, mRecordPopupWindow, context);
                        if (null != mRecordPopupWindow && mRecordPopupWindow.isShowing()) {
                            mRecordPopupWindow.dismiss();
                        }

                        if (mIsRecordCanceled) {
                            cancelRecord();
                        } else {
                            stopRecordAndSend();
                        }
                        if (animationDrawable != null) {
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        break;
                }
                return true;
            }
        });
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (iv_switch.isSelected()){//语音
                    if (mRecordOutputPath != null){//有路径时，说明已经路过音了
                        File file = null;
                        try {
                            file = new File(mRecordOutputPath);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        if (file.exists()){//文件存在
                            ApiManager.uploadSoundAutoReply(rb1.isChecked() ? "1" : "2", String.valueOf(mRecordDuration / 1000), file, new IGetDataListener<HostAutoReplyBean>() {
                                @Override
                                public void onResult(HostAutoReplyBean baseModel, boolean isEmpty) {
                                    if (baseModel.getIsSucceed().equals("1")){//成功
                                        EventBus.getDefault().post(new ReplyEvent());
                                        bottomDialog.dismiss();
                                    }else if (baseModel.getIsSucceed().equals("-1.1")){//自动回复条目超过上限
                                        ToastUtil.showShortToast(context,baseModel.getMsg());
                                    }
                                }

                                @Override
                                public void onError(String msg, boolean isNetworkError) {
                                    ToastUtil.showShortToast(context,msg);
                                }
                            });
                        }else{
                            ToastUtil.showShortToast(context,context.getString(R.string.dialog_hint_ask_gift));
                        }
                    }else{
                        ToastUtil.showShortToast(context,context.getString(R.string.dialog_hint_ask_gift));

                    }
                }else{//文字
                    if (!TextUtils.isEmpty(et_msg.getText().toString().trim())){
                        Log.e("uptext",et_msg.getText().toString().trim());
                        ApiManager.uploadTextAutoReply(rb1.isChecked() ? "1" : "2", et_msg.getText().toString().trim(), new IGetDataListener<HostAutoReplyBean>() {
                            @Override
                            public void onResult(HostAutoReplyBean baseModel, boolean isEmpty) {
                                if (baseModel.getIsSucceed().equals("1")){//成功
                                    EventBus.getDefault().post(new ReplyEvent());
                                    bottomDialog.dismiss();
                                }else if (baseModel.getIsSucceed().equals("-11")){//自动回复条目超过上限
                                    ToastUtil.showShortToast(context,baseModel.getMsg());
                                }
                            }

                            @Override
                            public void onError(String msg, boolean isNetworkError) {
                                ToastUtil.showShortToast(context,msg);
                            }
                        });
                    }else{
                        ToastUtil.showShortToast(context,context.getString(R.string.dialog_hint_ask_gift));
                    }
                }
            }
        });

        cancelDialog(bottomDialog, iv_dismiss);
        dialogShow(context, bottomDialog, contentView, Gravity.CENTER);
    }

    /**
     * 播放录音动画
     *
     * @param iv_sound_voice
     * @param context
     */
    private void startVoiceAnimation(ImageView iv_sound_voice, Context context) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_left1));
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_left2));
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_left3));
        soundanimationDrawable = Utils.getFrameAnim(drawableList, true, 150);
        iv_sound_voice.setImageDrawable(soundanimationDrawable);
    }

    private void startRecord() {
        // 录音开始前震动一下
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(88);
        // 根据系统时间生成文件名
        String recordFileName = FileUtil.createFileNameByTime() + ".aac";
        // 录音文件临时保存路径：data下包名music目录
        mRecordOutputPath = mRecordDirectory + recordFileName;
        // 开始录音
        RecordUtil.getInstance().startRecord(mRecordOutputPath);
        mIsRecording = true;
        mRecordDuration = 0;
        // 计时，间隔250毫秒刷新
        mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_TIMER, mRefreshInterval);
    }

    private void cancelRecord() {
        if (mIsRecordCanceled) {
            RecordUtil.getInstance().stopRecord();
            mIsRecordCanceled = false;
            FileUtil.deleteFile(mRecordOutputPath);
        }
    }

    private void stopRecordAndSend() {
        if (mIsRecording) {
            mIsRecording = false;
            if (mRecordDuration < 2000 || mRecordDuration >60000) {// 录音时长小于2秒或大于60秒的不发送
                // 删除文件并提示
                FileUtil.deleteFile(mRecordOutputPath);
                ToastUtil.showShortToast(context,context.getString(R.string.voice_limit));
            } else {// 发送语音
                RecordUtil.getInstance().stopRecord();
                btn_record.setVisibility(View.GONE);
                ll_record_voice.setVisibility(View.VISIBLE);
                tv_time_voice.setText(mRecordDuration/1000 + context.getString(R.string.second));
            }
        }
    }

    private void handleTouchEventMove(MotionEvent event, LinearLayout ll_dialog) {
        if (event.getY() < -100) { // 上滑取消发送
            mIsRecordCanceled = true;
            showRecordPopupWindow(C.message.STATE_CANCELED, mRecordPopupWindow, context);
        } else {
            mIsRecordCanceled = false;
            showRecordPopupWindow(C.message.STATE_RECORDING, mRecordPopupWindow, context);
        }
    }

    private void showRecordPopupWindow(int state, PopupWindow mRecordPopupWindow, Context context) {
        if (null != mRecordPopupWindow) {
            ImageView iv_animation = (ImageView) mRecordPopupWindow.getContentView().findViewById(R.id.popup_record_iv_duration);
            startAnimation(iv_animation, context);
        }
        View view = mRecordPopupWindow.getContentView();
        if (null != view) {
            // 录音中
            LinearLayout llRecording = (LinearLayout) view.findViewById(R.id.popup_recording_container);
            // 上滑取消
            LinearLayout ll_cancle = (LinearLayout) view.findViewById(R.id.popup_recording_container_cancle);
            switch (state) {
                case C.message.STATE_RECORDING: // 正在录音
                    llRecording.setVisibility(View.VISIBLE);
                    ll_cancle.setVisibility(View.GONE);
                    break;

                case C.message.STATE_CANCELED: // 取消录音
                    llRecording.setVisibility(View.GONE);
                    ll_cancle.setVisibility(View.VISIBLE);
                    break;

                case C.message.STATE_IDLE:// 录音结束
                    llRecording.setVisibility(View.GONE);
                    ll_cancle.setVisibility(View.GONE);
                    break;
            }
        }
        // 居中显示
        mRecordPopupWindow.showAtLocation(ll_dialog, Gravity.TOP, 0, 0);
    }

    private static void startAnimation(ImageView iv_animation, Context context) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
        drawableList.add(context.getResources().getDrawable(R.drawable.msg_record_voice_1));
        drawableList.add(context.getResources().getDrawable(R.drawable.msg_record_voice_2));
        drawableList.add(context.getResources().getDrawable(R.drawable.msg_record_voice_3));
        drawableList.add(context.getResources().getDrawable(R.drawable.msg_record_voice_4));
        animationDrawable = Utils.getFrameAnim(drawableList, true, 150);
        iv_animation.setImageDrawable(animationDrawable);
    }


    //对话框初始化显示的方法
    private static void dialogShow(Context context, Dialog bottomDialog, View contentView, int location) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = context.getResources().getDisplayMetrics().widthPixels - dp2px(context, 16f);
        params.bottomMargin = dp2px(context, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(false);
        bottomDialog.getWindow().setGravity(location);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    //取消对话框的显示
    private static void cancelDialog(final Dialog bottomDialog, ImageView iv_dismiss) {
        iv_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
    }

    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal,
                context.getResources().getDisplayMetrics());
    }
}
