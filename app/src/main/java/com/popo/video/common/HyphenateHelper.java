package com.popo.video.common;

import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.hyphenate.EMCallBack;
import com.hyphenate.EMConnectionListener;
import com.hyphenate.EMError;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMOptions;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.exceptions.HyphenateException;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.NotificationHelper;
import com.popo.video.R;
import com.popo.video.base.BaseApplication;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.model.NettyMessage;
import com.popo.video.data.model.PushMessage;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.db.DbModle;
import com.popo.video.event.MessageArrive;
import com.popo.video.event.UnreadMsgChangedEvent;
import com.popo.video.event.YeMeiMessageEvent;
import com.popo.video.parcelable.ChatParcelable;
import com.popo.video.parcelable.VideoInviteParcelable;
import com.popo.video.ui.chat.ChatActivity;
import com.popo.video.ui.main.MainActivity;
import com.popo.video.ui.video.VideoInviteActivity2;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Map;

/**
 * 环信帮助类（环信注册由后台完成，在调用注册接口时后台直接注册环信，返回用户account和密码）
 * Created by zhangdroid on 2017/6/29.
 */
public class HyphenateHelper {
    private static final String TAG = HyphenateHelper.class.getSimpleName();
    private static HyphenateHelper sInstance;
    // 环信消息监听器
    private EMMessageListener mMessageListener;
    private boolean isChatActivity = true;
    private String huanxinId = "";

    private HyphenateHelper() {
    }

    /**
     * 环信登录回调
     */
    public interface OnLoginCallback {
        /**
         * 环信登录成功
         */
        void onSuccess();

        /**
         * 环信登录失败
         */
        void onFailed();
    }

    public static HyphenateHelper getInstance() {
        if (null == sInstance) {
            synchronized (HyphenateHelper.class) {
                if (null == sInstance) {
                    sInstance = new HyphenateHelper();
                }
            }
        }
        return sInstance;
    }

    /**
     * 消息发送监听器
     */
    public interface OnMessageSendListener {
        /**
         * 发送成功
         *
         * @param emMessage 发送的消息对象
         */
        void onSuccess(EMMessage emMessage);

        /**
         * 发送失败
         */
        void onError();
    }

    /**
     * 初始化环信
     */
    public void init(Context context) {
        Log.i(TAG, "init");
        EMOptions emOptions = new EMOptions();
        emOptions.setSortMessageByServerTime(true);
        emOptions.setRequireAck(false);
        // GCM
        emOptions.setGCMNumber("");
//        emOptions.setMipushConfig();
        EMClient.getInstance().init(context.getApplicationContext(), emOptions);
        // 开启debug模式，上线的时候需要关闭
        EMClient.getInstance().setDebugMode(true);
    }

    /**
     * 给环信设置监听
     */
    public void setOnClick() {
        // 设置连接监听
        setConnectionListener();
        // 设置消息监听
        setGlobalMessageListener();
    }

    /**
     * 登录
     *
     * @param account  用户account
     * @param password 密码
     */
    public void login(String account, String password, final OnLoginCallback onLoginCallback) {
        if (isLoggedIn()) {
            return;
        }
        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(password)) {
            if (null != onLoginCallback) {
                onLoginCallback.onFailed();
            }
            return;
        }
        EMClient.getInstance().login(account, password, new EMCallBack() {
            @Override
            public void onSuccess() {
                Log.e("AAAAAAA", "onSuccess: 33333333333333333333333333333333333333333");
                // 登录成功后加载聊天会话
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        EMClient.getInstance().chatManager().loadAllConversations();
                    }
                }).start();
                if (null != onLoginCallback) {
                    onLoginCallback.onSuccess();
                }
            }

            @Override
            public void onError(int code, String error) {
                Log.e("AAAAAAA", "onError: 环信登录出现的问题::" + code + "----" + error);
                if (null != onLoginCallback) {
                    onLoginCallback.onFailed();
                }
            }

            @Override
            public void onProgress(int progress, String status) {
            }
        });
    }

    /**
     * 登出
     */
    public void logout() {
        // 绑定了GCM，参数必须为true，登出后会解绑GCM
        EMClient.getInstance().logout(true);
        // 移除消息接收监听
        removeMessageListener(mMessageListener);
    }

    /**
     * @return 是否登录过
     */
    public boolean isLoggedIn() {
        return EMClient.getInstance().isLoggedInBefore();
    }

    /**
     * 设置连接状态监听
     */
    private void setConnectionListener() {
        EMClient.getInstance().addConnectionListener(new EMConnectionListener() {
            @Override
            public void onConnected() {
                Log.i(TAG, "onConnected");
            }

            @Override
            public void onDisconnected(int errorCode) {
                if (errorCode == EMError.USER_REMOVED) {
                    // 帐号已经被移除
                } else if (errorCode == EMError.USER_LOGIN_ANOTHER_DEVICE) {
                    // 帐号在其他设备登录
                    if (EMClient.getInstance().isLoggedInBefore()) {
                        logout();
                    }
                }
            }
        });
    }

    /**
     * 设置全局消息监听
     */
    private void setGlobalMessageListener() {
        mMessageListener = new EMMessageListener() {
            @Override
            public void onMessageReceived(List<EMMessage> messages) {
                // 接收到新消息
                HuanXinUser user = null;
                for (EMMessage emMessage : messages) {
                    String userName = "";
                    String userPic = "";
                    String userAccount = "";
                    String userGuid = "";
                    String msgContent = "";
                    int extendType;
                    int notice = 0;
                    msgContent = emMessage.getStringAttribute("msg", "");
                    Log.e("msgcontent", msgContent);
                    if (!TextUtils.isEmpty(msgContent)) {
                        NettyMessage nettyMessage = setMsgContent(msgContent);
                        if (nettyMessage != null) {
                            Context context = BaseApplication.getGlobalContext();
                            userName = nettyMessage.getSendUserName();
                            userPic = nettyMessage.getSendUserIcon();
                            userAccount = nettyMessage.getSendUserAccount();
                            userGuid = String.valueOf(nettyMessage.getSendUserId());
                            extendType = nettyMessage.getExtendType();
                            notice = nettyMessage.getNotice();
                            if(Util.getLacalLanguage().equals("English")&&userAccount.equals("10000")){
                                userName="Custom service";
                            }else if(Util.getLacalLanguage().equals("Simplified")&&userAccount.equals("10000")){
                                userName="系统用户";
                            }else if(Util.getLacalLanguage().equals("Traditional")&&userAccount.equals("10000")){
                                userName="系統秘書";
                            }
                            EventBus.getDefault().post(new YeMeiMessageEvent(userGuid,userName,0,userPic,userAccount));
                            String lastMsg = context.getString(R.string.how_are_you);
                            if (emMessage.getType() == EMMessage.Type.TXT) {
                                EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessage.getBody();
                                if (nettyMessage.getExtendType() == 2 || emTextMessageBody.getMessage().equals("msgCall")) {
                                    lastMsg = context.getString(R.string.video_invitations);
                                } else if (nettyMessage.getExtendType() == 3) {
                                    lastMsg = context.getString(R.string.one_private_message);
                                } else if (nettyMessage.getExtendType() == 5) {
                                    lastMsg = context.getString(R.string.gift);
                                } else {
                                    lastMsg = emTextMessageBody.getMessage();
                                }
                            } else if (emMessage.getType() == EMMessage.Type.VOICE) {
                                lastMsg = context.getString(R.string.voice_message);
                            } else if (emMessage.getType() == EMMessage.Type.IMAGE) {
                                lastMsg = context.getString(R.string.photo_message);
                            }

                            if (isChatActivity) {
                                if (notice == 1) {
                                    showNotification(emMessage);
                                    wakeUpAndUnlock(BaseApplication.getGlobalContext());
                                }
                            }

                            user = new HuanXinUser(userGuid, userName, userPic, userAccount, "1", lastMsg, 0, String.valueOf(emMessage.getMsgTime()), extendType);
                            huanxinId = userGuid;
                            if (!TextUtils.isEmpty(huanxinId)) {
                                EMConversation conversation = EMClient.getInstance().chatManager().getConversation(userAccount);
                                if (conversation != null) {
                                    user.setMsgNum(conversation.getUnreadMsgCount());
                                }
                            }
                            DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
                            if (!TextUtils.isEmpty(nettyMessage.getMsgId())) {
                                msgReceived(nettyMessage.getMsgId());
                            }

                        }
                        //消息过来时刷新聊天列表
                        EventBus.getDefault().post(new MessageArrive(huanxinId));
                        EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                    }
                }


            }

            @Override
            public void onCmdMessageReceived(List<EMMessage> messages) {
                for (EMMessage message : messages) {
                    try {
                        String msg = message.getStringAttribute("msg").toString();
                        if (!TextUtils.isEmpty(msg)) {
                            NettyMessage pushMessage = new Gson().fromJson(msg, NettyMessage.class);
                            if (pushMessage != null) {
                                if (pushMessage.getType() == 5){//匹配成功
                                    String userName = pushMessage.getSendUserName();
                                    String userPic = pushMessage.getSendUserImageUrl();
                                    String userAccount = pushMessage.getSendUserAccount();
                                    String userGuid = String.valueOf(pushMessage.getSendUserId());
                                    HuanXinUser user = new HuanXinUser(userGuid,userName,userPic,
                                            userAccount,"1",BaseApplication.getGlobalContext().getString(R.string.match_success),
                                            0,pushMessage.getSendTime(),10);
                                    DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
                                    EventBus.getDefault().post(new MessageArrive(String.valueOf(pushMessage.getSendUserId())));
                                    EventBus.getDefault().post(new YeMeiMessageEvent(userGuid,userName,1,userPic,userAccount));
                                }

                                if (pushMessage.getExtendType() == 7) {//页脚的策略信息
                                    DataPreference.saveYeJiaoJsonData(msg);//保存透传的json数据
                                    DataPreference.saveYeJiaoTime(System.currentTimeMillis());//保存透传信息过来时的时间
                                    EventBus.getDefault().post(pushMessage);
                                }
                                if (pushMessage.getBaseType() == 1) {
                                    if (pushMessage.getExtendType() == 10) {//群发视频的呼叫信
                                        if (Long.valueOf(DataUtil.getTime()) - Long.valueOf(DataUtil.formatDate(pushMessage.getSendTime())) <= 110000) {
                                            VideoInviteParcelable videoInviteParcelable = new VideoInviteParcelable(true, pushMessage.getSendUserId(), pushMessage.getSendUserAccount(), pushMessage.getSendUserName(), pushMessage.getSendUserIcon(), 0, 0);
                                            videoInviteParcelable.setChannelId(String.valueOf(pushMessage.getSendUserId()) + String.valueOf(pushMessage.getRecvUserId()));
                                            LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                                            wakeUpAndUnlock(BaseApplication.getGlobalContext());
                                        }
                                    }
                                    if (pushMessage.getExtendType() == 11) {//群发语音的呼叫信
                                        if (Long.valueOf(DataUtil.getTime()) - Long.valueOf(DataUtil.formatDate(pushMessage.getSendTime())) <= 110000) {
                                            VideoInviteParcelable videoInviteParcelable = new VideoInviteParcelable(true, pushMessage.getSendUserId(), pushMessage.getSendUserAccount(), pushMessage.getSendUserName(), pushMessage.getSendUserIcon(), 1, 0);
                                            videoInviteParcelable.setChannelId(String.valueOf(pushMessage.getSendUserId()) + String.valueOf(pushMessage.getRecvUserId()));
                                            LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                                            wakeUpAndUnlock(BaseApplication.getGlobalContext());
                                        }
                                    }
                                }
                            }
                        }
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                    }

                }
            }

            /**
             * 客户端收到消息后，向服务器回执，确认已送达
             * @param msgId
             */
            private void msgReceived(String msgId) {
                ApiManager.msgReceived(msgId, new IGetDataListener<BaseModel>() {
                    @Override
                    public void onResult(BaseModel baseModel, boolean isEmpty) {

                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {

                    }
                });
            }

            /**
             * 锁屏状态下点亮屏幕
             */
            private void wakeUpAndUnlock(Context context) {
                //屏锁管理器
                KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
                KeyguardManager.KeyguardLock kl = km.newKeyguardLock("unLock");
                //解锁
                kl.disableKeyguard();
                //获取电源管理器对象
                PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                //获取PowerManager.WakeLock对象,后面的参数|表示同时传入两个值,最后的是LogCat里用的Tag
                PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_DIM_WAKE_LOCK, "bright");
                //点亮屏幕
                wl.acquire();
                //释放
                wl.release();
            }

            @Override
            public void onMessageRead(List<EMMessage> messages) {
                Log.e("AAAAAAA", "onMessageRead: " + messages.size());
            }

            @Override
            public void onMessageDelivered(List<EMMessage> messages) {
                Log.e("AAAAAAA", "onMessageDelivered: " + messages.size());
            }

            @Override
            public void onMessageRecalled(List<EMMessage> list) {

            }

            @Override
            public void onMessageChanged(EMMessage message, Object change) {
                Log.e("AAAAAAA", "onMessageChanged: 消息发生变动");
            }
        };
        setMessageListener(mMessageListener);
    }

    //解析发过来的数据
    private NettyMessage setMsgContent(String msgContent) {
        if (!TextUtils.isEmpty(msgContent)) {
            NettyMessage nettyMessage = new Gson().fromJson(msgContent, NettyMessage.class);
            return nettyMessage;
        } else {
            return null;
        }
    }

    private void showNotification(EMMessage emMessage) {
        if (null != emMessage) {
            String userName = "";
            String userPic = "";
            String userGuid = "";
            String userPrice = "";
            String msgContent = "";
            msgContent = emMessage.getStringAttribute("msg", "");
            if (!TextUtils.isEmpty(msgContent)) {
                NettyMessage nettyMessage = setMsgContent(msgContent);
                if (nettyMessage != null) {
                    userName = nettyMessage.getSendUserName();
                    userPic = nettyMessage.getSendUserIcon();
                    userGuid = String.valueOf(nettyMessage.getSendUserId());
                }
            }
            Context context = BaseApplication.getGlobalContext();
            // 根据消息类型显示不同的内容
            String message = "";
            if (emMessage.getType() == EMMessage.Type.TXT) {// 文字
                EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessage.getBody();
                if (null != emTextMessageBody) {
                    if (emTextMessageBody.getMessage().equals("msgCall")) {
                        message = context.getString(R.string.video_invitations);
                    } else if (emTextMessageBody.getMessage().contains("{") && emTextMessageBody.getMessage().contains("}")) {
                        message = "礼物";
                    } else {
                        message = TextUtils.isEmpty(emTextMessageBody.getMessage()) ? context.getString(R.string.private_news) : emTextMessageBody.getMessage();
                    }

                }

            } else if (emMessage.getType() == EMMessage.Type.VOICE) {// 语音
                message = context.getString(R.string.chat_type_voice);
            } else if (emMessage.getType() == EMMessage.Type.IMAGE) {// 图片
                message = context.getString(R.string.chat_type_image);
            }
            Intent intent = new Intent(context, ChatActivity.class);
            if (userGuid != null && userGuid.length() > 0) {
                intent.putExtra(LaunchHelper.INTENT_KEY_COMMON, new ChatParcelable(Long.parseLong(userGuid), emMessage.getFrom(), userName, userPic, 1));
            }
            NotificationHelper.getInstance(context)
                    .setTicker(context.getString(R.string.chat_new_msg_received, userGuid.equals("10000") ? context.getString(R.string.kefu_desc) : userName))
                    .setIcon(R.mipmap.logo)
                    .setTitle(userName)
                    .setMessage(message)
                    .setTime(emMessage.getMsgTime())
                    .setDefaults()
                    .setPendingIntentActivity(Integer.parseInt(emMessage.getFrom()), intent, PendingIntent.FLAG_UPDATE_CURRENT)
                    .notify(Integer.parseInt(userGuid));
        }
    }

    private void showReceiveNotification(PushMessage pushMsg) {

        if (null != pushMsg) {
            Context context = BaseApplication.getGlobalContext();
            Intent intent = new Intent(context, MainActivity.class);
            intent.setFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            NotificationHelper.getInstance(context)
                    .setTicker(context.getString(R.string.chat_new_msg_received, pushMsg.getTitle()))
                    .setIcon(R.mipmap.logo)
                    .setTitle(pushMsg.getTitle())
                    .setMessage(pushMsg.getContent())
                    .setTime(System.currentTimeMillis())
                    .setDefaults()
                    .setPendingIntentActivity(0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                    .notify(1);
        }
    }

    /**
     * 设置消息监听
     */
    public void setMessageListener(EMMessageListener listener) {
        if (null != listener) {
            EMClient.getInstance().chatManager().addMessageListener(listener);
        }
    }

    /**
     * 移除接收消息监听器
     */
    public void removeMessageListener(EMMessageListener listener) {
        if (null != listener) {
            EMClient.getInstance().chatManager().removeMessageListener(listener);
        }
    }

    /**
     * 发送消息（仅单聊）
     *
     * @param emMessage
     * @param listener
     */
    private void sendMessage(final EMMessage emMessage, final OnMessageSendListener listener) {
        if (null != emMessage) {
            emMessage.setChatType(EMMessage.ChatType.Chat);
            emMessage.setMessageStatusCallback(new EMCallBack() {
                @Override
                public void onSuccess() {
                    if (null != listener) {
                        listener.onSuccess(emMessage);
                    }
                }

                @Override
                public void onError(int code, String error) {
                    Log.e("AAAAAAA", "onError: 这是在HyphenateHelper消息发送失败的code=" + code + "----错误信息为" + error);
                    if (null != listener) {
                        listener.onError();
                    }
                }

                @Override
                public void onProgress(int progress, String status) {
                }
            });

            //设置扩展的消息
            NettyMessage nettyMessage = new NettyMessage();
            nettyMessage.setSendUserAccount(UserPreference.getAccount());
            nettyMessage.setSendUserName(UserPreference.getNickname());
            nettyMessage.setSendUserIcon(UserPreference.getMiddleImage());
            nettyMessage.setSendUserId(Long.parseLong(UserPreference.getId()));
            nettyMessage.setNotice(1);
            nettyMessage.setExtendType(1);
            nettyMessage.setExt(null);
            emMessage.setAttribute("msg", new Gson().toJson(nettyMessage));
            EMClient.getInstance().chatManager().sendMessage(emMessage);
        }
    }

    /**
     * 发送文字消息（仅单聊）
     *
     * @param account 接收方account
     * @param text    消息内容
     */
    public void sendTextMessage(String account, String text, OnMessageSendListener listener) {
        Log.i(TAG, "sendTextMessage#account = " + account + " text = " + text);
        sendMessage(EMMessage.createTxtSendMessage(text, account), listener);
    }

    /**
     * 发送语音消息（仅单聊）
     *
     * @param account  接收方account
     * @param filePath 语音文件路径
     * @param duration 语音时间长度(单位秒)
     */
    public void sendVoiceMessage(String account, String filePath, int duration, OnMessageSendListener listener) {
        Log.i(TAG, "sendVoiceMessage#account = " + account + " filePath = " + filePath + " duration = " + duration);
        sendMessage(EMMessage.createVoiceSendMessage(filePath, duration, account), listener);
    }

    /**
     * 发送图片消息（仅单聊）
     *
     * @param account  接收方account
     * @param filePath 图片文件路径
     */
    public void sendImageMessage(String account, String filePath, OnMessageSendListener listener) {
        Log.i(TAG, "sendImageMessage#account = " + account + " filePath = " + filePath);
        // 第二个参数表示是否发送原图，图片超过100k会默认压缩后发送
        sendMessage(EMMessage.createImageSendMessage(filePath, false, account), listener);
    }

    public EMConversation getConversation(String account) {
        // 查询单聊会话
        return EMClient.getInstance().chatManager().getConversation(account, EMConversation.EMConversationType.Chat);
    }

    /**
     * 初始化某个用户的聊天记录
     *
     * @param account  用户account
     * @param pageSize 每页显示的聊天记录条数
     * @return
     */
    public List<EMMessage> initConversation(String account, int pageSize) {
        // 查询单聊会话
        EMConversation emConversation = getConversation(account);
        if (null != emConversation) {
            // 标记所有未读消息为已读
            emConversation.markAllMessagesAsRead();
            return emConversation.loadMoreMsgFromDB(null, pageSize);
        }
        return null;
    }


    /**
     * 获取某个用户的最后一条信息
     */
    public List<EMMessage> getUnReadMsg(String account) {
        EMConversation emConversation = getConversation(account);
        if (emConversation != null) {
            return emConversation.loadMoreMsgFromDB(null, 5);
        }
        return null;
    }

    /**
     * 获取以前的聊天记录
     */
    public List<EMMessage> addConversation(String account, String msgId, int pageSize) {
        EMConversation conversation = getConversation(account);
        if (conversation != null) {
            return conversation.loadMoreMsgFromDB(msgId, pageSize);
        }
        return null;
    }


    /**
     * 查询更多聊天记录，以最后一条消息查询
     *
     * @param account
     * @param pageSize
     * @return
     */
    public List<EMMessage> loadMoreMessages(String account, int pageSize) {
        // 查询单聊会话
        EMConversation emConversation = getConversation(account);
        if (null != emConversation) {
            // 查询最后一条消息id前pageSize条消息记录
            return emConversation.loadMoreMsgFromDB(emConversation.getLastMessage().getMsgId(), pageSize);
        }
        return null;
    }

    /**
     * @return 与某个用户全部聊天记录条数
     */
    public int getAllMsgCount(String account) {
        EMConversation emConversation = getConversation(account);
        if (null != emConversation) {
            return emConversation.getAllMsgCount();
        }
        return 0;
    }

    /**
     * 下载消息附件
     *
     * @param emMessage
     */
    public void downloadAttachment(EMMessage emMessage) {
        if (null != emMessage) {
            EMClient.getInstance().chatManager().downloadAttachment(emMessage);
        }
    }

    /**
     * @return 全部未读消息数
     */
    public int getUnreadMsgCount() {
        return EMClient.getInstance().chatManager().getUnreadMessageCount();
    }

    /**
     * @return 所有会话（聊天记录）
     */
    public Map<String, EMConversation> getAllConversations() {
        return EMClient.getInstance().chatManager().getAllConversations();
    }

    /**
     * 判断是否在ChatActivity这个界面
     */
    public void isChatActivity() {
        isChatActivity = false;
    }

    /**
     * 离开ChatActivity这个界面时把标记改为true
     */
    public void isNotChatActivity() {
        isChatActivity = true;
    }

    /**
     * 清空所有未读消息
     */
    public void clearAllUnReadMsg() {
        //所有未读消息数清零
        EMClient.getInstance().chatManager().markAllConversationsAsRead();
    }

    /**
     * 获取某个人未读消息个数
     *
     * @param username
     * @return
     */
    public int getConverUnreadMsgCount(String username) {
        EMConversation conversation = EMClient.getInstance().chatManager().getConversation(username);
        int unreadMsgCount = 0;
        try {
            unreadMsgCount = conversation.getUnreadMsgCount();
        } catch (Exception e) {

        }
        return unreadMsgCount;
    }

    /**
     * 指定会话消息未读数清零
     */
    public void markMsgAsRead(String account) {
        EMConversation conversation = EMClient.getInstance().chatManager().getConversation(account);
        conversation.markAllMessagesAsRead();
    }

}
