package com.popo.video.common;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.FrameLayout;

import com.google.gson.Gson;
import com.popo.library.util.NotificationHelper;
import com.popo.video.C;
import com.popo.video.base.BaseApplication;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.AgoraChannelKey;
import com.popo.video.data.model.AgoraParams;
import com.popo.video.data.model.AgoraToken;
import com.popo.video.data.model.PurchaseHowMoneyDionmads;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.event.AgoraEvent;
import com.popo.video.event.AgoraMediaEvent;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.LogUtil;
import com.popo.video.event.InviteEvent2;
import com.popo.video.event.SingleLoginFinishEvent;
import com.popo.video.parcelable.ListParcelable;
import com.popo.video.parcelable.VideoInviteParcelable;
import com.popo.video.parcelable.VideoParcelable;
import com.popo.video.ui.login.LoginActivity;
import com.popo.video.ui.video.VideoActivity;
import com.popo.video.ui.video.VideoInviteActivity;

import org.greenrobot.eventbus.EventBus;

import io.agora.AgoraAPIOnlySignal;
import io.agora.IAgoraAPI;
import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.AgoraVideoFrame;
import io.agora.rtc.video.VideoCanvas;

import static com.popo.video.common.TimeUtils.mContext;

/**
 * 声网帮助类
 * Created by zhangdroid on 2017/6/14.
 */
public class AgoraHelper {
    private static final String TAG = AgoraHelper.class.getSimpleName();
    // 信令事件常量定义
    public static final int EVENT_CODE_INVITE_FAILED = 0;// 发起呼叫失败
    public static final int EVENT_CODE_INVITE_SUCCESS = 1;// 发起的呼叫已经接通
    public static final int EVENT_CODE_INVITE_CANCEL = 2;// 取消已经发起的呼叫
    public static final int EVENT_CODE_INVITE_ACCEPT = 3;// 对方接受呼叫
    public static final int EVENT_CODE_INVITE_REFUSE = 4;// 对方拒绝呼叫
    public static final int EVENT_CODE_INVITE_END_PEER = 5;// 发起方取消了呼叫
    public static final int EVENT_CODE_JOIN_CHANNEL_FAILED = 6;// 加入频道失败
    public static final int EVENT_CODE_LEAVE_CHANNEL_SELF = 7;// 已方离开频道
    public static final int EVENT_CODE_SEND_MSG_SUCCESS = 8;// 发送点对点消息成功
    public static final int EVENT_CODE_SEND_MSG_FAILED = 9;// 发送点对点消息失败
    public static final int EVENT_CODE_RECEIVE_MSG = 10;// 收到对方发送的消息
    public static final int EVENT_CODE_SEND_GIFTS = 11;//发送礼物的字段
    // 媒体事件常量定义
    public static final int EVENT_CODE_SETUP_REMOTE_VIDEO = 0;// 创建远端视频
    public static final int EVENT_CODE_MEDIA_JOIN_SUCCESS = 1;// 成功加入视频
    public static final int EVENT_CODE_USER_OFFLINE = 2;// 视频中用户离线或掉线
    public static final int EVENT_CODE_VIDEO_ERROR = 3;// 视频中发生错误
    public static final int EVENT_CODE_NOT_LOGIN = 103;//账户在别处登录
    private static AgoraHelper sInstance;
    // 信令
    private static AgoraAPIOnlySignal sAgoraAPIOnlySignal;
    // 媒体
    private static RtcEngine sRtcEngine;

    private AgoraHelper() {
        // 创建AgoraAPIOnlySignal
        sAgoraAPIOnlySignal = AgoraAPIOnlySignal.getInstance(BaseApplication.getGlobalContext(),
                C.KEY_AGORA);
        sAgoraAPIOnlySignal.callbackSet(sCallBack);
        // 创建RtcEngine
        sRtcEngine = RtcEngine.create(BaseApplication.getGlobalContext(), C.KEY_AGORA, sRtcEngineEventHandler);
        // 设置通信模式
        sRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION);
//        AgoraYuvEnhancer agoraYuvEnhancer = new AgoraYuvEnhancer(BaseApplication.getGlobalContext());
//        agoraYuvEnhancer.StartPreProcess();
    }

    public static AgoraHelper getInstance() {
        if (null == sInstance) {
            synchronized (AgoraHelper.class) {
                if (null == sInstance) {
                    sInstance = new AgoraHelper();
                }
            }
        }
        return sInstance;
    }

    /**
     * 信令API回调
     */
    private static final IAgoraAPI.ICallBack sCallBack = new IAgoraAPI.ICallBack() {

        @Override
        public void onReconnecting(int i) {
        }

        @Override
        public void onReconnected(int i) {
        }

        @Override
        public void onLoginSuccess(int i, int i1) {
        }

        @Override
        public void onLogout(int i) {//103账户在别处登录
            if (EVENT_CODE_NOT_LOGIN == i) {
//             //ListParcelable为MainActivity中选择加载对应的fragment的Parcelable,只有一个参数，适合使用，不再创建
                LaunchHelper.getInstance().launchFinish(BaseApplication.getGlobalContext(), LoginActivity.class, new ListParcelable(1));
                EventBus.getDefault().post(new SingleLoginFinishEvent());
                NotificationHelper.getInstance(mContext).cancelAll();//清空所有通知
                AgoraHelper.getInstance().logout();
                HyphenateHelper.getInstance().logout();
            }
        }

        @Override
        public void onLoginFailed(int i) {
        }

        @Override
        public void onChannelJoined(String s) {
            // 加入频道成功
            Log.e("AAAAAAA", "  onChannelJoined: " + s);
        }

        @Override
        public void onChannelJoinFailed(String s, int i) {
            // 加入频道失败
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_JOIN_CHANNEL_FAILED);
            EventBus.getDefault().post(agoraEvent);
            EventBus.getDefault().post(new InviteEvent2());
        }

        @Override
        public void onChannelLeaved(String s, int i) {
            // 离开频道
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_LEAVE_CHANNEL_SELF);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onChannelUserJoined(String s, int i) {
            // 其他用户加入频道
        }

        @Override
        public void onChannelUserLeaved(String s, int i) {
            // 其他用户离开频道
        }

        @Override
        public void onChannelUserList(String[] strings, int[] ints) {
        }

        @Override
        public void onChannelQueryUserNumResult(String s, int i, int i1) {
        }

        @Override
        public void onChannelAttrUpdated(String s, String s1, String s2, String s3) {
        }

        @Override
        public void onInviteReceived(String s, String s1, int i, String s2) {
            // 收到对方呼叫邀请请求，此处显示被邀请页面
            AgoraParams agoraParams = new AgoraParams(0, "", "", 0, 0, 0);
            if (!TextUtils.isEmpty(s2)) {
                agoraParams = new Gson().fromJson(s2, AgoraParams.class);
            }
            int inviteType = agoraParams.getInviteType();
            if (inviteType == 1) {//直接接通
                Log.e("AAAAAAA", "onInviteReceived: " + s + "---" + s1 + "----" + i + "-------" + s2);
                synchronized (this) {
                    DataPreference.saveTime(0);
                    acceptInvite(s, String.valueOf(agoraParams.getuId()), s1, agoraParams.getImgageUrl(), agoraParams.getNickname(), agoraParams.getType(), "2");
                }
            } else {//直接拨打
                long currentTime = Util.getCurrentTime();
                Log.e("AAAAAAA", "onInvit直接拨打eReceived: " + (Math.abs(currentTime - Long.valueOf(agoraParams.getTime()))));
                if (agoraParams.getTime() != 0 && (Math.abs(currentTime - Long.valueOf(agoraParams.getTime())) <= 3000)) {
                    VideoInviteParcelable videoInviteParcelable = new VideoInviteParcelable(true, agoraParams.getuId(), s1, agoraParams.getNickname(), agoraParams.getImgageUrl(), agoraParams.getType(), 0);
                    videoInviteParcelable.setChannelId(s);
                    LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity.class, videoInviteParcelable);
                    wakeUpAndUnlock(BaseApplication.getGlobalContext());
                }
            }
        }

        public void acceptInvite(final String channelId, final String guid, final String account, final String imageUrl, final String mNickname, int type, String fromType) {
            final VideoParcelable videoParcelable = new VideoParcelable(Long.parseLong(guid), account, imageUrl, mNickname, channelId, "", type, false);
            ApiManager.acceptCallVideo(String.valueOf(videoParcelable.type), String.valueOf(videoParcelable.guid), fromType, new IGetDataListener<PurchaseHowMoneyDionmads>() {
                @Override
                public void onResult(PurchaseHowMoneyDionmads dionmads, boolean isEmpty) {
                    if (dionmads.getRemoteStatus() == 2) {//通话中

                    } else {
                        LaunchHelper.getInstance().launchFinish(mContext, VideoActivity.class,
                                videoParcelable);
                        AgoraHelper.getInstance().acceptInvite(channelId, videoParcelable.account);
                    }
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {

                }
            });
//            ApiManager.videoCall(guid, new IGetDataListener<VideoCall>() {
//                @Override
//                public void onResult(VideoCall videoCall, boolean isEmpty) {
//                    if (!"-1".equals(videoCall.getBeanStatus())) {// 余额充足
//                        LaunchHelper.getInstance().launchFinish(mContext, VideoActivity.class,
//                                new VideoParcelable(Long.parseLong(guid), account, mNickname, channelId, videoCall.getHostPrice()));
//                        AgoraHelper.getInstance().acceptInvite(channelId, account);
//                    } else {
//                        // 余额不足，提示用户
////                        VideoHelper.showBalanceNotEnoughTip(mVideoInviteView.obtainFragmentManager());
//                    }
//                }
//
//                @Override
//                public void onError(String msg, boolean isNetworkError) {
////                    mVideoInviteView.showTip(mContext.getString(R.string.video_accept_failed));
//                }
//            });

        }

        @Override
        public void onInviteReceivedByPeer(String s, String s1, int i) {
            Log.e("AAAAAAA", "onInviteReceivedByPeer: " + s + "----" + s1 + "----" + i);
            // 对方收到呼叫邀请，此处表示呼叫成功
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_INVITE_SUCCESS);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onInviteAcceptedByPeer(String s, String s1, int i, String s2) {
            Log.e("AAAAAAA", "onInviteAcceptedByPeer: " + s + "----" + s1 + "----" + i + "------" + s2);
            // 对方接受呼叫邀请，此处开始视频（加入频道）
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_INVITE_ACCEPT);
            agoraEvent.setChannelId(s);
            agoraEvent.setAccount(s1);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onInviteRefusedByPeer(String s, String s1, int i, String s2) {
            Log.e("AAAAAAA", "onInviteRefusedByPeer: " + s + "----" + s1 + "-----" + i + "----" + s2);
            // 对方拒绝呼叫邀请
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_INVITE_REFUSE);
            agoraEvent.setChannelId(s);
            agoraEvent.setAccount(s1);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onInviteFailed(String s, String s1, int i, int i1, String s2) {
            Log.e("AAAAAAA", "onInviteFailed: " + s + "----" + s1 + "----" + i + "------" + i1 + "-----" + s2);
            // 主动发起呼叫失败，此处关闭邀请页面并提示用户
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_INVITE_FAILED);
            agoraEvent.setChannelId(s);
            agoraEvent.setAccount(s1);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onInviteEndByPeer(String s, String s1, int i, String s2) {
            Log.e("AAAAAAA", "onInviteEndByPeer: " + s + "-----" + s1 + "----" + i + "------" + s2);
            // 对方取消呼叫邀请，此处关闭被邀请页面
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_INVITE_END_PEER);
            agoraEvent.setChannelId(s);
            agoraEvent.setAccount(s1);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onInviteEndByMyself(String s, String s1, int i) {
            Log.e("AAAAAAA", "onInviteEndByMyself: " + s + "-----" + s1 + "----" + i + "------");
            // 主动取消呼叫邀请，此处关闭邀请页面
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_INVITE_CANCEL);
            agoraEvent.setChannelId(s);
            agoraEvent.setAccount(s1);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onInviteMsg(String s, String s1, int i, String s2, String s3, String s4) {
        }

        @Override
        public void onMessageSendError(String s, int i) {
            Log.e("AAAAAAA", "onMessageSendError: " + s + "-----" + "----" + i + "------");
            // 发送消息失败
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_SEND_MSG_FAILED);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onMessageSendSuccess(String s) {
            Log.e("AAAAAAA", "onMessageSendSuccess: " + s);
            // 发送消息成功
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_SEND_MSG_SUCCESS);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onMessageAppReceived(String s) {
        }

        @Override
        public void onMessageInstantReceive(String s, int i, String s1) {
            Log.e("AAAAAAA", "onMessageInstantReceive: " + s + "-----" + s1 + "----" + i + "------");
            // 收到消息
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_RECEIVE_MSG);
            agoraEvent.setMessage(s1);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onMessageChannelReceive(String s, String s1, int i, String s2) {
        }

        @Override
        public void onLog(String s) {
        }

        @Override
        public void onInvokeRet(String s, int i, String s1, String s2) {
        }

        @Override
        public void onMsg(String s, String s1, String s2) {
        }

        @Override
        public void onUserAttrResult(String s, String s1, String s2) {
        }

        @Override
        public void onUserAttrAllResult(String s, String s1) {
        }

        @Override
        public void onError(String s, int i, String s1) {
        }

        @Override
        public void onQueryUserStatusResult(String s, String s1) {
        }

        @Override
        public void onDbg(String s, String s1) {
        }
    };

    /**
     * 媒体API回调
     */
    private static final IRtcEngineEventHandler sRtcEngineEventHandler = new IRtcEngineEventHandler() {

        @Override
        public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
            super.onJoinChannelSuccess(channel, uid, elapsed);
            // 进入频道成功，此处开启视频轮询
            EventBus.getDefault().post(new AgoraMediaEvent(EVENT_CODE_MEDIA_JOIN_SUCCESS, 0));
        }

        @Override
        public void onError(int err) {
            super.onError(err);
            EventBus.getDefault().post(new AgoraMediaEvent(EVENT_CODE_VIDEO_ERROR, 0));
        }

        @Override
        public void onFirstRemoteVideoDecoded(int uid, int width, int height, int elapsed) {
            // 初次解码，在这里初始化远端视频
            EventBus.getDefault().post(new AgoraMediaEvent(EVENT_CODE_SETUP_REMOTE_VIDEO, uid));
        }

        @Override
        public void onUserOffline(int uid, int reason) {
            super.onUserOffline(uid, reason);
            // 用户离线或掉线
            EventBus.getDefault().post(new AgoraMediaEvent(EVENT_CODE_USER_OFFLINE, uid));
        }
    };

    /**
     * 登录信令系统
     */
    public void login() {
        ApiManager.getAgoraToken(new IGetDataListener<AgoraToken>() {
            @Override
            public void onResult(AgoraToken agoraToken, boolean isEmpty) {
                if (null != agoraToken) {
                    String token = agoraToken.getToken();
                    if (!TextUtils.isEmpty(token)) {
                        // 登陆信令系统
                        if (sAgoraAPIOnlySignal != null) {
                            sAgoraAPIOnlySignal.login(C.KEY_AGORA, UserPreference.getAccount(), token, 0, null);
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                Log.e("AAAAAAA", "onError: -------------------" + msg + "---------------------" + isNetworkError);
            }
        });
    }

    /**
     * 登出声网信令系统
     */
    public void logout() {
        if (sAgoraAPIOnlySignal.isOnline() == 1) {
            sAgoraAPIOnlySignal.logout();
        }
    }

    /**
     * 发起邀请
     *
     * @param channelId 被邀请加入的频道id
     * @param account   被邀请人account
     * @param userInfo  被邀请人信息（JSON字符串）{@link AgoraParams}
     */
    public void startInvite(String channelId, String account, String userInfo) {
        // channelID使用邀请人id+被邀请人id组合形式
        Log.e("AAAAAAA", "startInvite: shipinyaoiqng-------" + channelId + "---" + account + "--" + userInfo);
        sAgoraAPIOnlySignal.channelInviteUser2(channelId, account, userInfo);
    }

    /**
     * 结束邀请
     *
     * @param channelId 被取消邀请加入的频道id
     * @param account   被邀请人account
     */
    public void stopInvite(String channelId, String account) {
        // channelID使用邀请人id+被邀请人id组合形式
        sAgoraAPIOnlySignal.channelInviteEnd(channelId, account, 0);
    }

    /**
     * 接受邀请
     *
     * @param channelId 频道id
     * @param account   邀请人的account
     */
    public void acceptInvite(String channelId, String account) {
        sAgoraAPIOnlySignal.channelInviteAccept(channelId, account, 0);
    }

    /**
     * 拒绝邀请
     *
     * @param channelId 频道id
     * @param account   邀请人的account
     */
    public void refuseInvite(String channelId, String account) {
        sAgoraAPIOnlySignal.channelInviteRefuse(channelId, account, 0, null);
    }

    /**
     * 信令系统发送点对点消息
     *
     * @param account 接受消息方account
     * @param msg     消息内容
     */
    public void sendTextMessage(String account, String msg) {
        sAgoraAPIOnlySignal.messageInstantSend(account, 0, msg, "0");
    }

    /**
     * 加入指定的频道
     *
     * @param channelId 频道id
     */
    public void joinChannel(final String channelId) {
        sAgoraAPIOnlySignal.channelJoin(channelId);
        ApiManager.getAgoraChannelKey(channelId, new IGetDataListener<AgoraChannelKey>() {
            @Override
            public void onResult(AgoraChannelKey agoraChannelKey, boolean isEmpty) {
                if (null != agoraChannelKey) {
                    String channelKey = agoraChannelKey.getKey();
                    if (!TextUtils.isEmpty(channelKey)) {
                        // 此处第四个参数必须为用户guid，后台生成密钥时使用的也是guid，保持一致
                        int code = sRtcEngine.joinChannel(channelKey, channelId, null, Integer.parseInt(UserPreference.getId()));
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                sAgoraAPIOnlySignal.channelLeave(channelId);
                // 发送事件，提示用户加入频道失败
            }
        });
    }

    /**
     * 退出指定的频道
     *
     * @param channelId 频道id
     */
    public void leaveChannel(String channelId) {
        LogUtil.i(TAG, "leaveChannel#channelId = " + channelId);
        sAgoraAPIOnlySignal.channelLeave(channelId);
        sRtcEngine.leaveChannel();
        sRtcEngine.stopPreview();
    }

    /**
     * 设置本地视频
     *
     * @param context             上下文对象
     * @param localVideoContainer 包含显示本地视频的容器
     */
    public void setupLocalVideo(Context context, FrameLayout localVideoContainer) {
        LogUtil.i(TAG, "setupLocalVideo");
        // 设置视频属性并开启视频
        sRtcEngine.setVideoProfile(Constants.VIDEO_PROFILE_720P, false);
        sRtcEngine.enableVideo();
        // 创建SurfaceView并设置本地视频
        SurfaceView surfaceView = RtcEngine.CreateRendererView(context);
        localVideoContainer.addView(surfaceView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));
        sRtcEngine.setupLocalVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_ADAPTIVE, 0));
        sRtcEngine.startPreview();
    }

    /**
     * 创建远端视频
     *
     * @param context              上下文对象
     * @param remoteVideoContainer 包含显示远端视频的容器
     * @param uId
     */
    public void setupRemoteVideo(Context context, FrameLayout remoteVideoContainer, int uId) {
        LogUtil.i(TAG, "setupRemoteVideo#uId = " + uId);
        if (remoteVideoContainer.getChildCount() >= 1) {
            return;
        }
        SurfaceView surfaceView = RtcEngine.CreateRendererView(context);
        remoteVideoContainer.addView(surfaceView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));
        sRtcEngine.setupRemoteVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_ADAPTIVE, uId));
        surfaceView.setTag(uId); // 设置tag
    }

    /**
     * 切换摄像头
     */
    public void switchCamera() {
        sRtcEngine.switchCamera();
    }

    /**
     * 销毁对象，释放资源
     */
    public void destoryRtcEngine() {
        RtcEngine.destroy();
        sAgoraAPIOnlySignal = null;
        sRtcEngine = null;
    }

    /**
     * 静音
     */
    public void closeVioce() {
        sRtcEngine.muteLocalAudioStream(true);
    }

    /**
     * 关闭静音
     */
    public void openVioce() {
        sRtcEngine.muteLocalAudioStream(false);
    }

    /**
     * 锁屏状态下点亮屏幕
     */
    private static void wakeUpAndUnlock(Context context) {
        //屏锁管理器
        KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock kl = km.newKeyguardLock("unLock");
        //解锁
        kl.disableKeyguard();
        //获取电源管理器对象
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        //获取PowerManager.WakeLock对象,后面的参数|表示同时传入两个值,最后的是LogCat里用的Tag
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_DIM_WAKE_LOCK, "bright");
        //点亮屏幕
        wl.acquire();
        //释放
        wl.release();
    }

    public void push(byte[] b) {
        AgoraVideoFrame agoraVideoFrame = new AgoraVideoFrame();
        agoraVideoFrame.buf = b;
        sRtcEngine.pushExternalVideoFrame(agoraVideoFrame);

    }
}
