package com.popo.video.common;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.video.R;
import com.popo.video.event.YeMeiMessageEvent;
import com.popo.video.parcelable.ChatParcelable;
import com.popo.video.parcelable.MatchParcelable;
import com.popo.video.ui.chat.ChatActivity;
import com.popo.video.ui.match.MatchActivity;

/**
 * Created by WangYong on 2018/4/28.
 */

public class YeMeiPopopUtil {
    private ImageView yemei_iv_avatar;
    private TextView yemei_tv_msg_match;
    private TextView yemei_tv_name_and_msg;
    private RelativeLayout yemei_rl_miss;
    private RelativeLayout yemei_rl_tip;
    private PopupWindow yemei_pop_window;
    private  View parent;
    private Context context;
    private static volatile YeMeiPopopUtil sDefault;

    private YeMeiPopopUtil() {

    }

    public static YeMeiPopopUtil getInstance() {
        if (sDefault == null) {
            synchronized (YeMeiPopopUtil.class) {
                if (sDefault == null) {
                    sDefault = new YeMeiPopopUtil();
                }
            }
        }
        return sDefault;
    }
    public void showYeMei(Activity activity,View view,final YeMeiMessageEvent nettyMessage, final boolean isFinishing){
        context=activity;
        getYeMeiPopupWindow(activity);
        parent=view;
        yemei_rl_miss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yemei_pop_window.dismiss();
            }
        });
        if (nettyMessage!=null) {
            if (nettyMessage.getType()==1) {
                yemei_tv_msg_match.setText(context.getString(R.string.yemei_tip2));
                yemei_tv_name_and_msg.setText(nettyMessage.getNickName()+context.getString(R.string.yemei_tip3));
            }else{
                yemei_tv_msg_match.setText(context.getString(R.string.yemei_tip1));
                yemei_tv_name_and_msg.setText(nettyMessage.getNickName()+context.getString(R.string.yemei_tip4));
            }
            ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(nettyMessage.getImageUrl()).transform(new CropCircleTransformation(context))
                    .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(yemei_iv_avatar).build());
            yemei_rl_tip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (nettyMessage.getType()==1) {
                        LaunchHelper.getInstance().launch(context, MatchActivity.class,
                                new MatchParcelable(Long.parseLong(nettyMessage.getGuid()),nettyMessage.getAccount(),
                                        nettyMessage.getNickName(),nettyMessage.getImageUrl()));
                    }else{
                        LaunchHelper.getInstance().launch(context, ChatActivity.class, new ChatParcelable(Long.parseLong(nettyMessage.getGuid()),
                                nettyMessage.getAccount(), nettyMessage.getNickName(), nettyMessage.getImageUrl(), 0));
                    }

                }
            });
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing) {
                    if(yemei_pop_window.isShowing()){
                        yemei_pop_window.dismiss();
                        yemei_pop_window.setAnimationStyle(R.style.mypopwindow_anim_style);
                        yemei_pop_window.showAtLocation(parent, Gravity.TOP, 0, 0);
                        handler.sendEmptyMessageDelayed(1,3000);
                    }else{
                        yemei_pop_window.setAnimationStyle(R.style.mypopwindow_anim_style);
                        yemei_pop_window.showAtLocation(parent, Gravity.TOP, 0, 0);
                        handler.sendEmptyMessageDelayed(1,3000);
                    }
                }
            }
        },300);
    }
    private void getYeMeiPopupWindow(Activity activity) {
        View contentView = LayoutInflater.from(activity).inflate(R.layout.common_popu_yemei_layout, null);
        yemei_iv_avatar = ((ImageView) contentView.findViewById(R.id.yemei_layout_iv_avatar));
        yemei_tv_msg_match = ((TextView) contentView.findViewById(R.id.yemei_layout_tv_msg_match));
        yemei_tv_name_and_msg = ((TextView) contentView.findViewById(R.id.yemei_layout_tv_name_and_msg));
        yemei_rl_miss = ((RelativeLayout) contentView.findViewById(R.id.yemei_layout_rl_miss));
        yemei_rl_tip = ((RelativeLayout) contentView.findViewById(R.id.yemei_layout_tip_rl));
        yemei_pop_window = new PopupWindow(contentView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
        contentView.setFocusable(false); // 这个很重要
        contentView.setFocusableInTouchMode(false);
        yemei_pop_window.setFocusable(false);
        yemei_rl_tip.getBackground().setAlpha(150);
    }
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (yemei_pop_window.isShowing()) {
                yemei_pop_window.dismiss();
            }
        }
    };
}
