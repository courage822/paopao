package com.popo.video.common;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

import com.popo.video.base.BaseApplication;

/**
 * Created by WangYong on 2017/12/12.
 */

public class RingManage {
    private static RingManage sInstance;
    Ringtone ringtone;

    private RingManage() {
        Context context = BaseApplication.getGlobalContext();
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        ringtone = RingtoneManager.getRingtone(context, notification);
    }

    public static RingManage getInstance() {
        if (null == sInstance) {
            synchronized (RingManage.class) {
                if (null == sInstance) {
                    sInstance = new RingManage();
                }
            }
        }
        return sInstance;
    }

    public void openRing() {
        if (!ringtone.isPlaying()) {
            ringtone.play();
        }
    }

    public void closeRing() {
        if (ringtone.isPlaying()) {
            ringtone.stop();
        }
    }
}
