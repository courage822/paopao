package com.popo.video.common;

import android.content.Context;
import android.text.TextUtils;

import com.popo.video.R;
import com.popo.video.base.BaseApplication;
import com.popo.video.data.preference.DataPreference;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    public static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    public static Context mContext = BaseApplication.getGlobalContext();

    public static String getCurrentDate() {
        return dateFormat.format(new Date(System.currentTimeMillis()));
    }

    public static String getCurrentTime() {
        return timeFormat.format(new Date(System.currentTimeMillis()));
    }

    /**
     * 显示创建时间
     *
     * @param date   需要显示的日期
     * @param format 需要显示的格式，为空时显示默认格式
     * @return 返回指定格式的时间（默认为yyyy年MM月dd日 HH:mm），当前年不显示年份
     */
    public static String showCreatedTime(String date, String format) {
        if (TextUtils.isEmpty(format)) {
            format = "yyyy-MM-dd HH:mm";
        }
        String time = null;
        try {
            SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date old = mFormat.parse(date);
            time = new SimpleDateFormat(format).format(old);
            Date currentDate = new Date();
            if (old.getYear() == currentDate.getYear()) {// 当前年
                long result = currentDate.getTime() - old.getTime();
                if (result < 60 * 1000) {// 一分钟内
                    time = mContext.getString(R.string.moment_ago);
                } else if (result >= 60 * 1000 && result < 60 * 60 * 1000) {// 一小时内
                    long mins = result / (60 * 1000);
                    time = mins+ String.format(mContext.getResources().getQuantityString(R.plurals.minutes_ago, Integer.parseInt(String.valueOf(mins))));
                } else if (result >= 60 * 60 * 1000 && result <= 24 * 60 * 60 * 1000) {// 24小时内
                    long hours = result / (60 * 60 * 1000);
                    time =hours+ String.format(mContext.getResources().getQuantityString(R.plurals.hours_ago, Integer.parseInt(String.valueOf(hours))));
                } else {// 第二日及以后
                    time = pad(old.getMonth() + 1) + "-" + pad(old.getDate()) + " " + time.substring(11);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    /**
     * 显示距离现在已经过了多长时间
     *
     * @param date 要显示的时间
     * @return
     */
    public static String showTimeAgo(String date) {
        String time = null;
        try {
            SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date old = mFormat.parse(date);
            time = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(old);
            Date currentDate = new Date();
            if (old.getYear() == currentDate.getYear()) {// 当前年
                long result = currentDate.getTime() - old.getTime();
                if (result < 60 * 1000) {// 一分钟内
                    time = mContext.getString(R.string.moment_ago);
                } else if (result >= 60 * 1000 && result < 60 * 60 * 1000) {// 一小时内
                    long mins = result / (60 * 1000);
                    time = String.format(mContext.getResources().getQuantityString(R.plurals.minutes_ago, Integer.parseInt(String.valueOf(mins))));
                } else if (result >= 60 * 60 * 1000 && result < 24 * 60 * 60 * 1000) {// 24小时内
                    long hours = result / (60 * 60 * 1000);
                    time = String.format(mContext.getResources().getQuantityString(R.plurals.hours_ago, Integer.parseInt(String.valueOf(hours))));
                } else if (result >= 24 * 60 * 60 * 1000 && result < 7 * 24 * 60 * 60 * 1000) {// 一周内
                    long days = result / (24 * 60 * 60 * 1000);
                    time = String.format(mContext.getResources().getQuantityString(R.plurals.days_ago, Integer.parseInt(String.valueOf(days))));
                } else {// 一周以后
                    time = pad(old.getMonth() + 1) + "-" + pad(old.getDate()) + " " + time.substring(11);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    /**
     * 日期/时间小于10时格式补0
     */
    public static String pad(int c) {
        if (c >= 10) {
            return String.valueOf(c);
        } else {
            return "0" + String.valueOf(c);
        }
    }
    /**
     * 转换为手机本地时间
     *
     * @param serverSysTime 当前系统时间戳
     * @param time          需要显示的时间
     * @return 转换后的本地时间
     */
  /*  public static String getLocalTime(long serverSysTime, String time) {
        long localTime = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s", Locale.US);
        try {
            localTime = System.currentTimeMillis() - (serverSysTime - simpleDateFormat.parse(time).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (localTime == 0) {
            return time;
        }
        return timeFormat.format(new Date(localTime));
    }*/

    /**
     * 根据手机时间显示不同的时间
     *
     * @param serverSysTime 当前服务器时间戳
     * @param date          需要显示的时间
     * @return 转换后的时间
     */
    public static String getLocalTime(Context context, long serverSysTime, long date) {
        SimpleDateFormat showFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        Date targetDate = new Date(date);
        String result = showFormat.format(targetDate);
        if (targetDate.getYear() == new Date().getYear()) {// 当前年
            // 计算距离现在已经过了多长时间
            long timeDiff = serverSysTime - date;
            if (timeDiff < 60 * 1000) {// 一分钟内
                result = context.getString(R.string.moment_ago);
            } else if (timeDiff >= 60 * 1000 && timeDiff < 60 * 60 * 1000) {// 一小时内
                int mins = (int) (timeDiff / (60 * 1000));
                result = context.getResources().getQuantityString(R.plurals.minutes_ago, mins, mins);
            } else if (timeDiff >= 60 * 60 * 1000 && timeDiff < 24 * 60 * 60 * 1000) {// 24小时内
                int hours = (int) (timeDiff / (60 * 60 * 1000));
                result = context.getResources().getQuantityString(R.plurals.hours_ago, hours, hours);
            } else if (timeDiff >= 24 * 60 * 60 * 1000 && timeDiff < 7 * 24 * 60 * 60 * 1000) {// 一周内
                int days = (int) (timeDiff / (24 * 60 * 60 * 1000));
                result = context.getResources().getQuantityString(R.plurals.days_ago, days, days);
            } else {// 一周以后
                // 显示手机时间
                long localTime = System.currentTimeMillis() - timeDiff;
                Date localDate = new Date(localTime);
                result = pad(localDate.getMonth() + 1) + "-" + pad(localDate.getDate()) + " "
                        + pad(localDate.getHours()) + ":" + pad(localDate.getMinutes());
            }
        }
        return result;
    }

    /**
     *  把long 类型的时间戳转化成String 时间串
     * @param time
     * @return
     */
    public static String getStringTime(String time) {
        SimpleDateFormat sdr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        @SuppressWarnings("unused")
        long lcc = Long.valueOf(time);
//        int i = Integer.parseInt(time);
        String times = sdr.format(new Date(lcc));
        return times;

    }
    /*
      * 将秒数转为时分秒,格式为00:00:00或者00:00
      * */
    public static String fromSecondToTime(long second,int flag) {
        long h = 00;
        long d = 00;
        long s = 00;
        StringBuffer sb=new StringBuffer();
        long temp = second % 3600;
        if (second > 3600) {
            h = second / 3600;
            if (temp != 0) {
                if (temp > 60) {
                    d = temp / 60;
                    if (temp % 60 != 0) {
                        s = temp % 60;
                    }
                } else {
                    s = temp;
                }
            }
        } else {
            d = second / 60;
            if (second % 60 != 0) {
                s = second % 60;
            }
        }
        if(flag==1){//00:00:00
            sb.append(h/10).append(h%10)
                    .append(":")
                    .append(d/10).append(d%10)
                    .append(":")
                    .append(s/10).append(s%10);
        }else if(flag==2){//00:00
            sb.append(d/10).append(d%10)
              .append(":")
              .append(s/10).append(s%10);
        }

        return sb.toString();
    }
    public static boolean  timeIsPast(){
        long yejiaoTime = DataPreference.getYejiaoTime();//页脚到来的时间
        long currentTime = System.currentTimeMillis();//当前时间
        if(yejiaoTime==0){//初始化的时候
            return false;
        }
        if ((currentTime-yejiaoTime)>50*1000) {//页脚到来时间超过1分钟，不在展示页脚
            return false;
        } else if((currentTime-yejiaoTime)<=50*1000&&(currentTime-yejiaoTime)>0) {
            return true;
        }
        return  false;
    }
}
