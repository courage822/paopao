package com.popo.video.common;

import android.text.TextUtils;

import com.popo.video.data.model.PlatformInfo;
import com.popo.video.data.preference.PlatformPreference;

/**
 * 价格单位
 * Created by xuzhaole on 2018/2/8.
 */

public class PriceUnitUtils {

    /**
     * 给价格加单位 例如  ￥34
     *
     * @param price
     * @return
     */
    public static String getPriceFromCountry(String price) {
        String unitPrice = "";
        PlatformInfo platformInfo = PlatformPreference.getPlatformInfo();
        if (!TextUtils.isEmpty(platformInfo.getCountry())) {
            if (platformInfo.getCountry().equals("Australia")) {
                unitPrice = "AU$" + price;
            } else if (platformInfo.getCountry().equals("Canada")) {
                unitPrice = "CA$" + price;
            } else if (platformInfo.getCountry().equals("Hong Kong")) {
                unitPrice = "HK$" + price;
            } else if (platformInfo.getCountry().equals("India")) {
                unitPrice = "INR" + price;
            } else if (platformInfo.getCountry().equals("Indonesia")) {
                unitPrice = "IDR" + price;
            } else if (platformInfo.getCountry().equals("Ireland")) {
                unitPrice = "€" + price;
            } else if (platformInfo.getCountry().equals("NewZealand")) {
                unitPrice = "NZ$" + price;
            } else if (platformInfo.getCountry().equals("Pakistan")) {
                unitPrice = "PKR" + price;
            } else if (platformInfo.getCountry().equals("Philippines")) {
                unitPrice = "PHP" + price;
            } else if (platformInfo.getCountry().equals("Singapore")) {
                unitPrice = "S$" + price;
            } else if (platformInfo.getCountry().equals("SouthAfrica")) {
                unitPrice = "ZAR" + price;
            } else if (platformInfo.getCountry().equals("United Kingdom")) {
                unitPrice = "￡" + price;
            } else if (platformInfo.getCountry().equals("Taiwan")) {
                unitPrice = "NT$" + price;
            } else if (platformInfo.getCountry().equals("China")) {
                unitPrice = "¥" + price;
            } else {
                unitPrice = "$" + price;
            }
        } else {
            unitPrice = "$" + price;
        }
        return unitPrice;
    }

    /**
     * 根据国家获取价格单位
     *
     * @return
     */
    public static String getPriceUnit() {
        String unit = "$";
        PlatformInfo platformInfo = PlatformPreference.getPlatformInfo();
        if (!TextUtils.isEmpty(platformInfo.getCountry())) {
            if (platformInfo.getCountry().equals("Australia")) {
                unit = "AU$";
            } else if (platformInfo.getCountry().equals("Canada")) {
                unit = "CA$";
            } else if (platformInfo.getCountry().equals("Hong Kong")) {
                unit = "HK$";
            } else if (platformInfo.getCountry().equals("India")) {
                unit = "INR";
            } else if (platformInfo.getCountry().equals("Indonesia")) {
                unit = "IDR";
            } else if (platformInfo.getCountry().equals("Ireland")) {
                unit = "€";
            } else if (platformInfo.getCountry().equals("NewZealand")) {
                unit = "NZ$";
            } else if (platformInfo.getCountry().equals("Pakistan")) {
                unit = "PKR";
            } else if (platformInfo.getCountry().equals("Philippines")) {
                unit = "PHP";
            } else if (platformInfo.getCountry().equals("Singapore")) {
                unit = "S$";
            } else if (platformInfo.getCountry().equals("SouthAfrica")) {
                unit = "ZAR";
            } else if (platformInfo.getCountry().equals("United Kingdom")) {
                unit = "￡";
            } else if (platformInfo.getCountry().equals("Taiwan")) {
                unit = "NT$";
            } else if (platformInfo.getCountry().equals("China")) {
                unit = "¥";
            } else {
                unit = "$";
            }
        }
        return unit;
    }
}
