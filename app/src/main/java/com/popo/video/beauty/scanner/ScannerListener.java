package com.popo.video.beauty.scanner;

import android.net.Uri;

/**
 * Created by xuzhaole on 2018/3/27.
 */

public interface ScannerListener {
    void oneComplete(String path, Uri uri);

    void allComplete(String[] filePaths);

}
