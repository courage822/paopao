package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2017/6/7.
 *举报
 */
public class ReportParcelable implements Parcelable {
    public String remoteUid;

    public ReportParcelable(String remoteUid) {
        this.remoteUid = remoteUid;
    }

    protected ReportParcelable(Parcel in) {
        remoteUid = in.readString();
    }

    public static final Creator<ReportParcelable> CREATOR = new Creator<ReportParcelable>() {
        @Override
        public ReportParcelable createFromParcel(Parcel in) {
            return new ReportParcelable(in);
        }

        @Override
        public ReportParcelable[] newArray(int size) {
            return new ReportParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(remoteUid);
    }
}
