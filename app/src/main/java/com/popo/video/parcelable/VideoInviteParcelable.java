package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 跳转视频邀请页面需要传递的参数
 * Created by zhangdroid on 2017/6/7.
 */
public class VideoInviteParcelable implements Parcelable {
    /**
     * 是否被邀请，false表示主动发起视频邀请
     */
    public boolean isInvited;
    /**
     * 被邀请人/邀请人id
     */
    public long uId;
    /**
     * 被邀请人/邀请人account
     */
    public String account;
    /**
     * 被邀请人/邀请人昵称
     */
    public String nickname;
    /**
     * 被邀请人/邀请人头像
     */
    public String imgageUrl;
    /**
     * 被邀请加入的频道id
     */
    public String channelId;
    /**
     * 0为视频，1为语音
     */
    public int type;
    /**
     * 0是呼叫，1为直接接通
     */
    public int inviteType;
    public VideoInviteParcelable(boolean isInvited, long uId, String account, String nickname, String imgageUrl,int type,int inviteType) {
        this.isInvited = isInvited;
        this.uId = uId;
        this.account = account;
        this.nickname = nickname;
        this.imgageUrl = imgageUrl;
        this.type=type;
        this.inviteType=inviteType;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isInvited ? (byte) 1 : (byte) 0);
        dest.writeLong(this.uId);
        dest.writeString(this.account);
        dest.writeString(this.nickname);
        dest.writeString(this.imgageUrl);
        dest.writeString(this.channelId);
        dest.writeInt(this.type);
        dest.writeInt(this.inviteType);
    }

    protected VideoInviteParcelable(Parcel in) {
        this.isInvited = in.readByte() != 0;
        this.uId = in.readLong();
        this.account = in.readString();
        this.nickname = in.readString();
        this.imgageUrl = in.readString();
        this.channelId = in.readString();
        this.type = in.readInt();
        this.inviteType = in.readInt();
    }

    public static final Creator<VideoInviteParcelable> CREATOR = new Creator<VideoInviteParcelable>() {
        @Override
        public VideoInviteParcelable createFromParcel(Parcel source) {
            return new VideoInviteParcelable(source);
        }

        @Override
        public VideoInviteParcelable[] newArray(int size) {
            return new VideoInviteParcelable[size];
        }
    };
}
