package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 跳转支付页面需要传递的参数
 * Created by zhangdroid on 2017/6/10.
 */
public class PayParcelable implements Parcelable {
    // 支付拦截来源
    public String fromTag;
    public int type;//0为包月，1为我的钥匙
    public PayParcelable(String fromTag, int type) {
        this.fromTag = fromTag;
        this.type=type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fromTag);
        dest.writeInt(this.type);
    }

    protected PayParcelable(Parcel in) {
        this.fromTag = in.readString();
        this.type = in.readInt();
    }

    public static final Creator<PayParcelable> CREATOR = new Creator<PayParcelable>() {
        @Override
        public PayParcelable createFromParcel(Parcel source) {
            return new PayParcelable(source);
        }

        @Override
        public PayParcelable[] newArray(int size) {
            return new PayParcelable[size];
        }
    };
}
