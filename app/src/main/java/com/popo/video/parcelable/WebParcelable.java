package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 跳转公用网页Activity时需要传递的参数
 * Created by zhangdroid on 2017/5/18.
 */
public class WebParcelable implements Parcelable {
    public String title;
    public String url;
    public boolean showBottom;

    public WebParcelable(String title, String url, boolean showBottom) {
        this.title = title;
        this.url = url;
        this.showBottom = showBottom;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.url);
        dest.writeByte(this.showBottom ? (byte) 1 : (byte) 0);
    }

    protected WebParcelable(Parcel in) {
        this.title = in.readString();
        this.url = in.readString();
        this.showBottom = in.readByte() != 0;
    }

    public static final Creator<WebParcelable> CREATOR = new Creator<WebParcelable>() {
        @Override
        public WebParcelable createFromParcel(Parcel source) {
            return new WebParcelable(source);
        }

        @Override
        public WebParcelable[] newArray(int size) {
            return new WebParcelable[size];
        }
    };

}
