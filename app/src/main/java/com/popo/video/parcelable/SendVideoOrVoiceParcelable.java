package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by WangYong on 2017/11/7.
 */

public class SendVideoOrVoiceParcelable implements Parcelable {
    public String title;//标题
    public int price;//价格
    public String x;//预留字段
    public int pos;

    public SendVideoOrVoiceParcelable(String title, int price, String x,int pos) {
        this.title = title;
        this.price = price;
        this.x = x;
        this.pos=pos;
    }

    protected SendVideoOrVoiceParcelable(Parcel in) {
        title = in.readString();
        price = in.readInt();
        x = in.readString();
        pos = in.readInt();
    }

    public static final Creator<SendVideoOrVoiceParcelable> CREATOR = new Creator<SendVideoOrVoiceParcelable>() {
        @Override
        public SendVideoOrVoiceParcelable createFromParcel(Parcel in) {
            return new SendVideoOrVoiceParcelable(in);
        }

        @Override
        public SendVideoOrVoiceParcelable[] newArray(int size) {
            return new SendVideoOrVoiceParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeInt(price);
        dest.writeString(x);
        dest.writeInt(pos);
    }
}
