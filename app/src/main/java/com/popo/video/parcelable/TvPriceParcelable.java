package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2017/7/11.
 * 用来从个人中心页面向设置价格界面传递价格的参数
 */

public class TvPriceParcelable implements Parcelable {
    public String price;
    public int audioPrice;

    public String massVideoDesc;//群发视频说明
    public String massAudioDesc;//群发语音说明  2018/2/7


    public TvPriceParcelable(String price,int audioPrice,String massVideoDesc,String massAudioDesc) {
        this.price=price;
        this.audioPrice=audioPrice;
        this.massVideoDesc = massVideoDesc;
        this.massAudioDesc = massAudioDesc;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.price);
        dest.writeInt(this.audioPrice);
        dest.writeString(this.massVideoDesc);
        dest.writeString(this.massAudioDesc);
    }

    protected TvPriceParcelable(Parcel in) {
        this.price = in.readString();
        this.audioPrice = in.readInt();
        this.massVideoDesc = in.readString();
        this.massAudioDesc = in.readString();
    }

    public static final Creator<TvPriceParcelable> CREATOR = new Creator<TvPriceParcelable>() {
        @Override
        public TvPriceParcelable createFromParcel(Parcel source) {
            return new TvPriceParcelable(source);
        }

        @Override
        public TvPriceParcelable[] newArray(int size) {
            return new TvPriceParcelable[size];
        }
    };
}
