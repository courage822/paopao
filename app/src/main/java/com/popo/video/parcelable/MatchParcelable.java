package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * 展示大图时需要传递的参数
 * Created by zhangdroid on 2017/6/3.
 */
public class MatchParcelable implements Parcelable {
    public long guid; // 用户id
    public String account;// 账号
    public String nickName;// 昵称
    public String iconUrlMininum;// 小图

    public MatchParcelable(long guid, String account,String nickName,String iconUrlMininum) {
        this.guid = guid;
        this.account = account;
        this.nickName = nickName;
        this.iconUrlMininum = iconUrlMininum;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.guid);
        dest.writeString(this.account);
        dest.writeString(this.nickName);
        dest.writeString(this.iconUrlMininum);
    }

    protected MatchParcelable(Parcel in) {
        this.guid = in.readLong();
        this.account = in.readString();
        this.nickName = in.readString();
        this.iconUrlMininum = in.readString();
    }

    public static final Parcelable.Creator<MatchParcelable> CREATOR = new Parcelable.Creator<MatchParcelable>() {
        @Override
        public MatchParcelable createFromParcel(Parcel source) {
            return new MatchParcelable(source);
        }

        @Override
        public MatchParcelable[] newArray(int size) {
            return new MatchParcelable[size];
        }
    };
}
