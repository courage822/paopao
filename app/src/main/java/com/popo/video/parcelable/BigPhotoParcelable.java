package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * 展示大图时需要传递的参数
 * Created by zhangdroid on 2017/6/3.
 */
public class BigPhotoParcelable implements Parcelable {
    public int index;// 当前显示的图片索引
    public List<String> imgUrlList;// 需要展示的图片url列表

    public BigPhotoParcelable(int index, List<String> imgUrlList) {
        this.index = index;
        this.imgUrlList = imgUrlList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.index);
        dest.writeStringList(this.imgUrlList);
    }

    protected BigPhotoParcelable(Parcel in) {
        this.index = in.readInt();
        this.imgUrlList = in.createStringArrayList();
    }

    public static final Creator<BigPhotoParcelable> CREATOR = new Creator<BigPhotoParcelable>() {
        @Override
        public BigPhotoParcelable createFromParcel(Parcel source) {
            return new BigPhotoParcelable(source);
        }

        @Override
        public BigPhotoParcelable[] newArray(int size) {
            return new BigPhotoParcelable[size];
        }
    };
}
