package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2017/6/20.
 * 在视频录制与视频上传界面传递url信息
 */

public class PlayViewParcelable implements Parcelable{
    public String urlData;

    public PlayViewParcelable(String urlData) {
        this.urlData = urlData;
    }

    protected PlayViewParcelable(Parcel in) {
        urlData = in.readString();
    }

    public static final Creator<PlayViewParcelable> CREATOR = new Creator<PlayViewParcelable>() {
        @Override
        public PlayViewParcelable createFromParcel(Parcel in) {
            return new PlayViewParcelable(in);
        }

        @Override
        public PlayViewParcelable[] newArray(int size) {
            return new PlayViewParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(urlData);
    }
}
