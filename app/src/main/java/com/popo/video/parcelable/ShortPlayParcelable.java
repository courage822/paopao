package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2017/6/20.
 * 在视频录制与视频上传界面传递url信息
 */

public class ShortPlayParcelable implements Parcelable {
    public String urlData;
    public String thumbUrl;

    public ShortPlayParcelable(String urlData, String thumbUrl) {
        this.urlData = urlData;
        this.thumbUrl = thumbUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.urlData);
        dest.writeString(this.thumbUrl);
    }

    protected ShortPlayParcelable(Parcel in) {
        this.urlData = in.readString();
        this.thumbUrl = in.readString();
    }

    public static final Creator<ShortPlayParcelable> CREATOR = new Creator<ShortPlayParcelable>() {
        @Override
        public ShortPlayParcelable createFromParcel(Parcel source) {
            return new ShortPlayParcelable(source);
        }

        @Override
        public ShortPlayParcelable[] newArray(int size) {
            return new ShortPlayParcelable[size];
        }
    };
}
