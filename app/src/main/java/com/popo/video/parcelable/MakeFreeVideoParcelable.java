package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by WangYong on 2018/1/23.
 */

public class MakeFreeVideoParcelable implements Parcelable {
    public String nickname;
    public String icon;
    public String videoUrl;
    public String account;
    public String guid;

    public MakeFreeVideoParcelable(String nickname, String icon, String videoUrl, String account, String guid) {
        this.nickname = nickname;
        this.icon = icon;
        this.videoUrl = videoUrl;
        this.account = account;
        this.guid = guid;
    }

    protected MakeFreeVideoParcelable(Parcel in) {
        nickname = in.readString();
        icon = in.readString();
        videoUrl = in.readString();
        account = in.readString();
        guid = in.readString();
    }

    public static final Creator<MakeFreeVideoParcelable> CREATOR = new Creator<MakeFreeVideoParcelable>() {
        @Override
        public MakeFreeVideoParcelable createFromParcel(Parcel in) {
            return new MakeFreeVideoParcelable(in);
        }

        @Override
        public MakeFreeVideoParcelable[] newArray(int size) {
            return new MakeFreeVideoParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nickname);
        dest.writeString(icon);
        dest.writeString(videoUrl);
        dest.writeString(account);
        dest.writeString(guid);
    }
}
