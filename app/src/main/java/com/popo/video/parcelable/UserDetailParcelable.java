package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2017/6/7.
 * 这个类是用来传递用户的remoteUid,跳转进入对方详情页面
 */
public class UserDetailParcelable implements Parcelable {
    public String remoteUid;

    public UserDetailParcelable(String remoteUid) {
        this.remoteUid = remoteUid;
    }

    protected UserDetailParcelable(Parcel in) {
        remoteUid = in.readString();
    }

    public static final Creator<UserDetailParcelable> CREATOR = new Creator<UserDetailParcelable>() {
        @Override
        public UserDetailParcelable createFromParcel(Parcel in) {
            return new UserDetailParcelable(in);
        }

        @Override
        public UserDetailParcelable[] newArray(int size) {
            return new UserDetailParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(remoteUid);
    }
}
