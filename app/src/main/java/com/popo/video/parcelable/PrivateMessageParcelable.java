package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by xuzhaole on 2017/12/6.
 */

public class PrivateMessageParcelable implements Parcelable {
    private String msgId;

    // 发件人信息
    private Long sendUserId;//userId
    private String sendUserName;//name
    private String sendUserAccount;//account
    private String sendUserIcon;//icon
    private int sendUserFrom;

    private String sendTime;

    // 收件人信息
    private Long recvUserId;
    private int recvUserFrom;
    private String recvUserName;
    private String recvUserAccount;
    private String recvUserIcon;

    private short baseType; // 消息类型，1 文字 2语音 3图片
    private int extendType; // 消息业务扩展类型：1 聊天信 2 视频邀请信 3 礼物4 语音邀请信 5 发送礼物信 6 系统消息 7 视频邀请弹窗
    private String content; // 消息内容
    private String url; // 多媒体消息路径
    private Integer audioSeconds; // 多媒体消息路径

    private String pushToken; // ios 离线推送 pushToken
    private int pushChannel; // ios 离线推送 pushToken

    private int type; // 定义3 为视频邀请 1 普通推送 2  4 下线通知
    private int status; //1 发起 2 取消
    private String giftId; //解锁礼物
    private int visible; // 0 可见 1 不可见
    private int notice; // 0 不弹出通知 1 弹出 通知（是否推送栏显示）

    public PrivateMessageParcelable(short baseType, String content, String url, Integer audioSeconds) {
        this.baseType = baseType;
        this.content = content;
        this.url = url;
        this.audioSeconds = audioSeconds;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public Long getSendUserId() {
        return sendUserId;
    }

    public void setSendUserId(Long sendUserId) {
        this.sendUserId = sendUserId;
    }

    public String getSendUserName() {
        return sendUserName;
    }

    public void setSendUserName(String sendUserName) {
        this.sendUserName = sendUserName;
    }

    public String getSendUserAccount() {
        return sendUserAccount;
    }

    public void setSendUserAccount(String sendUserAccount) {
        this.sendUserAccount = sendUserAccount;
    }

    public String getSendUserIcon() {
        return sendUserIcon;
    }

    public void setSendUserIcon(String sendUserIcon) {
        this.sendUserIcon = sendUserIcon;
    }

    public int getSendUserFrom() {
        return sendUserFrom;
    }

    public void setSendUserFrom(int sendUserFrom) {
        this.sendUserFrom = sendUserFrom;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public Long getRecvUserId() {
        return recvUserId;
    }

    public void setRecvUserId(Long recvUserId) {
        this.recvUserId = recvUserId;
    }

    public int getRecvUserFrom() {
        return recvUserFrom;
    }

    public void setRecvUserFrom(int recvUserFrom) {
        this.recvUserFrom = recvUserFrom;
    }

    public String getRecvUserName() {
        return recvUserName;
    }

    public void setRecvUserName(String recvUserName) {
        this.recvUserName = recvUserName;
    }

    public String getRecvUserAccount() {
        return recvUserAccount;
    }

    public void setRecvUserAccount(String recvUserAccount) {
        this.recvUserAccount = recvUserAccount;
    }

    public String getRecvUserIcon() {
        return recvUserIcon;
    }

    public void setRecvUserIcon(String recvUserIcon) {
        this.recvUserIcon = recvUserIcon;
    }

    public short getBaseType() {
        return baseType;
    }

    public void setBaseType(short baseType) {
        this.baseType = baseType;
    }

    public int getExtendType() {
        return extendType;
    }

    public void setExtendType(int extendType) {
        this.extendType = extendType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getAudioSeconds() {
        return audioSeconds;
    }

    public void setAudioSeconds(Integer audioSeconds) {
        this.audioSeconds = audioSeconds;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public int getPushChannel() {
        return pushChannel;
    }

    public void setPushChannel(int pushChannel) {
        this.pushChannel = pushChannel;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public int getVisible() {
        return visible;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }

    public int getNotice() {
        return notice;
    }

    public void setNotice(int notice) {
        this.notice = notice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.msgId);
        dest.writeValue(this.sendUserId);
        dest.writeString(this.sendUserName);
        dest.writeString(this.sendUserAccount);
        dest.writeString(this.sendUserIcon);
        dest.writeInt(this.sendUserFrom);
        dest.writeString(this.sendTime);
        dest.writeValue(this.recvUserId);
        dest.writeInt(this.recvUserFrom);
        dest.writeString(this.recvUserName);
        dest.writeString(this.recvUserAccount);
        dest.writeString(this.recvUserIcon);
        dest.writeInt(this.baseType);
        dest.writeInt(this.extendType);
        dest.writeString(this.content);
        dest.writeString(this.url);
        dest.writeValue(this.audioSeconds);
        dest.writeString(this.pushToken);
        dest.writeInt(this.pushChannel);
        dest.writeInt(this.type);
        dest.writeInt(this.status);
        dest.writeString(this.giftId);
        dest.writeInt(this.visible);
        dest.writeInt(this.notice);
    }

    protected PrivateMessageParcelable(Parcel in) {
        this.msgId = in.readString();
        this.sendUserId = (Long) in.readValue(Long.class.getClassLoader());
        this.sendUserName = in.readString();
        this.sendUserAccount = in.readString();
        this.sendUserIcon = in.readString();
        this.sendUserFrom = in.readInt();
        this.sendTime = in.readString();
        this.recvUserId = (Long) in.readValue(Long.class.getClassLoader());
        this.recvUserFrom = in.readInt();
        this.recvUserName = in.readString();
        this.recvUserAccount = in.readString();
        this.recvUserIcon = in.readString();
        this.baseType = (short) in.readInt();
        this.extendType = in.readInt();
        this.content = in.readString();
        this.url = in.readString();
        this.audioSeconds = (Integer) in.readValue(Integer.class.getClassLoader());
        this.pushToken = in.readString();
        this.pushChannel = in.readInt();
        this.type = in.readInt();
        this.status = in.readInt();
        this.giftId = in.readString();
        this.visible = in.readInt();
        this.notice = in.readInt();
    }

    public static final Creator<PrivateMessageParcelable> CREATOR = new Creator<PrivateMessageParcelable>() {
        @Override
        public PrivateMessageParcelable createFromParcel(Parcel source) {
            return new PrivateMessageParcelable(source);
        }

        @Override
        public PrivateMessageParcelable[] newArray(int size) {
            return new PrivateMessageParcelable[size];
        }
    };
}
