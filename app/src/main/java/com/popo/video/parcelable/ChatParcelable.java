package com.popo.video.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 跳转聊天页面需要传递的参数
 * Created by zhangdroid on 2017/6/15.
 */
public class ChatParcelable implements Parcelable {
    public long guid;// 用户id
    public String account;// 用户account
    public String nickname;// 用户昵称
    public String imageUrl;// 用户头像url
    public int flag;
    public ChatParcelable(long guid, String account, String nickname, String imageUrl,int flag) {
        this.guid = guid;
        this.account = account;
        this.nickname = nickname;
        this.imageUrl = imageUrl;
        this.flag = flag;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.guid);
        dest.writeString(this.account);
        dest.writeString(this.nickname);
        dest.writeString(this.imageUrl);
        dest.writeInt(this.flag);
    }

    protected ChatParcelable(Parcel in) {
        this.guid = in.readLong();
        this.account = in.readString();
        this.nickname = in.readString();
        this.imageUrl = in.readString();
        this.flag=in.readInt();
    }

    public static final Creator<ChatParcelable> CREATOR = new Creator<ChatParcelable>() {
        @Override
        public ChatParcelable createFromParcel(Parcel source) {
            return new ChatParcelable(source);
        }

        @Override
        public ChatParcelable[] newArray(int size) {
            return new ChatParcelable[size];
        }
    };
}
