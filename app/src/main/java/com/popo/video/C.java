package com.popo.video;

import android.os.Environment;

import java.io.File;

/**
 * <p>该类保存项目中用到的所有常量，用法参考系统常量类R.java。</p>
 * 包括：
 * <li>1、全局常量</li>
 * <li>2、模块常量：各个功能模块独有，按模块分别配置</li>
 * <li>3、Service和Receiver action常量</li>
 * <li>4、Permission常量：高危权限</li>
 * Created by zhangdroid on 2017/5/17.
 */
public final class C {

    // *************************************************** 全局常量 ***************************************************
    /**
     * Google app key
     */
    public static final String KEY_GOOGLE_APP="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnBb+bgvXmXsnx26bDMBp3r9Gh75cNHtDMg0pTcANemLSe8zICRy4TpszW3MpgJnNwm6anNoreW8EAyTFQexviJBjBxcQJK+3aWY9/vSbVcM3/AQYKUxWDqsTQpSNlnTwhxPZC9t2UrQAyXNicwvRv67XpQEmXyG8oxWYzkKUJeS/+VDfFltmz3W3ED4uGsp/JfrVnN4M8mo2TSFBEaCB1GhQnr5iuJeXwS8cr00I+lqq87xYzGAJ8oI3ukCBBLtNS3IZMU33SBpDWr40+luFDg39bk0lg92noF1k7itK5+ddB016SZjwl1CXq4bSMawifk4aa8SXHzFz6xOnsRsaDQIDAQAB";
    /**
     * 声网 app id
     */
    public static final String KEY_AGORA = "9ebeab23fa13431089bb8760b6f3a47a";
    /**
     * 产品号
     */
    public static final String PRODUCT_ID = "1";
    /**
     * 台湾
     */
    public static final String TW_FID = "30108";
    /**
     * 美国
     */
    public static final String US_FID = "30101";
    /**
     * 香港
     */
    public static final String HK_FID = "30109";
    /**
     * 新加坡
     */
    public static final String SG_FID = "30110";
    /**
     * 澳大利亚
     */
    public static final String AU_FID = "30111";
    /**
     * 印度
     */
    public static final String IN_FID = "30112";
    /**7
     * 印尼
     */
    public static final String ID_FID = "30113";
    /**
     * 英国
     */
    public static final String GB_FID = "30114";
    /**
     * 加拿大
     */
    public static final String CA_FID = "30115";
    /**
     * 新西兰
     */
    public static final String NZ_FID = "30116";
    /**
     *爱尔兰
     */
    public static final String IE_FID="30117";
    /**
     *南非
     */
    public static final String ZA_FID="30118";
    /**
     *巴基斯坦
     */
    public static final String PK_FID="30124";
    /**
     *菲律宾
     */
    public static final String PH_FID="30125";
    /**
     * 测试包
     */
    public static final String CN_CESHI="3010077";
    // *************************************************** Service action常量 ***************************************************

    public static final class service {
        public static final String ACTION_APP_CREATE = "com.online.face2face.service.APP_CREATE";
    }

    // *************************************************** Receiver action常量 ***************************************************

    public static final class receiver {
    }

    // *************************************************** 高危权限常量 ***************************************************

    public static final class permission {
        public static final String PERMISSION_PHONE = "android.permission.READ_PHONE_STATE";
        public static final String PERMISSION_WRITE_EXTERNAL_STORAGE = "android.permission.WRITE_EXTERNAL_STORAGE";
        public static final String PERMISSION_READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";
        public static final String PERMISSION_CAMERA = "android.permission.CAMERA";
        public static final String PERMISSION_RECORD_AUDIO = "android.permission.RECORD_AUDIO";
        public static final String PERMISSION_LOCATION_FINE = "android.permission.ACCESS_FINE_LOCATION";
        public static final String PERMISSION_LOCATION_COARSE = "android.permission.ACCESS_COARSE_LOCATION";
    }

    // *************************************************** 首页tab常量 ***************************************************

    public static final class homepage {
        /**
         * 女神
         */
        public static final int TYPE_GODDESS = 1;
        /**
         * 活跃播主
         */
        public static final int TYPE_ACTIVE_ANCHOR = 2;
        /**
         * 新人
         */
        public static final int TYPE_NEW = 3;
        /**
         * 活跃聊友
         */
        public static final int TYPE_ACTIVE_FRIEND = 4;
        /**
         * 新晋聊友
         */
        public static final int TYPE_NEW_FRIEND = 5;
        /**
         * 女主播看到的男用户
         */
        public static final int TYPE_AUTHOR_SEE_MAN = 6;
        /**
         * 给男用户推荐的女主播
         */
        public static final int TYPE_AUTHOR_REMMEND = 7;

        /**
         * 空闲
         */
        public static final int STATE_FREE = 1;
        /**
         * 忙线中
         */
        public static final int STATE_BUSY = 2;
        /**
         * 勿扰
         */
        public static final int STATE_NO_DISTRUB = 3;
        /**
         * 离线
         */
        public static final int STATE_NO_OUT = 3;
        /**
         * 启动跳转的参数
         */
        public static final String EXTRA_BUNDLE = "launchBundle";

    }

    // *************************************************** 消息tab常量 ***************************************************

    public static final class message {
        // 录音状态常量
        public static final int STATE_IDLE = 0;// 默认
        public static final int STATE_RECORDING = 1;// 录音中
        public static final int STATE_CANCELED = 2;// 取消录音
        // 聊天页Handler常量
        public static final int MSG_TYPE_VOICE_UI_TIME = 666;// 录音框UI计时
        public static final int MSG_TYPE_UPDATE = 888;// 发送消息后更新
        public static final int MSG_TYPE_TIMER = 0;// 计时
        public static final int MSG_TYPE_DELAYED = 1;// 延时
        public static final int MSG_TYPE_INIT = 2;// 初始化聊天记录
        public static final int MSG_TYPE_LOAD_MORE = 3;// 加载更多聊天记录
        public static final int MSG_TYPE_SEND_TXT = 4;// 发送文字消息
        public static final int MSG_TYPE_SEND_VOICE = 5;// 发送语音消息
        public static final int MSG_TYPE_SEND_IMAGE = 6;// 发送图片消息
        public static final int MSG_TYPE_ADD = 7;// 加载一线的聊天记录
    }

    // *************************************************** 支付页常量 ***************************************************

    public static final class pay {
        // google商品SKU
        public static final String SKU1 = "1000keys";
        public static final String SKU2 = "500keys";
        public static final String SKU3 = "150keys";
        public static final String SKU4 = "60keys";
        public static final String SKU5 = "30keys";
        public static final String SKU6 = "10keys";


        public static final String SKU9 = "10000jewel";
        public static final String SKU10 = "5000jewel";
        public static final String SKU11 = "3000jewel";
        public static final String SKU12 = "1000jewel";
        public static final String SKU13 = "600jewel";
        public static final String SKU14 = "100jewel";

        public static final String SKU8 = "30dayvip";
        public static final String SKU7 = "90dayvip";
//        public static final String SKU6 = "6pay";
//        public static final String SKU7 = "7pay";
        // 支付拦截来源
        public static final String FROM_TAG_PERSON = "1";// 个人中心页面
        public static final String FROM_TAG_HOMEPAGE = "2";// 首页列表项
        public static final String FROM_TAG_CHAT = "3";// 聊天页面
    }

    // *************************************************** 视频录制和上传 ***************************************************

    public static final class Video {
        public static final String DATA = "URL";
        public static final int REQUEST_CODE = 1001;
        public static final int RESULT_CODE = 2001;
        public static final String SD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "videorecord" + File.separator + "recording.mp4";
        public static final String NAME = "videorecord";
        public final static int SIZE_1 = 640;
        public final static int SIZE_2 = 480;
    }
}
