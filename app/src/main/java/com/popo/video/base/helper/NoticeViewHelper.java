package com.popo.video.base.helper;

import android.view.View;
import android.view.ViewGroup;

/**
 * 页面提示信息帮助类
 * Created by zhangdroid on 2017/5/17.
 */
public class NoticeViewHelper implements INoticeViewHelper {
    private View view;// 当前显示的view
    private ViewGroup parentView;// 当前显示的view的父view
    private int viewIndex;// 当前显示的view在父view中的索引
    private ViewGroup.LayoutParams layoutParams;// view布局参数

    public NoticeViewHelper(View view) {
        this.view = view;
        this.layoutParams = view.getLayoutParams();
        init();
    }

    @Override
    public View getView() {
        return view;
    }

    @Override
    public void restoreView() {
        showView(view);
    }

    @Override
    public void showView(View view) {
        if (parentView == null) {
            init();
        }
        // 如果当前需要显示的view之前已经显示，那就不需要进行替换操作
        if (parentView.getChildAt(viewIndex) != view) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
            parentView.removeViewAt(viewIndex);
            parentView.addView(view, viewIndex, layoutParams);
        }
    }

    private void init() {
        if (view.getParent() != null) {
            parentView = (ViewGroup) view.getParent();
        } else {// 系统的根布局
            parentView = (ViewGroup) view.getRootView().findViewById(android.R.id.content);
        }
        // 检测当前需要的view是否已经存在，若存在则保存其在父view中的索引
        int count = parentView.getChildCount();
        for (int index = 0; index < count; index++) {
            if (view == parentView.getChildAt(index)) {
                viewIndex = index;
                break;
            }
        }
    }

}
