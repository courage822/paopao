package com.popo.video.base;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

import com.danikula.videocache.HttpProxyCacheServer;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.popo.video.common.HyphenateHelper;
import com.popo.video.common.Util;
import com.popo.video.data.model.PlatformInfo;
import com.popo.video.data.preference.PlatformPreference;
import com.popo.video.db.DbModle;
import com.popo.okhttp.OkHttpHelper;
import com.popo.okhttp.log.LoggerInterceptor;
import com.popo.video.service.InitializeService;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by zhangdroid on 2017/5/11.
 */
public class BaseApplication extends Application {
    private static Context sApplicationContext;
    private HttpProxyCacheServer proxy;

    public static HttpProxyCacheServer getProxy(Context context) {
        BaseApplication app = (BaseApplication) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(this).maxCacheSize(1024 * 1024 * 1024)       // 1 Gb for cache
                .build();
    }
    public static Context getGlobalContext() {
        return sApplicationContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplicationContext = getApplicationContext();
        // 初始化环信
        HyphenateHelper.getInstance().init(this);
        Stetho.initializeWithDefaults(this);
        initOkhttpClient();
        InitializeService.start(this);
        DbModle.getInstance().init(this);
    }
    /**
     * 初始化OkHttp
     */
    private void initOkhttpClient() {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                // Okhttp log
                .addInterceptor(new LoggerInterceptor("AAAAAAAA"))
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        Log.e("AAAAAAA", "intercept: --------");
                        if(request.body() instanceof FormBody){
                            Log.e("AAAAAAA", "intercept: 6666666666666666666666");
                            FormBody oidFormBody = (FormBody) request.body();
                            if(oidFormBody!=null&&oidFormBody.size()>0){
                                for (int i = 0;i<oidFormBody.size();i++){
                                    Log.e("AAAAAAA", "intercept: ------name="+oidFormBody.encodedName(i)+"-------+=value="+oidFormBody.encodedValue(i));
                                }
                            }
                        }
                        return chain.proceed(request);
                    }
                })
                // Stetho网络调试
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
        OkHttpHelper.init(okHttpClient);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (PlatformPreference.getPlatformInfo() != null) {
            PlatformInfo platformInfo = PlatformPreference.getPlatformInfo();
            platformInfo.setLanguage(Util.getLacalLanguage());
            PlatformPreference.setPlatfromInfo(platformInfo);
        }
    }
}
