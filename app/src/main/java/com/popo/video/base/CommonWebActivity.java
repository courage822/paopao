package com.popo.video.base;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.library.net.NetChangedListener;
import com.popo.library.net.NetStateReceiver;
import com.popo.library.net.NetUtil;
import com.popo.library.util.DeviceUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.parcelable.WebParcelable;
import com.popo.video.ui.personalcenter.AuthenticationActivity;
import com.popo.video.base.helper.NoticeViewManager;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 通用网页展示页面
 * Created by zhangdroid on 2017/5/11.
 */
public class CommonWebActivity extends FragmentActivity {
    // 标题栏
    @BindView(R.id.common_web_tv_title)
    TextView mTvTitle;
    @BindView(R.id.common_web_iv_left)
    ImageView mIvLeft;
    @BindView(R.id.common_web_iv_right)
    ImageView mIvRight;
    // 进度条
    @BindView(R.id.web_load_progress)
    ProgressBar mProgressBar;
    // 网页
    @BindView(R.id.webView)
    WebView mWebView;
    // 底部按钮
    @BindView(R.id.web_bottom)
    Button mBtnBottom;

    // view替换显示管理类
    private NoticeViewManager mNoticeViewManager;
    // 网络状态监听器
    private NetChangedListener mNetChangedListener;
    // 显示网络错误提示
    private Snackbar mSnackbar;
    // 需要加载的网页url
    private String mUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setWindowFeature();
        setScreenOrientation();
        setContentView(R.layout.activity_common_web);
        ButterKnife.bind(this);
        // 透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            // 设置状态栏背景
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        init();
        initWebView();
        setListeners();
        mNoticeViewManager = new NoticeViewManager(mWebView);
        // 注册网络状况监听广播和监听器
        NetStateReceiver.registerNetworkStateReceiver(this);
        mNetChangedListener = new NetChangedListener() {
            @Override
            public void onNetConnected(NetUtil.NetType type) {
                if (null != mSnackbar) {
                    mSnackbar.dismiss();
                }
            }

            @Override
            public void onNetDisConnected() {
                mSnackbar = SnackBarUtil.showWithAction(mWebView, getString(R.string.tip_network_error), getString(R.string.goto_set), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DeviceUtil.showSysSetting(CommonWebActivity.this);
                        if (null != mSnackbar) {
                            mSnackbar.dismiss();
                        }
                    }
                });
            }
        };
        NetStateReceiver.setNetChangedListener(mNetChangedListener);
    }

    /**
     * 设置无标题栏等属性
     */
    protected void setWindowFeature() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    /**
     * 设置屏幕方向
     */
    protected void setScreenOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void init() {
        WebParcelable webParcelable = LaunchHelper.getInstance().getParcelableExtra(this);
        if (null != webParcelable) {
            mUrl = webParcelable.url;
            String title = webParcelable.title;
            // 设置网页标题
            if (!TextUtils.isEmpty(title)) {
                mTvTitle.setText(title);
            }
            mBtnBottom.setVisibility(webParcelable.showBottom ? View.VISIBLE : View.GONE);
        }
    }

    private void initWebView() {
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setDefaultTextEncodingName("UTF-8");
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setSupportZoom(false);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setDomStorageEnabled(true);
        webSettings.setLoadsImagesAutomatically(true);
        loadUrl();
    }

    private void setListeners() {
        mIvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mIvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 刷新网页
                loadUrl();
            }
        });
        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                mProgressBar.setProgress(newProgress);
            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return true;
            }
        });
        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressBar.setVisibility(View.VISIBLE);
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBar.setVisibility(View.GONE);
                toggleShowError(false, null, null);
            }

            @Override
            public void onReceivedError(WebView view, final WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                mProgressBar.setVisibility(View.GONE);
                toggleShowError(true, null, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadUrl();
                    }
                });
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                mProgressBar.setVisibility(View.GONE);
                toggleShowError(true, null, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadUrl();
                    }
                });
            }
        });
        // 点击跳转到播主认证页面
        mBtnBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LaunchHelper.getInstance().launchFinish(CommonWebActivity.this, AuthenticationActivity.class);
            }
        });
    }

    /**
     * 加载网页，若不存在则显示默认提示页面
     */
    private void loadUrl() {
        if (!TextUtils.isEmpty(mUrl)) {
             mWebView.loadUrl(mUrl);
        } else {
            toggleShowError(true, getString(R.string.tip_url_empty), null);
        }
    }

    @Override
    public void onBackPressed() {
        if (null != mWebView && mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NetStateReceiver.unregisterNetworkStateReceiver(this);
        NetStateReceiver.removeNetChangedListener(mNetChangedListener);
    }

    /**
     * Toggle show error
     *
     * @param toggle          true is show, otherwise dismiss
     * @param msg             error message to show
     * @param onClickListener called when the error view is clicked
     */
    protected void toggleShowError(boolean toggle, String msg, View.OnClickListener onClickListener) {
        View errorView = LayoutInflater.from(this).inflate(R.layout.common_error, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) errorView.findViewById(R.id.common_error_msg);
            tvMsg.setText(msg);
        }
        if (null != onClickListener) {
            LinearLayout linearLayout = (LinearLayout) errorView.findViewById(R.id.common_error);
            linearLayout.setOnClickListener(onClickListener);
        }
        showOrDismiss(toggle, errorView);
    }

    private void showOrDismiss(boolean toggle, View view) {
        if (null != mNoticeViewManager) {
            if (toggle) {
                mNoticeViewManager.show(view);
            } else {
                mNoticeViewManager.restore();
            }
        }
    }

}
