package com.popo.video.base.helper;

import android.view.View;

/**
 * 页面提示信息帮助接口
 * Created by zhangdroid on 2017/5/17.
 */
public interface INoticeViewHelper {

    /**
     * @return 当前正显示的view
     */
    View getView();

    /**
     * 重置当前显示的view（显示初始view）
     */
    void restoreView();

    /**
     * 替换当前布局下的view
     *
     * @param view 需要替换显示的view
     */
    void showView(View view);

}