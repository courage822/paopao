package com.popo.video.base.helper;

import android.view.View;

/**
 * 页面提示信息管理类，替换显示不同的提示信息页面（加载、网络错误、默认空页面）
 * Created by zhangdroid on 2017/5/17.
 */
public class NoticeViewManager {
    private INoticeViewHelper iNoticeViewHelper;

    public NoticeViewManager(View view) {
        this(new NoticeViewHelper(view));
    }

    public NoticeViewManager(INoticeViewHelper helper) {
        this.iNoticeViewHelper = helper;
    }

    /**
     * 显示指定的view
     *
     * @param view 需要显示的view
     */
    public void show(View view) {
        iNoticeViewHelper.showView(view);
    }

    /**
     * 重置
     */
    public void restore() {
        iNoticeViewHelper.restoreView();
    }

}
