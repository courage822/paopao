package com.popo.video.base;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.library.util.ArgumentUtil;
import com.popo.video.base.helper.NoticeViewManager;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;

/**
 * 所有Fragment的基类
 * Created by zhangdroid on 2017/5/11.
 */
public abstract class BaseFragment extends Fragment {
    private View mRootView;
    // 方便在实现类中使用Context对象
    protected Context mContext;
    // 方便在实现类中使用LayoutInflater对象
    protected LayoutInflater mLayoutInflater;
    // view替换显示管理类
    private NoticeViewManager mNoticeViewManager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isRegistEventBus()) {
            EventBus.getDefault().register(this);
        }
        // 获得传递过来的Bundle
        getArgumentParcelable(ArgumentUtil.getArgumentParcelable(getArguments()));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (null == mRootView) {
            if (getLayoutResId() != 0) {
                mRootView = inflater.inflate(getLayoutResId(), container, false);
            } else {
                mRootView = super.onCreateView(inflater, container, savedInstanceState);
            }
        }
        ButterKnife.bind(this, mRootView);

        // 设置LayoutInflater对象
        mLayoutInflater = inflater;
        // 设置提示信息帮助类
        if (null != getNoticeView()) {
            mNoticeViewManager = new NoticeViewManager(getNoticeView());
        }
        // 设置view和listener
        initViews();
        setListeners();
        // 加载数据
        loadData();
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isRegistEventBus()) {
            EventBus.getDefault().unregister(this);
        }
    }

    /**
     * 获得布局文件资源id
     *
     * @return R.layout.xx
     */
    protected abstract int getLayoutResId();

    /**
     * 是否注册EventBus
     */
    protected abstract boolean isRegistEventBus();

    /**
     * 获得提示信息页面显示的区域
     */
    protected abstract View getNoticeView();

    /**
     * 获得Arguments中传递过来的Parcelable
     *
     * @param parcelable
     */
    protected abstract void getArgumentParcelable(Parcelable parcelable);

    /**
     * 初始化view
     */
    protected abstract void initViews();

    /**
     * 设置监听器
     */
    protected abstract void setListeners();

    /**
     * 加载数据
     */
    protected abstract void loadData();

    //********************************************** 公用方法 **********************************************//

    /**
     * Toggle show loading
     *
     * @param toggle true is show, otherwise dismiss
     * @param msg    laoding message to show
     */
    protected void toggleShowLoading(boolean toggle, String msg) {
        View loadingView = mLayoutInflater.inflate(R.layout.common_loading, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) loadingView.findViewById(R.id.common_loading_msg);
            tvMsg.setText(msg);
        }
        showOrDismiss(toggle, loadingView);
    }

    /**
     * Toggle show empty
     *
     * @param toggle          true is show, otherwise dismiss
     * @param msg             empty message to show
     * @param onClickListener called when the empty view is clicked
     */
    protected void toggleShowEmpty(boolean toggle, String msg, View.OnClickListener onClickListener) {
        View emptyView = mLayoutInflater.inflate(R.layout.common_empty, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) emptyView.findViewById(R.id.common_empty_msg);
            tvMsg.setText(msg);
        }
        if (null != onClickListener) {
            LinearLayout linearLayout = (LinearLayout) emptyView.findViewById(R.id.common_empty);
            linearLayout.setOnClickListener(onClickListener);
        }
        showOrDismiss(toggle, emptyView);
    }

    /**
     * Toggle show error
     *
     * @param toggle          true is show, otherwise dismiss
     * @param msg             error message to show
     * @param onClickListener called when the error view is clicked
     */
    protected void toggleShowError(boolean toggle, String msg, View.OnClickListener onClickListener) {
        View errorView = mLayoutInflater.inflate(R.layout.common_error, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) errorView.findViewById(R.id.common_error_msg);
            tvMsg.setText(msg);
        }
        if (null != onClickListener) {
            LinearLayout linearLayout = (LinearLayout) errorView.findViewById(R.id.common_error);
            linearLayout.setOnClickListener(onClickListener);
        }
        showOrDismiss(toggle, errorView);
    }

    private void showOrDismiss(boolean toggle, View view) {
        if (null != mNoticeViewManager) {
            if (toggle) {
                mNoticeViewManager.show(view);
            } else {
                mNoticeViewManager.restore();
            }
        }
    }

}
