package com.popo.video.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.video.base.helper.NoticeViewManager;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;

/**
 * 所有带有标题栏的Fragment基类
 * Created by zhangdroid on 2017/5/11.
 */
public abstract class BaseTopBarFragment extends Fragment {
    private View mRootView;
    // 方便在实现类中使用Context对象
    protected Context mContext;
    // 方便在实现类中使用LayoutInflater对象
    protected LayoutInflater mLayoutInflater;
    // view替换显示管理类
    private NoticeViewManager mNoticeViewManager;
    // 标题栏
    private TextView mTvTitle;
    private TextView mTvLeft;
    private ImageView mIvLeft;
    private TextView mTvRight;
    private ImageView mIvRight;
    // 内容
    protected FrameLayout mFlContentContainer;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isRegistEventBus()) {
            EventBus.getDefault().register(this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_base_title, container, false);
            mTvTitle = (TextView) mRootView.findViewById(R.id.fragment_base_tv_title);
            mTvLeft = (TextView) mRootView.findViewById(R.id.fragment_base_tv_left);
            mIvLeft = (ImageView) mRootView.findViewById(R.id.fragment_base_iv_left);
            mTvRight = (TextView) mRootView.findViewById(R.id.fragment_base_tv_right);
            mIvRight = (ImageView) mRootView.findViewById(R.id.fragment_base_iv_right);
            mFlContentContainer = (FrameLayout) mRootView.findViewById(R.id.fragment_base_content);
            if (getLayoutResId() != 0) {
                View contentView = inflater.inflate(getLayoutResId(), null);
                ButterKnife.bind(this, contentView);
                if (mFlContentContainer.getChildCount() > 0) {
                    mFlContentContainer.removeAllViews();
                }
                mFlContentContainer.addView(contentView);
            }
        }
        // 设置LayoutInflater对象
        mLayoutInflater = inflater;
        // 设置提示信息帮助类
        mNoticeViewManager = new NoticeViewManager(mFlContentContainer);
        // 设置默认标题
        if (!TextUtils.isEmpty(getDefaultTitle())) {
            mTvTitle.setText(getDefaultTitle());
        }
        // 设置view和listener
        initViews();
        setListeners();
        // 加载数据
        loadData();
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isRegistEventBus()) {
            EventBus.getDefault().unregister(this);
        }
    }

    /**
     * 获得布局文件资源id
     *
     * @return R.layout.xx
     */
    protected abstract int getLayoutResId();

    /**
     * 设置默认标题
     */
    protected abstract String getDefaultTitle();

    /**
     * 是否注册EventBus
     */
    protected abstract boolean isRegistEventBus();

    /**
     * 初始化view
     */
    protected abstract void initViews();

    /**
     * 设置监听器
     */
    protected abstract void setListeners();

    /**
     * 加载数据
     */
    protected abstract void loadData();

    //********************************************** 公用方法 **********************************************//

    /**
     * 设置标题栏左菜单（文字）
     *
     * @param rightText       左菜单名称
     * @param onClickListener 左菜单点击监听器，为null表示不监听
     */
    protected void setLeftText(String rightText, View.OnClickListener onClickListener) {
        if (!TextUtils.isEmpty(rightText)) {
            mTvLeft.setText(rightText);
            mTvLeft.setVisibility(View.VISIBLE);
            if (null != onClickListener) {
                mTvLeft.setOnClickListener(onClickListener);
            }
        } else {
            mTvLeft.setVisibility(View.GONE);
        }
    }

    /**
     * 设置标题栏左菜单（图标）
     *
     * @param leftIconId      左菜单图标
     * @param onClickListener 左菜单点击监听器，为null表示不监听
     */
    protected void setLeftIcon(int leftIconId, View.OnClickListener onClickListener) {
        if (leftIconId != 0) {
            mIvLeft.setImageResource(leftIconId);
            mIvLeft.setVisibility(View.VISIBLE);
            if (null != onClickListener) {
                mIvLeft.setOnClickListener(onClickListener);
            }
        } else {
            mIvLeft.setVisibility(View.GONE);
        }
    }

    /**
     * 设置标题
     *
     * @param title
     */
    protected void setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            mTvTitle.setText(title);
        }
    }

    /**
     * 设置标题栏右菜单（文字）
     *
     * @param rightText       右菜单名称
     * @param onClickListener 右菜单点击监听器，为null表示不监听
     */
    protected void setRightText(String rightText, View.OnClickListener onClickListener) {
        if (!TextUtils.isEmpty(rightText)) {
            mTvRight.setText(rightText);
            mTvRight.setVisibility(View.VISIBLE);
            if (null != onClickListener) {
                mTvRight.setOnClickListener(onClickListener);
            }
        } else {
            mTvRight.setVisibility(View.GONE);
        }
    }

    /**
     * 设置标题栏右菜单（图标）
     *
     * @param rightIconId     右菜单图标
     * @param onClickListener 右菜单点击监听器，为null表示不监听
     */
    protected void setRightIcon(int rightIconId, View.OnClickListener onClickListener) {
        if (rightIconId != 0) {
            mIvRight.setImageResource(rightIconId);
            mIvRight.setVisibility(View.VISIBLE);
            if (null != onClickListener) {
                mIvRight.setOnClickListener(onClickListener);
            }
        } else {
            mIvRight.setVisibility(View.GONE);
        }
    }

    /**
     * Toggle show loading
     *
     * @param toggle true is show, otherwise dismiss
     * @param msg    laoding message to show
     */
    protected void toggleShowLoading(boolean toggle, String msg) {
        View loadingView = mLayoutInflater.inflate(R.layout.common_loading, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) loadingView.findViewById(R.id.common_loading_msg);
            tvMsg.setText(msg);
        }
        showOrDismiss(toggle, loadingView);
    }

    /**
     * Toggle show empty
     *
     * @param toggle          true is show, otherwise dismiss
     * @param msg             empty message to show
     * @param onClickListener called when the empty view is clicked
     */
    protected void toggleShowEmpty(boolean toggle, String msg, View.OnClickListener onClickListener) {
        View emptyView = mLayoutInflater.inflate(R.layout.common_empty, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) emptyView.findViewById(R.id.common_empty_msg);
            tvMsg.setText(msg);
        }
        if (null != onClickListener) {
            LinearLayout linearLayout = (LinearLayout) emptyView.findViewById(R.id.common_empty);
            linearLayout.setOnClickListener(onClickListener);
        }
        showOrDismiss(toggle, emptyView);
    }

    /**
     * Toggle show error
     *
     * @param toggle          true is show, otherwise dismiss
     * @param msg             error message to show
     * @param onClickListener called when the error view is clicked
     */
    protected void toggleShowError(boolean toggle, String msg, View.OnClickListener onClickListener) {
        View errorView = mLayoutInflater.inflate(R.layout.common_error, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) errorView.findViewById(R.id.common_error_msg);
            tvMsg.setText(msg);
        }
        if (null != onClickListener) {
            LinearLayout linearLayout = (LinearLayout) errorView.findViewById(R.id.common_error);
            linearLayout.setOnClickListener(onClickListener);
        }
        showOrDismiss(toggle, errorView);
    }

    private void showOrDismiss(boolean toggle, View view) {
        if (null != mNoticeViewManager) {
            if (toggle) {
                mNoticeViewManager.show(view);
            } else {
                mNoticeViewManager.restore();
            }
        }
    }

}
