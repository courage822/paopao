package com.popo.video.base;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.popo.video.R;
import com.popo.library.net.NetChangedListener;
import com.popo.library.net.NetStateReceiver;
import com.popo.library.net.NetUtil;
import com.popo.library.util.DeviceUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.base.helper.NoticeViewManager;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;

/**
 * 所有Activity的基类
 * Created by zhangdroid on 2017/5/11.
 */
public abstract class BaseFragmentActivity<P extends Parcelable> extends FragmentActivity {
    // 方便在实现类中使用Context对象
    protected Context mContext;
    // 方便在实现类中使用LayoutInflater对象
    protected LayoutInflater mLayoutInflater;
    // view替换显示管理类
    private NoticeViewManager mNoticeViewManager;
    // 网络状态监听器
    private NetChangedListener mNetChangedListener;
    // 显示网络错误提示
    private Snackbar mSnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setWindowFeature();
        setScreenOrientation();
        setWakeAndLack();
        super.onCreate(savedInstanceState);
        if (getLayoutResId() != 0) {
            setContentView(getLayoutResId());
            ButterKnife.bind(this);
        } else {
            throw new IllegalArgumentException("You must return a right contentView layout resource id.");
        }

        // 设置Context和LayoutInflater对象
        mContext = this;
        mLayoutInflater = LayoutInflater.from(this);
        // 设置提示信息帮助类
        if (null != getNoticeView()) {
            mNoticeViewManager = new NoticeViewManager(getNoticeView());
        }
        // 是否设置状态栏背景透明
        if (isApplyTranslucentStatusBar()) {
            setTranslucentStatusBar();
        }
        // 获得传递过来的Bundle对象
        getBundleExtras((P) LaunchHelper.getInstance().getParcelableExtra(this));
        // 设置view和listener
        initViews();
        setListeners();
        // 是否注册EventBus
        if (isRegistEventBus()) {
            EventBus.getDefault().register(this);
        }
        // 加载数据
        loadData();

        // 注册网络状况监听广播和监听器
        NetStateReceiver.registerNetworkStateReceiver(this);
        mNetChangedListener = new NetChangedListener() {
            @Override
            public void onNetConnected(NetUtil.NetType type) {
                if (null != mSnackbar) {
                    mSnackbar.dismiss();
                }
                networkConnected(type);
            }

            @Override
            public void onNetDisConnected() {
                showNetworkError();
                networkDisconnected();
            }
        };
        NetStateReceiver.setNetChangedListener(mNetChangedListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 移除监听器
        if (isRegistEventBus()) {
            EventBus.getDefault().unregister(this);
        }
        NetStateReceiver.unregisterNetworkStateReceiver(this);
        NetStateReceiver.removeNetChangedListener(mNetChangedListener);
    }

    /**
     * 设置状态栏背景透明
     */
    private void setTranslucentStatusBar() {
        setStatusBar(Color.TRANSPARENT);
    }

    /**
     * 获得布局文件资源id
     *
     * @return R.layout.xx
     */
    protected abstract int getLayoutResId();

    /**
     * 是否设置状态栏背景透明
     */
    protected abstract boolean isApplyTranslucentStatusBar();

    /**
     * 是否注册EventBus
     */
    protected abstract boolean isRegistEventBus();

    /**
     * 获得传递过来的Bundle数据（统一使用Parcelable）
     */
    protected abstract void getBundleExtras(P parcelable);

    /**
     * 获得提示信息页面显示的区域
     */
    protected abstract View getNoticeView();

    /**
     * 初始化view
     */
    protected abstract void initViews();

    /**
     * 设置监听器
     */
    protected abstract void setListeners();

    /**
     * 加载数据
     */
    protected abstract void loadData();

    /**
     * 网络连接
     *
     * @param type 当前连接的网络类型{@link NetUtil.NetType}
     */
    protected abstract void networkConnected(NetUtil.NetType type);

    /**
     * 无网络连接
     */
    protected abstract void networkDisconnected();

    //********************************************** 公用方法 **********************************************//

    /**
     * 设置无标题栏等属性
     */
    protected void setWindowFeature() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }
    /**
     * 设置视频发送过来点亮屏幕
     */
    protected void setWakeAndLack() {
        getWindow().addFlags(

                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON|
                        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    /**
     * 设置屏幕方向
     */
    protected void setScreenOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    /**
     * 设置状态栏背景色
     *
     * @param statusBarColorRes 5.0及以上系统可以设置状态栏背景色
     */
    protected void setStatusBar(int statusBarColorRes) {
        // 透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            // 设置状态栏背景
            getWindow().setStatusBarColor(statusBarColorRes);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    /**
     * Toggle show loading
     *
     * @param toggle true is show, otherwise dismiss
     * @param msg    laoding message to show
     */
    protected void toggleShowLoading(boolean toggle, String msg) {
        View loadingView = mLayoutInflater.inflate(R.layout.common_loading, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) loadingView.findViewById(R.id.common_loading_msg);
            tvMsg.setText(msg);
        }
        showOrDismiss(toggle, loadingView);
    }

    /**
     * Toggle show empty
     *
     * @param toggle          true is show, otherwise dismiss
     * @param msg             empty message to show
     * @param onClickListener called when the empty view is clicked
     */
    protected void toggleShowEmpty(boolean toggle, String msg, View.OnClickListener onClickListener) {
        View emptyView = mLayoutInflater.inflate(R.layout.common_empty, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) emptyView.findViewById(R.id.common_empty_msg);
            tvMsg.setText(msg);
        }
        if (null != onClickListener) {
            LinearLayout linearLayout = (LinearLayout) emptyView.findViewById(R.id.common_empty);
            linearLayout.setOnClickListener(onClickListener);
        }
        showOrDismiss(toggle, emptyView);
    }

    /**
     * Toggle show error
     *
     * @param toggle          true is show, otherwise dismiss
     * @param msg             error message to show
     * @param onClickListener called when the error view is clicked
     */
    protected void toggleShowError(boolean toggle, String msg, View.OnClickListener onClickListener) {
        View errorView = mLayoutInflater.inflate(R.layout.common_error, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) errorView.findViewById(R.id.common_error_msg);
            tvMsg.setText(msg);
        }
        if (null != onClickListener) {
            LinearLayout linearLayout = (LinearLayout) errorView.findViewById(R.id.common_error);
            linearLayout.setOnClickListener(onClickListener);
        }
        showOrDismiss(toggle, errorView);
    }

    private void showOrDismiss(boolean toggle, View view) {
        if (null != mNoticeViewManager) {
            if (toggle) {
                mNoticeViewManager.show(view);
            } else {
                mNoticeViewManager.restore();
            }
        }
    }

    protected void showNetworkError() {
        mSnackbar = SnackBarUtil.showWithAction(getWindow().getDecorView(), getString(R.string.tip_network_error), getString(R.string.goto_set), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeviceUtil.showSysSetting(BaseFragmentActivity.this);
            }
        });
    }

}
