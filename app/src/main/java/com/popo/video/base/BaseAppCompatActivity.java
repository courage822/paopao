package com.popo.video.base;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.popo.library.image.CropCircleTransformation;
import com.popo.library.image.ImageLoader;
import com.popo.library.image.ImageLoaderUtil;
import com.popo.library.net.NetChangedListener;
import com.popo.library.net.NetStateReceiver;
import com.popo.library.net.NetUtil;
import com.popo.library.util.DeviceUtil;
import com.popo.library.util.LaunchHelper;
import com.popo.library.util.SnackBarUtil;
import com.popo.video.R;
import com.popo.video.base.helper.NoticeViewManager;
import com.popo.video.common.RingManage;
import com.popo.video.common.Util;
import com.popo.video.common.VideoHelper;
import com.popo.video.data.api.ApiManager;
import com.popo.video.data.api.IGetDataListener;
import com.popo.video.data.model.BaseModel;
import com.popo.video.data.model.NettyMessage;
import com.popo.video.data.preference.DataPreference;
import com.popo.video.data.preference.UserPreference;
import com.popo.video.parcelable.VideoInviteParcelable;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;

/**
 * 所有Activity的基类
 * Created by zhangdroid on 2017/5/11.
 */
public abstract class BaseAppCompatActivity<P extends Parcelable> extends AppCompatActivity {
    // 方便在实现类中使用Context对象
    protected Context mContext;
    // 方便在实现类中使用LayoutInflater对象
    protected LayoutInflater mLayoutInflater;
    // view替换显示管理类
    private NoticeViewManager mNoticeViewManager;
    // 网络状态监听器
    private NetChangedListener mNetChangedListener;
    // 显示网络错误提示
    private Snackbar mSnackbar;
    private PopupWindow pop_window;
    private TextView tv_accept;
    private TextView tv_refuse;
    private ImageView iv_avatar;
    private TextView tv_nickname;
    private TextView tv_video_or_voice;
    private boolean isCanShow=false;
    private  int screenWidth,screenHeight;
    private boolean isPast=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setWindowFeature();
        setScreenOrientation();
        super.onCreate(savedInstanceState);
        if (getLayoutResId() != 0) {
            setContentView(getLayoutResId());
            ButterKnife.bind(this);
        } else {
            throw new IllegalArgumentException("You must return a right contentView layout resource id.");
        }
        // 设置Context和LayoutInflater对象
        mContext = this;
        mLayoutInflater = LayoutInflater.from(this);
        // 设置提示信息帮助类
        if (null != getNoticeView()) {
            mNoticeViewManager = new NoticeViewManager(getNoticeView());
        }
        // 是否设置状态栏背景透明
        if (isApplyTranslucentStatusBar()) {
            setTranslucentStatusBar();
        }
        // 获得传递过来的Bundle对象
        getBundleExtras((P) LaunchHelper.getInstance().getParcelableExtra(this));
        // 设置view和listener
        initViews();
        setListeners();
        // 是否注册EventBus
        if (isRegistEventBus()) {
            EventBus.getDefault().register(this);
        }
        // 加载数据
        loadData();




        // 注册网络状况监听广播和监听器
        NetStateReceiver.registerNetworkStateReceiver(this);
        mNetChangedListener = new NetChangedListener() {
            @Override
            public void onNetConnected(NetUtil.NetType type) {
                if (null != mSnackbar) {
                    mSnackbar.dismiss();
                }
                networkConnected(type);
            }

            @Override
            public void onNetDisConnected() {
                showNetworkError();
                networkDisconnected();
            }
        };
        NetStateReceiver.setNetChangedListener(mNetChangedListener);
        getPopupWindow();//添加popuWindows
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widthPixels = dm.widthPixels;
        int heightPixels = dm.heightPixels;
        float density = dm.density;
        screenWidth = (int) (widthPixels * density);
        screenHeight = (int) (heightPixels * density);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 移除监听器
        if (isRegistEventBus()) {
            EventBus.getDefault().unregister(this);
        }
        NetStateReceiver.unregisterNetworkStateReceiver(this);
        NetStateReceiver.removeNetChangedListener(mNetChangedListener);
    }

    /**
     * 设置状态栏背景透明
     */
    private void setTranslucentStatusBar() {
        setStatusBar(Color.TRANSPARENT);
    }

    /**
     * 获得布局文件资源id
     *
     * @return R.layout.xx
     */
    protected abstract int getLayoutResId();

    /**
     * 是否设置状态栏背景透明
     */
    protected abstract boolean isApplyTranslucentStatusBar();

    /**
     * 是否注册EventBus
     */
    protected abstract boolean isRegistEventBus();

    /**
     * 获得传递过来的Bundle数据（统一使用Parcelable）
     */
    protected abstract void getBundleExtras(P parcelable);

    /**
     * 获得提示信息页面显示的区域
     */
    protected abstract View getNoticeView();

    /**
     * 初始化view
     */
    protected abstract void initViews();

    /**
     * 设置监听器
     */
    protected abstract void setListeners();

    /**
     * 加载数据
     */
    protected abstract void loadData();

    /**
     * 网络连接
     *
     * @param type 当前连接的网络类型{@link NetUtil.NetType}
     */
    protected abstract void networkConnected(NetUtil.NetType type);

    /**
     * 无网络连接
     */
    protected abstract void networkDisconnected();

    //********************************************** 公用方法 **********************************************//

    /**
     * 设置无标题栏等属性
     */
    protected void setWindowFeature() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    /**
     * 设置屏幕方向
     */
    protected void setScreenOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    /**
     * 设置状态栏背景色
     *
     * @param statusBarColorRes 5.0及以上系统可以设置状态栏背景色
     */
    protected void setStatusBar(int statusBarColorRes) {
        // 透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            // 设置状态栏背景
            getWindow().setStatusBarColor(statusBarColorRes);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    /**
     * Toggle show loading
     *
     * @param toggle true is show, otherwise dismiss
     * @param msg    laoding message to show
     */
    protected void toggleShowLoading(boolean toggle, String msg) {
        View loadingView = mLayoutInflater.inflate(R.layout.common_loading, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) loadingView.findViewById(R.id.common_loading_msg);
            tvMsg.setText(msg);
        }
        showOrDismiss(toggle, loadingView);
    }

    /**
     * Toggle show empty
     *
     * @param toggle          true is show, otherwise dismiss
     * @param msg             empty message to show
     * @param onClickListener called when the empty view is clicked
     */
    protected void toggleShowEmpty(boolean toggle, String msg, View.OnClickListener onClickListener) {
        View emptyView = mLayoutInflater.inflate(R.layout.common_empty, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) emptyView.findViewById(R.id.common_empty_msg);
            tvMsg.setText(msg);
        }
        if (null != onClickListener) {
            LinearLayout linearLayout = (LinearLayout) emptyView.findViewById(R.id.common_empty);
            linearLayout.setOnClickListener(onClickListener);
        }
        showOrDismiss(toggle, emptyView);
    }

    /**
     * 视频邀请信的悬浮框
     */
    protected void togglePopuWindow() {
        String yeJiaoJsonData = DataPreference.getYeJiaoJsonData();
        if(!TextUtils.isEmpty(yeJiaoJsonData)){
            final NettyMessage   strategyMsg = new Gson().fromJson(yeJiaoJsonData, NettyMessage.class);
            if (strategyMsg != null) {
                tv_nickname.setText(strategyMsg.getSendUserName());
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(strategyMsg.getSendUserIcon()).transform(new CropCircleTransformation(mContext))
                        .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(iv_avatar).build());
                tv_refuse.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (pop_window != null) {
                            pop_window.dismiss();
                            isPast=false;
                            DataPreference.saveYeJiaoTime(System.currentTimeMillis()-60*1000);//把时间调成已经过期状态
                        }
                        setMsgFlag(String.valueOf(strategyMsg.getSendUserId()),"2");
                        RingManage.getInstance().closeRing();
                    }
                });
                tv_accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //在这里跳转处理
//                        pop_window.dismiss();
//                        RingManage.getInstance().closeRing();
//                        DataPreference.saveYeJiaoTime(System.currentTimeMillis()+60*1000);//把时间调成已经过期状态
                        VideoHelper.startVideoInvite(new VideoInviteParcelable(false, strategyMsg.getSendUserId(), strategyMsg.getSendUserAccount()
                                , strategyMsg.getSendUserName(),strategyMsg.getSendUserIcon(), 0, 0), mContext,"1");
                        DataPreference.saveYejiaoString(1);
                        setMsgFlag(String.valueOf(strategyMsg.getSendUserId()),"1");
                        isPast=false;
                    }
                });
            }
        }
    }

    /**
     * 创建页脚对话框
     */
    private void getPopupWindow() {//单例模式最后考虑
        View contentView = mLayoutInflater.inflate(R.layout.common_popu_window, null);
        tv_accept = (TextView) contentView.findViewById(R.id.base_activity_popu_tv_accept);
        tv_refuse = (TextView) contentView.findViewById(R.id.base_activity_popu_tv_refuse);
        iv_avatar = (ImageView) contentView.findViewById(R.id.base_activity_popu_iv_avatar);
        tv_nickname = (TextView) contentView.findViewById(R.id.base_activity_popu_tv_nickname);
        tv_video_or_voice = (TextView) contentView.findViewById(R.id.base_activity_popu_tv_video_or_voice);
        pop_window = new PopupWindow(contentView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
        contentView.setFocusable(false); // 这个很重要
        contentView.setFocusableInTouchMode(false);
        pop_window.setFocusable(false);

    }

    /**
     * 判断是否能够创建悬浮页脚
     */
    protected void isCanShowPop(boolean flag){
        if(flag){
            togglePopuWindow();//悬浮页脚的数据添加
            if (pop_window != null) {
                new Handler().postDelayed(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    public void run() {
                        if (!isFinishing()) {
                            if(UserPreference.isMale()){
                                if(screenHeight==2416){
                                    pop_window.showAtLocation(getNoticeView(), Gravity.CENTER, 0,  380);
                                }
                                if(screenHeight==2560){
                                    pop_window.showAtLocation(getNoticeView(), Gravity.CENTER, 0,  450);
                                }
                                if(screenHeight==4709){
                                    pop_window.showAtLocation(getNoticeView(), Gravity.CENTER, 0,  580);
                                }
                                if(screenHeight==5040){
                                    pop_window.showAtLocation(getNoticeView(), Gravity.CENTER, 0,  680);
                                }
                                if(screenHeight==5760){
                                    pop_window.showAtLocation(getNoticeView(), Gravity.CENTER, 0,  650);
                                }
                                if(screenHeight<2416){
                                    pop_window.showAtLocation(getNoticeView(), Gravity.CENTER, 0,  360);
                                }else if(screenHeight>2416&&screenHeight<2560){
                                    pop_window.showAtLocation(getNoticeView(), Gravity.CENTER, 0,  430);
                                }else if(screenHeight>2560&&screenHeight<4709){
                                    pop_window.showAtLocation(getNoticeView(), Gravity.CENTER, 0,  520);
                                }else if(screenHeight>4709&&screenHeight<5040){
                                    pop_window.showAtLocation(getNoticeView(), Gravity.CENTER, 0,  620);
                                }else if(screenHeight>5040&&screenHeight<5760){
                                    pop_window.showAtLocation(getNoticeView(), Gravity.CENTER, 0,  630);
                                }else if(screenHeight>5760){
                                    pop_window.showAtLocation(getNoticeView(), Gravity.CENTER, 0,  680);
                                }

                                RingManage.getInstance().openRing();
                            }
                        }
                    }
                }, 2000);
                handler.sendEmptyMessageDelayed(2,50 * 1000);
            }
        }
    }
    /**
     * 关闭悬浮窗与声音
     */
    protected void closeShowPop(){
        if (pop_window != null) {
            pop_window.dismiss();
        }
        RingManage.getInstance().closeRing();
        isPast=false;
    }
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 2:
                    if (pop_window != null) {
                        pop_window.dismiss();
                        DataPreference.saveYeJiaoTime(System.currentTimeMillis()-60*1000);//把时间调成已经过期状态
                    }
                    RingManage.getInstance().closeRing();
                    if (isPast) {//还没有过期
                        String yeJiaoJsonData = DataPreference.getYeJiaoJsonData();
                        if(!TextUtils.isEmpty(yeJiaoJsonData)) {
                            final NettyMessage strategyMsg = new Gson().fromJson(yeJiaoJsonData, NettyMessage.class);
                            if (strategyMsg != null) {
                                setMsgFlag(String.valueOf(strategyMsg.getSendUserId()),"3");
                            }
                        }
                    }
                    break;
            }
        }
    };

    /**
     * Toggle show error
     *
     * @param toggle          true is show, otherwise dismiss
     * @param msg             error message to show
     * @param onClickListener called when the error view is clicked
     */
    protected void toggleShowError(boolean toggle, String msg, View.OnClickListener onClickListener) {
        View errorView = mLayoutInflater.inflate(R.layout.common_error, null);
        if (!TextUtils.isEmpty(msg)) {
            TextView tvMsg = (TextView) errorView.findViewById(R.id.common_error_msg);
            tvMsg.setText(msg);
        }
        if (null != onClickListener) {
            LinearLayout linearLayout = (LinearLayout) errorView.findViewById(R.id.common_error);
            linearLayout.setOnClickListener(onClickListener);
        }
        showOrDismiss(toggle, errorView);
    }

    private void showOrDismiss(boolean toggle, View view) {
        if (null != mNoticeViewManager) {
            if (toggle) {
                mNoticeViewManager.show(view);
            } else {
                mNoticeViewManager.restore();
            }
        }
    }

    protected void showNetworkError() {
        mSnackbar = SnackBarUtil.showWithAction(getWindow().getDecorView(), getString(R.string.tip_network_error), getString(R.string.goto_set), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeviceUtil.showSysSetting(BaseAppCompatActivity.this);
            }
        });
    }
    private static void setMsgFlag(String remoteId,String extendTag){
        ApiManager.userActivityTag(UserPreference.getId(), remoteId, "8", extendTag, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

}
