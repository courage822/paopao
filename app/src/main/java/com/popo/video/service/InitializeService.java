package com.popo.video.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.popo.video.C;
import com.popo.library.util.CrashHelper;
import com.popo.okhttp.OkHttpHelper;
import com.popo.okhttp.log.LoggerInterceptor;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Application initialize intent service(just in non-ui thread)
 */
public class InitializeService extends IntentService {

    public InitializeService() {
        super("InitializeService");
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, InitializeService.class);
        intent.setAction(C.service.ACTION_APP_CREATE);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (!TextUtils.isEmpty(action) && C.service.ACTION_APP_CREATE.equals(action)) {
                // 初始化异常收集
                CrashHelper.getInstance().init(this);
                // 初始化Stetho
//                Stetho.initializeWithDefaults(this);
                // 初始化OkHttpClient
//                initOkhttpClient();
            }
        }
    }

    /**
     * 初始化OkHttp
     */
    private void initOkhttpClient() {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                // Okhttp log
                .addInterceptor(new LoggerInterceptor("AAAAAAAA"))
                 .addInterceptor(new Interceptor() {
                     @Override
                     public Response intercept(Chain chain) throws IOException {
                         Request request = chain.request();
                         Log.e("AAAAAAA", "intercept: --------");
                         if(request.body() instanceof FormBody){
                             Log.e("AAAAAAA", "intercept: 6666666666666666666666");
                             FormBody oidFormBody = (FormBody) request.body();
                             if(oidFormBody!=null&&oidFormBody.size()>0){
                                 for (int i = 0;i<oidFormBody.size();i++){
                                     Log.e("AAAAAAA", "intercept: ------name="+oidFormBody.encodedName(i)+"-------+=value="+oidFormBody.encodedValue(i));
                                 }
                             }
                         }
                         return chain.proceed(request);
                     }
                 })
                // Stetho网络调试
                 .addNetworkInterceptor(new StethoInterceptor())
                .build();
        OkHttpHelper.init(okHttpClient);
    }

}
