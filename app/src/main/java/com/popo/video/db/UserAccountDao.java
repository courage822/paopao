package com.popo.video.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.popo.video.data.model.AccountPwdInfo;
import com.popo.video.data.model.HuanXinUser;
import com.popo.video.data.model.PicInfo;
import com.popo.video.data.model.QaMsg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 2017/7/14.
 */

public class UserAccountDao {
    private MyDbHelper dbHelper;

    public UserAccountDao(Context context) {
        dbHelper = new MyDbHelper(context);
    }

    public void addAccountAndPwd(String account, String pwd) {
        AccountPwdInfo info = getAccountAndPwdByAccount(account);
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserAccountTable.USER_ACCOUNT, account);
        values.put(UserAccountTable.USER_PWD, pwd);
        if (info != null){
            readableDatabase.update(UserAccountTable.TABLE_NAME4, values, UserAccountTable.USER_ACCOUNT + " = ?", new String[]{info.getAccount()});
        }else{
            readableDatabase.replace(UserAccountTable.TABLE_NAME4, null, values);
        }
    }

    private AccountPwdInfo getAccountAndPwdByAccount(String account) {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        String sql = "select * from " + UserAccountTable.TABLE_NAME4 + " where " + account + "=?";
        Cursor cursor = readableDatabase.rawQuery(sql, new String[]{account});
        AccountPwdInfo info = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                info.setAccount(cursor.getString(cursor.getColumnIndex(UserAccountTable.USER_ACCOUNT)));
                info.setPwd(cursor.getString(cursor.getColumnIndex(UserAccountTable.USER_PWD)));

            }
            cursor.close();
        }
        return info;
    }
    public List<AccountPwdInfo> getAllAccountPwd(){
        List<AccountPwdInfo> mlist = new ArrayList<>();
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        String sql = "select * from " + UserAccountTable.TABLE_NAME4;
        Cursor cursor = readableDatabase.rawQuery(sql, null);
        AccountPwdInfo info = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                info = new AccountPwdInfo();
                info.setAccount(cursor.getString(cursor.getColumnIndex(UserAccountTable.USER_ACCOUNT)));
                info.setPwd(cursor.getString(cursor.getColumnIndex(UserAccountTable.USER_PWD)));
                mlist.add(info);
            }
            cursor.close();
        }
        return mlist;
    }


    //添加用戶
    public void addAccount(HuanXinUser user) {
        //插入数据之前先要判断数据库中是否存在这条数据
        HuanXinUser accountByHyID = getAccountByHyID(user.getHxId());
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserAccountTable.COL_HXNAME, user.getHxName());
        values.put(UserAccountTable.COL_HXICON, user.getHxIcon());
        values.put(UserAccountTable.COL_HXID, user.getHxId());
        values.put(UserAccountTable.COL_ISREAD, user.getIsRead());
        values.put(UserAccountTable.COL_ACCOUNT, user.getAccount());
        values.put(UserAccountTable.COL_LASTMSG, user.getLastMsg());
        values.put(UserAccountTable.COL_MSGNUM, user.getMsgNum());
        values.put(UserAccountTable.COL_MSGTIME, user.getMsgTime());
        values.put(UserAccountTable.COL_EXTENDTYPE, user.getExtendType());
        if (accountByHyID != null) {
            readableDatabase.update(UserAccountTable.TABLE_NAME, values, UserAccountTable.COL_HXID + " = ?", new String[]{user.getHxId()});
        } else {
            readableDatabase.replace(UserAccountTable.TABLE_NAME, null, values);
        }
    }


    //根据环信ID获取用户
    public HuanXinUser getAccountByHyID(String hyxi) {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        String sql = "select * from " + UserAccountTable.TABLE_NAME + " where " + UserAccountTable.COL_HXID + "=?";
        Cursor cursor = readableDatabase.rawQuery(sql, new String[]{hyxi});
        HuanXinUser huanXinUser = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                huanXinUser = new HuanXinUser();
                huanXinUser.setHxName(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXNAME)));
                huanXinUser.setHxIcon(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXICON)));
                huanXinUser.setHxId(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXID)));
                huanXinUser.setIsRead(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_ISREAD)));
                huanXinUser.setAccount(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_ACCOUNT)));
                huanXinUser.setLastMsg(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_LASTMSG)));
                huanXinUser.setMsgNum(cursor.getInt(cursor.getColumnIndex(UserAccountTable.COL_MSGNUM)));
                huanXinUser.setMsgTime(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_MSGTIME)));
                huanXinUser.setExtendType(cursor.getInt(cursor.getColumnIndex(UserAccountTable.COL_EXTENDTYPE)));
            }
            cursor.close();
        }

        return huanXinUser;
    }

    //获取所有用户的集合
    public List<HuanXinUser> getAllAcount() {
        List<HuanXinUser> huanXinUsers = new ArrayList<>();
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        String sql = "select * from " + UserAccountTable.TABLE_NAME;
        Cursor cursor = readableDatabase.rawQuery(sql, null);
        HuanXinUser huanXinUser = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                huanXinUser = new HuanXinUser();
                huanXinUser.setHxName(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXNAME)));
                huanXinUser.setHxIcon(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXICON)));
                huanXinUser.setHxId(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXID)));
                huanXinUser.setIsRead(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_ISREAD)));
                huanXinUser.setAccount(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_ACCOUNT)));
                huanXinUser.setLastMsg(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_LASTMSG)));
                huanXinUser.setMsgTime(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_MSGTIME)));
                huanXinUser.setMsgNum(cursor.getInt(cursor.getColumnIndex(UserAccountTable.COL_MSGNUM)));
                huanXinUser.setExtendType(cursor.getInt(cursor.getColumnIndex(UserAccountTable.COL_EXTENDTYPE)));
//                EMConversation conversation = EMClient.getInstance().chatManager().getConversation(huanXinUser.getAccount());
//                if(conversation!=null){
//                    huanXinUser.setMsgNum(conversation.getUnreadMsgCount());
//                    }
                huanXinUsers.add(huanXinUser);
                Collections.sort(huanXinUsers);
            }
            cursor.close();
        }
        return huanXinUsers;
    }

    ;

    //获取信息是否已经读取的状态值
    public int getState(String hyxi) {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        String sql = "select * from " + UserAccountTable.TABLE_NAME + " where " + UserAccountTable.COL_HXID + "=?";
        Cursor cursor = readableDatabase.rawQuery(sql, new String[]{hyxi});
        HuanXinUser huanXinUser = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                huanXinUser = new HuanXinUser();
                huanXinUser.setHxName(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXNAME)));
                huanXinUser.setHxIcon(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXICON)));
                huanXinUser.setHxId(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXID)));
                huanXinUser.setIsRead(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_ISREAD)));
                huanXinUser.setAccount(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_ACCOUNT)));
                huanXinUser.setLastMsg(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_LASTMSG)));
                huanXinUser.setMsgNum(cursor.getInt(cursor.getColumnIndex(UserAccountTable.COL_MSGNUM)));
                huanXinUser.setMsgTime(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_MSGTIME)));
                huanXinUser.setExtendType(cursor.getInt(cursor.getColumnIndex(UserAccountTable.COL_EXTENDTYPE)));
            }
            cursor.close();
        }
        if (huanXinUser != null) {
            String isRead = huanXinUser.getIsRead();
            if (!TextUtils.isEmpty(isRead)) {
                int i = Integer.parseInt(isRead);
                return i;
            }
        }
        return 1;
    }

    //修改信息是否已经读取的状态值
    public void setState(HuanXinUser user, boolean flag) {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        if (true) {//已经读取
            values.put(UserAccountTable.COL_ISREAD, "0");
        } else {//尚未读取
            values.put(UserAccountTable.COL_ISREAD, "1");
        }
        readableDatabase.update(UserAccountTable.TABLE_NAME, values, UserAccountTable.COL_HXID + " = ?", new String[]{user.getHxId()});
    }

    //设置消息的数量
    public void setMsgNum(HuanXinUser user) {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserAccountTable.COL_MSGNUM, "0");
        readableDatabase.update(UserAccountTable.TABLE_NAME, values, UserAccountTable.COL_HXID + " = ?", new String[]{user.getHxId()});
    }

    //删除数据库中所有的数据
    public void deleteAllData() {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        readableDatabase.delete(UserAccountTable.TABLE_NAME, null, null);
    }

    //根据id删除指定的数据
    public void deleteID(String id) {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        readableDatabase.delete(UserAccountTable.TABLE_NAME, "hxid=?", new String[]{id});
    }

    //查看未读消息是否为0
    public int selectSqlit() {
        int msgNum = 0;
        List<HuanXinUser> allAcount = getAllAcount();
        if (allAcount != null && allAcount.size() > 0) {
            for (HuanXinUser user : allAcount) {
                if (user.getMsgNum() > 0) {
                    msgNum = user.getMsgNum();
                    break;
                } else {
                    msgNum = 0;
                }
            }
            return msgNum;
        } else {
            return 0;
        }
    }
    //*************************************************************************************************************************

    //获取总的唯独消息数量
    public int queryMsgNum() {
        int msgNum = 0;
        List<HuanXinUser> allAcount = getAllAcount();
        for (HuanXinUser user : allAcount) {
            msgNum += user.getMsgNum();
        }
        return msgNum;
    }

    //添加用戶
    public void addPicInfo(PicInfo picInfo) {
        //插入数据之前先要判断数据库中是否存在这条数据
        PicInfo accountByHyID = getPicInfo(picInfo.getPicId());
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserAccountTable.COL_PICID, picInfo.getPicId());
        values.put(UserAccountTable.COL_PICNAME, picInfo.getPicName());
        values.put(UserAccountTable.COL_PICPRICE, picInfo.getPicPrice());
        values.put(UserAccountTable.COL_PICNUM, picInfo.getPicIcon());
        if (accountByHyID != null) {
            readableDatabase.update(UserAccountTable.TABLE_NAME2, values, UserAccountTable.COL_PICID + " = ?", new String[]{picInfo.getPicId()});
        } else {
            readableDatabase.replace(UserAccountTable.TABLE_NAME2, null, values);
        }
    }

    //根据图片ID查询图片信息
    public PicInfo getPicInfo(String info) {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        String sql = "select * from " + UserAccountTable.TABLE_NAME2 + " where " + UserAccountTable.COL_PICID + "=?";
        Cursor cursor = readableDatabase.rawQuery(sql, new String[]{info});
        PicInfo picInfo = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                picInfo = new PicInfo();
                picInfo.setPicId(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_PICID)));
                picInfo.setPicName(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_PICNAME)));
                picInfo.setPicPrice(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_PICPRICE)));
                picInfo.setPicIcon(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_PICNUM)));
            }
            cursor.close();
        }

        return picInfo;
    }
    //获取所有用户的集合
    public List<PicInfo> getAllPic() {
        List<PicInfo> picInfos = new ArrayList<>();
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        String sql = "select * from " + UserAccountTable.TABLE_NAME2;
        Cursor cursor = readableDatabase.rawQuery(sql, null);
        PicInfo picInfo = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                picInfo = new PicInfo();
                picInfo.setPicId(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_PICID)));
                picInfo.setPicName(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_PICNAME)));
                picInfo.setPicPrice(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_PICPRICE)));
                picInfo.setPicIcon(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_PICNUM)));
                picInfos.add(picInfo);
            }
            cursor.close();
        }
        return picInfos;
    }


    //保存QA信息的数据
    public void addQaMsg(QaMsg msgId) {
        //插入数据之前先要判断数据库中是否存在这条数据
        QaMsg qaMsg = getQaMsg(msgId.getMsgId());
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserAccountTable.COL_MSGID, msgId.getMsgId());
        values.put(UserAccountTable.COL_USERID, msgId.getUserId());
        values.put(UserAccountTable.COL_FLAG, msgId.getFlag());
        if (qaMsg != null) {
        } else {
            readableDatabase.replace(UserAccountTable.TABLE_NAME3, null, values);
        }

    }

    //根据id查询QA消息
    public QaMsg getQaMsg(String msgId) {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        String sql = "select * from " + UserAccountTable.TABLE_NAME3 + " where " + UserAccountTable.COL_MSGID + "=?";
        Cursor cursor = readableDatabase.rawQuery(sql, new String[]{msgId});
        QaMsg qaMsg = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                qaMsg = new QaMsg();
                qaMsg.setMsgId(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_MSGID)));
                qaMsg.setUserId(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_USERID)));
                qaMsg.setFlag(cursor.getInt(cursor.getColumnIndex(UserAccountTable.COL_FLAG)));
            }
            cursor.close();
        }
        return qaMsg;
    }

    //根据id查询信息，看是否已经读取  0为已经读取，1位尚未读取
    public int getIsRead(String msgId) {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        String sql = "select * from " + UserAccountTable.TABLE_NAME3 + " where " + UserAccountTable.COL_MSGID + "=?";
        Cursor cursor = readableDatabase.rawQuery(sql, new String[]{msgId});
        QaMsg qaMsg = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                qaMsg = new QaMsg();
                qaMsg.setMsgId(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_MSGID)));
                qaMsg.setUserId(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_USERID)));
                qaMsg.setFlag(cursor.getInt(cursor.getColumnIndex(UserAccountTable.COL_FLAG)));
            }
            cursor.close();
        }
        if (qaMsg != null) {
            return qaMsg.getFlag();
        }
        return 1;
    }

    //设置标记这个信息已经读取完毕
    public void setMsgFlag(QaMsg msgId) {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserAccountTable.COL_FLAG, 0);//修改去读状态，此时为已经读取
        readableDatabase.update(UserAccountTable.TABLE_NAME3, values, UserAccountTable.COL_MSGID + " = ?", new String[]{msgId.getMsgId()});
    }

    //删除数据库中QA
    public void deleteQaAllData() {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        readableDatabase.delete(UserAccountTable.TABLE_NAME3, null, null);
    }
}
