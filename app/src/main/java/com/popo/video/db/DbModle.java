package com.popo.video.db;

import android.content.Context;

/**
 * Created by Administrator on 2017/7/14.
 */

public class DbModle {
    private static DbModle sInstance;
    private UserAccountDao userAccountDao;
    public DbModle(){

    }
    public static DbModle getInstance() {
        if (null == sInstance) {
            synchronized (DbModle.class) {
                if (null == sInstance) {
                    sInstance = new DbModle();
                }
            }
        }
        return sInstance;
    }
    public void init(Context context){
        userAccountDao=new UserAccountDao(context);
    };
    public UserAccountDao getUserAccountDao(){
        return userAccountDao;
    }
}
