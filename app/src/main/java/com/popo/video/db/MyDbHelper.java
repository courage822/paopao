package com.popo.video.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrator on 2017/7/14.
 */

public class MyDbHelper extends SQLiteOpenHelper{
    public MyDbHelper(Context context) {
        super(context, "account.db", null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
       db.execSQL(UserAccountTable.CREATE_TABLE);//创建数据库  存储信箱列表
       db.execSQL(UserAccountTable.CREATE_TABLE2);//创建数据库 存储礼物
        db.execSQL(UserAccountTable.CREATE_TABLE3);//创建数据库  QA消息是否展示的数据记录
        db.execSQL(UserAccountTable.CREATE_TABLE4);//创建数据库  用户信息，用在登陆账号选择
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
