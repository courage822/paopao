package com.popo.video.db;

/**
 * Created by Administrator on 2017/7/14.
 */

public class UserAccountTable {
    public static final String TABLE_NAME="tab_account";
    public static final String COL_HXID="hxid";
    public static final String COL_HXNAME="hxName";
    public static final String COL_HXICON="hxIcon";
    public static final String COL_ISREAD="isRead";
    public static final String COL_ACCOUNT="account";
    public static final String COL_LASTMSG="lastMsg";
    public static final String COL_MSGNUM="msgNum";
    public static final String COL_MSGTIME="msgTime";
    public static final String COL_EXTENDTYPE="extendType";
    public static final String CREATE_TABLE="create table "
            +TABLE_NAME+" ("
            +COL_HXID+" text primary key,"
            +COL_HXNAME+" text,"
            +COL_LASTMSG+" text,"
            +COL_HXICON+" text,"
            +COL_ACCOUNT+" text,"
            +COL_MSGNUM+" integer,"
            +COL_MSGTIME+" text,"
            +COL_EXTENDTYPE+" integer,"
            +COL_ISREAD+" text);";

   //图片名称 id，图片价格,数量
    public static final String TABLE_NAME2="tab_pic";
    public static final String COL_PICID="giftId";
    public static final String COL_PICNAME="giftName";
    public static final String COL_PICPRICE="price";
    public static final String COL_PICNUM="giftUrl";
    public static final String CREATE_TABLE2="create table "
            +TABLE_NAME2+" ("
            +COL_PICID+" text primary key,"
            +COL_PICNAME+" text,"
            +COL_PICPRICE+" text,"
            +COL_PICNUM+" text);";

    //QA消息是否已经展示
    public static final String TABLE_NAME3="tab_qa";
    public static final String COL_MSGID="msgId";
    public static final String COL_USERID="userId";
    public static final String COL_FLAG="flag";
    public static final String CREATE_TABLE3="create table "
            +TABLE_NAME3+" ("
            +COL_MSGID+" text primary key,"
            +COL_USERID+" text,"
            +COL_FLAG+" integer);";
    //账号密码保存
    public static final String TABLE_NAME4="tab_pwd";
    public static final String USER_ACCOUNT="account";
    public static final String USER_PWD="pwd";
    public static final String CREATE_TABLE4="create table "
            +TABLE_NAME4+" ("
            +USER_ACCOUNT+" text primary key,"
            +USER_PWD+" text);";
}
